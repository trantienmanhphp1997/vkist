<?php


namespace App\Enums;


class EAppraisalBoard {
    const TYPE_IDEAL = 1;
    const TYPE_TOPIC = 2;
    const TYPE_COST = 3;
    const TYPE_PLAN = 4;
    const STATUS_INCOMPLETE = 0;
    const STATUS_COMPLETE = 1;

    public static function statusAppraisal()
    {
        return [
            self::STATUS_INCOMPLETE => __('data_field_name.approval_topic.status_incomplete'),
            self::STATUS_COMPLETE => __('data_field_name.approval_topic.status_complete'),
        ];
    }

    public static function getListType()
    {
        return [
            self::TYPE_IDEAL => __('data_field_name.approval_topic.type_ideal'),
            self::TYPE_TOPIC => __('data_field_name.approval_topic.type_topic'),
            self::TYPE_COST => __('data_field_name.approval_topic.type_cost_estimate'),
            self::TYPE_PLAN => __('data_field_name.approval_topic.type_research_plan'),
        ];
    }
}
