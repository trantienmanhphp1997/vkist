@extends('layouts.master')

@section('homeLeft')
    @include('layouts.partials.sliderbar._categoryNewLeft')
@endsection
@section('content')
    <livewire:admin.news.detail :new="$new"/>
@endsection
