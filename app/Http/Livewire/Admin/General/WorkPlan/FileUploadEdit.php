<?php

namespace App\Http\Livewire\Admin\General\WorkPlan;

use App\Component\SizeFile;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;

class FileUploadEdit extends Component
{
    use WithFileUploads;
    public $work_plan_id;
    public $file;
    public $maximumFileSize = 255; // Mb
    public function render()
    {
        if ($this->file) {
            $this->store();
            $this->file = null;
        }
        $files = \App\Models\File::whereIn('type', [-2,1])->where('model_name', 'App/Models/WorkPlan')->where('admin_id',auth()->id())->where('model_id',$this->work_plan_id)->get();
        return view('livewire.admin.general.work-plan.file-upload-edit', compact('files'));
    }

    public function store()
    {
        $files = \App\Models\File::whereIn('type', [-2,1])->where('model_name', 'App/Models/WorkPlan')->where('admin_id',auth()->id())->where('model_id',$this->work_plan_id)->get();
        if(count($files)>=5) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.upload.maximum_uploads')]);
            return;
        }
        // check file size
        if ($this->file->getSize() > $this->maximumFileSize * 1024 * 1024) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.upload.maximum_size', ['value' => $this->maximumFileSize])]);
            return;
        }
        $file = $this->file;
        $size = new SizeFile();
        $size=$size->sizeFile($file->getSize());

        $file_name = $file->getClientOriginalName();
        $file_name_hash = Str::random(20) . '.' . $file->getClientOriginalExtension();
        $url = $this->file->storeAs('uploads/work-plan/files/' . auth()->id(), $file_name_hash, 'local');
        $file_upload = new \App\Models\File();
        $file_upload->url = $url;
        $file_upload->file_name = $file_name;
        $file_upload->size_file = $size;
        $file_upload->model_id = $this->work_plan_id;
        $file_upload->model_name = 'App/Models/WorkPlan';
        $file_upload->type = -2;
        $file_upload->save();
    }
    public function deleteFile($id){
        \App\Models\File::findOrFail($id)->delete();
    }
}
