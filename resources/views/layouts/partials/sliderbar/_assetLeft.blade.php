<!--start sidebar-left-->
<div class="col-md-2 col-xl-3 sidebar-left" id="asset_left">
    <ul class="nav flex-column memu-main-left">
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/Commode.png" alt="commode" height="24"></span> {{__('data_field_name.asset.list_title')}}
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul id="drop_menu" class="dropdown-menu-left active">
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span>{{__('menu_management.menu_name.asset')}}
                    </a>
                    <ul id="drop_menu" class="dropdown-menu-left active">
                        @if(checkPermission('admin.asset.dashboard.index'))
                            <li>
                                <a class="nav-link active" href="{{route('admin.asset.dashboard.index')}}">
                                    <span class="circle"></span> Dashboard
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.asset.category.index'))
                            <li>
                                <a class="nav-link" href="{{route('admin.asset.category.index')}}">
                                    <span
                                        class="circle"></span> {{__('data_field_name.asset_category.asset_category_list')}}
                                </a>
                            </li>
                        @endif

                        @if(checkPermission('admin.asset.index'))
                            <li>
                                <a class="nav-link" href="{{route('admin.asset.index')}}">
                                    <span class="circle"></span> {{__('data_field_name.asset.list_asset')}}
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> {{__('menu_management.menu_name.asset_volatility')}}
                    </a>
                    <ul id="drop_menu" class="dropdown-menu-left active">
                        @if(checkPermission('admin.asset.allocated-revoke.index'))
                            <li>
                                <a class="nav-link" href="{{route('admin.asset.allocated-revoke.index')}}">
                                    <span
                                        class="circle"></span> {{__('menu_management.menu_name.asset-allocated-revoke')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.asset.transfer.index'))
                            <li>
                                <a class="nav-link" href="{{route('admin.asset.transfer.index')}}">
                                    <span class="circle"></span> {{__('menu_management.menu_name.transfer')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.asset.lost-cancel-liquidation.index'))
                            <li>
                                <a class="nav-link" href="{{route('admin.asset.lost-cancel-liquidation.index')}}">
                                    <span class="circle"></span> {{__('menu_management.menu_name.lost-cancel-liquidation')}}
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

                @if(checkPermission('admin.asset.maintenance.index'))
                    <li>
                        <a class="nav-link" href="{{route('admin.asset.maintenance.index')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.maintenance')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.asset.inventory.index'))
                    <li>
                        <a class="nav-link" href="{{route('admin.asset.inventory.index')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.inventory')}}
                        </a>
                    </li>
                @endif
                <li>
                    <a class="nav-link" href="{{route('admin.asset.inventory.index')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.asset_report')}}
                    </a>
                </li>
                @if(checkPermission('admin.asset.allocated-revoke.index'))
                    <li>
                        <a class="nav-link" href="{{route('admin.asset.allocated-revoke.index')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.asset-allocated-revoke')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.asset.transfer.index'))
                    <li>
                        <a class="nav-link" href="{{route('admin.asset.transfer.index')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.transfer')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.asset.provider.index'))
                    <li>
                        <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.asset.provider.index'])) active @endif" href="{{route('admin.asset.provider.index')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.asset-provider')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
    </ul>
</div>
<!--end sidebar-left-->
