<?php

namespace App\Exports;

use App\Enums\EAssetStatus;
use App\Models\AssetAllocatedRevoke;
use App\Models\AssetMaintenance;
use App\Models\Department;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class AssetNeedMaintenanceExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $searchName;

    public function __construct($searchName = '')
    {
        $this->searchName = $searchName;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $index = 1;
        $query = AssetMaintenance::where('status',\App\Enums\EAssetStatus::REPORT_MAINTENANCE)
            ->orWhere('status',\App\Enums\EAssetStatus::REPORT_REPAIR)
            ->with('asset');
        if($this->searchName){
            $query->whereHas('asset',function(Builder $q){
                $q->where('asset.unsign_text','like','%'. strtolower(removeStringUtf8($this->searchName)).'%');

            });
        }

        $assets = $query->get();
        foreach ($assets as $asset) {
            $asset->index = $index++;
        }
        return $assets;
    }

    public function headings(): array
    {
        return [
            'STT',
            'MÃ tài sản',
            'Số biên bản',
            'tên tài sản',
            'Loại tài sản',
            'Nội dung sửa chữa-bảo dưỡng',
            'tình trạng',
            'Đối tượng sử dụng'
        ];
    }

    public function map($asset): array
    {
        return [
            $asset->index,
            $asset->asset->code ?? '',
            $asset->number_report ?? '',
            $asset->asset->name ?? '',
            $asset->asset->category->name ?? '',
            $asset->status==\App\Enums\EAssetStatus::REPORT_REPAIR?($asset->content_repair ?? ''):($asset->rule_maintenance->content_maintenance??''),
            $asset->status == \App\Enums\EAssetStatus::REPORT_REPAIR ? 'Chờ sửa chữa':'Chờ bảo dưỡng',
            $asset->asset->user->fullname ?? '',

        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E','F','G','H','I'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:E1');
            $event->sheet->setCellValue('A1', 'DANH SÁCH TÀI SẢN CHỜ SỬA CHỮA BẢO DƯỠNG');
            $event->sheet->getStyle('A1:E1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:I3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        }, ];

    }
    public function title(): string
    {
        return 'DANH SÁCH TÀI SẢN CHỜ SỬA CHỮA BẢO DƯỠNG';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
