<?php

namespace App\Http\Livewire\Admin\News;

use App\Http\Livewire\Base\BaseLive;
use App\Models\CategoryNews;
use App\Models\News;
use App\Models\User;
use App\Service\Community;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;


class Index extends BaseLive
{
    protected $IS_PUBLISHED = 1;
    protected $NOT_PUBLISHED = 0;

    public $status;
    public $searchKey;
    public $news_id;
    public $category_id;
    public $user_id;
    public $timeSearchField;
    public $nameSearchField;
    public $isFillerSearchBox = false;
    public $checkDownloadPermission ;
    public $files = [];

    public function mount($status)
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
        $this->checkDownloadPermission = checkRoutePermission('download');
        $this->status = $status;
    }

    public function render()
    {
        $searchKey = $this->searchKey;
        $nameFieldSearch = $this->nameSearchField;
        $timeFieldSearch = $this->timeSearchField;
        $categoryName = $this->category_id;
        $userName = $this->user_id;

        $query = News::query()->with(['file','admin']);

        if (!Auth::user()->hasRole('administrator')) {
            if (!checkPermission('admin.news.posts.approve')){
                $query->where([
                    'admin_id' => Auth::id()
                ]);
            }


            if ($this->status == News::NEWS_POSTED) {
                $query->whereIn('status', [
                    News::DRAFT_NEWS,
                    News::NEWS_POSTED,
                    News::NEWS_NEED_APPROVAL,
                    News::APPROVED_NEWS
                ]);
            } else {
                $query->where(['status' => $this->status]);
            }
        }
        else {
            if ($this->status == News::NEWS_POSTED) {
                $query->whereIn('status', [
                    News::NEWS_POSTED,
                    News::APPROVED_NEWS
                ]);
            } else {
                $query->where(['status' => $this->status]);
            }
        }

        if ($this->isFillerSearchBox) {
            $query->whereRaw("unsign_text LIKE '%".strtolower(removeStringUtf8(trim($nameFieldSearch)))."%'")
                ->whereRaw("updated_at LIKE '%{$timeFieldSearch}%'")
                ->whereHas('admin', function ($query) use ($userName) {
                    $query->whereRaw("LOWER(name) LIKE LOWER('%{$userName}%')");
                })
                ->whereHas('category', function ($query) use ($categoryName) {
                    $query->whereRaw("LOWER(name) LIKE LOWER('%{$categoryName}%')");
                });
        }

        if ($this->searchTerm != null) {
            $searchKey = $this->searchTerm;
            $query->whereRaw("unsign_text LIKE '%".strtolower(removeStringUtf8(trim($this->searchTerm)))."%'")
                ->orWhereHas('admin', function ($query) use ($searchKey) {
                    $query->whereRaw("unsign_text LIKE LOWER('%".strtolower(removeStringUtf8(trim($searchKey)))."%')");
                })
                ->orWhereHas('category', function ($query) use ($searchKey) {
                    $query->whereRaw("unsign_text LIKE LOWER('%".strtolower(removeStringUtf8(trim($searchKey)))."%')");
                });
        }

        $data = $query->orderBy('id', 'DESC')
            ->paginate($this->perPage);

        $categoryList = CategoryNews::pluck('name', 'name');
        $userList = User::pluck('name', 'name');
        return view('livewire.admin.news.index', ['data' => $data, 'categoryList' => $categoryList, 'userList' => $userList]);
    }

    public function boldTextSearch($data, $searchKey, $isFillerSearchBox)
    {
        if (!empty($searchKey) || !$isFillerSearchBox) {
            foreach ($data as $item) {
                if ($item->category) {
                    $item->category->name = str_replace(strtolower($searchKey), '<b>' . $searchKey . '</b>', strtolower($item->category->name));
                }
                $item->name = str_replace(strtolower($searchKey), '<b>' . $searchKey . '</b>', strtolower($item->name));
                $item->admin->name = str_replace(strtolower($searchKey), '<b>' . $searchKey . '</b>', strtolower($item->admin->name));
            }
        }

        return $data;
    }

    public function setNewsId($id)
    {
        $this->news_id = $id;
    }

    public function delete()
    {
        News::findOrFail($this->news_id)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __("news/newsManager.menu_name.success-to-delete-msg")] );
    }

    public function filterAnswers()
    {
        $this->isFillerSearchBox = true;
    }

    public function resetInputFieldBox()
    {
        $this->user_id = "";
        $this->category_id = "";
        $this->nameSearchField = "";
        $this->timeSearchField = "";
    }

    public function closeFillerSearchBox()
    {
        $this->resetInputFieldBox();
        $this->isFillerSearchBox = false;
    }

    public function publishNews($newId, $isPublished)
    {
        if ($isPublished) {
            News::findOrFail($newId)->update(['published' => $this->IS_PUBLISHED]);
        } else {
            News::findOrFail($newId)->update(['published' => $this->NOT_PUBLISHED]);
        }
    }

    public function getFileList($id){
        $this->files = \App\Models\File::query()->where('model_id', $id)->where(['model_name'=>News::class])->get();
    }

    public function download($fileUrl)
    {
        try {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('news/newsManager.menu_name.download-file-success')]);
            return Community::downloadFile($fileUrl);
        } catch (\Exception $e) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" =>  __('news/newsManager.menu_name.download-file-fail')]);
        }
    }

    public function transferTrending($id,$is_trending){
        News::findOrFail($id)->update([
            'is_trending'=>$is_trending
        ]);
        if ($is_trending == 1){
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>  __('news/newsManager.menu_name.transfer-to-trending-on')]);
        }else{
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>  __('news/newsManager.menu_name.transfer-to-trending-off')]);
        }
    }
}
