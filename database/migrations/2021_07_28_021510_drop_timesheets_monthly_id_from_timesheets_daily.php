<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropTimesheetsMonthlyIdFromTimesheetsDaily extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timesheets_daily', function (Blueprint $table) {
            $table->dropColumn('timesheets_monthly_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timesheets_daily', function (Blueprint $table) {
            $table->foreignId('timesheets_monthly_id')->nullable()->constrained('timesheets_daily');
        });
    }
}
