<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{ __('data_field_name.department.department_list') }}</h3>
            <div class="col-md-12 approve">
                <div class="row rowUser">
                    <div id="search" class="w-100 mb-3">
                        <div class="row">
                            <div class="col-md-6">

                                <div class="d-inline-block search-expertise">
                                    <div class="search-expertise">
                                        <input type="text" class="form-control size13" placeholder="{{ __('data_field_name.department.search') }}" name="searchTerm" autocomplete="off" wire:model.debounce.1000ms="searchTerm">
                                        <span>
                                            <img src="/images/Search.svg" alt="search">
                                        </span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 d-flex justify-content-end">
                                @if ($checkCreatePermission)
                                    <button wire:click="action('create', 0)" class="border-0 p-0 bg-white mr-3 " data-toggle="modal" data-target="#department-editor-modal" title="{{ __('common.button.create') }}">
                                        <img src="/images/filteradd.png" alt="filteradd">
                                    </button>
                                @endif

                                <button class="border-0 p-0 bg-white" data-toggle="modal" data-target="#export-modal" title="{{ __('common.button.export_file') }}">
                                    <img src="/images/filterdown.svg" alt="filterdown">
                                </button>
                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general" id="coUser">
                            <thead>
                                <tr class="title-approve">
                                    <th scope="col  text-center">{{ __('data_field_name.department.stt') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.code') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.name') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.type_class') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.type_group') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.type_unit') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.Number of employees') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.note') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.status') }}</th>
                                    <th scope="col text-center">{{ __('data_field_name.department.action') }}</th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>

                                @forelse($data as $row)
                                    <tr>
                                        <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                        <td>{{ $row->code }}</td>
                                        <td>
                                            <a href="" data-toggle="modal" data-target="#department-editor-modal" wire:click="action('detail', {{ $row->id }})">{!! boldTextSearch($row->name, $searchTerm) !!}</a>
                                        </td>
                                        <td scope="col text-center">{!! $row->class !!}</td>
                                        <td scope="col text-center">{!! $row->group !!}</td>
                                        <td scope="col text-center">{!! $row->unit !!}</td>

                                        <td>{{ $row->users_count }}</td>
                                        <td>{{ $row->note }}</td>
                                        <td>{{ $row->status == 0 ? __('data_field_name.system.role.active') : __('data_field_name.system.role.inactive') }}</td>
                                        <td>
                                            @if ($this->checkEditPermission)
                                                <button type="button" data-toggle="modal" data-target="#department-editor-modal" title="{{ __('common.button.edit') }}" wire:click="action('edit', {{ $row->id }})" class="btn-sm border-0 bg-transparent mr-1 show_modal_edit">
                                                    <img src="/images/edit.png" alt="edit">
                                                </button>
                                            @endif

                                            @if ($this->checkDestroyPermission)
                                                @include('livewire.common.buttons._delete')
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-center text-danger">
                                        <td colspan="12">{{ __('common.message.no_data') }}</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    {{ $data->links() }}
                    <div id="pageUser" class="title-approve ">

                    </div>

                </div>
            </div>

            <div wire:ignore.self class="modal fade" id="department-editor-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    @livewire('admin.user.department.department-editor', ['id' => $departmentId, 'editable' => ($action != 'detail')], key(Str::random(5)))
                </div>
            </div>

            @include('livewire.common.modal._modalDelete')
            @include('livewire.common.modal._modalConfirmExport')
            @include('livewire.admin.user._modalDeleteSelected')

        </div>
    </div>
</div>

<script>
    $("document").ready(function() {
        window.addEventListener('close-department-editor-event', () => {
            @this.render();
            document.getElementById('close-department-editor-btn').click()
        });
    })
</script>
