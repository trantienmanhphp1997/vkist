<?php


namespace App\Enums;


class EUserInfoLeaveStatus
{
    const PROCESSING = 0;
    const DONE = 1;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::PROCESSING:
                $result = __('executive/user-info-leave.status.processing');
                break;
            case self::DONE:
                $result = __('executive/user-info-leave.status.done');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
