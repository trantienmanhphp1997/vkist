<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnv2ToRequestLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_leave', function (Blueprint $table) {
            $table->float('day_leave',10,1)->nullable()->comment('So ngay nghi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_leave', function (Blueprint $table) {
            $table->dropColumn('day_leave');
        });
    }
}
