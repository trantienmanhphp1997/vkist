<div class="col-md-8 col-xl-11 box-news box-user">
    <div class="row">
        <div class="col-md-12">
            <h3 class="title">
                {{__('user/group_user.title.view')}}

            </h3>
            <div class="information">
                <div class="group-tabs">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link {{$currentTab == 'group-user-tab' ? 'active' : ''}}" wire:click="setCurrentTab('group-user-tab')" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                               role="tab" aria-controls="nav-home" aria-selected="true"><span>{{__('user/group_user.tab1')}}
                                </span></a>
                            <a class="nav-item nav-link {{$currentTab == 'topic-tab' ? 'active' : ''}}" wire:click="setCurrentTab('topic-tab')" id="nav-topic-tab" data-toggle="tab" href="#nav-topic"
                               role="tab" aria-controls="nav-topic" aria-selected="false"><span>{{__('user/group_user.tab2')}}</span></a>
                        </div>
                    </nav>
                    {{--                    {!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'autocomplete' => "off", 'enctype' => 'multipart/form-data', 'route' => ['admin.user.update', $data->id]]) !!}--}}
                    @csrf
                    <div class="tab-content" id="nav-tabContent">
                        <div  class="tab-pane fade {{$currentTab == 'group-user-tab' ? 'active show' : ''}}" id="nav-home" role="tabpanel"
                              aria-labelledby="nav-home-tab">
                            @include('admin.user.groupUser.tab._info-detail-tab')
                        </div>
                        <div  class="tab-pane fade {{$currentTab == 'topic-tab' ? 'active show' : ''}}" id="nav-topic" role="tabpanel" aria-labelledby="nav-topic-tab">
                            @include('admin.user.groupUser.tab._topic-detail-tab')
                        </div>
                    </div>
                    {{--                    {!! Form::close() !!}--}}
                </div>
                <div class="d-flex justify-content-between mt-3">
                    <div>&nbsp</div>
                    <div>
                        <button class="btn btn-light  mr-3" wire:click="back"
                                id="cancel-btn">{{ __('common.button.cancel') }}</button>
                    </div>
                    <div>&nbsp</div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
