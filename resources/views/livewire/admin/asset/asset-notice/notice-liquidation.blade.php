<div class="modal fade modal-fix" wire:ignore.self id="notice_liquidation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form wire:submit.prevent="submit">
                <div class="modal-header">
                    <div class="row width_100">
                        <div class="col-11">
                            <h6 class="text_center">{{__('data_field_name.asset.report_asset_liquidation')}}</h6>
                        </div>
                        <div class="col-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true close-btn">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-header">
                    <div class="form-group info-request">
                        <label class="text_center">{{__('data_field_name.asset.number_report')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" wire:model.defer="number_report" maxlength="10">
                        @error('number_report') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pd-col">
                                <div class="info-asset">
                                    @livewire('admin.asset.asset-notice.asset-info',['assetDetail'=>$assetDetail])
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.date_liquidation')}}<span
                                            class="text-danger">*</span></label>
                                    @include('layouts.partials.input._inputDate', ['input_id' => 'implementation_date', 'place_holder'=>'', 'default_date' => is_null($implementationDate) ? date('Y-m-d') : $implementationDate ])
                                    @error('implementation_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.asset_quantity')}}
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" wire:model.defer="implementation_quantity" class="num"
                                           maxlength="3"
                                           @if ($type_manage==\App\Enums\EAssetCategoryTypeManage::CODE)
                                           disabled
                                        @endif
                                    >
                                    @error('implementation_quantity') <span class="text-danger">{{ $message }}</span>@enderror
                                    @if(!empty($errorQuantity))
                                        <div class="text-danger mb-3">{{$errorQuantity}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>
                                        {{__('data_field_name.asset.reason_liquidation')}}
                                    </label>
                                    <textarea cols="30" rows="10" wire:model.defer="implementation_reason"></textarea>
                                    @error('implementation_reason') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div id="file_right">
                                    <label>{{__('data_field_name.asset.file')}}</label>
                                    @livewire('component.files',['uploadOnShow' => 1, 'model_name'=>$model_name,'canUpload'=> true, 'type'=>$type,'folder'=>$folder,'status'=>$status])

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" id="close-notice-liquidation"
                            data-dismiss="modal" wire:click.prevent="cancel()">{{__('common.button.cancel')}}
                    </button>
                    <button type="button" class="btn btn-primary"
                            wire:click.prevent="save()">{{__('common.button.perform')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
