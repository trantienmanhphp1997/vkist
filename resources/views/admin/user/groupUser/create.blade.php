@extends('layouts.master')
@section('title')
    <title>  {{__('user/group_user.title.create')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    <livewire:admin.user.group-user.form-data />
@endsection
@section('js')
@endsection
