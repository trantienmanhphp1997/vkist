 <!-- Modal Edit -->
      <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="target" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body box-user">
              <h4 class="modal-title" id="exampleModalLabel">{{__('data_field_name.department.edit_department')}}</h4>
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput11">{{__('data_field_name.department.name')}}<span class="text-danger">(*)</label>
                    <input type="text" class="form-control" id="exampleFormControlInput11" placeholder="{{__('data_field_name.department.input_name')}}" wire:model.defer="name">
                    @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">{{__('data_field_name.department.code')}}<span class="text-danger">(*)</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1"  wire:model.defer="code">
                    @error('code') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlInput14">{{__('data_field_name.department.under_department')}}</label>
                    <select name="parent_id" id="exampleFormControlInput14" class="form-control" wire:model.defer="parent_id">
                      <option value="">--{{__('data_field_name.department.master_room')}}--</option>
                      @foreach ($hierarchyDeparment as $value)
                      <option value="{{$value['id']}}">
                        {!!$value['padLeft'].$value['name']!!}
                      </option>
                      @endforeach
                    </select>
                    @error('parent_id') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlInput12">{{__('data_field_name.department.note')}}<span class="text-danger">(*)</label><br>
                    <textarea class="form-control" type="text" name="note" id="" cols="30" rows="5" wire:model.defer="note" placeholder="{{__('data_field_name.department.input_function_description')}}" style="width:100%;display: flex;flex-direction: row;
                        align-items: flex-start;position: static;border-radius: 5px;"></textarea>
                    @error('note') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlInput13">{{__('data_field_name.department.status')}}</label>
                    <select name="status" id="exampleFormControlInput13" class="form-control" wire:model.defer="status">
                      <option value="0">{{__('data_field_name.system.role.active')}}</option>
                      <option value="1">{{__('data_field_name.system.role.inactive')}}</option>
                    </select>
                    @error('status') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                </form>
              </div>
              <div class="group-btn2 text-center  pt-12">
                <button type="button" id="close-modal-edit-department" wire:click.prevent="resetInputFields()" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.close')}}</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-save">{{__('common.button.save')}}</button>
              </div>
            </div>
          </div>
        </div>
      </form>
      <!-- End Modal Edit -->
