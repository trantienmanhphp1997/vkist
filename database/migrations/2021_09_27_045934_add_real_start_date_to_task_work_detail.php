<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRealStartDateToTaskWorkDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_work_detail', function (Blueprint $table) {
            $table->date('real_start_date')->nullable()->comment('ngày bắt đầu công việc thực tế');
            $table->date('real_end_date')->nullable()->comment('ngày kết thúc công việc thực tế');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_work_detail', function (Blueprint $table) {
            $table->dropColumn('real_end_date');
            $table->dropColumn('real_start_date');
        });
    }
}
