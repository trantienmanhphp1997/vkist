<?php

namespace App\Exports;

use App\Models\UserWorkProcess;
use App\Models\UserInf;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
class UserWorkingProcessExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $getIdSelected;

    function __construct($user_info_id) {
        $this->user_info_id = $user_info_id;
    }

    public function collection()
    {
        $data = UserWorkProcess::where('user_info_id', $this->user_info_id)
            ->with(['position', 'department'])
            ->orderBy('user_working_process.id', 'desc')
            ->get();
        $data = $data->map(function ($item, $index) {
            return [
                'index' => $index + 1,
                'start_date' => !empty($item->start_date) ? date('d/m/Y',strtotime($item->start_date)) : null,
                'end_date' => !empty($item->end_date) ? date('d/m/Y',strtotime($item->end_date)) : null,
                'position' => !empty($item->position) ? $item->position->v_value : null,
                'department' => !empty($item->department) ? $item->department->name : null,
                'unit' => 'VKIST'
            ];
        });
        return $data;
    }
    public function headings():array
    {
        return
            [
                'STT',
                'Thời gian bắt đầu',
                'Thời gian kết thúc',
                'Chức danh',
                'Đơn vị',
                'Đơn vị công tác'
            ];
    }


    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F',
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('B1:F1');
            $event->sheet->setCellValue('B1','BÁO CÁO QUÁ TRÌNH CÔNG TÁC CỦA NHÂN SỰ');
            $event->sheet->getStyle('A6:F6')->applyFromArray(
                $default_font_style_title
            );
            $active_sheet->getStyle('A6:F6')->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $event->sheet->setCellValue('A3', 'Họ Tên');
            $event->sheet->setCellValue('D3', 'Ngày sinh');
            $user = UserInf::where('id', $this->user_info_id)->first();
            if (!empty($user)) {
                $event->sheet->setCellValue('B3', $user->fullname);
                $event->sheet->setCellValue('E3', date('d/m/Y',strtotime($user->birthday)));
                $event->sheet->getStyle('B3')->applyFromArray(['font' => ['bold' =>  true]]);
                $event->sheet->getStyle('E3')->applyFromArray(['font' => ['bold' =>  true]]);
            }
            // title
            $cellRange = 'B1:F1';

            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);
            $highestRow = $active_sheet->getHighestRow();
            $borderStyle = [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [
                       'rgb' => '808080'
                    ]
                ]
            ];
            $active_sheet->getStyle('A6:F' . $highestRow)->getBorders()->applyFromArray($borderStyle);
        },];

    }
    public function title(): string
    {
        return 'BÁO CÁO QUÁ TRÌNH CÔNG TÁC CỦA NHÂN SỰ';
    }

    public function startCell(): string
    {
        return 'A6';
    }
}
