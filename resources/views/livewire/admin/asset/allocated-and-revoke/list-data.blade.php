<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
                <div  class="breadcrumbs"><a href="{{route('admin.asset.allocated-revoke.index')}}">{{__('menu_management.menu_name.asset-management')}}</a> \ <span>{{__('menu_management.menu_name.asset-allocated-revoke')}}</span></div> 
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('menu_management.menu_name.asset-allocated-revoke')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="header-staff">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <a href="javascript:void(0)" style="border-bottom: 2px solid; padding-bottom: 10px">{{__('data_field_name.allocated-revoke.employee')}}</a>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="dropup navbar manipulation">
                                    <a class="btn btn-gray dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: black; font-size: 16px;">
                                        {{__('common.select-box.choosed')}}: {{count($arrData)}}
                                    </a>

                                    <div class="dropdown-menu px-2" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item text-left ml-2" style="cursor: pointer" id="report_lost_btn" wire:click="checked(0)">{{__('common.select-box.cancel-choose')}}</a>
                                        <a class="dropdown-item text-left ml-2" style="cursor: pointer" id="report_error_btn" wire:click="checked(1)">{{__('common.select-box.choose_all')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block search-staff">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="form-control size13" placeholder="{{__('data_field_name.allocated-revoke.search')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span> 
                                </div>
                                <div class=" inline-block">
                                    @if($checkCreatePermission)
                                        @if(count($arrData) > 0)
                                            <a href="{{route('admin.asset.allocated.create', ['data' => base64_encode(json_encode($arrData))])}}" class="btn mrl16 btn-gray btn-gray2">
                                                <img src="/images/Unlock2.svg" alt="unlock">
                                                {{__('data_field_name.allocated-revoke.btn_allocated')}}
                                            </a>
                                            <a href="{{route('admin.asset.revoke.create', ['data' => base64_encode(json_encode($arrData))])}}" class="btn mrl16 btn-gray btn-gray2">
                                                <img src="/images/Lock2.svg" alt="lock">
                                                {{__('data_field_name.allocated-revoke.btn_revoke')}}
                                            </a>
                                        @else
                                            <button disabled="" class="btn mrl16 btn-gray btn-gray2"><img src="/images/Unlock2.svg" alt="unlock">{{__('data_field_name.allocated-revoke.btn_allocated')}}</button>
                                            <button disabled="" class="btn  btn-gray mrl16 btn-gray2"><img src="/images/Lock2.svg" alt="unlock">{{__('data_field_name.allocated-revoke.btn_revoke')}}</button>
                                        @endif
                                    @endif
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group float-right">
                                @if(checkRoutePermission('download'))
                                    <button title="{{__('common.button.export_file')}}" type="button" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#exampleModal"> <img src="/images/filterdown.svg" alt="filterdown"> </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                                <tr class="border-radius">
                                    <th scope="col" class="border-radius-left">
                                        @if(count($data) > 0)
                                            @if(!is_numeric($this->selectedId) && $this->isChecked == 1)
                                                <label class="box-checkbox checkbox-analysis" wire:click="checked(0)">
                                                    <input type="checkbox" checked="" class="select_all">
                                                    <span class="checkmark"></span>
                                                </label>
                                            @else
                                                <label class="box-checkbox checkbox-analysis" wire:click="checked(1)">
                                                    <input type="checkbox" class="select_all">
                                                    <span class="checkmark"></span>
                                                </label>
                                            @endif
                                        @endif
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.allocated-revoke.user_code')}}
                                    </th>
                                    <th scope="col">
                                        {{__('data_field_name.allocated-revoke.user_name')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.allocated-revoke.gender')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.allocated-revoke.phone')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.allocated-revoke.email')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.allocated-revoke.position')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.allocated-revoke.deparment')}}
                                    </th>
                                    <th scope="col" class="text-center border-radius-right">
                                    {{__('data_field_name.allocated-revoke.number_of_assets_in_use')}}
                                    </th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                                @foreach($data as $index => $row)
                                    <tr>
                                        <td class="text-center">
                                            <label class="box-checkbox checkbox-analysis" wire:click="selectedItem({{$row['id']}}, {{$row['selected']}})">
                                                @if($row['selected'] == 1)
                                                    <input type="checkbox" class="select_all" checked="">
                                                @else
                                                    <input type="checkbox" class="select_all">
                                                @endif
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                        <td class="text-center">{!! boldTextSearch($row['code'], $searchTerm) !!}</td>
                                        <td class="text-center">{!! boldTextSearch($row['fullname'], $searchTerm) !!}</td>
                                        <td class="text-center">{!! $row['gender'] == 0 ? 'Nam' : 'Nữ' !!}</td>
                                        <td class="text-center">{!! $row['phone'] !!}</td>
                                        <td class="text-center">{!! $row['email'] !!}</td>
                                        <td class="text-center">{!! $row['position'] !!}</td>
                                        <td class="text-center">{!! $row['department'] !!}</td>
                                        <td class="text-center">{!! $row['total'] !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row mb-4 mx-2">
                        <div class="col-md-3">
                            {{__('data_field_name.allocated-revoke.total_number_of_records')}}: <span class="font-weight-bold">{{$data->total()}}</span>
                        </div>
                        <div class="col-md-3">
                            {{__('data_field_name.allocated-revoke.total_assets_in_use')}}: <span class="font-weight-bold">{{$total_asset}}</span>
                        </div>
                        <div class="col-md-3">
                            <a class="font-weight-bold" href="{{route('admin.asset.dashboard.index', [])}}">{{__('data_field_name.allocated-revoke.status_statistics')}}</a>
                        </div>
                    </div>
                    @include('livewire.common._modalExportFile')
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center">
                            <span>{{__('common.message.empty_search')}}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')
                </div>
            </div>
        </div>
    </div>
</div>