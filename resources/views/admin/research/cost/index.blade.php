@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.research_cost.budget_list') }}</title>
@endsection
@section('homeLeft')
@include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
@livewire('admin.research.cost.topic-fee-list')
@endsection
