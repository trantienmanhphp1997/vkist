@extends('layouts.master')

@section('homeLeft')
    @include('layouts.partials.sliderbar._categoryNewLeft')
@endsection
@section('homeRight')
    @include('layouts.partials.sliderbar._newsRight')

@endsection
@section('content')
    <style>
        .search-expertise{
            float: right;
            padding: 8px 0 0;
            position: relative;
        }
        .form-control{
            padding: 0.375rem 2rem;
        }
        .search-expertise span {
            top: 13px;
            left: 10px;
        }
        @media screen and (min-width: 1750px) {
            .col-md-8{
                max-width: 66.666667% !important;
            }
        }

        @media screen and (max-width: 1750px) {
            .col-md-8{
                max-width: 60.666667% !important;
            }
        }
        @media screen and (max-width: 1460px) {
            .col-md-8{
                max-width: 57.666667% !important;
            }
        }
    </style>
    <div class="col-md-7 col-md-8 ">
        <div class="row" style="padding: 10px 32px;">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="/">{{__('news/newsManager.menu_name.home')}} \</a>
                    <span>{{__('news/newsManager.menu_name.list-new')}}</span>
                    </div>
            </div>
        </div>
        <livewire:admin.news.list-new :categorySlug="$categorySlug"/>
    </div>
@endsection
