<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryBasicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_basic', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('nhân viên');
            $table->foreignId('salary_sheets_file_id')->nullable()->constrained('salary_sheets_file')->comment('bảng lương');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->bigInteger('actual_workday')->nullable()->comment('ngay cong thuc te (D3)');
            $table->string('coefficients_salary')->nullable()->comment('he so luong (E4)');
            $table->bigInteger('salary')->nullable()->comment('luong (F5)');
            $table->json('allowance')->nullable()->comment('{position_allowance: phu cap chuc vu (G6), responsibility_allowance: phu cap trach nhiem (H7)}');
            $table->json('social_insurance')->nullable()->comment('{service1: (I8), service2: (J9), laborer: (K10)}');
            $table->json('health_insurance')->nullable()->comment('{service: (L11), laborer: (M12)}');
            $table->json('accident_insurance')->nullable()->comment('{service: (B13), laborer: (O14)}');
            $table->bigInteger('salary_allowance_insurance')->nullable()->comment('luong + phu cap - bao hiem (P15)');
            $table->bigInteger('salary_special')->nullable()->comment('luong theo co che dac thu (Q16)');
            $table->bigInteger('lunch_allowance')->nullable()->comment('phu cap an trua (R17)');
            $table->bigInteger('other_costs')->nullable()->comment('Chi phi khac (S18)');
            $table->bigInteger('arrears')->nullable()->comment('truy thu (T19)');
            $table->bigInteger('pursuit_salary')->nullable()->comment('truy linh luong (U20)');
            $table->bigInteger('bonus')->nullable()->comment('thuong (V21)');
            $table->bigInteger('amount_still_received')->nullable()->comment('so tien con duoc linh (W22)');
            $table->bigInteger('reduce_yourself')->nullable()->comment('giam tru ban than (X23)');
            $table->bigInteger('number_of_dependents')->nullable()->comment('so nguoi phu thuoc (Y24)');
            $table->bigInteger('taxable_income')->nullable()->comment('thu nhap tinh thue (Z25)');
            $table->bigInteger('personal_income_tax')->nullable()->comment('thue thu nhap ca nhan tam thu (AA26)');
            $table->bigInteger('total_salary_received')->nullable()->comment('tong luong duoc nhan (AB27)');
            $table->string('note', 1000)->nullable()->comment('ghi chu (AC28)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_basic');
    }
}
