<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGwApproveResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gw_approve_response', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('model_id')->nullable()->comment('id topic, ideal, ...');
            $table->string('model_name', 255)->nullable()->comment('tên model');
            $table->string('request_id',1000)->nullable()->comment('id request');
            $table->bigInteger('admin_id')->nullable()->comment('Người tạo');

            $table->string('gw_document_id',1000)->nullable()->comment('id document');
            $table->string('gw_date_approve_time',1000)->nullable()->comment('Thời gian phê duyệt');
            $table->string('gw_status',1000)->nullable()->comment('trạng thái');
            $table->string('gw_userid',1000)->nullable()->comment('Người phê duyệt');
            $table->string('gw_empno',1000)->nullable()->comment('Mã nhân viên');
            $table->string('gw_comment', 1000)->nullable()->comment('Comment');
            $table->string('gw_next_users',1000)->nullable()->comment('Người phê duyệt tiếp theo');
            $table->string('gw_title', 1000)->nullable()->comment('Tiêu đề tài liệu');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gw_approve_response');
    }
}
