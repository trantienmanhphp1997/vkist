<?php


namespace App\Enums;


class EAssetStatus
{
    const ALLOCATE = 1;
    const RECALL = 2;
    const TRANSFER = 3;

    const REPORT_LOST = 4;
    const REPORT_CANCEL = 5;
    const REPORT_LIQUIDATION = 6;

    const LOSTED = 7;
    const CANCELLED = 8;
    const LIQUIDATION = 9;

    const REPORT_REPAIR = 10;
    const REPORT_MAINTENANCE = 11;
    const REPAIRED = 12;
    const MAINTENANCE = 13;

    const DELETED = 14;

    const USING = 15;
    const NO_USE = 16;



    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            EAssetStatus::USING => __('data_field_name.asset.using'),
            EAssetStatus::NO_USE => __('data_field_name.asset.no_use'),
            EAssetStatus::REPORT_LOST => __('data_field_name.asset.lost_report'),
            EAssetStatus::REPORT_CANCEL => __('data_field_name.asset.broken_report'),
            EAssetStatus::REPORT_LIQUIDATION => __('data_field_name.asset.report_liquidation'),
            EAssetStatus::LOSTED => __('data_field_name.asset.losted'),
            EAssetStatus::CANCELLED => __('data_field_name.asset.cancelled'),
            EAssetStatus::LIQUIDATION => __('data_field_name.asset.liquidated'),
            EAssetStatus::REPORT_REPAIR => __('data_field_name.asset.report_repair'),
            EAssetStatus::REPORT_MAINTENANCE => __('data_field_name.asset.report_maintenance'),
            EAssetStatus::REPAIRED => __('data_field_name.asset.repaired'),
            EAssetStatus::MAINTENANCE => __('data_field_name.asset.maintenance'),
        ];
    }

}
