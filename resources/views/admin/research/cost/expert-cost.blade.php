@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.research_cost.expert_cost') }}</title>
@endsection
@section('homeLeft')
@include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    @livewire('admin.research.cost.expert-cost-list', ['topicFeeDetailId' => $data->id, 'topicFeeId' => $data->topic_fee_id, 'topicFeeStatus' => $data->topicFee->status])
@endsection
