<?php


namespace App\Enums;


class ESurveyRequestStatus
{
    const NOT_STARTED = 1;
    const IN_PROGRESS = 2;
    const DONE = 3;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::NOT_STARTED:
                $result = __('data_field_name.request-survey-list.not_started');
                break;
            case self::IN_PROGRESS:
                $result = __('data_field_name.request-survey-list.in_progress');
                break;
            case self::DONE:
                $result = __('data_field_name.request-survey-list.done');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
