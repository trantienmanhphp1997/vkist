<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicDetailWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic_detail_work', function (Blueprint $table) {
            $table->id()->comment('Noi dung cong viec chi tiet de tai');
            $table->string('content', 1000)->comment('Noi dung cong viec');
            $table->string('expected_result', 1000)->comment('Du kien ket qua');
            $table->string('wage_coefficient', 10)->comment('He so tien cong');
            $table->integer('number_workday')->comment('So ngay lam viec');
            $table->bigInteger('total')->comment('Tong tien cong');
            $table->unsignedBigInteger('topic_has_user_info_id')->nullable()->comment('Map voi topic_has_user_info');
            $table->foreign('topic_has_user_info_id')->references('id')->on('topic_has_user_info');
            $table->bigInteger('state_capital')->nullable()->comment('Nguon von nha nuoc');
            $table->bigInteger('other_capital')->nullable()->comment('Nguon khac');
            $table->unsignedBigInteger('topic_labor_participation_id')->nullable()->comment('Map voi topic_labor_participation');
            $table->foreign('topic_labor_participation_id')->references('id')->on('topic_labor_participation');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topic_labor_participation');
    }
}
