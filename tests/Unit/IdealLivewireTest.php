<?php

namespace Tests\Unit;

use App\Http\Livewire\Admin\Research\Ideal\IdealList;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;
use App\Models\Ideal;
use App\Models\User;

class IdealLivewireTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test  */
    public function testSaveSuccessfulIdeal() {
        $user = User::first();
        if (empty($user)) {
            $user = User::factory()->create();
        }
        Livewire::actingAs($user)->test('admin.research.ideal.form-data', [
            'code' =>'AAA',
            'name' => 'john',
            'proposer' => 1,
            'agencies' => 'hospital',
            'necessity' => 'normal',
            'target' => 'Những người béo phì',
            'content' => 'Cách giảm cân',
            'result' => 'Cách giảm cân',
            'execution_time' => '15-01-2020',
            'fee' => '1000',
            'status' => '0',
            'admin_id' => '2',
            'research_field' => 1
        ])
        ->call('saveIdeal')->assertHasNoErrors();
    }


    public function testListIdealLivewireComponent() {
        $user = User::first();
        if (empty($user)) {
            $user = User::factory()->create();
        }
        Livewire::actingAs($user)->test('admin.research.ideal.ideal-list')
        ->call('render');
    }
}
