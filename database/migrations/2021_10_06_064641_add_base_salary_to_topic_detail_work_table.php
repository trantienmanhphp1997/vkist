<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBaseSalaryToTopicDetailWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_detail_work', function (Blueprint $table) {
            $table->bigInteger('base_salary')->nullable()->comment('Luong co so');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_detail_work', function (Blueprint $table) {
            $table->dropColumn('base_salary');
        });
    }
}
