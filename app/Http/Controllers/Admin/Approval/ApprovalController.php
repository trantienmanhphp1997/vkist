<?php

namespace App\Http\Controllers\Admin\Approval;

use App\Enums\EApproval;
use App\Enums\EIdeaStatus;
use App\Enums\ETopicSource;
use App\Http\Controllers\Controller;
use App\Models\AppraisalBoard;
use App\Models\Approval;
use App\Models\ApprovalIdealDetail;
use App\Models\Ideal;
use App\Models\ResearchPlan;
use App\Models\Topic;
use App\Models\TopicFee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ApprovalController extends Controller
{
    public function index()
    {
        return view('admin.approval.index');
    }

    public function create()
    {
        return view('admin.approval.create');
    }

    public function edit($idApproval)
    {
        $approval = Approval::findOrFail($idApproval);
        return view('admin.approval.edit', compact('approval'));
    }

    public function detail($idApproval)
    {
        $approval = Approval::findOrFail($idApproval);
        $modelId = $appraisalName =  $listIdeal =  $appraisal_board_id = null;
        $topic = $topic_fee = $research_plan = null;

        if ($approval->type == EApproval::TYPE_IDEAL) {
            $modelId = $approval->id;
        }
        if ($approval->type == EApproval::TYPE_TOPIC) {
            $modelId = $approval->topic_id;
            $topic = Topic::findOrFail($approval->topic_id);
        }
        if ($approval->type == EApproval::TYPE_COST) {
            $modelId = $approval->topic_fee_id;
            $topic_fee = TopicFee::findOrFail($approval->topic_fee_id);
        }
        if ($approval->type == EApproval::TYPE_PLAN) {
            $modelId = $approval->research_plan_id;
            $research_plan = ResearchPlan::findOrFail($approval->research_plan_id);
        }

        if ($approval->type != EApproval::TYPE_IDEAL) {
            if (!empty($approval->appraisal_board_id)) {
                $appraisalBoard = AppraisalBoard::findOrFail($approval->appraisal_board_id);
                $appraisalName = $appraisalBoard->name;
                $appraisal_board_id = $approval->appraisal_board_id;
            }
        } else {
            $listIdeal = $this->getDataIdeal($idApproval);
        }

        $typeAppraisal = EApproval::getListType();
        $statusAppraisal = EApproval::getListStatus();
        $requestId =  time() . random_int(0,99);

        return view('admin.approval.detail', compact(
            'approval', 'idApproval', 'typeAppraisal', 'statusAppraisal', 'topic', 'topic_fee', 'research_plan', 'appraisalName',
            'listIdeal', 'appraisal_board_id', 'modelId', 'requestId'
        ));
    }

    public function getDataIdeal($idApproval) {
        $approvalIdeals = ApprovalIdealDetail::with('ideal', 'ideal.admin', 'ideal.admin.info', 'ideal.researchField')
                            ->where('approval_id', '=', $idApproval)->get();

        $listIdeal = null;
        if ($approvalIdeals->isNotEmpty()) {
            foreach ($approvalIdeals as $approvalIdeal) {
                $listIdeal[] = [
                    'id' => $approvalIdeal->ideal->id ?? '',
                    'code' => $approvalIdeal->ideal->code ?? '',
                    'name' => $approvalIdeal->ideal->name ?? '',
                    'fee' => $approvalIdeal->ideal->fee ?? '',
                    'research_name' => $approvalIdeal->ideal->researchField->name ?? '',
                    'fullname' => $approvalIdeal->ideal->admin->info->fullname ?? '',
                    'created_at' => isset($approvalIdeal->ideal->created_at) ? reFormatDate($approvalIdeal->ideal->created_at,'d/m/Y') : '',
                ];
            }
        }

        return $listIdeal;
    }
}
