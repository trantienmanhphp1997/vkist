<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="{{ route('admin.research.topic-fee.index') }}">{{ __('data_field_name.research_cost.list_cost') }}</a> \ <span>{{ __('data_field_name.research_cost.text_create_type_people') }}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{ __('data_field_name.research_cost.text_cost_1') }}</h3>
            <p>{{ __('data_field_name.research_cost.text_material_cost') }}</p>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="col-md-12" wire:ignore>
                        <div class="form-group float-right" >
                            <a href="{{ route('admin.research.topic-fee.labor.detail', $topicFeeDetailId) }}" class="btn btn-viewmore-news mr0">
                                {{__('data_field_name.research_cost.detail_for_topic')}}
                            </a>
                        </div>
                    </div>
                    <table class="table">
                        <thead>
                            <tr class="border-radius text-center">
                                <th scope="col" class="border-radius-left">
                                    {{ __('data_field_name.common_field.stt') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.fullname_perform') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.acdymic_topic') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.wage_coefficient') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.number_workday') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.base_salary') }}
                                </th>
                                <th scope="col" class="border-radius-right">
                                    {{ __('data_field_name.research_cost.total_wages') }}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center"><em>(1)</em></td>
                                <td class="text-center"><em>(2)</em></td>
                                <td class="text-center"><em>(3)</em></td>
                                <td class="text-center"><em>(4)</em></td>
                                <td class="text-center"><em>(5)</em></td>
                                <td class="text-center"><em>(6)</em></td>
                                <td class="text-center"><em>(7)=(4)*(5)*(6)</em></td>
                            </tr>
                            @foreach ($data as $row)
                                <tr>
                                    <td class="text-center">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                    <td class="text-center">{!! $row->topicHasUserInfo->userInfo->fullname ?? '' !!}</td>
                                    <td class="text-center">{!! $row->topicHasUserInfo->role->v_value ?? '' !!}</td>
                                    <td class="text-center">{{ $arrDataUser[$row->topic_has_user_info_id]['wage_coefficient'] ?? '' }}</td>
                                    <td class="text-center">{{ $row->total_number_workday ?? '' }}</td>
                                    <td class="text-center">{{ numberFormat((int)$arrDataUser[$row->topic_has_user_info_id]['base_salary'] ?? '' )}}</td>
                                    <td class="text-center">{{ numberFormat($row->total) ?? '' }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td class="text-center">{{ __('data_field_name.research_cost.sum') }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-center">{{ numberFormat($dataTotal['total']) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')

                    <div class="col-md-12 text-center mt-5">
                        <a href="{{route('admin.research.topic-fee.edit', $topicFeeId)}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $("document").ready(function() {
            $('.buttonCreate, .buttonEdit').click(function() {
                $('.error').remove();
                $('.form-input').val('');
            });

            $('#createLaborParticipation, #editLaborParticipation').on('hidden.bs.modal', function () {
                $('.error').remove();
                window.livewire.emit('resetData');
            });
        });
    </script>
</div>
