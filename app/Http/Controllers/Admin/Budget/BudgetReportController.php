<?php

namespace App\Http\Controllers\Admin\Budget;

use App\Http\Controllers\Controller;

class BudgetReportController extends Controller
{
    public function index()
    {
        return view('admin.budget.budget-report.index');
    }
}