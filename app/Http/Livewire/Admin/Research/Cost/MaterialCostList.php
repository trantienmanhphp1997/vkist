<?php

namespace App\Http\Livewire\Admin\Research\Cost;

use App\Enums\ETopicFee;
use App\Models\TopicFee;
use App\Models\TopicFeeDetail;
use App\Models\MaterialCost;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Topic;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class MaterialCostList extends BaseLive
{
    public $topicFeeDetailId;
    public $name;
    public $unit;
    public $quantity;
    public $price;
    public $state_capital;
    public $other_capital;
    public $materialCostID;
    public $model;
    public $type_upload;
    public $topicFeeId;
    public $total;
    public $topicFeeStatus;
    public $updateMode = false;
    public $displayAttachFile = true;


    public function mount($topicFeeDetailId, $topicFeeId, $topicFeeStatus) {
        $this->updateMode = false;
        $this->type_upload = Config::get('common.type_upload.MaterialCost');
        $this->topicFeeDetailId = $topicFeeDetailId;
        $this->topicFeeId = $topicFeeId;
        $this->topicFeeStatus = $topicFeeStatus;
    }

    public function render()
    {
        $this->model = MaterialCost::class;
        $this->updateTopicFeeDetail();
        $query = MaterialCost::query();

        $query->where('topic_fee_detail_id', '=', $this->topicFeeDetailId);
        $dataTotal = [
            'total' => $query->sum('total'),
            'state_capital' => $query->sum('state_capital'),
            'other_capital' => $query->sum('other_capital'),
        ];
        $data = $query->with('files')->orderBy('id', 'desc')->paginate($this->perPage);

        if ($this->quantity != null && $this->price != null) {
            $total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
            $this->total = numberFormat($total);
        }

        return view('livewire.admin.research.cost.material-cost-list', ['dataTopicFee' => $this->topicFeeDetailId, 'data' => $data,'model' => $this->model, 'type_upload' => $this->type_upload, 'model_id'=>$this->materialCostID, 'updateMode'=>$this->updateMode, 'dataTotal'=>$dataTotal]);
    }

    public function validateData()
    {
        $this->validate([
            'name' => 'required|max:100',
            'unit' => 'required|max:10',
            'quantity' => 'required|regex:/^[0-9,]+$/|max:7',
            'price' => 'required|regex:/^[0-9,]+$/|max:14',
            'state_capital' => 'nullable|regex:/^[0-9,]+$/|max:18',
            'other_capital' => 'nullable|regex:/^[0-9,]+$/|max:18',
        ],[
            'quantity.max' => __('client_validation.form.research_cost.max.quantity'),
            'price.max' => __('client_validation.form.research_cost.max.price'),
            'state_capital.max' => __('client_validation.form.research_cost.max.state_capital'),
            'other_capital.max' => __('client_validation.form.research_cost.max.other_capital'),
        ],[
            'name' => __('data_field_name.research_cost.name'),
            'unit' => __('data_field_name.research_cost.unit'),
            'quantity' => __('data_field_name.research_cost.quantity'),
            'price' => __('data_field_name.research_cost.price'),
            'state_capital' => __('data_field_name.research_cost.state_capital'),
            'other_capital' => __('data_field_name.research_cost.other_capital'),

        ]);
    }
    public function store()
    {
        $this->validateData();
        $materialCost = new MaterialCost;
        $materialCost->name = $this->name;
        $materialCost->unit = $this->unit;
        $materialCost->quantity = removeFormatNumber($this->quantity);
        $materialCost->price = removeFormatNumber($this->price);
        $materialCost->state_capital = removeFormatNumber($this->state_capital);
        $materialCost->other_capital = removeFormatNumber($this->other_capital);
        $materialCost->total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
        $materialCost->topic_fee_detail_id = $this->topicFeeDetailId;
        $materialCost->admin_id = Auth::user()->id;
        $materialCost ->save();
        \App\Models\File::where('model_name', MaterialCost::class)
                ->where('type', $this->type_upload)->where('admin_id', auth()->id())->where('model_id', null)->update([
                    'model_id' => $materialCost->id,
        ]);
        $this->displayAttachFile = false;
        $this->emit('close-modal-create-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function resetInputFields(){
        $this->displayAttachFile = true;
        $this->name = null;
        $this->unit = null;
        $this->quantity = null;
        $this->price = null;
        $this->total = null;
        $this->state_capital = null;
        $this->other_capital = null;
        $this->resetValidation();
    }

    public function edit($id){
        $this->updateMode = true;
        $this->materialCostID = $id;
        $materialCost = MaterialCost::findOrFail($id);
        $this->name = $materialCost->name;
        $this->unit = $materialCost->unit;
        $this->quantity = numberFormat($materialCost->quantity);
        $this->price = numberFormat($materialCost->price);
        $this->state_capital = numberFormat($materialCost->state_capital);
        $this->other_capital = numberFormat($materialCost->other_capital);
        $this->total = numberFormat($materialCost->total);
        $this->resetValidation();
    }

    public function update(){
        $materialCost = MaterialCost::findOrFail($this->materialCostID);
        $this->validateData();
        $materialCost->name = $this->name;
        $materialCost->unit = $this->unit;
        $materialCost->quantity = removeFormatNumber($this->quantity);
        $materialCost->price = removeFormatNumber($this->price);
        $materialCost->state_capital = removeFormatNumber($this->state_capital);
        $materialCost->other_capital = removeFormatNumber($this->other_capital);
        $materialCost->total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
        $materialCost->save();
        \App\Models\File::where('model_name', MaterialCost::class)
        ->where('type', $this->type_upload)->where('admin_id', auth()->id())->where('model_id', null)->update([
            'model_id' => $materialCost->id,
        ]);
        $this->emit('close-modal-edit-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }

    public function deleteId($id){
        $this->materialCostID = $id;
    }

    public function delete(){
        $materialCost = MaterialCost::findOrFail($this->materialCostID);

        $materialCost->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }

    public function updateTopicFeeDetail(){
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->topicFeeDetailId);
        $topicFeeDetail->total_capital = MaterialCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('total');
        $topicFeeDetail->state_capital = MaterialCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('state_capital');
        $topicFeeDetail->other_capital = MaterialCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('other_capital');
        $topicFeeDetail->save();
    }

    public function resetMode(){
        $this->updateMode = false;
    }

    public function download($id){
        $file = \App\Models\File::findOrFail($id);
        return Storage::download($file->url, $file->file_name);
    }
}
