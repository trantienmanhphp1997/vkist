<?php

namespace App\Http\Controllers\Admin\DecisionReward;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DecisionReward;

class DecisionRewardController extends Controller
{
    //


    public function create(){
        return view('admin.decision-reward.create');
    }

    public function index(){
        return view('admin.decision-reward.index');
    }

    public function edit($id){
        $data = DecisionReward::with(['receivers.info.department','receivers.info.position'])->findOrFail($id);
        if(is_null($data)) {
            return redirect()->route('admin.decision-reward.index');
        }
        return view('admin.decision-reward.edit', compact('data'));
    }

}
