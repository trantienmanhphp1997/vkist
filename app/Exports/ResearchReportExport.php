<?php

namespace App\Exports;

use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Illuminate\Support\Carbon;
use App\Enums\EAssetStatus;
use Illuminate\Support\Collection;
use App\Models\MasterData;
use App\Models\ResearchCategory;
use App\Models\ResearchProject;
use App\Models\TaskWork;
use App\Models\Topic;
use App\Enums\EMasterDataType;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
class ResearchReportExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnWidths, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($topicType, $start_date_filter, $end_date_filter, $implementing_agencies, $field, $type_report, $projectStatusId)
    {
        $this->start_date_filter = $start_date_filter;
        $this->end_date_filter = $end_date_filter;
        $this->topicType = $topicType;
        $this->implementing_agencies = $implementing_agencies;
        $this->field = $field;
        $this->type_report = $type_report;
        $this->projectStatusId = $projectStatusId;
        $this->topicId = null;
    }
    public function columnWidths(): array
    {
        return [
            'A' => 25,
            'B' => 25,
            'C' => 25,
            'D' => 25,
            'E' => 25,
            'F' => 25,
            'G' => 25,
            'H' => 25,
            'I' => 25,
            'J' => 25,
            'K' => 25,
            'L' => 25,
            'M' => 25,
            'N' => 25,            
        ];
    }
    public function collection()
    {
        $researchCategoryLevel = MasterData::where('type', EMasterDataType::TOPIC_LEVEL)->get();
        $topicList = Topic::all();
        // danh sách công việc trong kpi modal
        $taskListQuery = TaskWork::query()->with(['assigner'])->where(['topic_id'=>$this->topicId])->orderBy('id', 'desc')->get();
        $totalTaskExpired = 0 ;
        foreach ($taskListQuery as $task){
            $endDateByPlan = strtotime(date('Y-m-d', strtotime($task->end_date)));
            $endDateByCurrent = strtotime(date('Y-m-d', strtotime($task->real_end_date))) ;
            if ($task->status == 3){
                if ($endDateByCurrent > $endDateByPlan){
                    $delayDate = (int)(( ($endDateByPlan - $endDateByCurrent  )/86400));

                    if ($delayDate < 0){
                        $totalTaskExpired += $delayDate;
                    }
                }else{
                    $totalTaskExpired += 0;
                }
            }else{
                if ($endDateByPlan > strtotime(date('Y-m-d'))){
                    $delayDate = (int)(( ($endDateByPlan - strtotime(date('Y-m-d'))   )/86400));

                    if ($delayDate < 0){
                        $totalTaskExpired += $delayDate;
                    }
                }else{
                    $totalTaskExpired += 0;
                }
            }
        }
        $taskListWithPagination = TaskWork::query()->with(['assigner'])->where(['topic_id'=>$this->topicId])->orderBy('id', 'desc')->get();
        foreach ($taskListWithPagination as $task){
            $endDateByPlan = strtotime(date('Y-m-d', strtotime($task->end_date)));
            $endDateByCurrent = strtotime(date('Y-m-d', strtotime($task->real_end_date))) ;
            if ($task->status == 3){
                if ($endDateByCurrent > $endDateByPlan){
                    $delayDate = (int)(( ($endDateByPlan - $endDateByCurrent )/86400));

                    if ($delayDate < 0){
                        $task->delayDate  = $delayDate;
                    }
                }else{
                    $totalTaskExpired += 0;
                }
            }else{
                if ($endDateByPlan > strtotime(date('Y-m-d'))){
                    $delayDate = (int)(( ($endDateByPlan - strtotime(date('Y-m-d'))  )/86400));

                    if ($delayDate < 0){
                        $task->delayDate  = $delayDate;
                    }
                }else{
                    $totalTaskExpired += 0;
                }
            }
        }
        $query = ResearchProject::query()->with(['topic.task.topicActualFee','researchCategory']);
        // if ($this->searchTerm != null){
        //     $query->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8($this->searchTerm)) . "%'")
        //         ->orWhere('project_code', 'LIKE', '%' . $this->searchTerm . '%');
        // }
        if ($this->projectStatusId != null){
            $query->where(['status'=>$this->projectStatusId]);
        }
        if ($this->topicType != null){
            $query->orWhereHas('topic', function ($q)  {
                $q->where(['level_id'=>$this->topicType]);
            });
        }
        if ($this->field != null){
            $query->orWhereHas('topic', function ($q)  {
                $q->where(['research_category_id'=>$this->field]);
            });
        }
        if (!empty($this->start_date_filter)) {
            $query->where('start_date', '>=', Carbon::parse($this->start_date_filter)->startOfDay());
        }
        if (!empty($this->end_date_filter)) {
            $query->where('end_date', '<', Carbon::parse($this->end_date_filter)->startOfDay()->addDays(1));
        }
        if (!empty($this->implementing_agencies)) {
            $query->where('department_id', $this->implementing_agencies);
        }
        $data = $query->orderBy('id', 'desc')->get();

        foreach ($data as $key => $researchProject) {
            $deadlineDate = (strtotime(date('Y-m-d', strtotime($researchProject->end_date)) ));
            $currentDate = strtotime(date('Y-m-d'));
            $researchProject->different_date = (int)(($deadlineDate - $currentDate)/86400);

            if (count($researchProject->topic->task ?? [] ) > 0){
                $researchProject->countTaskExpired = 0 ;
                $researchProject->countTaskExpiredByFee = 0 ;
                foreach ($researchProject->topic->task as $task){
                    if ($task->topicActualFee->disbursement_date ?? null && (int)((strtotime(date('Y-m-d', strtotime($task->topicActualFee->disbursement_date))) - strtotime(date('Y-m-d')))/86400) < 0){
                            $researchProject->countTaskExpiredByFee += 1 ;
                    }
                    if ($task->real_end_date && (int)((strtotime(date('Y-m-d', strtotime($task->real_end_date))) - strtotime(date('Y-m-d')))/86400) < 0){
                           $researchProject->countTaskExpired += 1 ;
                    }
                }
            }
        }

        $tmp = $data->map(function ($item, $index) {
            if ($item->status == 0) {
                $status = 'Chưa thực hiện';
            } else if ($item->status == 1){
                $status = 'Đang thực hiện';
            } else if ($item->status == 2){
                $status = 'Đã hoàn thành';
            } else if ($item->status == 3){
                $status = 'Hủy bỏ';
            }
            $total = TaskWork::query()->with(['topic.projectResearch', 'reviewer', 'follower.memberInfo', 'assigner']);
            $total->whereHas('topic.projectResearch', function ($query) use ($item) {
                $query->where(['contract_code'=>$item->contract_code]);
            });
            $totalTask = $total->get()->count();
            if ($totalTask > 0) {
                $progress = ($total->where('status', 3)->get()->count() / $totalTask) * 100;
            } else {
                $progress = 0;
            }
            if ($this->type_report == 1) {
                return [
                    'index' => $index + 1 . '',
                    'code' => $item->topic->projectResearch->project_code ?? null,
                    'department' => $item->department->name ?? null,
                    'name' => $item->name,
                    'group' => $item->topic->level->v_value ?? null,
                    'field' => $item->researchCategory->name ?? null,
                    'leader' => $item->topic->leader->fullname ?? null,
                    'start_date' => reFormatDate($item->start_date ?? ''),
                    'end_date' => reFormatDate($item->end_date ?? ''),
                    'status' => $status,
                    'work_delay' => $item->countTaskExpiredByFee . '' ?? '0',
                    'work_delay_for_disbursement' => $item->countTaskExpired . '' ?? '0',
                    'progress' => $progress . '%',
                    'different_day' => $item->different_date . '' ?? '0',
                ];
            } else {
                $grant_budget = 0;
                $budget_used = 0;
                $topicFee = $researchProject->topic->topicFee ?? null;
                if (!empty($topicFee) && count($topicFee) > 0) {
                    foreach($topicFee as $item) {
                        if ($item->status == 8) {
                            foreach($item->topicFeeDetail as $detail)
                            $grant_budget += $detail->total_capital;
                        }
                    }
                } else {
                    $grant_budget = '0';
                }

                $actualFee = $item->topic->actualFee ?? null;
                if (!empty($actualFee) && count($actualFee) > 0) {
                    foreach($actualFee as $item) {
                        if ($item->status == 1) {
                           $budget_used += $item->approval_costs;
                        }
                    }
                } else {
                    $budget_used = '0';
                }
                return [
                    'index' => $index + 1 . '',
                    'code' => $item->topic->projectResearch->project_code ?? null,
                    'department' => $item->department->name ?? null,
                    'name' => $item->name,
                    'group' => $item->topic->level->v_value ?? null,
                    'field' => $item->researchCategory->name ?? null,
                    'leader' => $item->topic->leader->fullname ?? null,
                    'start_date' => reFormatDate($item->start_date ?? ''),
                    'end_date' => reFormatDate($item->end_date ?? ''),
                    'grant_budget' => numberFormat($grant_budget, '.', '.'),
                    'budget_used' => numberFormat($budget_used, '.', '.'),
                    'budget_remaining' => numberFormat($grant_budget - $budget_used, '.', '.'),
                ];
            }
        });

        return $tmp;
    }
    public function headings():array
    {
        if ($this->type_report == 1) {
            $headings = [
                'STT',
                'MÃ DỰ ÁN',
                'ĐƠN VỊ THỰC HIỆN',
                'TÊN ĐỀ TÀI',
                'NHÓM ĐỀ TÀI',
                'LĨNH VỰC',
                'CHỦ NHIỆM',
                'NGÀY BẮT ĐẦU',
                'NGÀY KẾT THÚC',
                'TRẠNG THÁI CÔNG VIỆC',
                'CÔNG VIỆC QUÁ HẠN',
                'GIẢI NGÂN QUÁ HẠN',
                'TIẾN ĐỘ',
                'SỐ NGÀY LỆCH',
            ];
        } else {
            $headings = [
                'STT',
                'MÃ DỰ ÁN',
                'ĐƠN VỊ THỰC HIỆN',
                'TÊN ĐỀ TÀI',
                'NHÓM ĐỀ TÀI',
                'LĨNH VỰC',
                'CHỦ NHIỆM',
                'NGÀY BẮT ĐẦU',
                'NGÀY KẾT THÚC',
                'NGÂN SÁCH CẤP',
                'NGÂN SÁCH ĐÃ DÙNG',
                'NGÂN SÁCH CÒN LẠI',
            ];
        }
        return $headings;
    }

    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];

            $active_sheet = $event->sheet->getDelegate();

            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            //$tmp = $event->sheet->getRowDimensions();
            //$event->sheet->getDefaultRowDimension()->setRowHeight(25);
            $highestRow = $active_sheet->getHighestRow();
            $arrayAlphabet = [
                'A','B','C','D','E','F','G','H','I','J','K','L','M','N'
            ];
            $borderStyle = [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [
                       'rgb' => '808080'
                    ]
                ]
            ];

            $event->sheet->mergeCells('A1:F1');
            if ($this->type_report == 1) {
                $event->sheet->setCellValue('A1','BÁO CÁO TÌNH HÌNH THỰC HIỆN DỰ ÁN');
            } else {
                $event->sheet->setCellValue('A1','BÁO CÁO CHI PHÍ THỰC HIỆN DỰ ÁN');
            }

            $event->sheet->getStyle('A1:F1')->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );

            $event->sheet->getStyle('A1:N1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A2:N2';

            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle('A2:N' . $highestRow)->getBorders()->applyFromArray($borderStyle);
            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
        },];

    }
    public function title(): string
    {
        if ($this->type_report == 1) {
            $title = 'Báo cáo dự án';
        } else {
            $title = 'Báo cáo chi phí thực hiện dự án';
        }
        return $title;
    }

    public function startCell(): string
    {
        return 'A2';
    }
}
