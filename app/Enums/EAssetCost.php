<?php


namespace App\Enums;


class EAssetCost
{
    const TYPE_NEW = 1;
    const TYPE_REPAIR = 2;

    public static function getListType() {
        return [
            self::TYPE_NEW => __('data_field_name.research_cost.type_new'),
            self::TYPE_REPAIR => __('data_field_name.research_cost.type_repair'),
        ];
    }

}
