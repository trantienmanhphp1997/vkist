<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetClaimForm extends Model
{
    use HasFactory;
    protected $fillable = [
    	'form_id', 'typasset_ide', 'user_info_id', 'quantity'
    ];
    public $timestamps = false;
    protected $table='asset_claim_form';
}
