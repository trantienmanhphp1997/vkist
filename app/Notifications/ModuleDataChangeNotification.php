<?php

namespace App\Notifications;

use App\Enums\ENotificationType;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class ModuleDataChangeNotification extends BaseNotification
{

    const NOTIFIABLE_MODELS = [
        \App\Models\Ideal::class,
        \App\Models\Topic::class,
        \App\Models\ResearchPlan::class,
        \App\Models\TopicFee::class,
        \App\Models\UserInf::class,
        \App\Models\User::class,
        \App\Models\Budget::class,
        \App\Models\WorkPlan::class,
        \App\Models\Contract::class,
        \App\Models\UserInfoLeave::class,
        \App\Models\Asset::class,
    ];

    public User $user;
    public Model $model;
    public string $action;

    public function __construct(User $user, Model $model, string $action)
    {
        $this->type = ENotificationType::MODULE_DATA_CHANGE;
        $this->user = $user;
        $this->model = $model;
        $this->action = $action;
    }

    protected function message($notifiable)
    {
        return [
            'title' => 'Record updated',
            'template_name' => 'notification.user.module_data_change',
            'user' => [
                'id' => $this->user->id,
                'name' => $this->user->name
            ],
            'model' => [
                'id' => $this->model->id,
                'type' => get_class($this->model),
                'name' => $this->model->name,
                'code' => $this->model->code
            ],
            'action' => $this->action
        ];
    }
}
