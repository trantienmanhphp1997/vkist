@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.research_plan.setup_plan')}}</title>
@endsection
@section('homeLeft')
@include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
  @livewire('admin.research.plan.plan-info-list', ['researchPlan' => $researchPlan])
@endsection