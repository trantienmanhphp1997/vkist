@extends('layouts.master')
<title>{{__('data_field_name.import-salary.detail')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.executive.salary.detail', ['idShow' => $id])
@endsection
@section('js')
@endsection
