<?php


namespace App\Enums;


class EBudgetStatus {

    const NEW = 1;
    const WAIT = 2;
    const DONE = 3;
    const DENY = 4;
    const WAIT_CHANGE = 5;
    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            EBudgetStatus::NEW => __('data_field_name.budget.status_new'),
            EBudgetStatus::WAIT => __('data_field_name.budget.status_wait'),
            EBudgetStatus::DONE => __('data_field_name.budget.status_done'),
            EBudgetStatus::DENY => __('data_field_name.budget.status_deny'),
            EBudgetStatus::WAIT_CHANGE => __('data_field_name.budget.status_wait_change'),
        ];
    }
}

