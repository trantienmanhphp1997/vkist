<?php

namespace App\Http\Livewire\Admin\User\GroupUser;

use App\Http\Livewire\Base\BaseLive;
use App\Models\GroupUser;
use Livewire\Component;

class Index extends BaseLive
{
    public $groupId ;
    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render()
    {
        $query = GroupUser::query();
        if ($this->searchTerm != null) {
            $query->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8($this->searchTerm)) . "%'");
        }
        $groupUserList = $query->orderBy('id', 'desc')->paginate($this->perPage);

        return view('livewire.admin.user.group-user.index', compact('groupUserList'));
    }


    public function setGroupUserId($id)
    {
        $this->groupId = $id;
    }

    public function delete()
    {
        GroupUser::findOrFail($this->groupId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('user/group_user.msg.delete-success')]);
    }
}
