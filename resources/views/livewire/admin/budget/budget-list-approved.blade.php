<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('data_field_name.budget.update')}} </a></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.budget.update')}}</h3>
                </div>

            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block">
                                    <input type="text" class="form-control size13"
                                           placeholder="{{__('data_field_name.budget.search')}}"
                                           wire:model.debounce.1000ms="searchTerm" name="search" autocomplete="off">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="table-responsive">
                    <table class="table working-plan">
                            <thead>
                            <tr class="border-radius">
                                <th scope="col">{{__('data_field_name.budget.code')}}</th>
                                <th scope="col">{{__('data_field_name.budget.name')}}</th>
                                <th scope="col">{{__('data_field_name.budget.total')}}</th>
                                <th scope="col">{{__('data_field_name.budget.content')}}</th>
                                <th scope="col">{{__('data_field_name.budget.year')}}</th>
                                <th scope="col">{{__('data_field_name.budget.status')}}</th>
                                <th scope="col">{{__('data_field_name.budget.plan_budget')}}</th>
                                <th scope="col">{{__('data_field_name.budget.department')}}</th>
                                <th scope="col">{{__('data_field_name.budget.research_project')}}</th>
                                <th scope="col">{{__('data_field_name.budget.number_of_updates')}}</th>
                                <th scope="col">{{__('data_field_name.budget.action')}}</th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                            @forelse ($data as $key => $val)
                                <tr>
                                    <td class="text-center">
                                        <a href="{{route('admin.budget.show',$val->id)}}" data-toggle="tooltip" data-placement="top" title="{{__('common.detail')}}">
                                            {!! boldTextSearch($val->code,$searchTerm) !!}
                                        </a>
                                    </td>
                                    <td class="text-center">{!! boldTextSearch($val->name,$searchTerm) !!}</td>
                                    <td class="text-center"> {{numberFormat($val->total_budget)}}</td>
                                    <td class="text-center">{{$val->content}}</td>
                                    <td class="text-center">{{$val->year_created}}</td>
                                    <td class="text-center">
                                        <button
                                            class="btn  mr0 btn-plan bg-blue medium "> {{$budgetStatus[$val->status] ?? ''}}
                                        </button>
                                    </td>

                                    <td class="text-center">
                                        <button type="button" data-toggle="modal" data-target="#fileBudget"
                                                wire:click="listFile({{ $val->id }})" class="btn btn-primary btn-sm par6">
                                            <img src="/images/file3.svg" alt="file"></button>
                                    </td>
                                    <td class="text-center">{{$val->department->name??''}}</td>
                                    <td class="text-center">{{$val->researchProject->name??''}}</td>
                                    <td class="text-center">
                                        <a href="{{route('admin.budget.history-budget.index')}}">
                                        {{$val->version}}
                                        </a>
                                    </td>
                                    <td class ="text-center">
                                        <a title="{{__('common.button.edit')}}" href="{{route('admin.budget.edit',$val->id)}}">
                                            <img src="/images/edit.png" alt="edit">
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12">{{__('common.message.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                   </div>
                    {{$data->links()}}
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="fileBudget" tabindex="-1"
         aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('data_field_name.budget.list_file')}}</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @forelse ($file as $key => $val)
                            <div class="form-group col-md-12" wire:click="download({{$val->id}})">
                                <div class="attached-files">
                                    <img src="/images/File.svg" alt=file>
                                    <div class="content-file" >
                                        <p>{{ $val-> file_name }}</p>
                                        <p class="kb">{{ $val-> size_file }}</p>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>{{__('common.message.empty_search')}}</p>
                        @endforelse
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"
                            data-dismiss="modal">{{__('common.button.back')}}</button>
{{--                    <button type="button" wire:click.prevent="download()" class="btn btn-danger"--}}
{{--                             title="Download"  id="button_down" >Tải xuống</button>--}}
                </div>
            </div>
        </div>
    </div>
</div>


