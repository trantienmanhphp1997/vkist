@extends('layouts.master')
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    @livewire('admin.budget.report-budget')
@endsection