<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_plan', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Ke hoach cong tac');
            $table->bigInteger('project_id')->nullable()->comment('mã dự án');
            $table->string('content', 1000)->nullable()->comment('noi dung cong tac');
            $table->foreignId('task_id')->nullable()->constrained('task')->comment('thuoc nhiem vu');

            $table->string('missions', 255)->nullable()->comment('doan cong tac');
            $table->string('missions_code', 255)->nullable()->comment('ma doan');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->dateTime('real_start_date')->nullable();
            $table->dateTime('real_end_date')->nullable();
            $table->tinyInteger('status')->nullable()->comment('0 Cho tham dinh, 1 duoc phe duyet, 2 Tu choi');
            $table->string('result', 2000)->nullable()->comment('ket qua cong tac');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->unsignedBigInteger('leader_id')->nullable()->comment('id truong doan map voi user ino');
            $table->foreign('leader_id')->references('id')->on('user_info');
            $table->tinyInteger('priority_id')->nullable()->comment('do uu tien map voi master data type = 9');
            $table->tinyInteger('work_type_id')->nullable()->comment('loại hinh cong tac data type = 10');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_plan');
    }
}
