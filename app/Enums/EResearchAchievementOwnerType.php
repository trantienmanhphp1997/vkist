<?php


namespace App\Enums;


class EResearchAchievementOwnerType
{
    const INVENTION = 1;
    const INDUSTRIAL_DESIGNS = 2;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::INVENTION:
                $result = __('research/achievement.owner_type.invention');
                break;
            case self::INDUSTRIAL_DESIGNS:
                $result = __('research/achievement.owner_type.industrial_designs');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
