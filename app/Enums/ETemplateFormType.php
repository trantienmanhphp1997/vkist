<?php

namespace App\Enums;

class ETemplateFormType {

    const BUDGET = 1;
    const GENERAL = 2;
    const ESTIMATE = 3;
    const PLAN = 4;
    const OTHER = 5;

    public static function valueToName($value) {
        $result = "" ;
        switch ($value) {
            case '1':
                $result = __('data_field_name.template_form.budget');
                break;
            case '2':
                $result = __('data_field_name.template_form.general');
                break;
            case '3':
                $result = __('data_field_name.template_form.estimate');
                break;
            case '4':
                $result = __('data_field_name.template_form.plan');
                break;
            case '5':
                $result = __('data_field_name.template_form.other');
                break;
            default:
            $result = $value;
                break;
        }
        return $result;
    }
}
