<?php
 
return [
    "list" => [
        "header" => "Department management"
    ],
    "production" => "Production room",
    "camera" => "Video recording",
    "technical_camera"=>"Technology of the studio",
    "device" => "Device management room",
    "transportation" => "Transportation room",
    "accounting" => "Finance and accounting department",
    "production_general_news" => "General news room",
    "production_general_news_news" => "Newsroom",
    "production_general_news_documentary" => "Thematic room",
    "production_entertainment" => "Recreation room",
    "production_entertainment_music" => "Music room",
    "production_entertainment_film" => "Movie room",
    "production_entertainment_game" => "Game room",
    "production_entertainment_marketing" => "Marketing department",
    "production_e-commerce" => "Website room",
    "production_individual_customer" => "Individual customer room",
    "production_sale_marketing" => "Advertising department",
    "production_enterprise_customer" => "Corporate customer room",
    "production_technical_center" => "Technical center room",
    "production_editor" => "Editor's secretariat",
    "production_business_plan" => "Business planning department"
 
];