<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetRuleMaintenance extends Model
{
    use HasFactory;
    protected $table = 'asset_rule_maintenance';
    protected $guarded = ['*'];
    protected $fillable = ['asset_category_id', 'content_maintenance','frequency','repeat','maintenance_time_json'];

}
