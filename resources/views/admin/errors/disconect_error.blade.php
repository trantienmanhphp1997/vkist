@extends('layouts.master')
@section('title')
    <title>{{__('notification.common.fail.disconect_error')}}</title>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="mx-auto text-center" style="padding: 0px 0px 120px 0px;">
            <img class="img-fluid" src="/images/connection.svg" alt="connection"/>
            <h3 class="text-center" style="font-size: 40px;font-weight:bold">{{__('notification.common.fail.disconect_error')}}</h3>
            <p class="py-16">{{__('notification.common.fail.not_found')}}</p>
          <a href="{{route('home')}}"  class="btn btn-save size14">{{__('common.button.back_homepage')}}</a>
        </div>
    </div>
</div>
@endsection