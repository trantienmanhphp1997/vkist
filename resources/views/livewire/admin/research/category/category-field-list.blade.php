<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <a href="">{{__('research/category-field.breadcrumbs.category-field-management-1')}} \ 
                        </a><span>{{__('research/category-field.breadcrumbs.category-field-management-2')}}</span>
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">
                    {{__('research/category-field.breadcrumbs.category-field-list')}} 
                    @if($data->total() === 0) <br> <span>{{__('research/category-field.title.no-result')}}</span>  
                    @else
                    <br>
                    <span>
                            {{__('research/category-field.title.part-1')}}  {{count($data)}} {{__('research/category-field.title.part-2')}} {{$data->total()}}
                    </span>
                    @endif
            </h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group search-expertise">
                                <div class="search-expertise">
                                    <input type="text" placeholder="{{__('research/category-field.placeholder.search')}}" name="search" class="form-control w-xs-100 size13"
                                           wire:model.debounce.1000ms="searchTerm"
                                           id='input_vn_name' autocomplete="off">
                                    <span><img src="/images/Search.svg" alt="search"/></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            @if($checkCreatePermission)
                            <div class="form-group float-right">
                                <button class="btn btn-viewmore-news mr0" data-toggle="modal" data-target="#createOrEditCategoryFieldModal" 
                                wire:click="resetCategoryInfo">
                                    <img src="/images/plus2.svg" alt="plus"> {{__('research/category-field.title.create')}}
                                </button>
                            </div>
                            @endif
                        </div>
                        <div class="table-responsive">
                            <table class="table table-general"> 
                                <thead>
                                <tr class="border-radius">
                                    <th scope="col" style="width:5%" class=" border-radius-left">{{__('research/category-field.table_column.code')}}</th>
                                    <th scope="col" style="width:20%" >{{__('research/category-field.table_column.name')}}</th>
                                    <th scope="col" style="width:25%" >{{__('research/category-field.table_column.description')}}</th>
                                    <th scope="col" style="width:30%" >{{__('research/category-field.table_column.note')}}</th>
                                    <th scope="col" style="width:10%"  class="border-radius-right text-center">{{__('research/category-field.table_column.action')}}</th>
                                </tr>
                                </thead>
                                <div wire:loading class="loader"></div>
                                <tbody>
                                @forelse($data as $row)
                                    <tr>
                                        <td>
                                            <a href='#' class='tag_a' wire:click="showId({{$row->id}})" data-target="#createOrEditCategoryFieldModal" data-toggle="modal" >
                                                {{$row->id}}
                                            </a>
                                        </td>
                                        <td>{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                        <td>{!! boldTextSearch($row->description,$searchTerm) !!}</td>
                                        <td>{{$row->note}}</td>
                                        <td class="text-center">
                                            @if($checkEditPermission)
                                            <button type="button" data-toggle="modal" data-target="#createOrEditCategoryFieldModal" title="Sửa"
                                                    wire:click="getCategoryInfoForUpdateModal({{ $row->id }})" class="btn-sm border-0 bg-transparent mr-1">
                                                <img src="/images/edit.png" alt="edit">
                                            </button>
                                            @endif
                                            @if($checkDestroyPermission)
                                            <button type="button" title="{{__('common.button.delete')}}" style="background-color: white; border:none;" class="btn-sm"
                                                    data-toggle="modal" wire:click="deleteId({{$row->id}})" data-target="#exampleDeleteCategory">
                                                <img src="/images/trash.svg" alt="trash">
                                            </button>
                                            @endif
                                        </td>
                                    </tr>
                                    @empty 
                                        <tr class="text-center text-danger"><td colspan="12">{{__('common.message.no_data')}}</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        {{$data->links()}}
                        @include('livewire.common._modalExportFile')
                        <div wire:ignore.self class="modal fade" id="exampleDeleteCategory" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body box-user">
                                        <h4 class="modal-title">{{__('research/category-field.modal.title.delete')}}</h4>
                                            {{__('research/category-field.modal.body.delete-warning')}}
                                    </div>
                                    <div class="group-btn2 text-center pt-24">
                                        <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('research/category-field.modal.footer.back')}}</button>
                                        <button type="button" wire:click.prevent="delete" class="btn btn-save" data-dismiss="modal">{{__('research/category-field.modal.footer.delete')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Edit -->
                        <form wire:submit.prevent="store()">
                            <div wire:ignore.self class="modal fade" id="createOrEditCategoryFieldModal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        @if ($updateMode == true) {{__('research/category-field.modal.title.edit')}}
                                        @elseif($editMode == true) {{__('research/category-field.modal.title.show')}}
                                        @else {{__('research/category-field.modal.title.create')}}
                                        @endif
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true close-btn">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput11">{{__('research/category-field.modal.body.name')}} <span class="text-danger">(*)</label>
                                        <input type="text" class="form-control p-3" id="exampleFormControlInput11"
                                        placeholder="{{__('research/category-field.placeholder.name')}}" wire:model.defer="name" 
                                        {{($editMode) ?"readonly":''}}>
                                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput12">{{__('research/category-field.modal.body.description')}}</label><br>
                                        <textarea type="text" class="p-3" name="description" id="" cols="30" rows="10" wire:model.defer="description"
                                        placeholder="{{__('research/category-field.placeholder.description')}}" style="width:100%;display: flex;flex-direction: row;
                                            align-items: flex-start;position: static;border-radius: 5px;" {{($editMode) ?"readonly":''}}></textarea>
                                        @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group">
                                            <label for="exampleFormControlInput12">{{__('research/category-field.modal.body.note')}}</label><br>
                                            <textarea type="text" class="p-3" name="note" id="" cols="30" rows="10" wire:model.defer="note"
                                            placeholder="{{__('research/category-field.placeholder.note')}}" style="width:100%;display: flex;flex-direction: row;
                                                align-items: flex-start;position: static;border-radius: 5px;" {{($editMode) ?"readonly":''}}></textarea>
                                            @error('note') <span class="text-danger error">{{ $message }}</span>@enderror
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="close-modal-create-or-edit-category-field" class="btn btn-secondary close-btn"
                                    data-dismiss="modal">{{__('research/category-field.modal.footer.close')}}</button>
                                    @if(!$editMode)
                                    <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">{{__('research/category-field.modal.footer.save')}}</button>
                                    @endif
                                </div>
                                </div>
                            </div>
                            </div>
                        </form>
                        <!-- End Modal Edit -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function() {
    window.livewire.on('close-modal-create-or-edit-category-field', () => {
        document.getElementById('close-modal-create-or-edit-category-field').click()
    });
    $('.tag_a').click(function(e){
        e.preventDefault()
    })
  })
  </script>


