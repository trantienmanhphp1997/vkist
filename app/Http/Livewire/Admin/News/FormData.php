<?php

namespace App\Http\Livewire\Admin\News;

use App\Component\SizeFile;
use App\Http\Livewire\Base\BaseLive;
use App\Models\CategoryNews;
use App\Models\File;
use App\Models\News;
use App\Service\Community;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class FormData extends BaseLive
{

    use WithFileUploads;

    public $news;
    public $categories;
    public $image;
    public $editorFile;

    protected function rules()
    {
        return [
            'news.name' => 'required|max:255|unique:news,name' . (($this->news->exists) ? ",{$this->news->id}" : ""),
            'news.description' => 'required',
            'news.category_news_id' => 'required',
            'news.is_trending' => 'nullable',
            'news.content' => 'nullable',
            'image' => ($this->news->exists ? 'nullable' :  'required') . '|image|mimes:jpeg,png,jpg,gif,svg|max:200000,dimensions:min_width=700,min_height=500,max_width=1200,max_height=630',
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'news.name' => __('news/newsManager.menu_name.form-data.name'),
            'news.description' => __('news/newsManager.menu_name.form-data.description'),
            'news.category_news_id' => __('news/newsManager.menu_name.form-data.category'),
            'image' => __('news/newsManager.menu_name.form-data.image')
        ];
    }

    public function mount($newsId = 0)
    {
        $this->categories = CategoryNews::all()->pluck('name', 'id')->all();
        $this->news = News::query()->findOrNew($newsId);
        $this->news->is_trending = $this->news->is_trending == 1;
    }

    public function render()
    {
        return view('livewire.admin.news.form-data');
    }

    public function save($action = 'draft')
    {
        $this->validate();
        $message = $this->news->exists ? __('notification.common.success.update') : __('notification.common.success.add');
        $routeName = '';

        $status = 0;
        if ($action == 'draft') {
            $status = News::DRAFT_NEWS;
            $routeName = 'admin.news.posts.draft';
        } elseif ($action == 'publish') {
            $status = News::NEWS_POSTED;
            $routeName = 'admin.news.posts.index';
        } elseif ($action == 'wait_approval') {
            $status = News::NEWS_NEED_APPROVAL;
            $routeName = 'admin.news.posts.need_approval';
        } else {
            $routeName = 'admin.news.posts.index';
        }

        $imagePath = $this->uploadImage();

        $this->news->admin_id = auth()->id();
        $this->news->slug = Community::slugify($this->news->name) . '-' . rand(0, 100);
        $this->news->status = $status;
        $this->news->is_trending = ($this->news->is_trending) ? 1 : 0;
        if (!empty($imagePath)) {
            $this->news->image_path = $imagePath;
        }

        $this->news->save();

        File::query()->where('model_id', null)->where(['model_name' => News::class])->where('admin_id', auth()->id())->update([
            'model_id' => $this->news->id
        ]);

        session()->flash('success', $message);
        $this->redirectRoute($routeName);
    }

    public function uploadImage()
    {
        $file = $this->image;
        if (empty($file)) return '';
        $size = new SizeFile();
        $size = $size->sizeFile($file->getSize());

        $file->getClientOriginalName();
        $file_name_hash = Str::random(20) . '.' . $file->getClientOriginalExtension();
        return $this->image->storeAs('/uploads/news/files/' . auth()->id(), $file_name_hash, 'local');
    }

    public function delete()
    {
        $status = $this->news->status;
        $routeName = '';
        if ($status == News::DRAFT_NEWS) {
            $routeName = 'admin.news.posts.draft';
        } elseif ($status == News::NEWS_POSTED) {
            $routeName = 'admin.news.posts.index';
        } elseif ($status == News::NEWS_NEED_APPROVAL) {
            $routeName = 'admin.news.posts.need_approval';
        } else {
            $routeName = 'admin.news.posts.index';
        }

        $this->news->delete();

        session()->flash('success', __('notification.common.success.delete'));
        $this->redirectRoute($routeName);
    }

    public function approve()
    {
        if (empty($this->news)) return;
        $this->news->update([
            'status' => News::APPROVED_NEWS
        ]);

        session()->flash('success', __("news/newsManager.menu_name.success-approved-success-msg"));
        return redirect(route('admin.news.posts.approved'));
    }

    public function saveEditorFile(string $uploadUrl, string $eventName)
    {
        if ($this->editorFile->getFilename() == $uploadUrl) {
            $this->editorFile->getClientOriginalName();
            $file_name_hash = Str::random(20) . '.' . $this->editorFile->getClientOriginalExtension();
            $this->editorFile->storeAs('/uploads/news/files/' . auth()->id(), $file_name_hash, 'local');
            $url = Storage::url('uploads/news/files/' . auth()->id() . '/' .$file_name_hash);
            $this->dispatchBrowserEvent($eventName, [
                'url' => $url,
                'href' => $url
            ]);
            return;
        }
    }
}
