<div class="col-md-2 col-xl-3 sidebar-left">
    <ul class="nav flex-column memu-main-left">
        <li class="nav-item parent-menu">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span>{{__('menu_management.menu_name.research')}}
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.research.ideal.index', 'admin.research.topic.index', 'admin.research.plan.research_plan.index'])) active @endif ">
                @if(checkPermission('admin.research.ideal.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research.ideal.index'])) active @endif" href="{{route('admin.research.ideal.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research_ideal')}}">
                        <span class="circle"></span>{{__('menu_management.menu_name.research_ideal')}}
                    </a>
                </li>
                @endif
                @if(checkPermission('admin.research.topic.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research.topic.index'])) active @endif" href="{{route('admin.research.topic.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.topic_research')}}">
                        <span class="circle"></span>{{__('menu_management.menu_name.topic_research')}}
                    </a>
                </li>
                @endif
                @if(checkPermission('admin.research.plan.research_plan.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research.plan.research_plan.index'])) active @endif" href="{{route('admin.research.plan.research_plan.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research')}}">
                        <span class="circle"></span>{{__('menu_management.menu_name.research')}}
                    </a>
                </li>
                @endif
            </ul>
        </li>
        <!-- Quản lý chi phí nghiên cứu -->
        <li class="nav-item parent-menu">
            <a class="nav-link" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research_expense')}}">
                <span><img src="/images/calculator1.svg" alt="calculator" height="24"></span> {{__('menu_management.menu_name.research_expense')}}
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.reality_expense.index', 'admin.research.topic-fee.index'])) active @endif">
                @if(checkPermission('admin.research.topic-fee.index'))
                <li class="child-menu">
                    <a class="nav-link @if(Route::currentRouteName() == 'admin.research.topic-fee.index') active @endif" href="{{route('admin.research.topic-fee.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.estimation')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.estimation')}}
                    </a>
                </li>
                @endif
                @if(checkPermission('admin.reality_expense.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.reality_expense.index') active @endif" href="{{route('admin.reality_expense.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.expense')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.expense')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
        <!-- Quản lý dự án nghiên cứu -->
        <li class="nav-item parent-menu">
            <a class="nav-link" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research_project')}}">
                <span><img src="/images/Flag1.svg" alt="flag" height="24"></span> {{__('menu_management.menu_name.research_project')}}
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="nav-item dropdown-menu-left  @if(strpos(Route::currentRouteName(), 'admin.research-project') !== false) active @endif ">
                @if(checkPermission('admin.research-project.information.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research-project.information.index','admin.research-project.information.create'])) active @endif"
                       href="{{route('admin.research-project.information.index')}}"data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.project_infomation')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.project_infomation')}}
                    </a>
                </li>
                @endif
                @if(checkPermission('admin.research-project.task-work.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research-project.task-work.index','admin.research-project.task-work.create','admin.research-project.task-work.edit'])) active @endif"
                       href="{{route('admin.research-project.task-work.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.tasks_of_project')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.tasks_of_project')}}
                    </a>
                </li>
                @endif
                @if(checkPermission('admin.research-project.information.draft.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research-project.information.draft.index'])) active @endif"
                       href="{{route('admin.research-project.information.draft.index')}}"data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.draft-project')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.draft-project')}}
                    </a>
                </li>
                @endif
            </ul>
        </li>
        <!-- Quản lý thành quả nghiên cứu -->
        <li class="nav-item parent-menu">
            <a class="nav-link" data-toggle="tooltip" data-placement="right" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research_results')}}">
                <span><img src="/images/box1.svg" alt="box" height="24"></span> {{__('menu_management.menu_name.research_results')}}
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="nav-item dropdown-menu-left @if(strpos(Route::currentRouteName(), 'admin.research.achievement') !== false) active @endif" >
                @if(checkPermission('admin.research.achievement.science-article.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research.achievement.science-article.index'])) active @endif"
                    href="{{route('admin.research.achievement.science-article.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.scientific_article')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.scientific_article')}}
                    </a>
                </li>
                @endif
                @if(checkPermission('admin.research.achievement.intellectual-property.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research.achievement.intellectual-property.index'])) active @endif"
                    href="{{route('admin.research.achievement.intellectual-property.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.intellectual_property_rights')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.intellectual_property_rights')}}
                    </a>
                </li>
                @endif
                @if(checkPermission('admin.research.achievement.technology-transfer.index'))
                <li class="child-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research.achievement.technology-transfer.index'])) active @endif"
                    href="{{route('admin.research.achievement.technology-transfer.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.technology_transfer')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.technology_transfer')}}
                    </a>
                </li>
                @endif
            </ul>
        </li>
        <!-- Quản lý thống kê nghiên cứu -->
        @if(checkPermission('admin.research.statistic.index'))
		<li class="nav-item dropdown-menu-left" style="border:0;display:block;padding:0">
		<a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.research.statistic.index'])) active @endif" href="{{route('admin.research.statistic.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research_reporting')}}">
				<span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span>{{__('menu_management.menu_name.research_reporting')}}
			</a>
		</li>
		@endif
        <!-- Xét duyệt và điều phối -->
        <li class="nav-item parent-menu @if(in_array(Route::currentRouteName(), ['admin.approval.index','admin.appraisal.board.index'])) active @endif ">
            <a class="nav-link" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.approval_and_coordination')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{__('menu_management.menu_name.approval_and_coordination')}}
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.approval.index','admin.appraisal.board.index'])) active @endif">
            @if(checkPermission('admin.approval.index'))
                <li class="child-menu">
                    <a class="nav-link @if(Route::currentRouteName() == 'admin.approval.index') active @endif" href="{{route('admin.approval.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.list_of_approvals')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.list_of_approvals')}}
                    </a>
                </li>
            @endif
            @if(checkPermission('admin.appraisal.board.index'))
                <li class="child-menu">
                    <a class="nav-link @if(Route::currentRouteName() == 'admin.appraisal.board.index') active @endif " href="{{route('admin.appraisal.board.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.list_of_appraisal_committees')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.list_of_appraisal_committees')}}
                    </a>
                </li>
            @endif
            </ul>
        </li>
        <!-- Quản lý danh mục -->
        <li class="nav-item parent-menu @if(in_array(Route::currentRouteName(), ['admin.research.category-field.index','admin.research.category-topic.index', 'admin.research.expert.index'])) active @endif ">
            <a class="nav-link" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.category')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{__('menu_management.menu_name.category')}}
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.research.category-field.index','admin.research.category-topic.index', 'admin.research.expert.index'])) active @endif">
            @if(checkPermission('admin.research.category-topic.index'))
                <li class="child-menu">
                    <a class="nav-link @if(Route::currentRouteName() == 'admin.research.category-topic.index') active @endif" href="{{route('admin.research.category-topic.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.topic_type_management')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.topic_type_management')}}
                    </a>
                </li>
            @endif
            @if(checkPermission('admin.research.category-field.index'))
                <li class="child-menu">
                    <a class="nav-link @if(Route::currentRouteName() == 'admin.research.category-field.index') active @endif " href="{{route('admin.research.category-field.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research_field_management')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.research_field_management')}}
                    </a>
                </li>
            @endif
            @if(checkPermission('admin.research.expert.index'))
                <li class="child-menu">
                    <a class="nav-link @if(Route::currentRouteName() == 'admin.research.expert.index') active @endif " href="{{route('admin.research.expert.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.research_expert')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.research_expert')}}
                    </a>
                </li>
            @endif
            </ul>
        </li>
        @if(checkPermission('admin.research.survey.index'))
            <li class="nav-item @if(in_array(Route::currentRouteName(), ['admin.research.survey.index'])) active @endif">
                <a class="nav-link @if(in_array(Route::currentRouteName(), [ 'admin.research.survey.index'])) active @endif" href="{{route('admin.research.survey.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.survey')}}">
                    <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{__('menu_management.menu_name.survey')}}
                </a>
            </li>
        @endif
        @if(checkPermission('admin.research-project.productivity.index'))
            <li class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.research-project.productivity.index'])) active @endif" style="border:0;display:block;padding:0">
                <a class="nav-link @if(in_array(Route::currentRouteName(), [ 'admin.research-project.productivity.index'])) active @endif" href="{{route('admin.research-project.productivity.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.project_performance')}}">
                    <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{__('menu_management.menu_name.project_performance')}}
                </a>
            </li>
        @endif
    </ul>

</div>
