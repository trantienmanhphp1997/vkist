<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAcademic extends Model
{
    use HasFactory;
    protected $table='user_academic';
    protected $fillable = ['user_info_id','standard','major','school','graduated_year','degree','type','name', 'score','rank_id','level_id'];

    public function level(){
        return $this->belongsTo(MasterData::class,'standard');
    }
   
}
