<div wire:ignore.self class="modal fade" id="kpimodal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content " style="    width: 700px;
">
            <div class="modal-body box-user text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
                <h5 class="modal-title" style="    font-size: 28px;">{{__('research/productivity.modal.project')}} : {{$projectName ?? ''}}</h5>
                <p style="font-size: 17px;">{{__('research/productivity.modal.productivity-info')}} </p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="float-left">{{__('research/productivity.topic')}}</label>
                            <select class="form-control form-control-lg" wire:model.defer="topicId"
                                    disabled="disabled">
                                <option value="">-- --</option>
                                @foreach($topicList as $topic)
                                    <option value="{{$topic->id}}" style="height: 40px;">{{$topic->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="float-left">      {{__('research/productivity.table.current-period')}}</label>
                            {!! Form::select('status_id', array_merge(['' => __('common.select-box.choose')], $statusTaskList), $selected_id ?? '' ,[
'class'=>'form-control select2' , 'wire:model.defer'=>'statusId' , 'disabled="disabled"'
]) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="float-left">  {{__('research/taskwork.form-data.start-date')}}</label>
                            @include('layouts.partials.input._inputDate', ['wire_model'=>'startDate','input_id' => 'start_date', 'place_holder'=>'', 'default_date' => now()? date('Y-m-d') : '','disable_input' => true ])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="float-left">    {{__('research/taskwork.form-data.end-date')}}</label>
                            @include('layouts.partials.input._inputDate', ['wire_model'=>'endDate','input_id' => 'end_date', 'place_holder'=>'', 'default_date' => now()? date('Y-m-d') : '', 'disable_input' => true])
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="group-tabs">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" data-toggle="tab" href="#tab1" role="tab"
                                       aria-controls="tab1" aria-selected="false">
                                        <span>  {{__('research/productivity.modal.tab1')}}</span>
                                    </a>
                                    <a class="nav-item nav-link" data-toggle="tab" href="#tab2" role="tab"
                                       aria-controls="tab2" aria-selected="false">
                                        <span> {{__('research/productivity.modal.tab2')}}</span>
                                    </a>
                                </div>
                            </nav>

                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade active show" id="tab1" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <figure class="highcharts-figure w-100">
                                        <div id="pieChartProject"></div>
                                    </figure>
                                </div>

                                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="nav-home-tab">
                                    @include('admin.researchProject.productivity.productivityInfo')
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="group-btn2 text-center pt-24">
                <button type="button" style="width: 100%;" class="btn btn-cancel" data-dismiss="modal">{{__('research/productivity.modal.close')}}
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('getPieChart', event => {
        Highcharts.chart('pieChartProject', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            colors: [
                '#377DFF',
                '#6AD8A7',
                '#FFAB00',
            ],
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            title: '',
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                    },
                    showInLegend: true,
                    size: '75%'
                }
            },
            legend: {
                symbolHeight: 20,
                symbolWidth: 20,
                symbolRadius: 4,
                itemMarginTop: 20,
                labelFormatter: function () {
                    return '<div style="text-align: left; width:130px; font-family: Arial">' + this.name + '</div><div style="width:40px; text-align:right;font-family: Arial">&nbsp' + this.y + '%</div>';
                },
                align: 'right',
                verticalAlign: 'middle',
            },
            series: [{
                name: 'Dự án',
                colorByPoint: true,
                data: [{
                    name: event.detail.inProcesstitle,
                    y: event.detail.inProcess,
                }, {
                    name: event.detail.donetitle,
                    y:  event.detail.done,
                }, {
                    name: event.detail.undonetitle,
                    y: event.detail.undone
                }]
            }]
        });
    })


</script>
</div>
