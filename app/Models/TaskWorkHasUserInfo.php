<?php

namespace App\Models;

class TaskWorkHasUserInfo extends Participant
{
    public $timestamps = false;
    protected $table = "task_work_has_user_info";
    protected $fillable = [
        'task_work_detail_id',
        'user_info_id'
    ];

    protected $mapRelations = [
        'staff' => 'memberInfo',
        'work' => 'taskWorkDetail'
    ];

    public function memberInfo()
    {
        return $this->hasOne(UserInf::class, 'id', 'user_info_id');
    }
    public function taskWorkDetail()
    {
        return $this->belongsTo(TaskWork::class, 'task_work_detail_id');
    }
}
