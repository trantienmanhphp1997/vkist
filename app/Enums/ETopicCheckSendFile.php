<?php


namespace App\Enums;


class ETopicCheckSendFile {
    const CHECK_NOT_SUBMIT = 0;
    const CHECK_SUBMIT = 1;

    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            self::CHECK_NOT_SUBMIT => __('data_field_name.topic.check_not_submit'),
            self::CHECK_SUBMIT => __('data_field_name.topic.check_submit'),
        ];
    }
}

