@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.budget.edit') }}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    <!--start news -->
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a
                        href="{{ route('admin.budget.index') }}">{{ __('data_field_name.budget.general') }}</a> \
                    <span>{{ __('data_field_name.budget.edit') }}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            {!! Form::model($data, ['method' => 'POST', 'class' => 'form-horizontal formUpdateBudget', 'autocomplete' => 'off', 'route' => ['admin.budget.update-budget', $data->id]]) !!}
            {{ method_field('POST') }}
            @csrf
            <div class="col-md-12">
                <div class="detail-task information box-idea">
                    <h4 class="border-bottom-header">{{ __('data_field_name.budget.edit') }}</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{ __('data_field_name.budget.pick_year') }}<span
                                                class="text-danger">*</span></label>
                                        <input type="number" id="datePicker" class="form-control " name="year_created"
                                            value="{{ old('year_created') ?? $data->year_created }}">
                                        @error('year_created') <span
                                            class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{ __('data_field_name.budget.estimate') }}<span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" name="estimated_code"
                                            value="{{ old('estimated_code') ?? $data->estimated_code }}">
                                        @error('estimated_code') <span
                                            class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{ __('data_field_name.budget.code') }}<span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" name="code"
                                            value="{{ old('code') ?? $data->code }}">
                                        @error('code') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{ __('data_field_name.budget.name') }}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="" name="name"
                                    value="{{ old('name') ?? $data->name }}">
                                @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{ __('data_field_name.budget.total') }}<span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control format_number" placeholder=""
                                            name="total_budget"
                                            value="{{ old('total_budget') ?? numberFormat($data->total_budget) }}">
                                        @error('total_budget') <span
                                            class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{ __('data_field_name.budget.note') }}</label>
                                <textarea class="form-control" rows="4"
                                    name="note">{{ old('note') ?? $data->note }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{ __('data_field_name.budget.content') }}</label>
                                <textarea class="form-control" rows="4"
                                    name="content">{{ old('content') ?? $data->content }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{ __('data_field_name.budget.research_project') }}</label>
                                        {!! Form::select('research_project_id', $researchProject, $data->research_project_id, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{ __('data_field_name.budget.department') }}</label>
                                        {!! Form::select('department_id', $department, $data->department_id, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 d-none">
                            <div class="form-group">
                                <label>{{ __('data_field_name.budget.note') }}</label>
                                <input class="form-control" id="input-draft" name="draft" value="0" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="title-plan"> {{ __('data_field_name.budget.plan_spending') }}</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            @if (Request::old('plan_spending') == 'allocated')
                                                <input type="radio" id='allocated' name="plan_spending" checked
                                                    value="allocated">
                                                <label>{{ __('data_field_name.budget.allocated') }}</label><br>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="radio" id='unallocated' name="plan_spending" value="unallocated">
                                            <label>{{ __('data_field_name.budget.unallocated') }}</label><br>
                                        @elseif(Request::old('plan_spending') == 'unallocated')
                                            <input type="radio" id='allocated' name="plan_spending" value="allocated">
                                            <label>{{ __('data_field_name.budget.allocated') }}</label><br>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="radio" id='unallocated' name="plan_spending" checked
                                                value="unallocated">
                                            <label>{{ __('data_field_name.budget.unallocated') }}</label><br>
                                        @else
                                            <input type="radio" id='allocated' name="plan_spending"
                                                {{ $data->allocated ? 'checked' : '' }} value="allocated">
                                            <label>{{ __('data_field_name.budget.allocated') }}</label><br>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="radio" id='unallocated' name="plan_spending"
                                                {{ $data->allocated ? '' : 'checked' }} value="unallocated">
                                            <label>{{ __('data_field_name.budget.unallocated') }}</label><br>
                                            @endif
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-calendar col-md-12" id='budget_month'>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month1') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder="" name="money_plan[]"
                                        value="{{ old('money_plan.0') ?? ($data->hasPlan[0]->money_plan ?? '') }}">
                                    @error('money_plan.0') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month2') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder="" name="money_plan[]"
                                        value="{{ old('money_plan.1') ?? ($data->hasPlan[1]->money_plan ?? '') }}">
                                    @error('money_plan.1') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month3') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder="" name="money_plan[]"
                                        value="{{ old('money_plan.2') ?? ($data->hasPlan[2]->money_plan ?? '') }}">
                                    @error('money_plan.2') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month4') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder="" name="money_plan[]"
                                        value="{{ old('money_plan.3') ?? ($data->hasPlan[3]->money_plan ?? '') }}">
                                    @error('money_plan.3') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month5') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder="" name="money_plan[]"
                                        value="{{ old('money_plan.4') ?? ($data->hasPlan[4]->money_plan ?? '') }}">
                                    @error('money_plan.4') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month6') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder="" name="money_plan[]"
                                        value="{{ old('money_plan.5') ?? ($data->hasPlan[5]->money_plan ?? '') }}">
                                    @error('money_plan.5') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month7') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder="" name="money_plan[]"
                                        value="{{ old('money_plan.6') ?? ($data->hasPlan[6]->money_plan ?? '') }}">
                                    @error('money_plan.6') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month8') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder=""
                                        name="money_plan[]"
                                        value="{{ old('money_plan.7') ?? ($data->hasPlan[7]->money_plan ?? '') }}">
                                    @error('money_plan.7') <span
                                        class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month9') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder=""
                                        name="money_plan[]"
                                        value="{{ old('money_plan.8') ?? ($data->hasPlan[8]->money_plan ?? '') }}">
                                    @error('money_plan.8') <span
                                        class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month10') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder=""
                                        name="money_plan[]"
                                        value="{{ old('money_plan.9') ?? ($data->hasPlan[9]->money_plan ?? '') }}">
                                    @error('money_plan.9') <span
                                        class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month11') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder=""
                                        name="money_plan[]"
                                        value="{{ old('money_plan.10') ?? ($data->hasPlan[10]->money_plan ?? '') }}">
                                    @error('money_plan.10') <span
                                        class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.budget.month12') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control format_number" placeholder=""
                                        name="money_plan[]"
                                        value="{{ old('money_plan.11') ?? ($data->hasPlan[11]->money_plan ?? '') }}">
                                    @error('money_plan.11') <span
                                        class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    @livewire('component.files', ['model_name' => $configFile['model_name'], 'type' => $configFile['type'],
                    'folder' => $configFile['folder'], 'model_id' => $data->id,'status'=>-1])
                    <div class="col-md-12">
                        <div class="group-btn2 text-center group-btn ">
                            <a href="{{ route('admin.budget.index') }}"
                                class="btn btn-cancel">{{ __('data_field_name.work_plan.create.btn_cancel') }}</a>
                            <button type="submit" class="btn btn-save">{{ __('common.button.save') }}</button>
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#buttonDraft').click(function() {
                $('#input-draft').val(-1);
                $('.formUpdateBudget').submit();
            });
            // jQuery.noConflict();
            $('#datePicker').kendoDatePicker({
                value: new Date(2021, 10, 10),
                dateInput: true,
                start: 'decade',
                depth: 'decade',
                format: 'yyyy',
                // timeFormat: 'YYYY',
                type: 'number',

                max: new Date(2050, 0, 1),
                min: new Date(2020, 0, 1),

                activeView: 'none',

            });

            // $("#datepicker").mask("99-99-9999");
            // $("#timeStartPicker").mask("99:99:99");
            var datepicker = $("#datepicker").data("kendoDatePicker");
            $(document).on('focus', '#datepicker', function() {
                datepicker.value(new Date());
            });

            $('#allocated').click(function() {
                $('#budget_month').css("display", "block");
            });

            $('#unallocated').click(function() {
                $('#budget_month').css("display", "none");
            });

            window.onload = function() {
                if ($('#allocated').attr('checked') == 'checked') {
                    $('#budget_month').css("display", "block");
                } else
                    $('#budget_month').css("display", "none");
            };
        });
    </script>
    <!--end news -->

@endsection
