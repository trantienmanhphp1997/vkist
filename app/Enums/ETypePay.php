<?php


namespace App\Enums;

class ETypePay
{
    const DIRECT_EXPENSES = 2;
    const INDIRECT_EXPENSES = 1;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::DIRECT_EXPENSES:
                $result = __('data_field_name.reality_expense.direct_expenses');
                break;
            case self::INDIRECT_EXPENSES:
                $result = __('data_field_name.reality_expense.indirect_expenses');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
