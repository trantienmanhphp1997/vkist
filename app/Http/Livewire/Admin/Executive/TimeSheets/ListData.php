<?php

namespace App\Http\Livewire\Admin\Executive\TimeSheets;

use Livewire\Component;
use App\Models\TimeSheetsMonthly;
use App\Models\TimeSheetsDaily;
use App\Models\TimeSheetsUpload;
use App\Models\TimeSheetsFile;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use App\Models\UserInf;
use App\Enums\EWorkingStatus;
use Illuminate\Support\Facades\Config;
use App\Component\FileUpload;
use App\Models\File;
use App\Exports\ErrorFileExport;
use Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Http\Livewire\Base\BaseLive;
class ListData extends BaseLive
{
	use WithFileUploads;

	public $file;
	public $month;
    public $year;
	public $name;
    public $deleteId;

	public $fileErrorMessage = [];
	public $fileSuccessMessage = [];
    public $numItem = 0;
    public $numItemSaved = 0;
    public $showResult = false;
    public $disableSave = false;
    public $disableInput = false;
    public $numItemData = 0;
    public $type;
    public $model_name;
    public $folder;
    public $checkCreatePermission;
    public $checkDestroyPermission;
    
    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render()
    {
        $this->model_name = TimeSheetsFile::class;
        $this->folder = app($this->model_name)->getTable();
        if (!empty($this->file)) {
            $this->validateFile();
        }
        $timeSheetsList = TimeSheetsFile::OrderBy('id', 'desc')->paginate($this->perPage);
        return view('livewire.admin.executive.time-sheets.list-data' , [
        	'data' => $timeSheetsList
        ]);
    }
    public function resetData() {
        $this->file = null;
        $this->month = null;
        $this->name = null;
        $this->fileErrorMessage = [];
        $this->fileSuccessMessage = [];
        $this->showResult = false;
        $this->numItem = 0;
        $this->numItemSaved = 0;
        $this->numItemData = 0;
    }
    public function validateFile() {
        if ($this->file->getMimeType() != 'application/vnd.ms-excel' &&
            $this->file->getMimeType() != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
            $this->file->getMimeType() != 'application/vnd.ms-excel.sheet.macroEnabled.12') {
            array_push($this->fileErrorMessage, [
                'error' => __('server_validation.import-time-sheets.file-excel')
            ]);
            $this->file = null;
            return false;
        }
    }
    public function validateData() {
    	$this->validate([
            'file' => ['required'],
            'name' => ['required', 'max:48', 'regex:/[^.!@#$%^&*(){}\[\]\'\"\/\|]+$/'],
            'month' => ['required'],
        ],[],[
            'file' => __('data_field_name.import-time-sheets.file'),
            'name' => __('data_field_name.import-time-sheets.name'),
            'month' => __('executive/import-time-sheets.table_column.duration'),
        ]);
    }
    public function saveData() {
    	$this->validateData();
        $this->fileErrorMessage = [];
    	$this->importTimeSheets($this->file);
    }
    public function storeFile($id_model) {
        $file = new FileUpload();
        $dataUpload= $file->uploadFile($this->file, $this->folder, $this->model_name);
        $file_upload = new File();
        $file_upload->url = $dataUpload['url'];
        $file_upload->size_file = $dataUpload['size_file'];
        $file_upload->file_name = $dataUpload['file_name'];
        $file_upload->model_name = $this->model_name;
        $file_upload->model_id = $id_model;

        $file_upload->type = $this->type;
        $file_upload->save();
    }
    public function importTimeSheets($file) {
        return DB::transaction(function() use ($file) {
            $this->disableSave = true;

            if ($file->getMimeType() == 'application/vnd.ms-excel') {
                $excelReader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            } else {
                $excelReader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }
            $spreadsheet = $excelReader->load($file->getRealPath());
            $worksheet = $spreadsheet->getSheet(0);
            $rowIterator = $worksheet->getRowIterator();
            if (count($worksheet->getMergeCells()) <= 38 || count($worksheet->getMergeCells()) >= 42) {
                array_push($this->fileErrorMessage, [
                    'error' => __('server_validation.import-time-sheets.file-excel2')
                ]);
                $this->disableSave = false;
            	return false;
            }

            $isRowEmpty = function(\PhpOffice\PhpSpreadsheet\Worksheet\Row $row) {
                $is_row_empty = true;
                foreach ($row->getCellIterator() as $cell) {
                    if ($cell->getValue() !== null && $cell->getValue() !== "") {
                        $is_row_empty = false;
                        break;
                    }
                }
                return $is_row_empty;
            };


            $existsTimeSheets = TimeSheetsMonthly::where('year_working',  $this->year)->where('month_working', $this->month)->exists();

            $timeSheetsFile = new TimeSheetsFile();
            $timeSheetsFile->month_working = $this->month;
            $timeSheetsFile->year_working = $this->year;
            $timeSheetsFile->name = $this->name;
            $timeSheetsFile->save();

            $this->storeFile($timeSheetsFile->id);

            while ($rowIterator->valid()) {
                $this->numItem ++;
                if (!in_array($rowIterator->current()->getRowIndex(), [1, 2, 3, 4, 5])) {
                    if ($isRowEmpty($rowIterator->current())) {
                        break;
                    }
                    $this->numItemData ++;
                    $data = [];
                    $cellIterator = $rowIterator->current()->getCellIterator();
                    
                    for($i = 0; $i < count($worksheet->getMergeCells()); $i++) {
                        $cellValue = trim((string)$cellIterator->current()->getFormattedValue());
                        array_push($data, $cellValue);
                        if ($cellIterator->getCurrentColumnIndex() == 1) {
                            if ($cellValue == null && $cellValue == "") {
                                $data = [];
                                break;
                            }
                            $userInf = UserInf::where('code', $cellValue)->first();
                            if (empty($userInf)) {
                                array_push($this->fileErrorMessage, [
                                    'error' => 'Nhân viên ' . $cellValue . ' không tồn tại. Dòng số ' . $this->numItem
                                ]);
                                $data = [];
                            }
                        }
                        if ($cellIterator->getCurrentColumnIndex() == 2 && empty($cellValue)) {
                                array_push($this->fileErrorMessage, [
                                    'error' => 'Nhân viên ' . $cellValue . ' không tồn tại. Dòng số ' . $this->numItem
                                ]);
                                $data = [];
                        }
                        $cellIterator->next();

                    }
                    
                    if (!empty(count($data)) && count($data) == count($worksheet->getMergeCells())) {
                        //monthly
                        
                        if ($existsTimeSheets) {
                            $monthly = TimeSheetsMonthly::where('user_info_id', $userInf->id)->first();
                            if (empty($monthly)) {
                                $monthly = new TimeSheetsMonthly();
                            }
                        } else {
                            $monthly = new TimeSheetsMonthly();
                        }
                        $monthly->year_working = $this->year;
                        $monthly->month_working = $this->month;
                        $monthly->user_info_id = $userInf->id;
                        $monthly->total_work_day = $data[33];
                        $monthly->unpaid_day = $data[34];
                        $monthly->holiday = $data[35];
                        $monthly->leave_day = $data[36];
                        $monthly->total_leave_day = $data[37];
                        $monthly->monthly_leave_day = $data[38];
                        $monthly->yearly_leave_day = $data[39];
                        $monthly->admin_id = auth()->id();
                        $monthly->timesheets_file_id = $timeSheetsFile->id;
                        $monthly->save();

                        foreach ($data as $index => $item) {
                            //upload
                            if ($index >= 2 && $index <= 32) { // ngày làm việc

                                //daily
                                if ($existsTimeSheets) {
                                    $daily = TimeSheetsDaily::where('timesheets_monthly_id', $monthly->id)
                                    ->where('working_day', $index - 1)
                                    ->first();
                                    if (empty($daily)) {
                                        $daily = new TimeSheetsDaily();
                                    }
                                } else {
                                    $daily = new TimeSheetsDaily();
                                }
                                $daily->working_day = $index - 1;
                                $daily->working_status = EWorkingStatus::nameToValue($item);
                                $daily->timesheets_monthly_id = $monthly->id;
                                $daily->save();

                                //upload

                            }

                        }
                        $this->numItemSaved ++;
                    }
                }
                $rowIterator->next();
            }
            $this->fileSuccessMessage = __('server_validation.import-time-sheets.msg.success', ['value' => $this->numItemSaved]);
            $this->file = null;
            $this->showResult = true;
            $this->disableSave = false;
            $this->emit('closeModalImport');
        });
    }
    public function deleteFile() {
        $this->file = null;
        $this->fileErrorMessage = [];
    }
    public function delete() {
        return DB::transaction(function() {
            $timeSheetsFile = TimeSheetsFile::where('id', $this->deleteId)->first();
            if (!empty($timeSheetsFile)) {
                $timeSheetsFile->delete();
                $timeSheetsMonthly = TimeSheetsMonthly::where('timesheets_file_id', $this->deleteId)->get();
                foreach ($timeSheetsMonthly as $item) {
                    TimeSheetsDaily::where('timesheets_monthly_id', $item->id)->delete();
                    $item->delete();
                }
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
            } else {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.delete')]);
            }
        });
    }
    public function downloadErrorFile() {
        return Excel::download(new ErrorFileExport($this->fileErrorMessage, 'timeSheet'), 'error-import-timekeeping' . now()->format(Config::get('common.year-month-day')) . '-' . '.xlsx');
    }

    public function downloadFileExport() {
        $spreadsheet = IOFactory::load("template/time_sheet.xlsx");
        $worksheet = $spreadsheet->getActiveSheet();

        $userInf = UserInf::OrderBY('status', 'asc')->OrderBY('code', 'asc')->get();
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        foreach($userInf as $index => $item) {
            $worksheet->getCell('A' . ($index + 6))->setValue($item->code);
            $worksheet->getCell('B' . ($index + 6))->setValue($item->fullname);
            $worksheet->getStyle('A' . ($index + 6) . ':AN' . ($index + 6))->applyFromArray($styleArray);
            $worksheet->getStyle('D' . ($index + 6) . ':E' . ($index + 6))->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('C0C0C0');
            $worksheet->getStyle('K' . ($index + 6) . ':L' . ($index + 6))->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('C0C0C0');
            $worksheet->getStyle('R' . ($index + 6) . ':S' . ($index + 6))->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('C0C0C0');
            $worksheet->getStyle('Y' . ($index + 6) . ':Z' . ($index + 6))->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('C0C0C0');
            $worksheet->getStyle('AF' . ($index + 6) . ':AG' . ($index + 6))->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('C0C0C0');
        }

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $path  = storage_path("app/uploads/time_sheet.xlsx");
        $writer->save($path);
        return response()->download($path);
    }
}
