<?php

namespace App\Http\Controllers\Admin\Asset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class TransferController extends Controller
{
    public function index() {
        return view('admin.asset.transfer-asset.index');
    }

    public function create() {
    	return view('admin.asset.transfer-asset.transfer');
    }

    public function show($id) {
    	return view('admin.asset.transfer-asset.detail', ['id' => $id]);
    }
}
