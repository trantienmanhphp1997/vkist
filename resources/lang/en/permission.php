<?php
return [

    "access" => [
        "header" => "Decentralize access",
        "menus" => [
            "available" => "Catalogue",
            "selected" => "Category with access",
            "add" => "Add permission",
            "remove" => "Remove permissions"
        ]
    ],
    "features" => [
        "header" => "Functional Decentralization",
        "table" => [
            "menus" => "Category",
            "feature_permissions" => "Functional permissions",
            "all" => "All",
            "create" => "Create new",
            "view" => "View",
            "view_any" => "View all",
            "update" => "Update",
            "update_any" => "Update all",
            "delete" => "Delete",
            "delete_any" => "Delete all",
            "restore" => "Restore"
        ]
    ],
    'my-menu'=>[
        "over-option-in-menu" => 'no more than 5 options are allowed',
        "save-option-menu-success" => 'menu setting successful',
        "save-option-menu-fail" => 'menu setting failed',
    ],

];
