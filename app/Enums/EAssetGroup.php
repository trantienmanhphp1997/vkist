<?php


namespace App\Enums;


class EAssetGroup {
    const VISIBLE = 1;
    const INVISIBLE = 2;

    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            EAssetGroup::VISIBLE => __('data_field_name.asset.visible'),
            EAssetGroup::INVISIBLE => __('data_field_name.asset.invisible'),
        ];
    }
}

