<?php

namespace App\Http\Controllers\Admin\Asset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AssetInventoryController extends Controller
{
    public function index(){
        return view('admin.asset.inventory.index');
    }

    public function show($id){
        $inventory_id = $id;
        return view('admin.asset.inventory.list-asset', compact('inventory_id'));
    }
}
