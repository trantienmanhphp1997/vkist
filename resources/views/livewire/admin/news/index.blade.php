<div class="col-md-10 col-xl-11 box-detail box-user box-new-table list-role">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="#">{{__('news/newsManager.menu_name.news')}} </a> \
                <span>{{__('news/newsManager.menu_name.news_page')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('news/newsManager.menu_name.news_page')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group search-expertise">
                                <div class="search-expertise">
                                    <input type="text" placeholder="{{__('asset/provider.placeholder.search')}}" name="search" class="form-control"
                                           wire:model.debounce.1000ms="searchTerm" id='input_vn_name'
                                           autocomplete="off">
                                    <span><img src="/images/Search.svg" alt="search"/></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group float-right">
                                <button title="{{__('common.button.filter')}}" type="button" data-toggle="modal" data-target="#seachBox"
                                        style="border-radius: 11px; border:none;">
                                    <img src="/images/filter3gach.png" alt="" id="search_more_btn">
                                </button>
                                @if($checkCreatePermission)
                                    <button title="{{__('common.button.create')}}" type="button" style="border-radius: 11px; border:none;">
                                        <a href="{{route('admin.news.posts.create')}}">
                                            <img src="/images/filteradd.png" alt="filteradd">
                                        </a>
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                            <tr class="border-radius">
                                <th scope="col" class="text-center"
                                    class="border-radius-left">{{__('news/newsManager.menu_name.news_title_table.stt')}}</th>
                                <th scope="col" class="text-center">{{__('news/newsManager.menu_name.news_title_table.name')}}</th>
                                <th scope="col" class="text-center">{{__('news/newsManager.menu_name.news_title_table.author')}}</th>
                                <th scope="col" class="text-center">{{__('news/newsManager.menu_name.news_title_table.category')}}</th>
                                <th scope="col">{{__('news/newsManager.menu_name.news_title_table.publication_time')}}</th>
                                @if($status == 1)
                                    <th scope="col" class="text-center">{{__('news/newsManager.menu_name.news_title_table.status')}}</th>
                                @endif
                                <th scope="col" class="border-radius-right text-center">{{__('news/newsManager.menu_name.news_title_table.action')}}</th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                            @foreach($data as $key => $row)
                                <tr>
                                    <td class="text-center">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                    <td class="text-center" style="
            width: 14%;">{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                    <td class="text-center">{!! boldTextSearch($row->admin->name??"",$searchTerm) !!}</td>
                                    @if($row->category)
                                        <td class="text-center">{{boldTextSearch($row->category->name,$searchTerm)}}</td>
                                    @else
                                        <td class="text-center">Chưa xác định</td>
                                    @endif
                                    <td class="text-center"> {{reFormatDate($row->created_at,"j/n/Y , H:i A")}}</td>
                                    @if($status == 1)
                                        <td class="text-center">
                                            {!! Form::select(
                                                'category_id',
                                                [0 =>  __('news/newsManager.menu_name.status.off'), 1 => __('news/newsManager.menu_name.status.on')],
                                                $row->published,
                                                [
                                                    'class' => "form-control form-control-lg ",
                                                    'style'=>$row->published == 1 ? 'background: #F4F8FF;color: #377DFF;' : 'background: #FFF6F4;color: #FF5630;' ,
                                                    'wire:change' => 'publishNews(' . $row->id . ', $event.target.value)'
                                                ])
                                            !!}
                                        </td>
                                    @endif
                                    <td class="text-center">
                                        @if($checkEditPermission)
                                            <a title="{{__('common.button.edit')}}" class="btn par6" href="{{route('admin.news.posts.edit', $row->id)}}"><img
                                                    src="/images/pent2.svg" alt=""></a>
                                        @endif
                                        @if($checkDestroyPermission)
                                            <button title="{{__('common.button.delete')}}" type="button" class="btn par6" title="delete"
                                                    wire:click="setNewsId({{$row->id}})" data-toggle="modal"
                                                    data-target="#deleteBox">
                                                <img src="/images/trash.svg" alt="trash">
                                            </button>
                                        @endif
                                        @if(!$row->file->isEmpty())
                                            @if($checkDownloadPermission)
                                                <a class="btn par6" wire:click="getFileList({{$row->id}})"
                                                   data-toggle="modal" data-target="#downloadFile">
                                                    <img width="40px" src="/images/fileIcon.png" alt="file">
                                                </a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center">
                            <span>{{__('data_field_name.system.role.no_result')}}</span>
                        </div>
                    @endif
                    @include('livewire.common._modalDelete')
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="downloadFile" tabindex="-1" aria-labelledby="exampleModal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                @foreach ($files as $key => $file)
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="attached-files" wire:click="download('{{$file->url}}')" style="cursor: pointer">
                                <img src="/images/File.svg" alt="file">
                                <div class="content-file">
                                    <p>{{ $file-> file_name }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="seachBox" tabindex="-1" aria-labelledby="exampleModal"
         aria-hidden="true">
        <div class="modal-dialog" style="    padding: 10px;">
            <div class="modal-content" style="   border-radius: 30px;   padding: 10px 10px;">
                <div class="modal-header" style="border-bottom: 1px solid white;">
                    <h4 class="modal-title" style="margin: 0 auto;">{{__('asset/provider.placeholder.search')}}</h4>
                </div>
                <div class="modal-body container" style="padding: 0px 35px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    {{__('news/newsManager.menu_name.news_title_table.name')}}
                                </label>
                                <input type="text" name="proposer" class="form-control"
                                       wire:model.lazy="nameSearchField"
                                       placeholder=" {{__('news/newsManager.menu_name.news_title_table.name')}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('news/newsManager.menu_name.news_title_table.publication_time')}}</label>
                                <input type="date" class="form-control" wire:model.lazy="timeSearchField">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('news/newsManager.menu_name.news_title_table.category')}}</label>
                                {{ Form::select('category', ['' => __('common.select-box.choose')] + $categoryList->toArray(), null,
                                ['class' => 'form-control form-control-lg select-root-topic', 'wire:model' => 'category_id']) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('news/newsManager.menu_name.news_title_table.author')}}</label>
                                {{ Form::select('user', ['' => __('common.select-box.choose')] + $userList->toArray(), null,
                                ['class' => 'form-control form-control-lg select-root-topic', 'wire:model' => 'user_id']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-bottom: 25px;border-top: 1px solid white;">
                    <div class="row" style="margin: 0 auto;">
                        <div class="group-btn2">
                            <a data-dismiss="modal" wire:click.prevent="closeFillerSearchBox()"
                               style="padding: 12px 77px;"
                               class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                            <button wire:click.prevent="filterAnswers()" data-dismiss="modal" type="button"
                                    style="width: 180px;"
                                    class="btn btn-save">{{__('common.button.filter')}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="deleteBox" tabindex="-1" aria-labelledby="exampleModal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body box-user">
                    <h4 class="modal-title">{{__('data_field_name.common_field.confirm_delete')}}</h4>
                    {{__('notification.member.warning.warning-1')}} <br>
                </div>
                <div class="group-btn2 text-center  pt-12">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}
                    </button>
                    <button type="button" wire:click.prevent="delete()" class="btn btn-save" data-dismiss="modal">
                        {{__('common.button.delete')}}                      </button>
                </div>
            </div>
        </div>
    </div>
</div>



