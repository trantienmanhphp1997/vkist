<?php

namespace App\Http\Livewire\Admin\Asset\AssetDetail;

use App\Enums\EAssetTypeDate;
use App\Models\Asset;
use App\Service\Community;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class TabDetailDistribute extends Component
{
    public $assetDetail;
    public $assetTypeDate;


    public function mount() {
        $this->assetTypeDate = [
            EAssetTypeDate::DAY => __('data_field_name.asset.day'),
            EAssetTypeDate::MONTH => __('data_field_name.asset.month'),
            EAssetTypeDate::YEAR => __('data_field_name.asset.year'),
        ];
    }

    public function render()
    {


        return view('livewire.admin.asset.asset-detail.tab-detail-distribute');
    }
}
