@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.asset.list_asset')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('css')
    <style>
        #file_right{
            float:right!important;
        }
        .attached-files {
            max-width: 399px;
        }
        #file_right .bg-light{
            background: none !important;
        }
        #create_asset_import .file{
            top: 73px !important;
        }
    </style>
@endsection
@section('content')
    <script>
        document.addEventListener('livewire:load', function () {
            $(function () {
                $("#dataTable  tr td:not(:first-child)").click(function () {
                    $('#item_info').modal('show');
                });
                $('#report_lost_btn').on('click', function () {
                    var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                    var count = $checkedBoxes.length;
                    if (count === 1) {
                        window.livewire.emit('emitDeleteFile');
                        $('#report_lost').modal('show');
                    } else {
                        toastr.warning("{{ __('notification.common.fail.select_nothing') }}");
                    }
                });
                $('#report_error_btn').on('click', function () {
                    var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                    var count = $checkedBoxes.length;
                    if (count === 1) {
                        window.livewire.emit('emitDeleteFile');
                        $('#report_error').modal('show');
                    } else {
                        toastr.warning({{ __('notification.common.toast_message.select_nothing') }});
                    }

                });
                $('#report_transfer_btn').on('click', function () {
                    var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                    var count = $checkedBoxes.length;
                    if (count === 1) {
                        $('#report_transfer').modal('show');
                    } else if(count > 1){
                        var checkbox = document.getElementsByClassName('row_id');
                        var checked = [];
                        for (var i = 0; i < checkbox.length; i++) {
                            if (checkbox[i].checked === true) {
                                checked.push(checkbox[i].value);
                            }
                        }
                        window.livewire.emit('getAssetIds',checked);

                        $('#report_transfer_series').modal('show');
                    }
                    else {
                        toastr.warning({{ __('notification.common.toast_message.select_nothing') }});
                    }

                });
                $('#report_recall_btn').on('click', function () {
                    var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                    var count = $checkedBoxes.length;
                    if (count === 1) {
                        $('#report_recall').modal('show');
                    } else if(count > 1){
                        var checkbox = document.getElementsByClassName('row_id');
                        var checked = [];
                        for (var i = 0; i < checkbox.length; i++) {
                            if (checkbox[i].checked === true) {
                                checked.push(checkbox[i].value);
                            }
                        }
                        window.livewire.emit('getAssetIds',checked);
                        $('#report_recall_series').modal('show');
                    }else {
                        toastr.warning("{{ __('notification.common.fail.select_nothing') }}");
                    }
                });
            });
        });
        $(document).ready(function () {
            window.livewire.on('close-lost-asset', () => {
                document.getElementById('close-lost-asset').click();
            });
        });$(document).ready(function () {
            window.livewire.on('close-error-asset', () => {
                document.getElementById('close-error-asset').click();
            });
        });
        $(document).ready(function () {
            window.livewire.on('close-transfer-asset', () => {
                document.getElementById('close-transfer-asset').click();
            });
        });
        $(document).ready(function () {
            window.livewire.on('close-transfer-series', () => {
                document.getElementById('close-transfer-series').click();
            });
        });
        $(document).ready(function () {
            window.livewire.on('close-recall-asset', () => {
                document.getElementById('close-recall-asset').click();
            });
        });
        $(document).ready(function () {
            window.livewire.on('close-recall-series', () => {
                document.getElementById('close-recall-series').click();
            });
        });


    </script>

    @livewire('admin.asset.detail.asset-list')
@endsection
