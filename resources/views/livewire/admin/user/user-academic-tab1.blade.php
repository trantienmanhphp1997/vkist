<div>
    <div class="row">
        <div class="col-md-6">
            <h3 class="title-personal">{{__('data_field_name.user.diploma')}}</h3>
        </div>
        <div class="col-md-6 ">
            @if ($canEdit)
            <button type="button" id="btn-standard" data-toggle="modal" data-target="#createModal1"
                    class="btn btn-adduser float-right "><img src="/images/plususer.svg" alt="plus"></button>
            @endif
        </div>
    </div>
    <table class="table border-bottom">
        <thead>
        <tr class="border-radius">
            <th scope="col" class="border-radius-left">{{__('data_field_name.user.level')}}</th>
            <th scope="col">{{__('data_field_name.user.specialized')}}</th>
            <th scope="col">{{__('data_field_name.user.school')}}</th>
            <th scope="col">{{__('data_field_name.user.graduation_year')}}</th>
            <th scope="col">{{__('data_field_name.user.classification')}}</th>
            <th scope="col" class="border-radius-right">{{__('data_field_name.common_field.action')}}</th>
        </tr>
        </thead>
        <tbody>
        @forelse($dataType1 as $val)
            <tr>
                <td>{{$val->standard_name}}</td>
                <td>{{$val->major}}</td>
                <td>{{$val->school}}</td>
                <td>{{$val->graduated_year?date('Y',strtotime($val->graduated_year)):''}}</td>
                <td><a href="" class="btn-classification">{{$val->degree_name}}</a></td>
                <td>
                    <button type="button" class="btn par6" title="edit" data-toggle="modal" data-target="#updateModal1"
                            wire:click="editId1({{$val->id}})"><img src="/images/pent2.svg" alt=""></button>
                    <button type="button" class="btn par6" title="delete" data-toggle="modal"
                            data-target="#exampleDelete" wire:click="deleteId1({{$val->id}})"><img
                            src="/images/trash.svg" alt="trash"></button>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6">{{__('data_field_name.user.empty_diploma')}}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{$dataType1->links()}}
    <div wire:init="openModal" wire:ignore.self class="modal fade" id="createModal1" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('data_field_name.user.create_diploma')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.level')}} <span class="text-danger">(*)</span></label>
                            <select class="form-control" wire:model.lazy="standard">
                                <option value="">--{{__('data_field_name.user.level')}}--</option>
                                @foreach($standards as $key => $standard)
                                    <option value="{{$key}}">{{$standard}}</option>
                                @endforeach
                            </select>
                            @error('standard') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.specialized')}} <span
                                    class="text-danger">(*)</span></label>
                            <input type="text" class="form-control" wire:model.lazy="major"
                                   placeholder="{{__('data_field_name.user.input_specialized')}}">
                            @error('major') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.school')}} <span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control" wire:model.lazy="school"
                                   placeholder="{{__('data_field_name.user.input_school')}}">
                            @error('school') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.graduation_year')}}<span
                                    class="text-danger">(*)</span></label>

                            <input type="text" class="input-date-kendo form-control mb-0 graduated-year" id="graduated-year" placeholder="dd/mm/yyyy" value="{{ $graduated_year }}">
                            {{-- <input type="date" class="form-control" wire:model.lazy="graduated_year"
                                   placeholder="{{__('data_field_name.user.input_graduation_year')}}"> --}}
                            @error('graduated_year') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.classification')}}</label>
                            <select class="form-control" wire:model.lazy="degree">
                                <option value="">--{{__('data_field_name.user.classification')}}--</option>
                                @foreach($degrees as $key => $degree)
                                    <option value="{{$key}}">{{$degree}}</option>
                                @endforeach
                            </select>
                            @error('degree') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </form>
                </div>
                <div class="text-center">
                    <button type="button" id="close-academic-create1" wire:click.prevent="resetInputFields()"
                            class="btn btn-main bg-4 text-3 size16 px-48 py-14" data-dismiss="modal">Đóng
                    </button>
                    <button type="button" class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn" wire:click.prevent="storeType1()">Lưu
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="updateModal1" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('data_field_name.user.edit_diploma')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.level')}}<span class="text-danger">(*)</span></label>
                            <select class="form-control" wire:model.lazy="standard">
                                <option value="">--{{__('data_field_name.user.level')}}--</option>
                                @foreach($standards as $key => $standard)
                                    <option value="{{$key}}">{{$standard}}</option>
                                @endforeach
                            </select>
                            @error('standard') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.specialized')}} <span
                                    class="text-danger">(*)</span></label>
                            <input type="text" class="form-control" wire:model.lazy="major"
                                   placeholder="{{__('data_field_name.user.input_specialized')}}">
                                @error('major') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.school')}} <span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control" wire:model.lazy="school"
                                   placeholder="{{__('data_field_name.user.input_school')}}">
                                   @error('school') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.graduation_year')}}<span
                                    class="text-danger">(*)</span></label>
                            <input type="text" class="input-date-kendo form-control mb-0 graduated-year" id="graduated-year" placeholder="dd/mm/yyyy" value="{{ $graduated_year }}">
                            @error('graduated_year') <span class="text-danger">{{ $message }}</span>@enderror
                            {{-- <input type="date" class="form-control" wire:model.lazy="graduated_year"
                                   placeholder="{{__('data_field_name.user.input_graduation_year')}}"> --}}
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.user.classification')}}</label>
                            <select class="form-control" wire:model.lazy="degree">
                                <option value="">--{{__('data_field_name.user.classification')}}--</option>
                                @foreach($degrees as $key => $degree)
                                    <option value="{{$key}}">{{$degree}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="text-center">
                    <button type="button" id="close-academic-update1" wire:click.prevent="resetInputFields()"
                            class="btn btn-main bg-4 text-3 size16 px-48 py-14"
                            data-dismiss="modal">{{__('common.button.delete')}}</button>
                    <button type="button" class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn"
                            wire:click.prevent="storeType1()">{{__('common.button.save')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="exampleDelete" tabindex="-1" aria-labelledby="exampleModal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{__('data_field_name.common_field.confirm_delete')}}</h4>
                    {{__('notification.member.warning.warning-1')}}
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">Quay lại</button>
                    <button type="button" wire:click.prevent="delete1()" class="btn btn-save" data-dismiss="modal">
                        Xóa
                    </button>
                </div>
            </div>
        </div>
    </div>
    @livewire('component.files', ['model_name'=>$model_name, 'model_id'=>$model_id, 'type'=>$type, 'folder'=>$folder])

    <script>
        window.addEventListener('render-user-academic-tab1-event', function() {
            initDatePicker('.graduated-year');
        });
    </script>
</div>
