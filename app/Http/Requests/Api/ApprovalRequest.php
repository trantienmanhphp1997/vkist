<?php

namespace App\Http\Requests\Api;

use App\Enums\EAppraisalBoard;
use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class ApprovalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'request_id' => ['required'],
            'status' => ['required'],
            'type' => ['required'],
            'functionName' => ['required', 'max:255'],
            'message' => ['required'],
            'data' => ['required'],
        ];
    }

    public function attributes()
    {
        return [];
    }
}
