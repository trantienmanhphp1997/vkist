<?php

namespace App\Http\Livewire\Admin\ResearchProject\TaskWork;

use App\Component\FileUpload;
use App\Http\Livewire\Base\BaseLive;
use App\Models\File;
use App\Models\Task;
use App\Models\TaskWork;
use App\Models\TaskWorkHasResultPlan;
use App\Models\TaskWorkHasUserInfo;
use App\Models\Topic;
use App\Models\TopicHasUserInfo;
use App\Models\User;
use App\Models\UserInf;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class FormData extends BaseLive
{
    use WithFileUploads;

    public $code;
    public $level_id = 1;
    public $status_id = 0;
    public $topic_id;
    public $task_id;
    public $main_task_id;
    public $work_type_id = 2;
    public $name;
    public $start_date;
    public $end_date;
    public $description;
    public $reviewer_id;
    public $approve_id;
    public $isFollower;
    public $isAssign = true;
    public $isPreview = true;
    public $isApproval = true;
    public $listFollower = [];
    public $file;
    public $mainTask;
    public $topic;
    public $topicStartDate;
    public $topicEndDate;
    public $memberPickUpList = [];
    public $taskID;
    public $isOverFile;
    public $folder = 'tasks';
    public $model_name = TaskWork::class;
    public $admin_id;
    public $user_info_id;
    public $user_info_name;
    public $autoCompleteSearch = false;
    private $contract_code;
    public $real_start_date;
    public $real_end_date;
    public $model_id;
    /** @var array |\Livewire\TemporaryUploadedFile[] */
    public $newFiles = [];
    protected $listeners = [
        'set-start-time' => 'setStartTime',
        'set-end-time' => 'setEndTime',
        'open-auto-searchBox' => 'openAutoSearchBox',
        'close-auto-search' => 'closeAutoComplete',
        'changed-assigner-id-event' => 'selectAssigner',
    ];
    public function selectAssigner($userInfo)
    {
        $this->admin_id = $userInfo['id'] ?? null;
    }
    public function openAutoSearchBox()
    {
        $this->autoCompleteSearch = true;
    }

    public function mount($id = 0, $contractCode = '')
    {
        if (trim($contractCode) != null && $id == 0) {
            $this->contract_code = $contractCode;
            $this->topic_id = Topic::where(['contract_code' => $contractCode])->first()->id ?? '';
        }
        $this->admin_id = Auth::id();
        $this->start_date = Carbon::now()->toDateTimeString();
        $this->end_date = Carbon::now()->toDateTimeString();
        if ($id) {
            $this->model_id = $id;
            $taskWorkDetail = TaskWork::findOrFail($id);
            $this->taskID = $id;
            if ($taskWorkDetail->code) {
                $this->code = $taskWorkDetail->code;
            }

            $this->level_id = $taskWorkDetail->priority_level;
            $this->status_id = $taskWorkDetail->status;
            $this->topic_id = $taskWorkDetail->topic_id;
            $this->task_id = $taskWorkDetail->task_id;
            $this->work_type_id = $taskWorkDetail->work_type;
            $this->name = $taskWorkDetail->name;
            $this->start_date = $taskWorkDetail->start_date;
            $this->real_start_date = $taskWorkDetail->real_start_date;
            $this->real_end_date = $taskWorkDetail->real_end_date;
            $this->end_date = $taskWorkDetail->end_date;
            $this->description = $taskWorkDetail->description;
            if ($this->task_id) {
                $this->main_task_id = Task::findOrFail($this->task_id)->parent_id;
            }
            $this->reviewer_id = $taskWorkDetail->reviewer_id;
            $this->approve_id = $taskWorkDetail->approve_id;
            $this->admin_id = $taskWorkDetail->admin_id;
            $memberTask = TaskWorkHasUserInfo::query()->where(['task_work_detail_id' => $id])->get();
            foreach ($memberTask as $member) {
                $this->memberPickUpList[] = $member->user_info_id;
            }
            if ($taskWorkDetail->reviewer_id) {
                $this->isPreview = true;
            } else {
                $this->isPreview = false;
            }
            if ($taskWorkDetail->approve_id) {
                $this->isApproval = true;
            } else {
                $this->isApproval = false;
            }
            if ($taskWorkDetail->admin_id) {
                $this->isAssign = true;
            } else {
                $this->isAssign = false;
            }
            if (!empty($this->memberPickUpList)) {
                $this->isFollower = true;
            }
        }
    }

    public function render()
    {
        $mainTaskList = [];
        $taskList = [];
        $topicMember = [];
        $leaderProjectName = '';
        $previewerName = '';
        $topicList = Topic::all();
        $statusDataList = [
            TaskWork::STATUS_UNDO_TASK => __('research/taskwork.select-status.undo'),
            TaskWork::STATUS_ON_WORKING_TASK => __('research/taskwork.select-status.on-working'),
            TaskWork::STATUS_DONE_TASK => __('research/taskwork.select-status.done'),
        ];

        $levelDataList = [
            TaskWork::PRIORITY_LEVEL_LOW => __('research/taskwork.select-level.low'),
            TaskWork::PRIORITY_LEVEL_MEDIUM => __('research/taskwork.select-level.medium'),
            TaskWork::PRIORITY_LEVEL_HIGH => __('research/taskwork.select-level.high'),
            TaskWork::PRIORITY_LEVEL_EMERGENCY => __('research/taskwork.select-level.emergency'),
        ];


        $workTypeDataList = [
            TaskWork::MAIN_MISSION => __('research/taskwork.select-work-type.main'),
            TaskWork::SUB_MISSION => __('research/taskwork.select-work-type.sub'),
            TaskWork::DETAIL_TASK => __('research/taskwork.select-work-type.detail')
        ];

        if ($this->topic_id) {
            $this->topic = Topic::query()->where(['id' => $this->topic_id])->with([
                'projectResearch'
            ])->first();
            $topicMember = TopicHasUserInfo::where(['topic_id' => $this->topic_id])->with(['userInfo'])->whereHas('userInfo', function (Builder $query) {
                return $query->whereIn('id', $this->memberPickUpList);
            })->get();
            if ($this->taskID) {
                foreach ($topicMember as $member) {
                    foreach ($this->memberPickUpList as $memberPickUp) {
                        if ($memberPickUp == $member->user_info_id) {
                            $member->checked = true;
                        }
                    }
                }
            }
            $mainTaskList = Task::where(['topic_id' => $this->topic_id, 'parent_id' => null])->get();
            $leaderProjectName = Topic::query()->with(['leader'])->where(['id' => $this->topic_id])->first()->leader->fullname ?? '';
        }
        if ($this->main_task_id) {
            $taskList = Task::where(['parent_id' => $this->main_task_id, 'topic_id' => $this->topic_id])->get();
            $previewerName  = Task::query()->with(['userInfo'])->where(['parent_id' => null, 'topic_id' => $this->topic_id])->first()->userInfo->fullname ?? '';
        }
        if ($this->task_id) {
            $subTask = Task::findOrFail($this->task_id);
            $this->topicStartDate = reFormatDate($subTask->start_date, 'm/d/Y');
            $this->topicEndDate = reFormatDate($subTask->end_date, 'm/d/Y');
        }
        if ($this->file) {
            $this->store();
            $this->file = null;
        }

        $files = \App\Models\File::where('model_id', $this->taskID)->where('model_name', $this->model_name)->get();

        if (count($files) > 5) {
            $this->isOverFile = true;
        } else {
            $this->isOverFile = false;
        }
        $users = User::all();

        $query_user_infos = TopicHasUserInfo::query()->where(['topic_id' => $this->topic_id])->with(['userInfo']);
        if ($this->user_info_name) {
            $query_user_infos = $query_user_infos->whereHas('userInfo', function (Builder $query) {
                $query->where('unsign_text', 'like', "%" . strtolower(removeStringUtf8($this->user_info_name) . "%"));
            });
            $this->autoCompleteSearch = true;
        }
        $userInfos = $query_user_infos->get();

        return view(
            'livewire.admin.research-project.task-work.form-data',
            compact(
                'topicList',
                'statusDataList',
                'levelDataList',
                'mainTaskList',
                'taskList',
                'workTypeDataList',
                'topicMember',
                'files',
                'users',
                'leaderProjectName',
                'previewerName',
                'userInfos'
            )
        );
    }
    public function completeUpload(string $uploadUrl, string $eventName)
    {
        foreach ($this->newFiles as $file) {

            if ($file->getFilename() == $uploadUrl) {
                //                $url =  $file->storeAs('/', $file->getFilename(), 'local');
                $newFileName = $file->store('/', 'local');
                $url = Storage::disk('local')->url($newFileName);
                //                Storage::disk('local')->put('example.txt', 'Contents');
                $this->dispatchBrowserEvent($eventName, [
                    'url' => $url,
                    'href' => $url,

                ]);
                return;
            }
        }
    }
    public function deleteFile($id)
    {
        $data = \App\Models\File::findOrFail($id);
        if ($data) {
            $data->delete();
        }
    }

    public function store()
    {
        $isFileValid = $this->validateAttachedFile();
        if ($isFileValid) {
            $file = new FileUpload();
            $dataUpload = $file->uploadFile($this->file, $this->folder);

            $file_upload = new \App\Models\File();
            $file_upload->url = $dataUpload['url'];
            $file_upload->size_file = $dataUpload['size_file'];
            $file_upload->file_name = $dataUpload['file_name'];
            $file_upload->model_name = $this->model_name;
            $file_upload->model_id = $this->taskID;

            $file_upload->type = -1;
            $file_upload->save();

            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __("news/newsManager.menu_name.msg.upload-attached-file.success")]);
        }
    }

    public function checkFileIfExist()
    {
        if (is_string($this->file)) {
            return 'mimes:doc,dot,wbk,docx,docm,dotx,dotm,docb,xls,xlt,xlsx,xlsm,xltx,xltm,pdf,gif,png,jpg,tif,bmp';
        }
        return '';
    }

    protected function rules()
    {
        return [
            'status_id' => 'required',
            'level_id' => 'required',
            'topic_id' => 'required',
            'task_id' => 'required',
            'main_task_id' => 'required',
            'work_type_id' => 'required',
            'name' => 'required|max:100|regex:/^[a-zA-Z0-9' . config('common.special_character.alphabet') . '\.\,\-\(\)\/ ]+$/',
            'start_date' => 'required|date|after_or_equal:topicStartDate',
            'end_date' => 'required|date|after_or_equal:start_date|before:topicEndDate',
            'description' => 'max:1000',
            'file' => $this->checkFileIfExist()
        ];
    }

    public function removeMember($id)
    {
        unset($this->memberPickUpList[array_search($id, $this->memberPickUpList)]);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
        __('research/taskwork.member-remove-success')]);
    }
    public function memberClick($id)
    {
        if (array_search($id, $this->memberPickUpList) !== false) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" =>
            __('research/taskwork.member-exist')]);
        } elseif (!array_search($id, $this->memberPickUpList)) {
            array_push($this->memberPickUpList, $id);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
            __('research/taskwork.member-add-success')]);
        }
    }

    protected function getValidationAttributes()
    {
        return [
            'status_id' => __('research/taskwork.form-data.status'),
            'level_id' => __('research/taskwork.form-data.level'),
            'topic_id' => __('research/taskwork.form-data.topic'),
            'task_id' => __('research/taskwork.form-data.mission'),
            'main_task_id' => __('research/taskwork.form-data.main-mission'),
            'work_type_id' => __('research/taskwork.form-data.work-type'),
            'name' => __('research/taskwork.form-data.name'),
            'start_date' => __('research/taskwork.form-data.start-date'),
            'end_date' => __('research/taskwork.form-data.end-date'),

            'file' => __('research/taskwork.form-data.file'),
            'topicStartDate' => __('research/taskwork.form-data.topic-start-date'),
            'topicEndDate' => __('research/taskwork.form-data.topic-end-date'),
        ];
    }


    public function validateAttachedFile()
    {
        $allowed = array(
            'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', 'xls',
            'xlt', 'xlsx', 'xltx', 'xltm', 'pdf', 'gif', 'png', 'zip', 'jpg',
            'tif', 'bmp', 'xlsm'
        );
        if ($this->file->getSize() > 500000) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __("news/newsManager.menu_name.msg.upload-attached-file.is-over-size") . ' 500kb']);
            return false;
        } else {
            if (in_array($this->file->getClientOriginalExtension(), $allowed)) {
                return true;
            } else {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __("news/newsManager.menu_name.msg.upload-attached-file.wrong-file-format")]);
                return false;
            }
        }
    }

    public function storeTask()
    {
        $this->validate();
        if (!$this->isOverFile) {
            $formData = $this->setUpFormData();
            $taskWork = TaskWork::create($formData);
            TaskWorkHasResultPlan::query()
            ->where('admin_id', auth()->id())
            ->whereNull('task_work_detail_id')
            ->update([
                'task_work_detail_id' => $taskWork->id
            ]);
            if ($this->topic->projectResearch) {
                $formData['code'] = $this->topic->projectResearch->project_code . '_' . (string)$this->topic->projectResearch->id . (string)$taskWork->id;
            } else {
                $formData['code'] = $taskWork->id;
            }


            if ($this->isAssign) {
                if ($this->admin_id != null) {
                    $formData['admin_id'] = $this->admin_id;
                } else {
                    $formData['admin_id'] = Auth::id();
                }
            } else {
                $formData['admin_id'] = '';
            }

            TaskWork::findOrFail($taskWork->id)->update(
                $formData
            );
            $this->updateTaskFile($taskWork->id);
            $this->saveMemberTask($taskWork->id);
            session()->flash('success', __('research/taskwork.msg.create.success'));
            $this->redirect(route('admin.research-project.task-work.index'));
        }
    }

    public function updateTaskFile($taskID)
    {
        $files = \App\Models\File::where('model_id', null)->where('model_name', $this->model_name)->get();
        foreach ($files as $file) {
            File::findOrFail($file->id)->update(
                ['model_id' => $taskID]
            );
        }
    }

    public function update()
    {
        if ($this->status_id == TaskWork::STATUS_ON_WORKING_TASK) {
            $this->real_start_date = Carbon::now()->toDateTimeString();
        }
        if ($this->status_id == TaskWork::STATUS_DONE_TASK) {
            $this->real_end_date = Carbon::now()->toDateTimeString();
        }
        $this->validate([
            'name' => 'required|max:100|regex:/^[a-zA-Z0-9' . config('common.special_character.alphabet') . '\.\,\-\(\)\/ ]+$/',
            'description' => 'max:1000',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'file' => $this->checkFileIfExist()
        ], [], [
            'name' => __('research/taskwork.form-data.name'),
            'description' => __('news/newsManager.menu_name.form-data.description'),
            'start_date' => __('research/taskwork.form-data.start-date'),
            'end_date' => __('research/taskwork.form-data.end-date'),
            'file' => __('research/taskwork.form-data.file'),
        ]);
        if (!$this->isOverFile) {
            $formData = $this->setUpFormData();
            $formData['real_start_date'] = $this->real_start_date;
            $formData['real_end_date'] = $this->real_end_date;

            TaskWork::findOrFail($this->taskID)->update($formData);
            $this->removeMemberTask();
            $this->saveMemberTask($this->taskID);
            session()->flash('success', __('research/taskwork.msg.update.success'));
            $this->redirect(route('admin.research-project.task-work.edit', $this->taskID));
        }
    }

    public function removeMemberTask()
    {
        $listMember = TaskWorkHasUserInfo::where(['task_work_detail_id' => $this->taskID])->get();
        foreach ($listMember as $member) {
            TaskWorkHasUserInfo::findOrFail($member->id)->delete();
        }
    }

    public function saveMemberTask($taskWorkId)
    {
        if ($taskWorkId && $this->isFollower) {
            foreach ($this->memberPickUpList as $memberId) {
                $data = [
                    'user_info_id' => $memberId,
                    'task_work_detail_id' => $taskWorkId
                ];
                TaskWorkHasUserInfo::create($data);
            }
        }
    }

    public function setUpFormData()
    {
        $this->mainTask = Task::findOrFail($this->main_task_id);
        $this->reviewer_id = $this->mainTask->user_info_id ?? '';
        $this->topic =  Topic::query()->where(['id' => $this->topic_id])->with([
            'projectResearch'
        ])->first();
        $this->approve_id = $this->topic->leader_id ?? '';
        $formData = [
            'priority_level' => $this->level_id,
            'status' => $this->status_id,
            'topic_id' => $this->topic_id,
            'task_id' => $this->task_id,
            'work_type' => $this->work_type_id,
            'name' => trim($this->name),
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'description' => str_replace('&nbsp;', '', trim($this->description)),
        ];

        if ($this->isPreview) {
            $formData['reviewer_id'] = $this->reviewer_id;
        } else {
            $formData['reviewer_id'] = '';
        }

        if ($this->isApproval) {
            $formData['approve_id'] = $this->approve_id;
        } else {
            $formData['approve_id'] = '';
        }

        if ($this->isAssign) {
            if ($this->admin_id != null) {
                $formData['admin_id'] = $this->admin_id;
            } else {
                $formData['admin_id'] = Auth::id();
            }
        } else {
            $formData['admin_id'] = '';
        }

        return $formData;
    }

    public function setStartTime($date)
    {
        $this->start_date = Carbon::createFromFormat(config('common.formatDate'), $date)->format('Y-m-d');
    }

    public function setEndTime($date)
    {
        $this->end_date = Carbon::createFromFormat(config('common.formatDate'), $date)->format('Y-m-d');
    }

    public function back()
    {
        $task = TaskWork::find($this->model_id) ?? null;
        if (!$task) {
            $files = \App\Models\File::where('model_id', $this->model_id)->where('model_name', $this->model_name)->get();
            foreach ($files as $file) {
                File::findOrFail($file->id)->delete();
            }
        }

        $this->redirect(route('admin.research-project.task-work.index'));
    }

    public function setUserInfoId($id)
    {
        $this->user_info_id = $id;
        $this->memberClick($id);
        $this->user_info_name = '';
    }

    public function closeAutoComplete()
    {
        $this->user_info_name = '';
        $this->autoCompleteSearch = false;
    }
}
