<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToWorkPlanHasBusinessFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_plan_has_business_fee', function (Blueprint $table) {
            $table->tinyInteger('status')->nullable()->comment('0: luu nhap, 1: da luu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_plan_has_business_fee', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
