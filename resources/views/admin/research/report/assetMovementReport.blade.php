<div class="report-main-text text-center">
    <h3>{{__('research/report.report3-page.title')}}</h3>
    <p>{{__('research/report.report3-page.sub-title')}}</p>
    {{--    <h3>   {{__('research/report.report-title')}}</h3>--}}
    {{--    <p> {{__('research/report.report-session.year')}} 2021 - {{__('research/report.report-session.start')}} 15/10/1020 {{__('research/report.report-session.to')}} 15/10/2021</p>--}}
</div>
<div class="col-md-12 download-report-btn">
    @if(checkPermission('admin.asset.report.download'))
        <div class=" float-right">
            {{--                                wire:click='export'--}}
            <button type="button"  class="btn par6" data-toggle="modal" data-target="#exportModal"
                    title="{{__('common.button.export_file')}}"><img src="/images/filterdown.svg" alt="filter-down"></button>
        </div>
    @endif
</div>
<div class="table-responsive"  style="overflow-y: scroll;
overflow-x: hidden;">
    <table class="table table-general working-plan" >
        <thead>
        <tr class="border-radius text-center">
            <th scope="col" class="text-center">
                {{__('research/report.table.stt')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report3-page.table.code')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report3-page.table.name')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report3-page.table.begin-period')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report3-page.table.up')}}
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid">
                        {{__('research/report.report3-page.table.added')}}
                    </div>
                    <div class="col-md-6">
                        {{__('research/report.report3-page.table.move-to')}}
                    </div>
                </div>
            </th>
            <th scope="col" class="text-center">
               {{__('research/report.report3-page.table.down')}}
                <div class="row">
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report3-page.table.move-away')}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report3-page.table.lost')}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report3-page.table.cancel')}}
                    </div>
                    <div class="col-md-3">
                        {{__('research/report.report3-page.table.liquidation')}}
                    </div>
                </div>
            </th>
            <th scope="col">
                {{__('research/report.report3-page.table.end-period')}}
            </th>
            <th scope="col">
                {{__('research/report.report3-page.table.movement-statement')}}
                <div class="row">
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report3-page.table.inUse')}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report3-page.table.transfer')}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report3-page.table.repaired')}}
                    </div>
                    <div class="col-md-3">
                        {{__('research/report.report3-page.table.maintenance')}}
                    </div>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($assetMovement as $category)
            <tr class="border-radius text-center">
                <td colspan="3" class="text-left" style="    background: gray;
    color: white;">
                    {{$category->code}} - {{$category->name}}
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    {{$category->countAssetBeginQuanity}}
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    <div class="row">
                        <div class="col-md-6" style="border-right: 1px solid">
                            {{$category->countAssetBeginQuanity}}
                        </div>
                        <div class="col-md-6">
                            0
                        </div>
                    </div>
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    <div class="row">
                        <div class="col-md-3" style="border-right: 1px solid">
                           0
                        </div>
                        <div class="col-md-3" style="border-right: 1px solid">
                            {{$category->countAssetLosted}}
                        </div>
                        <div class="col-md-3" style="border-right: 1px solid">
                            {{$category->countAssetCancel}}
                        </div>
                        <div class="col-md-3">
                            {{$category->countAssetLidation}}
                        </div>
                    </div>
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    {{$category->countAssetEndQuanity}}
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    <div class="row">
                        <div class="col-md-3" style="border-right: 1px solid">
                            {{$category->countAssetInUse}}
                        </div>
                        <div class="col-md-3" style="border-right: 1px solid">
                            {{$category->countassetTransfer}}
                        </div>
                        <div class="col-md-3" style="border-right: 1px solid">
                            {{$category->countAssetRepaired}}
                        </div>
                        <div class="col-md-3">
                            {{$category->countAssetMaintenance}}
                        </div>
                    </div>
                </td>
            </tr>

            @if (count($category->asset) > 0)
                @foreach($category->asset as $key => $asset)
                    <tr class="border-radius text-center">
                        <td class="text-center">
                            {{$key + 1 }}
                        </td>
                        <td class="text-center">
                            {{$asset->code}}
                        </td>
                        <td class="text-center">
                            {{$asset->name}}
                        </td>
                        <td class="text-center">
                            {{$asset->beginPeriodQuanity}}
                        </td>
                        <td class="text-center">
                            <div class="row">
                                <div class="col-md-6" style="border-right: 1px solid">
                                    {{$asset->beginPeriodQuanity}}
                                </div>
                                <div class="col-md-6">
                                    0
                                </div>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="row">
                                <div class="col-md-3" style="border-right: 1px solid">
                                    0
                                </div>
                                <div class="col-md-3" style="border-right: 1px solid">
                                    {{$asset->losted}}
                                </div>
                                <div class="col-md-3" style="border-right: 1px solid">
                                    {{$asset->cancel}}
                                </div>
                                <div class="col-md-3">
                                    {{$asset->liquidation}}
                                </div>
                            </div>
                        </td>
                        <td class="text-center">
                            {{$asset->endPeriodQuanity}}
                        </td>
                        <td class="text-center">
                            <div class="row">
                                <div class="col-md-3" style="border-right: 1px solid">
                                    {{$asset->amount_use ?? 0}}
                                </div>
                                <div class="col-md-3" style="border-right: 1px solid">
                                    {{$asset->transfer}}
                                </div>
                                <div class="col-md-3" style="border-right: 1px solid">
                                    {{$asset->inRepaired}}
                                </div>
                                <div class="col-md-3">
                                    {{$asset->inMaintenance}}
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9">
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    </td>
                </tr>
            @endif

        @endforeach

        <tr class="border-radius">
            <td class="text-center" colspan="3" style="background: #F6F6F7;">
                {{__('research/report.report3-page.table.sum')}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetMovement->totalAssetBeginQuanity}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetBeginQuanity}}
                    </div>
                    <div class="col-md-6">
                        0
                    </div>
                </div>
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                <div class="row">
                    <div class="col-md-3" style="border-right: 1px solid">
                        0
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetLost}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetCancel}}
                    </div>
                    <div class="col-md-3">
                        {{$assetMovement->totalAssetLiquidation}}
                    </div>
                </div>
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetMovement->totalAssetEndQuanity}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                <div class="row">
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetInUse}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetTransfer}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetRepaired}}
                    </div>
                    <div class="col-md-3">
                        {{$assetMovement->totalAssetMaintenance}}
                    </div>
                </div>
            </td>
            </td>
        </tr>
        </tbody>
    </table>

    @if (count($assetMovement) > 0)
        {{ $assetMovement->links() }}
    @else
        <div class="title-approve text-center">
            <span>{{ __('common.message.empty_search') }}</span>
        </div>
    @endif
</div>

