<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="{{ route('admin.system.role.index') }}">{{ __('menu_management.menu_name.leave_configuration') }}</a></div>
        </div>
    </div>
    <div class="bd-border p-4">
        <form class="bg-white p-4">
            <div class="form-group mb-2">
                <input type="checkbox" wire:model.defer="leaveOnProbationary.check">
                {{ __('data_field_name.leave_config.leave_on_probationary') }}
            </div>

            <div class="form-group">
                <p>{{ __('data_field_name.leave_config.first_month_leave.name') }}</p>
                <p class="pl-3">
                    <input type="radio" name="firstMonthLeave.value" wire:model.defer="firstMonthLeave.value" value="any_date">
                    <label class="d-inline-block w-50">{{ __('data_field_name.leave_config.first_month_leave.any_date') }}</label>
                </p>
                <p class="pl-3">
                    <input type="radio" name="firstMonthLeave.value" wire:model.defer="firstMonthLeave.value" value="before_date">
                    <label class="d-inline-block w-50">
                        {!! __('data_field_name.leave_config.first_month_leave.before_date') !!}
                        <input type="text" wire:model.defer="firstMonthLeave.before_date" class="format_number form-control w-25 d-inline" maxlength="2" onchange="this.value = (Number(this.value) > 31) ? 31 : this.value">
                    </label>
                </p>
                @error('firstMonthLeave.before_date')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <p>
                    <input type="checkbox" wire:model.defer="allowAdvanceLeave.check">
                    {{ __('data_field_name.leave_config.allow_advance_leave.name') }}
                </p>
                <p class="pl-3">
                    <input type="radio" name="allowAdvanceLeave.value" wire:model.defer="allowAdvanceLeave.value" value="unlimited">
                    <label class="d-inline-block w-50">{{ __('data_field_name.leave_config.allow_advance_leave.unlimited') }}</label>
                </p>
                <p class="pl-3">
                    <input type="radio" name="allowAdvanceLeave.value" wire:model.defer="allowAdvanceLeave.value" value="limited">
                    <label class="d-inline-block w-50">
                        {!! __('data_field_name.leave_config.allow_advance_leave.limited', [
                            'day' => Form::text('limited_day', 0, ['class' => 'form-control w-25 d-inline format_number', 'maxlength' => 2, 'wire:model.defer' => 'allowAdvanceLeave.limited', 'onchange' => 'this.value = (Number(this.value) > 60) ? 60 : this.value']),
                        ]) !!}
                    </label>
                </p>
                @error('allowAdvanceLeave.limited')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <p>
                    <input type="checkbox" wire:model.defer="increaseLeaveBySeniority.check">
                    {{ __('data_field_name.leave_config.increase_leave_by_seniority.name') }}
                </p>
                <p class="pl-3">
                    <input type="radio" name="increaseLeaveBySeniority.value" wire:model.defer="increaseLeaveBySeniority.value" value="labor_law">
                    <label class="d-inline-block w-50">{{ __('data_field_name.leave_config.increase_leave_by_seniority.labor_law') }}</label>
                </p>
                <p class="pl-3">
                    <input type="radio" name="increaseLeaveBySeniority.value" wire:model.defer="increaseLeaveBySeniority.value" value="customize">
                    <label class="d-inline-block w-50">
                        {!! __('data_field_name.leave_config.increase_leave_by_seniority.customize', [
                            'day' => Form::text('day', 0, ['class' => 'form-control w-25 d-inline format_number', 'maxlength' => 2, 'wire:model.defer' => 'increaseLeaveBySeniority.customize.addition_days']),
                            'year' => Form::text('year', 0, ['class' => 'form-control w-25 d-inline format_number', 'maxlength' => 2, 'wire:model.defer' => 'increaseLeaveBySeniority.customize.passed_years']),
                        ]) !!}
                    </label>
                    @error('increaseLeaveBySeniority.customize.passed_years')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                    @error('increaseLeaveBySeniority.customize.addition_days')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </p>

                <p class="pl-3">
                    <label class="d-inline-block w-50">{{ __('data_field_name.leave_config.increase_leave_by_seniority.calculate_from') }}: </label>
                    <label class="d-inline-block w-50">
                        <input type="radio" name="calculate_from" wire:model.defer="increaseLeaveBySeniority.calculate_from" value="probation"> {{ __('data_field_name.leave_config.increase_leave_by_seniority.probation') }}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="calculate_from" wire:model.defer="increaseLeaveBySeniority.calculate_from" value="official"> {{ __('data_field_name.leave_config.increase_leave_by_seniority.official') }}
                    </label>
                </p>
            </div>

            <div class="form-group">
                <p>
                    <input type="checkbox" wire:model.defer="unusedLeaveToNewYear.check">
                    {{ __('data_field_name.leave_config.unused_leave_to_new_year.name') }}
                </p>
                <p class="pl-3">
                    <input type="radio" name="unusedLeaveToNewYear.value" wire:model.defer="unusedLeaveToNewYear.value" value="all">
                    <label class="d-inline-block w-50">{{ __('data_field_name.leave_config.unused_leave_to_new_year.all') }}</label>
                </p>
                <p class="pl-3">
                    <input type="radio" name="unusedLeaveToNewYear.value" wire:model.defer="unusedLeaveToNewYear.value" value="limit">
                    <label class="d-inline-block w-50">
                        {!! __('data_field_name.leave_config.unused_leave_to_new_year.limited', [
                            'day' => Form::text('limited_day', 0, ['class' => 'form-control w-25 d-inline format_number', 'maxlength' => 2, 'wire:model.defer' => 'unusedLeaveToNewYear.limit']),
                        ]) !!}
                    </label>
                    @error('unusedLeaveToNewYear.limit')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </p>
                <p class="pl-3">
                    <input type="checkbox" wire:model.defer="unusedLeaveToNewYear.expired_in.check">
                    <label class="d-inline-block w-50">
                        {!! __('data_field_name.leave_config.unused_leave_to_new_year.expired_in', [
                            'month' => Form::text('expired_value', 0, ['class' => 'form-control w-25 d-inline', 'maxlength' => 2, 'wire:model.defer' => 'unusedLeaveToNewYear.expired_in.value']),
                        ]) !!}
                    </label>
                    @error('unusedLeaveToNewYear.expired_in.value')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </p>
            </div>

            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.system.leave-config.index') }}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{ __('common.button.cancel') }}</a>
                <button type="button" wire:click="save" class="btn btn-main bg-primary text-9 size16 px-48 py-14">{{ __('common.button.save') }}</button>
            </div>
        </form>
    </div>
</div>
