@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.asset.dashboard')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    <!--start news -->
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.asset.list_title')}} </a>
                    \ <span>{{__('data_field_name.asset.dashboards')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="title">{{__('data_field_name.asset.dashboards')}}</h3>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="dashboard-asset">
                    <div class="row">
                        <div class="col-md-4 pd8">
                            <div class="card">
                                <div class="widget-chart-content">
                                    <div class="subheading">
                                        {{__('data_field_name.asset.total_value')}}
                                        <div class="float-right">
                                            <img src="/images/blocks.svg" alt="blocks"/>
                                        </div>
                                    </div>
                                    <div class="widget-numbers">
                                        {{numberFormat($total)}}<span> {{__('data_field_name.asset.billion')}}</span>
                                    </div>
                                    <div class="widget-description">
                                        {{$quantity}} {{__('data_field_name.asset.asset')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 pd8">
                            <div class="card">
                                <div class="widget-chart-content">
                                    <div class="subheading">
                                        {{__('data_field_name.asset.being_used')}}
                                        <div class="float-right">
                                            <img src="/images/thunder-move.svg" alt="thunder-move"/>
                                        </div>
                                    </div>
                                    <div class="widget-numbers">
                                        {{numberFormat($used_value)}}
                                        <span> {{__('data_field_name.asset.billion')}}</span>
                                    </div>
                                    <div class="widget-description">
                                        {{$mount_used}} {{__('data_field_name.asset.asset')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 pd8">
                            <div class="card">
                                <div class="widget-chart-content">
                                    <div class="subheading">
                                        {{__('data_field_name.asset.not_use')}}
                                        <div class="float-right">
                                            <img src="/images/archive.svg" alt="archive"/>
                                        </div>
                                    </div>
                                    <div class="widget-numbers">
                                        {{numberFormat($total-$used_value)}}
                                        <span> {{__('data_field_name.asset.billion')}}</span>
                                    </div>
                                    <div class="widget-description">
                                        {{$quantity-$mount_used}} {{__('data_field_name.asset.asset')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 pd8">
                            <div class="card">
                                <div class="widget-chart-content">
                                    <div class="subheading">
                                        {{__('data_field_name.asset.broken_maintenance')}}
                                        <div class="float-right">
                                            <img src="/images/thumbtack.svg" alt="thumbtack"/>
                                        </div>
                                    </div>
                                    <div class="widget-numbers">
                                        {{numberFormat($value_maintain)}}
                                        <span> {{__('data_field_name.asset.billion')}}</span>
                                    </div>
                                    <div class="widget-description">
                                        {{$mount_maintain}} {{__('data_field_name.asset.asset')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 pd8">
                            <div class="card">
                                <div class="widget-chart-content">
                                    <div class="subheading">
                                        {{__('data_field_name.asset.lost_cancel')}}
                                        <div class="float-right">
                                            <img src="/images/spam.svg" alt="spam"/>
                                        </div>
                                    </div>
                                    <div class="widget-numbers">
                                        {{numberFormat($value_lost)}}
                                        <span> {{__('data_field_name.asset.billion')}}</span>
                                    </div>
                                    <div class="widget-description">
                                        {{$mount_lost}} {{__('data_field_name.asset.asset')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 pd8">
                            <div class="card">
                                <div class="widget-chart-content">
                                    <div class="subheading">
                                        {{__('data_field_name.asset.diff')}}
                                        <div class="float-right">
                                            <img src="/images/Commode.svg" alt="commode"/>

                                        </div>
                                    </div>
                                    <div class="widget-numbers">
                                        {{numberFormat($value_report_lost+$value_report_maintain)}}
                                        <span> {{__('data_field_name.asset.billion')}}</span>
                                    </div>
                                    <div class="widget-description">
                                        {{$mount_report_maintain+$mount_report_lost}} {{__('data_field_name.asset.asset')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row chart-asset">
                        <div class="col-md-6 pd8 ">
                            <div class="card height576 ">
                                <div class="widget-chart-content card_title">
                                    <div class="subheading">
                                        {{__('data_field_name.asset.static_group')}}
                                        <div class="float-right sum-mumber text-uppercase">
                                            {{__('data_field_name.asset.total')}}: {{count($assetCategorys)}}
                                        </div>
                                    </div>
                                    <div class="widget-description text-uppercase">
                                        VKIST
                                    </div>
                                </div>
                                <div id="pieChart"></div>
                            </div>
                        </div>
                        <div class="col-md-6 pd8">
                            <div class="card">
                                <div class="widget-chart-content card_title">
                                    <div class="subheading">
                                        {{__('data_field_name.asset.volatility_asset')}}
                                    </div>
                                    <div class="widget-description text-uppercase">
                                        VKIST, {{__('data_field_name.asset.year')}} {{$assetRevenue[0]->report_year ?? ''}}
                                    </div>
                                </div>
                                <figure class="highcharts-figure container-fluid">
                                    <div id="chartColumn"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-12 pd8 ">
                            <div class="card">
                                <figure class="highcharts-figure container-fluid">
                                    <div id="chartColumn2"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end news -->
    <script>
        Highcharts.chart('chartColumn', {
            chart: {
                type: 'column'
            },
            plotOptions: {
                column: {
                    colorByPoint: true
                }
            },
            colors: [
                '#6AD8A7',
            ],
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'M1',
                    'M2',
                    'M3',
                    'M4',
                    'M5',
                    'M6',
                    'M7',
                    'M8',
                    'M9',
                    'M10',
                    'M11',
                    'M12'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',

                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0,
                    borderWidth: 0
                }
            },
            series: [{
                name: '',
                data: <?php echo json_encode($dataAssetRevenue)?>
            }]
        });

        Highcharts.chart('chartColumn2', {
            chart: {
                type: 'column',
                style: {
                    fontFamily: 'Arial'
                }
            },
            plotOptions: {
                column: {
                    colorByPoint: true
                }
            },
            colors: [
                '#6AD8A7',
            ],
            title: {
                text: '<?php echo __('data_field_name.asset.title2') ?>'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: <?php echo json_encode($nameCate)?>,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',

                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.05,
                    borderWidth: 0
                }
            },
            series: [
                {
                    color: '#2bb1d6',
                    name: '<?php echo __('data_field_name.asset.origin_value') ?>',
                    data: <?php echo json_encode($countTotalValue)?>
                },
                {
                    color: '#373b9e',
                    name: '<?php echo __('data_field_name.asset.depre_value') ?>',
                    data: <?php echo json_encode($depreciation)?>
                },
                {
                    color: '#8e5de3',
                    name: '<?php echo __('data_field_name.asset.value_left') ?>',
                    data: <?php echo json_encode($leftValue)?>
                }
            ]
        });

        // Build the chart
        function randomColor() {
            var letters = 'BCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * letters.length)];
            }
            return color;
        }

        Highcharts.chart('pieChart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

            colors: [
                <?php
                foreach ($assetCategorys as $category) {
                ?>
                randomColor(),
                <?php }?>

            ],
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            legend: {
                labelFormatter: function () {
                    return '<div style="text-align: left; width:130px; font-family: Arial">' + this.name + '</div><div style="width:40px; text-align:right;font-family: Arial">&nbsp' + this.y + '</div>';
                },
            },
            plotOptions: {
                column: {
                    colorByPoint: true
                },
                pie: {
                    size: '100%',
                    height: '100%',
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true,
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [
                        <?php
                        foreach ($assetCategorys as $category){
                        ?>
                    {
                        name: '<?php echo $category->name ?>',
                        y: <?php echo $category->countPercent ?>,
                        sliced: true,
                        selected: true
                    },
                    <?php }?>
                ]

            }]
        });


    </script>
@endsection
