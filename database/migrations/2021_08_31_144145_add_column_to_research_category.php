<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToResearchCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_category', function (Blueprint $table) {
            //
            $table->string('code', 255)->nullable()->comment('Ma loai de tai');
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_category', function (Blueprint $table) {
            //
            $table->dropColumn('code');
            $table->dropIndex('unsign_text');
            $table->dropColumn('unsign_text');
            $table->dropSoftDeletes();
        });
    }
}
