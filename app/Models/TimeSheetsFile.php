<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class TimeSheetsFile extends BaseModel
{
    use HasFactory;
    protected $table = 'timesheets_file';
    protected $fillable = [];
}
