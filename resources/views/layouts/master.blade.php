<!DOCTYPE html>
<html lang="en">
@include('layouts.partials._head')
@yield('css')
<body>
<!-- #section:basics/navbar.layout -->
@include('layouts.partials._header')

<!-- /section:basics/navbar.layout -->
<div class="container-fluid body-container" style="height:auto!important">
    <div class="row">
        @yield('homeLeft')
            @if (!isset($slot))
                @yield('content')
            @else
                {{ $slot }}
            @endif
        @yield('homeRight')
    </div>
</div>

    @include('layouts.partials._footer')
<!-- /.main-main-container -->
@include('layouts.partials._script')
@yield('js')
</body>
</html>
