<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use App\Models\TimeSheetsMonthly;
use App\Models\TimeSheetsDaily;
use App\Models\TimeSheetsFile;
use App\Models\UserInf;
use Illuminate\Support\Collection;
class TimeSheetsExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($id) {
        $this->id = $id;
        $this->month = null;
        $this->year = null;
    }
    public function collection()
    {
        $timeSheets = TimeSheetsFile::where('id', $this->id)->first();
        $this->name = $timeSheets->name;
        $this->month = $timeSheets->month_working;
        $this->year = $timeSheets->year_working;
        $data = TimeSheetsMonthly::where('timesheets_file_id', $this->id)
            ->OrderBy('id', 'desc')
            ->get();
        $data = $data->map(function ($item) {
            $countDate = TimeSheetsDaily::where('timesheets_monthly_id', $item->id)->where('working_status', '<>', -1)->count();
            $user = UserInf::where('id', $item->user_info_id)->first();

            return [
                'userCode' => !empty($user) ? $user->code : null,
                'fullname' => !empty($user) ? $user->fullname : null,
                'department' => !empty($user) && !empty($user->department) ? $user->department->name : null,
                'countDate' => $countDate,
                'actual_workday' => (int)$item->total_work_day,
                'leave_day' => $item->leave_day,
                'unpaid_day' => $item->unpaid_day,
                'holiday' => $item->holiday
            ];
        });
        return $data;
    }
    public function headings():array
    {
        return [
            'Mã nhân viên',
            'Họ và tên',
            'Đơn vị',
            'Ngày công chuẩn',
            'Công thực tế',
            'Nghỉ phép',
            'Nghỉ việc riêng không hưởng lương',
            'Nghỉ lễ'
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:H1');
            $event->sheet->setCellValue('A1', 'Bảng chấm công tháng ' . $this->month . ' năm ' . $this->year);
            $event->sheet->getStyle('A1:H1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:H3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        return 'Bảng chấm công tháng ' . $this->month . ' năm ' . $this->year;
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
