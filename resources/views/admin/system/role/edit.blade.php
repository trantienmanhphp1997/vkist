@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.system.role.text_info_role')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div  class="breadcrumbs"><a href="{{route('admin.system.role.index')}}">{{__('data_field_name.system.role.text_role_management')}} </a> \ <span>{{__('data_field_name.system.role.text_info_role')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="detail-task information box-idea create-edit-role">
                    <h4>{{__('data_field_name.system.role.text_info_role')}}</h4>
                    <div class="group-tabs">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link @if($tab != 'user') active @endif" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                    <span>{{__('data_field_name.common_field.general_information')}}</span></a>
                                <a class="nav-item nav-link @if($tab == 'user') active @endif" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="true">
                                    <span>{{__('data_field_name.system.account.user_list')}}</span>
                                </a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade @if($tab != 'user') active show @endif" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                {!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal',  'autocomplete' => "off",'route' => ['admin.system.role.update', $data->id]]) !!}
                                    @include('admin.system.role._formRole')
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade @if($tab == 'user') active show @endif" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <form method="POST" action="{{route('admin.system.role.addroleuser', $data->id)}}">
                                    @csrf
                                    {{ method_field('POST') }}
                                    <div class="inner-tab">
                                        @livewire('admin.system.role.user-list', ['roleId' => $data->id])
                                        <div class="col-md-12 ">
                                            <div class="group-btn2 text-center group-btn ">
                                                <a href="{{route('admin.system.role.index')}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                                                <button type="submit" class="btn btn-save">{{__('common.button.save')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
