<?php
return [
    'name' => 'Quản lý dự án nghiên cứu',
    'status' => [
        'wait_approval' => 'Chưa thực hiện',
        'in_process' => 'Đang thực hiện',
        'completed' => 'Đã hoàn thành',
        'cancel' => 'Hủy bỏ',
        'draft' => 'Lưu trữ',
        'unfinished' => 'Đang thực hiện dở',
        'acceptance' => 'Đang nghiệm thu'
    ],
    'information' => [
        'breadcrumbs' => 'Thông tin dự án',
        'name' => 'Quản lý thông tin dự án',
        'table-title' => 'Danh sách dự án',
        'search' => 'Tìm dự án',
        'create' => 'Tạo dự án',
        'table-column' => [
            'code-project' => 'MÃ DỰ ÁN',
            'name' => 'TÊN DỰ ÁN',
            'research-category' => 'LOẠI ĐỀ TÀI',
            'leader' => 'CHỦ NHIỆM ĐỀ TÀI',
            'status' => 'TRẠNG THÁI'
        ],
        'tab-project' => [
            'tab-name' => 'Dự án',
            'title-create' => 'Tạo dự án mới',
            'title-update' => 'Cập nhật dự án',
            'name' => 'Tên đề tài',
            'project-code' => 'Mã đề tài',
            'contract-code' => 'Mã hợp đồng',
            'research-type' => 'Loại đề tài',
            'status' => 'Trạng thái dự án',
            'man-month' => 'Quy mô dự án',
            'start' => 'Ngày bắt đầu',
            'end' => 'Ngày kết thúc',
            'description' => 'Mô tả dự án',
            'note'=>'Ghi chú',
            'department'=>'Đơn vị thực hiện',
        ],
        'tab-project-member' => [
            'tab-name' => 'Thành viên dự án'
        ],
        'create-project-information-success' => 'Tạo mới dự án thành công',
        'create-project-information-fail' => 'Tạo mới dự án thất bại',
        'update-project-information-success' => 'Cập nhật dự án thành công',
        'update-project-information-fail' => 'Cập nhật dự án thất bại',
        'delete-msg-success' => 'Xóa thông tin dự án thành công',
    ],
    'error' => [
        'warning-saving-draft' => 'Bạn có chắc chắn muốn lưu trữ dự án này không ?',
        'warning-transfer-to-operation' => 'Bạn có chắc chắn muốn khôi phục dự án này không',
        'no-contract-code-valid'=>'Không tìm thấy đề tài tương ứng với mã hợp đồng bạn nhập',
        'invalid-status' => 'Vẫn còn nhiệm vụ chưa hoàn thành',
    ],
    'success'=>[
        'success-saving-draft'=>'Lưu trữ dữ án thành công',
        'success-transfer-operation'=>'Dự án quay lại hoạt động thành công'
    ],
    'work' => [
        'breadcrumbs' => 'Thông tin dự án',
        'name' => 'Quản lý công việc dự án',
    ],
    'performance' => [
        'breadcrumbs' => 'Thông tin dự án',
        'name' => 'Quản lý hiệu suất công việc',
    ]
];
