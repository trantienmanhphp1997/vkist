<?php

namespace App\Http\Livewire\Admin\Asset\AllocatedAndRevoke;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use App\Models\AssetAllocatedRevoke;
use App\Models\UserInf;
use DB;
use App\Exports\AllocatedAndRevokeExport;
use Excel;
class ListData extends BaseLive
{
    public $searchTerm;
    public $isChecked;
    public $selectedId;
    public $selectedValue;
    public $arrData;
    public $total_asset = 0;

    public function mount() {
    	$this->isChecked = 0;
    	$this->arrData = [];
        $this->checkCreatePermission = checkRoutePermission('create');
    }

    public function render() {
    	$data = $this->searchData();
        return view('livewire.admin.asset.allocated-and-revoke.list-data', ['data' => $data]);
    }

    public function searchData() {
        $this->total_asset = 0;
        $query = UserInf::query()
        	->select('user_info.unsign_text', 'user_info.id', 'user_info.code', 'user_info.fullname', 'user_info.phone', 'user_info.email', 'user_info.gender', DB::raw('SUM(amount_used) as total'))
            ->join('asset', 'asset.user_info_id', 'user_info.id')
            ->groupBy('user_info.unsign_text', 'user_info.id', 'user_info.code', 'user_info.fullname', 'user_info.phone', 'user_info.email', 'user_info.gender', 'user_info_id');
        if (strlen($this->searchTerm)) {
        	$query->where('user_info.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        $data = $query->orderBy('user_info.id','DESC')->paginate($this->perPage);
        $tmp = $data->map(function ($item) {
        	$user = UserInf::where('id', $item->id)->first();
            $position = !empty($user->position) ? $user->position->v_value : null;
            $department = !empty($user->department) ? $user->department->name : null;
            if (is_numeric($this->selectedId)) {
                if ($item->id == $this->selectedId) {
                    $selected = $this->selectedValue == 1 ? 0 : 1;
                } else {
                    if (!in_array($item->id, $this->arrData)) {
                        $selected = $this->isChecked;
                    } else {
                        $selected = 1;
                    }
                }
            } else {
                $selected = $this->isChecked;
            }
            if ($selected == 1 && !in_array($item->id, $this->arrData)) {
                array_push($this->arrData, $item->id);
            }
            if ($selected == 0 && in_array($item->id, $this->arrData)) {
                array_splice($this->arrData, array_search($item->id, $this->arrData), 1);
            }
            $this->total_asset += $item->total;
            return [
                'id' => $item->id,
                'code' => $item->code,
                'fullname' => $item->fullname,
                'phone' => $item->phone,
                'email' => $item->email,
                'gender' => $item->gender,
                'position' => $position,
                'department' => $department,
                'selected' => $selected,
                'total' => $item->total,
            ];
        });
        $data->setCollection($tmp);
        return $data;
    }
    public function selectedItem($id, $value) {
    	$this->selectedId = $id;
    	$this->selectedValue = $value;
    }

    public function checked($value) {
    	$this->isChecked = $value;
    	$this->selectedId = null;
    }

    public function export() {
        return Excel::download(new AllocatedAndRevokeExport($this->searchTerm), 'allocated-revoke-' . now()->format(Config::get('common.year-month-day')) . '-' . '.xlsx');
    }
}
