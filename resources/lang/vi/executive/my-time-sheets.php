<?php
return [
    'title' => 'Bảng chấm công',
    'time-sheets' => 'Bảng chấm công tháng :month',
    'personal-info' => [
        'title' => 'THÔNG TIN CÁ NHÂN',
        'name' => 'Họ và tên',
        'position' => 'Chức danh',
        'code' => 'Mã nhân viên',
        'base_salary_coefficient' => 'Hệ số lương cơ bản',
        'bank_account' => 'Số tài khoản ngân hàng',
        'bank_name' => 'Tên ngân hàng'
    ],
    'timekeeping-info' => [
        'title' => 'THÔNG TIN CHẤM CÔNG',
        'period' => 'Kỳ chấm công',
        'number-of-paid-leave-days' => 'Số ngày nghỉ hưởng lương',
        'number-of-days-of-unpaid-leave' => 'Số ngày nghỉ không hưởng lương',
        'standard-work-by-month' => 'Công chuẩn theo tháng',
        'actual-number-of-working-days' => 'Công thực tế',
        'total-number-of-paid-working-days' => 'Tổng công hưởng lương'
    ],
    'table_column' => [
        'date' => 'Ngày số',
        'timekeeping' => 'Chấm công',
    ],
    'symbol-of-timesheets' => [
        'title' => 'Ký hiệu bảng chấm công',
        'working-day' => 'Công đi làm',
        'annual-leave' => 'Nghỉ phép năm',
        'paid-leave' => 'Nghỉ phép có hưởng lương',
        'holiday' => 'Nghỉ ngày lễ, tết...',
        'meetings' => 'Hội họp',
        'paid-personal-leave' => 'Nghỉ việc riêng có hưởng lương',
        'unpaid-personal-leave' => 'Nghỉ việc riêng không hưởng lương',
        'sick-leave' => 'Nghỉ ốm',
        'maternity-leave' => 'Nghỉ thai sản',
        'leave-without-reason' => 'Nghỉ không lý do',
        'labor-accident' => 'Tai nạn lao động',
    ],
];