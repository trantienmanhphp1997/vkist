<?php


namespace App\Enums;


class EContractStatus
{
    const ACTIVE = 1;
    const OFF = -1;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::ACTIVE:
                $result = __('executive/contract.status.active');
                break;
            case self::OFF:
                $result = __('executive/contract.status.off');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
