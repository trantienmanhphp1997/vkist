<?php

namespace Tests\Unit;

use App\Http\Livewire\Admin\News\FormData;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Tests\TestCase;

class NewsControllerTest extends TestCase
{
    use WithFaker;

    public function testNewsIndex()
    {
        $this->actingAsSuperAdmin()->get(route('admin.news.posts.index'))->assertOk();
        $this->actingAsSuperAdmin()->get(route('admin.news.approved'))->assertOk();
        $this->actingAsSuperAdmin()->get(route('admin.news.category.index'))->assertOk();
    }

    public function testNewsCreateSuccess()
    {
        $this->actingAsSuperAdmin()->get(route('admin.news.posts.create'))->assertOk();

    }
}
