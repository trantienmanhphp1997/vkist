<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;
use App\Models\UserResearchLicense;
use App\Component\Recursive;
use Illuminate\Support\Facades\Config;
use Route;

class TabTable3 extends Component
{
    public $user_info_id;
    public $name;
    public $license;
    public $number_license;
    public $author_name;
    public $date_created;
    public $updateMode = false;
    public $editReTable3='';
    public $deleteId ='';
    public $canEdit = true;

    protected $rule =[
        'name' => 'required|max:100',
        'license' => 'required|max:5',
        'number_license' => 'required|max:12',
        'date_created' => 'required',
    ];
    public function render()

    {
        $data3 =  UserResearchLicense::where('user_info_id',$this->user_info_id)
                    ->orderBy('user_research_license.id','desc')->get();
        $model_name = UserResearchLicense::class;
        $type = Config::get('common.type_upload.UserResearchLicense');
        $model_id = $this->user_info_id;
        $folder = app($model_name)->getTable();
        if(checkShowMode()) {
            $this->canEdit = false;
        }

        return view('livewire.admin.user.tab-table3',['data3'=>$data3, 'model_name'=>$model_name, 'type'=>$type, 'model_id'=>$model_id, 'folder'=>$folder]);
    }
    public function store(){
        $this->validate([
            'name' => 'required|max:100',
            'license' => 'required|max:5',
            'number_license' => 'required|max:12',
            'date_created' => 'required',
        ],[],[

            'name'=>__('data_field_name.working_process.solution_name'),
            'license'=>__('data_field_name.working_process.degree_number'),
            'number_license'=>__('data_field_name.working_process.number_order'),
            'date_created'=>__('data_field_name.working_process.announced_year'),

        ]);

        if($this->editReTable3){
            UserResearchLicense::findOrFail($this->editReTable3)->update([
                'user_info_id'=>$this->user_info_id,
                'name'=>$this->name,
                'license'=>$this->license,
                'number_license'=>$this->number_license,
                'author_name'=>$this->author_name,
                'date_created'=>$this->date_created,
            ]);
            $this->resetInputFields3();
            $this->emit('userStore');
            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.update')]);
        }else{
            UserResearchLicense::create([
                'user_info_id'=>$this->user_info_id,
                'name'=>$this->name,
                'license'=>$this->license,
                'number_license'=>$this->number_license,
                'author_name'=>$this->author_name,
                'date_created'=>$this->date_created,
            ]);
            $this->resetInputFields3();
            $this->emit('userStore');
            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.add')]);
        }

    }


    public function editReTable3($id){
        $this->editReTable3 = $id;
        $data=UserResearchLicense::findOrFail($this->editReTable3);
        $this->name=$data->name;
        $this->license=$data->license;
        $this->number_license=$data->number_license;
        $this->author_name=$data->author_name;
        $this->date_created = $data->date_created;
    }

    public function getIdDeleteTable3($id){
        $this->deleteId = $id;
    }
    public function deleteReTable3(){
        $data = UserResearchLicense::findOrFail($this->deleteId);
        $data->delete();
        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.delete')]);
    }
    public function resetInputFields3(){
        $this->name='';
        $this->license='';
        $this->number_license='';
        $this->author_name='';
        $this->date_created='';
    }
}
