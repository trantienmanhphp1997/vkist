@extends('layouts.master')
<title>{{__('menu_management.menu_name.research_field_management')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
@livewire('admin.research.category.category-field-list')
@endsection