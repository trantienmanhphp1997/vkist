@extends('layouts.master')
@section('homeLeft')
    @include('layouts.partials.sliderbar._assetLeft')
@endsection
@section('content')
    @livewire('admin.asset.instrument.asset-instrument-list')
@endsection
