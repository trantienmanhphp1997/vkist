<?php

namespace App\Http\Controllers\Admin\User;

use App\Component\DepartmentRecursive;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\UserInf;
use App\Models\Department;
use App\Models\MasterData;
use App\Imports\UserInfImport;
use App\Models\User;
use App\Notifications\Handlers\NotificationHandler;
use App\Notifications\NewUserInfoNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Excel;
use App\Enums\ETypeUpload;

class UserInfController extends Controller
{

    public function getMasterData()
    {
        $output = [
            "positions" => [],
            "ethnics" => [],
            "religions" => [],
            "nationalities" => [],
            "academic_levels" => [],
            "classifications" => [],
        ];

        $master_data = MasterData::all();
        foreach ($master_data as $data) {
            switch ($data->type) {
                case 1:
                    $output['positions'][$data->id] = $data->v_value;
                    break;
                case 2:
                    $output['ethnics'][$data->id] = $data->v_value;
                    break;
                case 3:
                    $output['religions'][$data->id] = $data->v_value;
                    break;
                case 4:
                    $output['nationalities'][$data->id] = $data->v_value;
                    break;
                case 5:
                    $output['academic_levels'][$data->id] = $data->v_value;
                    break;
                case 6:
                    $output['classifications'][$data->id] = $data->v_value;
                    break;
                default:
                    break;
            }
        }

        return $output;
    }

    public function create()
    {
        $master_data = $this->getMasterData();
        $data = new UserInf();
        $isEdit = false;
        $data_department = $this->getDepartment();
        $model_name = UserInf::class;
        app($model_name)->getTable();

        return view('admin.user.create', ['master_data'=>$master_data,'data'=>$data,'data_department'=>$data_department,'isEdit'=>$isEdit]);
    }
    public function store(Request $request)
    {
        $this->validateData($request);
        $userInf = new UserInf;
        $userInf->fullname = $request->fullname;
        $userInf->code = $request->code;
        $userInf->address = $request->address;
        $userInf->phone = $request->phone;
        $userInf->married = $request->married;
        $userInf->birthday = Carbon::createFromFormat(config('common.formatDate'), $request->birthday)->format('Y-m-d');
        $userInf->gender = $request->gender;
        $userInf->email = $request->email;
        $userInf->ethnic_id = $request->ethnic_id;
        $userInf->religion_id = $request->religion_id;
        $userInf->nationality_id = $request->nationality_id;
        $userInf->id_card = $request->id_card;
        $userInf->issued_date =Carbon::createFromFormat(config('common.formatDate'), $request->issued_date)->format('Y-m-d');
        $userInf->issued_place = $request->issued_place;
        $userInf->passport_number = $request->passport_number;
        $userInf->passport_date = Carbon::createFromFormat(config('common.formatDate'), $request->passport_date)->format('Y-m-d');
        $userInf->passport_place = $request->passport_place;
        $userInf->note = $request->note;
        $userInf->department_id = $request->department_id;
        $userInf->position_id = $request->position_id;

        if ($request->hasFile('avatar') && !empty($request->file('avatar'))) {
            $userInf->avatar = $this->uploadAvatar($request->file('avatar'));
        }

        $userInf->save();

        $this->updateUserWorkingProcess($userInf);
        $this->updateDepartmentLeader($userInf->department_id);

        NotificationHandler::sendNotification(User::all(), new NewUserInfoNotification($userInf));

        return redirect()->route('admin.user.index')->with('success', __('notification.common.success.add'));
    }

    public function edit($id)
    {
        $master_data = $this->getMasterData();
        $data = UserInf::findOrFail($id);
        $data_department = $this->getDepartment();
        $model_name = UserInf::class;
        $isEdit = true;
        $type = ETypeUpload::USER_WORKING_PROCESS;
        $folder = app($model_name)->getTable();
        return view('admin.user.edit', ['master_data'=>$master_data,'data'=>$data,'data_department'=>$data_department,'isEdit'=>$isEdit,'model_name' => $model_name, 'type'=>$type, 'folder' => $folder]);

    }


    public function show($id)
    {
        $master_data = $this->getMasterData();
        $data = UserInf::findOrFail($id);
        $isEdit = false;
        $model_name = UserInf::class;
        $type = ETypeUpload::USER_WORKING_PROCESS;
        $folder = app($model_name)->getTable();
        $data_department = $this->getDepartment();
        return view('admin.user.edit', compact('master_data', 'data', 'data_department','isEdit','type','folder','model_name'));
    }

    public function update(Request $request, $id)
    {
        $this->validateData($request, $id);
        $userInf = UserInf::findOrFail($id);
        $userInf->fullname = $request->fullname;
        $userInf->code = $request->code;
        $userInf->address = $request->address;
        $userInf->phone = $request->phone;
        $userInf->married = $request->married;
        $userInf->birthday = Carbon::createFromFormat(config('common.formatDate'), $request->birthday)->format('Y-m-d');
        $userInf->gender = $request->gender;
        $userInf->email = $request->email;
        $userInf->ethnic_id = $request->ethnic_id;
        $userInf->religion_id = $request->religion_id;
        $userInf->nationality_id = $request->nationality_id;
        $userInf->id_card = $request->id_card;
        $userInf->issued_date =Carbon::createFromFormat(config('common.formatDate'), $request->issued_date)->format('Y-m-d');
        $userInf->issued_place = $request->issued_place;
        $userInf->passport_number = $request->passport_number;
        $userInf->passport_date = Carbon::createFromFormat(config('common.formatDate'), $request->passport_date)->format('Y-m-d');
        $userInf->passport_place = $request->passport_place;
        $userInf->note = $request->note;
        $userInf->department_id = $request->department_id;
        $userInf->position_id = $request->position_id;

        $oldDepartmentId = $newDepartmentId = null;

        if ($userInf->isDirty('position_id') || $userInf->isDirty('department_id')) {
            $this->updateUserWorkingProcess($userInf);
            $originals = $userInf->getOriginal();
            $oldDepartmentId = $originals['department_id'];
            $newDepartmentId = $userInf->department_id;
        }
        if ($request->hasFile('avatar') && !empty($request->file('avatar'))) {
            $userInf->avatar = $this->uploadAvatar($request->file('avatar'));
        }

        $userInf->save();
        if ($oldDepartmentId) {
            $this->updateDepartmentLeader($oldDepartmentId);
        }
        if ($newDepartmentId) {
            $this->updateDepartmentLeader($newDepartmentId);
        }

        return redirect()->route('admin.user.index')->with('success', __('notification.common.success.update'));
    }


    public function storeResearch($id)
    {

    }

    public function validateData($request, $id = 0)
    {
        $this->validate($request, [
            'code' => 'max:10|regex:/^[A-Z0-9]+$/u|required|unique:user_info,code,' . $id,
            'fullname' => ['required', 'regex:/^([.a-zA-Z'.config('common.special_character.alphabet').'])+$/', 'max:48'],
            'birthday' => 'required|date_format:' . config('common.formatDate') . '|before_or_equal:' . now()->format('d/m/Y'),
            'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:user_info,email,' . $id,
            'address' => 'required|max:256',
            'issued_date' => 'required|date_format:' . config('common.formatDate') . '|before_or_equal:' . now()->format('d/m/Y'),
            'issued_place' => ['required', 'regex:/[^.][^0-9<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:48'],
            'passport_date' => 'nullable|date_format:' . config('common.formatDate') . '|before_or_equal:' . now()->format('d/m/Y'),
            'department_id' => 'required',
            'position_id' => 'required',
            'id_card' => 'required|max:15|regex:/^[0-9]+$/u|unique:user_info,id_card,'.$id,
            'passport_number' => !empty($request->get('passport_number')) ? 'max:15|regex:/^[A-Z0-9]+$/u' : '',
            'nationality_id' => 'required',
            'phone' => 'required|max:11|regex:/^[0-9]+$/i|unique:user_info,phone,'.$id,
            'avatar' => 'nullable|image',
            'note' => 'max:256',
            'passport_place' => !empty($request->get('passport_number')) ? ['max:48', 'regex:/[^.][^0-9<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:48'] : ''
        ], [], [
            'code' => __('data_field_name.user.code'),
            'fullname' => __('data_field_name.user.full_name'),
            'birthday' => __('data_field_name.user.birth_day'),
            'gender' => __('data_field_name.user.sex'),
            'email' => __('data_field_name.common_field.email'),
            'address' => __('data_field_name.common_field.address'),
            'issued_date' => __('data_field_name.user.create_at'),
            'issued_place' => __('data_field_name.user.area'),
            'passport_date' => __('data_field_name.user.create_at'),
            'department_id' => __('data_field_name.common_field.department'),
            'position_id' => __('data_field_name.common_field.position'),
            'id_card' => __('data_field_name.user.cmnd'),
            'passport_number' => __('data_field_name.user.passport'),
            'nationality_id' => __('data_field_name.user.country'),
            'phone' => __('data_field_name.user.phone'),
            'note' => __('data_field_name.common_field.note'),
            'passport_place' => __('data_field_name.user.area')
        ]);
    }

    public function getDepartment()
    {
        return DepartmentRecursive::hierarch(Department::all());
    }

    public function updateUserWorkingProcess(UserInf $userInf)
    {
        $lastWorkingProcesss = $userInf->workProcesses()->orderBy('id', 'desc')->first();

        if (!empty($lastWorkingProcesss)) {
            $lastWorkingProcesss->update([
                'end_date' => date('Y-m-d')
            ]);
        }

        $data = [
            'start_date' => date('Y-m-d'),
            'position_id' => $userInf->position_id,
            'department_id' => $userInf->department_id
        ];

        $userInf->workProcesses()->create($data);
    }

    protected function uploadAvatar(UploadedFile $file)
    {
        $fileHashName = $file->hashName();
        Storage::put('/uploads/avatars', $file);
        return asset('storage/uploads/avatars/' . $fileHashName);
    }

    protected function updateDepartmentLeader($departmentId)
    {
        $department = Department::query()->findOrFail($departmentId);
        $leader = $department->userInfos()
            ->whereHas('position', function (Builder $query) {
                return $query->where('order_number', 1);
            })
            ->orderBy('updated_at', 'desc')
            ->first();
        $department->leader_id = $leader->id ?? null;
        $department->save();
    }

    public function getImport()
    {
        return view('admin.user.user-info');
    }

    public function import()
    {
        Excel::import(new UserInfImport, request()->file('user_file'));
    }
}
