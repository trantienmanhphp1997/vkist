<?php

namespace App\Http\Livewire\Admin\Asset\AllocatedAndRevoke;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use App\Models\AssetAllocatedRevoke;
use App\Models\Asset;
use App\Models\UserInf;
use DB;
use Illuminate\Support\Carbon;
use App\Models\File;
use App\Component\FileUpload;
use Livewire\WithFileUploads;
use App\Models\ClaimForm;

class Revoke extends BaseLive
{
    use WithFileUploads;

    public $data;
    public $user;
    public $modalUserList = [];
    public $asset;
    public $indexSelected = 0;
    public $deleteSelected;
    public $fileList;
    public $selectedIndex;
    public $isChecked;
    public $disable = false;

    public $name;
    public $assetName;

    public $fileUpload = [];
    public $day;
    public $reason;
    public $number_report;

    public $type;
    public $model_name;
    public $folder;

    public $errorFile;
    public $errorUser;
    public $errorAsset;
    public $errorQuantity;
    public $export_file = false;

    protected $listeners = ['export' => 'export', 'resetData' => 'resetData', 'set-revokeDay' => 'setRevokeDay'];

    public function mount() {
        $this->data = json_decode(base64_decode($this->data));
        $this->day = now()->format(Config::get('common.year-month-day'));
        $this->getDataList();
        $this->isChecked = 0;

    }

    public function render() {
        $this->getUserlist();
        $this->type = Config::get('common.type_upload.Revoke');
        $this->model_name = AssetAllocatedRevoke::class;
        $this->folder = app($this->model_name)->getTable();

        if (!empty($this->fileUpload) && count($this->fileUpload) > 5) {
            $this->errorFile = __('validation.number_of_file.max', [
                'attribute' => __('data_field_name.labor-contract.file'),
                'max' => 5
            ]);
            array_splice($this->fileUpload, 5, 1);
        } elseif (!empty($this->fileUpload)) {
            $this->validateFile();
        }

        return view('livewire.admin.asset.allocated-and-revoke.revoke', ['dataList' => $this->user, 'userList' => $this->modalUserList]);
    }

    public function validateFile() {
        $file = new FileUpload();
        $arr = ['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z'];
        foreach ($this->fileUpload as $index => $item) {
            $dataUpload = $file->uploadFile($item, $this->folder, $this->model_name);
            if (!$item->getSize() > 255 * 1024 * 1024) {
                $this->errorFile = __('common.file_size', [
                    'value' => '255MB'
                ]);
            }
            $tmp = explode('.', $dataUpload['file_name']);
            if (count($tmp) > 0 && !in_array(end($tmp), $arr)) {
                $this->errorFile = __('data_field_name.allocated-revoke.invalid_file');
                array_splice($this->fileUpload, $index, 1);
                break;
            }
        }

    }

    public function getUserlist() {
        $modal_user_list = UserInf::query();
        if (strlen($this->name)) {
            $modal_user_list->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->name)) . '%');
        }
        $modal_user_list = $modal_user_list->limit(10)->get();
        $modal_user_list = $modal_user_list->map(function ($item) {
            $checked = 0;
            foreach ($this->user as $value) {
                if ($item->id == $value['id']) {
                    $checked = 1;
                    break;
                }
            }
            $asset_revoke = Asset::where('asset.user_info_id', $item->id)
                ->select('asset.id', 'asset.code', 'asset.name', 'asset.situation', 'amount_used')
                ->get();

            foreach ($asset_revoke as $value) {
                $value['error'] = null;
                $value['selected'] = 0;
                $value['revoke'] = 1;
            }
            Carbon::parse($this->day);
            $currentUser = UserInf::where('id', $item->id)->first();
            $department = !empty($currentUser->department) ? $currentUser->department->name : null;
            return [
                'id' => $item->id,
                'checked' => $checked,
                'fullname' => $item->fullname,
                'department' => $department,
                'asset' => $asset_revoke,
            ];
        });
        $this->modalUserList = collect($modal_user_list)->all();
    }

    public function getDataList() {
        if (count($this->data) > 0) {
            $user_information = UserInf::whereIn('id', $this->data)->get();
            $user_information = $user_information->map(function ($item) {
                Carbon::parse($this->day);
                $asset_revoke = Asset::where('asset.user_info_id', $item->id)
                    ->select('asset.id', 'asset.code', 'asset.name', 'asset.situation', 'amount_used')
                    ->get();

                foreach ($asset_revoke as $value) {
                    $value['error'] = null;
                    $value['selected'] = 0;
                    $value['revoke'] = 1;
                }
                $currentUser = UserInf::where('id', $item->id)->first();
                $department = !empty($currentUser->department) ? $currentUser->department->name : null;
                return [
                    'id' => $item->id,
                    'code' => $item->code,
                    'fullname' => $item->fullname,
                    'asset' => collect($asset_revoke)->all(),
                    'checked' => 0,
                    'department' => $department
                ];
            });
            $user_information = collect($user_information)->all();
            $this->user = [];
            foreach ($this->data as $index => $id) {
                foreach ($user_information as $item) {
                    if ($id == $item['id']) {
                        array_unshift($this->user, $user_information[$index]);
                    }
                }
            }
        } else {
            $this->user = [];
        }
    }

    public function deleteUser($index) {
        $this->deleteSelected = $index;
        if (count($this->user) == 1) {
           $this->user = [];
        }else {
            array_splice($this->user, $index, 1);
        }
        $this->indexSelected = 0;
    }

    public function openModalUser () {
        $this->emit('showModalUser');
    }

    public function chooseUser($id) {
        foreach ($this->modalUserList as $index => $value) {
            if ($this->modalUserList[$index]['id'] == $id) {
                if (count($this->user) > 0) {
                    foreach ($this->user as $ind => $user) {
                        if ($user['id'] == $id) {
                            array_splice($this->user, $ind, 1);
                            $this->deleteUser($ind);
                            break;
                        } else {
                            array_push($this->user, $this->modalUserList[$index]);
                            break;
                        }
                    }
                } else {
                    array_push($this->user, $this->modalUserList[$index]);
                }
            }
        }
    }

    public function deleteFileUpload($index) {
        array_splice($this->fileUpload, $index, 1);
    }

    public function validateData() {
        if (count($this->user) == 0) {
            $this->errorUser = __('validation.required', [
                'attribute' => __('data_field_name.allocated.employee')
            ]);
        }
        if (count($this->user) > 0) {
            foreach ($this->user as $index => $item) {
                if (count($item['asset']) == 0) {
                    $this->indexSelected = $index;
                    $this->errorAsset = __('validation.required', [
                        'attribute' => __('data_field_name.allocated-revoke.asset')
                    ]);
                } else {
                    $totalAssetChoosed = 0;
                    foreach ($item['asset'] as $ind => $value) {
                        if ($value['selected'] == 1) {
                            $totalAssetChoosed++;
                            if ($value['revoke'] <= 0) {
                                $this->indexSelected = $index;
                                $this->user[$index]['asset'][$ind]['error'] = __('validation.gt.numeric', [
                                    'attribute' => __('data_field_name.allocated-revoke.quantity'),
                                    'value' => 0
                                ]);
                                $this->errorQuantity = __('validation.gt.numeric', [
                                    'attribute' => __('data_field_name.allocated-revoke.quantity'),
                                    'value' => 0
                                ]);
                            }
                            if ((!empty($value['amount_used']) && $value['amount_used'] < $value['revoke']) || empty($value['amount_used'])) {
                                $this->indexSelected = $index;
                                $this->user[$index]['asset'][$ind]['error'] = __('validation.lt.numeric', [
                                    'attribute' => __('data_field_name.allocated-revoke.quantity'),
                                    'value' => __('data_field_name.allocated-revoke.used_quantity')
                                ]);
                                $this->errorQuantity = __('validation.lt.numeric', [
                                    'attribute' => __('data_field_name.allocated-revoke.quantity'),
                                    'value' => __('data_field_name.allocated-revoke.used_quantity')
                                ]);
                            }
                        }
                    }
                    if ($totalAssetChoosed == 0) {
                        $this->errorAsset = __('data_field_name.allocated-revoke.no_asset');
                    }
                }
            }
            $this->disable = false;
        }
        $this->validate([
            'day' => ['required'],
            'reason' => ['max:255'],
            'number_report' => ['required', 'max:10', 'regex:/^[A-Z0-9]+$/u'],
        ],[],[
            'day'=> __('data_field_name.allocated-revoke.allocated_date'),
            'reason'=> __('data_field_name.allocated-revoke.reason'),
            'number_report'=> __('data_field_name.allocated-revoke.number_report'),
            'fileUpload' => 'File'
        ]);
    }

    public function storeFile($id_model) {
        foreach ($this->fileUpload as $item) {
            $file = new FileUpload();
            $dataUpload= $file->uploadFile($item, $this->folder, $this->model_name);
            $file_upload = new File();
            $file_upload->url = $dataUpload['url'];
            $file_upload->size_file = $dataUpload['size_file'];
            $file_upload->file_name = $dataUpload['file_name'];
            $file_upload->model_name = $this->model_name;
            $file_upload->model_id = $id_model;

            $file_upload->type = $this->type;
            $file_upload->save();
        }
    }

    public function saveData($export) {
        $this->errorFile = null;
        $this->errorUser = null;
        $this->errorAsset = null;
        $this->errorQuantity = null;
        $this->disable = true;
        $this->validateData();
        if (empty($this->errorUser) && empty($this->errorAsset) && empty($this->errorQuantity)) {
            return DB::transaction(function() use ($export) {
                $day_revoke = Carbon::parse($this->day);
                $claimForm = new ClaimForm();
                $claimForm->claim_date = $day_revoke;
                $claimForm->reason = $this->reason;
                $claimForm->type = 7;
                $claimForm->save();
                foreach ($this->user as $user) {
                    foreach ($user['asset'] as $asset) {
                        if ($asset['selected'] == 1) {
                            $modelAsset = Asset::findOrFail($asset['id']);
                            $modelAsset->status = 2;
                            $modelAsset->amount_used = $modelAsset->amount_used - $asset['revoke'];
                            if ($modelAsset->amount_used == 0) {
                                $modelAsset->user_info_id = null;
                                $modelAsset->situation = 0;
                            }
                            $modelAsset->save();

                            DB::table('asset_claim_form')->insert(
                                [
                                    'form_id' => $claimForm->id,
                                    'asset_id' => $asset['id'],
                                    'quantity' => $asset['revoke'],
                                    'user_info_id' => $user['id'],
                                ]
                            );

                            $revoke = new AssetAllocatedRevoke();

                            $revoke->asset_id = $asset['id'];
                            $revoke->status = 2;
                            $revoke->implementation_date = $day_revoke;
                            $revoke->used_type = 1;
                            $revoke->number_report = $this->number_report;
                            $revoke->user_info_id = $user['id'];
                            $revoke->implementation_reason = $this->reason;
                            $revoke->implementation_quantity = $asset['revoke'];
                            $revoke->claim_form_id = $claimForm->id;
                            $revoke->save();
                        }
                    }
                }
                $this->storeFile($claimForm->id);
                if ($export) {
                    $this->emit('doExport');
                } else {
                    $this->resetForm();
                    $this->getDataList();
                    session()->flash('success', __('data_field_name.allocated-revoke.success.revoke'));
                    return redirect()->route('admin.asset.allocated-revoke.index');
                }
            });
        }
    }

    public function resetForm() {
        $this->reason = null;
        $this->number_report = null;
        $this->fileUpload = null;
        $this->disable = false;
    }

    public function updateQuantity($userIndex, $assetIndex, $value) {
        $this->user[$userIndex]['asset'][$assetIndex]['revoke'] = $value;
    }

    public function checked($value) {
        $this->isChecked = $value;
        $this->selectedIndex = null;
        foreach ($this->user as $index => $user) {
            foreach ($user['asset'] as $ind => $asset) {
                $this->user[$index]['asset'][$ind]['selected'] = $this->isChecked;
            }
        }
    }

    public function selectedItem($index, $value) {
        $this->selectedIndex = $index;
        $this->isChecked = null;
        if ($value == 0) {
            $this->user[$this->indexSelected]['asset'][$index]['selected'] = 1;
        } else {
            $this->user[$this->indexSelected]['asset'][$index]['selected'] = 0;
        }
    }

    public function export() {
        $data_export = $this->user;
        $this->emit('doResetData');
        $total = 0;
        $value = [];
        foreach ($data_export as $user) {
            $modelUser = UserInf::findOrFail($user['id']);
            if (!empty($modelUser)) {
                $userName = $modelUser->fullname;
                $position = !empty($modelUser->position) ? $modelUser->position->v_value : null;
                $department = !empty($modelUser->department) ? $modelUser->department->name : null;
                $code = $modelUser->code;
            } else {
                $userName =  null;
                $position = null;
                $department = null;
                $code = null;
            }

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template/template_revoke.docx');
            $templateProcessor->setValue('numberReport', $this->number_report);
            $templateProcessor->setValue('day', now()->format(Config::get('common.formatDate')));
            $templateProcessor->setValue('userName', $userName);
            $templateProcessor->setValue('position', $position);
            $templateProcessor->setValue('department', $department);
            $templateProcessor->setValue('userCode', $code);
            $templateProcessor->setValue('reason', $this->reason);
            foreach ($user['asset'] as $ind => $asset) {
                if ($asset['selected'] == 1) {
                    $modelAsset = Asset::findOrFail($asset['id']);
                    $total += $asset['revoke'];
                    $data_export = [
                        'index' => $ind + 1,
                        'assetName' => $modelAsset->name,
                        'assetCode' => $modelAsset->code,
                        'assetSerial' => $modelAsset->series_number,
                        'assetQuantity' => $asset['revoke'],
                        'assetUnitType' => $modelAsset->unit_type,
                        'assetNote' => $modelAsset->note
                    ];
                    array_push($value, $data_export);
                }
            }
        }

        $templateProcessor->cloneRowAndSetValues('index', $value);
        $templateProcessor->setValue('total', $total);
        $today = date("d_m_Y");
        $docxFile = 'revoke_' . $today . '.docx';
        $path  = storage_path("app/uploads/" . $docxFile);
        $templateProcessor->saveAs($path);
        $this->export_file = true;
        return response()->download($path);
    }

    public function resetData() {
        if ($this->export_file) {
            $this->export_file = false;
            session()->flash('success', __('data_field_name.allocated-revoke.success.revoke'));
            return redirect()->route('admin.asset.allocated-revoke.index');
        }
        $this->resetForm();
        $this->getDataList();
    }

    public function setRevokeDay($data) {
        $this->day= date('Y-m-d', strtotime($data['revokeDay']));
    }
}
