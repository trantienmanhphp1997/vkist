<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <a href="">{{__('research/category-topic.breadcrumbs.category-topic-management-1')}} \ 
                        </a><span>{{__('research/category-topic.breadcrumbs.category-topic-management-2')}}</span>
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">
                    {{__('research/category-topic.breadcrumbs.category-topic-list')}} 
                    @if($data->total() === 0) <br> <span>{{__('research/category-topic.title.no-result')}}</span>  
                    @else
                    <br>
                    <span>
                            {{__('research/category-topic.title.part-1')}}  {{count($data)}} {{__('research/category-topic.title.part-2')}} {{$data->total()}}
                    </span>
                    @endif
            </h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group search-expertise">
                                <div class="search-expertise">
                                    <input type="text" placeholder="{{__('research/category-topic.placeholder.search')}}" name="search" class="form-control size13 w-xs-100"
                                           wire:model.debounce.1000ms="searchTerm"
                                           id='input_vn_name' autocomplete="off">
                                    <span><img src="/images/Search.svg" alt="search"/></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            @if($checkCreatePermission)
                            <div class="form-group float-right">
                                <button class="btn btn-viewmore-news mr0" data-toggle="modal" data-target="#createOrEditCategoryTopicModal" 
                                wire:click="resetCategoryInfo">
                                    <img src="/images/plus2.svg" alt="plus"> {{__('research/category-topic.title.create')}}
                                </button>
                            </div>
                            @endif
                        </div>
                        <div class="table-responsive">
                            <table class="table table-general">
                                <thead>
                                <tr class="border-radius">
                                    <th scope="col" style="width:5%" class="border-radius-left">{{__('research/category-topic.table_column.code')}}</th>
                                    <th scope="col" style="width:25%">{{__('research/category-topic.table_column.name')}}</th>
                                    <th scope="col" style="width:30%">{{__('research/category-topic.table_column.description')}}</th>
                                    <th scope="col" style="width:30%">{{__('research/category-topic.table_column.note')}}</th>
                                    <th scope="col" class="text-center" style="width:10%" class="border-radius-right">{{__('research/category-topic.table_column.action')}}</th>
                                </tr>
                                </thead>
                                <div wire:loading class="loader"></div>
                                <tbody>
                                @forelse($data as $row)
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#DetailCategoryTopicModal" title=""
                                            wire:click="getCategoryInfoForUpdateModal({{ $row->id }})">
                                                {{$row->code}}
                                            </a>
                                        </td>
                                        <td>{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                        <td>{!! boldTextSearch($row->description,$searchTerm) !!}</td>
                                        <td>{{$row->note}}</td>
                                        <td class="text-center">
                                            @if($checkEditPermission)
                                            <button title="{{__('common.button.edit')}}" type="button" data-toggle="modal" data-target="#createOrEditCategoryTopicModal" title="Sửa"
                                            wire:click="getCategoryInfoForUpdateModal({{ $row->id }})" class="btn-sm border-0 bg-transparent mr-1">
                                                <img src="/images/edit.png" alt="edit">
                                            </button>
                                            @endif
                                            @if($checkDestroyPermission)
                                            <button title="{{__('common.button.delete')}}" type="button" style="background-color: white; border:none;" class="btn-sm"
                                                    data-toggle="modal" wire:click="deleteId({{$row->id}})" data-target="#exampleDeleteTopic">
                                                <img src="/images/trash.svg" alt="trash">
                                            </button>
                                            @endif
                                        </td>
                                    </tr>
                                    @empty 
                                        <tr class="text-center text-danger"><td colspan="12">{{__('common.message.no_data')}}</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        {{$data->links()}}
                        @include('livewire.common._modalExportFile')
                        <div wire:ignore.self class="modal fade" id="exampleDeleteTopic" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body box-user">
                                        <h4 class="modal-title">{{__('research/category-topic.modal.title.delete')}}</h4>
                                            {{__('research/category-topic.modal.body.delete-warning')}}
                                    </div>
                                    <div class="group-btn2 text-center pt-24">
                                        <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('research/category-topic.modal.footer.back')}}</button>
                                        <button type="button" wire:click.prevent="delete" class="btn btn-save" data-dismiss="modal">{{__('research/category-topic.modal.footer.delete')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Edit -->
                        <form wire:submit.prevent="store()">
                            <div wire:ignore.self class="modal fade modal-fix-1" id="createOrEditCategoryTopicModal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        @if ($updateMode == true) {{__('research/category-topic.modal.title.edit')}}
                                        @else {{__('research/category-topic.modal.title.create')}}
                                        @endif
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true close-btn">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput11">{{__('research/category-topic.modal.body.code')}} <span class="text-danger">(*)</label>
                                        <input type="text" class="form-control p-3" 
                                        placeholder="{{__('research/category-topic.placeholder.code')}}" wire:model.defer="code" @if ($updateMode == true) readonly @endif>
                                        @error('code') <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput11">{{__('research/category-topic.modal.body.name')}} <span class="text-danger">(*)</label>
                                        <input type="text" class="form-control p-3" id="exampleFormControlInput11"
                                        placeholder="{{__('research/category-topic.placeholder.name')}}" wire:model.defer="name">
                                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput12">{{__('research/category-topic.modal.body.description')}}</label><br>
                                        <textarea type="text" class="p-3" name="description" id="" cols="30" rows="5" wire:model.defer="description"
                                        placeholder="{{__('research/category-topic.placeholder.description')}}" style="width:100%;display: flex;flex-direction: row;
                                            align-items: flex-start;position: static;border-radius: 5px;"></textarea>
                                        @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group">
                                            <label for="exampleFormControlInput12">{{__('research/category-topic.modal.body.note')}}</label><br>
                                            <textarea type="text" class="p-3" name="note" id="" cols="30" rows="5" wire:model.defer="note"
                                            placeholder="{{__('research/category-topic.placeholder.note')}}" style="width:100%;display: flex;flex-direction: row;
                                                align-items: flex-start;position: static;border-radius: 5px;"></textarea>
                                            @error('note') <span class="text-danger error">{{ $message }}</span>@enderror
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <div class="group-btn2">
                                            <button type="button" id="close-modal-create-or-edit-category-topic" class="btn btn-cancel close-btn"
                                            data-dismiss="modal">{{__('research/category-topic.modal.footer.close')}}</button>
                                            <button type="button" wire:click.prevent="store()" class="btn btn-save close-modal">{{__('research/category-topic.modal.footer.save')}}</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </form>
                        <!-- End Modal Edit -->

                        <!-- Modal Detail -->
                        <form>
                                <div wire:ignore.self class="modal fade modal-fix-1" id="DetailCategoryTopicModal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                    
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            {{__('research/category-topic.modal.title.detail')}}
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true close-btn">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput11">{{__('research/category-topic.modal.body.code')}} <span class="text-danger">(*)</label>
                                            <input type="text" class="form-control p-3" 
                                            placeholder="{{__('research/category-topic.placeholder.code')}}" wire:model.defer="code" readonly>
                                            @error('code') <span class="text-danger error">{{ $message }}</span>@enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput11">{{__('research/category-topic.modal.body.name')}} <span class="text-danger">(*)</label>
                                            <input type="text" class="form-control p-3" id="exampleFormControlInput11"
                                            placeholder="{{__('research/category-topic.placeholder.name')}}" wire:model.defer="name" readonly>
                                            @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput12">{{__('research/category-topic.modal.body.description')}}</label><br>
                                            <textarea type="text" class="p-3" name="description" id="" cols="30" rows="5" wire:model.defer="description" readonly
                                            placeholder="{{__('research/category-topic.placeholder.description')}}" style="width:100%;display: flex;flex-direction: row;
                                                align-items: flex-start;position: static;border-radius: 5px;"></textarea>
                                            @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                                        </div>
                                        <div class="form-group">
                                                <label for="exampleFormControlInput12">{{__('research/category-topic.modal.body.note')}}</label><br>
                                                <textarea type="text" class="p-3" name="note" id="" cols="30" rows="5" wire:model.defer="note" readonly
                                                placeholder="{{__('research/category-topic.placeholder.note')}}" style="width:100%;display: flex;flex-direction: row;
                                                    align-items: flex-start;position: static;border-radius: 5px;"></textarea>
                                                @error('note') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </form>
                            <!-- End Modal Detail -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function() {
    window.livewire.on('close-modal-create-or-edit-category-topic', () => {
        document.getElementById('close-modal-create-or-edit-category-topic').click()
    });
  })
  </script>


