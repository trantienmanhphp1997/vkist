<div class="col-md-3 col-xl-4 sidebar-right">
    <div class="container-fluid news-container">
        <div class="row">
            <div class="col-md-12" style="padding-left: 0">
                <h4 class="sidebar-news-right-title-page" style="font-size: 22px !important;">
                    {{__("news/newsManager.menu_name.popular-news")}}
                </h4>
            </div>
        </div>
        @foreach($news as $key => $new)
            <a class="row trending-new box-new"
                 href="{{route('news.detail',['slug'=>$new->slug,'categorySlug'=>$new->category->slug ?? "unkhown"])}}">
                <div class="col-md-12" style="padding: 20px 10px 0 0">
                    <img style="    width: 100%;border-radius: 8px  ;  height: 180px;" src="{{trim($new->image_path) ? asset('storage/'.$new->image_path) : asset('images/img.jpg')}}" alt="">
                </div>
                <div class="date sidebar-news-right-date-detail" style="margin: 14px 0 0">
                    <img src="{{asset('images/date.svg')}}" alt="date">
                    {{formatDateLocale($new->created_at,"%a , %e/%m/%Y")}}
                </div>
                <div class="col-md-12" style="padding-left: 0">
                    <h6 class="sidebar-news-right-title-new">
                        {{  strLimit($new->name,50)}}
                    </h6>
                </div>
            </a>

        @endforeach
    </div>
</div>
