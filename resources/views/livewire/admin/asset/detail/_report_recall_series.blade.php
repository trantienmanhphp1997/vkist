<div wire:ignore.self class="modal fade modal-fix-1" id="report_recall_series" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.asset.report_recall')}}</h6>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container-fluid">
                        <div class="row justify-content-md-center">
                            <div class="row col-12 justify-content-md-center" style="height:100px;z-index: 3">
                            <div class="col-3 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.full_name')}}<span class="text-danger">*</span></label>
                                    @livewire('component.search-user',['name'=>'receiver_id'])
                                    @error('receiver_id')
                                    <span class="error text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-3 pd-col">
                                <div class="form-group info-request">
                                    <label> {{__('data_field_name.asset.user_info_code')}}</label>
                                    <input wire:model.lazy="receiver_code" type="text" disabled>
                                </div>
                            </div>
                            <div class="col-3 pd-col">
                                <div class="form-group info-request">
                                    <label> {{__('data_field_name.asset.recall_date')}}<span class="text-danger">*</span></label>
                                    <input type="date" wire:model.lazy="claim_date">
                                    @error('claim_date')
                                    <span class="error text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            </div>
                            <div class="col-9 pd-col">
                                <div class="form-group info-request">
                                    <label> {{__('data_field_name.asset.recall_info')}}</label>
                                    <textarea wire:model.lazy="reason" cols="30" rows="10"></textarea>
                                    @error('reason')
                                    <span class="error text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div class="info-asset bg-white p-0">
                                    <table class="table table-general">
                                        <thead>
                                        <tr class="border-radius">
                                            <th scope="col" class="border-radius-left text-left w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_code_')}}</th>
                                            <th scope="col" class="w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_name')}}</th>
                                            <th scope="col" class="w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_cate')}}</th>
                                            <th scope="col" class="w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_quantity')}}</th>
                                            <th scope="col" class="w-15 ts-11 text-uppercase">{{__('data_field_name.asset.department_management')}}</th>
                                            <th scope="col" class="w-15 ts-11 text-uppercase">{{__('data_field_name.asset.user_asset')}}</th>
                                            <th scope="col" class="w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_position')}}</th>
                                            <th scope="col" class="border-radius-right text-center w-10 ts-11 text-uppercase">{{__('data_field_name.asset.situation')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($dataForms as $dataForm)
                                            <tr>
                                                <td>{{$dataForm?$dataForm->code:''}}</td>
                                                <td>{{$dataForm?$dataForm->name:''}}</td>
                                                <td>{{$dataForm?$dataForm->code:''}}</td>
                                                <td>{{$dataForm?$dataForm->quantity:''}}</td>
                                                <td>{{$dataForm?$dataForm->code:''}}</td>
                                                <td>{{$dataForm?$dataForm->code:''}}</td>
                                                <td>{{$dataForm?$dataForm->code:''}}</td>
                                                <td><span class="badge badge-fix badge-color-1">
                                                        {{$assetStatus[$dataForm?$dataForm->situation:-1]??''}}
                                                </span>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" id="close-recall-series" data-dismiss="modal"  wire:click.prevent="cancel()">{{__('common.button.cancel')}}</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="saveForm(6,false)">{{__('common.button.perform')}}</button>
            </div>
        </div>
    </div>
</div>
