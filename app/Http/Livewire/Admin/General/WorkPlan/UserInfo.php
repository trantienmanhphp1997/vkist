<?php

namespace App\Http\Livewire\Admin\General\WorkPlan;


use App\Http\Livewire\Base\BaseLive;

use App\Models\UserInf;
use App\Models\WorkPlanHasSchedule;
use App\Models\WorkPlanHasUserInfo;
use Livewire\Component;

class UserInfo extends BaseLive
{
    public $work_plan_id;
    public $user_info_id;
    public $working_day;
    public $type;
    public $content;

    protected $listeners = [
        'set-working_date' => 'setWorkingDate',
    ];

    public function setWorkingDate($data) {
        $this->working_day = $data['working_date'] ? date('Y-m-d', strtotime($data['working_date'])) : null;
    }

    public function resetInput()
    {
        $this->user_info_id = '';
        $this->working_day = '';
        $this->content = '';
        $this->type = '';
    }

    public function render()
    {


        $user_info = WorkPlanHasSchedule::where('work_plan_id', $this->work_plan_id)
            ->get();

        $list_user = WorkPlanHasUserInfo::where('work_plan_id', $this->work_plan_id)
            ->get();

        $output = [
            'user_info' => $user_info,
            'list_user'=>$list_user
        ];
        return view('livewire.admin.general.work-plan.user-info', $output);
    }

    public function storeUserInfo()
    {

        $this->validate([
            'content' => 'max:255',
        ], [],
        [
            'content' => __('data_field_name.work_plan.edit.content'),
        ]);

        WorkPlanHasSchedule::create([
            'work_plan_id' => $this->work_plan_id,
            'user_info_id' => $this->user_info_id,
            'working_day' => $this->working_day,
            'status' => 0,
            'type' => $this->type,
            'content' => $this->content,
        ]);
        $this->resetInput();
    }
}
