@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.budget.plan_budget')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    @livewire('admin.budget.change-budget',['data' => $data, 'model_name' => $model_name,
    'type' => $type, 'folder' => $folder,'total_draft'=>$total_draft,'money_plan_draft'=>$money_plan_draft,
    'requestId' => $requestId ])
@endsection
