<div class="col-md-12">
    <div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="title-plan text-uppercase">{{__('data_field_name.work_plan.edit.work_sche')}}</h4>
                </div>
                <div class="col-md-6">
                    <div class="form-group float-right">
                        <button type="button" class="btn btn-viewmore-news mr0 " onclick="$('#working_date').val('')" wire:click.prevent="storeUserInfo()">
                            <img src="/images/plus2.svg" alt="plus"> {{__('data_field_name.work_plan.create_btn')}}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-calendar col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.work_plan.create.name')}}</label>
                        <select class="form-control form-control-lg" wire:model.lazy="user_info_id">
                            <option value="">{{__('data_field_name.work_plan.create.selected_default')}}</option>
                            @foreach($list_user as $key => $val)
                                <option value="{{$val->user_info_id}}">{{$val->userInfo->fullname}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.work_plan.edit.date')}}</label>
                        @include('layouts.partials.input._inputDate', ['input_id' => 'working_date','place_holder'=>'dd/mm/yyyy', 'set_null_when_enter_invalid_date' => true])
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.work_plan.edit.type')}}</label>
                        <select class="form-control form-control-lg" wire:model.lazy="type">
                            <option value="">-- --</option>
                            <option value="1">{{__('data_field_name.work_plan.edit.morning')}}</option>
                            <option value="2">{{__('data_field_name.work_plan.edit.afternoon')}}</option>
                            <option value="3">{{__('data_field_name.work_plan.edit.night')}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{__('data_field_name.common_field.content')}}</label>
                        <input type="text" class="form-control" placeholder="" wire:model.lazy="content">
                        @error('content') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                </div>
            </div>
        </div>
    </div>
    @forelse($user_info as $item)
        <div class="box-calendar col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.work_plan.create.name')}}</label>
                        <input type="text" class="form-control" placeholder="" value="{{$item->userInfo->fullname ??''}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.work_plan.edit.date')}}</label>
                        <input type="text" class="form-control" disabled
                               value="{{$item->working_day ? date('d/m/Y',strtotime($item->working_day)):''}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-group">
                            <label>{{__('data_field_name.work_plan.edit.type')}}</label>
                            <select class="form-control form-control-lg" disabled>
                                <option
                                    value="1" {{(1==$item->type) ?'selected' :''}}>{{__('data_field_name.work_plan.edit.morning')}}</option>
                                <option
                                    value="2" {{(2==$item->type) ?'selected' :''}}>{{__('data_field_name.work_plan.edit.afternoon')}}</option>
                                <option
                                    value="3" {{(3==$item->type) ?'selected' :''}}>{{__('data_field_name.work_plan.edit.night')}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{__('data_field_name.common_field.content')}}</label>
                        <input type="text" class="form-control" value="{{$item->content}}" disabled>
                    </div>
                </div>
            </div>
        </div>
    @empty
    @endforelse
</div>
