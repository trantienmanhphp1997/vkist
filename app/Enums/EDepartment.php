<?php

namespace App\Enums;

class EDepartment
{
    const TYPE_CLASS = 1;
    const TYPE_GROUP = 2;
    const TYPE_UNIT = 3;

    public static function getTypeList()
    {
        return [
            static::TYPE_CLASS => __('data_field_name.department.type_class'),
            static::TYPE_GROUP => __('data_field_name.department.type_group'),
            static::TYPE_UNIT => __('data_field_name.department.type_unit'),
        ];
    }

    public static function valueToTypeName($value)
    {
        $types = static::getTypeList();
        return ($types[$value] ?? $value);
    }
}
