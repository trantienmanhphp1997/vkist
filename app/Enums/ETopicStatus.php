<?php


namespace App\Enums;


class ETopicStatus {
    const STATUS_DRAFT = 0;

    const STATUS_NOT_SUBMIT = 1;
    const STATUS_SUBMIT = 2;
    const STATUS_COMPLETE_PROFILE = 3;
    const STATUS_INVALID_PROFILE = 4;
    const STATUS_WAIT_ASSESSMENT = 5;
    const STATUS_WAIT_APPROVAL = 6;
    const STATUS_REJECT = 7;
    const STATUS_APPROVAL = 8;

    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            self::STATUS_NOT_SUBMIT => __('data_field_name.topic.status_not_submit'),
            self::STATUS_SUBMIT => __('data_field_name.topic.status_submited'),
            self::STATUS_COMPLETE_PROFILE => __('data_field_name.topic.status_completed_file'),
            self::STATUS_INVALID_PROFILE => __('data_field_name.topic.status_invalid_file'),
            self::STATUS_WAIT_ASSESSMENT => __('data_field_name.topic.status_wait_validation'),
            self::STATUS_WAIT_APPROVAL => __('data_field_name.topic.status_wait_approval'),
            self::STATUS_REJECT => __('data_field_name.topic.status_rejected'),
            self::STATUS_APPROVAL => __('data_field_name.topic.status_approved'),
        ];
    }
}

