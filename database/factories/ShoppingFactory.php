<?php

namespace Database\Factories;

use App\Enums\EShoppingStatus;
use App\Models\Shopping;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShoppingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Shopping::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->domainWord,
            'note' => $this->faker->sentence,
            'estimate_money' => $this->faker->numberBetween(1000, 9999),
            'needed_date' => $this->faker->dateTimeBetween('-1 year', '+1 year'),
            'status' => EShoppingStatus::NOT_APPROVED,
            'budget_id' => null,
            'admin_id' => 1,
            'proposer_id' => 1,
            'executor_id' => null
        ];
    }
}
