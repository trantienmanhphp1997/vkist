<?php

namespace App\Models;
use App\Models\BaseModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Department extends BaseModel
{
    use HasFactory;
    protected $table="department";
    public $timestamps = false;
    public $autoincrement = true;
    protected $fillable = [
        'name',
        'note',
        'status',
        'parent_id',
        'admin_id',
        'unsign_text',
        'code',
    ];

    public function parentDepartment() {
        return $this->belongsTo(Department::class, 'parent_id');
    }

    public function childrenDepartment() {
        return $this->hasMany(Department::class, 'parent_id');
    }

    public function users(){
        return $this->hasMany(UserInf::class,'department_id') ;
    }
    private static $searchable = [
        'name',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }

    public function leader() {
        return $this->belongsTo(UserInf::class, 'leader_id');
    }

    public function userInfos() {
        return $this->hasMany(UserInf::class, 'department_id');
    }

}
