<?php

namespace App\Http\Livewire\Admin\Executive\Form;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use App\Models\TemplateForm;
use App\Enums\ETemplateFormType;
use Illuminate\Support\Facades\Config;
use App\Component\FileUpload;
use App\Models\File;
use App\Exports\ErrorFileExport;
use Excel;
use App\Http\Livewire\Base\BaseLive;

class ListData extends BaseLive
{
	use WithFileUploads;

	public $searchTerm;
    public $filterType;

    public $name;
    public $formType;
    public $fileUpload;
    public $idShow;
    public $listFile;
    public $deleteId;

    public $type;
    public $model_name;
    public $folder;
    public $checkCreatePermission;
    public $checkDestroyPermission;
    
    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render()
    {
        if (!empty($this->fileUpload)) {
            $this->listFile = null;
        }
        $this->type = Config::get('common.type_upload.TemplateForm');
        $this->model_name = TemplateForm::class;
        $this->folder = app($this->model_name)->getTable();
        $query = TemplateForm::query();
        if (strlen($this->searchTerm)) {
            $query->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }

        if (!empty($this->filterType)) {
            $query->where('type', $this->filterType);
        }
        $data = $query->orderBy('id','DESC')->paginate($this->perPage);
        foreach($data as $item) {
            $item->file = File::where('model_id', $item['id'])
                ->where('model_name', $this->model_name)
                ->where('type', $this->type)
                ->first();
        }
        return view('livewire.admin.executive.form.list-data', ['data' => $data]);
    }

    public function initData($item = null) {
        if (!empty($item)) {
            $this->name = $item['name'];
            $this->formType = $item['type'];
            $this->idShow = $item['id'];
            $this->listFile = File::where('model_id', $item['id'])
                ->where('model_name', $this->model_name)
                ->where('type', $this->type)
                ->first();
            $this->validateData();
        } else {
            $this->name = null;
            $this->formType = null;
            $this->fileUpload = null;
            $this->idShow = null;
            $this->listFile = null;
        }
        
    }
    public function saveData() {
        $this->validateData();
        return DB::transaction(function() {
            if (!empty($this->idShow)) {
                $template = TemplateForm::findOrFail($this->idShow);
            } else {
                $template = new TemplateForm();
            }
            $template->name = $this->name;
            $template->type = $this->formType;
            $template->save();

            if (!empty($this->fileUpload)) {
                $file = new FileUpload();
                $dataUpload= $file->uploadFile($this->fileUpload, $this->folder, $this->model_name);
                if (!empty($this->idShow)) {
                    $file_upload = File::where('model_name', $this->model_name)
                        ->where('type', $this->type)
                        ->where('model_id', $template->id)
                        ->first();
                } else {
                    $file_upload = new File();
                }
                $file_upload->url = $dataUpload['url'];
                $file_upload->size_file = $dataUpload['size_file'];
                $file_upload->file_name = $dataUpload['file_name'];
                $file_upload->model_name = $this->model_name;
                $file_upload->model_id = $template->id;

                $file_upload->type = $this->type;
                $file_upload->save();
            }
            if (!empty($this->idShow)) {
                    $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')]);
                } else {
                    $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);
                }
                $this->emit('closeModalCreate');
        });
    }
    public function validateData() {
        $this->validate([
            'name' => ['required', 'max:50', 'regex:/[^.][^<>!@#$%-+^&*(){}\[\]\'\"\/\|]+$/'],
            'formType' => ['required'],
            'fileUpload' => !empty($this->listFile) ? '' : ['required'],
        ],[],[
            'name' => __('data_field_name.template_form.name'),
            'formType' => __('data_field_name.template_form.type'),
            'fileUpload' => __('data_field_name.template_form.file'),
        ]);
    }
    public function deleteFile() {
        $this->fileUpload = null;
    }

    public function deleteFileEdit() {
        $this->listFile = null;
    }

    public function delete() {
        $template = TemplateForm::findOrFail($this->deleteId);
        if (!empty($template)) {
            $template->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
        }
    }
}
