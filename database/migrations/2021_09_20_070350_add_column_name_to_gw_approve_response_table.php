<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnNameToGwApproveResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gw_approve_response', function (Blueprint $table) {
            $table->string('name', 500)->nullable()->comment('Ten yeu cau phe duyet');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gw_approve_response', function (Blueprint $table) {
            $table->dropColumn('name');
        });
    }
}
