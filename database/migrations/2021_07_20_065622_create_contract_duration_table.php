<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractDurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_duration', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('chi muc');
            $table->string('name', 500)->comment('Ten thoi han');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->tinyInteger('status')->comment('1 show, 0 hidden');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_duration');
    }
}
