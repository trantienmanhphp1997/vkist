<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <div  class="breadcrumbs"><a href="{{route('admin.asset.index')}}">{{__('menu_management.menu_name.asset-management')}}</a> \ <span>{{__('menu_management.menu_name.asset-provider')}}</span></div> 
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">
                {{__('asset/provider.title.create')}}
            </h3>
        </div>
        <div class="col-md-12">
            <h4>
                {{__('asset/provider.form.general_info')}}
            </h4>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{__('asset/provider.label.name')}}</label>
                        <input type="text" class="form-control" placeholder="" name="name" wire:model.defer="name">
                        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('asset/provider.label.code')}}</label>
                        <input type="text" class="form-control" placeholder="" name="code" wire:model.defer="code">
                        @error('code') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('asset/provider.label.address')}}</label>
                        <input type="text" class="form-control" placeholder="" name="address" wire:model.defer="address">
                        @error('address') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('asset/provider.label.tax_code')}}</label>
                        <input type="text" class="form-control" placeholder="" name="tax_code" wire:model.defer="tax_code">
                        @error('tax_code') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('asset/provider.label.phone_number')}}</label>
                        <input type="text" class="form-control" placeholder="" name="phone_number" wire:model.defer="phone_number">
                        @error('phone_number') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                            <label>{{__('asset/provider.label.website')}}</label>
                            <input type="text" class="form-control" placeholder="" name="website" wire:model.defer="website">
                            @error('website') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('asset/provider.label.bank_account_number')}}</label>
                        <input type="text" class="form-control" placeholder="" name="bank_account_number" wire:model.defer="bank_account_number">
                        @error('bank_account_number') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('asset/provider.label.bank_name_id')}}</label>
                        <select  wire:model.defer="bank_name_id" class="form-control">
                                @foreach($list_bank_info as $item)
                                    <option value="{{$item->id}}">{{$item->v_value}}</option>
                                @endforeach
                        </select>
                        @error('bank_name_id')
                            <div class="text-danger mt-1">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('asset/provider.label.email')}}</label>
                        <input type="text" class="form-control" placeholder="" name="email" wire:model.defer="email">
                        @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('asset/provider.label.note')}}</label>
                        <textarea type="text" name="note" class="form-control" placeholder="" cols="30" rows="3" wire:model.defer="note"> </textarea>
                    </div>
                </div>
            </div>                    
        </div>

        <div class="col-md-12">
            <div class="d-flex justify-content-end">
                <button type="button" id="btn-save-asset-provider" wire:click="store" class="btn btn-primary mr-2">
                    {{__('common.button.save')}}
                </button>
                <button type="button" class="btn btn-secondary">
                    <a style="color:white" href="{{route('admin.asset.provider.index')}}">
                        {{__('common.button.cancel')}}
                    </a>
                </button>
            </div>
        </div>
    </div>

    
</div>


