<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnOfDecisionReward extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('decision_reward', function (Blueprint $table) {
            $table->dropColumn('type_of_receiver');
            $table->tinyInteger('type_of_decision')->nullable()->comment('Loai quyet dinh. 1 : khen thuong, 2: ky luat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('decision_reward', function (Blueprint $table) {
            $table->tinyInteger('type_of_receiver')->comment('Doi tuong khen thuong. 1 : ca nhan, 2: to chuc');
            $table->dropColumn('type_of_decision');
        });
    }
}
