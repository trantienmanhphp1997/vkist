<div class=" tab-pane" id="distribute" wire:ignore.self>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.info_dis')}}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.value_asset_distribute')}}</label>
                    <input type="text" class="form-control format_number" placeholder="" wire:model="value_origin" readonly>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.number_distribute_year')}}</label>
                    <input type="number" class="form-control" placeholder=""
                           wire:model.lazy="distribute_value" maxlength="2">
                    @error('distribute_value') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.number_distribute_month')}}</label>
                    <input type="text" class="form-control" placeholder="" wire:model="number_distribute_month" readonly>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.distribute_start')}}</label>
                    @include('layouts.partials.input._inputDate', ['input_id' => 'distribute_start_date', 'place_holder'=>'dd/mm/yyyy','default_date' => is_null($distribute_start_date)? date('Y-m-d'):$distribute_start_date])
                </div>

                <div class="form-group">
                    <label>{{__('data_field_name.asset.depreciation_year')}}</label>
                    <input type="text" class="form-control" placeholder="" wire:model="depreciation_year" readonly>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.depreciation_month')}}</label>
                    <input type="text" class="form-control" placeholder="" wire:model="depreciation_month" readonly>
                </div>
            </div>
        </div>
    </div>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.depreciation_value')}}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.depreciation_value_previous')}}</label>
                    <input type="text" class="form-control format_number" placeholder="" wire:model="depreciation_value_previous" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.depreciation_value_left')}}</label>
                    <input type="text" class="form-control" placeholder="" wire:model="depreciation_value_left" readonly>
                </div>
            </div>
        </div>
    </div>

</div>

