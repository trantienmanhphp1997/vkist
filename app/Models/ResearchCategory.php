<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;


class ResearchCategory extends BaseModel 
{
    use HasFactory;
    public $timestamps = false;
    protected $table = "research_category";
    protected $guarded=['*'];
    protected $fillable = [
        'name',
        'description',
        'note',
        'type',
    ];

    private static $searchable = [
        'name', 'description'
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }

    public function ideals() {
        return $this->hasMany(Ideal::class, 'research_field_id');
    }
}
