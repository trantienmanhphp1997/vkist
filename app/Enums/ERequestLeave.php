<?php


namespace App\Enums;


class ERequestLeave {
    const STATUS_NEW = 1;
    const STATUS_WAIT_APPROVAL = 2;
    const STATUS_REJECT = 3;
    const STATUS_APPROVAL = 4;

    public static function getListStatus() {
        return [
            self::STATUS_WAIT_APPROVAL => __('executive/my-day-off.waiting_for_approval'),
            self::STATUS_REJECT => __('executive/my-day-off.reject_approval'),
            self::STATUS_APPROVAL => __('executive/my-day-off.approved'),
            self::STATUS_NEW => __('executive/my-day-off.new_approval'),
        ];
    }
}
