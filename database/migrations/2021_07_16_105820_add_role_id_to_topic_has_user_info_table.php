<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRoleIdToTopicHasUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_has_user_info', function (Blueprint $table) {
            $table->bigInteger('role_id')->comment('map voi id master_data, type = 13');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_has_user_info', function (Blueprint $table) {
            $table->dropColumn('role_id');
        });
    }
}
