<div wire:ignore.self class="modal fade" id="createModal1" role="dialog" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: -90px">
            <div class="modal-header" style="border-bottom:0px ">
                <h4 class="modal-title" id="exampleModalLabel">{{__('data_field_name.asset_inventory.title_add')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        wire:click.prevent="resetInput()">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 pd-col">
                            <div class="form-group info-request">
                                <label>{{__('data_field_name.asset_inventory.minutes_name')}}<span
                                        class="text-danger">(*)</span></label>
                                <input type="text" wire:model.lazy="name">
                                @error('name') <span class="text-danger"
                                                     style="font-size: 14px">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group info-request">
                                <label>{{__('data_field_name.asset_inventory.department_execute')}}<span
                                        class="text-danger ">(*)</span></label>
                                <select wire:model.lazy="department_id">
                                    <option
                                        hidden>{{__('common.select-box.choose')}} {{__('data_field_name.asset_inventory.department_execute')}}</option>
                                    <option value="0">{{__('common.select-box.choose_all')}}</option>
                                    @foreach($department as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>
                                @error('department_id') <span class="text-danger"
                                                              style="font-size: 14px">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group info-request">
                                <label>{{__('data_field_name.asset_inventory.perform_date')}}</label>
                                @livewire('component.date-picker', ['inputId' => 'perform-date', 'value' => date('Y-m-d')])
                            </div>
                            <div class="form-group">
                                <div  wire:ignore>
                                <label>{{__('data_field_name.asset_inventory.group_asset')}}</label>
                                <select class="category" data-placeholder="{{__('data_field_name.asset_inventory.group_asset')}}"  id="search-role-list" multiple="multiple"
                                        wire:model.lazy="category_inventory_id">
                                    @foreach ($category as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>
                                </div>
                                @error('category_inventory_id') <span class="text-danger" style="font-size: 14px">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <div  wire:ignore>
                                    <label>{{__('data_field_name.asset_inventory.choose_member')}}</label>
                                    <select class="user-info" id='select_user' wire:model.lazy="user_info_id">
                                        <option hidden>{{__('data_field_name.asset_inventory.choose_member')}}</option>
                                        @foreach($user_info as $key => $val)
                                            <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group info-request">
                                <label>{{__('data_field_name.asset_inventory.period-inventory')}}<span
                                        class="text-danger ">(*)</span></label>
                                <select wire:model.lazy="period_id">
                                    <option hidden> {{__('data_field_name.asset_inventory.period-inventory')}}</option>
                                    @foreach($period as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>
                                @error('period_id') <span class="text-danger"
                                                          style="font-size: 14px">{{ $message }}</span>@enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="group-btn2 text-center ">
                <button type="submit" class="btn btn-cancel" wire:click.prevent="resetInput()" id="close-model-create"
                        data-dismiss="modal">{{__('common.button.cancel')}}</button>
                <button type="submit" class="btn btn-save"
                        wire:click.prevent="storeInventory()">{{__('common.button.save')}}</button>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $.fn.modal.Constructor.prototype._enforceFocus = function() {};
                $('#search-role-list').select2({
                    minimumResultsForSearch: -1,
                    placeholder: function(){
                        $(this).data('placeholder');
                    }
                });
                $("#search-role-list").on("select2:open", function (event) {
                    $('input.select2-search__field').attr('placeholder', '{{__('data_field_name.asset_inventory.group_asset')}}');
                });
                $('#select_user').select2({
                    placeholder: "{{ __('data_field_name.system.account.search_role') }}",
                    matcher: function(params, data) {

                        if ($.trim(params.term) === '') {
                            return data;
                        }
                        if (typeof data.text === 'undefined') {
                            return null;
                        }

                        if (data.text.toLowerCase().indexOf(jQuery.trim(params.term).toLowerCase()) > -1) {
                            var modifiedData = $.extend({}, data, true);
                            return modifiedData;
                        }
                        return null;
                    }
                });

            });
            document.addEventListener('livewire:load', function () {
                $('.user-info').on('change', function (e) {
                    let data = $(this).val();
                @this.set('user_info_id', data);
                });
                $('.category').on('change', function (e) {
                    let data = $(this).val();
                @this.set('category_inventory_id', data);
                });
                $('#search-role-list').select2({});
            });

        </script>
    </div>
</div>
