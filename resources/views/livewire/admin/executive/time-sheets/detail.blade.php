<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a
                    href="{{route('admin.research.ideal.index')}}">{{__('executive/contract.breadcrumbs.activity-management')}}</a>
                \ <span>{{__('data_field_name.import-time-sheets.detail')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('data_field_name.import-time-sheets.detail1', [
				'month' => App\Enums\EMonth::valueToName($month),
				'year' => $year,
			])}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-4 ">
                            @if(!empty($name))
                                @if($checkCreatePermission)
                                    <div class="form-group float-right">
                                        <a href="#" data-toggle="modal" data-target="#import"
                                           class="btn btn-viewmore-news mr0" wire:click="resetData">
                                            <img src="/images/plus2.svg" alt="plus">{{__('data_field_name.import-time-sheets.reload')}}
                                        </a>
                                    </div>
                                @endif
                                @if(checkPermission('admin.executive.salary.download'))
                                    <div class=" float-right">
                                        {{--                                wire:click='export'--}}
                                        <button type="button" class="btn par6" data-toggle="modal"
                                                data-target="#exportModal"
                                                title="{{__('common.button.export_file')}}"><img
                                                src="/images/filterdown.svg" alt="filterdown"></button>
                                    </div>
                                    <div wire:ignore.self class="modal fade" id="exportModal" tabindex="-1"
                                         aria-labelledby="exampleModal" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content ">
                                                <div class="modal-body box-user">
                                                    <h4 class="modal-title">{{__('notification.member.warning.warning')}}</h4>
                                                    {{__('notification.member.warning.Do you want to export excel file?')}}
                                                </div>
                                                <div class="group-btn2 text-center pt-24">
                                                    <button type="button" class="btn btn-cancel"
                                                            data-dismiss="modal">{{__('common.button.cancel')}}</button>
                                                    <button type="button" wire:click="exportData" class="btn btn-save"
                                                            data-dismiss="modal">{{__('common.button.export_file')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif

                        </div>
                    </div>
                    <table class="table">
                        <thead>
                        <th scope="col" class="text-center border-radius-left">
                            {{__('data_field_name.import-time-sheets.user_code')}}
                        </th>
                        <th scope="col" class="text-center">
                            {{__('data_field_name.import-time-sheets.user_name')}}
                        </th>
                        <th scope="col" class="text-center">
                            {{__('data_field_name.import-time-sheets.department')}}
                        </th>
                        <th scope="col" class="text-center">
                            {{__('data_field_name.import-time-sheets.number-of-working-days')}}
                        </th>
                        <th scope="col" class="text-center">
                            {{__('data_field_name.import-time-sheets.actual-number-of-working-days')}}
                        </th>
                        <th scope="col" class="text-center">
                            {{__('data_field_name.import-time-sheets.annual-leave')}}
                        </th>
                        <th scope="col" class="text-center border-radius-right">
                            {{__('data_field_name.import-time-sheets.unpaid-leave')}}
                        </th>
                        <th scope="col" class="text-center">
                            {{__('data_field_name.import-time-sheets.holiday')}}
                        </th>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>
                        @foreach($data as $row)
                            <tr>
                                <td class="text-center">{{$row['userCode']}}</td>
                                <td class="text-center">{{$row['fullname']}}</td>
                                <td class="text-center">{{$row['department']}}</td>
                                <td class="text-center">{{$row['countDate']}}</td>
                                <td class="text-center">{{$row['actual_workday']}}</td>
                                <td class="text-center">{{$row['leave_day']}}</td>
                                <td class="text-center">{{$row['unpaid_day']}}</td>
                                <td class="text-center">{{$row['holiday']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center ml-0">
                            <span>{{__('common.message.empty_search')}}</span>
                        </div>
                    @endif
                    @include('livewire.admin.executive.time-sheets.modal-import')
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(() => {
        window.livewire.on('closeModalImport', () => {
            $('#import').modal('hide');
        });
        $('#import').on('hidden.bs.modal', () => {
            $('#result').modal('show');
        });
    });
</script>
