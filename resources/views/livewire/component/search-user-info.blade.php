<div>
    <input
        type="text"
        class="form-input w-100"
        placeholder="Search ..."
        wire:model="query"
        wire:keydown.escape="resetInput"
        wire:keydown.tab="resetInput"
        wire:keydown.enter="resetInput"
        id="search-input-combobox{{is_null($classify) ? '': -$classify}}"
        style="height:48px"
    />
{{--    <div wire:loading class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg">--}}
{{--        <div class="list-item">Searching...</div>--}}
{{--    </div>--}}

        <div class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg search_bar_div @if(!$displayOption)d-none @endif"
        id="option-list{{is_null($classify) ? '': -$classify}}"
        style="max-height: 250px; overflow-y: auto;overflow-x: hidden">
            @if(!empty($data))
                @foreach($data as $i => $item)
                    <a wire:click="setInput({{ $item['id'] }},'{{ $item['fullname'] }}')"
                        class="search_bar"
                    >{{ $item['fullname'] }}</a>
                @endforeach
            @else
                <div class="search_bar">No results!</div>
            @endif
        </div>

</div>
<script>
    $("document").ready(function() {
        // optionListElement = $("#option-list{{is_null($classify) ? '': -$classify}}");
        // console.log('op-l',optionListElement);
        $("#option-list{{is_null($classify) ? '': -$classify}}").scroll( e => {
            // console.log('croll',optionListElement[0].scrollTop,optionListElement[0].clientHeight)
            if($("#option-list{{is_null($classify) ? '': -$classify}}")[0].scrollTop + $("#option-list{{is_null($classify) ? '': -$classify}}")[0].clientHeight === $("#option-list{{is_null($classify) ? '': -$classify}}")[0].scrollHeight) {
                Livewire.emit('loadMore{{is_null($classify) ? '':$classify}}');
            }
        })
        $('#search-input-combobox{{is_null($classify) ? '': -$classify}}').focus(() => {
            Livewire.emit('displayOption{{is_null($classify) ? '':$classify}}');
        })
        window.addEventListener('reset-height-scroll', () => {
            $("#option-list{{is_null($classify) ? '': -$classify}}")[0].scrollTop = 0;
        })
        $('#search-input-combobox{{is_null($classify) ? '': -$classify}}').blur(() => {
            Livewire.emit('hideOption{{is_null($classify) ? '':$classify}}');
        })
    })
</script>
