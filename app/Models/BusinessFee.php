<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessFee extends Model
{
    use HasFactory;
    protected $table = 'business_fee';
    protected $fillable = [
    	'code','name', 'tracking_type','note','status','admin_id','created_at','updated_at','accounting_code'
    ];
}
