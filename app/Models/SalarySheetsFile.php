<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalarySheetsFile extends BaseModel
{
    use HasFactory;
    protected $table = 'salary_sheets_file';
    protected $fillable = [];

    private static $searchable = [
        'name',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
}
