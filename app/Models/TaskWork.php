<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class TaskWork extends BaseModel
{
    const STATUS_UNDO_TASK = 0;
    const STATUS_ON_WORKING_TASK = 1;
    const STATUS_APPROVE_TASK = 2;
    const STATUS_DONE_TASK = 3;
    const PRIORITY_LEVEL_LOW = 0;
    const PRIORITY_LEVEL_MEDIUM = 1;
    const PRIORITY_LEVEL_HIGH = 2;
    const PRIORITY_LEVEL_EMERGENCY = 3;
    const MAIN_MISSION = 0;
    const SUB_MISSION = 1;
    const DETAIL_TASK = 2;
    use HasFactory;

    public $incrementing = true;
    protected $table = "task_work_detail";
    protected $fillable = [
        'id',
        'code',
        'priority_level',
        'status',
        'topic_id',
        'task_id',
        'work_type',
        'name',
        'start_date',
        'end_date',
        'description',
        'reviewer_id',
        'approve_id',
        'admin_id',
        'real_start_date',
        'real_end_date'
    ];

    public function topicActualFee()
    {
        return $this->hasOne(RealityExpense::class , 'task_id');
    }

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function assigner()
    {
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function reviewer()
    {
        return $this->hasOne(UserInf::class, 'id', 'reviewer_id');
    }

    public function approve()
    {
        return $this->hasOne(UserInf::class, 'íd', 'approve_id');
    }

    public function follower()
    {
        return $this->hasMany(TaskWorkHasUserInfo::class, 'task_work_detail_id', 'id');
    }

    private static $searchable = [
        'name', 'code'
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }

}
