<div wire:ignore.self class="modal fade" id="createExpert" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4>
                @if ($updateMode == true) {{__('research/expert.edit_expert')}}
                @else {{__('research/expert.add_expert')}}
                @endif
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="outline: none;" title="{{__('research/expert.close')}}" wire:click='clickBtnCancel'>
              <span aria-hidden="true close-btn">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label>{{ Str::ucfirst(Str::lower(__('research/expert.table_column.name_expert'))) }}
                  (<span class="text-danger">*</span>)
                </label>
                <input class="form-control" wire:model.defer="name_expert" placeholder="{{__('research/expert.name_expert')}}"></input>
                @error('name_expert_after_remove_utf8') <div class="text-danger"> {{ $message }} </div> @enderror
            </div>

            <div class="form-group">
              <label>{{ Str::ucfirst(Str::lower(__('data_field_name.unit_expert.unit'))) }}
                (<span class="text-danger">*</span>)
              </label>
              {{ Form::select('source', ['' => __('data_field_name.unit_expert.select_unit')] + $expert_unit, null, ['class' => 'form-control', 'wire:model.defer' => 'unit']) }}
              @error('unit') <div class="text-danger"> {{ $message }} </div> @enderror
          </div>

            <div class="form-group">
                <label>{{ Str::ucfirst(Str::lower(__('research/expert.table_column.academic_level'))) }}
                  (<span class="text-danger">*</span>)
                </label>
                {{ Form::select('academic_level', ['' => __('research/expert.academic_level')] + $masterDataCreate, null, ['class' => 'form-control', 'wire:model.defer' => 'academic_level']) }}
                @error('academic_level') <div class="text-danger"> {{ $message }} </div> @enderror
            </div>
            <div class="form-group">
                <label>{{ Str::ucfirst(Str::lower(__('research/expert.table_column.specialized'))) }}
                  (<span class="text-danger">*</span>)
                </label>
                {{ Form::select('specialized', ['' => __('research/expert.specialized')] + $masterDataSpecialCreate, null, ['class' => 'form-control', 'wire:model.defer' => 'specialized']) }}
                @error('specialized') <div class="text-danger"> {{ $message }} </div> @enderror
            </div>
            <div class="form-group">
                <label>{{ Str::ucfirst(Str::lower(__('research/expert.table_column.telephone_number'))) }}
                  (<span class="text-danger">*</span>)
                </label>
                <input class="form-control" wire:model.defer="telephone_number" placeholder="{{__('research/expert.telephone_number')}}"></input>
                @error('telephone_number') <div class="text-danger"> {{ $message }} </div> @enderror
            </div>
            <div class="form-group">
                <label>{{ Str::ucfirst(Str::lower(__('research/expert.table_column.email'))) }}
                  (<span class="text-danger">*</span>)
                </label>
                <input class="form-control" wire:model.defer="email" placeholder="{{__('research/expert.email')}}"></input>
                @error('email') <div class="text-danger"> {{ $message }} </div> @enderror
            </div> 
            <div class="form-group">
                <label>{{ Str::ucfirst(Str::lower(__('research/expert.table_column.work_unit'))) }}
                    (<span class="text-danger">*</span>)
                </label>
                <input class="form-control" wire:model.defer="work_unit" placeholder="{{__('research/expert.work_unit')}}"></input>
                @error('work_unit') <div class="text-danger"> {{ $message }} </div> @enderror
            </div>
            <div class="form-group">
                <label>{{ Str::ucfirst(Str::lower(__('research/expert.table_column.address'))) }}
                </label>
                <textarea rows='4' class="form-control" wire:model.defer="address" placeholder="{{__('research/expert.address')}}"></textarea>
                @error('address') <div class="text-danger"> {{ $message }} </div> @enderror
              </div> 
              <div class="form-group">
                <label>{{ Str::ucfirst(Str::lower(__('data_field_name.unit_expert.scientific_background'))) }}
                </label>
                @livewire('component.files',[
                  'model_name' => $model_name,
                  'type' => $type, 
                  'model_id' => $editID,
                  'folder' => $folder
                ],
                key( $editID ?? 'hello'))    
              </div> 
          </div>
          <div class="modal-footer">
              <div class="group-btn2 text-center ">
                  <button type="button" id="close-add-member-form-btn" data-dismiss="modal" class="btn btn-cancel" wire:click='clickBtnCancel'>{{ __('common.button.cancel') }}</button>
                  <button type="button" wire:click="save" class="btn btn-save">{{ __('common.button.save') }}</button>
              </div>
          </div>
        </div>
    </div>
</div>