<?php

namespace App\Http\Livewire\Admin\Executive\ContractAndSalary\MyContract;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Contract;
use Illuminate\Support\Facades\Auth;

class MyContractList extends BaseLive
{
    public function render()
    {
        $user_info_id = Auth::user()->user_info_id;
        $query = Contract::query();
        $query->where('contract.user_info_id','=', $user_info_id);
        $query->leftJoin('user_info ', "user_info.id", '=', 'contract.user_info_id');
        $query->select('contract.*', 'user_info.position_id');
        $query->with('position')->with('coefficients')->with('bankName');
        $data = $query->orderBy('contract.id', 'DESC')->paginate(25);
        return view('livewire.admin.executive.contract-and-salary.my-contract.my-contract-list', ['data' => $data]);
    }
}
