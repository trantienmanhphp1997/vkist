<?php

namespace App\Exports;

use App\Enums\EApproval;
use App\Enums\ETopicSource;
use App\Enums\ETopicStatus;
use App\Models\Approval;
use App\Models\Topic;
use App\Service\Community;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class ApprovalIdeaExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $mergeRows = [];

    public function __construct($searchTerm, $searchStatus)
    {
        $this->order = 1;
        $this->searchTerm = $searchTerm;
        $this->searchStatus = $searchStatus;
    }

    public function collection()
    {
        $query = Approval::query()
            ->with('ideals.researchField', 'ideals.admin')
            ->where('type', EApproval::TYPE_IDEAL);
        $status = $this->searchStatus;

        if ($this->searchTerm != null) {
            $term = strtolower(removeStringUtf8($this->searchTerm));
            $query->where('unsign_text', 'like', '%' . $term . '%')
                ->where(function ($q) use ($term) {
                    return $q->where('unsign_text', 'like', '%' . $term . '%')
                        ->with('admin')
                        ->orWhereHas('admin.info', function ($query) use ($term) {
                            return $query->where('unsign_text', 'like', '%' . $term . '%');
                        });
                });
        }

        if ($status != null) {
            $query->where('status', $status);
        }

        //anh ta add
        return $query->get();
    }
    public function headings(): array
    {
        return [
            __('data_field_name.common_field.stt'),
            __('data_field_name.approval_topic.profile_code'),
            __('data_field_name.approval_topic.name_profile'),
            __('data_field_name.approval_topic.text_sender'),
            __('data_field_name.common_field.status'),

            __('research/ideal.table_column.code'),
            __('research/ideal.table_column.name'),
            __('research/ideal.table_column.research_field'),
            __('data_field_name.ideal.fee'),
            __('data_field_name.ideal.execution_time'),
            __('research/ideal.table_column.proposer'),
        ];
    }
    public function map($approval): array
    {
        $ideaCount = $approval->ideals->count();
        $this->mergeRows[] = $ideaCount;

        if ($ideaCount > 0) {
            $row = [];
            foreach ($approval->ideals as $key => $idea) {
                $row[] = [
                    $key == 0 ? $this->order++ : '',
                    $key == 0 ? $approval->code : '',
                    $key == 0 ? $approval->name : '',
                    $key == 0 ? ($approval->admin->name ?? '') : '',
                    $key == 0 ? EApproval::valueToName($approval->status) : '',

                    $idea->code,
                    $idea->name,
                    $idea->researchField->name ?? '',
                    $idea->fee ?? 0,
                    $idea->execution_time,
                    $idea->admin->name ?? ''
                ];
            }
            return $row;
        }

        return [
            $this->order++,
            $approval->code,
            $approval->name,
            $approval->admin->name ?? '',
            EApproval::valueToName($approval->status),
            '', '', '', '', '', ''
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            // tạo tiêu đề
            $titleCells = ['A', 'B', 'C', 'D', 'E'];
            $event->sheet->mergeCells('A1:E1');
            $event->sheet->setCellValue('A1', mb_strtoupper(__('data_field_name.approval_topic.list_review'), 'UTF-8'));
            $event->sheet->getStyle('A1:K3')->applyFromArray([
                'font' => ['bold' => true],
                'alignment' => array(
                    'horizontal' => \PHPOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                )
            ]);
            foreach ($titleCells as $cell) {
                $value = $event->sheet->getCell("{$cell}3")->getValue();
                $event->sheet->mergeCells("{$cell}2:{$cell}3");
                $event->sheet->setCellValue("{$cell}2", $value);
            }

            // gộp các ô
            $index = 4;
            foreach ($this->mergeRows as $value) {
                if($value == 0) {
                    $index += 1;
                    continue;
                }

                foreach ($titleCells as $cell) {
                    $event->sheet->mergeCells("{$cell}{$index}:{$cell}" . ($index + $value - 1));
                }
                $index += $value;
            }

            // tạo phần thông tin ý tưởng
            $event->sheet->mergeCells('H2:M2');
            $event->sheet->setCellValue('H2', mb_strtoupper(__('research/ideal.list'), "UTF-8"));
        }];
    }

    public function title(): string
    {
        return  __('data_field_name.approval_topic.list_review');
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
