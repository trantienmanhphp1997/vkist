<?php

namespace App\Notifications;

use App\Notifications\Drivers\CustomDatabaseDriver;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class BaseNotification extends Notification implements ShouldBroadcastNow
{
    use Queueable;

    public $type;
    public $methods = [];

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if(in_array('broadcast', $this->methods)) {
            array_push($this->methods, CustomDatabaseDriver::class);
        }
        Log::info($this->methods);
        return array_merge($this->methods);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage);
            // ->line('The introduction to the notification.')
            // ->action('Notification Action', url('/'))
            // ->line('Thank you for using our application!');
    }

    public function toCustomDatabase($notifiable)
    {
        return $this->message($notifiable);
    }

    public function toBroadcast($notifiable)
    {
        $message = $this->message($notifiable);

        $body = __($message['template_name'] ?? '', [
            'name' => $message['user']['name'] ?? '',
            'action' => (isset($message['action']) && !empty($message['action'])) ? __('notification.user.actions.' . $message['action'], [], $notifiable->lang ?? 'en') : '',
            'model_type' => __('common.model.' . ($message['model']['type'] ?? ''), [], $notifiable->lang ?? 'en'),
            'model_value' => $message['model']['code'] ?? $message['model']['name'] ?? ''
        ], $notifiable->lang ?? 'en');

        return new BroadcastMessage([
            'body' => $body
        ]);
    }

    public function withMethods(array $methods = ['broadcast'])
    {
        $this->methods = $methods;
        return $this;
    }

    protected function message($notifiable)
    {
        return [
            'template_name' => 'Template name',
        ];
    }
}
