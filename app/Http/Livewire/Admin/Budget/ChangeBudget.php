<?php

namespace App\Http\Livewire\Admin\Budget;

use App\Models\Budget;
use App\Models\BudgetHasExpensePlan;
use App\Service\Community;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class ChangeBudget extends Component
{

    public $data;
    public $model_name;

    public $type;
    public $folder;
    public $total_draft;
    public $money_plan_draft;
    public $requestId;

    public $total_budget;
    public $money_plan;
    public $money_real;
    public $keys;
    public $request_change;
    public $idBudget;

    public function render()
    {
        if ($this->data->request_gw_status == \App\Enums\EBudgetStatus::WAIT){
            $this->total_budget= numberFormat($this->total_draft) ;
        }
        else{
            $this->total_budget = numberFormat($this->data->total_budget);

            foreach ($this->data->hasPlan as $key => $value){
                $this->money_plan[$key] = numberFormat($value->money_plan);
                $this->money_real[$key] = $value->money_real;
                $this->idBudget[$key] = $value->id;
            }

        }

        return view('livewire.admin.budget.change-budget');
    }

    public function save($id)
    {

        $this->validate( [
            'money_plan.*' => 'max:14|required',
        ], [], [
            'money_plan.*' => __('data_field_name.budget.money_plan'),
        ]);
        $budget = Budget::findOrFail($id);
        if ($budget->request_gw_status == \App\Enums\EBudgetStatus::WAIT) {
            $this->request_change = $budget->update([
                'request_gw_data'=>$budget->request_gw_data
            ]);

        } else {
            if ($budget->allocated == 1) {
                $plan = $this->money_plan;


                $data_plan = implode(" ", $plan);
                $total = Community::getAmount($this->total_budget);
                $data_old = $budget->hasPlan->pluck('money_plan')->toArray();

                $money_plan_old = implode(" ", $data_old);


                $data_json = ['total_budget' => $total, 'money_plan' => $data_plan, 'money_plan_old'=>$money_plan_old];
                $this->request_change = Budget::where('id',$id)->update([
                    'request_gw_data' => json_encode($data_json),
                    'request_gw_status' => \App\Enums\EBudgetStatus::WAIT
                ]);

            } else {
                $total = Community::getAmount($this->total_budget);
                $data_json = ['total_budget' => $total];
                $this->request_change = $budget->update([
                    'request_gw_data' => json_encode($data_json),
                    'request_gw_status' => \App\Enums\EBudgetStatus::WAIT,
                ]);
            }
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
                __('notification.common.success.add')]);
        }

        $this->emit('saveDataSendApproval');
    }
}
