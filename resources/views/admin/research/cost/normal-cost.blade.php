@extends('layouts.master')
@section('title')
    <title>{{ $data->name }}</title>
@endsection
@section('homeLeft')
@include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    @livewire('admin.research.cost.normal-cost-list', ['topicFeeDetailId' => $data->id, 'topicFeeId' => $data->topic_fee_id, 'topicFeeStatus' => $data->topicFee->status])
@endsection
