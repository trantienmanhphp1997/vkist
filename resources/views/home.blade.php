@extends('layouts.master')
@section('title')
    <title> {{ __('header.home-title') }}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._homeLeft')
@endsection
@section('homeRight')
    @include('layouts.partials.sliderbar._homeRight')

@endsection
@section('content')
<div class="col-md-12 col-xl-11 box-news">
    <div class="row bd-border mt-0">
        <div class="col-md-12 position-relative">
            @if (session('newsPerDayId'))
                <span class="badge badge-primary p-1" style="left: 100px;top: 0;position: absolute;">
                                 {{ count( session('newsPerDayId'))  }}
                            </span>
            @endif
            <h3 class="title pull-left">{{__("news/newsManager.menu_name.list-new")}} </h3>
            <a href="{{route('news.index')}}">
                <button class="btn btn-viewmore-news float-right mb-12" >{{__("news/newsManager.menu_name.see-more")}} <img src="images/Plus.svg" alt="plus"></button>
            </a>
        </div>
        <livewire:component.news />
        <div class="col-md-12">
            <div class="">
                <h3 class="title"> {{__('header.approved')}}</h3>
            </div>
            <div class="col-md-12 approve">
                <div class="row">
                    <div class="title-approve">
                <span>  {{ __('menu_management.menu_name.file-name') }}
                    <span class="float-right">
                         {{__('menu_management.menu_name.status')}}
                    </span>
                  </span>
                    </div>
                    @if (!empty($listApproval))
                        @foreach($listApproval as $approval)
                            <div class="col-md-12 item">
                                <a href="{{ route('api.login.approval.index') }}" target="_blank" class="d-inline-block col-md-12 row">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <h4 class="mb-0">{{ $approval['name'] }}</h4>
                                            <span class="date">
                                                @if ($approval['time'] != 0)
                                                    {{ $approval['time'] . ' ' . __('data_field_name.home.yesterday') }}
                                                @else
                                                    {{ __('data_field_name.home.today') }}
                                                @endif
                                            </span>
                                        </div>

                                        @if ($approval['status'] == \App\Enums\EApiApproval::STATUS_COMPLETE)
                                            <button type="button" class="btn btn-plan bg-blue medium">{{ $approval['text_status'] }}</button>
                                        @elseif ($approval['status'] == \App\Enums\EApiApproval::STATUS_REJECT)
                                            <button type="button" class="btn btn-plan bg-blue emergency">{{ $approval['text_status'] }}</button>
                                        @else
                                            <button type="button" class="btn btn-plan bg-blue high">{{ $approval['text_status'] }}</button>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @else

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
