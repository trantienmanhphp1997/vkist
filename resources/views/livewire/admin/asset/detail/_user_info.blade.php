<div class="info-item">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="d-flex item-tỉtle">
                    <p class="text-uppercase"> {{__('data_field_name.asset.user_info')}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.full_name')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->fullname:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.user_info_code')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->code:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.position')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->position_name:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.from_department')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->depart_name:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name"> {{__('data_field_name.asset.manager')}}</p>
                    <p class="content">: </p>
                </div>
            </div>
        </div>
    </div>
</div>
