<?php

namespace App\Http\Livewire\Admin\Asset\Provider;

use Livewire\Component;
use App\Models\AssetProvider;
use App\Http\Livewire\Base\BaseLive;
use App\Enums\EMasterDataType;
use App\Models\MasterData;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AssetProviderExport;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;


class AssetProviderList extends BaseLive
{
    public $searchTerm;
    public $current_id_data;

    //asset provider info for create and edit
    public $isEdit = false;
    public $list_bank_info;
    public $checkDownloadPermission;
    //asset_provider
    public $asset_provider;

    public $name;
    public $code;
    public $address;
    public $tax_code;
    public $phone_number;
    public $website;
    public $bank_account_number;
    public $bank_name_id;
    public $email;
    public $note;

    //checked
    public $allChecked = false;
    public $checked = []; //mảng associate với key là id của provider, value là trạng thái checked trong checkbox
    public $needToResetChecked = true;

    public function mount() {
        $this->checkDownloadPermission = checkRoutePermission("download");
        if($this->asset_provider) {
            $this->isEdit = true;

            $this->name = $this->asset_provider->name;
            $this->code = $this->asset_provider->code;
            $this->address = $this->asset_provider->address;
            $this->tax_code = $this->asset_provider->tax_code;
            $this->phone_number = $this->asset_provider->phone_number;
            $this->website = $this->asset_provider->website;
            $this->bank_account_number = $this->asset_provider->bank_account_number;
            $this->bank_name_id = $this->asset_provider->bank_name_id;
            $this->email = $this->asset_provider->email;
            $this->note = $this->asset_provider->note;
        }
        $this->list_bank_info = MasterData::where('type', EMasterDataType::BANK_NAME)->get();
    }
    public function render(){
        $query = AssetProvider::query();
        if (!empty($this->searchTerm)) {
            $query->where('asset_provider.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')->get();
        }

        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);
        if($this->needToResetChecked){
            $this->checked = [];
            foreach ($data as $item) {
                $this->checked[$item->id] = false;
            }
        }
        $this->needToResetChecked = true;
        $this->current_id_data = $data->map(function ($item) {
            return $item->id;
        });
        return view('livewire.admin.asset.provider.asset-provider-list', compact('data'));
    }

    public function updatedChecked() {
        $this->needToResetChecked = false;
    }

    public function updatedAllChecked() {
        $this->checked = array_fill_keys(array_keys($this->checked), $this->allChecked);
        $this->needToResetChecked = false;
    }

    public function resetDeleteId() {
        $this->deleteId = null;
        $this->needToResetChecked = false;
    }

    public function getProviderInfoForUpdateModal($id) {
        $assetProvider = AssetProvider::findOrFail($id);
        $this->asset_provider = $assetProvider;
        $this->isEdit = true;
        $this->name = $assetProvider->name;
        $this->code = $assetProvider->code;
        $this->address = $assetProvider->address;
        $this->tax_code = $assetProvider->tax_code;
        $this->phone_number = $assetProvider->phone_number;
        $this->website = $assetProvider->website;
        $this->bank_account_number = $assetProvider->bank_account_number;
        $this->bank_name_id = $assetProvider->bank_name_id;
        $this->email = $assetProvider->email;
        $this->note = $assetProvider->note;
    }

    public function resetProviderInfo() {
        $this->reset(['isEdit','asset_provider', 'name', 'code', 'address', 'tax_code','phone_number','website','bank_account_number','bank_name_id','email','note']);
    }

    public function delete()
    {
        if(is_null($this->deleteId)) {
            foreach($this->checked as $key => $item) {
                if($item) {
                    $assetProvider = AssetProvider::findOrFail($key);
                    $assetProvider->delete();
                }
            }
        } else {
            $assetProvider = AssetProvider::findOrFail($this->deleteId);
            $assetProvider->delete();
        }
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }

    public function store() {
        $this->bank_name_id = is_null($this->bank_name_id) ? intval($this->list_bank_info[0]->id) : intval($this->bank_name_id);
        $this->validate([
            'name' => ['required','max:100','regex:/^[a-zA-Z0-9'.config('common.special_character.alphabet').'\.\,\-\(\)\/ ]+$/'],
            'code' => ['required','not_regex:/[a-z' .config('common.special_character.alphabet_lowercase'). ']/', 'max:15'],
            'address' => 'max:100',
            'tax_code' => ['regex:/^[0-9]*$/','nullable', 'max:13'],
            'phone_number' => ['regex:/^[0-9]*$/','nullable', 'max:14'],
            'website' => 'max:100',
            'bank_account_number' => ['regex:/^[0-9]*$/','nullable', 'max:14'],
            'email' => 'nullable|email:rfc,dns|max:100',
            'note' => 'max:255'
        ], [], [
            'name' => __('asset/provider.label.name'),
            'code' => __('asset/provider.label.code'),
            'address' => __('asset/provider.label.address'),
            'tax_code' => __('asset/provider.label.tax_code'),
            'phone_number' => __('asset/provider.label.phone_number'),
            'website' => __('asset/provider.label.website'),
            'bank_account_number' => __('asset/provider.label.bank_account_number'),
            'email' => __('asset/provider.label.email'),
            'note' => __('asset/provider.label.note'),
        ]);
        if(is_null($this->asset_provider)) {
            $this->asset_provider = new AssetProvider();
        }
        $this->asset_provider->name= $this->name;
        $this->asset_provider->address= $this->address;
        $this->asset_provider->tax_code= $this->tax_code;
        $this->asset_provider->code= $this->code;
        $this->asset_provider->phone_number= $this->phone_number;
        $this->asset_provider->website= $this->website;
        $this->asset_provider->bank_account_number= $this->bank_account_number;
        $this->asset_provider->bank_name_id= $this->bank_name_id;
        $this->asset_provider->email= $this->email;
        $this->asset_provider->note= $this->note;
        $this->asset_provider->save();

        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->isEdit ? __('asset/provider.notification.edit-success') : __('asset/provider.notification.create-success')]);
        $this->resetProviderInfo();
        $this->emit('close-modal-create-or-edit-provider');
    }

    public function export() {
        $today = date("d_m_Y");
        return Excel::download(new AssetProviderExport($this->current_id_data), 'list-asset-provider-' . $today . '.xlsx');
    }

}
