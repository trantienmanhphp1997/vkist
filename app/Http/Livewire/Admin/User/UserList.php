<?php

namespace App\Http\Livewire\Admin\User;

use App\Component\DepartmentRecursive;
use App\Component\Recursive;
use App\Exports\UsersExport;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Department;
use App\Models\MasterData;
use App\Models\RequestLeave;
use App\Models\ResearchProject;
use App\Models\Topic;
use App\Models\User;
use App\Models\UserInf;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\Log;


class UserList extends BaseLive
{
    public $selectedtypes = [];
    public $searchName;
    public $searchDepartment;
    public $searchPosition;
    public $searchDate;
    public $searchProject;

    public array $searchStaffIds = [];

    public $projectList;
    public $departmentList;
    public $positionList;
    public $deleteId = '';
    protected $listeners = [
        'getIdSelected', 'resetSelected',
        'changed-project-event' => 'updatedSearchProject'
    ];
    public $getIdSelected = [];

    public function deleteIdUser($id)
    {
        $this->deleteId = $id;
    }

    public function mount()
    {
        $this->departmentList = DepartmentRecursive::hierarch(Department::all());
        $this->positionList = MasterData::where('type', 1)->get();
        $this->projectList = ResearchProject::all();
    }

    public function render()
    {
        $query = UserInf::with(['department:id,name', 'position:id,v_value,v_value_en', 'contract']);
        if (strlen($this->searchTerm)) {
            $query->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        if (!empty($this->searchDepartment)) {
            $query->where('department_id', $this->searchDepartment);
        }

        if (!empty($this->searchPosition)) {
            $query->where('position_id', $this->searchPosition);
        }

        if (!empty($this->searchDate)) {
            $query->whereDate('created_at', '=', $this->searchDate);
        }

        if (!empty($this->searchProject) && !empty($this->searchStaffIds)) {
            $query->whereIn('id', $this->searchStaffIds);
        }

        $data = $query->orderBy('id', 'DESC')->paginate($this->perPage);

        return view('livewire.admin.user.user-list', ['data' => $data]);
    }
    public function export()
    {
        $today = date("d_m_Y");
        return Excel::download(new UsersExport($this->getIdSelected,$this->searchTerm,$this->searchDepartment,$this->searchPosition,$this->searchDate,$this->searchProject,$this->searchStaffIds), 'list-user-' . $today . '.xlsx');
    }
    //delete

    public function delete()
    {
        parent::delete();
        if (empty($this->getIdSelected)) {
            Log::info('delete:' . $this->deleteId);
            UserInf::findOrFail($this->deleteId)->delete();
        } else {
            UserInf::whereIn('id', $this->getIdSelected)->delete();
        }
        $this->getIdSelected = [];
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }

    public function getIdSelected($checked)
    {
        $this->getIdSelected = $checked;
    }

    public function resetSelected()
    {
        $this->getIdSelected = [];
    }

    // nếu có sửa gì đến tìm nhân sự theo dự án thì chỉ cần sửa searchStaffId thôi :D
    public function updatedSearchProject($projectId)
    {
        $project = ResearchProject::query()->find($projectId);

        $contractCode = $project->contract_code ?? null;
        if (empty($contractCode)) {
            return;
        }

        $topic = Topic::query()->with('members')->where('contract_code', $contractCode)->first();
        if (empty($topic)) {
            $this->searchStaffIds = [];
            return;
        }

        $this->searchStaffIds = $topic->members->pluck('user_info_id')->all();
    }
}
