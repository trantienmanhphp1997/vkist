<?php

namespace App\Http\Livewire\Admin\ResearchProject\Information;

use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchProject;
use Livewire\Component;

class DraftList extends BaseLive
{
    public $projectID;

    public function mount(){
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render()
    {
        $query = ResearchProject::query()->with(['leader'])->where(['status'=>ResearchProject::DRAFT]);
        if ($this->searchTerm != '') {
            $searchTerm = $this->searchTerm;
            $query->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8($searchTerm)) . "%'")
                ->orWhereHas('researchCategory', function ($q) use($searchTerm){
                    $q->whereRaw("unsign_text LIKE LOWER('%{$searchTerm}%')");
                })
                ->orWhereHas('leader', function ($q) use($searchTerm){
                    $q->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8($searchTerm)) . "%'");
                })
                ->orWhere('project_code', 'LIKE', '%' . $this->searchTerm . '%');
        }
        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);

        return view('livewire.admin.research-project.information.draft-list',compact('data'));
    }

    public function setProjectID($id){
        $this->projectID = $id ;
    }

    public function transferToOperation(){
        if ($this->checkEditPermission){
            ResearchProject::findOrFail($this->projectID)->update([
                'status'=>ResearchProject::IN_PROCESSING
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('research/project.success.success-transfer-operation')]);

        }
    }

    public function delete(){
        if ($this->checkDestroyPermission){
            ResearchProject::findOrFail($this->deleteId)->forceDelete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('research/project.information.delete-msg-success')]);
        }
    }
}
