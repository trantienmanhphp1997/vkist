<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetCategoryTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('asset_category', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Mã danh mục tài sản');
            $table->string('code',255)->unique()->comment('Mã loại tài sản: hiển thị dưới dạng tree');
            $table->string('name', 500)->comment('Tên loại tài sản');
            $table->integer('group_id')->nullable()->comment('Nhóm tài sản map với master_data :15');
            $table->bigInteger('parent_id')->nullable()->comment('Mã tài sản cha');
            $table->string('lost_rate',10)->nullable()->comment('Tỷ lệ hao mòn %');
            $table->string('used_year',10)->nullable()->comment('số năm sử dụng');
            $table->string('note',1000)->nullable()->comment('Ghi chú');
            $table->tinyInteger('status')->nullable()->comment('tình trạng sử dụng: 0: không sử dụng, 1 : còn sử dụng');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_category');
    }
}
