<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSourceCostToWorkPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_plan', function (Blueprint $table) {
            $table->tinyInteger('source_cost')->nullable()->comment('Nguon kinh phi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_plan', function (Blueprint $table) {
            $table->dropColumn('source_cost');
        });
    }
}
