<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyRequestHasUserInfo extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'survey_request_has_user_info';

    protected $fillable = [
        'user_info_id',
        'survey_request_id'
    ];
}
