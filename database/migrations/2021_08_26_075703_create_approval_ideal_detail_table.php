<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApprovalIdealDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_ideal_detail', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('status')->nullable();
            $table->unsignedBigInteger('approval_id')->nullable()->comment('Map voi approval');
            $table->foreign('approval_id')->references('id')->on('approval');
            $table->foreignId('ideal_id')->nullable()->constrained('ideal');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval_ideal_detail');
    }
}
