<?php

namespace App\Http\Livewire\Admin\Executive\UserInfoLeave;

use App\Http\Livewire\Base\BaseLive;
use App\Component\SizeFile;
use Livewire\WithFileUploads;
use App\Component\FileUpload;
use App\Models\UserInfoLeave;
use App\Models\UserInf;
use Illuminate\Support\Facades\Config;


use function Complex\theta;

class UserInfoLeaveCreateAndUpdate extends BaseLive
{
    use WithFileUploads;

    public $searchName = '';
    public $isEdit = false;
    public $user_info_id;
    public $code ;
    public $name;
    public $department;
    public $position;
    public $reason;
    public $leaveDate;
    public $status;
    public $accept_file;
    public $limit_total_file = 5;
    public $limit_file_size = 255; //MB

    public $userInfoLeave;


    //upload file
    public $fileUpload;
    public $listFileUpload = [];

    //file cũ đã có sẵn của user_info_leave
    public $listOldFile = [];

    //file cũ đã có sẵn của user_info_leave, đây là bản tạm dùng để hiển thị cho user xem, xoá trên list nay, khi nào lưu thì mới áp dụng thay đổi vô list chính
    public $listOldFileTemp = [];

    //danh sách các file cũ mà user muốn xoá
    public $listIdOfOldFileNeedDelete = [];
    public $current_total_file;


    protected $listeners = [
        'setTargetId' => 'getUserInfo',
        'store-user-info-leave' => 'store',
        'set-leaveDate' => 'setLeaveDate',
    ];
    public function render()
    {
        return view('livewire.admin.executive.user-info-leave.create-and-edit');
    }

    public function mount()
    {  
        $this->accept_file = Config::get('common.mime_type.general');
        if($this->userInfoLeave) {
            //get general info
            $this->isEdit=true;
            $this->getUserInfo($this->userInfoLeave->user_info_id);
            $this->reason = $this->userInfoLeave->reason;
            $this->leaveDate = !is_null($this->userInfoLeave->leave_date) ? date('Y-m-d', strtotime($this->userInfoLeave->leave_date)) : null;
            $this->status = $this->userInfoLeave->status;

            //get document
            $model_name = UserInfoLeave::class;
            $type = Config::get('common.type_upload.UserInfoLeave');
            $model_id = $this->userInfoLeave->id;
            $this->listOldFile=\App\Models\File::where('model_name',$model_name)->where('type', $type)->where('model_id',$model_id)->get();

            //đẩy list file cũ qua list tạm
            foreach($this->listOldFile as $item) {
                $infoOldFile = [
                    'id' => $item->id,
                    'file_name' => $item->file_name,
                    'size_file' => $item->size_file,
                    'url' => $item->url,
                ];
                array_push($this->listOldFileTemp, $infoOldFile);
            }
        }
    }

    public function updatedFileUpload() {
        $this->current_total_file = count($this->listFileUpload) + count($this->listOldFileTemp);
        $rules = [
            'fileUpload' => [
                'file',
                'mimes:' . implode(',', $this->accept_file),
                'max:' . $this->limit_file_size * 1024
            ],
            'current_total_file' => 'numeric | max:'. ($this->limit_total_file - 1)
        ];
        $error_message = [
            'fileUpload.mimes' => __('notification.upload.mime_type'),
            'fileUpload.max' => __('notification.upload.maximum_size', ['value' => $this->limit_file_size]),
            'current_total_file.max' => __('notification.upload.maximum_uploads')
        ];
        $this->validate($rules,$error_message);
        $size = new SizeFile();
        $size=$size->sizeFile($this->fileUpload->getSize());
        $filenameOrigin = $this->fileUpload->getClientOriginalName();
        $fileUploadInfo = [
            'file_name' => $filenameOrigin,
            'size_file' => $size,
            'file' => $this->fileUpload,
        ];
        array_push($this->listFileUpload, $fileUploadInfo);
        $this->fileUpload = null;
    }
    
    public function download($url, $name) {
        return response()->download('storage/'.$url, $name);
    }


    public function getUserInfo($id) {
        $user = UserInf::with(['department', 'position'])->where('id','=',$id)->first();
        $this->user_info_id = $id;
        $this->code = $user->code;
        $this->name = $user->fullname;
        $this->department = $user->department->name ?? '';
        $this->position = $user->position->v_value ?? '';
    }

    public function setLeaveDate($data) {
        $this->leaveDate = $data['leaveDate'] ? date('Y-m-d', strtotime($data['leaveDate'])) : null;
    }

    public function deleteNewFileUpload($index) {
        unset($this->listFileUpload[$index]);
    }

    public function deleteFileOldTemp($index) {
        if(isset($this->listOldFileTemp[$index])) {
            array_push($this->listIdOfOldFileNeedDelete, $this->listOldFileTemp[$index]['id']);
            unset($this->listOldFileTemp[$index]);
        }
    }

    public function store() {

        //để lấy default leaveDate là ngày hiện tại
        if(is_null($this->leaveDate)) {
            $this->leaveDate = date('Y-m-d');
        }

        $this->validate([
            'name' => 'required',
            'reason' => ['required','max:100', 'regex:/^[a-zA-Z0-9'.config('common.special_character.alphabet').']+$/'],
            'leaveDate' => 'required',
        ], [], [
            'name' => __('executive/user-info-leave.create-update-form.name'),
            'reason' => __('executive/user-info-leave.create-update-form.reason'),
            'leaveDate' => __('executive/user-info-leave.create-update-form.leave_date'),
        ]);
        if(is_null($this->userInfoLeave)) {
            $this->userInfoLeave = new UserInfoLeave();
        }
        
        $this->userInfoLeave->status = intval($this->status);
        $this->userInfoLeave->reason = $this->reason;
        $this->userInfoLeave->leave_date = $this->leaveDate;
        $this->userInfoLeave->user_info_id = $this->user_info_id;
        $this->userInfoLeave->save();

        //lưu các file đính kèm
        $fileUploadComponent=new FileUpload();
        $model_name = UserInfoLeave::class;
        $type = Config::get('common.type_upload.UserInfoLeave');
        $model_id = $this->userInfoLeave->id;
        $folder = app($model_name)->getTable();
        foreach($this->listFileUpload as $file) {
            $dataUpload= $fileUploadComponent->uploadFile($file['file'],$folder);
            $file_upload = new \App\Models\File();
            $file_upload->url = $dataUpload['url'];
            $file_upload->size_file = $dataUpload['size_file'];
            $file_upload->file_name = $dataUpload['file_name'];
            $file_upload->model_name = $model_name;
            $file_upload->model_id = $model_id;
            $file_upload->type = $type;
            $file_upload->save();
        }

        //xoá các file đính kèm cần xoá
        foreach($this->listOldFile as $item) {
            if(in_array($item->id, $this->listIdOfOldFileNeedDelete)) {
                $item->delete();
            }
        }

        if($this->isEdit) {
            session()->flash('success', __('notification.common.success.edit'));
        } else {
            session()->flash('success', __('notification.common.success.add'));
        }
        
        redirect()->route('admin.executive.user-info-leave.index');
    }
}
