<?php

namespace App\Http\Controllers\Admin\Executive;

use App\Enums\EBudgetStatus;
use App\Enums\EShoppingStatus;
use App\Enums\ETypeUpload;
use App\Enums\ERouteName;
use App\Http\Controllers\Controller;
use App\Models\Budget;
use App\Models\File;
use App\Models\Shopping;
use App\Models\User;
use App\Service\Community;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShoppingController extends Controller
{
    public function index()
    {
        return view(ERouteName::ROUTE_SHOPPING_INDEX);
    }

    public function create()
    {
        $model = Shopping::class;
        $type = ETypeUpload::SHOPPING_ATTACHMENT;
        $budgets = Budget::query()->where('status', EBudgetStatus::DONE)->get()->mapWithKeys(fn ($item) => [$item['id'] => $item['name']])->all();
        return view('admin.executive.shopping.create', [
            'model' => $model,
            'type' => $type,
            'budgets' => $budgets
        ]);
    }

    public function store(Request $request)
    {
        $dateFormat = config('common.formatDate');

        $request->merge([
            'estimate_money' => Community::getAmount($request->input('estimate_money'))
        ]);

        $request->validate([
            'name' => 'required|max:100|regex:/^[\w\.\,' . config('common.special_character.alphabet') . ']+$/',
            'budget_id' => 'required',
            'estimate_money' => 'required|max:10|regex:/^[0-9]+$/',
            'needed_date' => 'nullable|date_format:' . $dateFormat,
            'device_name' => 'required|array',
            'device_name.*' => 'required|max:48',
            'device_quantity' => 'required|array',
            'device_quantity.*' => 'required|max:6|regex:/^[0-9]+$/',
            'note' => 'nullable|max:255',
            'estimated_delivery_date' => 'nullable|date_format:' . $dateFormat
        ], [], [
            'name' => __('data_field_name.shopping.name'),
            'budget_id' => __('data_field_name.shopping.budget'),
            'estimate_money' => __('data_field_name.shopping.estimate_money'),
            'needed_date' => __('data_field_name.shopping.needed_date'),
            'device_name' => __('data_field_name.shopping.device_list'),
            'device_name.*' => __('data_field_name.shopping.device_name'),
            'device_quantity.*' => __('data_field_name.shopping.device_quantity'),
            'note' => __('data_field_name.shopping.note'),
            'estimated_delivery_date' => __('data_field_name.shopping.estimated_delivery_date')
        ]);

        $currentId = auth()->id();

        $neededDate = (empty($request->input('needed_date'))) ? null : Carbon::createFromFormat($dateFormat, $request->needed_date)->format('Y-m-d');
        $estimatedDeliveryDate = (empty($request->input('estimated_delivery_date'))) ? null : Carbon::createFromFormat($dateFormat, $request->estimated_delivery_date)->format('Y-m-d');

        $shopping = Shopping::create([
            'name' => $request->name,
            'budget_id' => $request->budget_id,
            'estimate_money' => $request->estimate_money,
            'needed_date' => $neededDate,
            'note' => $request->note,
            'admin_id' => $currentId,
            'proposer_id' => $currentId,
            'status' => EShoppingStatus::NOT_APPROVED,
            'estimated_delivery_date' => $estimatedDeliveryDate
        ]);

        File::query()
            ->where('model_name', Shopping::class)
            ->where('type', ETypeUpload::SHOPPING_ATTACHMENT)
            ->where('admin_id', $currentId)
            ->whereNull('model_id')
            ->update([
                'model_id' => $shopping->id
            ]);

        $deviceCount = count($request->input('device_name'));
        $devicesData = [];
        for ($i = 0; $i < $deviceCount; $i++) {
            array_push($devicesData, [
                'name' => $request->input('device_name')[$i],
                'quantity' => $request->input('device_quantity')[$i],
            ]);
        }

        $shopping->devices()->createMany($devicesData);

        return redirect()->route(ERouteName::ROUTE_SHOPPING_INDEX)->with('success', __('notification.common.success.submit_proposal'));
    }

    public function show($id)
    {

        $shopping = Shopping::findOrFail($id);
        $shopping->load(['proposer', 'budget', 'devices']);

        $shoppingStatus = EShoppingStatus::getList();

        $checkApprovePermission = checkRoutePermission('approve');
        $checkEditPermission = checkRoutePermission('edit');
        $canEdit = ($shopping->status != EShoppingStatus::NOT_APPROVED) && ($shopping->status != EShoppingStatus::REJECTED);

        $availableUsers = [];

        if ($checkEditPermission) {
            $availableUsers = User::all()->mapWithKeys(fn ($item) => [$item['id'] => $item['name']]);
            $shoppingStatus = array_diff($shoppingStatus, [
                EShoppingStatus::NOT_APPROVED => __('data_field_name.shopping.status_not_approved'),
                EShoppingStatus::REJECTED => __('data_field_name.shopping.status_rejected'),
            ]);
        }

        return view('admin.executive.shopping.show', [
            'shopping' => $shopping,
            // 'files' => $files,
            'modelName' => Shopping::class,
            'fileType' => ETypeUpload::SHOPPING_ATTACHMENT,
            'shoppingStatus' => $shoppingStatus,
            'checkEditPermission' => $checkEditPermission,
            'checkApprovePermission' => $checkApprovePermission,
            'availableUsers' => $availableUsers,
            'canEdit' => $canEdit
        ]);
    }

    public function update(Request $request, $id)
    {
        $shopping = Shopping::findOrFail($id);
        $action = $request->input('action', 'validation');
        if ($action == 'validation') {
            $request->validate([
                'approved_note' => 'nullable|max:255'
            ], [], [
                'approved_note' => __('data_field_name.shopping.approved_note')
            ]);
            $status = ($request->input('validation_type') == 'approve') ? EShoppingStatus::APPROVED : EShoppingStatus::REJECTED;

            $shopping->update([
                'approved_note' => $request->input('approved_note'),
                'status' => $status,
                'approver_id' => auth()->id()
            ]);
        } else if ($action == 'edit') {
            $request->validate([
                'executor_id' => 'required',
                'status' => 'required',
                'estimated_delivery_date' => 'nullable'
            ]);

            $estimatedDeliveryDate = empty($request->estimated_delivery_date) ? null : Carbon::createFromFormat(config('common.formatDate'), $request->estimated_delivery_date)->format('Y-m-d');

            $shopping->update([
                'executor_id' => $request->input('executor_id'),
                'status' => $request->input('status'),
                'estimated_delivery_date' => $estimatedDeliveryDate
            ]);
        }

        return redirect()->route(ERouteName::ROUTE_SHOPPING_INDEX)->with('success', __('notification.common.success.update'));
    }
}
