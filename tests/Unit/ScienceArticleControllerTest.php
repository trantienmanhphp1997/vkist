<?php

namespace Tests\Unit;


use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire;
use App\Models\ResearchAchievement;
use App\Http\Livewire\Admin\Research\Achievement\AchievementCreateAndUpdate;
use App\Enums\EResearchAchievementType;


class ScienceArticleControllerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use RefreshDatabase;

    /** @test */
    function can_create_science_article()
    {
        $user =  $this->user;

        $this->actingAs($user);
        Livewire::test(AchievementCreateAndUpdate::class)
            ->set([
                'type' => EResearchAchievementType::SCIENCE_ARTICLE,
                'name' => 'bài báo khoa học test',
                // 'topic_id' => '1',
                'publish_date' => date('Y-m-d'),
                'magazine_name' => 'Tạp chí khoa học',
                'article_link' => 'tapchi.com'
                ])
            ->call('store');

        $this->assertTrue(ResearchAchievement::whereName('bài báo khoa học test')->exists());
    }

    /** @test */
    public function can_index_science_article()
    {
        $user = $this->user;
        $this->actingAs($user)->get(route('admin.research.achievement.science-article.index'))->assertOk();
    }

    

    
}
