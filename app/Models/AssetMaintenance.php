<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use OwenIt\Auditing\Contracts\Auditable;

class AssetMaintenance extends BaseModel implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    protected $table = 'asset_maintenance';
    protected $guarded = ['*'];
    protected $fillable = ['number_report', 'asset_id','implementation_date',
        'estimated_completion_date','user_info_id','asset_provider_id',
        'finished','estimated_cost','content_repair','place','status',
        'content_maintenance_id','detailed_description','implementation_quantity'
        ];


    public function user() {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }

    public function provider()
    {
        return $this->belongsTo(AssetProvider::class, 'asset_provider_id');
    }

    public function asset() {
        return $this->belongsTo(Asset::class, 'asset_id');
    }

    public function rule_maintenance() {
        return $this->belongsTo(AssetRuleMaintenance::class, 'content_maintenance_id');
    }

    private static $searchable = [
        'number_report',
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }
}
