<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.news.category.index');
    }
}
