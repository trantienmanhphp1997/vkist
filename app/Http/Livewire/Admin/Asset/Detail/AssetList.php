<?php

namespace App\Http\Livewire\Admin\Asset\Detail;

use App\Enums\EAssetSituation;
use App\Enums\ETypeUpload;
use App\Http\Livewire\Base\BaseLive;
use App\Imports\AssetImport;
use App\Models\Asset;
use App\Models\AssetMaintenance;
use App\Models\ClaimForm;
use App\Models\File;
use App\Models\User;
use App\Models\UserInf;
use Carbon\Carbon;
use App\Enums\EAssetStatus;
use App\Enums\EClaimFormStatus;
use Illuminate\Support\Facades\Config;
use DB;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Excel;

use App\Models\AssetCategory;
use App\Models\AssetProvider;
use App\Models\AssetAllocatedRevoke;
use App\Models\Department;
use App\Models\MasterData;
use App\Models\UserInfo;
use Illuminate\Support\Facades\Auth;
use App\Exports\AssetErrorExport;
use Illuminate\Support\Facades\Storage;
use DateTime;


use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use App\Enums\EWorkingStatus;
use App\Component\FileUpload;
use App\Models\AssetOrigin;

class AssetList extends BaseLive
{
    use WithFileUploads;

    public $rowId = '';
    public $idAssets = [];
    public $claim_date;
    public $content;
    public $receiver_code;
    public $manager_code;
    public $form_id;
    public $dataInput;
    public $receiver_id = '';
    public $manager_id = '';
    public $department;
    public $reason;
    public $assetStatus=[];
    public $formStatus=[];


    public $fileErrorMessage = [];
    public $fileSuccessMessage = [];
    public $numItem = 0;
    public $numItemSaved = 0;
    public $numItemData = 0;
    public $type;
    public $folder;
    public $fileExcel;
    public $error;

    public $assetSituation;
    public $assetStatuss;
    public $deleteId;

    public $departments = [];
    public $assetCategories = [];
    public $assetOrigins = [];
    public $owners = [];

    public $searchDepartment;
    public $searchAssetCategory;
    public $searchOrigin;
    public $searchOwner;

    protected $rules = [
        'claim_date' => 'required',
        'content' => 'required|max:255',
        'receiver_id' => 'required',
        'manager_id' => 'required',
        'reason'=>'max:255',
    ];
    protected $listeners = [
        'emitDeleteFile',
        'setDepartment11',
        'getAssetIds',
        'resetUser',
        'updateAsset'=>'render',
        'getError',
        'loadStatus'=>'render'
    ];


    public function mount(UserInf $user)
    {
        $this->dataInput = $user;

        $this->assetSituation = [
            EAssetSituation::SITUATION_NOT_USE =>__('data_field_name.asset.no_use'),
            EAssetSituation::SITUATION_USING => __('data_field_name.asset.using'),
        ];
        $this->assetStatuss = EAssetStatus::getList();

        $this->departments = Department::all()->mapWithKeys(fn ($item) => [$item['id'] => $item['name']])->all();
        $this->assetCategories = AssetCategory::all()->mapWithKeys(fn ($item) => [$item['id'] => $item['name']])->all();
        $this->assetOrigins = AssetOrigin::all()->mapWithKeys(fn ($item) => [$item['id'] => $item['origin_name']])->all();
        $this->owners = UserInf::all()->mapWithKeys(fn ($item) => [$item['id'] => $item['fullname']])->all();
    }

    public function render()
    {
        $dataItem = [];
        $dataForms = [];
        $guarantee_date = '';
        $dataItems = Asset::leftJoin('user_info', 'user_info.id', '=', 'asset.user_info_id')
            ->leftJoin('department', 'department.id', '=', 'user_info.department_id')
            ->leftJoin('master_data as a', 'a.id', '=', 'user_info.position_id')
            ->select('asset.*', 'fullname', DB::raw('department.name as depart_name'), DB::raw('a.v_value as position_name'))->get();
        if($this->fileExcel) {
            $this->resetValidation('fileExcel');
        }
        if ($this->rowId) {
            $dataItem = $dataItems->findOrFail($this->rowId);
            $finish_time = Carbon::parse($dataItem->guarantee_date);
            $start_time = Carbon::parse($dataItem->buy_date);
            $day = $finish_time->diffInDays($start_time, true);
            $guarantee_date = $this->changeDate($day);
        }
        if ($this->idAssets) {
            $dataForms = Asset::leftJoin('user_info', 'user_info.id', '=', 'asset.user_info_id')
                ->leftJoin('asset_category', 'asset_category.id', '=', 'asset.category_id')
                ->leftJoin('department', 'department.id', '=', 'user_info.department_id')
                ->leftJoin('master_data as a', 'a.id', '=', 'user_info.position_id')->whereIn('asset.id',$this->idAssets)
                ->select('asset.*', 'fullname',DB::raw('asset_category.name as cate_name'), DB::raw('department.name as depart_name'), DB::raw('a.v_value as position_name'))->get();
        }
        $query = Asset::query()->with('department', 'category', 'originAsset', 'user');
        if (strlen($this->searchTerm)) {
            $query->where('asset.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }

        if (!empty($this->searchAssetCategory)) {
            $query->where('asset.category_id', $this->searchAssetCategory);
        }

        if (!empty($this->searchDepartment)) {
            $query->where('asset.department_id', $this->searchDepartment);
        }

        if (!empty($this->searchOrigin)) {
            $query->where('origin', $this->searchOrigin);
        }

        if (!empty($this->searchOwner)) {
            $query->where('user_info_id', $this->searchOwner);
        }

        $data = $query->orderBy('asset.id', 'desc')->paginate(25);

        $total = 0;
        foreach ($data as $val){
            $total +=$val->quantity*$val->asset_value;
        }

        $model_name = Asset::class;
        $type_file = Config::get('common.type_upload.Asset');
        $folder_file = app($model_name)->getTable();
        $model_id = null;
        $model_form = ClaimForm::class;
        $type_form = ETypeUpload::FORM_CLAIM;
        $folder_form = app($model_form)->getTable();
        $formId = $this->form_id;
        return view('livewire.admin.asset.detail.asset-list', [
                'model_name' => $model_name,
                'type' => $type_file,
                'model_id' => $model_id,
                'folder' => $folder_file,
                'model_form' => $model_form,
                'type_form' => $type_form,
                'form_id' => $formId,
                'folder_form' => $folder_form,
                'status' => -1,
                'data' => $data,
                'dataItem' => $dataItem,
                'guarantee_date' => $guarantee_date,
                'dataForms' => $dataForms,
                'total'=>$total
            ]
        );
    }

    public function deleteIdAsset($id)
    {
        $this->deleteId = $id;
    }

    public function delete(){
        if($this->deleteId){
            $asset_lost = AssetAllocatedRevoke::where('asset_id',$this->deleteId)->get()->toArray();
            $asset_maintain = AssetMaintenance::where('asset_id',$this->deleteId)->get()->toArray();

            if (empty($asset_lost) && empty($asset_maintain)){
                Asset::where('id', $this->deleteId)->delete();
                $this->emit('closeDelete');
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
                    __('notification.common.success.delete')] );
            }else{
                $this->emit('closeDelete');
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" =>
                    __('notification.common.fail.no_delete_has_mark')] );
            }

        }
    }
    public function changeDate($day)
    {
        $year = 0;
        $month = 0;
        if ($day >= 365) {
            $year = (int)($day / 365);
            $day = $day % 365;
        }
        if ($day >= 30) {
            $month = (int)($day / 30);
            $day = $day % 30;
        }
        return ($year ? $year . ' năm' : '') .
            ($month ? ', ' . $month . ' tháng' : '') .
            ($day ? ', ' . $day . ' ngày' : '');
    }

    public function saveForm($type, $status)
    {

        $validate=[
            'claim_date' => 'required',
            ];
        if(in_array($type,[EAssetStatus::SITUATION_RECALLED, EAssetStatus::STATUS_ERROR, EAssetStatus::STATUS_RECALLE])) {
            $validate=array_merge($validate,['content' => 'required|max:255']);
        }
        if(in_array($type,[AssetStatus::STATUS_TRANSFER, AssetStatus::STATUS_TRANSFER_SERIES, AssetStatus::STATUS_RECALL_SERIES,])) {
            $validate=array_merge($validate,['receiver_id' => 'required']);
        }
        if(in_array($type,[AssetStatus::STATUS_TRANSFER_SERIES, AssetStatus::STATUS_RECALL_SERIES,])) {
            $validate=array_merge($validate,['reason' => 'max:255']);
        }
        $this->validate($validate);

        $form = ClaimForm::create([
            'claim_date' => $this->claim_date,
            'content' => $this->content,
            'type' => $type,
            'receiver_id' => $this->receiver_id,
            'manager_id' => $this->manager_id,
            'reason'=>$this->reason
        ]);
        if ($this->rowId&&!$this->idAssets) {
            $form->assetForm()->attach($this->rowId);
        }
        if ($status) {
            File::where('admin_id', auth()->id())->where('model_name', ClaimForm::class)->where('type', ETypeUpload::FORM_CLAIM)->where('model_id', null)->update([
                'model_id' => $form->id
            ]);
        }
        if($this->idAssets){
            $form->assetForm()->attach($this->idAssets);
            $this->emit('close-transfer-series');
            $this->emit('close-recall-series');
        }
        $this->form_id = $form->id;
        $this->emit('close-lost-asset');
        $this->emit('close-error-asset');
        $this->emit('close-transfer-asset');
        $this->emit('close-recall-asset');
        $this->resetInput();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);

    }

    public function resetReceiver(){
        $this->receiver_code='';
    }
    public function resetManager(){
        $this->manager_code='';
    }
    public function resetInput()
    {
        $this->claim_date = '';
        $this->content = '';
        $this->receiver_id = '';
        $this->receiver_code='';
        $this->department='';
        $this->manager_code='';
    }

    public function cancel()
    {
        File::where('admin_id', auth()->id())->where('model_name', ClaimForm::class)->where('type', ETypeUpload::FORM_CLAIM)->where('model_id', null)->forceDelete();
        $this->resetInput();
    }

    public function emitDeleteFile()
    {
        $this->emit('updateFile');
        $this->cancel();
        $this->resetInput();
    }

    public function setDepartment11($id)
    {
        if (isset($id['receiver_id'])){
            $this->receiver_id = $id['receiver_id'];
            $this->receiver_code = UserInf::findOrFail($id['receiver_id'])->code;
            $user = UserInf::leftJoin('department', 'department.id', '=', 'user_info.department_id')->findOrFail($this->receiver_id);
            $this->department = $user->name;
        }
        if (isset($id['manager_id'])){
            $this->manager_id = $id['manager_id'];
            $this->manager_code = UserInf::findOrFail($id['manager_id'])->code;
        }

    }

    //lấy nhiều dòng dl
    public function getAssetIds($ids)
    {
        $this->idAssets = $ids;
    }
    public function uploadAsset(){
        $files=File::where('model_name',Asset::class)->
                where('type', Config::get('common.type_upload.Asset'))
                    ->where('status',0)->get();
        if($files){
            foreach ($files as $file){
                Excel::import(new AssetImport, storage_path('app/'.$file->url));
            }
        }
    }

    public function validateData() {
    	$this->validate([
            'fileExcel' => ['required'],
        ],[
            'fileExcel.required' => __('data_field_name.asset.file_required'),
        ],[]);
    }
    public function saveData() {
    	$this->validateData();
        $this->fileErrorMessage = [];
    	$this->importTimeSheets($this->fileExcel);
    }
    public function importTimeSheets($file) {
        return DB::transaction(function() use ($file) {

            if ($file->getMimeType() == 'application/vnd.ms-excel') {
                $excelReader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            } else {
                $excelReader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }

            $spreadsheet = $excelReader->load($file->getRealPath());
            $worksheet = $spreadsheet->getSheet(0);
            $rowIterator = $worksheet->getRowIterator();
            if (count($worksheet->getMergeCells()) != 3 &&Str::lower($spreadsheet->getActiveSheet()->getCell('A1')->getCalculatedValue())!="thông tin chung") {
                $this->emit('closeModalImport');
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.importError')] );
            	return;
            }

            function(\PhpOffice\PhpSpreadsheet\Worksheet\Row $row) {
                $is_row_empty = true;
                foreach ($row->getCellIterator() as $cell) {
                    if ($cell->getValue() !== null && $cell->getValue() !== "") {
                        $is_row_empty = false;
                        break;
                    }
                }
                return $is_row_empty;
            };
            $dataImport = [];
            // get data
            while ($rowIterator->valid()) {
                $this->numItem ++;
                if (!in_array($rowIterator->current()->getRowIndex(), [1, 2])) {
                    $data = [];
                    $cellIterator = $rowIterator->current()->getCellIterator();
                    $checkCategory = 1;
                    $checkCategoryName = 1;
                    $checkCode = 1;
                    $checkName = 1;
                    $checkBuyDate = 1;
                    $checkDepartment = 1;
                    $checkManage = 1;
                    for($i = 0; $i < 31; $i++) {
                        $cellValue = trim((string)$cellIterator->current()->getFormattedValue());
                        array_push($data, $cellValue);
                        if ($cellIterator->getCurrentColumnIndex() == 1 && $cellValue == null && $cellValue == "") {
                                $checkCategory = 0;
                        }
                        if ($cellIterator->getCurrentColumnIndex() == 2 && $cellValue == null && $cellValue == "") {
                                $checkCategoryName = 0;
                        }
                        if ($cellIterator->getCurrentColumnIndex() == 3 && $cellValue == null && $cellValue == "") {
                                $checkCode = 0;
                        }
                        if ($cellIterator->getCurrentColumnIndex() == 4 && $cellValue == null && $cellValue == "") {
                                $checkName = 0;
                        }
                        if ($cellIterator->getCurrentColumnIndex() == 10 && $cellValue == null && $cellValue == "") {
                                $checkBuyDate = 0;
                        }
                        if ($cellIterator->getCurrentColumnIndex() == 15 && $cellValue == null && $cellValue == "") {
                                $checkDepartment = 0;
                        }
                        if ($cellIterator->getCurrentColumnIndex() == 16 && $cellValue == null && $cellValue == "") {
                                $checkManage = 0;
                        }
                        $cellIterator->next();
                    }
                    if(!$checkCategory&&!$checkCategoryName&&!$checkCode&&!$checkName&&!$checkBuyDate&&!$checkDepartment&&!$checkManage){
                        $data = [];
                    }
                    array_push($dataImport,$data);
                }
                $rowIterator->next();
            }
            for($i = count($dataImport)-1; $i >=0; $i--){
                if($dataImport[$i]==[]){
                    unset($dataImport[$i]);
                } else {
                    break;
                }
            }
            for($i = count($dataImport)-1; $i >=0; $i--){
                if($dataImport[$i]==[]){
                    for($j=0; $j<31; $j++){
                        array_push($dataImport[$i],'');
                    }
                }
            }

            $adminID = Auth::user()->id;
            $AssetCategory = AssetCategory::select('code')->get();
            $AssetCategory = json_decode(json_encode($AssetCategory), true);
            $newAssetCategory = [];
            // check asset code
            $AssetCode = Asset::pluck('code','id')->toArray();
            $AssetCode = array_map('mb_strtolower', $AssetCode);
            $ManagerCode = UserInf::pluck('code','id')->toArray();
            $ManagerCode = array_map('mb_strtolower', $ManagerCode);
            $AssetListSerial = Asset::pluck('series_number','id')->toArray();
            $AssetListSerial = array_map('mb_strtolower', $AssetListSerial);

            $checkError = false;
            $errorUniqueAssetCode = '';
            $errorCategoryID = '';
            $errorCategoryName = '';
            $errorAssetcode = '';
            $errorAssetName = '';
            $errorAssetBuyDate = '';
            $errorDepartmentName = '';
            $errorAssetManage = '';
            $errorUserInfo = '';
            $errorSerial = '';
            foreach($dataImport as $key => $value){
                $asset_code = array_search(Str::lower($value[2]),$AssetCode);
                if($asset_code) {
                    $checkError = true;
                    $errorUniqueAssetCode = $errorUniqueAssetCode?$errorUniqueAssetCode.', '.($key+1):($key+1);
                }

                if(!$value[0]){
                    $checkError = true;
                    $errorCategoryID = $errorCategoryID?$errorCategoryID.', '.($key+1):($key+1);
                }
                if(!$value[1]){
                    $checkError = true;
                    $errorCategoryName = $errorCategoryName?$errorCategoryName.', '.($key+1):($key+1);
                }
                if(!$value[2]){
                    $checkError = true;
                    $errorAssetcode = $errorAssetcode?$errorAssetcode.', '.($key+1):($key+1);
                }
                if(!$value[3]){
                    $checkError = true;
                    $errorAssetName = $errorAssetName?$errorAssetName.', '.($key+1):($key+1);
                }
                if(!$value[9]){
                    $checkError = true;
                    $errorAssetBuyDate = $errorAssetBuyDate.' '.($key+1);
                }
                if(!$value[14]){
                    $checkError = true;
                    $errorDepartmentName = $errorDepartmentName?$errorDepartmentName.', '.($key+1):($key+1);
                }
                if(!$value[15]){
                    // true
                    $checkError = true;
                    $errorAssetManage = $errorAssetManage?$errorAssetManage.', '.($key+1):($key+1);
                }
                if($value[22]){
                    $indexID = array_search(Str::lower($value[22]),$ManagerCode);
                    if(!$indexID) {
                        $checkError = true;
                        $errorUserInfo = $errorUserInfo?$errorUserInfo.', '.($key+1):($key+1);
                    }
                }
                if($value[12]){
                    $indexID = array_search(Str::lower($value[12]),$AssetListSerial);
                    if($indexID) {
                        $checkError = true;
                        $errorSerial = $errorSerial?$errorSerial.', '.($key+1):($key+1);
                    }
                }
            }
            // check trùng bên trong bảng file excel
            $errorUniqueSerialInFile = '';
            $count = count($dataImport);
            $duplicate = [];
            $checkFirst = 1;
            for ($i = 0; $i < $count; $i++) {
                if($dataImport[$i][12]){
                    $check = 1;
                    for ($j = 0; $j < $count; $j++) {
                        if($dataImport[$j][12]){
                            if (Str::lower($dataImport[$j][12]) == Str::lower($dataImport[$i][12]) && $i != $j) {
                                if(!in_array($i,$duplicate)&&$check){
                                    array_push($duplicate,$i);
                                    $check = 0;
                                    $checkError = true;
                                    if($checkFirst) {
                                        $errorUniqueSerialInFile = $errorUniqueSerialInFile.'('.($i+1);
                                        $checkFirst = 0;
                                    }
                                    else{
                                        $errorUniqueSerialInFile = $errorUniqueSerialInFile.'; ('.($i+1);
                                    }
                                }

                                if(!in_array($j,$duplicate)){
                                    array_push($duplicate,$j);
                                    $errorUniqueSerialInFile = $errorUniqueSerialInFile.', '.($j+1);
                                }
                            }
                        }
                    }
                    if($check==0) {
                        $errorUniqueSerialInFile = $errorUniqueSerialInFile.')';
                    }
                }
            }

            if(!$checkError){
                // tìm kiếm danh mục sản phẩm
                foreach($dataImport as $key => $value){
                    $category = [
                        'code' => $value[0],
                    ];
                    if(!in_array($category,$AssetCategory)&&!in_array($category,$newAssetCategory)){
                        array_push($newAssetCategory,$category);
                        $NewCategory = new AssetCategory();
                        $NewCategory->code = $value[0];
                        $NewCategory->name = $value[1];
                        $NewCategory->admin_id = $adminID;
                        $NewCategory->save();
                    }
                }
                $AssetCategory = AssetCategory::pluck('code','id')->toArray();
                $AssetCategory = array_map('mb_strtolower', $AssetCategory);
                $AssetProvider = AssetProvider::pluck('name','id')->toArray();
                $AssetProvider = array_map('mb_strtolower', $AssetProvider);
                $Department = Department::pluck('name','id')->toArray();
                $Department = array_map('mb_strtolower', $Department);
                $Manager = UserInf::pluck('fullname','id')->toArray();
                $Manager = array_map('mb_strtolower', $Manager);
                $Unit = MasterData::where('type',20)->pluck('v_value','id')->toArray();
                $Unit = array_map('mb_strtolower', $Unit);
                $dem = 0;
                foreach($dataImport as $key => $value){
                    $dem++;
                    $category_id = array_search(Str::lower($value[0]),$AssetCategory);
                    if(!$category_id) {
                        $category_id = null;
                    }

                    $unit_id = array_search(Str::lower($value[5]),$Unit);
                    if(!$unit_id) {
                        $unit_id = null;
                    }

                    $origin_id = 0;
                    if($value[8]){
                        if(strpos(Str::lower($value[8]),'vod')!==false){
                            $origin_id = 1;
                        }
                        else {
                            $origin_id = 2;
                        }
                    }

                    $asset_provider_id = array_search(Str::lower($value[11]),$AssetProvider);
                    if(!$asset_provider_id) {
                        $asset_provider_id = null;
                    }

                    $department_id = array_search(Str::lower($value[14]),$Department);
                    if(!$department_id) {
                        $department_id = null;
                    }

                    $manager_id = array_search(Str::lower($value[15]),$Manager);
                    if(!$manager_id) {
                        $manager_id = null;
                    }

                    if ($value[20]) {
                        $value[20] = ['maintenance_date'=>(int) $value[20], 'maintenance_type_date' => 2];
                        $value[20] = json_encode($value[20]);
                    } else {
                        $value[20]=null;
                    }

                    if ($value[26]) {
                        $value[26] = ['distribute_number'=>(int) $value[26], 'distribute_number_type' => 2];
                        $value[26] = json_encode($value[26]);
                    } else {
                        $value[26]=null;
                    }
                    if ($value[27]) {
                        $value[27] = ['distribute_left_number'=>(int) $value[27], 'distribute_left_number_type' => 2];
                        $value[27] = json_encode($value[27]);
                    } else {
                        $value[27]=null;
                    }

                    // xử lý ngày
                    $value[9] = $value[9]?reFormatDate($value[9], 'Y-m-d'):null;
                    $value[17] = $value[17]?reFormatDate($value[17], 'Y-m-d'):null;
                    $value[18] = $value[18]?reFormatDate($value[18], 'Y-m-d'):null;
                    $value[29] = $value[29]?reFormatDate($value[29], 'Y-m-d'):null;
                    $value[21] = $value[21]?reFormatDate($value[21], 'Y-m-d'):null;
                    if($category_id){
                        $Asset = new Asset();
                        $Asset->category_id = $category_id;
                        $Asset->code = $value[2];
                        $Asset->name = $value[3];
                        $Asset->quantity = (int) $value[4];
                        $Asset->unit_id = $unit_id;
                        $Asset->asset_value = $value[6]?$value[6]:null;
                        $Asset->origin = $origin_id;
                        $Asset->buy_date = $value[9];
                        $Asset->contract_number = $value[10];
                        $Asset->asset_provider_id = $asset_provider_id;
                        $Asset->series_number = $value[12]?$value[12]:null;
                        $Asset->unit_type = $value[13];
                        $Asset->department_id = $department_id;
                        $Asset->manager_id = $manager_id;
                        $Asset->note = $value[16];
                        $Asset->guarantee_date = $value[17];
                        $Asset->expiry_warranty_date = $value[18];
                        $Asset->condition_warranty = $value[19];
                        $Asset->maintenance_date_json = $value[20];
                        $Asset->distribute_value = $value[25]?$value[25]:null;
                        $Asset->distribute_number_times_json = $value[26];
                        $Asset->distribute_left_times_json = $value[27];
                        $Asset->distribute_value_done = $value[28]?$value[28]:null;
                        $Asset->distribute_start_date = $value[29];
                        $Asset->distribute_value_wait = $value[30]?$value[30]:null;
                        // trạng thái sử dụng
                        $Asset->situation = \App\Enums\EAssetSituation::SITUATION_NOT_USE;
                        $Asset->save();
                    }

                    // xử lý lưu vào bảng asset_allocated_revoke
                    if($value[22]&&$category_id){
                        $user_info_id = array_search(Str::lower($value[22]),$ManagerCode);
                        // nếu chỉ tồn tại 1 người thì tạo ms
                        if($user_info_id){
                            $Asset->amount_used = 1;
                            $Asset->user_info_id = $user_info_id;
                            // trạng thái sử dụng
                            $Asset->situation = \App\Enums\EAssetSituation::SITUATION_USING;

                            $AssetAllocatedRevoke = new AssetAllocatedRevoke();
                            $AssetAllocatedRevoke->asset_id = $Asset->id;
                            $AssetAllocatedRevoke->user_info_id = $user_info_id;
                            $AssetAllocatedRevoke->number_report = $value[24];
                            $AssetAllocatedRevoke->implementation_date = $value[21];
                            // số lượng cấp phát cho người dùng
                            $AssetAllocatedRevoke->implementation_quantity = 1;
                            $AssetAllocatedRevoke->save();
                            $Asset->save();
                        }
                    }

                }
            }
            $this->fileExcel = null;
            $this->emit('closeModalImport');
            if(!$checkError) {
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.import')] );
            } else {
                $this->error = [];
                array_push($this->error, [
                    'errorCategoryID' => $errorCategoryID
                ]);
                array_push($this->error, [
                    'errorCategoryName' => $errorCategoryName
                ]);
                array_push($this->error, [
                    'errorAssetcode' => $errorAssetcode
                ]);
                array_push($this->error, [
                    'errorAssetName' => $errorAssetName
                ]);
                array_push($this->error, [
                    'errorAssetBuyDate' => $errorAssetBuyDate
                ]);
                array_push($this->error, [
                    'errorDepartmentName' => $errorDepartmentName
                ]);
                array_push($this->error, [
                    'errorAssetManage' => $errorAssetManage
                ]);
                array_push($this->error, [
                    'errorUniqueAssetCode' => $errorUniqueAssetCode
                ]);
                array_push($this->error, [
                    'errorUserInfo' => $errorUserInfo
                ]);
                array_push($this->error, [
                    'errorSerial' => $errorSerial
                ]);
                array_push($this->error, [
                    'errorUniqueSerialInFile' => $errorUniqueSerialInFile
                ]);
                $this->error = json_encode($this->error);
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.import')]);
                $this->emit('openModalResult');
            }
        });
    }
    public function deleteFile() {
        $this->fileExcel = null;
        $this->fileErrorMessage = [];
    }
    public function getError($data){
        $today = date("d-m-Y");
        return Excel::download(new AssetErrorExport($data), 'Asset_error('.$today.').xlsx');
    }

    public function getExampleFile(){
        return Storage::download('public/asset.xls');
    }

}
