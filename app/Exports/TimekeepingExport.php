<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use App\Enums\EWorkingStatus;
use Illuminate\Support\Collection;
class TimekeepingExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($timeSheetsDaily, $filter_month_year, $user) {
        $this->timeSheetsDaily = $timeSheetsDaily;
        $this->filter_month_year = $filter_month_year;
        $this->columnTable = [mb_strtoupper(__('executive/my-time-sheets.personal-info.name'), 'UTF-8')];
        $this->user = $user;
    }
    public function collection()
    {
        $timekeeping = [$this->user->fullname];
        if (!empty($this->timeSheetsDaily)) {
            foreach ($this->timeSheetsDaily as $item) {
               array_push($timekeeping, EWorkingStatus::valueToName($item->working_status));
            }
        }
        return collect([$timekeeping]);
    }
    public function headings():array
    {
        for ($i = 0; $i < 31; $i++) { 
            array_push($this->columnTable, __('common.day2', ['day' => $i + 1]));
        }
        return $this->columnTable;
    }

    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'EF'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('H1:N1');
            $event->sheet->setCellValue('H1', mb_strtoupper(__('executive/my-time-sheets.time-sheets', ['month' => $this->filter_month_year])));
            $event->sheet->getStyle('H1:N1')->applyFromArray(
                $default_font_style_title
            );
            $status = [
                'K', 'P', 'X', 'L', 'H', 'R', 'Ro', 'Co', 'D', 'O', 'T'
            ];
            $event->sheet->setCellValue('A8', mb_strtoupper(__('executive/my-time-sheets.symbol-of-timesheets.title'), 'UTF-8'));
            for ($i = 0; $i < count($status); $i++) {
                $index = 9 + $i;
                $event->sheet->setCellValue('A' . $index, EWorkingStatus::nameToLocale($status[$i]));
                $event->sheet->setCellValue('B' . $index, $status[$i]);
            }
            // title
            $cellRange = 'A3:AE3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle('B4:AE4')->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        return mb_strtoupper(__('executive/my-time-sheets.time-sheets', ['month' => $this->filter_month_year]));
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
