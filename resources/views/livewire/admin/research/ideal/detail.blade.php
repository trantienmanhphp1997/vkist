<div class="group-tabs">
	<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane fade active show" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
		    <div class="row">
		        <div class="col-md-12">
		            <div class="form-group">
		                <label>
		                    {{__('data_field_name.ideal.content')}}
		                    <span class="text-danger">*</span>
		                </label>
		                <textarea class="form-control" rows="6" disabled="" wire:model.lazy="content" placeholder="{{__('data_field_name.ideal.content')}}"></textarea>
		                @error('content')
		                    <div class="text-danger mt-1">{{$message}}</div>
		                @enderror
		            </div>
		        </div>
		        <div class="col-md-12">
		            <div class="form-group">
		                <label>
		                    {{__('data_field_name.ideal.result')}}
		                    <span class="text-danger">*</span>
		                </label>
		                <textarea class="form-control" disabled="" rows="6" wire:model.lazy="result" placeholder="{{__('data_field_name.ideal.result')}}"></textarea>
		                @error('result')
		                    <div class="text-danger mt-1">{{$message}}</div>
		                @enderror
		            </div>
				</div>
				<div class="col-md-12">
						<div class="form-group">
							<label>
								{{__('data_field_name.ideal.research_field')}}
								<span class="text-danger">*</span>
							</label>
						<input type="text" class="form-control" disabled="" value="{{$ideal->researchField->name}}" />
						</div>
					</div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label>
		                	{{__('data_field_name.ideal.execution_time')}}
		                	<span class="text-danger">*</span>
		                </label>
		                <input type="text" name="execution_time" disabled="" class="form-control" wire:model.lazy="execution_time" placeholder="{{__('data_field_name.ideal.execution_time')}}">
		                @error('execution_time')
		                    <div class="text-danger mt-1">{{$message}}</div>
		                @enderror
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label>
		                    {{__('data_field_name.ideal.fee')}}
		                    <span class="text-danger">*</span>
		                </label>
		                <input type="text" name="fee" disabled="" class="form-control" wire:model.lazy="fee" placeholder="{{__('data_field_name.ideal.fee')}}">
		                @error('fee')
		                    <div class="text-danger mt-1">{{$message}}</div>
		                @enderror
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>