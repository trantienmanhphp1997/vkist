<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkPlanHasUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_plan_has_user_info', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('du an');
            $table->bigInteger('work_plan_id')->nullable()->comment('map voi work_plan');
            $table->bigInteger('position_id')->nullable()->comment('map voi master_data');
            $table->dateTime('working_day')->nullable();
            $table->tinyInteger('type')->nullable()->comment('1 sang; 2 chieu, 3 toi');
            $table->string('content', 1000)->nullable()->comment('noi dung cong tac');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('map với user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_plan_has_user_info');
    }
}
