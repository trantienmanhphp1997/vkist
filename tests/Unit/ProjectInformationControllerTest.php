<?php

namespace Tests\Unit;

use App\Http\Livewire\Admin\ResearchProject\Information\Tabs\ProjectTab;
use App\Http\Livewire\Admin\System\Role\RoleList;
use App\Models\ResearchProject;
use App\Models\Topic;
use App\Models\ResearchCategory;
use Database\Seeders\UserDBSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ProjectInformationControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function testIndexProject()
    {
        $this->actingAs($this->user)->get(route('admin.research-project.information.index'))->assertOk();
    }

    /** @test */
    public function testGetDraftListProject()
    {
        $this->actingAs($this->user)->get(route('admin.research-project.information.draft.index'))->assertOk();
    }

    /** @test */
    public function testCreateProject()
    {
        $this->actingAs($this->user)->get(route('admin.research-project.information.create'))->assertOk();
    }

    /** @test */
    public function testSaveProjectFail() {
        $this->actingAsAdmin()->get(route('admin.system.account.create'))->assertOk();

        $data = [
            'name' => '',
            'project_code' => 'TEST',
            'contract_code' => 'TEST',
            'research_category_id' => 1,
            'start_date' => '2021/09/08',
            'end_date' => '2021/09/08',
            'description' => 'description',
            'project_scale' => '11',
            'status' => 1
        ];

        Livewire::test(ProjectTab::class)
            ->fill($data)
            ->call('save')
            ->assertHasErrors(['name']);
    }

    /** @test */
    public function testSaveProjectSuccess() {
        $this->actingAsAdmin()->get(route('admin.system.account.create'))->assertOk();

        $category = ResearchCategory::create([
            'name' => 'Research Category',
            'description' => 'Research Category 1',
            'note' => 'Research Category 2',
            'type' => 1,
        ]);

        $data = [
            'name' => 'Project',
            'project_code' => 'AF74',
            'contract_code' => 'AFOTY74',
            'research_category_id' => $category->id,
            'start_date' => '2022/09/09',
            'end_date' => '2022/09/18',
            'description' => 'description',
            'project_scale' => '11',
            'status' => 1
        ];

        $livewire = Livewire::test(ProjectTab::class)
            ->fill($data)
            ->call('save')
            ->assertHasNoErrors();

        $project = ResearchProject::first();

        $livewire->assertRedirect(route('admin.research-project.information.edit', $project->id));
    }
}
