<div>
    <div class="row">
        <div class="col-md-6">
            <span>{{__('data_field_name.task_work_has_result.achieve_expected_results')}}</span>
        </div>
        <div class="col-md-6">
            <button style="float:right" wire:click="resetInputFields()"  id="show_modal_create"  class="border-0 p-0 bg-white mr-3 "
                data-toggle="modal" data-target="#createModal" title="{{ __('common.button.create') }}"><img
                    src="/images/filteradd.png" alt="filteradd"></button>
        </div>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-general" id="coUser" id="businessFee">
            <thead>
                <tr class="title-approve">
                    <th scope="col" class="text-center border-radius-left">{{__('data_field_name.task_work_has_result.stt')}}</th>
                    <th scope="col" class="text-center">{{__('data_field_name.task_work_has_result.name')}}</th>
                    <th scope="col" class="text-center">{{__('data_field_name.task_work_has_result.file')}}</th>
                    <th scope="col" class="text-center border-radius-right">{{__('data_field_name.task_work_has_result.action')}}</th>
                </tr>
            </thead>
            <div wire:loading class="loader"></div>
            <tbody>
                <?php
                $i = 0;
                ?>
                @forelse($data as $row)
                    <tr>
                        <td class="text-center">{{ ++$i }}</td>
                        <td class="text-center">{{ $row->name }}</td>
                        <td class="text-center">
                            <button type="button" class="btn btn-sm" wire:click="getListFile({{ $row->id }})">
                                <img src="/images/fileIcon.png" alt="file" width="50px">
                            </button>
                        </td>
                        <td class="text-center">
                            <button type="button" data-toggle="modal" id="show_modal_edit" data-target="#createModal"
                                title="{{ __('common.button.edit') }}" wire:click="edit({{ $row->id }})"
                                class="btn-sm border-0 bg-transparent mr-1 show_modal_edit"><img
                                    src="/images/edit.png" alt="edit"></button>
                            <button type="button" data-toggle="modal" data-target="#deleteModal"
                                title="{{ __('common.button.delete') }}" wire:click="deleteId({{ $row->id }})"
                                class="btn-sm border-0 bg-transparent"><img src="/images/trash.svg" alt="trash"></button>

                        </td>
                    </tr>
                @empty
                    <tr class="text-center text-danger">
                        <td colspan="12">{{ __('common.message.no_data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {{-- Popup Upload File --}}
        <div wire:ignore.self class="modal fade" id="downloadFileModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            style="position: absolute;right:15px;top:15px">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                        <h5 class="modal-title text-uppercase">
                            {{ __('data_field_name.user.attach_documents') }}
                        </h5>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-3">
                            @forelse ($listFile as $val)
                                <div class="form-group col-md-12">
                                    <div class="attached-files"
                                        wire:click="download('{{ $val->url }}', '{{ $val->file_name }}')"
                                        style="cursor: pointer">
                                        <img src="/images/File.svg" alt="file">
                                        <div class="content-file">
                                            <p>{{ $val->file_name }}</p>
                                            <p class="kb">{{ $val->size_file }}</p>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p>{{ __('common.message.empty_search') }}</p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Popup --}}
    </div>

    {{-- Modal Create --}}
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="createModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body box-user">
                        @if ($isEdit == false)
                            <h4 class="modal-title" id="exampleModalLabel">{{__('data_field_name.task_work_has_result.create_achieve_expected_results')}}</h4>
                        @else
                            <h4 class="modal-title" id="exampleModalLabel">{{__('data_field_name.task_work_has_result.edit_achieve_expected_results')}}</h4>
                        @endif
                        <form>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{__('data_field_name.task_work_has_result.name')}}<span
                                        class="text-danger">(*)</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder=""
                                    wire:model.defer="name">
                                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{__('data_field_name.task_work_has_result.file')}}<span class="text-danger"></label>
                                @livewire('component.files',['model_name'=>$model_name,'type'=>$type,'folder'=>$folder,'model_id'=>$task_work_has_result_id],key($task_work_has_result_id
                                ??''))
                            </div>
                        </form>
                    </div>
                    <div class="group-btn2 text-center pt-12">
                        <button type="button"   id="close-modal-create-task-work-has-result"
                            class="btn btn-cancel " data-dismiss="modal">{{ __('common.button.close') }}</button>
                        @if ($isEdit == false)
                            <button type="submit" wire:click.prevent="store()"
                                class="btn btn-save">{{ __('common.button.save') }}</button>
                        @else
                            <button type="submit" wire:click.prevent="update()"
                                class="btn btn-save">{{ __('common.button.save') }}</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
    {{-- End Modal --}}
    @include('livewire.common.modal._modalDelete')
    <script>
        $("document").ready(function() {
            window.livewire.on('close-modal-create-task-work-has-result', () => {
                document.getElementById('close-modal-create-task-work-has-result').click()
            });
            window.addEventListener('openDownloadFileModal', function() {
                $('#downloadFileModal').modal('show');
            })
        })
    </script>
</div>
