<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class UserHasMenu extends Eloquent
{
    use HasFactory;
    protected $table = 'user_has_menu';
    protected $fillable = ['admin_id','menu_id'];

    public function admin(){
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }
    public function menu(){
        return $this->belongsTo(Menu::class, 'menu_id', 'id');
    }
}
