@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.approval_topic.text_edit_approval')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    @livewire('admin.approval.approval-edit', ['approval' => $approval])
@endsection
@section('js')
@endsection
