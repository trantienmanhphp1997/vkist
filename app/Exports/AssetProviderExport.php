<?php

namespace App\Exports;

use App\Models\AssetProvider;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class AssetProviderExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $id_export;
    public function __construct($id_export)
    {
        $this->id_export = $id_export;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $index = 1;
        $query = AssetProvider::with('bankName');
        if (!empty($this->id_export)) {
            $query->whereIn('id', $this->id_export);
            $assetProviders = $query->get();
            foreach ($assetProviders as $assetProvider) {
                $assetProvider->index = $index++;
            }
        } else {
            $assetProviders = collect();
        }
        return $assetProviders;
    }

    public function headings(): array
    {
        return [
            'STT',
            'Tên nhà cung cấp',
            'Mã nhà cung cấp',
            'Địa chỉ',
            'Mã số thuế',
            'Số điện thoại',
            'Website',
            'Email',
            'TK ngân hàng',
            'Tên ngân hàng',
            'Note',
        ];
    }

    public function map($assetProvider): array
    {
        return [
            $assetProvider->index,
            $assetProvider->name,
            $assetProvider->code,
            $assetProvider->address,
            $assetProvider->tax_code,
            $assetProvider->phone_number,
            $assetProvider->website,
            $assetProvider->email,
            $assetProvider->bank_account_number,
            $assetProvider->bankName->v_value ?? '',
            $assetProvider->note,
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E','F','G','H','I','J','K',
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:E1');
            $event->sheet->setCellValue('A1', 'DANH SÁCH NHÀ CUNG CẤP');
            $event->sheet->getStyle('A1:E1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:K3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        }, ];

    }
    public function title(): string
    {
        return 'DANH SÁCH NHÀ CUNG CẤP';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
