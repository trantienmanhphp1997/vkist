<div>
    <div class="inner-tab pt0">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group search-expertise search-plan">
                    <div class="search-expertise inline-block">
                        <input type="text" class="form-control" name="searchTerm" autocomplete="off"
                               wire:model.debounce.1000ms="searchTerm"
                               placeholder="{{__('data_field_name.asset.search_place')}}">
                        <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                @if(checkRoutePermission('download'))
                    <div class="form-group float-right">
                        <button type="button" id='btnOpenExportModal' style="border-radius: 11px; border:none;"
                                data-toggle="modal" data-target="#export-modal-cancel">
                            <img src="/images/filterdown.svg" alt="filterdown">
                        </button>
                    </div>
                @endif
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-general">
                <thead>
                <tr class="border-radius">
                    <th scope="col"
                        class="text-uppercase border-radius-left">{{__('data_field_name.asset.asset_code')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.number_report')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.asset_name')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.asset_cate')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.serial')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.asset_quantity')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.unit')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.status')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.date_cancel')}}</th>
                    @if (checkRoutePermission('destroy'))
                        <th class="text-uppercase border-radius-right" scope="col">
                            {{__('data_field_name.common_field.action')}}</th>
                    @endif
                </tr>
                </thead>
                <div wire:loading class="loader"></div>
                <tbody>
                @forelse($data as $val)
                    <tr>
                        <td>{!! boldTextSearch($val->asset->code,$searchTerm) !!}</td>
                        <td>
                            @if(checkRoutePermission('approve'))
                                <a style="cursor: pointer;" class="text-primary" href="#" data-toggle="modal"
                                   data-target="#confirm_notice_cancel"
                                   wire:click="confirmNotice({{ $val->id }})">
                                    {{$val->number_report ?? ''}}
                                </a>
                            @else
                                {{$val->number_report ?? ''}}
                            @endif
                        </td>
                        <td> {!! boldTextSearch($val->asset->name,$searchTerm) !!}</td>
                        <td>{{$val->asset->category->name ?? ''}}</td>
                        <td class="text-center">{{$val->asset->series_number ?? ''}}</td>
                        <td class="text-center">{{$val->implementation_quantity ?? ''}}</td>
                        <td class="text-center">{{$val->asset->unit->v_value ?? ''}}</td>
                        <td class="text-center">{{$val->status?$assetStatus[$val->status]:''}}</td>
                        <td class="text-center">{{reFormatDate($val->implementation_date,'d/m/Y') ?? ''}}</td>
                        @if (checkRoutePermission('destroy'))
                            <td class="text-center">
                                <button type="button" style="background-color: white; border:none;"
                                        wire:click="deleteIdAssetCancel({{$val->id}})" data-target="#DeleteCancel"
                                        data-toggle="modal">
                                    <img src="/images/trash.svg" alt="trash">
                                </button>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr class="text-danger">
                        <td colspan="10">
                            {{__('common.warning.no_data')}}
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        {{$data->links()}}
    </div>
    <div class="modal fade modal-fix" wire:ignore.self id="confirm_notice_cancel" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="submit">
                    <div class="modal-header">
                        <div class="row width_100">
                            <div class="col-11">
                                <h6 class="text_center">{{__('data_field_name.asset.report_asset_canel')}}</h6>
                            </div>
                            <div class="col-1">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true close-btn">×</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-header">
                        <div class="form-group info-request">
                            <label>{{__('data_field_name.asset.number_report')}}</label>
                            <input type="text" value="{{$detailNotice->number_report ?? ''}}" name="number_report"
                                   disabled>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 pd-col">
                                    <div class="info-asset">
                                        @livewire('admin.asset.lost-cancel-liquidation.asset-info')
                                    </div>
                                </div>
                                <div class="col-6 pd-col">
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.asset_quantity')}}</label>
                                        <input type="number"
                                               value="{{$detailNotice->implementation_quantity ?? ''}}" disabled>
                                        @if(!empty($errorQuantity))
                                            <div class="text-danger mb-3">{{$errorQuantity}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-6 pd-col">
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.date_cancel')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice?reFormatDate($detailNotice->implementation_date, 'd/m/Y') : ''}}"
                                               disabled>
                                    </div>
                                </div>
                                <div class="col-12 pd-col">
                                    <div class="form-group info-request">
                                        <label>
                                            {{__('data_field_name.asset.reason_cancel')}}
                                        </label>
                                        <textarea cols="30" rows="10" value="" disabled>
                                            {{$detailNotice->implementation_reason ?? ''}}
                                        </textarea>
                                    </div>
                                </div>
                                {{--                                <div class="col-12 pd-col">--}}
                                {{--                                    <div id="file_right">--}}
                                {{--                                        <label>{{__('data_field_name.asset.file')}}</label>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-cancel" id="close-confirm-cancel"
                                data-dismiss="modal">{{__('common.button.cancel')}}
                        </button>
                        <button type="button" class="btn btn-primary"
                                wire:click.prevent="confirm()">{{__('common.button.perform')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal export /-->
    <div class="modal fade" id="export-modal-cancel" tabindex="-1" aria-labelledby="delete-department-modal-label"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>
                        <h5 class="modal-title"
                            id="delete-department-modal-label">{{__('notification.member.warning.warning')}}</h5>
                    </strong>
                </div>
                <div class="modal-body">
                    {{__('notification.member.warning.Do you want to export excel file?')}}
                </div>
                <div class="modal-footer group-btn2 text-center  pt-24">
                    <button type="button" class="btn btn-cancel"
                            data-dismiss="modal">{{__('common.button.back')}}</button>
                    <button type="button" class="btn btn-save" data-dismiss="modal"
                            wire:click="export">{{__('common.button.export_file')}}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal export /-->
    <div wire:ignore.self class="modal fade" id="DeleteCancel" tabindex="-1"
         aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{__('common.confirm_message.confirm_title')}}</h4>
                    <p>{{__('common.confirm_message.are_you_sure_delete')}}</p>
                </div>
                <div class="group-btn2 text-center  pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal"
                            id="close-delete-cancel">{{__('common.button.no')}}</button>
                    <button type="button" wire:click.prevent="delete()" class="btn btn-save"
                            data-dismiss="modal">{{__('common.button.yes')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
