@extends('layouts.app')
    <title>{{__('common.change_password')}}</title>

@section('content')
<div class="box-login col-md-12">
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <a class="logo-login" href="/" >
                    <img src="../images/Logo_VKIST_Transparent_background-08f.png" style="width:180px" alt="logo">
                </a>
                <div class="bg-login">
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-7">
            <div class="login-box">
                <h2 >{{__('common.change_password')}}</h2>
                <p >{{__('common.change_password_text')}}</p>
                <form method="POST" action="{{ route('change_password.store') }}">
                    @csrf
                    {{-- <div class="form-group">
                        <span><img src="/images/mail-icon.svg" alt="mail-icon"/></span>
                        <input id="old_password" type="password" placeholder="Old password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" value="{{ old('old_password') }}" required autocomplete="old_password" autofocus>
                        @error('old_password')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div> --}}
                    <label for="old_password">{{ __('common.old_password') }}</label>
                    <div class="form-group group-password">
                        <input id="old_password" style="background:white" type="password" maxlength="50" class="password form-control @error('old_password') is-invalid @enderror" name="old_password"  autocomplete="current-password" value="{{old('old_password') ?? ''}}">
                        <img src="/images/eye.svg" alt="eye" class="eye-login eyeSlash" id="eyeSlash" style="display: none;" onclick="visibility(this)">
                        <img src="/images/eye.svg" alt="eye" class="eye-login eyeShow" id="eyeShow" onclick="visibility(this)">
                        @error('old_password')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <label for="new_password">{{ __('common.new_password') }}</label>
                    <div class="form-group group-password">
                        <input id="new_password" style="background:white" type="password" maxlength="50" class="password form-control @error('new_password') is-invalid @enderror" name="new_password"  autocomplete="current-password" value="{{old('new_password') ?? ''}}">
                        <img src="/images/eye.svg" alt="eye" class="eye-login eyeSlash" id="eyeSlash" style="display: none;" onclick="visibility(this)">
                        <img src="/images/eye.svg" alt="eye" class="eye-login eyeShow" id="eyeShow" onclick="visibility(this)">
                        @error('new_password')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <label for="confirm_password ">{{ __('common.confirm_password') }}</label>
                    <div class="form-group group-password">
                    <input id="confirm_password" style="background:white" type="password" maxlength="50" class="password form-control @error('confirm_password') is-invalid @enderror" name="confirm_password"  autocomplete="current-password" value="{{old('confirm_password') ?? ''}}">
                        <img src="/images/eye.svg" alt="eye" class="eye-login eyeSlash" id="eyeSlash" style="display: none;" onclick="visibility(this)">
                        <img src="/images/eye.svg" alt="eye" class="eye-login eyeShow" id="eyeShow" onclick="visibility(this)">
                        @error('confirm_password')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn  btn-block">{{__('common.change_password')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>  
<script>
    function visibility(element) {
        let parent = $(element).parents('.group-password');
        let input = $(parent).children('.password')
        let eyeShow = $(parent).children('.eyeShow')
        let eyeSlash = $(parent).children('.eyeSlash')
        if (input[0].type === 'password') {
            input[0].type = "text";
            $(eyeShow[0]).hide()
            $(eyeSlash[0]).show()
        }else {
            input[0].type = "password";
            $(eyeShow[0]).show()
            $(eyeSlash[0]).hide()
        }
    }
</script>
<script src="https://kit.fontawesome.com/02129e47ff.js"></script>
@endsection
