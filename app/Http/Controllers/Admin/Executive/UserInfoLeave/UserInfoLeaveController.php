<?php

namespace App\Http\Controllers\Admin\Executive\UserInfoLeave;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserInfoLeave;

class UserInfoLeaveController extends Controller
{
    //
    public function index(Request $request){
        return view('admin.executive.user-info-leave.index');
    }

    public function create(Request $request){
        return view('admin.executive.user-info-leave.create');
    }

    public function edit($id){
        $data = UserInfoLeave::findOrFail($id);
        return view('admin.executive.user-info-leave.edit', compact('data'));
    }
}
