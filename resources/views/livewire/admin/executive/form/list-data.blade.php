<div class="col-md-10 col-xl-11 box-detail box-user">
	<div class="row">
		<div class="col-md-12">
				@if(Route::currentRouteName() == 'admin.template.form.index')
					<div class="breadcrumbs"><a href="{{route('admin.template.form.index')}}">{{__('data_field_name.template_form.management')}}</a></div>
				@else
					<div class="breadcrumbs"><a href="{{route('admin.general.form.index')}}">{{__('data_field_name.template_form.management')}}</a></div>
				@endif
		</div>
	</div>
	<div class="row bd-border">
		<div class="col-md-12">
			<h3 class="title">{{__('data_field_name.template_form.list')}}</h3>
			<div class="information">
				<div class="inner-tab pt0">
					<div class="row">
						<div class="col-md-4">
							<div class=" search-expertise d-inline-block ">
								<div class="search-expertise inline-block mb-24 w-xs-100 ">
									<input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control w-xs-100" placeholder="{{__('research/ideal.filter.input.placeholder.search')}}">
									<span>
										<img src="/images/Search.svg" alt="search"/>
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<select wire:model="filterType" class="form-control">
	                            <option value=''>{{__('data_field_name.template_form.type')}}</option>
	                            <option value="{{App\Enums\ETemplateFormType::BUDGET}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::BUDGET)}}</option>
	                            <option value="{{App\Enums\ETemplateFormType::GENERAL}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::GENERAL)}}</option>
	                            <option value="{{App\Enums\ETemplateFormType::ESTIMATE}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::ESTIMATE)}}</option>
	                            <option value="{{App\Enums\ETemplateFormType::PLAN}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::PLAN)}}</option>
	                            <option value="{{App\Enums\ETemplateFormType::OTHER}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::OTHER)}}</option>
	                        </select>
						</div>
						<div class="col-md-6">
							@if($checkCreatePermission)
								<div class="form-group float-right">
									<a href="#" data-toggle="modal" data-target="#create-or-edit" class="btn btn-viewmore-news mr0" wire:click="initData">
										<img src="/images/plus2.svg" alt="plus">{{__('common.button.create')}}
									</a>
								</div>
							@endif
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-general">
							<thead>
								<tr class="border-radius">
									<th scope="col" class="text-center border-radius-left" style="width:10%">
										{{__('data_field_name.template_form.index')}}
									</th>
									<th scope="col" class="text-center">
										{{__('data_field_name.template_form.name')}}
									</th>
									<th scope="col" class="text-center">
										{{__('data_field_name.template_form.type')}}
									</th>
									<th scope="col" class="text-center">
										{{__('data_field_name.template_form.created_at')}}
									</th>
									<th scope="col" class="border-radius-right w-140">
										{{__('research/ideal.table_column.action')}}
									</th>
								</tr>
							</thead>
							<div wire:loading class="loader"></div>
							<tbody>
								@foreach($data as $index => $row)
									<tr>
										<td class="text-center">{{$index + 1}}</td>
										<td class="text-center"><a href="{{asset('storage/' . $row['file']->url)}}" download="">{{$row['name']}}</td>
										<td class="text-center">{{App\Enums\ETemplateFormType::valueToName($row['type'])}}</td>
										<td class="text-center">{{Illuminate\Support\Carbon::parse($row['created_at'])->format(Config::get('common.formatDate'))}}</td>
										<td>
											@if($checkEditPermission)
												<a href="#" data-toggle="modal" data-target="#create-or-edit" class="btn" wire:click="initData({{$row}})">
													<img src="/images/pent2.svg" alt="">
												</a>
											@endif
											@if($checkDestroyPermission)
												<button type="button" class="btn par6" title="{{__('common.button.delete')}}" wire:click="$set('deleteId', {{$row['id']}})" data-target="#exampleDelete" data-toggle="modal">
													<img src="/images/trash.svg" alt="trash">
												</button>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@if(count($data) > 0)
						{{$data->links()}}
					@else
						<div class="title-approve text-center">
							<span>{{__('common.message.empty_search')}}</span>
						</div>
					@endif
					@include('livewire.common._modalDelete')
					@include('livewire.admin.executive.form.modal-create-edit')
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("document").ready(() => {
		window.livewire.on('closeModalCreate', () => {
        	$('#create-or-edit').modal('hide');
        });
    });
    function uploadFile(input, proxy = null) {
        let file = input.files[0];
        if (!file || !proxy) return;
        let $error = $(input).parents('.modal-fix').find('.upload-error');
        if(file.size/(1024*1024) >= 255) {
            $error.html("{{ __('notification.upload.maximum_size', ['value' => 255]) }}");
            return;
        }

        let mimes = {!! json_encode(['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z']) !!};
        let extension = file.name.split('.').pop();
        if(!mimes.includes(extension)) {
            $error.html("{{ __('notification.upload.mime_type', ['values' => implode(', ', ['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z'])]) }}");
            return;
        }

        proxy.upload('fileUpload', file, (uploadedFilename) => {

        }, () => {
            console.log(error);
        }, (event) => {

        });
    }
</script>