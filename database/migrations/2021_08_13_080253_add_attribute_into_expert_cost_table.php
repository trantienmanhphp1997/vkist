<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeIntoExpertCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expert_cost', function (Blueprint $table) {
            $table->string('expert_name', 255)->nullable()->comment('Ten chuyen gia');
            $table->string('nationality', 255)->nullable()->comment('Quoc tich');
            $table->string('organization', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expert_cost', function (Blueprint $table) {
            $table->dropColumn('expert_name');
            $table->dropColumn('nationality');
            $table->dropColumn('organization');
        });
    }
}
