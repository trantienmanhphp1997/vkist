<?php

namespace Tests\Unit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Livewire;
use App\Models\UserInfoLeave;
use App\Http\Livewire\Admin\Executive\UserInfoLeave\UserInfoLeaveCreateAndUpdate;

class UserInfoLeaveControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    /** @test */
    function can_create_user_info_leave()
    {
        $user =  $this->user;
        $this->actingAs($user);
        Livewire::test(UserInfoLeaveCreateAndUpdate::class)
            ->set([
                'reason' => 'Nghỉ dịch covid',
                'leaveDate' => date('Y-m-d'),
                'status' => '',
                'name' => 'Nguyễn Văn A',
                ])
            ->call('store');

        $this->assertTrue(UserInfoLeave::whereReason('Nghỉ dịch covid')->exists());
    }

    /** @test */
    public function can_index_user_info_leave()
    {
        $user = $this->user;
        $this->actingAs($user)->get(route('admin.executive.user-info-leave.index'))->assertOk();
    }
}
