<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetOriginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_origin', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('nguồn gốc tài sản');
            $table->string('origin_name', 500)->comment('Ten nguồn gốc tài sản');
            $table->tinyInteger('origin_status')->comment('1 show, 0 hidden');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_origin');
    }
}
