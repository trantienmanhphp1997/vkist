<?php

namespace App\Http\Livewire\Admin\Research\Category;

use App\Models\Expert;
use App\Models\MasterData;
use App\Enums\EMasterDataType;
use App\Enums\EUnitExpert;
use App\Models\Department;
use App\Http\Livewire\Base\BaseLive;
use App\Models\File;
use Illuminate\Support\Facades\Config;
use DB;

class ExpertList extends BaseLive {
    public $searchTerm = '';
    public $deleteId = '';
    public $name_expert = '';
    public $name_expert_after_remove_utf8 = '';
    public $academic_level = '';
    public $specialized = '';
    public $work_unit = '';
    public $telephone_number = '';
    public $address = '';
    public $email = '';
    public $updateMode = false;
    public $editID;
    public $expert_unit;
    public $unit ;


    public function mount() {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
        $this->expert_unit = [
            EUnitExpert::ENTERPRISE =>  __('data_field_name.unit_expert.enterprise'),
            EUnitExpert::GOVERNMENT => __('data_field_name.unit_expert.government'),
            EUnitExpert::MANAGE => __('data_field_name.unit_expert.manage'),
            
        ];
    }
    public function render() {
        $query = Expert::leftjoin('DEPARTMENT','DEPARTMENT.ID', 'expert.department_id');
        $query->select('expert.*',DB::raw('DEPARTMENT.NAME as department_name'));
        if (strlen($this->searchTerm)) {
                $query->where('expert.unsign_text', 'like', '%'.strtolower(removeStringUtf8($this->searchTerm)) .'%');
        }
        $masterData = MasterData::where('type',EMasterDataType::EDUCATION_BACKGROUND)->get();
        $masterDataCreate = MasterData::getDataByType(EMasterDataType::EDUCATION_BACKGROUND);
        $masterDataSpecial = MasterData::where('type',EMasterDataType::SPECIALIZED)->get();
        $masterDataSpecialCreate = MasterData::getDataByType(EMasterDataType::SPECIALIZED);
        $data = $query->orderBy('expert.id','DESC')->paginate($this->perPage);
        $model_name = Expert::class;
        $type = Config::get('common.type_upload.UnitExpert');
        $folder = app($model_name)->getTable();

        return view('livewire.admin.research.category.expert-list',[
            'data' => $data,
            'masterData'=>$masterData,
            'masterDataCreate'=>$masterDataCreate,
            'masterDataSpecial'=>$masterDataSpecial,
            'masterDataSpecialCreate'=>$masterDataSpecialCreate,
            'model_name'=>$model_name,
            'type'=>$type,
            'folder'=>$folder
        ]);
    }

    //delete
    public function deleteId($id) {
        $this->deleteId=$id;
    }
    public function delete() {
        Expert::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }

    // lưu trữ
    public function create(){
        $this->updateMode = false;
    }
    public function save(){
        if($this->academic_level=='') {
            $this->academic_level=0;
        }
        if($this->specialized=='') {
            $this->specialized=0;
        }
        $this->standardizedData();
        $this->validateData();
        if($this->updateMode){
            Expert::findOrFail($this->editID)->update([
                'fullname' => $this->name_expert,
                'academic_level_id' => $this->academic_level,
                'specialized_id' => $this->specialized,
                'department' => $this->work_unit,
                'phone' => $this->telephone_number,
                'address' => $this->address,
                'email' => $this->email,
                'unit'=>$this->unit,
                // 'unsign_text' => removeStringUtf8($this->name_expert),
                'nationality' => "Việt Nam",
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')]);
        }
        else {
            $newExpert = Expert::create([
                'fullname' => $this->name_expert,
                'academic_level_id' => $this->academic_level,
                'specialized_id' => $this->specialized,
                'department' => $this->work_unit,
                'phone' => $this->telephone_number,
                'address' => $this->address,
                'unit'=>intval($this->unit),
                'email' => $this->email,
                // 'unsign_text' => removeStringUtf8($this->name_expert),
                'nationality' => "Việt Nam",
            ]);
            $this->editID = $newExpert->id;

            File::query()
            ->whereNull('model_id')
            ->where('admin_id', auth()->id())
            ->where('type', Config::get('common.type_upload.UnitExpert'))
            ->update([
                'model_id' => $this->editID
            ]);

            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);
        }
        $this->emit('close_modal_create');
        $this->resetInput();
    }
    // chỉnh sửa
    public function editId($id){
        $this->updateMode = true;
        $expert = Expert::findorFail($id);
        $this->editID = $id;
        $this->name_expert = $expert->fullname;
        $this->academic_level = $expert->academic_level_id;
        $this->specialized = $expert->specialized_id;
        $this->unit =$expert->unit;
        $this->work_unit = $expert->department;
        $this->telephone_number = $expert->phone;
        $this->address = $expert->address;
        $this->email = $expert->email;
    }

    public function validateData(){
        if(!$this->academic_level){
            $this->academic_level = null;
        }
        if(!$this->specialized){
            $this->specialized = null;
        }
        $this->name_expert_after_remove_utf8 = removeStringUtf8($this->name_expert);
        $this->validate([
            'name_expert_after_remove_utf8' => ['required', 'max:100','regex:/^[a-zA-Z ]+$/'],
            'academic_level' => ['required'],
            'specialized' => ['required'],
            'work_unit' => ['required', 'max:100'],
            'unit'=>'required',
            'telephone_number' => ['required', 'max:12','min:9','digits_between:9,12'],
            'address' => ['max:255'],
            'email' => ['required', 'max:100', 'email', 'unique:expert,email'.($this->updateMode?','.$this->editID:'') ],
        ],[
            'telephone_number.digits_between' => __('research/expert.validate.telephoneNumberDigits'),
        ],[
            'name_expert_after_remove_utf8'=> __('research/expert.validate.name_expert'),
            'academic_level' => __('research/expert.validate.academic_level'),
            'specialized' => __('research/expert.validate.specialized'),
            'work_unit' => __('research/expert.validate.work_unit'),
            'telephone_number' => __('research/expert.validate.telephone_number'),
            'address' => __('research/expert.validate.address'),
            'email' => __('research/expert.validate.email'),
            'unit'=>__('data_field_name.unit_expert.unit'),
        ]);
    }
    public function resetInput(){
        $this->name_expert = null;
        $this->name_expert_after_remove_utf8 = null;
        $this->academic_level = null;
        $this->specialized = null;
        $this->work_unit = null;
        $this->telephone_number = null;
        $this->unit = null ;
        $this->address = null;
        $this->email = null;
        $this->editID = null;
    }
    public function standardizedData(){
        $this->name_expert = trim($this->name_expert);
        $this->academic_level = trim($this->academic_level);
        $this->specialized = trim($this->specialized);
        $this->work_unit = trim($this->work_unit);
        $this->unit = trim($this->unit);
        $this->telephone_number = trim($this->telephone_number);
        $this->address = trim($this->address);
        $this->email = trim($this->email);
    }
    public function clickBtnCancel(){
        $this->resetInput();
        $this->resetValidation();
    }


}
