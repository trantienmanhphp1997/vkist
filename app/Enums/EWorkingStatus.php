<?php

namespace App\Enums;

class EWorkingStatus
{

    const K = 1;
    const P = 2;
    const X = 3;
    const L = 4;
    const H = 5;
    const R = 6;
    const Ro = 7;
    const Co = 8;
    const D = 9;
    const O = 10;
    const T = 11;

    public static function nameToValue($name)
    {
        switch ($name) {
            case 'K':
                $result = 1;
                break;
            case 'P':
                $result = 2;
                break;
            case 'X':
                $result = 3;
                break;
            case 'L':
                $result = 4;
                break;
            case 'H':
                $result = 5;
                break;
            case 'R':
                $result = 6;
                break;
            case 'Ro':
                $result = 7;
                break;
            case 'Co':
                $result = 8;
                break;
            case 'D':
                $result = 9;
                break;
            case 'O':
                $result = 10;
                break;
            case 'T':
                $result = 11;
                break;
            default:
                $result = -1;
                break;
        }
        return $result;
    }

    public static function valueToName($value)
    {
        switch ($value) {
            case '1':
                $result = 'K';
                break;
            case '2':
                $result = 'P';
                break;
            case '3':
                $result = 'X';
                break;
            case '4':
                $result = 'L';
                break;
            case '5':
                $result = 'H';
                break;
            case '6':
                $result = 'R';
                break;
            case '7':
                $result = 'Ro';
                break;
            case '8':
                $result = 'Co';
                break;
            case '9':
                $result = 'D';
                break;
            case '10':
                $result = 'O';
                break;
            case '11':
                $result = 'T';
                break;
            default:
                $result = '-';
                break;
        }
        return $result;
    }

    public static function nameToLocale($name)
    {
        switch ($name) {
            case 'K':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.working-day');
                break;
            case 'P':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.annual-leave');
                break;
            case 'X':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.paid-leave');
                break;
            case 'L':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.holiday');
                break;
            case 'H':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.meetings');
                break;
            case 'R':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.paid-personal-leave');
                break;
            case 'Ro':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.unpaid-personal-leave');
                break;
            case 'Co':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.sick-leave');
                break;
            case 'D':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.maternity-leave');
                break;
            case 'O':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.leave-without-reason');
                break;
            case 'T':
                $result = __('executive/my-time-sheets.symbol-of-timesheets.labor-accident');
                break;
            default:
                $result = $name;
                break;
        }
        return $result;
    }
}
