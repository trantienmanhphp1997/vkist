<div class="inner-tab">

        @livewire('admin.user.tab-direct-research', ['user_info_id'=>$user_info_id])

        @livewire('admin.user.tab-table1', ['user_info_id'=>$user_info_id])

        @livewire('admin.user.tab-table2', ['user_info_id'=>$user_info_id])

        @livewire('admin.user.tab-table3', ['user_info_id'=>$user_info_id])

</div>
<script>
    $("document").ready(function() {
        window.livewire.on('userStore', () => {
            document.getElementById('close-modal-create-researchTable1').click();
            document.getElementById('close-modal-edit-researchTable1').click();
            document.getElementById('close-modal-create-researchTable2').click();
            document.getElementById('close-modal-edit-researchTable2').click();
            document.getElementById('close-modal-create-researchTable3').click();
            document.getElementById('close-modal-edit-researchTable3').click();
            document.getElementById('close-modal-edit-direct-research').click();
        });
    });
</script>
