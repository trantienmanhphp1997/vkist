<div class="row inner-tab">
    @csrf
    {{ method_field('POST') }}
    @if (isset($rolePermissions))
    {!! Form::hidden('id') !!}
    @endif
    <div class="col-md-6">
        <div class="form-group">
            <label>{{__('data_field_name.system.role.name')}} <span class="text-danger">*</span></label>
            {!! Form::text('name', null, array('class' => 'form-control')) !!}
            @error('name') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{__('data_field_name.system.role.code')}} <span class="text-danger">*</span></label>
            {!! Form::text('code', null, array('class' => 'form-control')) !!}
            @error('code') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>{{__('data_field_name.common_field.descriptions')}}</label>
            {!! Form::textarea('note', null, array('class' => 'form-control', 'rows' => '4')) !!}
            @error('note') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <label>{{__('data_field_name.common_field.status')}}</label>
            </div>
        </div>
        <div class=" participants-check bg-user bg-user2">
            <label class="box-checkbox">{{__('data_field_name.system.role.active')}}
                @if (isset($data))
                    {{ Form::checkbox('status', 1, null) }}
                @else
                    {{ Form::checkbox('status', 1, true) }}
                @endif
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="table-responsive">
            <table class="table  table-general working-plan">
                <thead>
                    <tr class="border-radius">
                        <th rowspan="2" scope="col" class="border-radius-left">{{__('data_field_name.system.role.function')}}</th>
                        <th scope="col" class="text-center" rowspan="2">{{__('data_field_name.system.role.function_code')}}</th>
                        <th scope="col" class="text-center"><img src="/images/eye.svg" alt="eye"/></th>
                        <th scope="col" class="text-center"><img src="/images/pent2.svg" alt="pent"/> </th>
                        <th scope="col" class="text-center"><img src="/images/trash.svg" alt="trash"></th>
                        <th scope="col" class="text-center"><img src="/images/add.svg" alt="add"></th>
                        <th scope="col" class="text-center"><img src="/images/Download.svg" alt="download"/></th>
                        <th scope="col" class="text-center"><img src="/images/eye.svg" alt="eye"/></th>
                        <th scope="col" class="text-center"><img src="/images/eye.svg" alt="eye"/></th>
                        <th scope="col" class="border-radius-right text-center"></th>
                    </tr>
                    <tr class="border-radius">
                        <th scope="col" class="text-center">{{__('data_field_name.common_field.view')}}</th>
                        <th scope="col" class="text-center">{{__('data_field_name.common_field.edit')}}</th>
                        <th scope="col" class="text-center">{{__('data_field_name.common_field.delete')}}</th>
                        <th scope="col" class="text-center">{{__('data_field_name.common_field.add')}}</th>
                        <th scope="col" class="text-center">{{__('data_field_name.common_field.download')}}</th>
                        <th scope="col" class="text-center">{{__('data_field_name.common_field.wait_approve')}}</th>
                        <th scope="col" class="text-center">{{__('data_field_name.common_field.approve')}}</th>
                        <th scope="col" class="border-radius-right text-center"></th>
                    </tr>
                </thead>
                <tbody>
                @php $stt = 1; @endphp
                @foreach($menus as $menu)
                    <tr>
                        <td>{!! $listNameMenu[$menu->name] ?? $menu->name !!}</td>
                        <td class="text-center">{!! $menu->code !!}</td>
                        @foreach($permissions as $permission)
                            @if (strpos($permission->name, str_replace('.index', '', $menu->permission_name)) !== false)
                                @foreach($arrAction as $action)
                                    @if ($permission->name == str_replace('.index', '', $menu->permission_name). '.' . $action)
                                        <td class="text-center">
                                            <label class="box-checkbox checkbox-analysis">
                                                @if (isset($rolePermissions))
                                                    @if (!empty($rolePermissions) && in_array($permission->id, $rolePermissions))
                                                        {{ Form::checkbox("role[$permission->id]", 1, true) }}
                                                    @else
                                                        {{ Form::checkbox("role[$permission->id]", 1, null) }}
                                                    @endif
                                                @else
                                                    {{ Form::checkbox("role[$permission->id]", 1, null) }}
                                                @endif
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </tr>
                    @php $stt++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 ">
        <div class="group-btn2 text-center group-btn ">
            <a href="{{route('admin.system.role.index')}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
            <button type="submit" class="btn btn-save">{{__('common.button.save')}}</button>
        </div>
    </div>
</div>

