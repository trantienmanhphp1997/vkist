<?php
return [
    'name'=>'Research project management',
    'status'=> [
        'wait_approval'=>'Not in process',
        'in_process'=>'In process',
        'completed'=>'Completed',
        'cancel'=>'Cancel',
        'draft' => 'Draft',
        'unfinished' => 'Unfinished',
        'acceptance' => 'On acceptance'
    ],
    'information'=>[
        'breadcrumbs'=>'Project information',
        'name'=>'Project information management',
        'table-title'=>'Project list',
        'search'=>'Find project',
        'create'=>'Create Project',
        'table-column'=>[
            'code-project'=>'PROJECT CODE',
            'name'=>'NAME OF PROJECT',
            'research-category'=>'RESEARCH CATEGORY TYPE',
            'leader'=>'LEADER SUBJECT',
            'status'=>'STATUS'
        ],
        'tab-project'=>[
            'tab-name'=>'Project',
            'title-create'=>'Create a new project research',
            'title-update'=>'Update project research',
            'name'=>'Name of topic',
            'project-code'=>'Topic code',
            'contract-code'=>'Contract Code',
            'research-type'=>'Topic research type',
            'status'=>'Project Status',
            'man-month'=>'Man month',
            'start'=>'Start date',
            'end'=>'End date',
            'description'=>'Describe project',
            'note'=>'Note',
            'department'=>'Department',
        ],
        'tab-project-member'=>[
            'tab-name'=>'Project member'
        ],
        'create-project-information-success'=>'Create project research success',
        'create-project-information-fail'=>'Create project research fail',
        'update-project-information-success'=>'Update project research success',
        'update-project-information-fail'=>'Update project research fail',
        'delete-msg-success'=>'Delete project information success',
    ],
    'error' => [
        'warning-saving-draft' => 'Are you surely want to save this project to draft ?',
        'warning-transfer-to-operation' => 'Are you surely want to open this project back to live ?',
        'no-contract-code-valid'=>'There are non contract code exist',
        'invalid-acceptance-status' => 'There are some incompleted task',
    ],
    'success'=>[
        'success-saving-draft'=>'Saving project to draft success',
        'success-transfer-operation'=>'Transfer project back to live success'
    ],
    'work'=>[
        'breadcrumbs'=>'Thông tin dự án',
        'name'=>'Project work management',
    ],
    'performance'=>[
        'breadcrumbs'=>'Thông tin dự án',
        'name'=>'Work performance management',
    ]
];
