<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class TopicFeeDetail extends BaseModel
{
    use HasFactory;

    protected $table = 'topic_fee_detail';
    protected $fillable = [
    	'name', 'total_capital', 'ratio', 'state_capital', 'other_capital', 'note', 'topic_fee_id', 'admin_id', 'type',
        'type_cost', 'payment_cost', 'source_cost', 'approval_cost','topic_fee_detail_expense','total_prescribed_expense'
    ];
    protected $casts = [
        'topic_fee_detail_expense'=>'array',
    ];

    public function topicFee() {
        return $this->belongsTo(TopicFee::class, 'topic_fee_id');
    }
    public function topic_detail_work(){
        return $this->hasMany(TopicDetailWork::class,'topic_fee_detail_id','id');
    }
    public function expert_cost(){
        return $this->hasMany(ExpertCost::class,'topic_fee_detail_id','id');
    }
    public function material_cost(){
        return $this->hasMany(MaterialCost::class,'topic_fee_detail_id','id');
    }
    public function equipment_cost(){
        return $this->hasMany(EquipmentCost::class,'topic_fee_detail_id','id');
    }
    public function asset_cost(){
        return $this->hasMany(AssetCost::class,'topic_fee_detail_id','id');
    }
    public function topic_other_fee(){
        return $this->hasMany(OtherCost::class,'topic_fee_detail_id','id');
    }
    public function topic_normal_cost(){
        return $this->hasMany(TopicNormalCost::class,'topic_fee_detail_id','id');
    }
}
