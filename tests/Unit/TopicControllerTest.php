<?php

namespace Tests\Unit;

use App\Enums\ETopicSource;
use App\Enums\ETopicType;
use App\Http\Livewire\Admin\Research\Topic\TabExpectedResult;
use App\Http\Livewire\Admin\Research\Topic\TabGeneral;
use App\Http\Livewire\Admin\Research\Topic\TabMember;
use App\Http\Livewire\Admin\Research\Topic\TabPresentation;
use App\Models\Topic;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Livewire\Livewire;
use Tests\TestCase;

class TopicControllerTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function testTopicIndex()
    {
        $this->actingAsSuperAdmin()->get(route('admin.research.topic.index'))->assertOk();
    }

    /** @test */
    public function testTopicCreateSuccess()
    {
        $this->actingAsSuperAdmin()->get(route('admin.research.topic.create'))->assertOk();

        $input = $this->prepareData();

        Livewire::test(TabGeneral::class)
            ->fill($input['tab_general'])
            ->call('save')
            ->assertHasNoErrors();

        $topic = Topic::query()->orderBy('created_at', 'desc')->first();

        Livewire::test(TabPresentation::class)
            ->fill($input['tab_presentation'])
            ->call('save', $topic->id)
            ->assertHasNoErrors();

        try {
            Livewire::test(TabMember::class)
                ->fill($input['tab_member'])
                ->call('save', $topic->id)
                ->assertHasNoErrors();
        } catch (Exception $exception) {
        }

        Livewire::test(TabExpectedResult::class)
            ->fill($input['tab_expected_result'])
            ->call('save', $topic->id)
            ->assertHasNoErrors();
    }

    /** @test */
    public function testTopicCreateFail()
    {
        $this->actingAsSuperAdmin()->get(route('admin.research.topic.create'))->assertOk();

        $input = $this->prepareData();
        $input['tab_general']['topic.name'] = '';
        $input['tab_general']['topic.level_id'] = '';

        Livewire::test(TabGeneral::class)
            ->fill($input['tab_general'])
            ->call('save')
            ->assertHasErrors();

        $topic = Topic::query()->orderBy('created_at', 'desc')->first();

        Livewire::test(TabPresentation::class)
            ->fill($input['tab_presentation'])
            ->call('save', $topic->id)
            ->assertHasNoErrors();

        try {
            Livewire::test(TabMember::class)
                ->fill($input['tab_member'])
                ->call('save', $topic->id)
                ->assertHasNoErrors();
        } catch (Exception $exception) {
        }

        $input['tab_expected_result']['topic.achievement_note'] = '';

        Livewire::test(TabExpectedResult::class)
            ->fill($input['tab_expected_result'])
            ->call('save', $topic->id)
            ->assertHasErrors();
    }

    public function prepareData()
    {
        return [
            'tab_general' => [
                'topic.source' => ETopicSource::FROM_IDEA,
                'topic.type' => ETopicType::NEW_TOPIC,
                'topic.research_category_id' => '1',
                'topic.major_id' => '1',
                'topic.necessary' => 'This is necessary',
                'topic.overview' => 'This is overview',
                'topic.target' => 'This is target',
                'topic.level_id' => 350,
                'topic.space_scope' => 'This is space scope',
                'topic.start_date' => Carbon::today(),
                'topic.end_date' => Carbon::tomorrow(),
                'topic.expected_fee' => '1000000',
                'topic.parent_id' => null,
                'topic.ideal_id' => 1,
                'topic.name' => 'topic name',
            ],
            'tab_presentation' => [
                'topic.present_note' => 'This is present note'
            ],
            'tab_member' => [
                'readyMembers' => [
                    [
                        'academic_level_id' => 340,
                        'mission_note' => 'This is mission note',
                        'user_info_id' => 1,
                        'research_id' => 1,
                        'role_id' => 351
                    ]
                ]
            ],
            'tab_expected_result' => [
                'topic.achievement_note' => 'This is archivement note',
                'topic.achievement_paper_check' => 0,
                'topic.achievement_review_check' => 0,
                'topic.achievement_announcement_check' => 1,
                'topic.achievement_conference_check' => 1,
                'topic.achievement_work_check' => 1,
                'topic.achievement_textbook_check' => 1,
            ]
        ];
    }
}
