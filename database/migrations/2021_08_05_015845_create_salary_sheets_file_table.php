<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalarySheetsFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_sheets_file', function (Blueprint $table) {
            $table->id()->comment('luu du lieu luong theo thang');
            $table->tinyInteger('month_working')->comment('Thang');
            $table->integer('year_working')->comment('nam');
            $table->string('name', 500)->comment('Ten bang luong');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_sheets_file');
    }
}
