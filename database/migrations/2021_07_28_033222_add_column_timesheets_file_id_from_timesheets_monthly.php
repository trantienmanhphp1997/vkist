<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTimesheetsFileIdFromTimesheetsMonthly extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timesheets_monthly', function (Blueprint $table) {
            $table->unsignedBigInteger('timesheets_file_id')->nullable()->comment('Map voi timesheets_file');
            $table->foreign('timesheets_file_id')->references('id')->on('timesheets_file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timesheets_monthly', function (Blueprint $table) {
            $table->dropColumn('timesheets_file_id');
        });
    }
}
