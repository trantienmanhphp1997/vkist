<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApprovalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval', function (Blueprint $table) {
            $table->id()->comment('Xet duyet');
            $table->string('name', 500)->nullable()->comment('Ten ho so');
            $table->string('code', 255)->nullable()->comment('Ma ho so');
            $table->string('contract_code', 255)->nullable()->comment('Ma hop dong');
            $table->tinyInteger('type')->nullable()->comment('Loai ho so');
            $table->tinyInteger('status')->nullable()->comment('Trang thai ho so');
            $table->date('complete_date')->nullable()->comment('Han hoan thanh');
            $table->foreignId('topic_id')->nullable()->constrained('topic');
            $table->foreignId('ideal_id')->nullable()->constrained('ideal');
            $table->foreignId('topic_fee_id')->nullable()->constrained('topic_fee');
            $table->foreignId('appraisal_board_id')->nullable()->constrained('appraisal_board');
            $table->bigInteger('user_info_id')->constrained('user_info')->comment('Nguoi gui');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval');
    }
}
