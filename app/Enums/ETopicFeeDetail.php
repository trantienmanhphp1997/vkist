<?php


namespace App\Enums;


class ETopicFeeDetail
{
    const TYPE_PEOPLE = 1;
    const TYPE_EXPERT = 2;
    const TYPE_MATERIAL = 3;
    const TYPE_EQUIPMENT = 4;
    const TYPE_SHOP_REPAIR = 5;
    const TYPE_OTHER = 6;
    const TYPE_NORMAL = 7;
    const TYPE_COST_INDIRECT = 1;
    const TYPE_COST_DIRECT = 2;
    const PAYMENT_COST_NO_CONTRACT = 0;
    const PAYMENT_COST_CONTRACT = 1;
    const SOURCE_COST_ODA = 1;
    const SOURCE_COST_GOV = 2;
    const SOURCE_COST_OTHER = 3;

    public static function getListType()
    {
        return [
            self::TYPE_PEOPLE => __('data_field_name.research_cost.type_people'),
            self::TYPE_EXPERT => __('data_field_name.research_cost.type_expert'),
            self::TYPE_MATERIAL => __('data_field_name.research_cost.type_material'),
            self::TYPE_EQUIPMENT => __('data_field_name.research_cost.type_equipment'),
            self::TYPE_SHOP_REPAIR => __('data_field_name.research_cost.type_shop_repair'),
            self::TYPE_OTHER => __('data_field_name.research_cost.type_other'),
        ];
    }
    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::TYPE_PEOPLE:
                $result = __('data_field_name.research_cost.type_people');
                break;
            case self::TYPE_EXPERT:
                $result = __('data_field_name.research_cost.type_expert');
                break;
            case self::TYPE_MATERIAL:
                $result = __('data_field_name.research_cost.type_material');
                break;
            case self::TYPE_EQUIPMENT:
                $result = __('data_field_name.research_cost.type_equipment');
                break;
            case self::TYPE_SHOP_REPAIR:
                $result = __('data_field_name.research_cost.type_shop_repair');
                break;
            case self::TYPE_OTHER:
                $result = __('data_field_name.research_cost.type_other');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
