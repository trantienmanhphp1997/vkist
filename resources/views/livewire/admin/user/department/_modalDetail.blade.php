 <!-- Modal Detail -->
      <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="detailDepartment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('data_field_name.department.detail_department')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true close-btn">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">{{__('data_field_name.department.name')}}</label>
                    <input type="text" readonly class="form-control" id="exampleFormControlInput1" placeholder="Nhập tên...." wire:model.defer="name">
                    @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">{{__('data_field_name.department.code')}}<span class="text-danger">(*)</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1"  wire:model.defer="code">
                    @error('code') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlInput4">{{__('data_field_name.department.under_department')}}</label>
                    <select  name="parent_id" disabled id="exampleFormControlInput4" class="form-control" wire:model.defer="parent_id">
                      <option  value="">--{{__('data_field_name.department.master_room')}}--</option>
                      @foreach ($hierarchyDeparment as $value)
                      <option value="{{$value['id']}}">
                        {!!$value['padLeft'].$value['name']!!}
                      </option>
                      @endforeach
                    </select>
                    @error('parent_id') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlInput2">{{__('data_field_name.department.note')}}</label><br>
                    <textarea type="text" name="note" id="" cols="30" rows="10" wire:model.defer="note" placeholder="Nhập mô tả chức năng" style="width:100%;display: flex;flex-direction: row;
                        align-items: flex-start;position: static;border-radius: 5px;" readonly></textarea>
                    @error('note') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlInput3">{{__('data_field_name.department.status')}}</label>
                    <select name="status" readonly id="exampleFormControlInput3" class="form-control" wire:model.defer="status">
                      <option value="0">{{__('data_field_name.system.role.active')}}</option>
                      <option value="1">{{__('data_field_name.system.role.inactive')}}</option>
                    </select>
                    @error('status') <span class="text-danger error">{{ $message }}</span>@enderror
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" id="close-modal-create-department" class="btn btn-secondary close-btn" data-dismiss="modal">{{__('common.button.close')}}</button>
              </div>
            </div>
          </div>
        </div>
      </form>
      <!-- End Modal Detail -->
