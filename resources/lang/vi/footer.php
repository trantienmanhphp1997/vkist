<?php
return [
    'licence' => 'Giấy phép thiết lập trang thông tin điện tử tổng hợp trên Internet
    số 3221/GP-TTDT do Sở thông tin và Truyền thông TP Hà nội cấp ngày 03 tháng 07 năm 2019',
    'info'=>'THÔNG TIN',
    'address'=>'Địa chỉ: Khu Công nghệ cao Hòa Lạc, km29, Đại lộ Thăng Long, Hà Nội',
    'phone'=>'Điện thoại: (024) 3556 0695',
    'new'=>'TIN TỨC - SỰ KIỆN',
    'new-1'=>'Tin hoạt động VKIST',
    'new-2'=>'Tin tuyển dụng',
    'new-3'=>'Email',
];
