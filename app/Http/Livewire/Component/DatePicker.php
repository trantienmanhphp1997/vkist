<?php

namespace App\Http\Livewire\Component;

use Livewire\Component;

class DatePicker extends Component
{

    public $inputId;
    public $name;
    public $setNullWhenEnterInvalidDate;
    public $disabled;
    public $placeholder;
    public $minDate;
    public $maxDate;
    public $value;

    public function render()
    {
        return view('livewire.component.date-picker');
    }
}
