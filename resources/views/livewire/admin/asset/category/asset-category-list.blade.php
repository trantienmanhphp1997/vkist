<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
    <div class="col-md-12">
        @if(checkPermission('admin.asset.category.index'))
            <div  class="breadcrumbs"><a href="{{route('admin.asset.category.index')}}">{{ __('menu_management.menu_name.asset-management') }}</a>
        @endif
             \ <span>{{__('data_field_name.asset_category.asset_category_list')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.asset_category.asset_category_list')}}</h3>
                </div>
                <div class="col-md-6">
                    <div class=" float-right">
                        @if($checkCreatePermission)
                            <button id="modal_create" wire:click.prevent="resetInputFields()" class="btn btn-viewmore-news mr0 " data-toggle="modal" data-target="#createModal"><img src="/images/plus2.svg" alt="plus">{{__('data_field_name.asset_category.add')}}</button>
                            @include('livewire.admin.asset.category._modalCreate')
                        @endif
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group search-expertise search-plan">
                            <div class="search-expertise inline-block">
                                    <input type="text" class="form-control size13"
                                           placeholder="{{__('data_field_name.asset_category.search')}}"
                                           wire:model.debounce.1000ms="searchTerm" autocomplete="off">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group float-right">
                                @if(checkRoutePermission('download'))
                                    <button title="{{__('common.button.export_file')}}" class="border-0 p-0 bg-white" data-toggle="modal" data-target="#export-modal"><img src="/images/filterdown.svg" alt="filterdown"></button>
                                @endif
                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="coUser" style="height:100%;">
                            <thead>
                                <tr class="border-radius">
                                <th scope="col colUser" class="border-radius-left">{{__('data_field_name.asset_category.code')}}</th>
                                <th scope="col colUser">{{__('data_field_name.asset_category.name')}}</th>
                                <th scope="col colUser">{{__('data_field_name.asset_category.type_manage')}}</th>
                                <th scope="col" class="text-center">{{__('data_field_name.asset_category.quantity')}}</th>
                                <th scope="col"class="text-center">{{__('data_field_name.asset_category.no_use')}}</th>
                                <th scope="col"class="text-center">{{__('data_field_name.asset_category.using')}}</th>
                                <th scope="col colUser" class="border-radius-right">{{__('data_field_name.asset_category.action')}}</th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                            @forelse ($data as $row)
                                <tr>
                                <td>{!! boldTextSearch($row->code,$searchTerm) !!}</td>
                                <td>{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                <td>{{ (($row->type_manage == 0) ? __('data_field_name.asset_category.manage_by_code') : __('data_field_name.asset_category.manage_by_quantity')) }}</td>
                                <td class="text-center">{{ $row->asset->sum('quantity') }}</td>
                                <td class="text-center">{{ $row->asset->sum('quantity') - $row->asset->sum('amount_used') }}</td>
                                <td class="text-center">{{ $row->asset->sum('amount_used') }}</td>
                                <td>
                                        @if($checkEditPermission)
                                            <button type="button" data-toggle="modal" id="show_modal_edit" data-target="#editModal" title="Sửa" wire:click="edit({{ $row->id }})" class="btn-sm border-0 bg-transparent mr-1 show_modal_edit"><img src="/images/edit.png" alt="edit"></button>
                                        @endif
                                        @if($checkDestroyPermission)
                                            <button type="button" data-toggle="modal" data-target="#deleteModal" title="{{__('common.button.delete')}}" wire:click="deleteId({{$row->id}})" class="btn-sm border-0 bg-transparent"><img src="/images/trash.svg" alt="trash"></button>
                                        @endif
                                </td>
                            @empty
                                <tr>
                                    <td colspan="12">{{__('common.message.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                            </table>
                        </div>
                    {{$data->links()}}
                </div>
            </div>
                @include('livewire.admin.asset.category._modalEdit')
                @include('livewire.common.modal._modalDelete')
                @include('livewire.common.modal._modalConfirmExport')
        </div>
    </div>
</div>

<script>
    $("document").ready(function () {
        window.livewire.on('close-modal-create-asset-category', () => {
            document.getElementById('close-modal-create-asset-category').click()
        });
        window.livewire.on('close-modal-edit-asset-category', () => {
            document.getElementById('close-modal-edit-asset-category').click()
        });
    })
</script>
