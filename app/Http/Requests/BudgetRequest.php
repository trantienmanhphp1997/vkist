<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class BudgetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'year_created' => 'required|integer|between:2020,2050',
            'estimated_code' =>['required','regex:/^[A-Z0-9' . config('common.special_character.alphabet') .']+$/', 'max:10'],
            'code' => ['required','regex:/^[A-Z0-9' . config('common.special_character.alphabet') .']+$/', 'max:10'],
            'name' => ['required','regex:/^[a-zA-Z0-9' . config('common.special_character.alphabet') .']+$/', 'max:100'],
            'total_budget' =>['required','regex:/^[0-9\.\,]+$/','max:14'],
            'content' =>['regex:/^[a-zA-Z0-9' . config('common.special_character.alphabet') . '\.\,\-\(\)\/ ]+$/', 'max:255','nullable'],
            'note' => ['regex:/^[a-zA-Z0-9' . config('common.special_character.alphabet') . '\.\,\-\(\)\/ ]+$/', 'max:255','nullable'],
            'money_plan.*' => 'required|regex:/^[0-9\.\,]+$/|max:14',
        ];
    }

    public function attributes()
    {
        return [
            'year_created' => __('data_field_name.budget.year'),
            'estimated_code' => __('data_field_name.budget.estimate'),
            'code' => __('data_field_name.budget.code'),
            'name' => __('data_field_name.budget.name'),
            'total_budget' => __('data_field_name.budget.total'),
            'content' => __('data_field_name.budget.content'),
            'money_plan.*' => __('data_field_name.budget.money_plan'),
        ];
    }
}
