<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class TimeSheetsUpload extends BaseModel
{
    use HasFactory;
    protected $table = 'timesheets_upload';
    protected $fillable = [];
}
