<?php

namespace App\Http\Requests;

use App\Enums\EAppraisalBoard;
use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class AppraisalBoardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arrType = [EAppraisalBoard::TYPE_IDEAL, EAppraisalBoard::TYPE_TOPIC, EAppraisalBoard::TYPE_COST_ESTIMATE, EAppraisalBoard::TYPE_RESEARCH_PLAN];
        return [
            'name' => ['required', 'regex:/^[a-zA-Z0-9'.config('common.special_character.VN').'\.\,\-\(\)\/ ]+$/', 'max:100'],
            'type' => ['required', 'in:' . implode(',', $arrType)],
            'topic_id' => ['required', 'numeric', 'exists:topic,id'],
            'appraisal_date' => ['required', 'date'],
            'note' => ['nullable', 'regex:/^[a-zA-Z0-9'.config('common.special_character.VN').'\.\,\-\(\)\/ ]+$/', 'max:255'],
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('data_field_name.approval_topic.appraisal_name'),
            'type' => __('data_field_name.approval_topic.type_appraisal'),
            'topic_id' => __('data_field_name.approval_topic.topic'),
            'appraisal_date' => __('data_field_name.approval_topic.appraisal_date'),
            'note' => __('data_field_name.common_field.descriptions'),
        ];
    }
}
