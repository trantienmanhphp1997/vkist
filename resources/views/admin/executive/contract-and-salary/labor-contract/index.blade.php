@extends('layouts.master')
<title>{{__('executive/contract.list')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.executive.contract-and-salary.labor-contract.list-data')
@endsection
@section('js')
@endsection
