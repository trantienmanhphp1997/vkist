<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Topic;
use App\Models\TopicHasUserInfo;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResearchPlan extends BaseModel
{
    use HasFactory;
    protected $table = 'research_plan';
    protected $fillable = [
    	'name','status','mission_name','type','star_date','end_date','created_at','updated_at', 'gw_attach_file','admin_id'
    ];

    public function topic(){
        return $this->belongsTo(Topic::class,'topic_id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'research_plan_id');
    }

    public function mainTasks()
    {
        return $this->hasMany(Task::class, 'research_plan_id')->whereNull('parent_id');
    }
}
