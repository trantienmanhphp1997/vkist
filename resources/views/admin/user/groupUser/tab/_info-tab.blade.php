<div class="row inner-tab">
    <div class="col-md-3">
        <div class="form-group">
            <label>{{ __('user/group_user.form-data.code') }} <span class="text-danger"> *</span></label>
            <input type="text" class="form-control" wire:model.defer="groupUser.code" placeholder="{{ __('user/group_user.form-data.code') }}">
            @error('groupUser.code') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group">
            <label>{{ __('user/group_user.form-data.name') }} <span class="text-danger">*</span></label>
            <input type="text" class="form-control" wire:model.defer="groupUser.name" placeholder="{{ __('user/group_user.form-data.name') }}">
            @error('groupUser.name') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>{{ __('user/group_user.form-data.description') }} <span class="text-danger">*</span></label>
            <textarea class="form-control" rows="6" wire:model.defer="groupUser.description" placeholder="{{ __('user/group_user.form-data.description') }}"></textarea>
            @error('groupUser.description') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>

</div>
