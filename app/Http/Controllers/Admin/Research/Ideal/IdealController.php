<?php

namespace App\Http\Controllers\Admin\Research\Ideal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ideal;
use DB;

class IdealController extends Controller
{
	public function index(Request $request) {
		return view('admin.research.ideal.index');
	}

	public function create() {
		return view('admin.research.ideal.create');
	}

	public function edit($id) {
		$data = Ideal::with('researchField')->findOrFail($id);
		return view('admin.research.ideal.edit', compact('data'));
	}

	public function show($id) {
		$data = Ideal::with('researchField')->findOrFail($id);
		return view('admin.research.ideal.detail', compact('data'));
	}
}
