<?php

namespace App\Enums;

class EWeekDays {
    const SUNDAY = 0;
    const MONDAY = 1;
    const TUESDAY = 2;
    const WEDNESDAY = 3;
    const THURSDAY = 4;
    const FRIDAY = 5;
    const SATURDAY = 6;

    public static function getList() {
        return [
            self::SUNDAY => __('data_field_name.notification.sunday'),
            self::MONDAY => __('data_field_name.notification.monday'),
            self::TUESDAY => __('data_field_name.notification.tuesday'),
            self::WEDNESDAY => __('data_field_name.notification.wednesday'),
            self::THURSDAY => __('data_field_name.notification.thursday'),
            self::FRIDAY => __('data_field_name.notification.friday'),
            self::SATURDAY => __('data_field_name.notification.saturday'),
        ];
    }
}
