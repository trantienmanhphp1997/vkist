<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_periods', function (Blueprint $table) {
            $table->id();
            $table->string('name', 500)->nullable()->comment('ten ki kiem ke');
            $table->date('start_date')->nullable()->comment('ngay bat dau');
            $table->date('end_date')->nullable()->comment('ngay ket thuc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_periods');
    }
}
