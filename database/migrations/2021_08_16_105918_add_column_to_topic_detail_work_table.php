<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTopicDetailWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_detail_work', function($table) {
            $table->dropColumn('content');
            $table->foreignId('task_id')->nullable()->constrained('task');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_detail_work', function($table) {
            $table->string('content', 1000)->comment('Noi dung cong viec');
            $table->dropColumn('task_id');
        });
    }
}
