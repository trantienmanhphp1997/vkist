<?php
return [
    'status' => 'Trạng thái',
    'level' => 'Mức độ ưu tiên',
    'project-code'=>'Mã dự án',
    'group-task' => 'Công việc nhóm',
    'personal-task' => 'Công việc của tôi',
    'select-status'=>[
        'undo'=>'Chưa làm',
        'on-working'=>'Đang làm',
        'approve'=>'Đợi phê duyệt',
        'done'=>'Đã hoàn thành'
    ],
    'select-work-type'=>[
        'main'=>'Nhiệm vụ chính',
        'sub'=>'Nhiệm vụ con',
        'detail'=>'Công việc chi tiết'
    ],
    'select-level'=>[
        'low'=>'Thấp',
        'medium'=>'Trung bình',
        'high'=>'Cao',
        'emergency'=>'Khẩn cấp'
    ],
    'table' => [
        'project-code'=>'Mã dự án',
        'work-type'=>'Loại công việc',
        'task-code'=>'Mã công việc',
        'priority-level'=>'Mức độ ưu tiên',
        'name'=>'Tên công việc',
        'plan'=>'Kế hoạch',
        'start'=>'Bắt đầu',
        'end'=>'Kết thúc',
        'reality'=>'Thực tế',
        'status'=>'Trạng thái',
        'assign'=>'Người thực hiện',
        'preview'=>'Người đánh giá'
    ],
    'form-data' => [
        'topic'=>'Tên đề tài',
        'main-mission'=>'Công việc chính',
        'select-main-task'=>'Chọn công việc chính',
        'mission'=>'Công việc',
        'select-task'=>'Chọn công việc',
        'level'=>'Mức ưu tiên',
        'status'=>'Trạng thái',
        'work-type'=>'Loại công việc',
        'actual-time-topic'=>'Thời gian thực hiện',
        'name'=>'Tên công việc',
        'start-date'=>'Ngày bắt đầu',
        'end-date'=>'Ngày kết thúc',
        'description'=>'Mô tả công việc',
        'follower'=>'Người theo dõi',
        'assign'=>'Chỉ định',
        'reviewer'=>'Đánh giá',
        'approve'=>'Phê duyệt',
        'file'=>'Đính kèm tài liệu',
        'member'=>'Người tham gia',
        'topic-start-date'=>'Thời gian bắt đầu theo kế hoạch',
        'topic-end-date'=>'Thời gian kết thúc theo kế hoạch',
        'real-start-date'=>'Ngày bắt đầu thực tế',
        'real-end-date'=>'Ngày kết thúc thực tế',

    ],
    'create' => 'Tạo mới công việc',
    'update' => 'Cập nhật công việc',
    'index'=>'Danh sách công việc',
    'not-khown-project'=>'Chưa được tạo dự án',
    'msg'=>[
        'create'=>[
            'success'=>'Tạo mới công việc thành công',
            'fail'=>'Tạo mới công việc thất bại'
        ],
        'update'=>[
            'success'=>'Cập nhập công việc thành công',
            'fail'=>'Cập nhập thông tin thất bại'
        ]
    ],
    'member-exist'=>'Người này đã tồn tài trong danh sách',
    'member-remove-success'=>'Xóa người theo dõi thành công',
    'member-add-success'=>'Thêm người theo dõi thành công',
    'person'=>[
        'follow'=>'Danh sách người theo dõi',
        'assign'=>'người được chỉ định',
        'preview'=>'người đánh giá',
        'approval'=>'Người phê duyệt'
    ],
    'unknown'=>'Không xác định',
    'name-follower'=>'Tên người theo dõi'
];
