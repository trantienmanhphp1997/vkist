<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Config;

use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;


class ResearchAchievement extends BaseModel
{
    use HasFactory;
    protected $table = "research_achievements";
    protected $guarded=['*'];
    private static $searchable = [
        'name',
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }

    public function topic() {
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function files() {
        return $this->hasMany(File::class, 'model_id')->where('files.type','=',Config::get('common.type_upload.ResearchAchievement'));
    }
}
