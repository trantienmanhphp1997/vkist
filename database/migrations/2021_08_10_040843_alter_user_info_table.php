<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->dropColumn('birthday');
        });

        Schema::table('user_info', function (Blueprint $table) {
            $table->date('birthday')->nullable()->comment('Ngày sinh');
            $table->tinyInteger('status')->nullable()->comment('Trạng thái làm việc');
            $table->date('start_working_date')->nullable()->comment('Ngày làm việc đầu tiên');
            $table->date('contract_sign_date')->nullable()->comment('Ngày ký hợp đồng');
            $table->date('retire_date')->nullable()->comment('Ngày nghỉ việc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->dropColumn('birthday');
            $table->dropColumn('status');
            $table->dropColumn('start_working_date');
            $table->dropColumn('contract_sign_date');
            $table->dropColumn('retire_date');
        });

        Schema::table('user_info', function (Blueprint $table) {
            $table->string('birthday', 255)->nullable();
        });
    }
}
