<?php


namespace App\Enums;


class EDecisionRewardDecisionType
{
    const REWARD = 1;
    const DISCIPLINE = 2;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::REWARD:
                $result = __('decision-reward/decision-reward.decision-reward-decision-type.reward');
                break;
            case self::DISCIPLINE:
                $result = __('decision-reward/decision-reward.decision-reward-decision-type.discipline');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
