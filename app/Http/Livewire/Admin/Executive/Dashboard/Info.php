<?php

namespace App\Http\Livewire\Admin\Executive\Dashboard;

use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\UserInf;
use App\Models\UserInfoLeave;
use App\Models\WorkPlan;
use App\Models\WorkPlanHasUserInfo;
use Illuminate\Support\Carbon;
class Info extends Component
{

	public $data;

    public function render() {
        $thirty = now()->copy()->subYears(30);
        $fortyFive = now()->copy()->subYears(45);
        $sixty = now()->copy()->subYears(60);

        $underThirty = UserInf::where('birthday', '>', $thirty->startOfDay())->count();
        $thirtyToFortyFive = UserInf::where('birthday', '<=', $thirty->copy()->endOfDay())->where('birthday', '>=', $fortyFive->copy()->startOfDay())->count();
        $fortyFiveToSixTy = UserInf::where('birthday', '<=', $fortyFive->copy()->endOfDay())->where('birthday', '>=', $sixty->copy()->startOfDay())->count();
        $overSixty = UserInf::where('birthday', '>', $sixty->startOfDay())->count();

        $pieChartData = [$underThirty, $thirtyToFortyFive, $fortyFiveToSixTy, $overSixty];

        $year1 = now()->copy()->subYears(3)->year;
        $year2 = now()->copy()->subYears(2)->year;
        $year3 = now()->copy()->subYears(1)->year;
        $year4 = now()->copy()->year;

        $userLeave = [
            count($this->getUserLeave(now()->copy()->subYears(3))),
            count($this->getUserLeave(now()->copy()->subYears(2))),
            count($this->getUserLeave(now()->copy()->subYears(1))),
            count($this->getUserLeave(now())),
        ];
        
        $userWorking = [
            $this->countUserWorking(now()->copy()->subYears(3)),
            $this->countUserWorking(now()->copy()->subYears(2)),
            $this->countUserWorking(now()->copy()->subYears(1)),
            $this->countUserWorking(now()),
        ];

        $start = [1, 4, 7, 10];
        $quarter = null;
        $startQuarter = null;
        $endQuater = null;

        foreach($start as $index => $item) {
            if (now()->copy()->month >= $item) {
                $quarter = $index + 1;
                if ($quarter == 1) {
                    $startQuarter = now()->copy()->startOfYear();
                    $endQuater = now()->copy()->startOfYear()->addMonths(2)->endOfMonth();
                }elseif ($quarter == 2) {
                    $startQuarter = now()->copy()->startOfYear()->addMonths(3);
                    $endQuater = now()->copy()->startOfYear()->addMonths(5)->endOfMonth();
                }elseif ($quarter == 3) {
                    $startQuarter = now()->copy()->startOfYear()->addMonths(6);
                    $endQuater = now()->copy()->startOfYear()->addMonths(8)->endOfMonth();
                }else {
                    $startQuarter = now()->copy()->startOfYear()->addMonths(9);
                    $endQuater = now()->copy()->startOfYear()->addMonths(11)->endOfMonth();
                }
            }
        }
        
        $workPlan = WorkPlan::
            where('start_date', '>=', $startQuarter)->where('start_date', '<=', $endQuater)
            ->orderBy('start_date', 'desc')
            ->limit(10)->get();
        $workPlan = $workPlan->map(function ($item, $index) {
            $workPlanHasUserInfo = WorkPlanHasUserInfo::join('user_info', 'user_info_id', 'user_info.id')->where('work_plan_id', $item->id)->select('fullname')->get();
            return [
                'index' => $index + 1,
                'id' => $item->id,
                'code' => $item->missions_code,
                'start_date' => Carbon::parse($item->start_date)->format('d/m/Y'),
                'workPlanHasUserInfo' => $workPlanHasUserInfo,
                'content' => $item->content
            ];
        });
        
        return view('livewire.admin.executive.dashboard.info', [
            'pieChartData' => $pieChartData,
            'userLeave' => $userLeave,
            'userWorking' => $userWorking,
            'year' => [$year1, $year2, $year3, $year4],
            'workPlan' => $workPlan,
            'quarter' => $quarter
        ]);
    }

    public function getUserLeave($day) {
        $arrUserId = [];
        $data = UserInfoLeave::where('created_at', '>=', $day->copy()->startOfYear())->where('created_at', '<=', $day->copy()->endOfYear())->where('status', 1)->get()->unique('user_info_id');
        foreach($data as $item) {
            array_push($arrUserId, $item->user_info_id);
        }
        return $arrUserId;
    }

    public function countUserWorking($day) {
        $arrUserId = $this->getUserLeave($day);
        return UserInf::where('created_at', '>=', $day->copy()->startOfYear())->where('created_at', '<=', $day->copy()->endOfYear())->whereNotIn('id', $arrUserId)->count();
    }
}
