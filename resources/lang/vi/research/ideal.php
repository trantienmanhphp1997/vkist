<?php
return [
    'breadcrumbs' => [
        'research-ideal-management' => 'Quản lý ý tưởng nghiên cứu',
    ],
    'list' => 'Danh sách ý tưởng',
    'filter' => [
        'input' => [
            'placeholder' => [
                'search' => 'Tìm kiếm',
                'proposer' => 'Người đề xuất',
            ],
        ],
        'select-box' => [
            'option' => [
                'status' => 'Trạng thái',
            ],
        ],
    ],
    'status' => [
        'waiting_for_appraisal' => 'Chờ phê duyệt',
        'approved' => 'Phê duyệt',
        'refuse' => 'Từ chối',
        'just_created' => 'Chưa nộp',
    ],
    'table_column' => [
        'code' => 'MÃ Ý TƯỞNG',
        'name' => 'TÊN Ý TƯỞNG',
        'proposer' => 'NGƯỜI ĐỀ XUẤT',
        'status' => 'TRẠNG THÁI',
        'research_field' => 'Lĩnh vực',
        'created_at' => 'Thời gian đề xuất',
        'updated_at' => 'Thời gian cập nhật',
        'action' => 'HÀNH ĐỘNG'
    ],
    'title' => [
        'create' => 'Tạo ý tưởng mới',
        'edit' => 'Chỉnh sửa',
        'detail' => 'Thông tin chi tiết ý tưởng'
    ],
];
