<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClaimForm extends BaseModel
{
    use HasFactory;
    protected $table='claim_form';
    protected $fillable=['claim_date','content','reason','admin_id','type','receiver_id','manager_id'];
    public function assetForm(){
        return $this->belongsToMany(Asset::class,'asset_claim_form','form_id','asset_id');
    }
}
