<?php

namespace App\Enums;

class EShoppingStatus {
    const NOT_APPROVED = 1;
    const APPROVED = 2;
    const PENDING = 3;
    const DELIVERING = 4;
    const COMPLETED = 5;
    const REJECTED = 6;

    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            self::NOT_APPROVED => __('data_field_name.shopping.status_not_approved'),
            self::APPROVED => __('data_field_name.shopping.status_approved'),
            self::PENDING => __('data_field_name.shopping.status_pending'),
            self::DELIVERING => __('data_field_name.shopping.status_delivering'),
            self::COMPLETED => __('data_field_name.shopping.status_completed'),
            self::REJECTED => __('data_field_name.shopping.status_rejected'),
        ];
    }

    public static function getHTMLClassList() {
        return [
            self::NOT_APPROVED => '<button class="btn btn-plan bg-8 text-8">' . __('data_field_name.shopping.status_not_approved') . '</button>',
            self::APPROVED => '<button class="btn btn-plan bg-7 text-5">' . __('data_field_name.shopping.status_approved') . '</button>',
            self::PENDING => '<button class="btn btn-plan bg-9 text-7">' . __('data_field_name.shopping.status_pending') . '</button>',
            self::DELIVERING => '<button class="btn btn-plan bg-2 text-6">' . __('data_field_name.shopping.status_delivering') . '</button>',
            self::COMPLETED => '<button class="btn btn-plan bg-2 text-6">' . __('data_field_name.shopping.status_completed') . '</button>',
            self::REJECTED => '<button class="btn btn-plan bg-8 text-8">' . __('data_field_name.shopping.status_rejected') . '</button>',
        ];
    }

    public static function valueToHTMLClassName($value = 0) {
        $arr = self::getHTMLClassList();
        return $arr[$value] ?? $value;
    }
}
