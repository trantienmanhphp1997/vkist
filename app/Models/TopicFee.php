<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\TopicFeeDetail;

class TopicFee extends BaseModel
{
    use HasFactory;

    protected $table = 'topic_fee';
    protected $fillable = [
    	'name', 'source_capital', 'status', 'topic_id', 'admin_id', 'unsign_text', 'code', 'gw_attach_file'
    ];

    private static $searchable = [
        'code', 'name'
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }

    public function topic() {
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function admin(){
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function topicFeeDetail(){
        return $this->hasMany(TopicFeeDetail::class, 'topic_fee_id', 'id');
    }

    public function totalCapital(){
        return TopicFeeDetail::where('topic_fee_id', $this->id)->sum('total_capital');
    }

    public function getTotalStateCapitalAttribute()
    {
        return TopicFeeDetail::where('topic_fee_id', $this->id)->sum('state_capital');
    }

    public function getTotalOtherCapitalAttribute()
    {
        return TopicFeeDetail::where('topic_fee_id', $this->id)->sum('other_capital');
    }

    public function getTotalCapitalAttribute()
    {
        return TopicFeeDetail::where('topic_fee_id', $this->id)->sum('total_capital');
    }
}
