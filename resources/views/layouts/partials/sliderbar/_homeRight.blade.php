<div class="col-md-3 col-xl-3 sidebar-right">
    <style>
        .title-nav-link{
            color: #4E5D78;
        }
        .count-number-nav-link{
            color: #8A94A6;
        }
        .media:hover{
            background: #EBF2FF;
        }
    </style>
    @if(checkPermission('admin.user.index'))
        <div class="media align-items-center">
            <img src="/images/Group.svg" alt="group" height="32px">
            <div class="media-body pl-media-body">

                <a class="nav-link pl-0-nav-link" href="{{route('admin.user.index')}}">
                    <h4 class="mb-0 title-nav-link">{{ __('menu_management.menu_name.user_info_list') }}
                    </h4>
                    <span class="count-number-nav-link">{{$userInfoCount}} {{ __('menu_management.menu_name.personal') }}</span>
                </a>
            </div>
        </div>
    @endif


@if(checkPermission('admin.general.work-plan.index'))
        <div class="media align-items-center">
            <img src="/images/Pen&ruller.svg" alt="pen-ruler" height="32px">
            <div class="media-body">
                <a class="nav-link" href="{{route('admin.general.work-plan.index')}}">
                    <h4 class="mb-0 title-nav-link">{{ __('menu_management.menu_name.general-manager') }}
                    </h4>
                    <span class="count-number-nav-link">{{$workPlanCount}} {{ __('menu_management.menu_name.workplan') }}</span>
                </a>

            </div>
        </div>
    @endif

    @if(checkPermission('admin.budget.index'))
        <div class="media align-items-center">
            <img src="/images/Money.svg" alt="money" height="32px">
            <div class="media-body pl-media-body">
                <a class="nav-link pl-0-nav-link" href="{{route('admin.budget.index')}}">
                    <h4 class="mb-0 title-nav-link">{{ __('menu_management.menu_name.budget-management') }}
                    </h4>
                    <span class="count-number-nav-link">{{$budgetCount}} {{ __('menu_management.menu_name.budget') }}</span>
                </a>

            </div>
        </div>

    @endif

    @if(checkPermission('admin.executive.shopping.index'))
        <div class="media align-items-center">
            <img src="/images/Cart.svg" alt="cart" height="32px">
            <div class="media-body pl-media-body">
                    <a class="nav-link pl-0-nav-link @if(in_array(Route::currentRouteName(), ['admin.executive.shopping.index'])) active @endif"
                href="{{route('admin.executive.shopping.index')}}">
                <h4 class="mb-0 title-nav-link"> {{ __('menu_management.menu_name.manage-shopping') }}
                </h4>
                <span class="count-number-nav-link">{{$shoppingCount}} {{ __('menu_management.menu_name.shopping') }}</span>
                </a>
            </div>
        </div>
    @endif
    @if(checkPermission('admin.asset.dashboard.index'))
        <div class="media align-items-center">
            <img src="/images/Key.svg" alt="key" height="32px">
            <div class="media-body pl-media-body">
                <a class="nav-link pl-0-nav-link @if(in_array(Route::currentRouteName(), ['admin.asset.dashboard.index'])) active @endif"
                   href="{{route('admin.asset.dashboard.index')}}">
                    <h4 class="mb-0 title-nav-link">{{ __('menu_management.menu_name.asset-management') }}
                    </h4>
                    <span class="count-number-nav-link">{{$assetCount}} {{ __('menu_management.menu_name.asset') }}</span>
                </a>

            </div>
        </div>
    @endif

    <div class="media align-items-center">
        <img src="/images/Group-chat.png" alt="group-chat" height="30px">
        <div class="media-body">
            <a class="nav-link" href="{{route('api.login.whisper.index')}}" target="_blank">
                <h4 class="mb-0 title-nav-link">{{ __('menu_management.menu_name.message') }}
                </h4>
                <span class="count-number-nav-link">{{$userCount}} {{ __('menu_management.menu_name.new_message') }}</span>
            </a>
        </div>
    </div>
    @if(checkPermission('admin.news.posts.index'))
        <div class="media align-items-center">
            <img style="    margin-left: 4px;" src="/images/newsShortcut.png" alt="news-shortcut" height="30px">
            <div class="media-body pl-media-body" >
                <a class="nav-link pl-0-nav-link"
                   href="{{route('admin.news.posts.index')}}">
                    <h4 class="mb-0 title-nav-link">{{__('news/newsManager.menu_name.news')}}
                    </h4>
                    <span class="count-number-nav-link">{{$newsCount}} {{ __('menu_management.menu_name.news') }}</span>
                </a>
            </div>
        </div>
    @endif

    @if(checkPermission('admin.system.account.index'))
        <div class="media align-items-center">
            <img src="/images/Group.svg" alt="group" height="32px">
            <div class="media-body pl-media-body">

                <a class="nav-link pl-0-nav-link" href="{{route('admin.system.account.index')}}">
                    <h4 class="mb-0 title-nav-link">{{ __('menu_management.menu_name.account_management') }}
                    </h4>
                    <span class="count-number-nav-link">{{$userCount}} {{ __('menu_management.menu_name.personal') }}</span>
                </a>
            </div>
        </div>
    @endif

</div>
