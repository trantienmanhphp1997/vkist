<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplateFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_form', function (Blueprint $table) {
            $table->id();
            $table->string('name', 1000)->nullable()->comment('Ten bieu mau');
            $table->tinyInteger('type')->comment('loai bieu mau. 1: ngan sach, 2: tong vu, 3: du toan, 4: ke hoach, 5: khac');
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_form');
    }
}
