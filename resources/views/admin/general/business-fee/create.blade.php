@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.businesfee.update_fee_result')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
<div class="col-md-8 col-xl-11 box-news box-user">
<div class="row">
            <div class="col-md-12">
                <div  class="breadcrumbs"><a href="">{{__('data_field_name.businesfee.business_fee_control')}}</a> \ <span>{{__('data_field_name.businesfee.create_business')}}</span></div> 
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="information">
                    <div class="group-tabs">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <form method="POST" action="{{route('admin.general.businessfee.store')}}" class="">
                                    {{ method_field('POST') }}
                                    @csrf
                                    @include('admin.general.business-fee._formBusinessFee')
                                </form>  
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

