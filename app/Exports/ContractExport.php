<?php

namespace App\Exports;

use App\Models\UserInf;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use App\Models\Contract;
use App\Models\MasterData;
use App\Enums\EContractType;
use App\Models\ContractDuration;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
class ContractExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnWidths, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($searchTerm, $filterStatus, $from_date, $to_date) {
        $this->searchTerm = $searchTerm;
        $this->filterStatus = $filterStatus;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 5,
            'B' => 20,
            'C' => 20,
            'D' => 20,
            'E' => 20,
            'F' => 20,
            'G' => 20,
            'H' => 20,
            'I' => 20,
            'J' => 20,
            'K' => 20,
            'L' => 20,
            'M' => 20,      
        ];
    }

    public function collection()
    {
        $query = Contract::query()
            ->leftJoin('user_info', 'user_info.id', 'contract.user_info_id')
            ->leftJoin('master_data', 'master_data.id', 'user_info.position_id')
            ->leftJoin('contract_duration', 'contract_duration.id', 'contract.duration_id')
            ->whereIn('contract_type', [EContractType::WORK, EContractType::LABOR])
            ->select('contract.*', 'user_info.fullname as user_name', 'master_data.v_value as position_value', 'contract_duration.name as duration');
        $searchTerm = $this->searchTerm;
        $status = $this->filterStatus;

        if ($status != null) {
            if (!in_array($status, [1, -1])) {
                $query->onlyTrashed();
            } else {
                $query->where('contract.status', $status);
            }
        }

        if (!empty($searchTerm)) {
            $query->where(function($q) use ($searchTerm) {
                $q->whereRaw('lower(user_info.fullname) like ? ', ['%' . trim(mb_strtolower($searchTerm, 'UTF-8')) . '%'])
                    ->orWhereRaw('lower(contract.contract_code) like ? ', ['%' . trim(mb_strtolower($searchTerm, 'UTF-8')) . '%']);
                });
        }
        if(!is_null($this->from_date)) {
            $query->where('start_date', '>=', $this->from_date);
        }
        if(!is_null($this->to_date)) {
            $query->where('end_date', '<=', $this->to_date);
        }
        $data = $query->orderBy('contract.id','DESC')->get();
        $data = $data->map(function ($item, $index) {
            return [
                'index' => $index + 1,
                'name' => $item->user_name,
                'code' => $item->contract_code,
                'position' => $item->position_value,
                'type' => EContractType::valueToName($item->contract_type),
                'basic_salary' => numberFormat($item->basic_salary, '.', '.'),
                'social_insurance' => numberFormat($item->social_insurance_salary, '.', '.'),
                'coefficients_salary' => $item->coefficients->v_value ?? null,
                'bank_account_number' => $item->bank_account_number,
                'bank_name_id' => MasterData::find($item->bank_name_id)->v_value ?? null,
                'duration' => ContractDuration::find($item->duration_id)->name ?? null,
                'start_date' => reFormatDate($item->start_date),
                'end_date' => reFormatDate($item->end_date)
            ];
        });
        return $data;
    }
    public function headings():array
    {
        return
        [
            'STT',
            'HỌ VÀ TÊN',
            'SỐ HỢP ĐỒNG',
            'CHỨC DANH',
            'LOẠI HỢP ĐỒNG',
            'LƯƠNG CƠ BẢN',
            'LƯƠNG BHXH',
            'HỆ SỐ LƯƠNG',
            'TK NGÂN HÀNG',
            'NGÂN HÀNG',
            'THỜI HẠN HỢP ĐỒNG',
            'NGÀY BẮT ĐẦU',
            'NGÀY KẾT THÚC'
        ];
    }

    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Arial', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Arial', 'size' => 20,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $borderStyle = [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [
                       'rgb' => '808080'
                    ]
                ]
            ];
            $active_sheet = $event->sheet->getDelegate();
            $highestRow = $active_sheet->getHighestRow();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'
            ];
            $event->sheet->mergeCells('C1:F1');
            $event->sheet->setCellValue('C1','DANH SÁCH HỢP ĐỒNG LAO ĐỘNG');
            $event->sheet->getStyle('C1:F1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:M3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle('A3:M' . $highestRow)->getBorders()->applyFromArray($borderStyle);
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );

        },];

    }
    public function title(): string
    {
        return 'DANH SÁCH HỢP ĐỒNG LAO ĐỘNG';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
