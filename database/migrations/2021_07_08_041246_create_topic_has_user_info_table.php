<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicHasUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic_has_user_info', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('danh sach de tai');
            $table->bigInteger('academic_level_id')->nullable()->comment(' trinh do học van map master data type = 5');
            $table->string('mission_note', 255)->nullable()->comment('nhiem vu neu co');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('Map voi user_info ');
            $table->foreign('user_info_id')->references('id')->on('user_info');
            $table->unsignedBigInteger('topic_id')->nullable()->comment('Map voi topic ');
            $table->foreign('topic_id')->references('id')->on('topic');
            $table->bigInteger('research_id')->nullable()->comment('map voi master_ data type = 18');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topic_has_user_info');
    }
}
