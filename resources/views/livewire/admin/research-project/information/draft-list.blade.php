<div class="col-md-10 col-xl-11 box-detail box-user">
    <style>
        .select-status-project {
            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }
        .title-approve{
            margin: 0 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a
                    href="{{ route('admin.research-project.information.index') }}">{{__('research/project.name')}}</a> \
                <span>{{__('research/project.information.breadcrumbs')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('research/project.information.table-title')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group search-expertise">
                                <div class="search-expertise inline-block">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control"
                                           placeholder="{{__('research/project.information.search')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @if($checkCreatePermission)
                        <div class="col-md-2 ">
                            <div class="form-group float-right">
                                <a href="{{ route('admin.research-project.information.create') }}"
                                   class="btn btn-viewmore-news mr0 ">
                                    <img src="/images/plus2.svg" alt="plus">{{__('research/project.information.create')}}
                                </a>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div wire:loading class="loader" style="z-index: 10"></div>
                   <div class="table-responsive">
                    <table class="table table-general">
                            <thead>
                            <tr class="border-radius text-center">
                                <th scope="col" class="border-radius-left">
                                    {{ __('data_field_name.topic.stt') }}
                                </th>
                                <th scope="col">
                                    {{__('research/project.information.table-column.code-project')}}
                                </th>
                                <th scope="col" class="text-center w-25">
                                    {{__('research/project.information.table-column.name')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('research/project.information.table-column.research-category')}}
                                </th>
                                <th scope="col">
                                    {{__('research/project.information.table-column.leader')}}
                                </th>
                                <th scope="col">
                                    {{__('research/project.information.table-column.status')}}
                                </th>
                                <th scope="col">
                                </th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>

                            @foreach ($data as $row)
                                <tr>
                                    <td class="text-center">{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.research-project.information.edit', $row->id) }}">
                                            {!! boldTextSearch($row->project_code, $searchTerm) !!}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        {!! boldTextSearch($row->name, $searchTerm) !!}
                                    </td>
                                    <td class="text-center">{!! boldTextSearch(($row->researchCategory->name ?? ''), $searchTerm) !!}</td>
                                    <td class="text-center">{!! boldTextSearch(( $row->topic->leader->fullname ?? ''), $searchTerm) !!}</td>
                                    <td class="text-center">
                                        {{
                                            Form::select('status',
                                            array(
                                                0=> __('research/project.status.wait_approval') ,
                                                1=>__('research/project.status.in_process'),
                                                2=>__('research/project.status.completed'),
                                                3=>__('research/project.status.cancel'),
                                                4=>__('research/project.status.draft')
                                                ),  $row->status,
                                                ['class'=> ($row->status == 0 ? 'wait-approval' :
                                                ($row->status == 1 ? 'in-process' :
                                                ($row->status == 2 ? 'completed' : (
                                                    $row->status == 3 ?  'cancel' : 'draft'
                                                ))).'').' select-status-project',
                                                'disabled'
                                                ]
                                            )}}
                                    </td>

                                    <td>
                                        @if($checkEditPermission)
                                            <a class="btn par6"
                                            data-toggle="modal" data-target="#transferToOperation" wire:click="setProjectID({{$row->id}})">
                                                <img width="40px" src="/images/fileIcon.png" alt="file">
                                            </a>
                                        @endif

                                        @if(checkButtonCanView('admin.research-project.information.delete'))
                                            @include('livewire.common.buttons._delete')
                                        @endif
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                   </div>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif
                    <div wire:ignore.self class="modal fade" id="transferToOperation" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header" style="padding-left: 0">
                                    <strong>
                                        <h5 class="modal-title" id="exampleModalLabel">{{__('notification.member.warning.warning')}}</h5>
                                    </strong>
                                </div>
                                <div class="modal-body" style="font-size: 16px">
                                    {{__('research/project.error.warning-transfer-to-operation')}}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-cancel" style="    padding: 8px 20px;" data-dismiss="modal">{{__('common.button.back')}}</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"  wire:click="transferToOperation">Khôi phục</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('livewire.common.modal._modalDelete')
                </div>
            </div>
        </div>
    </div>
</div>
