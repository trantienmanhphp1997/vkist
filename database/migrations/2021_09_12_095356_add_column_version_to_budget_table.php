<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnVersionToBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget', function (Blueprint $table) {
            $table->string('request_gw_data')->nullable()->comment('thông tin thay đổi budget- lưu json');
            $table->tinyInteger('request_gw_status')->nullable()->comment('request status sau khi thay đổi budget');
            $table->tinyInteger('version')->nullable()->comment('version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget', function (Blueprint $table) {
            $table->dropColumn('request_gw_data');
            $table->dropColumn('request_gw_status');
            $table->dropColumn('version');
        });
    }
}
