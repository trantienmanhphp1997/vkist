<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="{{ route('admin.research.topic-fee.index') }}">{{ __('data_field_name.research_cost.research_cost_text') }}</a> \ <span>{{ __('data_field_name.research_cost.budget_list') }}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{ __('data_field_name.research_cost.budget_list') }}</h3>
                </div>
                <div class="col-md-6" wire:ignore>
                    @if($checkCreatePermission)
                    <div class=" float-right">
                        <button class="btn btn-viewmore-news mr0 buttonCreate" data-toggle="modal" data-target="#createTopicFee"><img src="/images/plus2.svg" alt="plus">
                            {{__('data_field_name.research_cost.budget_create')}}
                        </button>
                    </div>
                    @endif
                </div>
            </div>
            <div class="information">
                <div class="col-md-12">
                    <div class="row box-filter mb-24">
                        <div class="d-flex filter-left">
                            <div class="search-expertise inline-block search-staff w-xs-100 ">
                                <input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 w-xs-100  form-control" placeholder="{{ __('data_field_name.research_cost.text_search_topic_fee') }}">
                                <span><img src="/images/Search.svg" alt="search"/></span>
                            </div>
                            <div class="px-12 d-flex">
                                <label class="w-160 size13">{{__('data_field_name.research_cost.source_capital')}}</label>
                                <select class="form-control size13" wire:model.lazy="searchSource">
                                    <option value="">{{__('data_field_name.common_field.all')}}</option>
                                    @foreach($getListSource as $key => $source)
                                        <option value='{!! $key !!}'>{!! $source !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="px-12 d-flex">
                                <label class="w-140 size13">{{__('data_field_name.common_field.status')}}</label>
                                <select class="form-control size13" wire:model.lazy="searchStatus">
                                    <option value="">{{__('data_field_name.common_field.all')}}</option>
                                    @foreach($getListStatus as $key => $status)
                                        <option value='{!! $key !!}'>{!! $status !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inner-tab pt0">
                    <div wire:loading class="loader" style="z-index: 10"></div>
                   <div class="table-responsive">
                    <table class="table table-general">
                            <thead>
                                <tr class="border-radius text-center">
                                    <th scope="col" class="border-radius-left text-center">
                                        {{ __('data_field_name.research_cost.estimate_code') }}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{ __('data_field_name.research_cost.name_topic_fee') }}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{ __('data_field_name.topic.code') }}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{ __('data_field_name.topic.name') }}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{ __('data_field_name.research_cost.people_create') }}
                                    </th>
                                    <th scope="col">
                                        {{ __('data_field_name.research_cost.source_capital') }}
                                    </th>
                                    <th scope="col">
                                        {{ __('data_field_name.topic.status') }}
                                    </th>
                                    <th scope="col" class="border-radius-right">
                                        {{ __('data_field_name.common_field.action') }}
                                    </th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                                @foreach ($data as $row)
                                    <tr>
                                        <td class="text-center" wire:ignore>
                                            @if($checkEditPermission)
                                                <a href="{{ route('admin.research.topic-fee.edit', $row->id) }}">
                                                    {!! $row->code ?? '' !!}
                                                </a>
                                            @else
                                                {!! $row->code ?? '' !!}
                                            @endif
                                        </td>
                                        <td class="text-center">{!! boldTextSearchV2($row->name, $searchTerm) !!}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.research.topic.detail', $row->topic->id) }}">
                                                {!! $row->topic->code ?? '' !!}
                                            </a>
                                        </td>
                                        <td class="text-center">{!! isset($row->topic->name) ? boldTextSearchV2($row->topic->name, $searchTerm) : '' !!}</td>
                                        <td class="text-center">{{ $row->admin->info->fullname ?? '' }}</td>
                                        <td class="text-center">{{ $getListSource[$row->source_capital] ?? '' }}</td>
                                        <td class="text-center">{{ $getListStatus[$row->status] ?? '' }}</td>
                                        <td class="text-center">
                                            @if ($row->status == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                                                @include('livewire.common.buttons._delete')
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                   </div>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete', ['title' => __('notification.common.warning.delete')])
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Create -->
    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="createTopicFee" tabindex="-1" aria-labelledby="createTopicFee" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body box-user ">
                        <h4 class="text-center">{{__('data_field_name.research_cost.text_create_cost')}}</h4>
                        <div class="form-group">
                            <label>{{__('data_field_name.topic.name')}}</label>
                            <select class="form-control form-control-lg" wire:model.defer="topic_id">
                                <option value="">{{__('data_field_name.research_plan.select_topic')}}</option>
                                @if(!empty($listTopic))
                                    @foreach($listTopic as $k => $val)
                                        <option value="{{$k}}">{{$val}}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('topic_id') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.name_topic_fee')}}</label>
                            <input type="text" class="form-control" wire:model.defer="name" placeholder="{{__('data_field_name.research_cost.text_input_cost')}}">
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                    </div>
                    <div class="text-center mr-24 ">
                        <button type="button" id="close-modal-edit-department" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                        <button wire:ignore id="buttonCreateBoard" type="button" wire:click.prevent="store()" class="btn btn-save buttonCreateEdit">{{__('common.button.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Create -->

    <!-- Modal Edit -->
    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="editTopicFee" tabindex="-1" aria-labelledby="editTopicFee" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body box-user ">
                        <h4 class="text-center">{{__('data_field_name.research_cost.edit_budget')}}</h4>
                        <div class="form-group">
                            <label>{{__('data_field_name.topic.name')}}</label>
                            <select class="form-control form-control-lg" wire:model.defer="topic_id">
                                <option value="">{{__('data_field_name.research_plan.select_topic')}}</option>
                                @if(!empty($listTopic))
                                    @foreach($listTopic as $k => $val)
                                        <option value="{{$k}}">{{$val}}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('topic_id') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.name_topic_fee')}}</label>
                            <input type="text" class="form-control" wire:model.defer="name" placeholder="{{__('data_field_name.research_cost.text_input_cost')}}">
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                    </div>
                    <div class="text-center mr-24 ">
                        <button type="button" id="close-modal-edit-department" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                        <button wire:ignore id="buttonCreateBoard" type="button" wire:click.prevent="update()" class="btn btn-save buttonCreateEdit">{{__('common.button.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Edit -->

    <script>
        $("document").ready(function() {
            $('.buttonCreate, .buttonEditTopicFee').click(function() {
                $('.error').remove();
                $('.form-input').val('');
            });

            $('#createTopicFee, #editTopicFee').on('hidden.bs.modal', function () {
                $('.error').remove();
                window.livewire.emit('resetData');
            });
        });
    </script>
</div>
