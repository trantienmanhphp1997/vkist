<div class="col-md-12">
    <div class="col-md-12">
        <form wire:submit.prevent="submit" enctype="multipart/form-data">
            <div class="form-group upload-file">
                <label>{{__('data_field_name.work_plan.create.file_title')}}</label>
                <div class="bd-attached">
                    <div class="attached-center">
                        <input type="file" wire:model.lazy="file" name="file_name" id="input_file_create" class="custom-file-input cur_input">
                        <span class="file" >
                            {{__('data_field_name.work_plan.create.file_btn')}}
                            <img src="/images/Clip.svg" alt="" height="24">
                        </span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        @foreach ($files as $key => $val)
        <div class="form-group">
            <div class="attached-files">
                <img src="/images/File.svg" alt="file">
                <div class="content-file">
                    <p>{{ $val-> file_name }}</p>
                </div>
                <span>
                    <button wire:click.prevent="deleteFile({{$val->id}})"
                        style="border: none;background: white" type="button"><img
                        src="/images/close.svg" alt="close"/>
                    </button>
                </span>
            </div>
        </div>
        @endforeach
    </div>
</div>



