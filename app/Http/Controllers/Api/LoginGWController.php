<?php

namespace App\Http\Controllers\Api;

use App\Enums\EApiApproval;
use App\Enums\EApproval;
use App\Enums\EBudgetStatus;
use App\Enums\EIdeaStatus;
use App\Enums\ERequestLeave;
use App\Enums\ETopicFee;
use App\Enums\ETopicStatus;
use App\Enums\EWorkPlan;
use App\Http\Controllers\Controller;
use App\Models\Approval;
use App\Models\ApprovalIdealDetail;
use App\Models\Budget;
use App\Models\Ideal;
use App\Models\TopicFeeDetail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\GwApproveResponse;
use Illuminate\Support\Facades\Log;
use DateTime;
use DB;
use function GuzzleHttp\json_encode;

class LoginGWController extends Controller
{
    public function __construct(){
        $this->middleware('auth', ['except' => 'handleResponse']);
    }
    public function loginMail(Request $request, Response $response){
        $admin_id = \auth()->id();
        try {
            $response = Http::post('http://gw.vkist.gov.vn/ngw/sign/auth', [
                'gw_id' => Auth::user()->gw_user_id,
                'gw_pass' => Auth::user()->gw_pass,
                'raw' => 1,
            ]);
        } catch (\Exception $e) {
            Log::error('(LoginGwController/loginMail) Caught exception: '.$e->getMessage().', '.$admin_id);
        }

        if($response->json()['success']=='true') {
            return Redirect::to('http://gw.vkist.gov.vn/ngw/sign/check_token?auth_key='.$response->json()['jwt'].'&method=mail');
        }
        else {
            $url = 'https://gw.vkist.gov.vn/ngw/app/#/';
            return redirect($url);
        }
    }
    public function loginApproval(Request $request, Response $response){
        $admin_id = \auth()->id();
        try {
            $response = Http::post('http://gw.vkist.gov.vn/ngw/sign/auth', [
                'gw_id' => Auth::user()->gw_user_id,
                'gw_pass' => Auth::user()->gw_pass,
                'raw' => 1,
            ]);
        } catch (\Exception $e) {
            Log::error('(LoginGwController/loginApproval) Caught exception: '.$e->getMessage().', '.$admin_id);
        }
        if($response->json()['success']=='true') {
            return redirect('http://gw.vkist.gov.vn/ngw/sign/check_token?auth_key='.$response->json()['jwt'].'&method=approval');
        }
        else {
            $url = 'https://gw.vkist.gov.vn/ngw/app/#/';
            return redirect($url);
        }
    }

    public function loginWhisper(Request $request, Response $response){
        $admin_id = \auth()->id();
        try {
            $response = Http::post('http://gw.vkist.gov.vn/ngw/sign/auth', [
                'gw_id' => Auth::user()->gw_user_id,
                'gw_pass' => Auth::user()->gw_pass,
                'raw' => 1,
            ]);
        } catch (\Exception $e) {
            Log::error('(LoginGwController/loginWhisper) Caught exception: '.$e->getMessage().', '.$admin_id);
        }
        if($response->json()['success']=='true') {
            return redirect('http://gw.vkist.gov.vn/ngw/sign/check_token?auth_key='.$response->json()['jwt'].'&method=whisper');
        }
        else {
            $url = 'https://gw.vkist.gov.vn/ngw/app/#/';
            return redirect($url);
        }
    }

    public function handleRequest(Request $request, Response $response){
        $admin_id = Auth::user()->id;
        try {
            $response = Http::get('http://gw.vkist.gov.vn/ngw/approval/sso/token', [
                'id' => 'Admin',
                'empno' => '999999',
            ]);
        } catch (\Exception $e) {
            Log::error('(LoginGwController/handleRequest) Caught exception (get_token): '.$e->getMessage().', '.$admin_id);
        }

        if(!$response->json()['success']){
            Log::error('(LoginGwController/handleRequest) Approval request denied, ip is not allowed: '. json_encode($response));
            return [
                'success' => false,
                'message' => 'Gửi đề tài thất bại, IP không được cho phép',
            ];
        }

        // tạo mới request_id
        $request_id =  time() . random_int(0,99);
        $model_id = 2; // đang fix cứng $model_id = $requset->model_id
        $domain = $request->getSchemeAndHttpHost();
        $url = $domain.'/api/approve/topic?topic_id='.$model_id.'&request_id='.$request_id;

        try {
            $response_2 = Http::post('http://gw.vkist.gov.vn/ngw/approval/sso/write_form', [
                'token' => $response->json()['data']['token'],
                // 'token' => 'test',
                'callback' => $url,
                'type' => 'inner',
                'formname' => 'basic form',
                'subject' => 'Phê duyệt đề tài',
                'content' => 'Phê duyệt đề tài nghiên cứu dự án năng lượng mặt trời',
                'file' => $request->file,
            ]);
        } catch (\Exception $e) {
            Log::error('(LoginGwController/handleRequest) Caught exception (write_form): '.$e->getMessage().', '.$admin_id);
        }

        if($response->json()['success']){
            $approve = new GwApproveResponse([
                'request_id' => $request_id,
                'admin_id' => Auth::user()->id,
                'model_name' => 'App\Models\Topic' ,
                'model_id' => 2,
            ]);
            $approve->save();
            Log::info('Approval request has been sent: '.$admin_id);
            return [
                'success' => true,
                'message' => 'Gửi đề tài thành công',
            ];
        }
        else {
            Log::error('Handle response approval error: '.$admin_id);
            return [
                'success' => false,
                'message' => 'Gửi đề tài thất bại, sai token.',
            ];
        }
    }

    public function handleResponse(Request $request){
        // Log data response GW
        Log::info('Response callback from GW');
        Log::info($request->all());

        $data = $request->all();
        if (!isset($data['status'])) {
            return ['success' => false,];
        }

        $model = $approval = $statusModel = null;
        $arrTypeApproval = [EApiApproval::TYPE_TOPIC, EApiApproval::TYPE_COST, EApiApproval::TYPE_RESEARCH_PLAN];

        if ($data['status'] == EApiApproval::STATUS_REJECT) {
            $status = EApproval::STATUS_REJECT;
            if (in_array($data['type'], $arrTypeApproval)) {
                $statusModel = ETopicStatus::STATUS_REJECT;
            }
            if ($data['type'] == EApiApproval::TYPE_IDEAL) {
                $statusModel = EIdeaStatus::REJECTED;
            }
            if ($data['type'] == EApiApproval::TYPE_WORK_PLAN) {
                $statusModel = EWorkPlan::STATUS_REJECT;
            }
            if ($data['type'] == EApiApproval::TYPE_BUDGET) {
                $statusModel = EBudgetStatus::DENY;
            }
            if ($data['type'] == EApiApproval::TYPE_REQUEST_LEAVE) {
                $statusModel = ERequestLeave::STATUS_REJECT;
            }
        } else if ($data['status'] == EApiApproval::STATUS_COMPLETE) {
            $status = EApproval::STATUS_APPROVAL;
            if (in_array($data['type'], $arrTypeApproval)) {
                $statusModel = ETopicStatus::STATUS_APPROVAL;
            }
            if ($data['type'] == EApiApproval::TYPE_IDEAL) {
                $statusModel = EIdeaStatus::APPROVED;
            }
            if ($data['type'] == EApiApproval::TYPE_WORK_PLAN) {
                $statusModel = EWorkPlan::STATUS_APPROVAL;
            }
            if ($data['type'] == EApiApproval::TYPE_BUDGET) {
                $statusModel = EBudgetStatus::DONE;
            }
            if ($data['type'] == EApiApproval::TYPE_REQUEST_LEAVE) {
                $statusModel = ERequestLeave::STATUS_APPROVAL;
            }
        } else {
            $status = EApproval::STATUS_WAIT_APPROVAL;
            if (in_array($data['type'], $arrTypeApproval)) {
                $statusModel = ETopicStatus::STATUS_WAIT_APPROVAL;
            }
            if ($data['type'] == EApiApproval::TYPE_IDEAL) {
                $statusModel = EIdeaStatus::WAIT_APPROVE;
            }
            if ($data['type'] == EApiApproval::TYPE_WORK_PLAN) {
                $statusModel = EWorkPlan::STATUS_WAIT_APPROVAL;
            }
            if ($data['type'] == EApiApproval::TYPE_BUDGET) {
                $statusModel = EBudgetStatus::WAIT;
            }
            if ($data['type'] == EApiApproval::TYPE_REQUEST_LEAVE) {
                $statusModel = ERequestLeave::STATUS_WAIT_APPROVAL;
            }
        }

        Log::info('status');
        Log::info($status);
        Log::info('statusModel');
        Log::info($statusModel);

        Log::info('request_id');
        Log::info($data['request_id']);

        $gwapproval = GwApproveResponse::where('request_id', $data['request_id'])->first();

        Log::info('gwapproval');
        Log::info($gwapproval);

        if (!empty($data['approval_id'])) {
            $approval = Approval::findOrFail($data['approval_id']);
        }

        Log::info('approval_id');
        Log::info($data['approval_id']);

        Log::info('approval');
        Log::info($approval);

        Log::info('model_id');
        Log::info($data['model_id']);

        Log::info('model');
        Log::info($model);

        $listFile = null;
        if (isset($data['attach_files']) && !empty($data['attach_files'])) {
            foreach ($data['attach_files'] as $file) {
                $listFile[] = [
                    'name' => $file['orgname'],
                    'docu_no' => $data['docId'],
                    'file_no' => $file['file_no'],
                ];
            }
        }

        // Update data
        if ($gwapproval){
            // Update approval
            if (!empty($data['approval_id'])) {
                $approval->update([
                    'status' => $status
                ]);
            }

            // Update model
            if ($data['type'] == EApiApproval::TYPE_IDEAL) {
                $dataApprovalIdeal = ApprovalIdealDetail::where('approval_id', $data['approval_id'])->get();

                if ($dataApprovalIdeal->isNotEmpty()) {
                    foreach ($dataApprovalIdeal as $detail) {
                        // Update table approval_ideal_detail
                        $approvalIdeal = ApprovalIdealDetail::where('approval_id', $data['approval_id'])
                            ->where('ideal_id', $detail->ideal_id)->first();
                        $approvalIdeal->update([
                            'status' => $statusModel
                        ]);

                        // Update table ideal
                        $ideal = Ideal::findOrFail($detail->ideal_id);
                        $ideal->update([
                            'status' => $statusModel
                        ]);
                        Log::info('ideal');
                        Log::info($ideal);
                    }
                }

            } else {
                $dataUpdate = [
                    'status' => $statusModel,
                ];
                if ($status == EApproval::STATUS_APPROVAL) {
                    $dataUpdate['gw_attach_file'] = !empty($listFile) ? json_encode($listFile) : null;
                }
                $model = $gwapproval->model_name::where('id', $data['model_id'])->first();
                Log::info('Data model');
                Log::info($gwapproval->model_name);
                Log::info($model);

                if ($data['type'] == EApiApproval::TYPE_BUDGET) {
                    Budget::updateApproveBudget($data['model_id'], $statusModel);

                    $model->update([
                        'gw_attach_file'=> !empty($listFile) ? json_encode($listFile) : null,
                    ]);
                } else {
                    Log::info('Data update model');
                    Log::info($dataUpdate);
                    $model->update($dataUpdate);
                }

                if ($data['type'] == EApiApproval::TYPE_REQUEST_LEAVE && $statusModel == ERequestLeave::STATUS_APPROVAL) {
                    handleRequestLeave($data['model_id']);
                }

                if ($data['type'] == EApiApproval::TYPE_COST && $statusModel == ETopicFee::STATUS_APPROVAL) {
                    $dataTopicFeeDetail = TopicFeeDetail::where('topic_fee_id', $data['model_id'])->get();
                    if ($dataTopicFeeDetail->isNotEmpty()) {
                        foreach ($dataTopicFeeDetail as $detail) {
                            $topicFeeDetail = TopicFeeDetail::findOrFail($detail->id);
                            $topicFeeDetail->update([
                                'approval_cost' => $topicFeeDetail->total_capital
                            ]);
                        }
                    }
                }
            }

            // Update GwApproveResponse
            Log::info('Update GwApproveResponse');
            Log::info($data['status']);
            $gwapproval->update([
                'gw_document_id' => $data['docId'],
                'gw_date_approve_time' => $data['date'],
                'gw_status' => $data['status'],
                'gw_userid' => $data['userid'],
                'gw_empno' => $data['empno'],
                'gw_comment' => $data['comment'],
                'gw_next_users' => $data['next_users'],
                'gw_title' => $data['title'],
            ]);

            Log::info('(LoginGwController/handleResponse) Handle response approval success: '.$gwapproval->admin_id);
            return [
                'success' => true,
            ];

        } else {
            Log::error('(LoginGwController/handleResponse) Handle response approval error: '.$gwapproval->admin_id);
            return [
                'success' => false,
            ];
        }
    }
}
