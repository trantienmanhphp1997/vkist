<?php

namespace App\Http\Controllers\Admin\Research\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class CategoryFieldController extends Controller
{
    //
    public function index(Request $request){
        return view('admin.research.category.category-field.index');
    }
}
