<?php

namespace App\Exports;

use App\Enums\ETopicSource;
use App\Enums\ETopicStatus;
use App\Models\Topic;
use App\Service\Community;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class TopicExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $order;
    protected $searchTerm;
    protected $searchStatus;
    protected $searchField;
    protected $searchStartDate;
    protected $searchEndDate;

    public function __construct($searchTerm, $searchStatus, $searchField, $searchStartDate, $searchEndDate)
    {
        $this->order = 1;
        $this->searchTerm = $searchTerm;
        $this->searchStatus = $searchStatus;
        $this->searchField = $searchField;
        $this->searchStartDate = $searchStartDate;
        $this->searchEndDate = $searchEndDate;
    }

    public function collection()
    {
        $query = Topic::query()->with(['researchCategory', 'level', 'major', 'idea', 'leader']);

        $this->searchTerm = trim($this->searchTerm);
        if (strlen($this->searchTerm) > 0) {
            $query->where(function ($query) {
                $query->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')
                    ->orWhereRaw("LOWER(code) LIKE LOWER('%{$this->searchTerm}%')");
            });
        }

        if (strlen($this->searchStatus) > 0) {
            $query->where('status', '=', $this->searchStatus);
        }

        if (!empty($this->searchField)) {
            $query->where('research_category_id', $this->searchField);
        }

        //check permission topic
        if (!empty(Community::listTopicIdAllowed())) {
            $query->whereIn('id', Community::listTopicIdAllowed());
        }

        if (!empty($this->searchStartDate)) {
            $query->where('start_date', '>=', $this->searchStartDate);
        }

        if (!empty($this->searchEndDate)) {
            $query->where('end_date', '<=', $this->searchEndDate);
        }

        return $query->orderBy('id', 'desc')->get();
    }
    public function headings(): array
    {
        return [
            __('data_field_name.common_field.stt'),
            __('data_field_name.topic.code'),
            __('data_field_name.topic.name'),
            __('data_field_name.topic.level'),
            __('data_field_name.topic.source'),
            __('data_field_name.topic.field'),
            __('data_field_name.topic.idea'),
            __('data_field_name.topic.scientific'),
            __('data_field_name.topic.topic_chairman'),
            __('data_field_name.topic.start_time'),
            __('data_field_name.topic.end_time'),
            __('data_field_name.topic.expected_fee'),
            __('data_field_name.topic.status'),
        ];
    }
    public function map($topic): array
    {
        return [
            $this->order++,
            $topic->code,
            $topic->name,
            $topic->level->v_value ?? '',
            ETopicSource::valueToName($topic->source),
            $topic->researchCategory->name ?? '',
            $topic->idea->name ?? '',
            $topic->major->v_value ?? '',
            $topic->leader->fullname ?? '',
            reFormatDate($topic->start_date),
            reFormatDate($topic->end_date),
            $topic->expected_fee,
            ETopicStatus::valueToName($topic->status)
        ];
    }
    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => [
                    'name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5'
                    ]
                ]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([]);
            $arrayAlphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:M1');
            $event->sheet->setCellValue('A1', __('data_field_name.topic.topic_list'));
            $event->sheet->getStyle('A1:M1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A2:M2';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);
        },];
    }
    public function title(): string
    {
        return __('data_field_name.topic.topic_list');
    }

    public function startCell(): string
    {
        return 'A2';
    }
}
