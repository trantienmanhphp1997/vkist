<div class="col-md-2 col-xl-3 sidebar-left">
    <ul class="nav flex-column memu-main-left">
        <li class="nav-item @if(in_array(Route::currentRouteName(), ['admin.system.account.index','admin.system.role.index','admin.system.group-user.index','admin.system.group-user.edit','admin.system.group-user.create'])) active @endif">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="{{__('user/group_user.title.main-title')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list"
                           height="24"></span> {{__('user/group_user.title.main-title')}}
            </a>
            <span class="angle-right">
				<img src="/images/angle-right.svg" alt="angle-right">
			</span>
            <ul class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.system.account.index','admin.system.role.index','admin.system.group-user.index','admin.system.group-user.edit','admin.system.group-user.create'])) active @endif">
                @if(checkPermission('admin.system.account.index'))
                    <li>
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.system.account.index') active @endif"
                           href="{{route('admin.system.account.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.users')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.users')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.system.role.index'))
                    <li>
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.system.role.index') active @endif"
                           href="{{route('admin.system.role.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.roles')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.roles')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.system.group-user.index'))
                    <li>
                        <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.system.group-user.index','admin.system.group-user.edit','admin.system.group-user.create'])) active @endif"
                           href="{{route('admin.system.group-user.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.user_group')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.user_group')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
        <!-- Quản lý thông báo -->
        <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.notification')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list"
                           height="24"></span> {{__('menu_management.menu_name.notification')}}
            </a>
            <span class="angle-right">
				<img src="/images/angle-right.svg" alt="angle-right">
			</span>
            <ul class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.general.work-plan.index','admin.general.businessfee.index'])) active @endif">

                <li>
                    <a class="nav-link" href="{{ route('admin.config.notification.public') }}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.notification_configuration')}}">
                        <span class="circle2"></span> {{__('menu_management.menu_name.notification_configuration')}}
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('admin.config.notification.private') }}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.private_notification_configuration')}}">
                        <span
                            class="circle2"></span> {{__('menu_management.menu_name.private_notification_configuration')}}
                    </a>
                </li>
            </ul>
        </li>
        <!-- Cấu hình nghỉ phép -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.system.leave-config.index') }}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.leave_configuration')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list"
                           height="24"></span> {{__('menu_management.menu_name.leave_configuration')}}
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.system.general-config.index') }}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.general_configuration')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list"
                           height="24"></span> {{__('menu_management.menu_name.general_configuration')}}
            </a>
        </li>
        @if(Auth::user()->hasRole('administrator'))
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.system.audit.list') }}" data-toggle="tooltip" data-placement="right" title="{{__('data_field_name.system.audit.title')}}">
                    <span><img src="/images/clipboard-list.svg" alt="clipboard-list"
                               height="24"></span> {{__('data_field_name.system.audit.title')}}
                </a>
            </li>
        @endif
    </ul>
</div>

