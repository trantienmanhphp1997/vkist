<?php


namespace App\Enums;


class EInventoryAssetStatus
{
    const ACCOMPLISHED =2;
    const UNFULFILLED = 1;
}
