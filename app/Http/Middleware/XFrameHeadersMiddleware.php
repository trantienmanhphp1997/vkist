<?php

namespace App\Http\Middleware;

use Closure;

class XFrameHeadersMiddleware
{
    private $unwantedHeaderList = [
        'X-Powered-By',
        'Server',
        'Access-Control-Allow-Origin',
    ];

    public function handle($request, Closure $next)
    {
        $this->removeUnwantedHeaders($this->unwantedHeaderList);

        $url = "http://";
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
            $url = "https://";
        }
        $url .= $_SERVER['HTTP_HOST'];

        $response = $next($request);
        $response->headers->set('X-Frame-Options', 'DENY');
        $response->headers->set('Access-Control-Allow-Origin', $url);
        $response->headers->set('X-Content-Type-Options', 'nosniff');

        return $response;
    }

    private function removeUnwantedHeaders($headerList)
    {
        foreach ($headerList as $header) {
            header_remove($header);
        }
    }
}
