<?php

namespace App\Component;

class DepartmentRecursive extends Recursive
{
    public function itemPattern($item, $level) {
        return [
            'id' => $item['id'],
            'name' => $item['name'],
            'padLeft' => str_repeat('&nbsp;&nbsp;|----', $level)
        ];
    }
}
