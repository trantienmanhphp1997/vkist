<?php

namespace App\Models;

use App\Enums\ERequestLeave;
use App\Notifications\Handlers\NotificationHandler;
use App\Notifications\WorkResultNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestLeave extends Model
{
    use HasFactory;
    protected $table = 'request_leave';
    protected $fillable = [
        'content',
        'start_date',
        'end_date',
        'approved_by',
        'type',
        'admin_id',
        'user_info_id',
        'status',
        'gw_attach_file',
        'day_leave',
    ];

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        static::updated(function(RequestLeave $requestleave) {
            $action = '';
            if($requestleave->status == ERequestLeave::STATUS_REJECT) {
                $action = 'reject';
            } elseif($requestleave->status == ERequestLeave::STATUS_APPROVAL) {
                $action = 'approve';
            }

            if(empty($action)) {
                return;
            }
            NotificationHandler::sendNotification($requestleave->admin, new WorkResultNotification($requestleave, $action));
        });
    }
}
