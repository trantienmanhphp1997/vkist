<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ideal extends BaseModel
{
    use HasFactory;
    protected $table = "ideal";
    protected $guarded=['*'];
    protected $fillable = [
        'code',
        'name',
        'proposer',
        'proposer',
        'agencies',
        'necessity',
        'target',
        'content',
        'result',
        'execution_time',
        'fee',
        'status',
        'admin_id',
    ];
    private static $searchable = [
        'name',
        'code'
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }

    public function researchField() {
        return $this->belongsTo(ResearchCategory::class, 'research_field_id');
    }

    public function admin(){
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }
}
