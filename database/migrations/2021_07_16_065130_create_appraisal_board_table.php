
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalBoardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_board', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Danh sach hoi dong');
            $table->string('name', 500)->nullable()->comment('Ten hoi dong');
            $table->tinyInteger('type')->nullable()->comment('Loai ho so: 1 Y tuong, 2 De tai, 3 Du toan chi phi, 4 Ke hoach nghien cuu');
            $table->dateTime('appraisal_date')->nullable()->comment('Han tham dinh');
            $table->tinyInteger('status')->comment('0 Chua hoan thanh, 1 Hoan thanh');
            $table->unsignedBigInteger('topic_id')->nullable()->comment('Map voi topic');
            $table->foreign('topic_id')->references('id')->on('topic')->comment('Map voi topic');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->string('note',1000)->nullable()->comment('Mo ta');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_board');
    }
}
