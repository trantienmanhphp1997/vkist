<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemConfig extends Model
{
    use HasFactory;

    protected $table = 'system_config';

    protected $fillable = [
        'content',
        'model_name',
        'admin_id',
        'type',
        'name'
    ];

    protected $casts = [
        'content' => 'array'
    ];

    public $timestamps = false;
}
