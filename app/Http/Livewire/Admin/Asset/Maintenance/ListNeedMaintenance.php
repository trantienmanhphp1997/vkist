<?php

namespace App\Http\Livewire\Admin\Asset\Maintenance;

use App\Enums\EAssetSituation;
use App\Enums\EAssetStatus;
use App\Exports\AssetNeedMaintenanceExport;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use App\Models\AssetAllocatedRevoke;
use App\Models\AssetMaintenance;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class ListNeedMaintenance extends BaseLive
{
    public $searchTerm;
    public $idConfirm;
    public $assetSituation;
    public $assetStatus;
    public $deleteId;
    public $errorQuantity;

    public function mount()
    {
        $this->assetSituation = [
            EAssetSituation::SITUATION_NOT_USE => __('data_field_name.asset.no_use'),
            EAssetSituation::SITUATION_USING => __('data_field_name.asset.using'),
        ];
        $this->assetStatus = EAssetStatus::getList();
    }

    public function render()
    {
        $list = AssetMaintenance::where('status', \App\Enums\EAssetStatus::REPORT_MAINTENANCE)
            ->orWhere('status', \App\Enums\EAssetStatus::REPORT_REPAIR)
            ->with('asset');

        $detailNotice = [];
        if ($this->idConfirm) {
            $this->emit('loadAssetInfo', $this->idConfirm);
            $detailNotice = AssetMaintenance::findOrFail($this->idConfirm);
        }
        if ($this->searchTerm) {
            $list->whereHas('asset', function (Builder $q) {
                $q->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%');
            });
        }
        $data = $list->orderBy('asset_maintenance.id', 'DESC')->paginate($this->perPage);

        return view('livewire.admin.asset.maintenance.list-need-maintenance', ['data' => $data,
            'detailNotice' => $detailNotice]);
    }

    public function confirmNotice($idConfirm)
    {
        $this->idConfirm = $idConfirm;
    }

    public function confirm()
    {
        if ($this->idConfirm) {
            $detailNotice = AssetMaintenance::findOrFail($this->idConfirm);
            $asset = Asset::find($detailNotice->asset_id);

            $quantity = $asset->amount_used;

            if (!empty($quantity) && $quantity < $detailNotice->implementation_quantity) {
                $this->errorQuantity = __('validation.lt.numeric', [
                    'attribute' => __('data_field_name.asset.asset_quantity'),
                    'value' => __('data_field_name.asset.asset_used_quantity')
                ]);
            }
            if ($quantity >= $detailNotice->implementation_quantity) {
                $data = AssetMaintenance::where('id', $this->idConfirm)->get();
                foreach ($data as $val) {
                    if ($val->status == \App\Enums\EAssetStatus::REPORT_REPAIR) {
                        AssetMaintenance::where('id', $this->idConfirm)->update([
                            'status' => \App\Enums\EAssetStatus::REPAIRED,
                        ]);
                        $assetDetail = Asset::findOrFail($val->asset_id);
                        $type_manage = $assetDetail->category->type_manage;
                        if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                            $assetDetail->update([
                                'situation' => \App\Enums\EAssetStatus::REPAIRED,
                            ]);
                        } else {
                            if ($assetDetail->quantity == $val->implementation_quantity) {
                                $assetDetail->update([
                                    'situation' => \App\Enums\EAssetStatus::REPAIRED,
                                ]);
                            }
                        }
                    } elseif ($val->status == \App\Enums\EAssetStatus::REPORT_MAINTENANCE) {
                        AssetMaintenance::where('id', $this->idConfirm)->update([
                            'status' => \App\Enums\EAssetStatus::MAINTENANCE,
                        ]);
                        $assetDetail = Asset::findOrFail($val->asset_id);
                        $type_manage = $assetDetail->category->type_manage;
                        if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                            $assetDetail->update([
                                'situation' => \App\Enums\EAssetStatus::MAINTENANCE,
                            ]);
                        } else {
                            if ($assetDetail->quantity == $val->implementation_quantity) {
                                $assetDetail->update([
                                    'situation' => \App\Enums\EAssetStatus::MAINTENANCE,
                                ]);
                            }
                        }
                    }

                }
                $this->emit('closeRule');
                $this->emit('loadConfirm');
            }
        }


        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
            __('notification.common.success.confirmed')]);
    }

    public function export()
    {
        $today = date("d_m_Y");
        return \Maatwebsite\Excel\Facades\Excel::download(new AssetNeedMaintenanceExport($this->searchTerm), 'list-asset-need-maintenance-' . $today . '.xlsx');
    }

    public function deleteIdAsset($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        if ($this->deleteId) {
            $data_lost = AssetMaintenance::findOrFail($this->deleteId);
            $asset = Asset::findOrFail($data_lost->asset_id);
            $type_manage = $asset->category->type_manage;
            if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                $data_lost->delete();
                $asset->update([
                    'situation' => \App\Enums\EAssetSituation::SITUATION_USING
                ]);
            } else {
                if ($asset->quantity == $data_lost->implementation_quantity) {
                    $data_lost->delete();
                    $asset->update([
                        'situation' => \App\Enums\EAssetSituation::SITUATION_USING
                    ]);
                } else {
                    $data_lost->delete();
                }
            }
        }
        $this->emit('closeRule');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
            __('notification.common.success.delete')]);
    }
}
