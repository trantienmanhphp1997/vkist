<div class="tab-pane active" id="profile" wire:ignore.self>
    <h3 class="title-asset">{{__('data_field_name.asset.info_general')}}</h3>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>{{__('data_field_name.asset.department')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->department->name ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.manager_id')}}</label>
                <input type="text" class="form-control" placeholder=""
                       value="{{$assetDetail->manager->fullname ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_cate')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->category->name ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_name')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->name ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_code')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->code ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_quantity')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->quantity ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.unit')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->unit->v_value ?? ''}}" disabled>
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>{{__('data_field_name.asset.origin')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->originAsset->origin_name ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.buy_date')}}</label>
                <input type="text" class="form-control" placeholder=""
                       value="{{reFormatDate($assetDetail->buy_date)? date('d/m/Y',strtotime($assetDetail->buy_date)):''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.contract_number')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->contract_number ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.provider')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->provider->name ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.serial')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{$assetDetail->series_number ?? ''}}" disabled>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_value')}}</label>
                <input type="text" class="form-control" placeholder="" value="{{number_format($assetDetail->asset_value) ?? ''}}" disabled>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>{{__('data_field_name.asset.unit_type')}}</label>
                <textarea class="form-control" rows="4" disabled>
                    {{$assetDetail->unit_type ?? ''}}
                </textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>{{__('data_field_name.asset.note')}}</label>
                <textarea class="form-control" rows="4" disabled>
                    {{$assetDetail->note ?? ''}}
                </textarea>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <input type="checkbox" name="achievement_paper_check" id="a-1"
                   @if($assetDetail->fixed_assets == 1) checked @endif disabled
            >
            <label for="a-1">{{__('data_field_name.asset.fix_asset')}}</label>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>{{__('data_field_name.asset.group_asset')}}</label>
                <div class="form-group checkradio">
                    @foreach($assetGroup as $key => $val)
                        <input type="radio" value="{{$key}}" @if($assetDetail->group_asset==$key) checked @endif disabled>
                        <label for="">{{$val}}</label>&ensp;
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-12 pd-col">
            <div id="file_right">
                <label>{{__('data_field_name.asset.file')}}</label>
                @livewire('component.files',[ 'model_name'=>$model_name, 'type'=>$type,'folder'=>$folder,'status'=>$status,'model_id'=>$assetDetail->id])
            </div>
        </div>
    </div>
</div>
