<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class RealityExpense extends Model
{
    use HasFactory;
    protected $table = 'topic_actual_fee';
    protected $fillable = [
    	'source_capital','approval_costs','payment','type_cost','total_cost','money_remain','user_request_id','note',
        'money_cost','disbursement_date','remain_day','status','task_id','topic_fee_detail_id','topic_id','admin_id',
        'unsign_text','expense_task'
    ];
    public function topic(){
        return $this->hasOne(Topic::class,'id','topic_id');
    }
    
    public function topicFeeDetail() {
        return $this->belongsTo(TopicFeeDetail::class, 'type_expense');
    }
}