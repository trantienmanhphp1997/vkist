<?php

namespace App\Models;

use App\Enums\EAppraisalBoardHasUserInfo;
use App\Enums\EApproval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppraisalBoard extends BaseModel
{
    protected $table = 'appraisal_board';

    protected $fillable = [
        'name', 'type', 'appraisal_date', 'status', 'topic_id', 'note', 'admin_id', 'topic_fee_id', 'research_plan_id'
    ];

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function topicFee()
    {
        return $this->belongsTo(TopicFee::class, 'topic_fee_id');
    }

    public function researchPlan()
    {
        return $this->belongsTo(ResearchPlan::class, 'research_plan_id');
    }

    public function getValidationModel()
    {
        if ($this->type == EApproval::TYPE_TOPIC) {
            return $this->topic;
        } elseif ($this->type == EApproval::TYPE_COST) {
            return $this->topicFee;
        } elseif ($this->type == EApproval::TYPE_PLAN) {
            return $this->researchPlan;
        }
    }

    public function approval()
    {
        return $this->hasOne(Approval::class, 'appraisal_board_id');
    }

    public function members()
    {
        return $this->hasMany(AppraisalBoardHasUserInfo::class, 'appraisal_board_id');
    }

    public function chairman()
    {
        return $this->hasOne(AppraisalBoardHasUserInfo::class, 'appraisal_board_id')->where('role_id', EAppraisalBoardHasUserInfo::ROLE_CHAIRMAN);
    }
}
