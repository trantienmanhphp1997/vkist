<?php

namespace App\Http\Livewire\Admin\Profile;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Enums\ESystemConfigType;
use App\Models\SystemConfig;


class ChangePassword extends Component
{
    public $current_password;
    public $password;
    public $password_confirmation;

    public function render()
    {
        return view('livewire.admin.profile.change-password');
    }

    public function updatePassword()
    {
        $config = SystemConfig::query()->where('name','Import salary config')->where('type', ESystemConfigType::TYPE_IMPORT_SALARY)->first();
        $min_length_password = $config->content['min_length_of_password']['value'] ?? 8;
        $password_special_requirement = $config->content['password_special_requirement']['value'] ?? false;
        $user = auth()->user();
        $rules = [
            'current_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!Hash::check($value, $user->password)) {
                    return $fail(__('client_validation.form.profile.password.old'));
                }
            }],
            'password' => $password_special_requirement ? ['required', 'min:'.$min_length_password, 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{3,}$/'] : ['required', 'min:'. $min_length_password],
            'password_confirmation' => ['required', 'same:password']
        ];
        $this->validate($rules,[
            'password.regex' => __('client_validation.form.profile.password.regex'),
            'password_confirmation.same' => __('client_validation.form.profile.password.same'),
        ], [
            'current_password' => __('data_field_name.user.current_password'),
            'password' => __('data_field_name.user.new_password'),
            'password_confirmation' => __('data_field_name.user.confirm_password'),
        ]);

        if ($this->password == $this->current_password) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('client_validation.form.profile.password.not_same')]);
            return false;
        }

        $user->password = Hash::make($this->password);
        $user->save();

        session()->flash('success', __("notification.profile.success_change_pass"));
        return redirect()->route('admin.profile.index');
    }

}
