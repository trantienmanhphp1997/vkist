<?php


namespace App\Imports;


use App\Models\Asset;
use App\Models\AssetCategory;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FirstSheetImport implements ToModel,WithHeadingRow
{

    /**
     * FirstSheetImport constructor.
     */
    public function __construct()
    {
    }
    public function model(array $row)
    {
        $category=AssetCategory::where('code',$row['ma_loai_tai_san'])->first();
        if(!$category){
            $categoryNew=AssetCategory::create([
                'code'=>$row['ma_loai_tai_san'],
                'name'=>$row['ten_loai_tai_san']
            ]);
            $id_cate=$categoryNew->id;
        }else{
            $id_cate=$category->id;
        }
        return new Asset([
            'code' => $row['ma_tai_san'],
            'name' => $row['ten_tai_san'],
            'category_id'=>$id_cate,
            'series_number'=>$row[''],
        ]);
    }
    public function headingRow(): int
    {
        return 2;
    }
}
