<div class="col-lg-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs"><a href="{{ route('admin.research.topic.index') }}">{{ __('data_field_name.topic.topic_control') }}</a> \ <span>{{ __('data_field_name.topic.topic_list') }}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-lg-12">
            <h3 class="title">{{ __('data_field_name.topic.topic_list') }}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row mb-3">
                        <div class="search-expertise col-md-3">
                            <input wire:model.debounce.1000ms="searchTerm" type="text" class="form-control size13" style="width: 100%;" placeholder="{{ __('data_field_name.topic.search') }}">
                            <span class="ml-3">
                                <img src="/images/Search.svg" alt="search"/>
                            </span>
                        </div>

                        <div class="col d-flex justify-content-end align-items-center">
                            @if ($checkCreatePermission)
                                <a href="{{ route('admin.research.topic.create') }}" class="btn btn-viewmore-news w-xs-100" style="margin: 0px;">
                                    <img src="/images/plus2.svg" alt="plus">{{ __('data_field_name.topic.create') }}
                                </a>
                            @endif

                            @if ($checkDownloadPermission)
                                <button type="button" wire:loading.attr="disabled" class="ml-2" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#export-modal">
                                    <img src="/images/filterdown.svg" alt="filterdown">
                                </button>
                            @endif
                        </div>
                    </div>

                    <div class="row mb-3 bg-light pt-4" style="border-radius: 12px;">
                        <div class="col-sm-3">
                            {{ Form::select('search_field', ['' => __('data_field_name.topic.field')] + $topicFields, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchField']) }}
                        </div>
                        <div class="col-sm-3">
                            {{ Form::select('search_status', ['' => __('data_field_name.topic.status')] + $topicStatus, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchStatus']) }}
                        </div>
                        <div class="col-sm-3">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'from_date', 'place_holder' => __('data_field_name.common_field.start_date'), 'set_null_when_enter_invalid_date' => true])
                        </div>
                        <div class="col-sm-3">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'to_date', 'place_holder' => __('data_field_name.common_field.end_date'), 'set_null_when_enter_invalid_date' => true])
                        </div>
                    </div>

                    <div wire:loading class="loader" style="z-index: 10"></div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                                <tr class="border-radius text-center">
                                    <th scope="col" class="border-radius-left">
                                        {{ __('data_field_name.topic.stt') }}
                                    </th>
                                    <th scope="col">
                                        {{ __('data_field_name.topic.code') }}
                                    </th>
                                    <th scope="col" class="text-center w-25">
                                        {{ __('data_field_name.topic.name') }}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{ __('data_field_name.topic.field') }}
                                    </th>
                                    <th scope="col">
                                        {{ __('data_field_name.topic.level') }}
                                    </th>
                                    <th scope="col">
                                        {{ __('data_field_name.topic.status') }}
                                    </th>
                                    <th scope="col">
                                        {{ __('data_field_name.topic.topic_group') }}
                                    </th>
                                    <th scope="col" class="border-radius-right">
                                        {{ __('data_field_name.common_field.action') }}
                                    </th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>

                                @foreach ($data as $row)
                                    <tr>
                                        <td class="text-center">{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.research.topic.detail', $row->id) }}">
                                                {!! boldTextSearch($row->code, $searchTerm) !!}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            {!! boldTextSearch($row->name, $searchTerm) !!}

                                        </td>
                                        <td class="text-center">{{ $row->researchCategory->name ?? '' }}</td>
                                        <td class="text-center">{{ $row->level->v_value ?? '' }}</td>
                                        <td class="text-center">{{ $topicStatus[$row->status] ?? '' }}</td>
                                        <td class="text-center">{{ $topicTypes[$row->type] ?? '' }}</td>
                                        <td class="text-center">
                                            @if (in_array($row->status, $topicEditableConditions))
                                                @php $id = $row->id; @endphp
                                                @include('layouts.partials.button._edit')
                                            @endif
                                            @if ($row->status == \App\Enums\ETopicStatus::STATUS_NOT_SUBMIT)
                                                @include('livewire.common.buttons._delete')
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif
                    @include('livewire.common._modalDelete')
                    @include('livewire.common.modal._modalConfirmExport')
                </div>
            </div>
        </div>
    </div>
</div>
