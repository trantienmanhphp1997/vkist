<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="{{route('admin.asset.allocated-revoke.index')}}">{{__('menu_management.menu_name.asset-management')}}</a> \ <span>{{__('menu_management.menu_name.asset-allocated-revoke')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title">{{__('data_field_name.allocated-revoke.allocated_to_employees')}}</h3>
                </div>
            </div>
            <div class="information information-asset">
              <div class="inner-tab pt0">
                <div class="header-staff2">
                    <div class="row">
                      <div class="col-md-4 mb-3">
                          <label>{{__('data_field_name.allocated-revoke.allocated_date')}}<span class="text-danger">*</span></label>
                           @include('layouts.partials.input._inputDate', ['input_id' => 'allocatedDay', 'place_holder'=>'', 'default_date' => is_null($day) ? date('Y-m-d') : $day ])
                          @error('day')
                            <div class="text-danger mt-1">{{$message}}</div>
                          @enderror
                      </div>
                      <div class="col-md-8 mb-3">
                          <label>{{__('data_field_name.allocated-revoke.reason')}}</label>
                          <textarea wire:model.lazy="reason" placeholder="{{__('data_field_name.allocated-revoke.reason')}}" class="form-control" ></textarea>
                          @error('reason')
                            <div class="text-danger mt-1">{{$message}}</div>
                          @enderror
                      </div>
                    </div>
                </div>
              </div>
                <div class="row">
                    <div class="col-md-4 w-240">
                        @if(!empty($errorUser))
                          <div class="text-danger mt-1">{{$errorUser}}</div>
                        @endif
                        <ul class="menu-asset menu-asset2 position-relative">
                          @if(count($dataList) > 0)
                            @foreach($dataList as $index => $item)
                              <li class="position-relative">
                                <a href="javascript:void(0)" class="@if($index == $indexSelected) active @endif text-capitalize" wire:click="selectUser({{$index}})">{{$item['fullname']}}
                                    <span class="d-block">{{$item['department']}}</span>
                                </a>
                                <span class="delete-staff">
                                  @if($index == $indexSelected)
                                    <img src="/images/close-white.svg" alt="close-white" wire:click="deleteUser({{$index}})" style="cursor: pointer;">
                                  @else
                                    <img src="/images/close.svg" alt="close" wire:click="deleteUser({{$index}})" style="cursor: pointer;">
                                  @endif
                                </span>
                              </li>
                            @endforeach
                          @endif
                        </ul>
                        <div class="group-btn" style="height: 50px">
                            <button class="btn btn-asset" wire:click="openModalUser"><img src="/images/Plus3.svg" alt="plus"> {{__('data_field_name.allocated-revoke.add_employee')}} </button>
                        </div>
                    </div>
                    <div class="col-md-8 border-left">
                        <div class="row inner-tab pt-3">
                          <div class="col-md-12">
                              <div class="row">
                                    <div class="col-md-12 mb-4">
                                        <label>{{__('data_field_name.allocated-revoke.number_report')}}<span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" wire:model.lazy="number_report" placeholder="{{__('data_field_name.allocated-revoke.number_report')}}">
                                        @error('number_report')
                                          <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-12 tab-content">
                                        <table class="table border-bottom">
                                            <thead>
                                                <tr class="border-radius">
                                                    <th scope="col" class="border-radius-left">
                                                        {{__('data_field_name.allocated-revoke.asset_code')}}
                                                    </th>
                                                    <th scope="col">
                                                        {{__('data_field_name.allocated-revoke.asset_name')}}
                                                    </th>
                                                    <th scope="col" class="text-center">
                                                        {{__('data_field_name.allocated-revoke.unused_quantity')}}
                                                    </th>
                                                    <th scope="col" class="text-center">
                                                        {{__('data_field_name.allocated-revoke.number_of_allocations')}}
                                                    </th>
                                                    <th scope="col" class="border-radius-right">
                                                        {{__('data_field_name.allocated-revoke.specifications')}}
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                  @if(count($dataList) > 0)
                                    @foreach($dataList[$indexSelected]['asset'] as $index => $row)
                                        <tr>
                                            <td class="text-center">{{$row['code']}}</td>
                                            <td class="text-center">{{$row['name']}}</td>
                                            <td class="text-center">{{$row['quantity'] - $row['amount_used']}}</td>
                                            <td class="text-center">
                                              <input class="text-right" type="number" name="" wire:change="updateQuantity({{$indexSelected}}, {{$index}}, $event.target.value)" value="{{$row['implementation_quantity']}}" style="height: 25px; border: 1px solid #00000040">
                                              @if(!empty($row['error']))
                                                <div class="text-danger mt-2">{{$row['error']}}</div>
                                              @endif
                                            </td>
                                            <td class="text-center">{{$row['unit_type']}}</td>
                                        </tr>
                                    @endforeach
                                  @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                      @if(!empty($errorAsset))
                                        <div class="text-danger mb-3">{{$errorAsset}}</div>
                                      @endif
                                      <div class="group-btn">
                                        @if(count($user) > 0)
                                          <button class="btn btn-asset" wire:click="openModalAsset" style="position: unset;"><img src="/images/Plus3.svg" alt="plus"> {{__('data_field_name.allocated-revoke.quick_pick')}} </button>
                                        @else
                                          <button class="btn btn-asset" disabled="" wire:click="openModalAsset" style="position: unset;"><img src="/images/Plus3.svg" alt="plus"> {{__('data_field_name.allocated-revoke.quick_pick')}} </button>
                                        @endif
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group upload-file mt-3">
                                          <label>{{__('data_field_name.asset.distribution_file')}} </label>
                                          <div class="form-group info-request mb-0">
                                                <input type="file"
                                                       class="custom-file-input"
                                                       onchange="uploadFile(this, @this)" id="input-file">
                                                <label for="input-file" class="attachfile-field text-center py-2 bd-attached">{{__('data_field_name.asset.distribution_file')}} <span><img src="/images/Clip-blue.svg" alt="clip-blue"></span></label>
                                              </div>
                                              </form>
                                              @if(!empty($errorFile))
                                                  <div class="text-danger mt-1">{{$errorFile}}</div>
                                              @endif
                                              @error('fileUpload')
                                                  <div class="text-danger mt-1">{{$message}}</div>
                                              @enderror
                                              <div class="text-danger upload-error"></div>
                                          </div>
                                      <div class="form-group">
                                          <div class="row">
                                              @if(!empty($fileUpload))
                                                @foreach ($fileUpload as $index => $item)
                                                  <div class="form-group col-md-4">
                                                      <div class="attached-files">
                                                          <img src="/images/File.svg" alt="file">
                                                          <div class="content-file">
                                                              <p class="kb" style="word-break: break-all;">{{ $item->getClientOriginalName() }}</p>
                                                              <p class="kb">{{$item->getSize() / 1024}} KB</p>
                                                          </div>
                                                          <span>
                                                          <button wire:click.prevent="deleteFileUpload({{$index}})" style="border: none;background: white" type="button"><img
                                                                  src="/images/close.svg" alt="close"/>
                                                          </button>
                                                      </span>
                                                      </div>
                                                  </div>
                                                @endforeach
                                              @endif
                                        </div>
                                      </div>
                                  </div>
                                  <div class="col-md-12 mt-3">
                                    <div class="group-btn2 text-right">
                                      <a href="{{route('admin.asset.allocated-revoke.index')}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                                      @if($disable)
                                        <button disabled="" class="btn btn-cancel btn-save bg-primary2" wire:click="saveData(false)">{{__('data_field_name.allocated-revoke.btn_allocated')}}</button>
                                        @if(checkRoutePermission('download'))
                                          <button disabled="" type="button" wire:click="saveData(true)" class="btn btn-save px-4">{{__('data_field_name.allocated-revoke.btn_allocated_and_print')}}</button>
                                        @endif
                                      @else
                                        <button class="btn btn-cancel btn-save bg-primary2" wire:click="saveData(false)">{{__('data_field_name.allocated-revoke.btn_allocated')}}</button>
                                        @if(checkRoutePermission('download'))
                                          <button type="button" wire:click="saveData(true)" class="btn btn-save px-4">{{__('data_field_name.allocated-revoke.btn_allocated_and_print')}}</button>
                                        @endif
                                      @endif
                                    </div>
                                  </div>
                                </div>
                                @include('livewire.admin.asset.allocated-and-revoke.modal-user')
                                @include('livewire.admin.asset.allocated-and-revoke.modal-asset')
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(() => {
        window.livewire.on('showModalUser', () => {
          $('#nhanvien').modal('show');
        });
        window.livewire.on('showModalAsset', () => {
          $('#taisan').modal('show');
        });
        window.livewire.on('doExport', () => {
          window.livewire.emit('export');
        });
        window.livewire.on('doResetData', () => {
          window.livewire.emit('resetData');
        });
    });
    function uploadFile(input, proxy = null) {
        let file = input.files[0];
        if (!file || !proxy) return;
        let $error = $(input).parents('.box-user').find('.upload-error');
        if(file.size/(1024*1024) >= 255) {
            $error.html("{{ __('notification.upload.maximum_size', ['value' => 255]) }}");
            return;
        }

        let mimes = {!! json_encode(['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z']) !!};
        let extension = file.name.split('.').pop();
        if(!mimes.includes(extension)) {
            $error.html("{{ __('notification.upload.mime_type', ['values' => implode(', ', ['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z'])]) }}");
            return;
        }

        proxy.upload('fileUpload', file, (uploadedFilename) => {

        }, () => {
            console.log(error);
        }, (event) => {

        });
    }
</script>
