@extends('layouts.master')
@section('homeLeft')
@include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('title')
    @if($isEdit == true)
        <title>{{__('data_field_name.user.edit_personal_infomation')}}</title>
    @else
        <title>{{__('data_field_name.user.detail_personal_infomation')}}</title>
    @endif
@endsection


@section('content')
<div class="col-md-8 col-xl-11 box-news box-user">
    <div class="col-md-12 px-0">
            <div  class="breadcrumbs"><a href="{{route('admin.user.index')}}">{{ __('menu_management.menu_name.user_info_list') }}</a> \ <span>@if($isEdit == true)
                {{__('data_field_name.user.edit_personal_infomation')}}
            @else
                {{__('data_field_name.user.detail_personal_infomation')}}
            @endif</span></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if($isEdit == true)
            <h3 class="title">{{__('data_field_name.user.edit_personal_infomation')}}</h3>
            @else
            <h3 class="title">{{__('data_field_name.user.detail_personal_infomation')}}</h3>
            @endif
            <div class="information">
                <div class="group-tabs">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><span>{{__('data_field_name.user.general_information')}}</span></a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><span>{{__('data_field_name.user.academic_level')}}</span></a>
                            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><span>{{__('data_field_name.user.working_process')}}</span></a>
                            <a class="nav-item nav-link" id="nav-research-tab" data-toggle="tab" href="#nav-research" role="tab" aria-controls="nav-research" aria-selected="false"><span>{{__('data_field_name.user.research_results')}}</span></a>
                            <a class="nav-item nav-link" id="nav-seniority-tab" data-toggle="tab" href="#nav-seniority" role="tab" aria-controls="nav-seniority" aria-selected="false"><span>{{__('data_field_name.user.working_seniority')}}</span></a>
                        </div>
                    </nav>
                    {!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'autocomplete' => "off", 'enctype' => 'multipart/form-data', 'route' => ['admin.user.update', $data->id]]) !!}
                    @csrf
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                            @include('admin.user._tabInfo')
                            <!-- Modal Edit -->
                            <form wire:submit.prevent="submit">
                                <div wire:ignore.self class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body box-user">
                                                <h4 class="modal-title text-center" id="exampleModalLabel">{{__('data_field_name.user.appoint_dismissed')}}</h4>
                                                <form>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlInput4">{{__('data_field_name.common_field.department')}}<span class="text-danger">(*)</label>
                                                        <select name="department_id"  id="exampleFormControlInput4" class="form-control"  wire:model.lazy="parent_id">
                                                            <option value="">-- --</option>
                                                            @foreach ($data_department as $value)
                                                            <option value="{{$value['id']}}" {{($value['id'] == $data->department_id || $value['id'] == old('department_id')) ? 'selected' : ''}}>
                                                                {!!$value['padLeft'].$value['name']!!}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlInput4">{{__('data_field_name.common_field.position')}}<span class="text-danger">(*)</label>
                                                        {!! Form::select('position_id', $master_data['positions'], null, ['class' => 'form-control']) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        <label>{{__('data_field_name.reality_expense.decision')}}</label>
                                                        @livewire('component.files',['model_name'=>$model_name,'canUpload'=> true,'uploadOnShow'=>1, 'type'=>$type,'folder'=>$folder,'model_id'=>$data->id])
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="group-btn2 text-center  pt-12">
                                                <button type="button" id="close-modal-create-research-plan" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.close')}}</button>
                                                <button type="submit"  class="btn btn-save" wire:loading.attr="disabled">{{__('common.button.save')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- End  Model Edit -->
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            @include('admin.user._tabAcademic')
                        </div>
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            @include('admin.user._tabWorkingProcess')
                        </div>
                        <div class="tab-pane fade" id="nav-research" role="tabpanel" aria-labelledby="nav-research-tab">
                            @include('admin.user._tabResearch')
                        </div>
                        <div class="tab-pane fade" id="nav-seniority" role="tabpanel" aria-labelledby="nav-seniority-tab">
                            @include('admin.user._tabSeniority')
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
