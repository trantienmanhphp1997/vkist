<?php

namespace App\Models;

use App\Notifications\Handlers\NotificationHandler;
use App\Notifications\PersonalWorkNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Yajra\Oci8\Eloquent\OracleEloquent;

class Participant extends OracleEloquent
{

    use HasFactory;

    protected $mapRelations = [
        'staff' => 'staff',
        'work' => 'work'
    ];

    protected static function boot()
    {
        parent::boot();
        self::created(function (Participant $participant) {
            $participant->notifyToUser();
        });

        self::updated(function (Participant $participant) {
            $participant->notifyToUser();
        });
    }

    protected function notifyToUser() {
        $mapping = $this->mapRelations;
        if (method_exists($this, $mapping['staff'] ?? '') && method_exists($this, $mapping['work'] ?? '')) {
            $staff = $this->{$mapping['staff']};
            $work = $this->{$mapping['work']};
            $user = $staff->account;

            if (empty($user) || empty($work)) {
                return;
            }
            NotificationHandler::sendNotification($user, new PersonalWorkNotification($work));
        }
    }
}
