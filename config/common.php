<?php
return [
    "groups" => [
        "admin" => [
            "id" => 1,
            "code" => "admin",
            "display_name" => "Administrator",
        ],
    ],
    "departments" => [
        "production" => "production",
        "music" => "production_entertainment_music",
        "film" => "production_entertainment_film",
    ],

    "action" => [
        "updated" => "updated",
        "added" => "added",
        "deleled" => "deleted",
    ],
    "upload" => [
        "path" => "store/files",
        "diskType" => "local",
    ],
    "permission" => [
        "action" => [
            "show" => "show",
            "edit" => "edit",
            "delete" => "delete",
            "add" => "add",
            "download" => "download",
            "approve" => "approve",
            "wait_approve" => "wait_approve",
        ],
        "prefix" => ".",
    ],
    "media" => [
        "music" => 1,
        "film" => 2,
    ],

    "special_character" => [
        "VN" => "ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ\s\W|_",
        "alphabet" => "ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ\s",
        "alphabet_lowercase" => "àáâãèéêìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểếễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵýỷỹ",
        "alphabet_uppercase" => "ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴÝỶỸ",
        "white_space" => "(?!\S*\s\S*\s)",
    ],
    "regex" => [
        "standard_name" => '/^[ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\w]+$/',
        "standard_note" => '/^[ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\w\,\.]+$/'
    ],
    "type_upload" => [
        "UserResearchPaper" => 3,
        "UserResearchLicense" => 4,
        "UserAcademic_diploma" => 5,
        "UserAcademic_certificate" => 6,
        "TopicResearchOverview" => 7,
        "TopicAttachment" => 8,
        "TopicPresentation" => 9,
        "Budget" => 10,
        "BudgetHasExpensePlan" => 11,
        "Asset" => 12,
        'AppraisalBoard' => 13,
        "LaborContract" => 14,
        "UserInfoLeave" => 15,
        'FormClaim' => 16,
        "AppraisalBoardHasUserInfo" => 17,
        "ImportTimeSheets" => 18,
        "DecisionReward" => 19,
        "Allocated" => 20,
        "EquipmentCost" => 21,
        "Revoke" => 22,
        "Transfer" => 23,
        "AssetAllocatedRevoke" => 24,
        'ResearchAchievement' => 26,
        "TopicActualFee" => 25,
        'AssetMaintenance' => 27,
        "Receipt" => 25,
        "License" => 27,
        "SurveyRequest" => 28,
        "AppointUser"=>29,
        "UserWorkingProcess"=>30,
        "UserInfo"=>31,
        'RequestLeave' => 32,
        "ImportSalarySheets" => 33,
        'WorkPlan'=>34,
        'WorkPlanHasBusinessFee'=>35,
        'BudgetTotalReal'=>36,
        'TemplateForm' => 37,
        'UnitExpert'=>38,
        'TaskWorkHasResult'=>39,
        'MaterialCost' => 40,
        'AssetCost' => 41,
        'OtherCost' => 42,
        'Ideal' => 43,
    ],
    "listNameFunction" => [
        'admin.user.index' => 'Quản lý nhân sự',
        'admin.department.index' => 'Quản lý đơn vị',
        'admin.research.ideal.index' => 'Quản lý ý tưởng nghiên cứu',
        'admin.research.category-topic.index' => 'Quản lý loại đề tài',
        'admin.research.category-field.index' => 'Quản lý lĩnh vực nghiên cứu',
        'admin.general.work-plan.index' => 'Quản lý tổng vụ',
        'admin.system.role.index' => 'Quản lý vai trò',
        'admin.system.account.index' => 'Quản lý người dùng',
        'admin.research.topic.index' => 'Quản lý đề tài',
        'admin.general.businessfee.index' => 'Quản lý công tác phí',
        'admin.budget.index' => 'Quản lý ngân sách',
        'admin.research.plan.research_plan.index' => 'Quản lý kế hoạch nghiên cứu',
        'admin.approval.topic.index' => "Quản lý duyệt đề tài",
        'admin.executive.contract-and-salary.contract.index' => 'Quản lý thông tin hợp đồng lao động',
        'admin.executive.user-info-leave.index' => 'Quản lý nhân viên nghỉ việc',
        'admin.asset.category.index' => 'Quản lý tài sản',
        'admin.asset.allocated-revoke.index' => 'Cấp phát, thu hồi',
        'admin.executive.my-day-off.index' => 'Quản lý nghỉ phép',
        'admin.executive.my-time-sheets.index' => 'Bảng chấm công',
        'admin.executive.time-sheets.index' => 'Dữ liệu chấm công'
    ],
    'profile_type' => [
        1 => 'Ý tưởng',
        2 => 'Đề tài',
        3 => 'Dự toán chi phí',
        4 => 'Kế hoạch nghiên cứu',
    ],
    'status_topic' => [
        0 => 'Từ chối và yêu cầu update',
        1 => 'Hoàn tất hồ sơ online',
        2 => 'Chờ thẩm định',
    ],
    'formatDate' => 'd/m/Y',
    "status" => [
        "wait" => 1,
        "done" => 2,
        "deny" => 3,
        "new" => 4,
    ],
    'year-month-day' => 'Y-m-d',
    'day-month-year' => 'd-m-Y',
    'day/month' => 'd/m',
    'mime_type' => [
        'general' => [
            'jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms',
            'doc', 'docx', 'docb', 'docm', 'pdf', 'zip', 'rar', '7z'
        ],
        'document' => ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'zip', 'rar'],
        'image' => ['jpg', 'jpeg', 'png'],
        'topic_fee' => ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm'],
    ],
    'code' => [
        'topic_fee' => 'DT',
    ],
    'url_gw' => [
        'approval' => 'http://gw.vkist.gov.vn/ngw/approval/sso/write_form',
        'download' => 'https://gw.vkist.gov.vn/ngw/approval/vkist/download_file?',
    ],
    'default_password' => 'vkist2021^'
];
