<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWorkingProcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_working_process', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('qua trinh cong tac');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('position')->nullable()->comment('vi tri');
            $table->string('major')->nullable();
            $table->string('work_unit')->nullable()->comment('don vi cong tac');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('map với user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_working_process');
    }
}
