<div class="modal fade" wire:ignore.self id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" >
                <h4 class="text-center">{{__('data_field_name.asset.add_btn')}}</h4>
                <div class="bd-attached add-property" id="create_asset_import">
                    @livewire('component.files', ['model_name' => $model_name, 'model_id'=>$model_id, 'type'=>$type,
                    'folder'=>$folder,'status'=>$status])
                </div>
            </div>
            <div class="group-btn2 text-center">
                <button type="submit" class="btn btn-cancel">{{__('common.button.cancel')}}</button>
                <button type="submit" class="btn btn-save" wire:click.prevent="uploadAsset()">{{__('common.button.save')}}</button>
            </div>
        </div>
    </div>
</div>
