<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class TopicDetailWork extends BaseModel
{
    use HasFactory;

    protected $table = 'topic_detail_work';
    protected $fillable = [
    	'task_id', 'expected_result', 'number_workday', 'total', 'topic_has_user_info_id',
        'state_capital', 'other_capital', 'topic_fee_detail_id', 'admin_id', 'wage_coefficient','base_salary'
    ];

    public function topicFeeDetail() {
        return $this->belongsTo(TopicFeeDetail::class, 'topic_fee_detail_id');
    }

    public function topicHasUserInfo(){
        return $this->belongsTo(TopicHasUserInfo::class, 'topic_has_user_info_id');
    }

    public function task(){
        return $this->belongsTo(Task::class, 'task_id');
    }

}
