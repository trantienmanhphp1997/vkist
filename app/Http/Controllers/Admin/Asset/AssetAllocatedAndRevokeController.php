<?php

namespace App\Http\Controllers\Admin\Asset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class AssetAllocatedAndRevokeController extends Controller
{
    public function index() {
        return view('admin.asset.allocated-and-revoke.index');
    }

    public function allocated($data) {
    	return view('admin.asset.allocated-and-revoke.allocated', ['data' => $data]);
    }

    public function revoke($data) {
    	return view('admin.asset.allocated-and-revoke.revoke', ['data' => $data]);
    }

    public function transfer() {
    	return view('admin.asset.allocated-and-revoke.transfer');
    }
}
