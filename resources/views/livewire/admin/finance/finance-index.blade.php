<div class="col-md-10 col-xl-11 box-detail box-user bg-light">
    <div class="breadcrumbs">
        <a class="text-primary" href="#">{{ __('menu_management.menu_name.finance_management') }}</a>
    </div>

    <div class="box-content">
        <div class="row">
            <div class="col-md-6">
                <h3 class="title">{{ __('menu_management.menu_name.finance_management') }}</h3>
            </div>
        </div>

        <div class="information">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="misa:" class="btn btn-save">{{__('data_field_name.finance.open_misa')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
