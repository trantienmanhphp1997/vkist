<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => ['required', 'max:48', 'regex:/^([.a-zA-Z'.config('common.special_character.alphabet').'])+$/'],
            'phone' => ['required', 'max:11', 'regex:/^[0-9]+$/'],
            'telephone' => ['nullable', 'max:11', 'regex:/^[0-9]+$/'],
            'ext' => ['nullable', 'max:4', 'regex:/^[0-9]+$/'],
            'username_groupware' => 'max:50',
            'password_groupware' => 'max:50'
        ];
    }

    public function attributes()
    {
        return [
            'fullname' => __('data_field_name.user.full_name'),
            'phone' => __('data_field_name.profile.mobile_number'),
            'telephone' => __('data_field_name.profile.phone'),
            'ext' => __('data_field_name.profile.ext'),
            'username_groupware' => __('data_field_name.profile.username_groupware'),
            'password_groupware' => __('data_field_name.profile.password_groupware'),
        ];
    }
}
