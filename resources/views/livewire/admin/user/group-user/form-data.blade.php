<div class="col-md-8 col-xl-11 box-news box-user">
    <h3 class="title">
        @if ($groupUser->exists)
            {{ __('user/group_user.title.update') }}
        @else
            {{ __('user/group_user.title.create') }}
        @endif
    </h3>
    <div class="information">
        <div class="group-tabs">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link {{ $currentTab == 'group-user-tab' ? 'active' : '' }}" wire:click="setCurrentTab('group-user-tab')" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><span>{{ __('user/group_user.tab1') }}
                        </span></a>
                    <a class="nav-item nav-link {{ $currentTab == 'topic-tab' ? 'active' : '' }}" wire:click="setCurrentTab('topic-tab')" id="nav-topic-tab" data-toggle="tab" href="#nav-topic" role="tab" aria-controls="nav-topic" aria-selected="false"><span>{{ __('user/group_user.tab2') }}</span></a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade {{ $currentTab == 'group-user-tab' ? 'active show' : '' }}" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    @include('admin.user.groupUser.tab._info-tab')
                </div>
                <div class="tab-pane fade {{ $currentTab == 'topic-tab' ? 'active show' : '' }}" id="nav-topic" role="tabpanel" aria-labelledby="nav-topic-tab">
                    @include('admin.user.groupUser.tab._topic-tab')
                </div>
            </div>
        </div>
        <div class="text-center mt-3">
            <span>
                <button class="btn btn-main bg-4 text-3 size16 px-48 py-14" wire:click="back" id="cancel-btn">{{ __('common.button.cancel') }}</button>
                <button class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn" wire:click="save" id="save-submit-btn">{{ __('common.button.save') }}</button>
            </span>
        </div>
    </div>
</div>
