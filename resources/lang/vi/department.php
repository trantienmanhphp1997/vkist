<?php

return [
    "list" => [
        "header" => "Quản lý đơn vị"
    ],
    "production" => "Phòng sản xuất",
    "camera" => "Quay phim",
    "technical_camera"=>"Kĩ thuật phòng quay",
    "device" => "Phòng quản lý thiết bị",
    "transportation" => "Phòng vận tải",
    "accounting" => "Phòng tài chính kế toán",
    "production_general_news" => "Phòng tin tức tổng hợp",
    "production_general_news_news" => "Phòng tin tức",
    "production_general_news_documentary" => "Phòng chuyên đề",
    "production_entertainment" => "Phòng giải trí",
    "production_entertainment_music" => "Phòng âm nhạc",
    "production_entertainment_film" => "Phòng phim ảnh",
    "production_entertainment_game" => "Phòng game",
    "production_entertainment_marketing" => "Phòng marketing",
    "production_e-commerce" => "Phòng trang tin điện tử",
    "production_individual_customer" => "Phòng khách hàng cá nhân",
    "production_sale_marketing" => "Phòng kinh doanh quảng cáo",
    "production_enterprise_customer" => "Phòng khách hàng doanh nghiệp",
    "production_technical_center" => "Phòng trung tâm kỹ thuật",
    "production_editor" => "Phòng thư ký biên tập",
    "production_business_plan" => "Phòng kế hoạch kinh doanh"

];
