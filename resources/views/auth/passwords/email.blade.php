@extends('layouts.app')
<title>{{ __('auth.vi.forgot-password') }}</title>
@section('content')
<div class="col-md-12">
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <a class="logo-login" href="/" >
                    <img src="../images/Logo_VKIST_Transparent_background-08f.png" style="width:180px" alt="logo">
                </a>
                <div class="bg-login">
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="login-box">
                <h2>{{Cookie::get('lang') === 'vi' ? __('auth.vi.reset-password'):__('auth.en.reset-password')}}</h2>
                <p >{{Cookie::get('lang') === 'vi' ? __('auth.vi.title'):__('auth.en.title')}}</p>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form-group">
                        <span><img src="../images/mail-icon.svg" alt="mail-icon"/></span>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="{{Cookie::get('lang') === 'vi' ? __('auth.vi.email'):__('auth.en.email')}}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <p class="invalid-feedback mb-0 text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block">
                            {{Cookie::get('lang') === 'vi' ? __('auth.vi.btn-reset-password'):__('auth.en.btn-reset-password')}}
                        </button>
                        <p class="text-center"> {{Cookie::get('lang') === 'vi' ? __('auth.vi.already-have-account'):__('auth.en.already-have-account')}} <a href="/login">{{Cookie::get('lang') === 'vi' ? __('auth.vi.sign-in'):__('auth.en.sign-in')}}</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
