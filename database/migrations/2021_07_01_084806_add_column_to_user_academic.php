<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToUserAcademic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_academic', function (Blueprint $table) {
            $table->integer('score')->nullable()->comment('diem so');
            $table->integer('rank_id')->nullable()->comment('diem so');
            $table->bigInteger('level_id')->nullable()->comment('diem so');
            $table->string('name', 500)->nullable()->comment('gia tri');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_academic', function (Blueprint $table) {
            $table->dropColumn('score');
            $table->dropColumn('rank_id');
            $table->dropColumn('level_id');
            $table->dropColumn('name');

        });
    }
}
