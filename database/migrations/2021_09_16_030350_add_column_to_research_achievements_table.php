<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToResearchAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_achievements', function (Blueprint $table) {
            //
            $table->string('code', 255)->nullable()->comment('Mã thành quả nghiên cứu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_achievements', function (Blueprint $table) {
            //
            $table->dropColumn('code');
        });
    }
}
