<?php


namespace App\Enums;


class EUnitExpert {
    const ENTERPRISE = 0;
    const GOVERNMENT = 1;
    const MANAGE  = 2;

    public static function valueToName($value) {
       $result = "";
       switch ($value) {
        case self::ENTERPRISE:
            $result = __('data_field_name.unit_expert.enterprise');
            break;
        case self::GOVERNMENT:
            $result = __('data_field_name.unit_expert.government');
            break;
        case self::MANAGE:
            $result = __('data_field_name.unit_expert.manage');
            break;
        default:
            $result = $value;
            break;
    }
    return $result;
    }

}

