<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\BaseModel;

class BudgetHasExpensePlan extends BaseModel
{
    use HasFactory;

    protected $table='budget_has_expense_plan';
    protected $guarded=['*'];
    protected $fillable = [
        'budget_id',
        'month_budget',
        'money_plan',
        'money_real',
        'admin_id',
    ];
}
