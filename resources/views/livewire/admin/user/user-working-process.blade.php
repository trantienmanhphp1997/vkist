<div class="inner-tab">
    <div class="row float-right">
        <div class="col-md-12 mb-3">
            @if(checkRoutePermission('download') && Route::currentRouteName() == 'admin.user.show')
                <button type="button" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#exampleModal"> <img src="/images/filterdown.svg" alt="filterdown"> </button>
            @endif
        </div>
    </div>
    <table class="table border-bottom">
        <thead>
            @forelse ($data as $val)
            <tr class="border-radius">
                <th scope="col" class="border-radius-left text-center">{{__('data_field_name.working_process.start_time')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.working_process.end_time')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.common_field.position')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.common_field.department')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.working_process.work_unit')}}</th>
                <th scope="col" class="text-center">File</th>
                {{-- <th scope="col" class="border-radius-right text-center">{{__('data_field_name.common_field.action')}}</th>--}}
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">{{ $val->start_date? date('d/m/Y',strtotime($val->start_date)):'' }}</td>
                <td class="text-center">{{ $val->end_date? date('d/m/Y',strtotime($val->end_date)):'' }}</td>
                <td class="text-center">{{ $val->position != null ? $val->position->v_value : '' }}</td>
                <td class="text-center">{{ $val->department != null ? $val->department->name : '' }}</td>
                <td class="text-center">VKIST</td>
                <td class="text-center"> <button type="button" class="btn btn-sm" wire:click="getListFile({{$val->id}})">
                        <img src="/images/fileIcon.png" alt="file" width="50px">
                    </button>
                </td>
                {{-- <td>--}}
                {{-- <button type="button" class="btn par6" title="edit"><img src="/images/pent2.svg" alt="edit"></button>--}}
                {{-- <button type="button" class="btn par6" title="delete"><img src="/images/trash.svg" alt="trash"></button>--}}
                {{-- </td>--}}
            </tr>
            @empty
            <tr>
                <td colspan="6">{{__('data_field_name.working_process.empty')}}</td>
            </tr>
            @endforelse
        </tbody>
    </table>
    <div wire:ignore.self class="modal fade" id="downloadFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;right:15px;top:15px">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                    <h5 class="modal-title text-uppercase">
                    {{__('data_field_name.user.attach_documents')}}
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row mt-3">
                        @forelse ($listFile as $val)
                        <div class="form-group col-md-12">
                            <div class="attached-files" wire:click="download('{{$val->url}}', '{{$val->file_name}}')" style="cursor: pointer">
                                <img src="/images/File.svg" alt="file">
                                <div class="content-file">
                                    <p>{{$val->file_name }}</p>
                                    <p class="kb">{{ $val->size_file }}</p>
                                </div>
                            </div>
                        </div>
                        @empty
                        <p>{{__('common.message.empty_search')}}</p>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common._modalExportFile')
    <script>
    window.addEventListener('openDownloadFileModal', function() {
        $('#downloadFileModal').modal('show');
    })
</script>
</div>

