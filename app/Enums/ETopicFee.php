<?php


namespace App\Enums;


class ETopicFee {
    const STATUS_NOT_SUBMIT = 1;
    const STATUS_SUBMIT = 2;
    const STATUS_COMPLETE_PROFILE = 3;
    const STATUS_INVALID_PROFILE = 4;
    const STATUS_WAIT_ASSESSMENT = 5;
    const STATUS_WAIT_APPROVAL = 6;
    const STATUS_REJECT = 7;
    const STATUS_APPROVAL = 8;
    const STATE_CAPITAL = 1;
    const ODA = 2;

    public static function getListSource() {
        return [
            self::STATE_CAPITAL => __('data_field_name.research_cost.state_capital'),
            self::ODA => __('data_field_name.research_cost.oda'),
        ];
    }

    public static function getListStatus() {
        return [
            self::STATUS_NOT_SUBMIT => __('common.status.status_not_submit'),
            self::STATUS_SUBMIT => __('common.status.status_submit'),
            self::STATUS_COMPLETE_PROFILE => __('common.status.status_complete_profile'),
            self::STATUS_INVALID_PROFILE => __('common.status.status_invalid_profile'),
            self::STATUS_WAIT_ASSESSMENT => __('common.status.status_wait_assessment'),
            self::STATUS_WAIT_APPROVAL => __('common.status.status_wait_approval'),
            self::STATUS_REJECT => __('common.status.status_reject'),
            self::STATUS_APPROVAL => __('common.status.status_approval'),
        ];
    }

    public static function getListStatusChose() {
        return [
            self::STATUS_WAIT_APPROVAL => __('common.status.status_wait_approval'),
            self::STATUS_REJECT => __('common.status.status_reject'),
            self::STATUS_APPROVAL => __('common.status.status_approval'),
        ];
    }
}
