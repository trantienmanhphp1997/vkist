<?php

namespace App\Http\Livewire\Admin\ResearchProject\Productivity;

use Illuminate\Support\Facades\Config;
use App\Enums\EMasterDataType;
use App\Http\Livewire\Base\BaseLive;
use App\Models\MasterData;
use App\Models\ResearchCategory;
use App\Models\ResearchProject;
use App\Models\TaskWork;
use App\Models\Department;
use App\Models\Topic;
use Livewire\Component;
use Cookie;
use Illuminate\Support\Carbon;
use App\Exports\ResearchReportExport;
use Excel;
class Index extends BaseLive
{
    public $projectName ;
    public $topicId ;
    public $statusId ;
    public $pieChart ;
    public $projectStatusId ;
    public $topicType ;
    public $type_report = 1;
    public $startDate;
    public $endDate;
    public $start_date_filter;
    public $end_date_filter;
    public $implementing_agencies;
    public $field;

    // trạng thái tiến bộ của dự án
    public $projectScheduleStatus ;

    protected $listeners = [
        'set-start_date_filter' => 'setStartDateFilter',
        'set-end_date_filter' => 'setEndDateFilter',
    ];

    public function render()
    {
        $researchCategoryLevel = MasterData::where('type', EMasterDataType::TOPIC_LEVEL)->get();
        $topicList = Topic::all();
        // danh sách công việc trong kpi modal
        $taskListQuery = TaskWork::query()->with(['assigner'])->where(['topic_id'=>$this->topicId])->orderBy('id', 'desc')->get();
        $totalTaskExpired = 0 ;
        foreach ($taskListQuery as $task){
            $endDateByPlan = strtotime(date('Y-m-d', strtotime($task->end_date)));
            $endDateByCurrent = strtotime(date('Y-m-d', strtotime($task->real_end_date))) ;
            if ($task->status == 3){
                if ($endDateByCurrent > $endDateByPlan){
                    $delayDate = (int)(( ($endDateByPlan - $endDateByCurrent  )/86400));

                    if ($delayDate < 0){
                        $totalTaskExpired += $delayDate;
                    }
                }else{
                    $totalTaskExpired += 0;
                }
            }else{
                if ($endDateByPlan > strtotime(date('Y-m-d'))){
                    $delayDate = (int)(( ($endDateByPlan - strtotime(date('Y-m-d'))   )/86400));

                    if ($delayDate < 0){
                        $totalTaskExpired += $delayDate;
                    }
                }else{
                    $totalTaskExpired += 0;
                }
            }
        }
        $taskListWithPagination = TaskWork::query()->with(['assigner'])->where(['topic_id'=>$this->topicId])->orderBy('id', 'desc')->get();
        foreach ($taskListWithPagination as $task){
            $endDateByPlan = strtotime(date('Y-m-d', strtotime($task->end_date)));
            $endDateByCurrent = strtotime(date('Y-m-d', strtotime($task->real_end_date))) ;
            if ($task->status == 3){
                if ($endDateByCurrent > $endDateByPlan){
                    $delayDate = (int)(( ($endDateByPlan - $endDateByCurrent )/86400));

                    if ($delayDate < 0){
                        $task->delayDate  = $delayDate;
                    }
                }else{
                    $totalTaskExpired += 0;
                }
            }else{
                if ($endDateByPlan > strtotime(date('Y-m-d'))){
                    $delayDate = (int)(( ($endDateByPlan - strtotime(date('Y-m-d'))  )/86400));

                    if ($delayDate < 0){
                        $task->delayDate  = $delayDate;
                    }
                }else{
                    $totalTaskExpired += 0;
                }
            }
        }
        $query = ResearchProject::query()->with(['topic.task.topicActualFee','researchCategory']);
        if ($this->searchTerm != null){
            $query->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8($this->searchTerm)) . "%'")
                ->orWhere('project_code', 'LIKE', '%' . $this->searchTerm . '%');
        }
        if ($this->projectStatusId != null){
            $query->where(['status'=>$this->projectStatusId]);
        }
        if ($this->topicType != null){
            $query->orWhereHas('topic', function ($q)  {
                $q->where(['level_id'=>$this->topicType]);
            });
        }
        if ($this->field != null){
            $query->orWhereHas('topic', function ($q)  {
                $q->where(['research_category_id'=>$this->field]);
            });
        }
        if (!empty($this->start_date_filter)) {
            $query->where('start_date', '>=', Carbon::parse($this->start_date_filter)->startOfDay());
        }
        if (!empty($this->end_date_filter)) {
            $query->where('end_date', '<', Carbon::parse($this->end_date_filter)->startOfDay()->addDays(1));
        }
        if (!empty($this->implementing_agencies)) {
            $query->where('department_id', $this->implementing_agencies);
        }
        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);

        //dem tong cong cac du an theo loai de tai
        $countProjectInStateLevel = 0 ;
        $countProjectInBaseLevel = 0 ;
        $countProjectInCadres = 0 ;
        // dem tong cong so du an co cong viec vuot tien bo , dung han , qua han
        $countProjectBeyondTime = 0 ;
        $countProjectOnTime = 0 ;
        $countProjectExpired = 0 ;
        foreach ($data as $key => $researchProject) {
            $deadlineDate = (strtotime(date('Y-m-d', strtotime($researchProject->end_date)) ));
            $currentDate = strtotime(date('Y-m-d'));
            $researchProject->different_date = (int)(($deadlineDate - $currentDate)/86400);
            if ($researchProject->different_date > 0){
                $countProjectBeyondTime += 1;
            }else if ($researchProject->different_date = 0){
                $countProjectOnTime += 1;
            }else if ($researchProject->different_date < 0){
                $countProjectExpired += 1 ;
            }

            //loai bo ban ghi tuy vao trang thai tien do
            if ($this->projectScheduleStatus == ResearchProject::BEYOND_PROCESS && $researchProject->different_date <= 0){
                unset($data[$key]);
            }
            if ($this->projectScheduleStatus == ResearchProject::ON_TIME && $researchProject->different_date != 0){
                unset($data[$key]);
            }
            if ($this->projectScheduleStatus == ResearchProject::EXPIRED && $researchProject->different_date >= 0){
                unset($data[$key]);
            }
            //lay tong so cac cap do cua moi du an dua tren de tai
            if (isset($researchProject->topic) && isset($researchProject->topic->level_id) && $researchProject->topic->level_id == EMasterDataType::TOPIC_LEVEL_CADRES){
                $countProjectInCadres += 1 ;
            }
            if (isset($researchProject->topic) && isset($researchProject->topic->level_id) && $researchProject->topic->level_id == EMasterDataType::TOPIC_LEVEL_BASE){
                $countProjectInBaseLevel += 1 ;
            }
            if (isset($researchProject->topic) && isset($researchProject->topic->level_id) && $researchProject->topic->level_id == EMasterDataType::TOPIC_LEVEL_STATE){
                $countProjectInStateLevel += 1 ;
            }

            if (count($researchProject->topic->task ?? [] ) > 0){
                $researchProject->countTaskExpired = 0 ;
                $researchProject->countTaskExpiredByFee = 0 ;
                foreach ($researchProject->topic->task as $task){
                    if ($task->topicActualFee->disbursement_date ?? null && (int)((strtotime(date('Y-m-d', strtotime($task->topicActualFee->disbursement_date))) - strtotime(date('Y-m-d')))/86400) < 0){
                            $researchProject->countTaskExpiredByFee += 1 ;
                    }
                    if ($task->real_end_date && (int)((strtotime(date('Y-m-d', strtotime($task->real_end_date))) - strtotime(date('Y-m-d')))/86400) < 0){
                           $researchProject->countTaskExpired += 1 ;
                    }
                }
            }
            $total = TaskWork::query()->with(['topic.projectResearch', 'reviewer', 'follower.memberInfo', 'assigner']);
            $total->whereHas('topic.projectResearch', function ($query) use ($researchProject) {
                $query->where(['contract_code'=>$researchProject->contract_code]);
            });
            $totalTask = $total->get()->count();
            if ($totalTask > 0) {
                $researchProject->progress = ($total->where('status', 3)->get()->count() / $totalTask) * 100;
            } else {
                $researchProject->progress = 0;
            }

            $topicFee = $researchProject->topic->topicFee ?? null;
            if (!empty($topicFee) && count($topicFee) > 0) {
                foreach($topicFee as $item) {
                    if ($item->status == 8) {
                        foreach($item->topicFeeDetail as $detail)
                        $researchProject->grant_budget += $detail->total_capital;
                    }
                }
            } else {
                $researchProject->grant_budget = 0;
            }

            $actualFee = $researchProject->topic->actualFee ?? null;
            if (!empty($actualFee) && count($actualFee) > 0) {
                foreach($actualFee as $item) {
                    if ($item->status == 1) {
                       $researchProject->budget_used += $item->approval_costs;
                    }
                }
            } else {
                $researchProject->budget_used = 0;
            }
        }

        //danh sach the loai nghien cuu
        $researchCategory = ResearchCategory::all();
        //danh sach trang thai du an
        $statusProjectList = [
            ['name' => __('research/project.status.wait_approval'), 'id' => ResearchProject::WAIT_APPROVAL],
            ['name' => __('research/project.status.in_process'), 'id' => ResearchProject::IN_PROCESSING],
            ['name' => __('research/project.status.completed'), 'id' => ResearchProject::COMPLETED],
            ['name' => __('research/project.status.cancel'), 'id' => ResearchProject::CANCEL],
        ];

        // danh sach trang thai cong viec
        $statusTaskList = [
            TaskWork::STATUS_UNDO_TASK => __('research/taskwork.select-status.undo'),
            TaskWork::STATUS_ON_WORKING_TASK => __('research/taskwork.select-status.on-working'),
            TaskWork::STATUS_APPROVE_TASK => __('research/taskwork.select-status.approve'),
            TaskWork::STATUS_DONE_TASK => __('research/taskwork.select-status.done'),
        ];
        $department = Department::get();
        return view('livewire.admin.research-project.productivity.index',compact('statusProjectList','data','topicList','statusTaskList',
            'taskListWithPagination','totalTaskExpired','researchCategory','researchCategoryLevel' , 'countProjectBeyondTime','countProjectOnTime','countProjectExpired'
            , 'countProjectInBaseLevel' , 'countProjectInStateLevel' , 'countProjectInCadres', 'department'
        ));
    }

    public function getKpiAnalysis($id){
        $project = ResearchProject::findOrFail($id);
        //thông tin dự án

        $this->topicId = Topic::query()->where(['contract_code'=>$project->contract_code])->first()->id ?? '';

        $this->projectName = $project->name ?? '';
        $this->statusId = $project->status ?? '';

        $this->startDate = reFormatDate($project->start_date  ) ?? '' ;
        $this->endDate = reFormatDate($project->end_date ) ?? '';

        //tổng các chỉ số của từng trạng thái công việc
        $countTotalTask = TaskWork::query()->with(['assigner'])->where(['topic_id'=>$this->topicId])->get()->count();
        $countTotalTask = $countTotalTask != 0 ? $countTotalTask : 1 ;
        $unDoneTask =  TaskWork::query()->with(['assigner'])->where(['status'=>TaskWork::STATUS_UNDO_TASK])->where(['topic_id'=>$this->topicId])->get()->count();
        $undoneTaskPercent = round( (100/$countTotalTask)*$unDoneTask );
        $inProcess = TaskWork::query()->with(['assigner'])->where(['status'=>TaskWork::STATUS_ON_WORKING_TASK])->where(['topic_id'=>$this->topicId])->get()->count();
        $inProcessTaskPercent = round((100/$countTotalTask)*$inProcess);
        $waitApproval = TaskWork::query()->with(['assigner'])->where(['status'=>TaskWork::STATUS_APPROVE_TASK])->where(['topic_id'=>$this->topicId])->get()->count();
        $waitApprovalTask = round((100/$countTotalTask)*$waitApproval);
        $doneTask = TaskWork::query()->with(['assigner'])->where(['status'=>TaskWork::STATUS_DONE_TASK])->where(['topic_id'=>$this->topicId])->get()->count();
        $doneTaskPercent = round((100/$countTotalTask)* $doneTask);


        $this->dispatchBrowserEvent('getPieChart',['undone'=>$undoneTaskPercent , 'inProcess'=>$inProcessTaskPercent,'waitApproval'=>$waitApprovalTask,'done'=>$doneTaskPercent ,
            'undonetitle'=>__('research/statistic.title.task.task_status.waiting') ,
            'inProcesstitle'=>__('research/statistic.title.task.task_status.in-progress'),
            'donetitle'=> __('research/statistic.title.task.task_status.done'),
            'outDateTask'=>__('research/statistic.title.project_overview.out_of_date')
        ]);
    }

    public function getListTask($contract_code){
        $this->redirect(route('admin.research-project.task-work.index',$contract_code));
    }
    public function setStartDateFilter($data) {
        $this->start_date_filter = $data['start_date_filter'] ? date('Y-m-d', strtotime($data['start_date_filter'])) :null;
    }
    public function setEndDateFilter($data) {
        $this->end_date_filter = $data['end_date_filter'] ? date('Y-m-d', strtotime($data['end_date_filter'])) : null;
    }
    public function export() {
        return Excel::download(new ResearchReportExport($this->topicType, $this->start_date_filter, $this->end_date_filter, $this->implementing_agencies, $this->field, $this->type_report, $this->projectStatusId), 'research-report-' . now()->format(Config::get('common.year-month-day')) . '-' . '.xlsx');
    }
}
