<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <a href="">{{__('decision-reward/decision-reward.breadcrumbs.decision-reward-management')}}</a>
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row mx-5 p-4 bg-white">
                    <div class="col-md-12">
                            <h3 class="title">
                                @if ($isEdit == false)  {{__('decision-reward/decision-reward.title.create')}}
                                @else  {{__('decision-reward/decision-reward.title.edit')}}
                                @endif
                            </h3>
                        </div>
                        <div class="col-md-12">
                            <h4 class="size14">
                                {{__('decision-reward/decision-reward.form.general_info')}}
                            </h4>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.name')}}<span class="text-danger">&nbsp*</span></label>
                                        <input type="text" class="form-control" placeholder="" name="name" wire:model.defer="name">
                                        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.reward_date')}}</label>
                                        {{-- <input type="date" id="reward_date" class="form-control" name="reward_date" value="{{$reward_date}}"> --}}
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'reward_date', 'place_holder'=>'', 'default_date' => $reward_date ?? null ])
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.number_decision')}}<span class="text-danger">&nbsp*</span></label>
                                        <input type="text" class="form-control" placeholder="" name="number_decision" wire:model.defer="number_decision">
                                        @error('number_decision') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.user_decision')}}</label>
                                        @livewire('component.search-combobox', ['model_name' => App\Models\UserInf::class, 'classify' => 1, 'filters' => ['fullname'], 'value_render_on_combobox' => 'fullname','targetId' => $user_decide_id ?? null])
                                        @error('user_decision') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.type_of_decision')}}</label>
                                        <select class="form-control form-control-lg" name="type_of_decision" id="type_of_decision" wire:model.defer="type_of_decision">
                                            <option value="1">{{App\Enums\EDecisionRewardDecisionType::valueToName(App\Enums\EDecisionRewardDecisionType::REWARD)}}</option>
                                            <option value="2">{{App\Enums\EDecisionRewardDecisionType::valueToName(App\Enums\EDecisionRewardDecisionType::DISCIPLINE)}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.reason')}}</label>
                                        <textarea type="text" name="reason" class="form-control" placeholder="" cols="30" rows="3" wire:model.defer="reason"> </textarea>
                                        @error('reason') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.type_reward')}}</label>
                                        <input type="text" class="form-control" placeholder="" name="type_reward" wire:model.defer="type_reward">
                                        @error('type_reward') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.decision_date')}}</label>
                                        {{-- <input type="date" id="decision_date" class="form-control" name="decision_date" value="{{$decision_date}}"> --}}
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'decision_date', 'place_holder'=>'', 'default_date' => $decision_date ?? null ])
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.type_of_prize')}}</label>
                                        <select class="form-control form-control-lg" name="type_of_prize" id="type_of_prize" wire:model.defer="type_of_prize">
                                            <option value="1">{{App\Enums\EDecisionRewardPrizeType::valueToName(App\Enums\EDecisionRewardPrizeType::MONEY)}}</option>
                                            <option value="2">{{App\Enums\EDecisionRewardPrizeType::valueToName(App\Enums\EDecisionRewardPrizeType::GLORIFY)}}</option>
                                            <option value="3">{{App\Enums\EDecisionRewardPrizeType::valueToName(App\Enums\EDecisionRewardPrizeType::PRODUCT)}}</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.user_decision_position')}}</label>
                                        <input type="text" class="form-control" placeholder="" readonly wire:model.lazy="user_decision_position" name="user_decision_position">
                                        @error('user.email') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.total_value')}}</label>
                                        <input type="text" class="form-control form-input format_number" placeholder="" name="total_value" wire:model.defer="total_value">
                                        @error('total_value') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('decision-reward/decision-reward.label.base')}}</label >
                                        <textarea type="text" class="form-control" placeholder="" cols="30" rows="3" name="base" wire:model.defer="base"> </textarea>
                                        @error('base') <span class="text-danger">{{ $message }}</span> @enderror

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h4 class="size14 text-uppercase">
                                {{__('decision-reward/decision-reward.form.receiver_list')}}
                            </h4>
                            <button type="button" id="btn-add-decision-reward-receiver"
                            data-toggle="modal" data-target="#add-receiver-modal" class="btn btn-primary my-2">
                                {{__('common.button.add')}}
                            </button>
                            <table class="table">
                                <thead>
                                    <tr class="border-radius">
                                        <th scope="col" style="width:15%">{{__('decision-reward/decision-reward.receiver_table_column.name')}}</th>
                                        <th scope="col" style="width:10%">{{__('decision-reward/decision-reward.receiver_table_column.code')}}</th>
                                        <th scope="col" style="width:15%">{{__('decision-reward/decision-reward.receiver_table_column.department')}}</th>
                                        <th scope="col" style="width:15%">{{__('decision-reward/decision-reward.receiver_table_column.position')}}</th>
                                        <th scope="col" style="width:15%">{{__('decision-reward/decision-reward.receiver_table_column.prize_value')}}</th>
                                        <th scope="col" style="width:20%">{{__('decision-reward/decision-reward.receiver_table_column.reason')}}</th>
                                        <th scope="col" style="width:10%">{{__('decision-reward/decision-reward.receiver_table_column.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($listOldReceiverTemp as $key => $val)
                                    <tr>
                                        <td>{{$val['fullname'] ?? '' }}</td>
                                        <td>{{$val['code'] ?? '' }}</td>
                                        <td>{{$val['department'] ?? '' }}</td>
                                        <td>{{$val['position'] ?? '' }}</td>
                                        <td>{{numberFormat($val['prize_value']) }}</td>
                                        <td>{{$val['reason'] ?? '' }}</td>
                                        <td>
                                        <button type="button" wire:click="deleteReceiverOldTemp({{$key}})" class="btn-sm border-0 bg-transparent">
                                                <img src="/images/trash.svg" alt="trash">
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                @foreach ($listNewReceiver as $key => $val)
                                    <tr>
                                        <td>{{$val['fullname'] ?? '' }}</td>
                                        <td>{{$val['code'] ?? '' }}</td>
                                        <td>{{$val['department'] ?? '' }}</td>
                                        <td>{{$val['position'] ?? '' }}</td>
                                        <td>{{$val['prize_value'] ?? ''}}</td>
                                        <td>{{$val['reason'] ?? '' }}</td>
                                        <td>
                                            <button type="button" wire:click="deleteNewReceiver({{$key}})" class="btn-sm border-0 bg-transparent">
                                                <img src="/images/trash.svg" alt="trash">
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12 upload-container">
                            <h4 class="size14">
                                {{__('decision-reward/decision-reward.form.document')}}
                            </h4>
                            <form wire:submit.prevent="submit" enctype="multipart/form-data">
                                <div class="form-group upload-file">
                                    <label></label>
                                    <div class="bd-attached file_form">
                                        <div class="attached-center">
                                            <input type="file" multiple onchange="uploadFile(this, @this)" id="file_create" class="custom-file-input cur_input">
                                            <span class="file" >
                                                {{__('decision-reward/decision-reward.form.attach_file')}}
                                                <img src="/images/Clip.svg" alt="clip" height="24">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="col-md-12">
                                <div class="row">
                                        @foreach ($listFileUpload as $key => $val)
                                        <div class="form-group col-md-4">
                                            <div class="attached-files">
                                                <img src="/images/File.svg" alt="file">
                                                <div class="content-file">
                                                    <p>{{ $val['file_name'] }}</p>
                                                    <p class="kb">{{ $val['size_file']}}</p>
                                                </div>
                                                <span>
                                                <button wire:click.prevent="deleteNewFileUpload({{$key}})"
                                                        style="border: none;background: white" type="button"><img
                                                        src="/images/close.svg" alt="close"/>
                                                </button>
                                            </span>
                                            </div>
                                        </div>
                                    @endforeach
                                    @foreach ($listOldFileTemp as $key => $val)
                                        <div class="form-group col-md-4">
                                            <div class="attached-files">
                                                <img src="/images/File.svg" alt="file">
                                                <div class="content-file" wire:click="download('{{$val['url']}}', '{{$val['file_name']}}')" style="cursor: pointer">
                                                    <p>{{ $val['file_name'] }}</p>
                                                    <p class="kb">{{ $val['size_file']}}</p>
                                                </div>
                                                <span>
                                                    <button wire:click.prevent="deleteFileOldTemp({{$key}})"
                                                            style="border: none;background: white" type="button"><img
                                                            src="/images/close.svg" alt="close/>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="file-loading position-absolute bg-primary rounded text-center text-white" style="transition: width 0.3s; bottom: 0px; left: 0; height: 5px; width: 0%;">&nbsp;</div>
                                <div class="row">
                                    @error('fileUpload')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                
                                    @error('current_total_file')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror   
                                </div>
                                <div class="row">
                                    <div class="text-danger upload-error"></div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 mt-4">
                            <div class="d-flex justify-content-center group-btn2">
                                <a class="btn btn-cancel" style="color:gray" href="{{route('admin.decision-reward.index')}}">
                                    {{__('common.button.cancel')}}
                                </a>
                                <button type="button" id="btn-save-decision-reward" wire:click="store" class="btn btn-save mr-2">
                                    {{__('common.button.save')}}
                                </button>

                            </div>
                        </div>
                            <!-- Modal add receiver -->
                            <form wire:submit.prevent="submit">
                                <div wire:ignore.self class="modal fade" id="add-receiver-modal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document" style="max-width: 1140px">

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;right:15px;top:15px">
                                                    <span aria-hidden="true close-btn">×</span>
                                                </button>
                                                <h5 class="modal-title">
                                                    {{__('decision-reward/decision-reward.title.create-receiver')}}
                                                </h5>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>{{__('decision-reward/decision-reward.receiver_table_column.name')}}</label>
                                                            @livewire('component.search-combobox', ['model_name' => App\Models\UserInf::class, 'classify' => 2, 'filters' => ['fullname'], 'value_render_on_combobox' => 'fullname','targetId' => null])
                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{__('decision-reward/decision-reward.receiver_table_column.department')}}</label>
                                                            <input type="text" class="form-control" placeholder="" name="department" readonly wire:model.lazy="receiver_department">
                                                        </div>
                                                        <div class="form-group">
                                                                <label>{{__('decision-reward/decision-reward.receiver_table_column.prize_value')}}</label>
                                                                <input type="text" class="form-control form-input format_number" placeholder="" name="prize_value" wire:model.defer="receiver_prize_value">
                                                            </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>{{__('decision-reward/decision-reward.receiver_table_column.code')}}</label>
                                                            <input type="text" class="form-control" placeholder="" name="code" readonly wire:model.lazy="receiver_code">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{__('decision-reward/decision-reward.receiver_table_column.position')}}</label>
                                                            <input type="text" class="form-control" placeholder="" name="position" readonly wire:model.lazy="receiver_position">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>{{__('decision-reward/decision-reward.receiver_table_column.reason')}}</label>
                                                            <input type="text" class="form-control" placeholder="" name="receiver_reason" wire:model.defer="receiver_reason">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer border-0">
                                                <div class="group-btn2">
                                                    <button type="button" class="btn btn-cancel" data-dismiss="modal" wire:click="resetFormAddReceiver"                                                      ">
                                                        {{__('common.button.cancel')}}
                                                    </button>
                                                    {{-- <button type="button"  class="btn btn-outline-primary mr-2" wire:click="addReceiver">
                                                            {{__('common.button.save_and_add')}}
                                                    </button> --}}
                                                    <button type="button"  class="btn btn-save mr-2" data-dismiss="modal" wire:click="addReceiver">
                                                            {{__('common.button.save')}}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- End add receiver -->
            </div>

        </div>

    </div>
    <script type="text/javascript">
        function uploadFile(input, proxy = null, index = 0) {
                let file = input.files[index];
                if (!file || !proxy) return;
                let $fileLoading = $(input).parents('.upload-container').find('.file-loading');
                let $error = $(input).parents('.upload-container').find('.upload-error');
                $fileLoading.width('0%');
                if(file.size/(1024*1024) >= {{$limit_file_size}}) {
                    console.log($error);
                    $error.html("{{ __('notification.upload.maximum_size', ['value' => $limit_file_size]) }}");
                    return;
                }
                let mimes = {!! json_encode($accept_file) !!};
                let extension = file.name.split('.').pop();
                if (!mimes.includes(extension)) {
                    $error.html("{{ __('notification.upload.mime_type', ['values' => implode(', ', $accept_file)]) }}");
                    return;
                }
                proxy.upload('fileUpload', file, (uploadedFilename) => {
                    setTimeout(() => {
                        $fileLoading.width('0%');
                        uploadFile(input, proxy, index + 1);
                    }, 500);
                }, () => {
                    console.log(error);
                }, (event) => {
                    $fileLoading.width(event.detail.progress + '%');
                });
                
            }
    </script>

</div>


