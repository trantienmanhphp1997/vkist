<div class="col-md-10 col-xl-11 box-detail box-user bg-light">
    <div class="breadcrumbs">
        <a class="text-primary" href="#">{{ __('data_field_name.shopping.shopping_management') }}</a> \ <span class="text-muted">{{ __('data_field_name.shopping.shopping_proposal') }}</span>
    </div>

    <div class="box-content">
        <div wire:loading class="loader" style="z-index: 10;"></div>

        <div class="row">
            <div class="col-md-6">
                <h3 class="title">{{ __('data_field_name.shopping.list') }}</h3>
            </div>
            <div class="col-md-6 text-right">
                @if ($checkCreatePermission)
                    <a href="{{ route('admin.executive.shopping.create') }}" class="btn btn-viewmore-news mr0 mb-12 border-12"><img src="{{ asset('images/plus2.svg') }}" alt="plus"> {{ __('data_field_name.shopping.create') }}</a>
                @endif
            </div>
        </div>

        <div class="information">
            <div class="row mb-3">
                <div class="col-sm-3">
                    <input type="text" class="form-control size13" placeholder="{{ __('data_field_name.shopping.search') }}" aria-describedby="basic-addon2" wire:model.debounce.1000ms="searchTerm">
                </div>
                @if (!$currentWork)
                    <div class="col-sm-3">
                        {{ Form::select('status', ['' => __('data_field_name.shopping.status')] + $shoppingStatus, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchStatus']) }}
                    </div>
                @endif

                @if ($checkEditPermission)
                    <div class="col-sm-3 d-flex align-items-center">
                        <input type="checkbox" wire:model.lazy="currentWork" id="current-work" class="mr-2">
                        <label class="m-0" for="current-work">{{ __('data_field_name.shopping.current_work') }}</label>
                    </div>
                @endif
                @if ($checkDownloadPermission)
                    <button class="btn btn-fix bg-content px-10 py-10" wire:click="export"><img src="/images/DownloadBlue.svg" alt="download"></button>
                @endif
            </div>
            <div class="table-responsive">
                <table class="table table-general">
                    <thead>
                        <tr class="border-radius text-center">
                            <th scope="col" class="border-radius-left">{{ __('data_field_name.shopping.proposal_date') }}</th>
                            <th scope="col">{{ __('data_field_name.shopping.budget') }}</th>
                            <th scope="col">{{ __('data_field_name.shopping.name') }}</th>
                            <th scope="col">{{ __('data_field_name.shopping.estimated_delivery_date') }}</th>
                            <th scope="col">{{ __('data_field_name.shopping.proposer') }}</th>
                            @if ($checkApprovePermission)
                                <th scope="col">{{ __('data_field_name.shopping.proposer_department') }}</th>
                            @endif

                            @if ($checkEditPermission)
                                <th scope="col">{{ __('data_field_name.shopping.executor') }}</th>
                            @endif
                            <th scope="col" class="rounded-right">{{ __('data_field_name.shopping.status') }}</th>
                        </tr>
                    </thead>
                    <div wire:loading class="loader"></div>
                    <tbody>
                        @foreach ($data as $value)
                            <tr class="text-center">
                                <td class="rounded-left">{{ reFormatDate($value->created_at) }}</td>
                                <td>{!! boldTextSearch($value->budget->name ?? '', $searchTerm) !!}</td>
                                <td><a href="{{ route('admin.executive.shopping.show', ['id' => $value->id]) }}">{!! boldTextSearch($value->name, $searchTerm) !!}</a></td>
                                <td>{{ reformatDate($value->estimated_delivery_date) }}</td>
                                <td>{{ $value->proposer->name ?? '' }}</td>
                                @if ($checkApprovePermission)
                                    <td>{{ $value->proposer->info->department->name ?? '' }}</td>
                                @endif

                                @if ($checkEditPermission)
                                    <td>{{ $value->executor->name ?? '' }}</td>
                                @endif
                                <td class="rounded-right">{!! $shoppingHTMLClassList[$value->status] ?? '' !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @if (count($data) > 0)
                {{ $data->links() }}
            @else
                <div class="title-approve text-center">
                    <span>{{ __('common.message.empty_search') }}</span>
                </div>
            @endif
        </div>


    </div>
</div>
