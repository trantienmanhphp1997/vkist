<div>
    <div class="inner-tab pt0">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group search-expertise search-plan">
                    <div class="search-expertise inline-block">
                        <input type="text" class="form-control size13" name="searchTerm" autocomplete="off"
                               wire:model.debounce.1000ms="searchTerm"
                               placeholder="{{__('data_field_name.asset.search_place')}}">
                        <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                @if(checkRoutePermission('download'))
                    <div class="form-group float-right">
                        <button type="button" id='btnOpenExportModal' style="border-radius: 11px; border:none;"
                                data-toggle="modal" data-target="#export-modal-maintain">
                            <img src="/images/filterdown.svg" alt="filterdown">
                        </button>
                    </div>
                @endif
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-general" id='dataTable'>
                <thead>
                <tr class="border-radius">
                    <th scope="col"
                        class="text-uppercase border-radius-left">{{__('data_field_name.asset.asset_code')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.number_report')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.asset_name')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.asset_cate')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.asset_content')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.implementation_date')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.provider')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.asset_status')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.asset_quantity')}}</th>
                    <th scope="col" class="text-uppercase ">{{__('data_field_name.asset.maintenance_user')}}</th>
                    <th scope="col"
                        class="text-uppercase border-radius-right">{{__('data_field_name.asset.estimated_cost')}}</th>
                </tr>
                </thead>
                <div wire:loading class="loader"></div>
                <tbody>
                @forelse($data as $key=> $val)
                    <tr>
                        <td class="text-center">{!! boldTextSearch($val->asset->code,$searchTerm) !!}</td>
                        <td class="text-center">
                            @if(checkRoutePermission('approve'))
                                <a style="cursor: pointer;" class="text-primary" href="#" data-toggle="modal"
                                   data-target="#confirm_notice_maintenancing"
                                   wire:click="confirmNotice({{ $val->id }})">
                                    {{$val->number_report ?? ''}}
                                </a>
                            @else
                                {{$val->number_report ?? ''}}
                            @endif
                        </td>
                        <td class="text-center">{!! boldTextSearch($val->asset->name,$searchTerm) !!}</td>
                        <td class="text-center">{{$val->asset->category->name ?? ''}} </td>
                        <td class="text-center">
                            @if($val->status==\App\Enums\EAssetStatus::REPAIRED)
                                {{$val->content_repair ?? ''}}
                            @elseif($val->status==\App\Enums\EAssetStatus::MAINTENANCE)
                                {{$val->rule_maintenance->content_maintenance ?? ''}}
                            @endif
                        </td>
                        <td class="text-center">{{reFormatDate($val->implementation_date, 'd/m/Y')??''}}</td>
                        <td class="text-center">{{ $val->provider->name ??''}}</td>
                        <td class="text-center">
                            @if($val->status==\App\Enums\EAssetStatus::REPAIRED||$val->status==\App\Enums\EAssetStatus::MAINTENANCE)
                                {{$val->status?$assetStatus[$val->status]:''}}
                            @else
                                {{$val->status?$assetSituation[$val->status]:''}}
                            @endif

                        </td>
                        <td class="text-center">{{$val->implementation_quantity}}</td>
                        <td class="text-center">{{$val->asset->user->fullname ?? ''}}</td>
                        <td class="text-center">{{numberFormat($val->estimated_cost) ?? ''}}</td>
                    </tr>
                @empty
                    <tr class="text-danger">
                        <td colspan="12" class="text-center">{{__('common.message.no_data')}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        {{$data->links()}}
    </div>
    <div class="modal fade modal-fix" wire:ignore.self id="confirm_notice_maintenancing" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="submit">
                    <div class="modal-header">
                        <h6></h6>
                        <h6 class="text_center">{{__('data_field_name.asset.maintenance_Update_completed_repair')}}
                            -{{__('data_field_name.asset.maintenance_property_maintenance')}}</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-header">
                        <div class="form-group info-request">
                            <label class="text_center">{{__('data_field_name.asset.number_report')}}</label>
                            <input type="text" value="{{$detailNotice->number_report ?? ''}}" name="number_report"
                                   disabled>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 pd-col">
                                    <div class="info-asset">
                                        @livewire('admin.asset.maintenance.asset-info')
                                    </div>
                                </div>
                                <div class="col-6 pd-col-15">
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.asset_quantity')}}</label>
                                        <input type="number"
                                               value="{{$detailNotice->implementation_quantity ?? ''}}" disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.repair_date')}}
                                            / {{__('data_field_name.asset.maintenance_insurance')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice?reFormatDate($detailNotice->implementation_date, 'd/m/Y') : ''}}"
                                               disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.maintenance_Estimated_completion_date')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice?reFormatDate($detailNotice->estimated_completion_date, 'd/m/Y') : ''}}"
                                               disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.maintenance_performer')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice->user->fullname?? ''}}" disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.maintenance_implementing_agencies')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice->provider->name ?? ''}}" disabled>
                                    </div>
                                    <div class="form-group ">
                                        <input type="checkbox" name="achievement_paper_check" id="a-1" value="1"
                                               wire:model.lazy="finished">
                                        <label for="a-1">{{__('data_field_name.asset.check_finish')}}</label><br>
                                        @error('finished') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="col-6 pd-col-15">
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.estimated_cost')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice->estimated_cost ?? ''}}" disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.maintenance_content_repair')}}</label>
                                        <textarea class="form-control" rows="3" disabled>
                                            @if($detailNotice)
                                                @if($detailNotice->status==\App\Enums\EAssetStatus::REPAIRED)
                                                    {{$detailNotice->content_repair ?? ''}}
                                                @elseif($detailNotice->status==\App\Enums\EAssetStatus::MAINTENANCE)
                                                    {{$detailNotice->rule_maintenance->content_maintenance ?? ''}}
                                                @endif
                                            @endif
                                        </textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-cancel" id="close-confirm-maintenancing"
                                data-dismiss="modal">{{__('common.button.cancel')}}
                        </button>
                        <button type="button" class="btn btn-primary"
                                wire:click.prevent="confirm()">{{__('common.status.status_approval')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal export /-->
    <div class="modal fade" id="export-modal-maintain" tabindex="-1" aria-labelledby="delete-department-modal-label"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>
                        <h5 class="modal-title"
                            id="delete-department-modal-label">{{__('notification.member.warning.warning')}}</h5>
                    </strong>
                </div>
                <div class="modal-body">
                    {{__('notification.member.warning.Do you want to export excel file?')}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel"
                            data-dismiss="modal">{{__('common.button.back')}}</button>
                    <button type="button" class="btn btn-save" data-dismiss="modal"
                            wire:click="export">{{__('common.button.export_file')}}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal export /-->
</div>
