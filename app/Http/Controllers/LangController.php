<?php
namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cookie;

class LangController extends Controller
{
    private $langActive = [
        'vi',
        'en',
    ];

    public function changeLang(Request $request, $lang)
    {
        if (in_array($lang, $this->langActive)) {
            $request->session()->put(['lang' => $lang]);
            Cookie::queue('lang', $lang, 60);
            $userID = Auth::id();
            User::findOrFail($userID)->update(
                [
                    'lang'=>$lang
                ]
            );
            return redirect()->back();
        }
    }

}
