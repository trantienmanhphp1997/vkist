<?php
return [
    "form" => [
        "required" => [
            "common" => [
                "abstract" => "You do not iput :fieldName",
                "user_name" => "You don't input username",
                "password" => "You don't input password",
                "phone_number" => "You don't input phone number",
                "full_name" => "Fullname"
            ],
            "contract" => [
                "department_id" => "Bạn chưa chọn đơn vị lập hợp đồng",
                "statement_id" => "Bạn chưa chọn tờ trình tương ứng của hợp đồng",
            ],
            "plan" => [
                "department" => "Bạn chưa chọn đơn vị",
            ],
            "manager" => [
                "department_code" => "Bạn chưa chọn nhóm cho quản lý",
                "password" => "Bạn chưa nhập mật khẩu"
            ],
            "outsource" => [
                "type" => "Bạn chưa chọn loại nguồn lực",
            ],
            "record_plan" => [
                "plan" => "Bạn chưa chọn kế hoạch",
                "address" => "Bạn chưa nhập thông tin địa chỉ",
                "address2" => "Bạn chưa chọn phòng quay",
                "offer_technical_camera_number" => "Bạn chưa chọn đề xuất số lượng kỹ thuật phòng quay",
            ],
            "phone_number" => "Bạn chưa nhập số điện thoại",
            "device" => [
                "device_category_id" => "Bạn chưa chọn danh mục thiết bị"
            ],
            "user" => [
                "current_password" => "Bạn chưa nhập mật khẩu hiện tại",
                "new_password" => "Bạn chưa nhập mật khẩu mới",
                "confirm_password" => "Bạn chưa xác nhận mật khẩu mới"
            ]
        ],
        "max_length" => [
            "abstract" => "Thông tin :fieldName không dài quá :maxlength ký tự",
        ],
        "format"=>[
            "email" => "Email không đúng định dạng",
            "phone_number" => "Số điện thoại không đúng định dạng",
            "file" => "Định dạng file không đúng"
        ],
        "number" => [
            "min" => ":fieldName phải lớn hơn :min",
            "number" => "Số không hợp lệ"
        ],
        "equal" => [
            "confirm_password" => "Xác nhận mật khẩu mới chưa chính xác"
        ],
        "role" => [
            "name" => "Invalid role name. Copy failed.",
            "code" => "Invalid role code. Copy failed.",
        ],
        'profile' => [
            'password' => [
                'regex' => 'Please enter a password containing uppercase, lowercase, numbers and special characters.',
                'old' => 'The old password is incorrect.',
                'same' => 'Re-enter new password that does not match the new password.',
                'not_same' => 'The new password does not match the old password.'
            ]
        ],
        'research_cost' => [
            'max' => [
                'quantity' => 'The quantity cannot be more than 6 characters.',
                'price' => 'The price cannot be more than 6 characters.',
                'state_capital' => 'State capital cannot be more than 14 characters.',
                'other_capital' => 'Other capital cannot be more than 14 characters.',
                'base_salary' => 'Base salary cannot be more than 12 characters.',
                'salary' => 'Salary cannot be more than 12 characters.',
                'number_workday' => 'Number work day cannot be more than 5 characters.',
            ],
        ],
    ]
];
