<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Cookie;
class LangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // kiểm tra nếu user tồn tại và nếu như cookie có lưu ngôn ngữ nếu tồn tại ngôn ngữ trong cookie thì
        // sẽ set up ngôn ngữ đó và ngược lại nếu chưa lưu ngôn ngữ trong cookie sẽ trực tiếp đổi thành ngôn ngữ dc lưu trong db
        if (Auth::user()){
            if (Cookie::get('lang') != null){
                App::setLocale(Cookie::get('lang') );
            }else{
                App::setLocale(Auth::user()->lang);
            }
        }else{
            App::setLocale('en');
        }


        return $next($request);
    }
}
