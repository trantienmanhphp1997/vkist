<?php


namespace App\Enums;


class ETopicSource {
    const FROM_IDEA = 0;
    const FROM_OFFER = 1;
    const FROM_DIRECTION = 2;

    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            self::FROM_IDEA => __('data_field_name.topic.topic_from_idea'),
            self::FROM_OFFER => __('data_field_name.topic.topic_from_offer'),
            self::FROM_DIRECTION => __('data_field_name.topic.topic_from_direction'),
        ];
    }
}
