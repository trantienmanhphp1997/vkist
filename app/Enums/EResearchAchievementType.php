<?php


namespace App\Enums;


class EResearchAchievementType {
    const SCIENCE_ARTICLE = 1;
    const INTELLECTUAL_PROPERTY = 2;
    const TECHNOLOGY_TRANSFER = 3;
}