<div wire:ignore.self class="modal fade" id="modalSeleteUserInfo" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
    <style>
        .user-select-hover {
            width: 100%;
            cursor: pointer;
            color: black;
        }
        .user-select-hover:hover{
            background: #FAFBFC;
            color:black
        }
    </style>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Chọn người theo dõi</h5>
            </div>
            <div class="modal-footer">
                <input type="text" class="form-control" wire:model.lazy="user_info_name"
                       placeholder="ten nguoi theo doi">
                @error('userInfoName') <span class="text-danger">{{ $message }}</span>@enderror
                <div class="container-fluid mt-16">
                    <div class="row">
                        @if($autoCompleteSearch)
                            @foreach($userInfos as $userInfo)
                                <a class="user-select-hover" data-dismiss="modal" wire:click="setUserInfoId({{$userInfo->userInfo->id}})">
                                    <div class="col-md-12" style="padding: 20px;margin-bottom: 10px"> {{$userInfo->userInfo->fullname}}</div>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
