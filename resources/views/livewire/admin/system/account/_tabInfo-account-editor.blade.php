<div class="row inner-tab">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.user_code') }}</label>
            <div class="d-flex">
                <input list="staff-list" class="form-control" placeholder="{{ __('data_field_name.system.account.search_user_info') }}" {{ $shouldEditCode ? '' : 'readonly' }} wire:model.debounce.1000ms="searchCode">
                @if ($user->exists)
                    <button class="btn" wire:click="toggleShoudEditCode"><img src="/images/edit.png" alt="edit"></button>
                @endif
                <datalist id="staff-list">
                    @foreach ($searchInfoList as $item)
                        <option value="{{ $item->code }}">
                    @endforeach
                </datalist>
                <input type="hidden" wire:model.lazy="user.user_info_id">
            </div>
            @error('user.user_info_id') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.user_name') }} *</label>
            <input type="text" class="form-control" placeholder="" wire:model.defer="user.username">
            @error('user.username') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.phone') }}</label>
            <input type="text" class="form-control" placeholder="" value="{{ $userInfo->phone ?? '' }}" readonly>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.name') }}</label>
            <input type="text" class="form-control" placeholder="" value="{{ $userInfo->fullname ?? $user->name ?? '' }}" readonly>
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.email') }} *</label>
            <input type="email" class="form-control" placeholder="" value="{{ $userInfo->email ?? '' }}" readonly>
            @error('user.email') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.user_group') }}</label>
            <select class="form-control form-control-lg" wire:model.defer="group_id">
                <option>-- --</option>
                @foreach($groupUserList as $group)
                    <option value="{{$group->id}}">{{$group->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <label>{{ __('data_field_name.system.account.status') }}</label>
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="customSwitch1" value="1" wire:model.lazy="userStatusBool">
                    <label class="custom-control-label" for="customSwitch1">
                        @if ($userStatusBool)
                            {{ __('data_field_name.system.account.active') }}
                        @else
                            {{ __('data_field_name.system.account.not_active') }}
                        @endif
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
