<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('formatHour', function($attribute, $value) {
             return (ctype_digit(substr($value, 0, 2)) && substr($value, 0, 2) <='23' && ctype_digit(substr($value, 3, 4)) && substr($value, 3, 4) <='59');
        });
        Validator::replacer('formatHour', function($message, $attribute) {
            return str_replace(':attribute',$attribute, __('common.validate.hour'));
        });
    }
}
