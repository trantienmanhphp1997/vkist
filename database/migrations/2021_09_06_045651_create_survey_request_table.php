<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_request', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Danh sach yeu cau khao sat');
            $table->string('name', 500)->nullable()->comment('Ten yeu cau khao sat');
            $table->foreignId('field_id')->nullable()->comment('`Map voi master_data, type = 22`')->constrained('master_data');
            $table->tinyInteger('status')->nullable()->comment('Trang thai 1:Chua thuc hien, 2: Dang thuc hien, 3: Hoan thanh');
            $table->dateTime('start_date')->nullable()->comment('Thoi gian bat dau thuc hien khao sat');
            $table->dateTime('end_date')->nullable()->comment('Thoi gian ket thuc khao sat');
            $table->foreignId('user_perform_id')->nullable()->comment('`nguoi thuc hien`')->constrained('user_info');
            $table->string('survey_target', 1500)->nullable()->comment('Muc tieu khao sat');
            $table->string('survey_content', 1500)->nullable()->comment('Noi dung khao sat');
            $table->string('surveyed_unit', 1500)->nullable()->comment('Don vi duoc khao sat');
            $table->tinyInteger('result')->nullable()->comment('Ket qua 1:Khong dat, 2: Dat');
            $table->string('technology_demand', 10)->nullable()->comment('Nhu cau cong nghe');
            $table->string('economic_efficiency', 10)->nullable()->comment('Hieu qua kinh te');
            $table->string('unsign_text', 2000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_request');
    }
}
