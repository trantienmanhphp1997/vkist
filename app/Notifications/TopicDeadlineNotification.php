<?php

namespace App\Notifications;

use App\Enums\ENotificationType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TopicDeadlineNotification extends BaseNotification
{
    public function __construct()
    {
        $this->type = ENotificationType::TOPIC_DEADLINE;
    }

    protected function message($notifiable)
    {
        return [
            'title' => 'Topic Deadline',
            'body' => 'You have 15 days left to submit your topic'
        ];
    }
}
