<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_report', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type')->nullable()->comment('loại báo cáo, fix trong enums');
            $table->tinyInteger('report_period')->nullable()->comment('kỳ báo cáo,theo quý fix trong enums');
            $table->foreignId('department_id')->nullable()->constrained('department')->comment('chọn đơn vị, map vs department');
            $table->date('start_date')->nullable()->comment('từ ngày');
            $table->date('end_date')->nullable()->comment('đến ngày');

            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_report');
    }
}
