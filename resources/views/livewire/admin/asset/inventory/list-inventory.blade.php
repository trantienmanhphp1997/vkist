<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="">{{__('common.listNameFunction.asset')}} </a> \ <span>{{__('data_field_name.asset_inventory.title')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.asset_inventory.title')}}</h3>
                </div>
                <div class="col-md-6">
                    <div class=" float-right">
                        @if($checkCreatePermission)
                            <button class="btn btn-viewmore-news mr0 mrr16" data-toggle="modal" data-target="#createModal1"><img src="/images/plus2.svg" alt="plus"> {{__('common.button.create')}} </button>
                        @endif
                        {{-- <button type="button" class="btn par6 mrr16 btn-header list-active" id='export-inventory' title="print"><img src="/images/Download.svg" alt="download"></button> --}}
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row list-active">
                        <div class="col-md-4">
                            <div  class="form-group search-expertise search-plan ">
                                <div class="search-expertise inline-block">
                                    <input type="text" class="form-control size13" placeholder="{{__('data_field_name.asset_inventory.search')}}" wire:model.debounce.1000ms="searchTerm">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="parent_id" id="exampleFormControlInput4" class="form-control" wire:model.debounce.1000ms="searchDeparkment" style="font-size: 14px;">
                                    <option value="">{{__('common.select-box.choose')}} {{__('data_field_name.asset_inventory.department_execute')}}</option>
                                    @foreach($department as $id=>$name)
                                    <option value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select wire:model.lazy="searchStatus" style="min-width:90%; font-size: 14px;" class="form-control">
                                    <option value="0" selected="selected">{{__('data_field_name.asset_inventory.all')}}</option>
                                    <option value="1">{{__('data_field_name.asset_inventory.unaccomplished')}}</option>
                                    <option value="2">{{__('data_field_name.asset_inventory.accomplished')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div wire:loading class="loader" style="z-index: 3"></div>
                    <div class="table-responsive">
                        <table class="table table-general list-active">
                            <thead>
                                <tr class="border-radius text-uppercase">
                                    <th  scope="col" class="name_w_lg" >{{__('data_field_name.asset_inventory.minutes_name')}}</th>
                                    <th  scope="col" class="text-center">{{__('data_field_name.asset_inventory.perform_date')}}</th>
                                    <th  scope="col" class="text-center">{{__('data_field_name.asset_inventory.units')}}</th>
                                    <th  scope="col" class="text-center">{{__('data_field_name.asset_inventory.status')}}</th>
                                    <th scope="col" class="border-radius-right min-width text-center">{{__('data_field_name.reality_expense.action')}}</th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>

                            <tbody id="dataTable">
                                @forelse($data as $val)
                                <tr >
                                    {{-- <td class="w50">
                                        <label class="box-checkbox checkbox-analysis ">
                                                <input  type="checkbox" value="{{$val->id}}" class="row_id">

                                            <span class="checkmark"></span>
                                        </label>
                                    </td> --}}
                                    <td><a href="{{route('admin.asset.inventory.show',$val->id)}}">{{$val->name}}</a></td>
                                    <td  class="text-center">{{reFormatDate($val->inventory_date,'d/m/Y')}} </td>
                                    <td   class="text-center">{{$val->depart_name}}</td>
                                    <td class="text-center">
                                        <span class="badge badge-fix badge-color-1">{{$val->status==2?__('data_field_name.asset_inventory.accomplished'):__('data_field_name.asset_inventory.unaccomplished')}}</span>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn par6 mrr16 btn-header list-active" id='export-inventory' title="print" wire:click="deleteId({{$val->id}})" data-target="#export-modal" data-toggle="modal"><img src="/images/Download.svg" alt="download"></button>
                                        @if($checkDestroyPermission)
                                        <button title="{{__('common.button.delete')}}" type="button" style="background-color: white; border:none;" wire:click="deleteId({{$val->id}})" data-target="#deleteModal" data-toggle="modal">
                                            <img src="/images/trash.svg" alt="trash">
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-danger p-3" colspan="5">
                                        @if($searchTerm!='')
                                        {{__('common.message.empty_search')}}
                                        @else
                                        {{__('common.message.no_data')}}
                                        @endif
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="list-active">
                        {{$data->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common.modal._modalDelete')
    <div wire:ignore.self class="modal fade" id="EditInventory" tabindex="-1"
    aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('common.confirm_message.confirm_title')}}</h5>
                </div>
                <div class="modal-body">
                    {{__('common.confirm_message.are_you_sure_delete')}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                    data-dismiss="modal">{{__('common.button.back')}}</button>
                    <button type="button"  class="btn btn-danger"
                    data-dismiss="modal" wire:click.prevent="update()">{{__('common.button.delete')}}</button>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.admin.asset.inventory._add-inventory')
    <div wire:ignore.self class="modal fade" id="export-modal" tabindex="-1" aria-labelledby="delete-department-modal-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>
                        <h5 class="modal-title" id="delete-department-modal-label">{{__('notification.member.warning.warning')}}</h5>
                        </strong>
                </div>
                <div class="modal-body">
                    {{__('notification.member.warning.Do you want to export excel file?')}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.back')}}</button>
                    <button type="button" class="btn btn-save" data-dismiss="modal" wire:click="export">{{__('common.button.export_file')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // $('body').on('click','.row_id', function () {
    //     var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
    //     var count = $checkedBoxes.length;
    //     if (count === 1) {
    //         var checkbox = document.getElementsByClassName('row_id');
    //         var checked = [];
    //         for (var i = 0; i < checkbox.length; i++) {
    //             if (checkbox[i].checked === true) {
    //                 checked.push(checkbox[i].value);
    //             }
    //         }
    //     }
    // });

    $("#select_all").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('#select_all');
    });


    $(".row_id").change(function(){
        var $uncheckedBoxes = $('#dataTable input[type=checkbox]:not(:checked)').not('#select_all');
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('#select_all');
        if($uncheckedBoxes.length != 0){
            $("#select_all").prop('checked', false);
        }
        else {
            $("#select_all").prop('checked', true);
        }
    });

    $('#delete-inventory').on('click', function () {
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
        var count = $checkedBoxes.length;
        if (count === 1) {
            var checkbox = document.getElementsByClassName('row_id');
            var checked = [];
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked === true) {
                    checked.push(checkbox[i].value);
                }
            }
            window.livewire.emit('deleteId', checked[0]);
            $('.row_id').prop('checked', false);
            $('#deleteModal').modal('show');

        }
    });

    // $('#export-inventory').on('click', function () {
    //     var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
    //     var count = $checkedBoxes.length;
    //     if (count === 1) {
    //         var checkbox = document.getElementsByClassName('row_id');
    //         var checked = [];
    //         for (var i = 0; i < checkbox.length; i++) {
    //             if (checkbox[i].checked === true) {
    //                 checked.push(checkbox[i].value);
    //             }
    //         }
    //         window.livewire.emit('deleteId', checked[0]);
    //         $('.row_id').prop('checked', false);
    //         $('#export-modal').modal('show');
    //     }
    // });

    $('#edit-inventory').on('click', function () {
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
        var count = $checkedBoxes.length;
        if (count === 1) {
            var checkbox = document.getElementsByClassName('row_id');
            var checked = [];
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked === true) {
                    checked.push(checkbox[i].value);
                }
            }
            window.livewire.emit('deleteId', checked[0]);
            $('.row_id').prop('checked', false);
            $('#EditInventory').modal('show');
        }
    });

    $('#detail-inventory').on('click', function () {
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
        var count = $checkedBoxes.length;
        if (count === 1) {
            var checkbox = document.getElementsByClassName('row_id');
            var checked = [];
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked === true) {
                    checked.push(checkbox[i].value);
                }
            }
            var url = "inventory/show/" + checked[0];
            window.location.href = url;
        }

    });

    $('#createModal1').on('hidden.bs.modal', function () {
        window.livewire.emit('resetInput');
    });

</script>
