<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col_md_10 col-xl-10 box-user py-0 px-15 rounded-0 bg-white">
            <div class="row">
                <div class="col-md-12 px-0">
                    <div class="breadcrumbs"><a href="">{{ __('data_field_name.research_plan.research_project_control') }} </a> \
                        <span>{{ __('data_field_name.research_plan.setup_plan') }}</span>
                    </div>
                </div>
            </div>
            <div class="row bd-border">
                <div class="col-md-12">
                    <h3 class="head-8 text-1">{{ __('data_field_name.research_plan.setup_plan') }}</h3>
                </div>
                <div class="col-md-12">
                    <div class="bg-white bd-rds-12 px-24 py-24">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="py-12 px-24 bg-3 body-4 text-3 bd-rds-12 mb-24">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="py-8 m-0">
                                                {{ __('data_field_name.research_plan.plan_name') }}</p>
                                        </div>
                                        <div class="col-md-9">
                                            <p class="py-8 m-0 text-5" style="color:red">:&nbsp;
                                                {{ $researchPlan->name }}</p>
                                        </div>
                                        <div class="col-md-3 ">
                                            <p class="py-8 m-0">
                                                {{ __('data_field_name.research_plan.perform_time') }}</p>
                                        </div>
                                        <div class="col-md-9">
                                            <p class="py-8 m-0 text-5" style="color:blue">:&nbsp;
                                                {{ __('data_field_name.research_plan.from') }}
                                                {{ reFormatDate($researchPlan->topic->start_date ?? '') }}
                                                {{ __('data_field_name.research_plan.to') }}
                                                {{ reFormatDate($researchPlan->topic->end_date ?? '') }}</p>
                                        </div>

                                        <div class="col-md-3">
                                            <p class="py-8 m-0">
                                                {{ __('data_field_name.research_plan.topic_name') }}</p>
                                        </div>
                                        <div class="col-md-9">
                                            <p class="py-8 m-0 text-5" style="color:red">:&nbsp;
                                                {{ $researchPlan->topic->name }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="misson-list w-100">
                                    <div class="misson-list-title text-2 body-3 mb-12">
                                        <p class="m-0">
                                            {{ __('data_field_name.research_plan.mission_list') }}</p>
                                    </div>
                                    <div class="wrap-list-items" id='table-scroll'>
                                        <!-- Mission item -->
                                        <?php
                                        $i = 0;
                                        ?>
                                        @foreach ($parentTasks as $row)
                                            <div class="card mission-item p-0 mb-12">
                                                <div class="card-header  px-24 py-12 bd-rds-12 border-0 d-flex flex-nowrap">
                                                    <div class="mission-item-num mr-8">
                                                        <span class="text-2 body-3">{{ ++$i }}.</span>
                                                    </div>
                                                    <div class="mission-item-name mr-8">
                                                        @if ($researchPlan->status == \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT)
                                                            <p class="text-2 body-3"><a href="" wire:click="editMainMission({{ $row->id }})" data-toggle="modal" data-target="#target">{{ stringLimit($row->name, 20) }}</a>
                                                            </p>
                                                        @else
                                                            <p class="text-2 body-3">
                                                                <a>{{ stringLimit($row->name, 20) }}</a>
                                                            </p>
                                                        @endif
                                                        <span class="text-3 button-1">{{ reFormatDate($row->start_date) }}
                                                            {{ __('data_field_name.research_plan.to') }}
                                                            {{ reFormatDate($row->end_date) }}</span> <br>
                                                        <span class="text-3 button-1">{{ $row->userInfos->pluck('fullname')->join(', ') }}</span>
                                                    </div>
                                                    <img src="/images/Angle-down.svg" alt="" class="ml-auto" data-toggle="collapse" data-target="#listmission{{ $row->id }}">
                                                </div>
                                            </div>
                                            <button style="float: right;" type="button" @if ($researchPlan->status != \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT || $researchPlan->status == \App\Enums\EResearchPlan::STATUS_INVALID_PROFILE || $researchPlan->status == \App\Enums\EResearchPlan::STATUS_REJECT)
                                                hidden
                                        @endif
                                        wire:click="edit({{ $row->id }})"
                                        data-toggle="modal"
                                        data-target="#target2"
                                        class="btn btn-fix btn-secondary-1 py-12 px-16">
                                        <img src="/images/addblue.svg" alt="" class="mr-8">{{ __('data_field_name.research_plan.create_mission') }}</button>
                                        <br><br><br>
                                        <div id="listmission{{ $row->id }}" class="table-sub-scroll">
                                            @foreach ($row->childrenTask as $row2)

                                                <ul class="list-group rounded-0 button-1 text-2 mission-child-list">
                                                    <li class="list-group-item py-8 pl-32 pr-0 d-flex flex-nowrap py-12">
                                                        @if ($researchPlan->status == \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT)
                                                            <span class="mr-16 py-8"><a href="" wire:click="editChildrenMission({{ $row2->id }})" data-toggle="modal" data-target="#target2">{{ $row2->name }}---</a></span>
                                                        @else
                                                            <span class="mr-16 py-8"><a>{{ $row2->name }}---</a></span>
                                                        @endif
                                                        <span class="mr-16 py-8">{{ $row2->userInfos->pluck('fullname')->join(', ') }}</span>
                                                        <span class="mr-16 py-8 text-3 ml-auto">Từ
                                                            {{ reFormatDate($row2->start_date) }}
                                                            {{ __('data_field_name.research_plan.to') }}
                                                            {{ reFormatDate($row2->end_date) }}</span>
                                                        @if ($researchPlan->status == \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT)
                                                            <a href="" data-toggle="modal" data-target="#deleteModal" wire:click="deleteId({{ $row2->id }})"><img src="/images/close.svg" alt="" class="ml-auto"></a>
                                                        @endif
                                                    </li>
                                                </ul>
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-xl-2 sidebar-right pt-36">
            <div class="text-2 body-3 mb-12 d-flex align-items-center">
                <p class="m-0">{{ __('data_field_name.research_plan.main_mission') }}</p>
                @if ($researchPlan->status == \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT)
                    <button type="button" id="show_create_modal_main_mission" wire:click="resetInputFields" data-toggle="modal" data-target="#target" class="btn btn-fix btn-secondary-1 py-12 px-12 ml-auto"><img src="/images/addblue.svg" alt=""></button>
                @endif
            </div>
            <div class="wrap-list-items button-1 text-2">
                @php $i = 0; @endphp
                @foreach ($parentTasks as $row)
                    <div class="card main-mission-item p-0 mb-12 border-0">
                        <a class="nav navbar-nav navbar-right">
                            <div class="card-header px-12 py-12 bd-rds-12 border-0 position-relative">
                                <div class="d-flex text-5 size13 mr-20">
                                    <span class="pr-8">{{ ++$i }}.</span>
                                    <span>{{ $row->name }}</span>
                                </div>
                                @if ($researchPlan->status == \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT)
                                    <a href="" style="position:absolute;right: 11px;top:6px;" data-toggle="modal" data-target="#deleteModal" wire:click="deleteId({{ $row->id }})"><img src="/images/close.svg" alt="close"></a>
                                @endif
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-12 pt-24">
            <div class="group-btn2 text-center">
                <button type="" class="btn btn-cancel" data-toggle="modal" data-target="#noidungkehoach" name="cancel">{{ __('data_field_name.research_plan.review') }}</button>
                @if ($researchPlan->status == \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT)
                    <button type="submit" wire:click="submitted" class="btn btn-save" name="save">{{ __('data_field_name.research_plan.submit_plan') }}</button>
                @endif
            </div>
        </div>
        <!-- Modal Create Main Mission -->
        <div wire:ignore.self class="modal fade" id="target" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <h4 class="modal-title text-center" id="titleParent">
                            @if ($isEdit == true)
                                {{ __('data_field_name.research_plan.update_mission') }}
                            @else {{ __('data_field_name.research_plan.create_mission') }}
                            @endif
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="resetInputFields">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>

                            <div class="form-group">
                                <label for="exampleFormControlInput4">{{ __('data_field_name.research_plan.topic') }}<span class="text-danger">(*)</label>
                                <input type="text" name="topic_id" disabled id="exampleFormControlInput4" class="form-control" value="{{ $researchPlan->topic->name }}">
                                @error('selectedTopicId') <span class="text-danger error">{{ $message }}</span>@enderror

                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput4">Thời gian thực hiện :
                                    {{ reFormatDate($researchPlan->topic->start_date ?? '') }} đến
                                    {{ reFormatDate($researchPlan->topic->end_date ?? '') }}
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput4">{{ __('data_field_name.research_plan.text_task') }}</label>
                                <select name="type" id="exampleFormControlInput4" class="form-control">
                                    <option value="0" selected>{{ __('data_field_name.research_plan.main_mission') }}
                                    </option>
                                    <option value="1" disabled>
                                        {{ __('data_field_name.research_plan.children_mission') }}</option>
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{ __('data_field_name.research_plan.mission_name') }}<span class="text-danger">(*)</span></label>
                                <input type="text" wire:model.lazy="name" class="form-control" id="exampleFormControlInput1" placeholder="{{ __('data_field_name.research_plan.task_name') }}">
                                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="exampleFormControlInput1">{{ __('data_field_name.research_plan.start_time') }}<span class="text-danger">(*)</label>
                                    @include('layouts.partials.input._inputDate', ['input_id' => 'start_date',
                                    'place_holder'=>'','default_date' => $start_date, 'min_date' =>
                                    $researchPlan->topic->start_date,'max_date'=>$researchPlan->topic->end_date ])
                                    @error('start_date') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleFormControlInput1">{{ __('data_field_name.research_plan.end_time') }}<span class="text-danger">(*)</label>
                                    @include('layouts.partials.input._inputDate', ['input_id' => 'end_date',
                                    'place_holder'=>'','default_date' =>
                                    $end_date,'max_date'=>$researchPlan->topic->end_date,'min_date'=>$researchPlan->topic->start_date
                                    ])
                                    @error('end_date') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput4">{{ __('data_field_name.research_plan.created_at') }}<span class="text-danger">(*)</span></label>
                                @livewire('component.advanced-select', [
                                    'name' => 'user-infos',
                                    'modelName' => \App\Models\UserInf::class,
                                    'searchBy' => ['fullname', 'code'],
                                    'columns' => ['fullname'],
                                    'multiple' => true,
                                    'conditions' => [
                                        ['id', 'in', $members->pluck('user_info_id')->all()]
                                    ],
                                    'selectedIds' => $userInfoIds
                                ], key($advancedSelectKey . '-1'))
                                @error('userInfoIds') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="close-modal-create-parent_mission" class="btn btn-secondary close-btn" data-dismiss="modal" wire:click="resetInputFields">{{ __('common.button.close') }}</button>
                        <button type="submit" onclick="$('#start_date').val('');$('#end_date').val('')" wire:click="store_research_mission" id="btn_save_research_plan" class="btn btn-primary close-modal">{{ __('common.button.save') }}</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Modal Create Main Mission -->
        <!-- Modal Create Children Mission -->

        <div wire:ignore.self class="modal fade" id="target2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body box-user">
                        <div class="modal-header">
                            <h4 class="modal-title" id="titleChildren">
                                @if ($isEditChildren == true)
                                    {{ __('data_field_name.research_plan.update_children') }}
                                @else {{ __('data_field_name.research_plan.create_children') }}
                                @endif
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="resetInputFields">
                                <span aria-hidden="true close-btn">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="col-12 col-p12">
                                    <div class="bg-3 bd-rds-12 body-5 text-3 py-12 px-20 mb-24">
                                        <div class="main-mission-item mb-8">
                                            <div class="row row-m2">
                                                <div class="col-5 col-p2">
                                                    <span>{{ __('data_field_name.research_plan.main_mission') }}</span>
                                                </div>
                                                <div class="col-7 col-p2">
                                                    <span class="text-2">: {{ $currentParentTask->name ?? '' }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="main-mission-item mb-8">
                                            <div class="row row-m2">
                                                <div class="col-5 col-p2">
                                                    <span>{{ __('data_field_name.research_plan.main_responsible') }}</span>
                                                </div>
                                                <div class="col-7 col-p2">
                                                    <span class="text-2">: {{ $currentParentTask->userInfo->fullname ?? '' }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="main-mission-item">
                                            <div class="row row-m2">
                                                <div class="col-5 col-p2">
                                                    <span>{{ __('data_field_name.research_plan.perform_time') }}</span>
                                                </div>
                                                <div class="col-7 col-p2">
                                                    <span class="text-2">:
                                                        {{ __('data_field_name.research_plan.from') }}
                                                        {{ reFormatDate($currentParentTask->start_date ?? '') }}
                                                        {{ __('data_field_name.research_plan.to') }}
                                                        {{ reFormatDate($currentParentTask->end_date ?? '') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput4">{{ __('data_field_name.research_plan.topic_name') }}</label>
                                    <input type="text" name="topic_id" disabled id="exampleFormControlInput4" class="form-control" value="{{ $researchPlan->topic->name }}">
                                    @error('selectedTopicId') <span class="

                                         text-danger error">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">{{ __('data_field_name.research_plan.mission_name') }}<span class="text-danger">(*)</span></label>
                                    <input type="text" wire:model.lazy="name" class="form-control" id="exampleFormControlInput1" placeholder="{{ __('data_field_name.research_plan.task_name') }}">
                                    @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                                <input type="hidden" wire:model.lazy="parent_id">
                                <div class="form-group">
                                    <label for="exampleFormControlInput4">{{ __('data_field_name.research_plan.text_task') }}</label>
                                    <select name="type" id="exampleFormControlInput4" class="form-control">
                                        <option value="0" disabled>
                                            {{ __('data_field_name.research_plan.main_mission') }}</option>
                                        <option value="1" selected>
                                            {{ __('data_field_name.research_plan.children_mission') }}</option>
                                    </select>

                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6" wire:key="parentTask-{{ $currentParentTask->id ?? '0' }}-1">
                                        <label for="exampleFormControlInput1">{{ __('data_field_name.research_plan.start_time') }}
                                            <span class="text-danger">(*)</span></label>
                                        @if (!empty($currentParentTask))
                                            @include('layouts.partials.input._inputDate', [
                                                'input_id' => 'start_date_2',
                                                'place_holder' => '',
                                                'default_date' => $start_date_2,
                                                'min_date' => $currentParentTask->start_date ?? '',
                                                'max_date' => $currentParentTask->end_date ?? ''
                                            ])
                                        @endif
                                        @error('start_date_2') <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-md-6" wire:key="parentTask-{{ $currentParentTask->id ?? '0' }}-2">
                                        <label for="exampleFormControlInput1">{{ __('data_field_name.research_plan.end_time') }}<span class="text-danger">(*)</span></label>
                                        @if (!empty($currentParentTask))
                                            @include('layouts.partials.input._inputDate', [
                                                'input_id' => 'end_date_2',
                                                'place_holder' => '',
                                                'default_date' => $end_date_2,
                                                'max_date' => $currentParentTask->end_date ?? '',
                                                'min_date' => $currentParentTask->start_date ?? ''
                                            ])
                                        @endif

                                        @error('end_date_2') <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput4">{{ __('data_field_name.research_plan.perform_human') }}<span class="text-danger">(*)</span></label>
                                    @livewire('component.advanced-select', [
                                        'name' => 'user-infos',
                                        'modelName' => \App\Models\UserInf::class,
                                        'searchBy' => ['fullname', 'code'],
                                        'columns' => ['fullname'],
                                        'multiple' => true,
                                        'conditions' => [
                                            ['id', 'in', $members->pluck('user_info_id')->all()]
                                        ],
                                        'selectedIds' => $userInfoIds
                                    ], key($advancedSelectKey . '-2'))
                                    @error('userInfoIds') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="group-btn2 text-center  pt-12">
                        <button type="button" id="close-modal-create-children_mission" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.close') }}</button>
                        <button type="submit" onclick="$('#start_date_2').val('');$('#end_date_2').val('')" id="btn_save_research_plan_children" wire:click="store_research_children_mission" class="btn btn-save">{{ __('common.button.save') }}</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Modal Create Children Mission -->
        <!-- Modal Delete -->
        <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body box-user">
                        <h4 class="modal-title" id="exampleModalLabel">
                            {{ __('notification.member.warning.warning') }}</h4>
                        {{ __('notification.member.warning.warning_mission') }}
                    </div>
                    <div class="group-btn2 text-center  pt-12">
                        <button type="button" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.back') }}</button>
                        <button type="button" class="btn btn-save" data-dismiss="modal" wire:click.prevent="delete()">{{ __('common.button.delete') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal Delete -->
        <!-- Modal Export -->
        <form wire:submit.prevent="submit">
            <div wire:ignore.self class="modal fade modal-fix" id="noidungkehoach" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header mb-32">
                            <div class="title-modal">
                                <h6 class="m-0">{{ __('data_field_name.research_plan.plan') }}</h6>
                                <p class="text-3 body-4 m-0">{{ $researchPlan->name }}</p>
                                <p class="text-3 body-4 m-0">{{ $researchPlan->topic->name ?? '' }}</p>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row justify-content-md-center">
                                    <div class="col-12 pd-col mb-24">
                                        <div class="w-100">
                                            <p class="mb-8 body-3 text-1">1.
                                                {{ __('data_field_name.research_plan.target') }}</p>
                                            <p class="m-0 body-4 text-2 font-weight-normal">
                                                {{ $researchPlan->topic->target ?? '' }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 pd-col mb-24">
                                        <div class="w-100">
                                            <p class="mb-8 body-3 text-1">2.
                                                {{ __('data_field_name.research_plan.topic_content') }}</p>
                                            <p class="m-0 body-4 text-2 font-weight-normal">
                                                {{ $researchPlan->topic->overview ?? '' }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 pd-col">
                                        <div class="w-100 mb-8">
                                            <p class="m-0 body-3 text-1">3.
                                                {{ __('data_field_name.research_plan.plan_content') }}</p>
                                        </div>
                                        <div class="info-asset bg-white p-0 mb-24">
                                            <table class="table table-general">
                                                <thead>
                                                    <tr class="border-radius">
                                                        <th scope="col colUser">
                                                            {{ __('data_field_name.research_plan.stt') }}</th>
                                                        <th scope="col colUser">
                                                            {{ __('data_field_name.research_plan.content') }}</th>
                                                        <th scope="col colUser">
                                                            {{ __('data_field_name.research_plan.timed') }}</th>
                                                        <th scope="col colUser">
                                                            {{ __('data_field_name.research_plan.responsible') }}
                                                        </th>
                                                        <th scope="col colUser">
                                                            {{ __('data_field_name.research_plan.department') }}
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($parentTasks as $row)
                                                        <tr>
                                                            <td class="pt-16 pb-8">
                                                                <strong>{{ $row->name }}</strong>
                                                            </td>
                                                            <td class="pt-16 pb-8"></td>
                                                            <td class="pt-16 pb-8">
                                                                <strong>{{ __('data_field_name.research_plan.from') }}
                                                                    {{ reFormatDate($researchPlan->topic->start_date ?? '') }}
                                                                    {{ __('data_field_name.research_plan.to') }}
                                                                    {{ reFormatDate($researchPlan->topic->end_date ?? '') }}</strong>
                                                            </td>
                                                            <td class="pt-16 pb-8">
                                                                <strong>{{ $row->userInfo->fullname ?? '' }}</strong>
                                                            </td>
                                                            <td class="pt-16 pb-8"></td>
                                                        </tr>
                                                        @php $i = 0; @endphp
                                                        @foreach ($row->childrenTask as $row2)
                                                            <tr>
                                                                <td class="pt-16 pb-8">{{ ++$i }}</td>
                                                                <td class="pt-16 pb-8">{{ $row2->name }}</td>
                                                                <td class="pt-16 pb-8">
                                                                    {{ __('data_field_name.research_plan.from') }}
                                                                    {{ reFormatDate($row2->start_date) }}
                                                                    {{ __('data_field_name.research_plan.to') }}
                                                                    {{ reFormatDate($row2->end_date) }}
                                                                </td>
                                                                <td class="pt-16 pb-8">
                                                                    {{ $row2->userInfo->fullname ?? '' }}</td>
                                                                <td class="pt-16 pb-8">
                                                                    {{ $row2->userInfo->department->name ?? '' }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-12 pd-col mb-24">
                                        <div class="w-100">
                                            <p class="mb-8 body-3 text-1">4.
                                                {{ __('data_field_name.research_plan.achievement_textbook_check') }}
                                            </p>
                                            <p class="m-0 body-4 text-2 font-weight-normal">
                                                {{ $researchPlan->topic->achievement_textbook_check ?? '' }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 pd-col mb-8">
                                        <div class="w-100">
                                            <p class="mb-8 body-3 text-1">5.
                                                {{ __('data_field_name.research_plan.other_action') }}</p>
                                            <p class="m-0 body-4 text-2 font-weight-normal">
                                                Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
                                                sint. Velit officia consequat duis enim velit mollit. Exercitation
                                                veniam consequat sunt nostrud amet.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="group-btn2 text-center  pt-12">
                                <button type="button" id="close-modal-create-research-plan" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.close') }}</button>
                                {{-- @include('livewire.common.buttons._submit') --}}
                                <!-- <div class="col-3 col-p8">
                                        <a wire:click="export" class="btn btn-fix h-52  text-4 float-right">Export <img class="mr-1 w-24" src="../images/Export.svg" alt=""></a>
                                    </div> -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- End Modal Export -->
    </div>
</div>
<script type=text/javascript>
    $("document").ready(function() {
        window.livewire.on('close-modal-create-parent_mission', () => {
            document.getElementById('close-modal-create-parent_mission').click();
        });
    });
    $("document").ready(function() {
        window.livewire.on('close-modal-create-children_mission', () => {
            document.getElementById('close-modal-create-children_mission').click()
        });
    });

    window.addEventListener('render-plan-info-event', function() {
        initDatePicker('#start_date_2'); // hàm initDatePicker jup xóa kendodatepicker và khởi tạo lại 1 lần nữa
        initDatePicker('#end_date_2');
    });
</script>
