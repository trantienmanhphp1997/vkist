<?php

namespace App\Http\Livewire\Admin\Executive\UserInfoLeave;

use App\Http\Livewire\Base\BaseLive;
use App\Models\UserInfoLeave;


class UserInfoLeaveList extends BaseLive
{
    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render()
    {
        $query = UserInfoLeave::query();
        $searchTerm = $this->searchTerm;

        $query->leftJoin('user_info as userinf', "userinf.id", '=', 'user_info_leave.user_info_id');

        if (!empty($searchTerm)) {
            $query->where(function ($q) use ($searchTerm) {
                $q->where('userinf.fullname', 'like', '%' . $searchTerm . '%')
                ->orWhere('userinf.code', 'like', '%' . $searchTerm . '%');
            });
        }
        $query->select('user_info_leave.*', 'userinf.code', 'userinf.phone', 'userinf.fullname', 'userinf.department_id', 'userinf.position_id');
        $query->with('department')->with('position');

        $data = $query->orderBy('user_info_leave.id', 'DESC')->paginate($this->perPage);
        return view('livewire.admin.executive.user-info-leave.user-info-leave-list', ['data' => $data]);
    }

    public function delete()
    {
        $userInfoLeave = UserInfoLeave::findOrFail($this->deleteId);
        $userInfoLeave->delete();
    }
    
}
