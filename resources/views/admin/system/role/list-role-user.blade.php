@extends('layouts.master')
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs">
                    <a href="{{route('admin.system.role.index')}}">{{__('data_field_name.system.role.text_role_management')}} </a> \ <span>{{__('data_field_name.system.role.text_info_role')}}</span>
                </div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="detail-task information box-idea">
                    <h4>{{__('data_field_name.system.role.text_info_role')}}</h4>
                    <div class="group-tabs">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link" id="nav-home-tab" href="{{route('admin.system.role.edit', $role->id)}}">
                                    <span>{{__('data_field_name.common_field.general_information')}}</span>
                                </a>
                                <a class="nav-item nav-link active" id="nav-profile-tab">
                                    <span>{{__('data_field_name.system.account.user_list')}}</span>
                                </a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <form method="POST" action="{{route('admin.system.role.addroleuser', $role->id)}}">
                                    @csrf
                                    {{ method_field('POST') }}
                                    <div class="inner-tab">
                                        @livewire('admin.system.role.user-list', ['roleId' => $role->id])
                                        <div class="col-md-12 ">
                                            <div class="group-btn2 text-center group-btn ">
                                                <a href="{{route('admin.system.role.index')}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                                                <button type="submit" class="btn btn-save">{{__('common.button.save')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
