<?php


namespace App\Enums;


class EAssetTimeRepeat {
    const MONTH = 1;
    const QUARTER = 2;
    const SIX_MONTH = 3;
    const YEAR = 4;
}

