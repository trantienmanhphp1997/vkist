<div class="col-md-10 col-xl-11 box-detail box-user list-role">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{ __('data_field_name.approval_topic.list_appraisal') }} </a> \ <span>{{ __('data_field_name.approval_topic.list_appraisal') }}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{ __('data_field_name.approval_topic.list_appraisal') }}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="row mb-24">
                                <div class="col-md-4 px-8">
                                    <div class="search-expertise">
                                        <input wire:model.debounce.1000ms="searchTerm" type="text" class="form-control size13 w-xs-100" placeholder="{{ __('data_field_name.approval_topic.text_search_name_topic_appraisal') }}">
                                        <span><img src="../images/Search.svg" alt="search"></span>
                                    </div>
                                </div>
                                <div class="col-md-2 px-8">
                                    <select class="form-control form-control-lg size13" wire:model.lazy="searchType">
                                        <option value="">{{ __('data_field_name.approval_topic.profile_type') }}</option>
                                        @foreach ($appraisalType as $key => $type)
                                            <option value='{!! $key !!}'>{!! $type !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 search-date-css px-8" wire:ignore>
                                    <input type="text" class="form-control input-date-kendo size13" placeholder="{{ __('data_field_name.approval_topic.appraisal_date') }}" onchange="@this.set('searchAppraisalDate', this.value)" value="{{ $searchAppraisalDate }}">
                                </div>
                                <div class="col-md-3 px-8">
                                    <select class="form-control form-control-lg size13" wire:model.lazy="searchStatus">
                                        <option value="">{{ __('data_field_name.common_field.status') }}</option>
                                        @foreach ($statusAppraisal as $key => $appraisal)
                                            <option value='{!! $key !!}'>{!! $appraisal !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 " wire:ignore>
                            <div class="form-group float-right">
                                @if ($checkCreatePermission)
                                    <button class="btn btn-viewmore-news mr0 buttonCreate" data-toggle="modal" onclick="appraisalEditor(null)">
                                        <img src="../images/plus2.svg" alt="plus"> {{ __('data_field_name.system.role.create') }}
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                                <tr class="border-radius">
                                    <th scope="col" class="border-radius-left text-center">{{ __('data_field_name.approval_topic.profile_code') }}</th>
                                    <th scope="col" class="text-center">{{ __('data_field_name.approval_topic.appraisal_name') }}</th>
                                    <th scope="col" class="text-center">{{ __('data_field_name.approval_topic.profile_type') }}</th>
                                    <th scope="col" class="text-center">{{ __('data_field_name.approval_topic.appraisal_date') }}</th>
                                    <th scope="col" class="text-center">{{ __('data_field_name.common_field.status') }}</th>
                                    <th scope="col" class="border-radius-right text-center">{{ __('data_field_name.common_field.action') }}</th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                                @foreach ($data as $row)
                                    @php $id = $row->id; @endphp
                                    <tr>
                                        <td class="text-center">
                                            @if ($checkEditPermission)
                                                <a href="javascript:void(0)" class="btn-edit-film editAppraisalBoard-{{ $id }}" onclick="appraisalDetail({{ $row->id }})">{!! $row->approval->code ?? __('data_field_name.approval_topic.text_code_not') !!}</a>
                                            @else
                                                {!! __('data_field_name.approval_topic.text_code_not') !!}
                                            @endif
                                        </td>
                                        <td class="text-center">{!! boldTextSearchV2(htmlspecialchars($row->name), $searchTerm) ?? '' !!}</td>
                                        <td class="text-center">{{ !empty($row->type) ? $appraisalType[$row->type] : '' }}</td>
                                        <td class="text-center">{!! reFormatDate($row->appraisal_date) !!}</td>
                                        <td class="text-center">{!! $statusAppraisal[$row->status] ?? '' !!}</td>
                                        <td class="text-center" wire:ignore>
                                            @if ($checkEditPermission)
                                                <button title="{{__('common.button.edit')}}" wire:loading.attr="disabled" class="btn-edit-film btn editAppraisalBoard-{{ $id }}" onclick="appraisalEditor({{ $row->id }})">
                                                    <img src="/images/pent2.svg" alt="pent">
                                                </button>
                                            @endif
                                            @include('livewire.common.buttons._delete')
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span >{{__('common.message.no_data')}}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show modal-fix" id="appraisal-modal" tabindex="-1" aria-labelledby="editAppraisal" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                @livewire('admin.appraisal.appraisal-editor', ['appraisalBoardId' => $currentAppraisalBoardId, 'editable' => !$showOnly], key(Str::random(5)))
            </div>
        </div>
    </div>

    <script>

        function appraisalDetail(id) {
            @this.showOnly = true;
            @this.currentAppraisalBoardId = id;
        }

        function appraisalEditor(id) {
            @this.showOnly = false;
            @this.currentAppraisalBoardId = id;
        }

        window.addEventListener('close-appraisal-modal-event', function() {
            $('.modal').modal('hide');
            // @this.set('currentAppraisalBoardId', null);
            @this.render();
        });

        window.addEventListener('open-appraisal-modal-event', function() {
            $('#appraisal-modal').modal('show');
            initDatePicker('#appraisal-date');
        });

    </script>

</div>
