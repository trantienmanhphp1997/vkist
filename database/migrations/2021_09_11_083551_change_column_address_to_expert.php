<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnAddressToExpert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expert', function (Blueprint $table) {
            $table->dropColumn('address');
        });
        Schema::table('expert', function (Blueprint $table) {
            $table->string('address', 500)->nullable()->comment('Dia chi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expert', function (Blueprint $table) {
            $table->dropColumn('address');
        });

        Schema::table('expert', function (Blueprint $table) {
            $table->string('address', 500)->comment('Dia chi');
        });
    }
}
