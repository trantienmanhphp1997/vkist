<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecisionReward extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decision_reward', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Danh sach quyet dinh khen thuong');
            $table->string('name', 500)->nullable()->comment('Quyet dinh khen thuong');
            $table->dateTime('reward_date')->nullable()->comment('Ngay khen thuong');
            $table->string('type_reward', 255)->nullable()->comment('Loai khen thuong');
            $table->string('number_decision', 255)->nullable()->comment('So quyet dinh');
            $table->dateTime('decision_date')->nullable()->comment('Ngay quyet dinh');
            $table->foreignId('user_decide_id')->nullable()->comment('`nguoi quyet dinh`')->constrained('user_info');
            $table->tinyInteger('type_of_prize')->comment('Hinh thuc khen thuong. 1 : thuong tien, 2: bieu duong, 3: tang hien vat');
            $table->bigInteger('total_value')->nullable()->comment('Tong gia tri');
            $table->tinyInteger('type_of_receiver')->comment('Doi tuong khen thuong. 1 : ca nhan, 2: to chuc');
            $table->string('reason', 255)->nullable()->comment('Ly do');
            $table->string('base', 255)->nullable()->comment('Can cu');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decision_reward');
    }
}
