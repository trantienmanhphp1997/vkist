<?php

namespace App\Notifications;

use App\Enums\ENotificationType;
use App\Models\UserInf;

class NewUserInfoNotification extends BaseNotification
{
    public UserInf $userInfo;
    public function __construct(UserInf $userInfo)
    {
        $this->type = ENotificationType::NEW_USER_INFO;
        $this->userInfo = $userInfo;
    }

    protected function message($notifiable)
    {
        return [
            'template_name' => 'notification.user.new_user_info',
            'user' => [
                'id' => $this->userInfo->id,
                'name' => $this->userInfo->fullname
            ]
        ];
    }
}
