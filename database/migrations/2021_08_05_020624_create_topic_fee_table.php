<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic_fee', function (Blueprint $table) {
            $table->id()->comment('Danh sach du toan');
            $table->string('name', 500)->nullable()->comment('Ten du toan');
            $table->tinyInteger('source_capital')->comment('Nguon von');
            $table->tinyInteger('status')->comment('Trang thai du toan');
            $table->unsignedBigInteger('topic_id')->nullable()->comment('Map voi topic');
            $table->foreign('topic_id')->references('id')->on('topic')->comment('Map voi topic');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimate_topic');
    }
}
