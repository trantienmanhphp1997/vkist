<div class="col-md-10 col-xl-11 box-detail box-user">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.asset.list_title')}} </a>
                    \ <span>{{__('data_field_name.asset.add_btn')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="title">{{__('data_field_name.asset.add_btn')}}</h3>
                    </div>
                </div>
                <div class="information information-asset">
                    <div class="inner-tab pt0">
                        <form wire:submit.prevent="submit">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class=" menu-asset nav" role="tablist">
                                        <li role="presentation" wire:ignore>
                                            <a href="#profile" class="active" id="tab-info" aria-controls="profile" role="tab" data-toggle="tab">
                                                {{__('data_field_name.asset.info_general')}}
                                            </a>
                                        </li>
                                        <li role="presentation" wire:ignore>
                                            <a href="#insurance" aria-controls="insurance"  id="tab-warranty" role="tab" data-toggle="tab">
                                                {{__('data_field_name.asset.info_insurance')}}
                                            </a>
                                        </li>
                                        <li role="presentation" wire:ignore>
                                            <a href="#distribute" aria-controls="distribute" role="tab" data-toggle="tab">
                                                {{__('data_field_name.asset.info_distribute')}}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9 tab-content" wire:ignore>
                                    @livewire('admin.asset.asset-add.tab-info')
                                    @livewire('admin.asset.asset-add.tab-warranty')
                                    @livewire('admin.asset.asset-add.tab-distribute')
                                </div>
                                <div class="col-md-12">
                                    <div class="group-btn2 text-center">
                                        <button type="submit" class="btn btn-cancel" wire:click.prevent="cancel()">
                                            {{__('common.button.cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-save" id="btn-save"   wire:click.prevent="save('add')">
                                            {{__('common.button.perform')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <script>
                            $(document).ready(function () {
                                window.livewire.on('click-tab-info', () => {
                                    document.getElementById('tab-info').click();
                                });
                                window.livewire.on('click-tab-warranty', () => {
                                    document.getElementById('tab-warranty').click();
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


