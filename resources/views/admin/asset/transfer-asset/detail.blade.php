@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.allocated-revoke.detail')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    @livewire('admin.asset.asset-transfer.detail', ['allocatedId' => $id])
@endsection
