<?php
return [
    'breadcrumbs' => [
        'research_project_management' => 'Research project management',
        'research_statistic' => 'Research statistic',
    ],
    'title' => [
        'general' => 'General',
        'project_statistic_by_status' => 'Project statistic by status',
        'task_statistic_by_status' => 'Task statistic by status',
        'ideal_statistic_by_status' => 'Ideal statistic by status',
        'task_statistic' => 'Task statistic',
        'task_in_progress' => 'Task in progress',
        'total_task' => 'Total task',
        'total_ideal' => 'Total ideal',
        'total_project' => 'Total project',
        'project_overview' => [
            'project_overview' => 'Project overview',
            'done' => 'Done',
            'in-progress' => 'In progress',
            'out_of_date' => 'Out of date',
            'not_started' => 'Not started',
        ],
        'project_schedule' => [
            'project_schedule' => 'Project schedule',
            'implementation_meeting' => 'Implementation meeting',
            'unified_scope' => 'Unified scope',
            'finance' => 'Finance',
            'software_requirements' => 'Software requirements',
            'hardware_requirements' => 'Hardware requirements',
            'resource_plan' => 'Resource plan',
            'human_recource' => 'Human resource',
            'technical_requirements' => 'Technical requirements',
            'testing_1' => 'Testing 1',
            'testing_2' => 'Testing 2',
            'release' => 'Release',
        ],
        'budget' => [
            'budget' => 'Budget',
            'plan' => 'Plan',
            'reality' => 'Reality',
        ],
        'project' => [
            'in-progress' => 'On-going project',
            'waiting' => 'To do project',
            'done' => 'Finished project',
            'total' => 'total project',
            'status' => [
                'on_schedule' => 'On schedule',
                'behind_schedule' => 'Behind schedule',
                'ahead_of_schedule' => 'Ahead of schedule' 
            ]
        ],
        'task' => [
            'in-progress' => 'On-going task',
            'waiting' => 'To do task',
            'done' => 'Finished task',
            'total' => 'total task',
            'status' => [
                'on_schedule' => 'On schedule',
                'behind_schedule' => 'Behind schedule',
                'ahead_of_schedule' => 'Ahead of schedule' 
            ],
            'task_status' => [
                'done' => 'Done',
                'waiting' => 'To do',
                'in-progress' => 'In progress',
            ]
        ]
    ],
    'table_column' => [
        'mission' => 'Mision',
        'assignee' => 'Assignee',
        'priority' => 'Priority',
        'status' => 'Status',
    ]
];
