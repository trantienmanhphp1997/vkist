<div class="col-md-10 col-xl-11 box-detail box-user">
	<div class="row">
		<div class="col-md-12">
				<div  class="breadcrumbs"><a href="{{route('admin.research.ideal.index')}}">{{__('executive/contract.breadcrumbs.activity-management')}}</a> \ <span>{{__('executive/import-salary.title')}}</span></div> 
		</div>
	</div>
	<div class="row bd-border">
		<div class="col-md-12">
			<h3 class="title">{{__('executive/import-salary.title')}}</h3>
			<div class="information">
				<div class="inner-tab pt0">
					<div class="row">
						<div class="col-md-9">
							<div class="search-expertise inline-block">
								<input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control" placeholder="{{__('research/ideal.filter.input.placeholder.search')}}">
								<span>
									<img src="/images/Search.svg" alt="search"/>
								</span>
							</div>
							<div class="wlp-140 w-170 inline-block">
								{{-- <select wire:model.lazy="monthFilter" class="form-control" @if($disableSave) disabled @endif>
                                    <option value=''>{{__('data_field_name.import-salary.month_placeholder')}}</option> 
									@foreach($monthDataFilter as $val)
                                        <option value="{{$val->month_working}}">{{App\Enums\EMonth::valueToName($val->month_working)}}</option>
                                    @endforeach
                                </select> --}}
								{{ Form::select('source', ['' =>__('data_field_name.import-salary.from_month')] + $fromMonth, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchFromMonth']) }}
							</div>
							<div class="wlp-140 w-170 inline-block">
								{{ Form::select('source', ['' =>__('data_field_name.import-salary.to_month')] + $toMonth, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchToMonth']) }}
							</div>
							<div class="wlp-140 w-170 inline-block">
								<select wire:model.lazy="yearFilter" class="form-control size13" @if($disableSave) disabled @endif>
                                    <option value=''>{{__('data_field_name.import-salary.year_placeholder')}}</option>
                                        @foreach($yearDataFilter as $val)
                                        	<option value="{{$val->year_working}}">{{$val->year_working}}</option>
                                        @endforeach
                                </select>
							</div>
						</div>
						<div class="col-md-3">
							@if($checkCreatePermission)
								<div class="form-group float-right">
									<a href="#" data-toggle="modal" data-target="#import" class="btn btn-viewmore-news mr0" wire:click="resetData">
										<img src="/images/plus2.svg" alt="plus">{{__('executive/import-salary.btn-add')}}
									</a>
								</div>
							@endif
						</div>
					</div>
					<table class="table">
						<thead>
							<th scope="col" class="text-center border-radius-left">
								{{__('data_field_name.import-salary.index')}}
							</th>
							<th scope="col" class="text-center">
								{{__('executive/import-salary.table_column.name')}}
							</th>
							<th scope="col" class="text-center">
								{{__('data_field_name.import-salary.month_placeholder')}}
							</th>
							<th scope="col" class="text-center">
								{{__('data_field_name.import-salary.year_placeholder')}}
							</th>
							<th scope="col" class="text-center">
								{{__('executive/import-salary.table_column.type')}}
							</th>
							<th scope="col" class="text-center border-radius-right">
								{{__('executive/contract.table_column.action')}}
							</th>
						</thead>
						<div wire:loading class="loader"></div>
						<tbody>
							@foreach($data as $index => $row)
								<tr>
									<td class="text-center">{{$index + 1}}</td>
									<td class="text-left"><a href="{{route('admin.executive.salary.show', ['id' => $row->id])}}">{!! $row->name !!}</a></td>
									<td class="text-center">{{App\Enums\EMonth::valueToName($row->month_working)}}</td>
									<td class="text-center">{!! $row->year_working !!}</td>
									<td class="text-center">{{App\Enums\ESalarySheetsFileType::valueToName($row->type)}}</td>
									<td class="text-center">
										@include('livewire.common.buttons._delete')
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					@if(count($data) > 0)
						{{$data->links()}}
					@else
						<div class="title-approve text-center ml-0">
							<span>{{__('common.message.empty_search')}}</span>
						</div>
					@endif
					@include('livewire.common.modal._modalDelete')
					@include('livewire.admin.executive.salary.modal-import')
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	 $("document").ready(() => {
	 	window.livewire.on('closeModalImport', () => {
	    	$('#import').modal('hide');
	    });
	    $('#import').on('hidden.bs.modal', () => {
		 	$('#result').modal('show');
		});
	 });
</script>
