<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class AssetOrigin extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $table='asset_origin';
    protected $guarded=['*'];
    protected $fillable = [
        'origin_name','origin_status'
    ];
    private static $searchable = [
        'origin_name',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
}
