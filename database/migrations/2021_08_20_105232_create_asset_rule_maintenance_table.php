<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetRuleMaintenanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_rule_maintenance', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('quy định bảo dưỡng');
            $table->foreignId('asset_category_id')->nullable()->constrained('asset_category')->comment(' loại ts, map asset_category');
            $table->string('content_maintenance',255)->nullable()->comment('nội dung bảo dưỡng');
            $table->tinyInteger('frequency')->nullable()->comment('tần suất,1:1 lần, 2: định kỳ');
            $table->string('maintenance_time_json')->nullable()->comment('thời điểm bảo dưỡng-lưu json');
            $table->tinyInteger('repeat ')->nullable()->comment('lặp lại theo, 1:tháng, 2: quý,3:6tháng, 4:năm');

            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_rule_maintenance');
    }
}
