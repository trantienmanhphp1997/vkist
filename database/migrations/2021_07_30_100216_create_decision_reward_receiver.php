<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecisionRewardReceiver extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decision_reward_receiver', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Doi tuong nhan khen thuong');
            $table->foreignId('user_info_id')->nullable()->comment('`Map voi user_info, nguoi nhan khen thuong`')->constrained('user_info');
            $table->foreignId('decision_reward_id')->nullable()->comment('`Map voi decision_reward, quyet dinh khen thuong`')->constrained('decision_reward');
            $table->string('prize_value', 255)->nullable()->comment('Gia tri khen thuong');
            $table->string('reason', 255)->nullable()->comment('Ly do');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decision_reward_receiver');
    }
}
