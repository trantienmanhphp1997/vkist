<div class="col-md-10 col-xl-11 box-detail box-user list-topic-fee list-equipment">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="{{ route('admin.research.topic-fee.index') }}">{{ __('data_field_name.research_cost.list_cost') }}</a> \ <span>{{ __('data_field_name.research_cost.text_create_equipment_cost') }}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{ __('data_field_name.research_cost.text_cost_4') }}:</h3>
            <h5>{{ __('data_field_name.research_cost.equipment_cost') }}</h5>

            <div class="information">
                <div class="inner-tab pt0">
                    <div class="col-md-12 ">
                        @if ($topicFeeStatus == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                            @if($checkCreatePermission)
                                <div class="form-group float-right">
                                    <a javascript:void(0) wire:click="resetInputFields" data-toggle="modal" data-target="#createMaterialCostDetail" class="buttonCreate btn btn-viewmore-news mr0 ">
                                        <img src="/images/plus2.svg" alt="plus">{{__('common.button.create')}}
                                    </a>
                                </div>
                            @endif
                        @endif
                    </div>
                    <table class="table">
                        <thead>
                            <tr class="border-radius text-center">
                                <th scope="col" class="border-radius-left">
                                    {{ __('data_field_name.research_cost.No') }}
                                </th>
                                <th scope="col" class="text-center">
                                    {{ __('data_field_name.research_cost.name') }}
                                </th>
                                <th scope="col" class="text-center">
                                    {{ __('data_field_name.research_cost.file') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.unit') }}
                                </th>
                                <th scope="col" class="text-center">
                                    {{ __('data_field_name.research_cost.quantity') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.price') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.total') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.state_capital') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.other_capital') }}
                                </th>
                                <th scope="col" class="border-radius-right">
                                    {{ __('data_field_name.common_field.action') }}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $row)
                                <tr>
                                    <td class="text-center">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                    <td class="text-center">{{ $row->name}}</td>
                                    <td class="text-center">
                                        @foreach($row->files as $file)
                                        <div wire:click="download({{$file->id}})" style="cursor:pointer">{{ $file->file_name ?? ''}}</div>
                                        @endforeach
                                    </td>
                                    <td class="text-center">{{ $row->unit}}</td>
                                    <td class="text-center">{{ numberFormat($row->quantity) }}</td>
                                    <td class="text-center">{{ numberFormat($row->price) }}</td>
                                    <td class="text-center">{{ numberFormat($row->total) }}</td>
                                    <td class="text-center">{{ numberFormat($row->state_capital) }}</td>
                                    <td class="text-center">{{ numberFormat($row->other_capital) }}</td>
                                    <td class="text-center">
                                        @if ($topicFeeStatus == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                                            @include('livewire.common.buttons._edit')
                                            @include('livewire.common.buttons._delete')
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td class="text-center">{{ __('data_field_name.research_cost.sum') }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-center">{{ numberFormat($dataTotal['total']) }}</td>
                                <td class="text-center">{{ numberFormat($dataTotal['state_capital']) }}</td>
                                <td class="text-center">{{ numberFormat($dataTotal['other_capital']) }}</td>
                                <td></td>
                            </tr>

                        </tbody>
                    </table>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')
                    <div class="col-md-12 text-center mt-5">
                        <a href="{{route('admin.research.topic-fee.edit', $topicFeeId)}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Create -->
    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="createMaterialCostDetail" tabindex="-1" aria-labelledby="createTopicFee" aria-hidden="true">
            <div class="modal-dialog  modal-appraisal">
                <div class="modal-content personnel">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{__('data_field_name.research_cost.create_cost')}}</h4>

                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.name')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="name">
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        @if($displayAttachFile)
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.file')}}</label>
                            @livewire('component.files', ['model_name' => $model,  'type' => $type_upload, 'folder' => 'topic-fee/equipment', 'acceptMimeTypes' => config('common.mime_type.topic_fee')])
                        </div>
                        @endif
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.unit')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="unit">
                            @error('unit') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.quantity')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.lazy="quantity">
                            @error('quantity') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.price')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.lazy="price">
                            @error('price') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.state_capital')}}</label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="state_capital">
                            @error('state_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.other_capital')}}</label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="other_capital">
                            @error('other_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="attached-center">
                            <button type="button" id="close-modal-create-material" class="btn btn-cancel close-btn mt-0" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="store()" class="btn btn-save close-modal">{{__('common.button.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Create -->

    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="target" tabindex="-1" aria-labelledby="createTopicFee" aria-hidden="true">
            <div class="modal-dialog  modal-appraisal">
                <div class="modal-content personnel">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{__('data_field_name.research_cost.text_edit_cost')}}</h4>

                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.name')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="name">
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        @if($updateMode)
                            <div class="form-group">
                                <label>{{__('data_field_name.research_cost.file')}}</label>
                                @livewire('component.files', ['model_name' => $model, 'model_id' => $equipmentCostID, 'type' => $type_upload, 'folder' => 'topic-fee/equipment', 'acceptMimeTypes' => config('common.mime_type.topic_fee')])
                            </div>
                        @endif
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.unit')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="unit">
                            @error('unit') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.quantity')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.lazy="quantity">
                            @error('quantity') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.price')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.lazy="price">
                            @error('price') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.state_capital')}}</label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="state_capital">
                            @error('state_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.other_capital')}}</label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="other_capital">
                            @error('other_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="attached-center">
                            <button type="button" wire:click='resetMode' id="close-modal-edit-material" class="btn btn-cancel close-btn mt-0" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="update()" class="btn btn-save close-modal">{{__('common.button.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <script>
        $("document").ready(function() {
            $('.buttonCreate').click(function() {
                $('.error').remove();
                $('.list-equipment .text-danger').html('');
                $('.form-input').val('');
            });

            $('#target').on('hidden.bs.modal', function () {
                $('#close-modal-edit-material').click();
            });

            window.livewire.on('close-modal-create-material', () => {
                document.getElementById('close-modal-create-material').click()
            });
            window.livewire.on('close-modal-edit-material', () => {
                document.getElementById('close-modal-edit-material').click()
            });

        });
    </script>
</div>
