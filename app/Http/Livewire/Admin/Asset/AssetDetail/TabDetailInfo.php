<?php

namespace App\Http\Livewire\Admin\Asset\AssetDetail;

use App\Enums\EAssetCategoryTypeManage;
use App\Enums\EAssetGroup;
use App\Enums\EAssetOrigin;
use App\Models\Asset;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class TabDetailInfo extends Component
{
    public $idAsset;
    public $assetDetail;
    public $assetOrigin;
    public $assetGroup;

    public function mount()
    {
        $this->assetOrigin = [
            EAssetOrigin::ODA => __('data_field_name.asset.oda'),
            EAssetOrigin::NATION => __('data_field_name.asset.nation'),
        ];
        $this->assetGroup = EAssetGroup::getList();
    }
    public function render()
    {
        $model_name = Asset::class;
        $type = Config::get('common.type_upload.Asset');
        $folder = app($model_name)->getTable();
        $status = 1;
        return view('livewire.admin.asset.asset-detail.tab-detail-info',[ 'model_name' => $model_name, 'type' => $type,
            'folder' => $folder, 'status' => $status,]);
    }
}
