<div class="appraisal-editor position-relative">
    {{-- @dump($appraisalBoard->id, $modelId) --}}
    <form>
        <h4 class="text-center">{{ isset($appraisalBoard->id) ? __('data_field_name.approval_topic.detail_appraisal') : __('data_field_name.approval_topic.create_appraisal') }}</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{ __('data_field_name.approval_topic.profile_type') }}<span class="text-danger">(*)</span></label>
                    {{ Form::select('modelType', ['' => __('data_field_name.common_field.select_default')] + $modelTypeList, '', ['class' => 'form-control', 'wire:model.lazy' => 'modelType', 'disabled' => (!$editable || $appraisalBoard->exists || $checkDisable)]) }}
                    @error('modelType') <span class="error text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            @if (!empty($modelType))
                <div class="col-md-6">
                    <label>{{ $textModelName }}<span class="text-danger">(*)</span></label>
                    {{ Form::select('modelId', ['' => __('data_field_name.common_field.select_default')] + $modelList, null, ['class' => 'form-control', 'wire:model.defer' => 'modelId', 'disabled' => (!$editable || $appraisalBoard->exists || $checkDisable)]) }}
                    @error('modelId') <span class="error text-danger">{{ $message }}</span>@enderror
                </div>
            @endif
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.approval_topic.appraisal_name') }}<span class="text-danger">(*)</span></label>
            <input type="text" class="form-control" wire:model.defer="name" {{ $editable ? '' : 'disabled' }} {{ $checkDisable ? 'disabled' : '' }}>
            @error('name') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.approval_topic.decide_establish') }}</label>
            @if ($checkDisable)
            @livewire('component.files', ['canUpload' => $editable, 'model_name' => \App\Models\AppraisalBoard::class, 'model_id' => $this->appraisalBoard->id, 'type' => config('common.type_upload.AppraisalBoard'), 'folder' => $appraisalBoard->getTable(), 'acceptMimeTypes' => config('common.mime_type.document'), 'canUpload' => false])
            @else
            @livewire('component.files', ['canUpload' => $editable, 'model_name' => \App\Models\AppraisalBoard::class, 'model_id' => $this->appraisalBoard->id, 'type' => config('common.type_upload.AppraisalBoard'), 'folder' => $appraisalBoard->getTable(), 'acceptMimeTypes' => config('common.mime_type.document')])
            @endif
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{ __('data_field_name.approval_topic.appraisal_date') }}<span class="text-danger">(*)</span></label>
                    @include('layouts.partials.input._inputDate', ['input_id' => 'appraisal-date', 'place_holder' => 'dd/mm/yyyy', 'default_date' => $appraisalDate, 'disable_input' => (!$editable || $checkDisable )])
                    @error('appraisalDate') <span class="error text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>{{ __('data_field_name.common_field.descriptions') }}</label>
            <textarea class="form-control" rows="4" wire:model.defer="note" {{ $editable ? '' : 'disabled' }} {{ $checkDisable ? 'disabled' : '' }}></textarea>
            @error('note') <span class="error text-danger">{{ $message }}</span>@enderror
        </div>

        <div class="row">
            <div class="col-md-6">
                <h5 class="title-plan text-uppercase">{{ __('data_field_name.work_plan.create.title_item') }}</h5>
            </div>
            @if($editable)
                <div class="col-md-6">
                    <div class="form-group float-right">
                        <button type="button" class="btn btn-viewmore-news mr0 " wire:click="addTmpMember"><img src="/images/plus2.svg" alt="plus"> {{ __('data_field_name.work_plan.create_btn') }} </button>
                    </div>
                </div>
            @endif
        </div>

        @if ($editable)
            <div class="box-calendar row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('data_field_name.work_plan.create.name') }}</label>
                        <select class="form-control form-control-lg" wire:model.lazy="tmpStaffId">
                            <option value="">{{ __('data_field_name.work_plan.create.selected_default') }}</option>
                            @foreach ($staffList as $id => $value)
                                <option value="{{ $id }}">{{ $value['fullname'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('data_field_name.topic.qualification') }}</label>
                        <input type="text" class="form-control" disabled value="{{ $staffList[$tmpStaffId]['academic'] ?? '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('data_field_name.topic.role') }}</label>
                        {{ Form::select('role', ['' => __('data_field_name.common_field.select_default')] + $roleList, '', ['class' => 'form-control', 'wire:model.defer' => 'tmpRoleId']) }}
                    </div>
                </div>
            </div>
        @endif

        <div class="inner-tab col-md-12">
            <div class="row">
                <table class="table">
                    <thead>
                        <tr class="border-radius">
                            <th scope="col" class="border-radius-left text-center">{{ __('data_field_name.common_field.stt') }}</th>
                            <th scope="col" class="text-center" scope="col">{{ __('data_field_name.topic.member_name') }}</th>
                            <th scope="col" class="text-center" scope="col">{{ __('data_field_name.topic.qualification') }}</th>
                            <th scope="col" class="text-center" scope="col">{{ __('data_field_name.topic.role') }}</th>
                            <th scope="col" class="border-radius-right text-center">{{ __('data_field_name.common_field.action') }}</th>
                        </tr>
                    </thead>
                    @php $stt = 1; @endphp
                    <tbody>
                        @foreach ($tmpMemberList as $staffId => $value)
                            <tr>
                                <td class="text-danger text-center">{{ $stt }}</td>
                                <td class="text-danger text-center">{{ $value['fullname'] }}</td>
                                <td class="text-danger text-center">{{ $value['academic'] ?? '' }}</td>
                                <td class="text-danger text-center">{{ $roleList[$value['role_id']] ?? '' }}</td>
                                <td class="text-danger text-center">
                                    <button type="button" wire:click="deleteTmpMember({{ $staffId }})" title="Xóa" class="btn-sm border-0 bg-transparent">
                                        <img src="/images/trash.svg" alt="trash">
                                    </button>
                                </td>
                            </tr>
                            @php $stt++; @endphp
                        @endforeach
                    </tbody>
                    @foreach ($currentMemberList as $memberId => $value)
                        <tr>
                            <td class="text-center">{{ $stt }}</td>
                            <td class="text-center">{{ $value['fullname'] }}</td>
                            <td class="text-center">{{ $value['academic'] }}</td>
                            <td class="text-center">{{ $roleList[$value['role_id']] ?? '' }}</td>
                            <td class="text-center">
                                @if ($editable)
                                    <button type="button" onclick="$(this).parents('.appraisal-editor').find('.delete-modal').removeClass('d-none');$(this).parents('.appraisal-editor').find('#delete-member-btn').attr('data-member', {{ $memberId }})" class="btn-sm border-0 bg-transparent">
                                        <img src="/images/trash.svg" alt="trash">
                                    </button>
                                @endif
                            </td>
                        </tr>
                        @php $stt++; @endphp
                    @endforeach
                </table>
            </div>
        </div>

        <div class="attached-center text-center">
            <button type="button" wire:click="cancel" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14 close-btn" data-dismiss="modal">{{ __('common.button.close') }}</button>
            @if ($editable)
                <button type="button" wire:click="save" wire:loading.attr="disabled" class="btn btn-main bg-primary text-9 size16 px-48 py-14 close-modal">{{ __('common.button.save') }}</button>
            @endif
        </div>
    </form>

    {{-- Delete member modal --}}
    <div class="modal-dialog delete-modal d-none" style="position: fixed; box-shadow: 0px 0px 100px black; top: 50%; left: 50%; transform: translate(-50%, -50%);">
        <div class="modal-content">
            <div class="modal-body box-user">
                <h4 class="modal-title">{{ __('common.confirm_message.confirm_title') }}</h4>
                {{ __('common.confirm_message.are_you_sure_delete') }}
            </div>
            <div class="group-btn2 text-center pt-24">
                <button type="button" class="btn btn-cancel" onclick="$(this).parents('.delete-modal').addClass('d-none')">{{ __('common.button.cancel') }}</button>
                <button type="button" class="btn btn-save" wire:click="deleteMember($event.target.getAttribute('data-member'))" id="delete-member-btn">{{ __('common.button.delete') }}</button>
            </div>
        </div>
    </div>

    {{-- Send invitation modal --}}
    {{-- <div class="modal-dialog invite-modal d-none" style="position: fixed; box-shadow: 0px 0px 100px black; top: 50%; left: 50%; transform: translate(-50%, -50%);">
        <div class="modal-content personnel">
            <div class="modal-body box-user">
                <h4 class="text-center">{{ __('data_field_name.approval_topic.send_invitations') }}</h4>

                <div class="form-group">
                    <label>{{ __('data_field_name.approval_topic.send_invitations') }}</label>
                    @livewire('component.files', ['model_name' => \App\Models\AppraisalBoardHasUserInfo::class, 'type' => config('common.type_upload.AppraisalBoardHasUserInfo'), 'folder' => app(\App\Models\AppraisalBoardHasUserInfo::class)->getTable()], key(date('Y-m-d H:i:s')))
                </div>

                <div class="attached-center">
                    <button type="button" class="btn btn-cancel" onclick="$(this).parents('.invite-modal').addClass('d-none')">{{ __('common.button.close') }}</button>
                    <button type="button" class="btn btn-save" wire:click="sendInvitation($event.target.getAttribute('data-member'))" id="invite-btn">{{ __('common.button.send') }}</button>
                </div>
            </div>
        </div>
    </div> --}}

    <script>
        window.addEventListener('close-delete-modal', function() {
            $('.delete-modal').addClass('d-none');
        });

    </script>
</div>
