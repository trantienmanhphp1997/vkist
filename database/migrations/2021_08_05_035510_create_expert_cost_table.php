<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpertCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expert_cost', function (Blueprint $table) {
            $table->id()->comment('Danh sach thue chuyen gia');
            $table->string('content', 1000)->nullable()->comment('Noi dung thuc hien');
            $table->string('working_time', 255)->nullable()->comment('Thoi gian thuc hien');
            $table->bigInteger('salary')->nullable()->comment('Muc luong');
            $table->bigInteger('total')->nullable()->comment('Tong kinh phi');
            $table->bigInteger('state_capital')->nullable()->comment('Nguon von nha nuoc');
            $table->bigInteger('other_capital')->nullable()->comment('Nguon khac');
            $table->unsignedBigInteger('expert_id')->nullable()->comment('Map voi expert');
            $table->foreign('expert_id')->references('id')->on('expert');
            $table->foreignId('topic_fee_detail_id')->nullable()->constrained('topic_fee_detail');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_cost');
    }
}
