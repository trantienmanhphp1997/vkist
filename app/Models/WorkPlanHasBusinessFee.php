<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkPlanHasBusinessFee extends Model
{
    use HasFactory;
    protected $table='work_plan_has_business_fee';

    protected $fillable=['working_day','work_plan_id','business_fee_id','user_info_id','money', 'status'];

  

}
