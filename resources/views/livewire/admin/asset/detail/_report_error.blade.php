<div class="modal fade modal-fix" wire:ignore.self id="report_error" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.asset.report_error')}}</h6>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 pd-col">
                            <div class="info-asset">
                                @include('livewire.admin.asset.detail._asset-info')
                                @include('livewire.admin.asset.detail._user_info')
                            </div>
                        </div>
                        <div class="col-4 pd-col">
                            <div class="form-group info-request">
                                <label>{{__('data_field_name.asset.offer_date')}}<span class="text-danger">*</span></label>
                                <input type="date" wire:model.lazy="claim_date">
                                @error('claim_date')
                                <span class="error text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 pd-col">
                            <div class="form-group info-request">
                                <label>//    {{__('data_field_name.asset.fix_reason')}}<span class="text-danger">*</span></label>
                                <textarea cols="30" rows="10" wire:model.lazy="content"></textarea>
                                @error('content')
                                <span class="error text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 pd-col">
                            <div id="file_right">
                            @livewire('component.files', ['model_name' => $model_form, 'model_id'=>$form_id,
                            'type'=>$type_form, 'folder'=>$folder_form,'status'=>$status],key('upload-file-2'))
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" id="close-error-asset" wire:click.prevent="cancel()"
                        data-dismiss="modal">{{__('common.button.cancel')}}
                </button>
                <button type="button" class="btn btn-primary" wire:click.prevent="saveForm({{2}}, true)">{{__('common.button.perform')}}
                </button>
            </div>
        </div>
    </div>
</div>
