<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use App\Models\Topic;
use App\Models\TopicHasUserInfo;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class TabDetailUser extends Component
{
    public $user_detail_id;
    public $id_member;
    public function render()
    {
        $data = Topic::findOrFail($this->user_detail_id);
        $members = $data->members()->with(['userInfo', 'role', 'research'])
            ->paginate(Config::get('app.per_page') ?? 25);
        $detail_member= [];

        if($this->id_member){
            $detail_member = TopicHasUserInfo::findOrFail($this->id_member);
        }

        return view('livewire.admin.research.topic.tab-detail-user',['members'=>$members,'detail_member'=>$detail_member]);
    }
    public function detailMember($id){
        $this->id_member=$id;

    }
}
