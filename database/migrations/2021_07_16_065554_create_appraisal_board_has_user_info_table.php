<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalBoardHasUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_board_has_user_info', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Danh sach thanh vien hoi dong');
            $table->tinyInteger('role_id')->comment('1 Chu tich hoi dong, 2 Thu ky, 3 Chuyen gia tu van, 4 Thanh vien hoi dong');
            $table->tinyInteger('status')->comment('0 Chua gui giay moi, 1 Da gui giay moi, 2 Nhan loi, 3 Tu choi');
            $table->unsignedBigInteger('appraisal_board_id')->nullable()->comment('Map voi appraisal_board_id');
            $table->foreign('appraisal_board_id')->references('id')->on('appraisal_board')->comment('Map voi appraisal_board_id');
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('Map voi user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info')->comment('Map voi user_info');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_board_has_user_info');
    }
}
