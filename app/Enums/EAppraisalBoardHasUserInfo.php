<?php


namespace App\Enums;


class EAppraisalBoardHasUserInfo {
    const ROLE_CHAIRMAN = 1;
    const ROLE_SECRETARY = 2;
    const ROLE_CONSULTANT = 3;
    const ROLE_MEMBER = 4;
    const STATUS_UNSENT = 0;
    const STATUS_SEND = 1;
    const STATUS_ACCEPT = 2;
    const STATUS_REJECT = 3;

    public static function appraisalRole()
    {
        return [
            self::ROLE_CHAIRMAN => __('data_field_name.approval_topic.role_chairman'),
            self::ROLE_SECRETARY => __('data_field_name.approval_topic.role_secretary'),
            self::ROLE_CONSULTANT => __('data_field_name.approval_topic.role_consultant'),
            self::ROLE_MEMBER => __('data_field_name.approval_topic.role_member'),
        ];
    }
}
