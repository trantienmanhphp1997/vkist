<?php

namespace Tests\Unit;

use App\Http\Livewire\Admin\System\Role\RoleList;
use App\Models\User;
use Database\Seeders\UserDBSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class RoleControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    public $routeIndex = 'admin.system.role.index';
    public $routeCreate = 'admin.system.role.create';
    public $routeStore = 'admin.system.role.store';
    public $routeEdit = 'admin.system.role.edit';
    public $routeListRoleUser = 'admin.system.role.listroleuser';
    public $routeAddRoleUser = 'admin.system.role.addroleuser';

    /** @test */
    public function testIndexRole()
    {
        $this->actingAs($this->user)->get(route($this->routeIndex))->assertOk();
    }

    /** @test */
    public function testValidationRoleName()
    {
        $this->actingAs($this->user)->get(route($this->routeCreate))->assertOk();
        $this->actingAs($this->user)->post(route($this->routeStore), [
            'name' => '',
            'code' => 'edit',
            'guard_name' => 'web',
            'status' => 1,
            'note' => 'test role',
        ])->assertRedirect(route($this->routeCreate))->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function testValidationRoleCode()
    {
        $this->actingAs($this->user)->get(route($this->routeCreate))->assertOk();
        $this->actingAs($this->user)->post(route($this->routeStore), [
            'name' => 'Edit',
            'code' => '',
            'guard_name' => 'web',
            'status' => 1,
            'note' => 'test role 2',
        ])->assertRedirect(route($this->routeCreate))->assertSessionHasErrors(['code']);
    }

    /** @test */
    public function testStoreRole()
    {
        $this->actingAs($this->user)->get(route($this->routeCreate))->assertOk();
        $this->actingAs($this->user)->post(route($this->routeStore), [
            'name' => 'Edit',
            'code' => 'edit',
            'guard_name' => 'web',
            'status' => 0,
            'note' => 'test',
        ])->assertRedirect(route($this->routeEdit, 5));
    }

    /** @test */
    public function testEditRole()
    {
        $this->actingAs($this->user)->post(route($this->routeStore), [
            'name' => 'Edit',
            'code' => 'edit',
            'guard_name' => 'web',
            'status' => 0,
            'note' => 'test role 3',
        ]);

        $role = Role::first();

        $this->actingAs($this->user)->get(route($this->routeEdit, $role->id))->assertOk();
    }

    /** @test */
    public function testUpdateRole()
    {
        $this->actingAs($this->user)->post(route($this->routeStore), [
            'name' => 'Edit',
            'code' => 'edit',
            'guard_name' => 'web',
            'status' => 0,
            'note' => 'test role 4',
        ]);

        $role = Role::first();
        $this->actingAs($this->user)->post(route('admin.system.role.update', $role->id), [
            'id' => $role->id,
            'name' => 'EditTest',
            'code' => 'dfgd',
            'guard_name' => 'web',
            'status' => 1,
            'note' => 'test role 5',
        ])->assertRedirect(route($this->routeIndex));
    }


    /** @test */
    public function testListRoleUser()
    {
        $this->actingAs($this->user)->post(route($this->routeStore), [
            'name' => 'Edit',
            'code' => 'edit',
            'guard_name' => 'web',
            'status' => 0,
            'note' => 'test role 6',
        ]);
        $role = Role::first();
        $this->actingAs($this->user)->get(route($this->routeListRoleUser, $role->id))->assertOk();
    }

    /** @test */
    public function testListRoleUserNotFound()
    {
        $this->actingAs($this->user)->get(route($this->routeListRoleUser, 123))
            ->assertRedirect(route($this->routeIndex));
    }

    /** @test */
    public function testAddRoleUser()
    {
        $this->actingAs($this->user)->post(route($this->routeStore), [
            'name' => 'Edit',
            'code' => 'edit',
            'guard_name' => 'web',
            'status' => 0,
            'note' => 'test role 7',
        ]);
        $role = Role::first();
        $this->actingAs($this->user)->get(route($this->routeListRoleUser, $role->id));
        $this->actingAs($this->user)->post(route($this->routeAddRoleUser, $role->id), [
            'user' => [$this->user->id => 1],
        ]);
        $this->actingAs($this->user)->post(route($this->routeAddRoleUser, $role->id), [
            'user' => [$this->user->id => 1],
        ])->assertRedirect(route($this->routeEdit, $role->id));
    }

    /** @test */
    public function testAddRoleUserNotFound()
    {
        $this->actingAs($this->user)->post(route($this->routeAddRoleUser, 123), [
            'user' => [$this->user->id => 1],
        ])->assertRedirect(route($this->routeIndex));
    }

    /** @test */
    public function testDeleteRole() {
        $this->actingAsAdmin()->get(route($this->routeIndex))->assertOk();

        $this->post(route($this->routeStore), [
            'name' => 'Edit',
            'code' => 'edit',
            'guard_name' => 'web',
            'status' => 0,
            'note' => 'test role 8',
        ]);
        $role = Role::where('code', 'edit')->first();

        Livewire::test(RoleList::class)->call('deleteRole', $role->id)->call('delete');
        $this->assertDeleted('roles', ['id' => $role->id]);
    }

    /** @test */
    public function testStatusRole() {
        $this->actingAsAdmin()->get(route($this->routeIndex))->assertOk();

        $this->post(route($this->routeStore), [
            'name' => 'Edit',
            'code' => 'edit',
            'guard_name' => 'web',
            'status' => 0,
            'note' => 'test role 9',
        ]);
        $role = Role::first();

        Livewire::test(RoleList::class)->call('updateStatusRole', $role->id)->call('delete');
    }
}
