<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicFeeDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic_fee_detail', function (Blueprint $table) {
            $table->id()->comment('Danh sach khoan chi');
            $table->string('name', 500)->nullable()->comment('Noi dung khoan chi');
            $table->bigInteger('total_capital')->nullable()->comment('Tong kinh phi');
            $table->string('ratio', 10)->nullable()->comment('Ti le %');
            $table->bigInteger('state_capital')->nullable()->comment('Nguon von nha nuoc');
            $table->bigInteger('other_capital')->nullable()->comment('Nguon khac');
            $table->string('note', 1000)->nullable()->comment('Ghi chu');
            $table->unsignedBigInteger('topic_fee_id')->nullable()->comment('Map voi topic_fee');
            $table->foreign('topic_fee_id')->references('id')->on('topic_fee');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimate_detail_topic');
    }
}
