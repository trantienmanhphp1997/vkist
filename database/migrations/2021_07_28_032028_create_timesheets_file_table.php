<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimesheetsFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheets_file', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Luu bang cham cong theo thang');
            $table->tinyInteger('month_working')->comment('Thang');
            $table->integer('year_working')->comment('nam');
            $table->string('name', 500)->comment('Ten bang cham cong');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheets_file');
    }
}
