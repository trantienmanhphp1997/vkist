<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\User;
use App\Models\Expert;
use Faker\Factory as Factory;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
class UserDBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pass = 'vkist2021^';
        $user = User::where('username', 'admin_vkist')->first();
        if(!$user) {
            $user = User::updateOrCreate([
                'username' => 'admin_vkist',
                'name' => 'admin_vkist',
                'email' => 'admin_vkist_test@gmail.com',
                'password' => Hash::make('vkist2021^'), // password
                "status" => 1,
                'gw_user_id' => '0000000',
                'gw_pass' => $pass,
            ]);
        }

        $user->password = Hash::make($pass);
        $user->change_password_at = now();
        $role = Role::updateOrCreate(['name' => 'administrator', 'note'=>'Quyền quản trị', 'status' => 1 ]);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);
        $user->save();

        $user->assignRole([$role->id]);

        $data = require_once(database_path('raw/UserNameData.php'));
        foreach($data as $value){
            if($value){
                $userNew = User::where('username', $value)->first();
                if(!$userNew) {
                    $userNew = User::updateOrCreate([
                        'name' => 'admin_vkist' . $value,
                        'username' => $value,
                        'email' => 'admin_vkist_test' . $value . '@gmail.com',
                        "status" => 1,
                        'password' => Hash::make($pass), // password
                        'gw_user_id' => $value,
                        'gw_pass' => $pass,
                    ]);
                    $userNew->gw_user_id = $value;
                    $userNew->gw_pass = $pass;
                    $userNew->password = Hash::make($pass);
                    $userNew->change_password_at = now();
                    $userNew->save();
                    $userNew->assignRole([$role->id]);
                }
            }
        }
    }
}
