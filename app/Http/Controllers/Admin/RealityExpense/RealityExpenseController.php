<?php

namespace App\Http\Controllers\admin\RealityExpense;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RealityExpense;

class RealityExpenseController extends Controller
{
    public function index(){
        return view('admin.reality_expense.index');
    }
    public function create(){
        return view('admin.reality_expense.create');
    }
    public function edit($id){
        $reality_expense = RealityExpense::findOrFail($id);
        return view('admin.reality_expense.edit',['reality_expense'=>$reality_expense]);
    }
    public function show($id){
        $reality_expense = RealityExpense::findOrFail($id);
        return view('admin.reality_expense.detail',['reality_expense'=>$reality_expense]);
    }
}
