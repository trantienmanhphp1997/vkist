<?php

namespace App\Http\Controllers\Admin\System;

use App\Enums\ESystemConfigType;
use App\Http\Controllers\Controller;
use App\Models\SystemConfig;
use App\Models\UserInfoLeave;
use Illuminate\Http\Request;

class LeaveConfigController extends Controller
{
    public function index()
    {
        return view('admin.system.leave-config.index');
    }
}
