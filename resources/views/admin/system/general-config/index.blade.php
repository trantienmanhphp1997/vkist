@extends('layouts.master')
@section('title')
    <title>{{__('menu_management.menu_name.general_configuration')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection

@section('content')
    @livewire('admin.system.general-config')
@endsection
