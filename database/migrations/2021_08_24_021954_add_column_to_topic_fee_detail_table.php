<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTopicFeeDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_fee_detail', function (Blueprint $table) {
            $table->tinyInteger('type_cost')->nullable()->comment('Loai chi phi: 1 gian tiep, 2 truc tiep');
            $table->tinyInteger('payment_cost')->nullable()->comment('Hinh thuc chi: 0 khong khoan chi, 1 khoan chi');
            $table->tinyInteger('source_cost')->nullable()->comment('Nguon kinh phi: 1 ODA, 2 Ngan sach nha nuoc, 3 Ngan sach khac');
            $table->bigInteger('approval_cost')->nullable()->comment('Duyet chi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_fee_detail', function (Blueprint $table) {
            $table->dropColumn('type_cost');
            $table->dropColumn('payment_cost');
            $table->dropColumn('source_cost');
            $table->dropColumn('approval_cost');
        });
    }
}
