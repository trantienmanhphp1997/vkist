<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfoLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info_leave', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Danh sach nhan vien nghi viec');
            $table->string('reason', 255)->nullable()->comment('Ly do nghi viec');
            $table->dateTime('leave_date')->nullable()->comment('Ngay nghi viec');
            $table->tinyInteger('status')->nullable()->comment('0 Dang thuc hien, 1 Da xu ly');
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('Map voi user_info ');
            $table->foreign('user_info_id')->references('id')->on('user_info'); 
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao'); 
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info_leave');
    }
}
