<div wire:ignore.self class="modal fade" id="category_budget" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 600px">
        <div class="modal-content">
            <form wire:submit.prevent="submit">
                <div class="modal-header">
                    <div class="row width_100">
                        <div class="col-11">
                            <h6 class="text_center fontsize_28"
                                id="exampleModalLabel">{{__('data_field_name.budget.create_budget')}}</h6>
                        </div>
                        <div class="col-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true close-btn">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group" wire:ignore>
                        <label>{{__('data_field_name.budget.new_type_added')}}</label>
                        <select class="form-control form-control-lg" wire:model.lazy="chooseCategory">
                            <option value="1">
                            {{__('data_field_name.budget.diff')}}
                            </option>
                            <option value="2">{{__('data_field_name.budget.copy_budget')}}</option>
                            <option value="3">{{__('data_field_name.budget.topic_budget')}}</option>
                        </select>
                    </div>
                    @if($chooseCategory == 2)
                    <div class="form-group">
                        <label>{{__('data_field_name.budget.copy_budget')}}</label>
                        <select class="form-control form-control-lg" wire:model.defer="searchBudget">
                            @foreach($budget_code as $key => $val)
                                <option value="{{$key}}">{{$val}}</option>
                            @endforeach
                        </select>
                    </div>
                    @elseif($chooseCategory == 3)
                    <div class="form-group">
                        <label>{{__('data_field_name.budget.estimated_code')}}</label>
                        <select class="form-control form-control-lg" wire:model.defer="topic_fee_id">
                            @foreach($topic_fee_list as $key => $val)
                                <option value="{{$key}}">{{$val}}</option>
                            @endforeach
                        </select>

                    </div>
                    @else
                    <div class="form-group">
                        <label>{{__('data_field_name.budget.estimated_code')}}</label>
                        <input type="text" wire:model.lazy="topic_fee" class="form-control form-control-lg">
                        @error('topic_fee') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" id="close-rule"
                            data-dismiss="modal">{{__('common.button.cancel')}}
                    </button>
                    <button type="button" class="btn btn-save"
                            wire:click.prevent="save()">{{__('common.button.continue')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


