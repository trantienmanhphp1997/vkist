<?php

namespace Database\Seeders;


use App\Enums\ETopicFee;
use App\Models\TopicFee;
use App\Models\TopicFeeDetail;
use Illuminate\Database\Seeder;
use DB;

class TopicFeeApprovalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topicFees = TopicFee::where('status', ETopicFee::STATUS_APPROVAL)->get();
        if ($topicFees->isNotEmpty()) {
            foreach ($topicFees as $topicFee) {
                $dataTopicFeeDetail = TopicFeeDetail::where('topic_fee_id', $topicFee->id)->get();
                if ($dataTopicFeeDetail->isNotEmpty()) {
                    foreach ($dataTopicFeeDetail as $detail) {
                        $topicFeeDetail = TopicFeeDetail::findOrFail($detail->id);
                        $topicFeeDetail->update([
                            'approval_cost' => $topicFeeDetail->total_capital
                        ]);
                    }
                }
            }
        }
    }
}
