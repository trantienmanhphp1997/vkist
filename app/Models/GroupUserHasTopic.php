<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class GroupUserHasTopic extends Eloquent
{
    use HasFactory;
    protected $table = 'group_user_has_topic';
    protected $fillable = [
        'topic_id','group_user_id'
    ];

    public function topic(){
        return $this->hasOne(Topic::class,'topic_id','id');
    }

    public function groupUser(){
        return $this->belongsTo(GroupUser::class,'group_user_id','id');
    }
}
