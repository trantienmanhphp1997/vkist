<?php

namespace App\Http\Livewire\Admin\Research\Achievement;

use App\Enums\EResearchAchievementType;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use App\Models\ResearchAchievement;
use App\Component\SizeFile;
use Livewire\WithFileUploads;
use App\Component\FileUpload;
use App\Models\Topic;
use App\Enums\EResearchAchievementOwnerType;
use App\Enums\EResearchAchievementObjectTransferType;

class AchievementCreateAndUpdate extends BaseLive
{
    public $type;
    public $isEdit = false;

    //achievement info
    public $research_achievement;
    public $name;
    public $topic_id;
    public $publish_date;

    public $magazine_name; //dùng cho type 1 bài báo khoa học
    public $article_link; //dùng cho type 1 bài báo khoa học

    public $type_of_owner; //dùng cho type 2 sở hữu trí tuệ

    public $receiver_department; //dùng cho type 3 chuyển giao công nghệ
    public $type_of_object_transfer; //dùng cho type 3 chuyển giao công nghệ

    //topic info
    public $topic_name;
    public $topic_code;
    public $topic_start_date;
    public $topic_end_date;
    public $topic_field_name;
    public $topic_leader_name;
    public $topic_execution_time;

    //file upload
    public $fileUpload;
    public $listFileUpload = [];
    public $limit_total_file = 5;
    public $limit_file_size = 255; //MB
    public $accept_file ;

    //file cũ đã có sẵn của decision_reward
    public $listOldFile = [];
    //file cũ đã có sẵn của decision_reward, đây là bản tạm dùng để hiển thị cho user xem, xoá trên list nay, khi nào lưu thì mới áp dụng thay đổi vô list chính
    public $listOldFileTemp = [];
    //danh sách các file cũ mà user muốn xoá
    public $listIdOfOldFileNeedDelete = [];
    public $current_total_file;

    use WithFileUploads;


    protected $listeners = [
        'setTargetId' => 'getTopicInfo',
        'set-publish_date' => 'setPublishDate',
    ];

    public function setPublishDate($data) {
        $this->publish_date = $data['publish_date'] ? date('Y-m-d', strtotime($data['publish_date'])) : null;
    }

    public function mount() {
        $this->accept_file = Config::get('common.mime_type.general');
        if($this->research_achievement){
            $this->isEdit=true;
            $this->getTopicInfo($this->research_achievement->topic_id);
            $this->name = $this->research_achievement->name;
            $this->publish_date = date('Y-m-d', strtotime($this->research_achievement->publish_date));
            $this->magazine_name =$this->research_achievement->magazine_name;
            $this->article_link = $this->research_achievement->article_link;
            $this->type_of_owner = $this->research_achievement->type_of_owner;
            $this->receiver_department = $this->research_achievement->receiver_department;
            $this->type_of_object_transfer = $this->research_achievement->type_of_object_transfer;

            //get document
            $model_name = ResearchAchievement::class;
            $type = Config::get('common.type_upload.ResearchAchievement');
            $model_id = $this->research_achievement->id;
            $this->listOldFile=\App\Models\File::where('model_name',$model_name)->where('type', $type)->where('model_id',$model_id)->get();

             //đẩy list file cũ qua list tạm
            foreach($this->listOldFile as $item) {
                $infoOldFile = [
                    'id' => $item->id,
                    'file_name' => $item->file_name,
                    'size_file' => $item->size_file,
                    'url' => $item->url,
                ];
                array_push($this->listOldFileTemp, $infoOldFile);
            }
        }
    }

    public function deleteNewFileUpload($index) {
        unset($this->listFileUpload[$index]);
    }

    public function deleteFileOldTemp($index) {
        if(isset($this->listOldFileTemp[$index])) {
            array_push($this->listIdOfOldFileNeedDelete, $this->listOldFileTemp[$index]['id']);
            unset($this->listOldFileTemp[$index]);
        }
    }


    public function getTopicInfo($id) {
        $topic = Topic::with(['leader', 'researchCategory'])->findOrFail($id);
        $this->topic_id = $id;
        $this->topic_name = $topic->name ?? '';
        $this->topic_code = $topic->code ?? '';
        $this->topic_start_date = $topic->start_date ?? '';
        $this->topic_end_date = $topic->end_date ?? '';
        $this->topic_field_name = $topic->researchCategory->name ?? '';
        $this->topic_leader_name = $topic->leader->fullname ?? '';
        $this->topic_execution_time = __('research/achievement.label.from').' '. date('d-m-Y', strtotime($this->topic_start_date)).' '. __('research/achievement.label.to').' '.date('d-m-Y', strtotime($this->topic_end_date));
    }

    public function updatedFileUpload() {
        $this->current_total_file = count($this->listFileUpload) + count($this->listOldFileTemp);
        $rules = [
            'fileUpload' => [
                'file',
                'mimes:' . implode(',', $this->accept_file),
                'max:' . $this->limit_file_size * 1024
            ],
            'current_total_file' => 'numeric | max:'. ($this->limit_total_file - 1)
            // 'files' => [
            //     'array',
            //     'max:' . ($this->limit_total_file - 1)
            // ]
        ];
        $error_message = [
            'fileUpload.mimes' => __('notification.upload.mime_type'),
            'fileUpload.max' => __('notification.upload.maximum_size', ['value' => $this->limit_file_size]),
            'current_total_file.max' => __('notification.upload.maximum_uploads')
        ];
        $this->validate($rules,$error_message);
        $size = new SizeFile();
        $size=$size->sizeFile($this->fileUpload->getSize());
        $filenameOrigin = $this->fileUpload->getClientOriginalName();
        $fileUploadInfo = [
            'file_name' => $filenameOrigin,
            'size_file' => $size,
            'file' => $this->fileUpload,
        ];
        array_push($this->listFileUpload, $fileUploadInfo);
        $this->fileUpload = null;
    }

    public function render()
    {
        return view('livewire.admin.research.achievement.achievement-create-and-update');
    }

    public function validateData() {

        $rules= [];
        $att = [];
        switch($this->type){
            case EResearchAchievementType::SCIENCE_ARTICLE: {
                $rules = [
                    'name' => 'required|max:100',
                    'magazine_name' => 'required|max:100',
                ];
                $att = [
                    'name' => __('research/achievement.label.name_of_science_article'),
                    'magazine_name' => __('research/achievement.label.magazine_name'),
                ];
                break;
            }
            case EResearchAchievementType::INTELLECTUAL_PROPERTY: {
                $rules = [
                    'name' => 'required|max:100',
                ];
                $att = [
                    'name' => __('research/achievement.label.name_of_intellectual_property'),
                ];
                break;
            }
            case EResearchAchievementType::TECHNOLOGY_TRANSFER: {
                $rules = [
                    'name' => 'required|max:100',
                    'receiver_department' => 'required|max:100'
                ];
                $att = [
                    'name' => __('research/achievement.label.name_of_technology_transfer'),
                    'receiver_department' => __('research/achievement.label.receiver_department'),
                ];
                break;
            }
            default:
                break;
        }
        $this->validate($rules, [], $att);
    }

    public function generateNewCode($topic, $type, $id) {
        switch($type) {
            case EResearchAchievementType::SCIENCE_ARTICLE:
                $typeCode = 'SA';
                break;
            case EResearchAchievementType::INTELLECTUAL_PROPERTY:
                $typeCode = 'IP';
                break;
            case EResearchAchievementType::TECHNOLOGY_TRANSFER:
                $typeCode = 'TT';
                break;
            default:
                break;
        }
        return $topic . $typeCode . $id;
    }

    public function download($url, $name) {
        return response()->download('storage/'.$url, $name);
    }

    public function store(){
        if($this->isEdit) {
            parent::edit($this->research_achievement->id);
        } else {
            parent::store();
        }
        $this->validateData();

        if(is_null($this->research_achievement)) {
            $this->research_achievement = new ResearchAchievement();
        }
        $this->research_achievement->type = $this->type;
        $this->research_achievement->name = trim($this->name);
        $this->research_achievement->topic_id = $this->topic_id;
        $this->research_achievement->magazine_name = trim($this->magazine_name);
        $this->research_achievement->article_link = trim($this->article_link);
        $this->research_achievement->publish_date= $this->publish_date;
        $this->research_achievement->type_of_owner = $this->type_of_owner;
        $this->research_achievement->type_of_object_transfer = $this->type_of_object_transfer;
        $this->research_achievement->receiver_department = trim($this->receiver_department);
        $this->research_achievement->save();
        $this->research_achievement->code = $this->generateNewCode($this->topic_code, $this->type, $this->research_achievement->id);
        $this->research_achievement->save();

        // //save file
        $fileUploadComponent=new FileUpload();
        $model_name = ResearchAchievement::class;
        $type = Config::get('common.type_upload.ResearchAchievement');
        $model_id = $this->research_achievement->id;
        $folder = app($model_name)->getTable();
        foreach($this->listFileUpload as $file) {
            $dataUpload= $fileUploadComponent->uploadFile($file['file'],$folder);
            $file_upload = new \App\Models\File();
            $file_upload->url = $dataUpload['url'];
            $file_upload->size_file = $dataUpload['size_file'];
            $file_upload->file_name = $dataUpload['file_name'];
            $file_upload->model_name = $model_name;
            $file_upload->model_id = $model_id;
            $file_upload->type = $type;
            $file_upload->save();
        }

        //xoá các file đính kèm cần xoá
        foreach($this->listOldFile as $item) {
            if(in_array($item->id, $this->listIdOfOldFileNeedDelete)) {
                $item->delete();
            }
        }

        if($this->isEdit) {
            session()->flash('success', __('notification.common.success.edit'));
        } else {
            session()->flash('success', __('notification.common.success.add'));
        }

        switch($this->type){
            case EResearchAchievementType::SCIENCE_ARTICLE: {
                return redirect()->route('admin.research.achievement.science-article.index');
            }
            case EResearchAchievementType::INTELLECTUAL_PROPERTY: {
                return redirect()->route('admin.research.achievement.intellectual-property.index');
            }
            case EResearchAchievementType::TECHNOLOGY_TRANSFER: {
                return redirect()->route('admin.research.achievement.technology-transfer.index');
            }
            default:
                break;
        }
    }

}
