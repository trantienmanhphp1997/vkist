<?php

namespace App\Exports;

use App\Enums\EAssetStatus;
use App\Models\AssetAllocatedRevoke;
use App\Models\Department;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class AssetLiquidationExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $searchName;

    public function __construct($searchName = '')
    {
        $this->searchName = $searchName;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $index = 1;
        $query = AssetAllocatedRevoke::where('status',\App\Enums\EAssetStatus::REPORT_LIQUIDATION)
            ->orWhere('status',\App\Enums\EAssetStatus::LIQUIDATION)
            ->with('asset');
        if($this->searchName){
            $query->whereHas('asset',function(Builder $q){
                $q->where('asset.unsign_text','like','%'. strtolower(removeStringUtf8($this->searchName)).'%');

            });
        }

        $assetLiquidations = $query->get();
        foreach ($assetLiquidations as $assetLiquidation) {
            $assetLiquidation->index = $index++;
        }
        return $assetLiquidations;
    }

    public function headings(): array
    {
        return [
            'STT',
            'MÃ tài sản',
            'Số biên bản',
            'Tên tài sản',
            'Loại tài sản',
            'Số lượng',
            'Số serial',
            'Tình trạng',
            'Ngày báo thanh lý'
        ];
    }

    public function map($assetLiquidation): array
    {
        return [
            $assetLiquidation->index,
            $assetLiquidation->asset->code ?? '',
            $assetLiquidation->number_report ?? '',
            $assetLiquidation->asset->name ?? '',
            $assetLiquidation->asset->category->name ?? '',
            $assetLiquidation->implementation_quantity ?? '',
            $assetLiquidation->asset->series_number ?? '',
            $assetLiquidation->status == \App\Enums\EAssetStatus::LIQUIDATION ? 'Đã thanh lý':'Báo thanh lý',
            reFormatDate($assetLiquidation->implementation_date) ?? '',

        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E','F','G','H','I'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:E1');
            $event->sheet->setCellValue('A1', 'DANH SÁCH TÀI SẢN THANH LÝ');
            $event->sheet->getStyle('A1:E1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:I3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        }, ];

    }
    public function title(): string
    {
        return 'DANH SÁCH TÀI SẢN THANH LÝ';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
