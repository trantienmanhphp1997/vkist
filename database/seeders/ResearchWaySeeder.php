<?php

namespace Database\Seeders;

use App\Models\MasterData;
use Illuminate\Database\Seeder;

class ResearchWaySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 10;
        for($i = 0; $i < $count; $i++) {
            MasterData::query()->create([
                'type' => 19,
                'v_key' => 'Research Way',
                'v_value' => 'Hướng nghiên cứu ' . $i,
                'order_number' => $i
            ]);
        }
    }
}
