<?php
return [
    'breadcrumbs' => [
        'category-field-management-1' => 'Category management',
        'category-field-management-2' => 'Research field management',
        'category-field-list' => 'List of fields of study',
    ],
    'table_column' => [
        'code' => 'CODE',
        'name' => 'FIELD NAME',
        'description' => 'DESCRIPTION',
        'note' => 'NOTE',
        'action' => 'Action',
    ],
    'title' => [
        'create' => 'Create new',
        'edit' => 'Edit',
        'no-result' => 'No results!',
        'part-1' => 'Display',
        'part-2' => 'field of study in',
    ],
    'modal' => [
        'title' => [
            'delete' => 'Delete confirmation',
            'create' => 'Add field of study',
            'edit' => 'Edit field of study',
            'show' => 'Show field of study details',
        ],
        'body' => [
            'delete-warning' => 'Did you delete? This operation cannot be undone!',
            'name' => 'Name of research field',
            'description' => 'Description',
            'note' => 'Note',
        ],
        'footer' => [
            'back' => 'Back',
            'delete' => 'Delete',
            'close' => 'Close',
            'save' => 'Save',
        ]
    ],
    'empty-record' => 'No records',
    'placeholder' => [
        'search' => 'Search',
        'name' => "Enter the name of the field of study",
        'description' => "Enter a description of the field of study",
        'note' => "Enter field notes",
    ],
    'validate' => [
        'name' => 'Name of research field',
        'note' => 'Research field notes',
        'description' => 'Description of research area',
    ],
    'notification' => [
        'edit-success' => 'Edit successful',
        'create-success' => 'New successfully added',
        'delete-fail' => 'Already have ideas related to this field. Cannot delete this field',
    ]
];