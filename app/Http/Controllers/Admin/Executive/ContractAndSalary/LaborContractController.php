<?php

namespace App\Http\Controllers\Admin\Executive\ContractAndSalary;

use App\Http\Controllers\Controller;
use App\Models\LaborContract;
use DB;

class LaborContractController extends Controller {
    public function index() {
        return view('admin.executive.contract-and-salary.labor-contract.index');
    }
}
