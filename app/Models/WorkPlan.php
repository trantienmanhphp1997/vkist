<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class WorkPlan extends BaseModel implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    protected $table='work_plan';
    protected $guarded=['*'];
    protected $fillable = [
        'project_id',
        'content',
        'mission_id',
        'missions',
        'missions_code',
        'start_date',
        'end_date',
        'real_start_date',
        'real_end_date',
        'status',
        'result',
        'admin_id',
        'leader_id',
        'estimated_cost',
        'result',
        'priority_level',
        'source_cost',
        'budget_id',
        'gw_attach_file',
    ];
    private static $searchable = [
        'content'
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
    public function memberList()
    {
        return $this->hasMany(WorkPlanHasUserInfo::class, 'work_plan_id');
    }
    public function fee() {
        return $this->belongsTo(Budget::class, 'budget_id');
    }
}
