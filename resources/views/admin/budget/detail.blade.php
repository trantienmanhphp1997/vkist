@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.budget.detail')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    <!--start news -->
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.budget.general')}}</a> \
                    <span>{{__('data_field_name.budget.detail')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="detail-task information box-idea">
                    <h4 class="border-bottom-header">{{__('data_field_name.budget.detail')}}</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.year')}}</label>
                                        <input type="number" id="datePicker" class="form-control disabled_box"
                                               name="year_created" value="{{$data->year_created}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.estimate')}}</label>
                                        <input type="text" class="form-control disabled_box" placeholder=""
                                               name="estimated_code" value="{{$data->estimated_code}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.code')}}</label>
                                        <input type="text" class="form-control disabled_box" placeholder="" name="code"
                                               value="{{$data->estimated_code}}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.name')}}</label>
                                <input type="text" class="form-control disabled_box" placeholder="" name="name"
                                       value="{{$data->name}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.total')}}</label>
                                        <input type="text" class="form-control disabled_box" placeholder=""
                                               name="total_budget" value="{{numberFormat($data->total_budget) ?? ''}}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.note')}}</label>
                                <textarea class="form-control disabled_box" rows="4" name="note" value="" disabled>
                                    {{$data->note}}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                                <div class="form-group">
                                <label>{{__('data_field_name.budget.content')}}</label>
                                <textarea class="form-control disabled_box" rows="4" name="content" value="" disabled>
                                    {{$data->note}}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.research_project')}}</label>
                                        <input type="text" class="form-control disabled_box" placeholder=""
                                               name="total_budget" value="{{$data->researchProject->name ?? ''}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.department')}}</label>
                                        <input type="text" class="form-control disabled_box" placeholder=""
                                               name="total_budget" value="{{$data->department->name ?? ''}}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="title-plan"> {{__('data_field_name.budget.plan_spending')}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-calendar col-md-12">
                        <div class="row">
                            @foreach ($data->hasPlan as  $val)
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.month') }} {{$val->month_budget}}</label>
                                        <input type="text" class="form-control disabled_box" placeholder=""
                                               name="money_plan[]" value="{{numberFormat($val->money_plan) ?? ''}}" disabled>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @livewire('component.files', ['model_name' => $configFile['model_name'], 'type' => $configFile['type'], 'folder' => $configFile['folder'], 'model_id' => $data->id])

{{--                    @if ($data->status == \App\Enums\EBudgetStatus::DONE)--}}
{{--                        @livewire('common.list-attach-file', ['modelId' => $data->id, 'type' => \App\Enums\EApiApproval::TYPE_BUDGET])--}}
{{--                    @endif--}}

                    <div class="col-md-12">
                        <div class="group-btn2 text-center group-btn ">
                            <a href="{{route('admin.budget.index')}}">
                                <button type="" class="btn btn-cancel"
                                        name="cancel">{{__('common.button.cancel')}}</button>
                            </a>
                            @if ($data->status==\App\Enums\EBudgetStatus::NEW||$data->status==\App\Enums\EBudgetStatus::DENY||$data->status==\App\Enums\EBudgetStatus::WAIT)
                            <button type="button" class="btn btn-save buttonSendApproval">
                                {{__('data_field_name.budget.send_offer')}}
                            </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end news -->

    @livewire('common.api-approval', ['modelId' => $data->id, 'type' => \App\Enums\EApiApproval::TYPE_BUDGET, 'requestId' => $requestId])

@endsection
