<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shopping extends BaseModel
{
    use HasFactory;

    protected $table = 'shopping';

    protected $fillable = [
        'name',
        'note',
        'approved_note',
        'estimate_money',
        'needed_date',
        'status',
        'budget_id',
        'unsign_text',
        'admin_id',
        'proposer_id',
        'executor_id',
        'approver_id',
        'estimated_delivery_date',
    ];

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id');
    }

    public function proposer()
    {
        return $this->belongsTo(User::class, 'proposer_id');
    }

    public function executor()
    {
        return $this->belongsTo(User::class, 'executor_id');
    }

    public function devices()
    {
        return $this->hasMany(ShoppingDevice::class, 'shopping_id');
    }

    public function budget()
    {
        return $this->belongsTo(Budget::class, 'budget_id');
    }

    private static $searchable = [
        'name',
    ];

    public static function getListSearchAble(){
        return self::$searchable;
    }
}
