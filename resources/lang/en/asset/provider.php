<?php
return [
    'breadcrumbs' => [
        'asset-provider-management' => 'Provider Management',
        'asset-provider-list' => 'List of Provider',
    ],
    'form' => [
        'general_info' => 'General info',
        'document' => 'Document',
        'attach_file' => 'Attach file',
    ],
    'table_column' => [
        'name' => 'Provider name',
        'code' => 'Provider code',
        'address' => 'Address',
        'phone_number' => 'Phone number',
        'email' => 'Email',
        'action' => 'Action',
    ],
    'label' => [
        'name' => 'Provider name',
        'code' => 'Provider code',
        'address' => 'Address',
        'tax_code' => 'Tax code',
        'phone_number' => 'Phone number',
        'website' => 'Website',
        'bank_account_number' => 'Bank account number',
        'bank_name_id' => 'Bank name',
        'email' => 'Email',
        'note' => 'Note',
    ],
    'placeholder' => [
        'search' => 'Search',
    ],

    'title' => [
        'create' => 'Create new provider',
        'edit' => 'Edit provider',
        'no-result' => 'No result!',
        'part-1' => 'Display',
        'part-2' => 'loại đề tài trong',
    ],
    'empty-record' => 'No result!',
    
    'notification' => [
        'edit-success' => 'Edit successful',
        'create-success' => 'New successfully added'
    ]
];
