<?php

return [
    [
        'v_key' => 'position',
        'v_value' => 'Trưởng phòng',
        'type' => '1',
        'order_number' => '1',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Phó phòng',
        'type' => '1',
        'order_number' => '2',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Nhân viên',
        'type' => '1',
        'order_number' => '3',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'PI',
        'type' => '1',
        'order_number' => '4',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Researcher',
        'type' => '1',
        'order_number' => '5',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Mechanical Technician',
        'type' => '1',
        'order_number' => '6',
    ],
    [
        'v_key' => 'position',
        'v_value' => '"Team leader R&D management"',
        'type' => '1',
        'order_number' => '7',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'R&D management',
        'type' => '1',
        'order_number' => '8',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Strategic Planning',
        'type' => '1',
        'order_number' => '9',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Tech-based business development',
        'type' => '1',
        'order_number' => '10',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Chief accountant',
        'type' => '1',
        'order_number' => '11',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Accountant',
        'type' => '1',
        'order_number' => '12',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'External Affairs',
        'type' => '1',
        'order_number' => '13',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Nhân viên',
        'type' => '1',
        'order_number' => '14',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'PR',
        'type' => '1',
        'order_number' => '15',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Secretary',
        'type' => '1',
        'order_number' => '16',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Asset Management cum. Bidding ',
        'type' => '1',
        'order_number' => '17',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Head of division',
        'type' => '1',
        'order_number' => '18',
    ],
    [
        'v_key' => 'position',
        'v_value' => '"Team leader HR management"',
        'type' => '1',
        'order_number' => '19',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'HR Management Staff',
        'type' => '1',
        'order_number' => '20',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Construction',
        'type' => '1',
        'order_number' => '21',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Building Operation Management ',
        'type' => '1',
        'order_number' => '22',
    ],
    [
        'v_key' => 'position',
        'v_value' => 'Driver',
        'type' => '1',
        'order_number' => '23',
    ],

];
