
@extends('layouts.master')
@section('title')
    <title>{{__('notification.common.fail.access_error')}}</title>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="mx-auto text-center" style="padding: 0px 0px 120px 0px;">
            <img class="img-fluid" src="/images/Alert.svg" alt="alert"/>
            <h3 class="text-center" style="font-size: 40px;font-weight:bold">{{__('notification.common.fail.warning')}}</h3>
            <p class="py-16">{{__('notification.common.fail.access_warning')}}</p>
          <a href="{{route('home')}}"  class="btn btn-save size14 ">{{__('common.button.back')}}</a>
        </div>
    </div>
</div>
@endsection