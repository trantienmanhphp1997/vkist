<?php

namespace Tests\Unit;

use App\Http\Livewire\Admin\System\Account\AccountEditor;
use App\Http\Livewire\Admin\System\Account\AccountList;
use App\Models\User;
use App\Models\UserInf;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Livewire\Livewire;
use Tests\TestCase;

class AccountControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public $routeIndex = 'admin.system.account.index';
    public $username = 'user.username';
    public $email = 'user.email';

    /** @test */
    public function testAccountIndex()
    {
        $this->actingAs($this->user)->get(route($this->routeIndex))->assertOk();
    }

    /** @test */
    public function testAccountCreateSuccess()
    {
        $this->actingAsAdmin()->get(route('admin.system.account.create'))->assertOk();

        $userInfo = UserInf::factory()->create();

        $data = [
            $this->username => 'test',
            $this->email => 'test@gmail.com',
            'userStatusBool' => true
        ];

        $livewire = Livewire::test(AccountEditor::class, ['userInfo' => $userInfo])
            ->fill($data)
            ->call('save')
            ->assertHasNoErrors();

        $user = User::where('username', 'test')->first();
        $livewire->assertRedirect(route('admin.system.account.edit', $user->id));
    }


    public function testAccountCreateFail() {
        $this->actingAsAdmin()->get(route('admin.system.account.create'))->assertOk();

        $userInfo = UserInf::factory()->create();

        $data = [
            $this->username => '',
            $this->email => '',
            'userStatusBool' => true
        ];

        Livewire::test(AccountEditor::class, ['userInfo' => $userInfo])
            ->fill($data)
            ->call('save')
            ->assertHasErrors([$this->username]);
    }


    /** @test */
    public function testAccountEditSuccess() {
        $this->actingAsAdmin()->get(route($this->routeIndex))->assertOk();

        $user = User::factory()->create();
        $userInfo = UserInf::factory()->create();

        $data = [
            $this->username => 'editedUsername',
            $this->email => 'editedusername@gmail.com',
            'userStatusBool' => true
        ];

        $livewire = Livewire::test(AccountEditor::class, ['user' => $user, 'userInfo' => $userInfo])
            ->fill($data)
            ->call('save')
            ->assertHasNoErrors();

        $user = User::where('username', 'editedUsername')->first();
        $livewire->assertRedirect(route('admin.system.account.edit', $user->id));
    }

    public function testAccountEditFail() {
        $this->actingAsAdmin()->get(route($this->routeIndex))->assertOk();

        $user = User::factory()->create();
        $userInfo = UserInf::factory()->create();

        $data = [
            $this->username => '',
            $this->email => '',
            'userStatusBool' => true
        ];

        Livewire::test(AccountEditor::class, ['user' => $user, 'userInfo' => $userInfo])
            ->fill($data)
            ->call('save')
            ->assertHasErrors([$this->username]);
    }

    public function testAccountDelete() {
        $this->actingAsAdmin()->get(route($this->routeIndex))->assertOk();

        $userInfo = UserInf::factory()->create();

        $data = [
            $this->username => 'test',
            $this->email => 'test@gmail.com',
            'userStatusBool' => true
        ];

        Livewire::test(AccountEditor::class, ['userInfo' => $userInfo])
            ->fill($data)
            ->call('save')
            ->assertHasNoErrors();

        $user = User::where('username', 'test')->first();

        Livewire::test(AccountList::class)->call('getIdDelete', $user->id)->call('delete');
    }

}
