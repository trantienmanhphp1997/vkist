<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ApprovalRequest;
use Illuminate\Http\Request;

class ApprovalController extends Controller
{
    public function approval(ApprovalRequest $request)
    {
        return response()->json([
            'success' => true,
            'request_id' => $request->request_id,
            'type' => $request->type,
            'status' => $request->status,
            'functionName' => $request->functionName,
            'data' => $request->data,
        ]);
    }
}
