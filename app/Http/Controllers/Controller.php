<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	public function __construct()
	{
        $name = Route::getCurrentRoute()->getName();
        $name = str_replace('update', 'edit', $name);
        $name = str_replace('show', 'index', $name);
        $name = str_replace('detail', 'index', $name);
        $name = str_replace('store', 'create', $name);
        $name = str_replace('duplicate', 'create', $name);

        $arr_role = explode(".", $name);
        $action = end($arr_role);
        if($action == 'show' || $action == 'detail'){
           $action = ['show', 'detail', 'index'];
        }
        if($action == 'update'){
            $action = ['edit', 'update'];
        }
        if($action == 'store' || $action == 'duplicate'){
           $action = ['store', 'create', 'duplicate'];
        }
        self::middleware('role_or_permission:administrator|'
          . '|' . $name
          , ['only' => $action]);
	}
}
