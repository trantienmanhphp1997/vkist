<?php

namespace App\Enums;

class EScheduleType {
    const NONE = 0;
    const MOMENT = 1;
    const DAILY = 2;
    const WEEKLY = 3;
    const CUSTOMIZE = 4;

    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            self::NONE => __('data_field_name.notification.schedule_type_none'),
            self::MOMENT => __('data_field_name.notification.schedule_type_moment'),
            self::DAILY => __('data_field_name.notification.schedule_type_daily'),
            self::WEEKLY => __('data_field_name.notification.schedule_type_weekly'),
            self::CUSTOMIZE => __('data_field_name.notification.schedule_type_customize'),
        ];
    }
}
