<?php

namespace App\Http\Livewire\Admin\Asset\LostCancelLiquidation;

use App\Enums\EAssetSituation;
use App\Enums\EAssetStatus;
use App\Models\Asset;
use App\Models\AssetAllocatedRevoke;
use Livewire\Component;

class AssetInfo extends Component
{

    public $idConfirm;
    protected $listeners=[
        'loadAssetInfo'
    ];

    public $assetSituation;
    public $assetStatuss;
    public function mount()
    {
        $this->assetSituation = [
            EAssetSituation::SITUATION_NOT_USE =>__('data_field_name.asset.no_use'),
            EAssetSituation::SITUATION_USING => __('data_field_name.asset.using'),
        ];
        $this->assetStatuss = EAssetStatus::getList();
    }
    public function render()
    {
        $assetDetail=[];
        if ($this->idConfirm){
            $detailNotice = AssetAllocatedRevoke::findOrFail($this->idConfirm);
            if ($detailNotice){
                $assetDetail= Asset::findOrFail($detailNotice->asset_id);
            }
        }
        return view('livewire.admin.asset.lost-cancel-liquidation.asset-info',['assetDetail'=>$assetDetail]);
    }
    public function loadAssetInfo($idConfirm){
        $this->idConfirm=$idConfirm;
    }
}
