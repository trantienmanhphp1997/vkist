@extends('layouts.master')
@section('title')
    <title>Danh mục tin</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._newsLeft')
@endsection
@section('content')
    @livewire('admin.news.index', ['status' => $status])
@endsection
