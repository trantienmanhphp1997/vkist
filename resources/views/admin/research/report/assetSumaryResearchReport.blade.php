<div class="report-main-text text-center">
    <h3>{{__('research/report.report4-page.title')}}</h3>
    <p>{{__('research/report.report4-page.sub-title')}}</p>
</div>
<div class="col-md-12 download-report-btn">
    @if(checkPermission('admin.asset.report.download'))
        <div class=" float-right">
            {{--                                wire:click='export'--}}
            <button type="button"  class="btn par6" data-toggle="modal" data-target="#exportModal"
                    title="{{__('common.button.export_file')}}"><img src="/images/filterdown.svg" alt="filter-down"></button>
        </div>
    @endif
</div>
<div class="table-responsive">
    <table class="table table-general working-plan">
        <thead>
        <tr class="border-radius text-center">
            <th scope="col" class="text-center">
                {{__('research/report.table.stt')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report4-page.table.status')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report4-page.table.department')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report4-page.total')}}
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="text-center">
                1
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.in-use')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_in_using']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_in_using']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                2
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.in-maintenance')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_maintenance']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_maintenance']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                3
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.report-lost')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_lost']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_lost']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                4
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.liquidation')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_liquidation']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_liquidation']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                5
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.in-repaired')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_repaired']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_repaired']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                6
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.report-liquidation')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_liquidation']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_liquidation']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                7
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.report-cancel')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_cancel']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_cancel']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                8
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.losted')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_lost']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_lost']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                9
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.wait-repaired')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_wait_repaired']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_wait_repaired']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                10
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.wait-maintenance')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_wait_maintenance']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_wait_maintenance']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                11
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.not-use')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_not_use']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_not_use']['count']}}
            </td>
        </tr>
        <tr>
            <td class="text-center">
                12
            </td>
            <td class="text-center">
                {{__('research/report.report4-page.table.cancel')}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_cancel']['department_name']}}
            </td>
            <td class="text-center">
                {{$assetSumaryReport['report_asset_has_cancel']['count']}}
            </td>
        </tr>
        <tr class="border-radius">
            <td class="text-center" style="background: #F6F6F7;">

            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{__('research/report.report2-page.total')}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryReport['count_department']}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryReport['countAssetAmount']}}
            </td>
        </tr>
        </tbody>
    </table>
    {{--                    </div>--}}
    {{--                    @if (count($data) > 0)--}}
    {{--                        {{ $data->links() }}--}}
    {{--                    @else--}}
    {{--                        <div class="title-approve text-center">--}}
    {{--                            <span>{{ __('common.message.empty_search') }}</span>--}}
    {{--                        </div>--}}
    {{--                    @endif--}}
</div>
