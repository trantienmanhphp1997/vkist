<div>
    <div class="row">
        <div class="col-md-12">
            
            <h2 class="title-main text-center">{{__('data_field_name.working_process.following_research')}} 
            @foreach ($data as $val)

                @if($canEdit)
                <button type="button" class="btn btn-edit2" title="edit" data-target="#directResearch" data-toggle="modal" title="edit" wire:click="edit({{ $val->id }})">
                    <img src="/images/pent2.svg" alt="">
                </button>
                @endif
            </h2>
                <p class="text-center pd20">
                    {{$val->research}}
                </p>
            @endforeach
        </div>
    </div>
    <!-- Modal Create -->
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="directResearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true close-btn">×</span>
                     </button>
                    </div>
                    <div class="modal-body">
                        <form>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">HƯỚNG NGHIÊN CỨU</span></label>
                             <select  class="form-control" id="floatingTextarea2"  name="research" wire:model.lazy="research">
                                   @foreach($masterdata as $row)
                                       <option value="{{$row->v_value}}">{{$row->v_value}}</option>
                                   @endforeach
                             </select>
                            @error('research') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" id="close-modal-edit-direct-research" class="btn btn-secondary close-btn" data-dismiss="modal">Đóng</button>
                    <button type="button" wire:click.prevent="update()"  class="btn btn-primary close-modal">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</form>
</div>

<!-- end modal create -->
