<?php
return [
    'title' => 'Timesheets',
    'time-sheets' => 'Timesheet :month',
    'personal-info' => [
        'title' => 'PERSONAL INFOMATION',
        'name' => 'Full name',
        'position' => 'Position',
        'code' => 'Code',
        'base_salary_coefficient' => 'Base salary coefficient',
        'bank_account' => 'Bank account',
        'bank_name' => 'Bank name'
    ],
    'timekeeping-info' => [
        'title' => 'TIMEKEEPING INFOMATION',
        'period' => 'Period',
        'number-of-paid-leave-days' => 'Number of paid leave days',
        'number-of-days-of-unpaid-leave' => 'Number of days of unpaid leave',
        'standard-work-by-month' => 'Number of working days by month',
        'actual-number-of-working-days' => 'Actual number of working days',
        'total-number-of-paid-working-days' => 'Total number of paid working days'
    ],
    'table_column' => [
        'date' => 'Date',
        'timekeeping' => 'Timekeeping',
    ],
    'symbol-of-timesheets' => [
        'title' => 'Symbol of timesheets',
        'working-day' => 'Working day',
        'annual-leave' => 'Annual-leave',
        'paid-leave' => 'Paid-leave',
        'holiday' => 'Holiday',
        'meetings' => 'Meetings',
        'paid-personal-leave' => 'Paid personal leave',
        'unpaid-personal-leave' => 'Unpaid personal leave',
        'sick-leave' => 'Sick leave',
        'maternity-leave' => 'Maternity leave',
        'leave-without-reason' => 'Leave without reason',
        'labor-accident' => 'Labor accident',
    ],
];