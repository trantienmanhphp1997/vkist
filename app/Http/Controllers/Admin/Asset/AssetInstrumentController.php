<?php

namespace App\Http\Controllers\Admin\Asset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AssetInstrumentController extends Controller
{
    //
    public function index(){
        return view('admin.asset.instrument.index');
    }
}
