<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class SurveyRequest extends BaseModel
{
    use HasFactory;
    protected $table = "survey_request";
    protected $guarded = ['*'];
    private static $searchable = [
        'name', 'survey_content'
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }

    public function userInfos()
    {
        return $this->belongsToMany(UserInf::class, 'survey_request_has_user_info', 'survey_request_id', 'user_info_id');
    }
}
