<div class="col-md-12">

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <h4 class="title-plan text-uppercase">{{ __('data_field_name.work_plan.edit.title_item2') }}</h4>
            </div>
            <div class="col-md-6">
                <div class="form-group float-right">
                    <button class="btn btn-viewmore-news mr0 " onclick="$('#work_date').val('')" wire:click.prevent="storeBusinessFee" wire:loading.attr="disabled">
                        <img src="/images/plus2.svg" alt="plus">{{ __('data_field_name.work_plan.create_btn') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="box-calendar col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{ __('data_field_name.work_plan.edit.date') }}</label>
                    @include('layouts.partials.input._inputDate', ['input_id' => 'work-date', 'set_null_when_enter_invalid_date' => true, 'place_holder' => 'dd/mm/yyyy'])
                    @error('date')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>{{ __('data_field_name.work_plan.edit.user_name') }}</label>
                    <select class="form-control form-control-lg" wire:model.defer="user_info_id">
                        <option>{{ __('data_field_name.work_plan.create.selected_default') }}</option>
                        @foreach ($user_info_list as $value)
                            <option value="{{ $value->user_info_id }}">{{ $value->userInfo->fullname ?? '' }}</option>
                        @endforeach
                    </select>
                    @error('user_info_id')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{ __('data_field_name.work_plan.edit.fee_name') }}</label>
                    <select class="form-control form-control-lg" wire:model.defer="business_fee_id">
                        <option>{{ __('data_field_name.work_plan.create.selected_default') }}</option>
                        @foreach ($business_fee_list as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    @error('business_fee_id')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>{{ __('data_field_name.work_plan.edit.fee') }}</label>
                    <input type="text" class="form-control format_number" placeholder="" wire:model.defer="fee" maxlength="18">
                    @error('fee')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-12">
                @livewire('component.files', [
                    'name' => __('data_field_name.work_plan.edit.file_btn'),
                    'model_name' => App\Models\WorkPlanHasBusinessFee::class,
                    'model_id' => null,
                    'type' => config('common.type_upload.WorkPlanHasBusinessFee'),
                    'folder' => 'work-plan-has-business-fee'
                ], key($key))
            </div>
            {{-- <div class="col-md-2">
                    <form wire:submit.prevent="submit">
                        <div class="form-group pull-right upload-file">
                            <input type="file"
                                   class="custom-file-input box-new"
                                   wire:model.lazy="file_fee" id="input-file">
                            <span class="file">{{__('data_field_name.work_plan.edit.file_btn')}} <img
                                    src="/images/Clip.svg" alt="" height="24"></span>
                        </div>
                    </form>
                </div> --}}
        </div>
    </div>
    @foreach ($business_fee as $item)
        <div class="box-calendar col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{ __('data_field_name.work_plan.edit.date') }}</label>
                        <input type="text" class="form-control" value="{{ $item->working_day ? date('d/m/Y', strtotime($item->working_day)) : '' }}" disabled>
                    </div>
                    <div class="form-group">
                        <label>{{ __('data_field_name.work_plan.edit.user_name') }}</label>
                        <select class="form-control form-control-lg" disabled>
                            <option>{{ __('data_field_name.work_plan.create.selected_default') }}</option>
                            @foreach ($user_info_list as $val)
                                <option value="{{ $val->user_info_id }}" {{ $val->user_info_id == $item->user_info_id ? 'selected' : '' }}>{{ $val->userInfo->fullname ?? '' }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{ __('data_field_name.work_plan.edit.fee_name') }}</label>
                        <select class="form-control form-control-lg" disabled>
                            <option>{{ __('data_field_name.work_plan.create.selected_default') }}</option> -->
                            @foreach ($type_fee as $key => $val)
                                <option value="{{ $key }}" {{ $key == $item->business_fee_id ? 'selected' : '' }}>{{ $val }}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('data_field_name.work_plan.edit.fee') }}</label>
                        <input type="text" class="form-control" value="{{ $item->money }}" disabled>
                    </div>
                </div>
                <div class="col-md-12">
                    {{-- @foreach ($fileFee as $file)
                        @if ($file->model_id == $item->id)
                            <div class="form-group">
                                <div class="attached-files mb-2 mr-3">
                                    <img src="/images/File.svg" alt="file"/>
                                    <div class="content-file">
                                        <p>{{$file->file_name}}</p>
                                        <p class="kb">{{$item->size_file}}</p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach --}}
                    @livewire('component.files', [
                        'name' => __('data_field_name.work_plan.edit.file_btn'),
                        'model_name' => App\Models\WorkPlanHasBusinessFee::class,
                        'model_id' => $item->id,
                        'type' => config('common.type_upload.WorkPlanHasBusinessFee'),
                        'canUpload' => false
                    ], key($item->id))
                </div>

            </div>
        </div>
    @endforeach

    <script>
        window.addEventListener('stored-business-fee-event', () => {
            document.getElementById('work-date').value = '';
        })
    </script>
</div>
