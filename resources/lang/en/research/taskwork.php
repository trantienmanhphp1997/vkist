<?php
return [
    'status' => 'Status',
    'level' => 'Priority level',
    'project-code'=>'Project code',
    'group-task' => 'Group task',
    'personal-task' => 'My task',
    'select-status' => [
        'undo' => 'Not Working yet',
        'on-working' => 'On Working',
        'approve' => 'Wait approval',
        'done' => 'Done'
    ],
    'select-work-type' => [
        'main' => 'Main task',
        'sub' => 'Sub task',
        'detail' => 'Task detail'
    ],
    'select-level' => [
        'low' => 'Low',
        'medium' => 'Medium',
        'high' => 'High',
        'emergency' => 'Emergency'
    ],
    'table' => [
        'project-code' => 'Project code',
        'work-type' => 'Work type',
        'task-code' => 'Task code',
        'priority-level' => 'Priority level',
        'name' => 'Name of task',
        'plan' => 'Plan',
        'start' => 'Start',
        'end' => 'End',
        'reality' => 'Reality',
        'status' => 'Status',
        'assign' => 'Assign',
        'preview' => 'Reviewer'
    ],
    'form-data' => [
        'topic' => 'Topic name',
        'main-mission' => 'Main task',
        'select-main-task'=>'Select main task',
        'mission' => 'Task',
        'select-task'=>'Select task',
        'level' => 'Priority level',
        'status' => 'Status',
        'work-type' => 'Task work type',
        'actual-time-topic' => 'Real time by topic plan',
        'name' => 'Task name',
        'start-date' => 'Start date',
        'end-date' => 'End date',
        'description' => 'Task description',
        'follower' => 'Follower',
        'assign' => 'Assign',
        'reviewer' => 'Preview',
        'approve' => 'Approve',
        'file' => 'Attach file',
        'member' => 'Member',
        'topic-start-date' => 'Start time of topic plan',
        'topic-end-date' => 'End time of topic plan',
        'real-start-date'=>'real start date',
        'real-end-date'=>'real end date',
    ],
    'create' => 'Create task',
    'update' => 'Update task',
    'index'=>'List task work',
    'not-khown-project'=>'Not operation project yet',
    'msg' => [
        'create' => [
            'success' => 'Create task success',
            'fail' => 'Create task fail'
        ],
        'update'=>[
            'success'=>'Update task success',
            'fail'=>'Úpdate task fail'
        ]
    ],
    'member-exist'=>'This user has already exist in list follower',
    'member-remove-success'=>'Remove user success',
    'member-add-success'=>'Add user success',
    'person'=>[
        'follow'=>'List follower',
        'assign'=>'Assigner',
        'preview'=>'Previewer',
        'approval'=>'Approve'
    ],
    'unknown'=>'unknown',
    'name-follower'=>'Name of person follow task'
];
