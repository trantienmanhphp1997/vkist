<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpertCost extends Model
{
    use HasFactory;

    protected $table = "expert_cost";
    protected $fillable = [
        'expert_name',
        'nationality',
        'organization',
        'content',
        'working_time',
        'salary',
        'total',
        'state_capital',
        'other_capital',
    ];

    public function topicFeeDetail() {
        return $this->belongsTo(TopicFeeDetail::class, 'topic_fee_detail_id');
    }
}
