<div class="table-responsive">
    <div class="col-md-12 my-5">
        <p class="float-right">  {{__('research/productivity.modal.schedule')}} : <span class="font-weight-bold">{{$totalTaskExpired}}</span> {{ __('common.time.day') }}
        </p>
    </div>
    <table class="table table-general" style="@if(count($taskListWithPagination) > 10)  overflow-y: scroll;
        height: 800px;
        display: block; @endif">
        <thead>
        <tr class="border-radius text-center">
            <th scope="col" class="border-radius-left">
                {{__('research/taskwork.table.task-code')}}
            </th>
            <th scope="col">
                {{__('research/taskwork.table.name')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/taskwork.table.assign')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/productivity.modal.expired-date')}}
            </th>
        </tr>
        </thead>
        <tbody >

        @foreach($taskListWithPagination as $task)
            <tr class="border-radius text-center">
                <td  class="border-radius-left">
                    <a href="{{ route('admin.research-project.task-work.edit', $task->id) }}">
                        {{$task->code}}
                    </a>
                </td>
                <td>
                    {{$task->name ?? ''}}
                </td>
                <td  class="text-center">
                    {{$task->assigner->name ?? ''}}
                </td>
                <td  class="text-center">
{{--                    {{rand(1,100)}} {{__('research/productivity.modal.day')}}--}}
                    {{$task->delayDate ?? 0}}
                </td>

        @endforeach

        </tbody>
    </table>
    @if (count($taskListWithPagination) > 0)

    @else
        <div class="title-approve text-center">
            <span>{{ __('common.message.empty_search') }}</span>
        </div>
    @endif
</div>
