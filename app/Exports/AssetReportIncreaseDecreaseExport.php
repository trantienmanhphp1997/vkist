<?php

namespace App\Exports;

use App\Models\AssetCategory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class AssetReportIncreaseDecreaseExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            __('research/report.table.stt'),
            __('research/report.report1-page.table.code'),
            __('research/report.report1-page.table.name'),
            __('research/report.report1-page.table.begin-period'),
            __('research/report.report1-page.table.added'),
            __('research/report.report1-page.table.move-to'),
            __('research/report.report1-page.table.move-away'),
            __('research/report.report1-page.table.lost'),
            __('research/report.report1-page.table.cancel'),
            __('research/report.report1-page.table.liquidation'),
            __('research/report.report1-page.table.end-period')
        ];
    }

    public function map($data): array
    {
        $rows = [];
        $rows[] =  [
            '*',
            $data->code,
            $data->name,
            $data->countAssetBeginQuanity != null ? $data->countAssetBeginQuanity : 0,
            $data->countAssetAdded != null ? $data->countAssetAdded : 0,
            $data->move_to != null ? $data->move_to : 0,
            $data->move_away != null ? $data->move_away : 0 ,
            $data->countAssetLosted != null ? $data->countAssetLosted : 0,
            $data->countAssetCancel != null ? $data->countAssetCancel : 0,
            $data->countAssetLidation != null ? $data->countAssetLidation : 0,
            $data->countAssetEndQuanity != null ? $data->countAssetEndQuanity : 0,
            'is_parent' => true
        ];
        foreach ($data->asset as $index => $data) {

            $rows[] = [
                $index + 1,
                $data->code,
                $data->name,
                $data->beginPeriodQuanity != null ? $data->beginPeriodQuanity : 0,
                $data->added != null ? $data->added : 0,
                $data->move_to != null ? $data->move_to : 0,
                $data->move_away != null ? $data->move_away : 0,
                $data->losted != null ? $data->losted : 0,
                $data->cancel != null ? $data->cancel : 0,
                $data->liquidation != null ? $data->liquidation : 0,
                $data->endPeriodQuanity != null ? $data->endPeriodQuanity : 0,
            ];
        }
        $this->excel_data = $rows ;

        return $rows;
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);


            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J','K'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
                $event->sheet->getColumnDimension($alphabet)->setWidth(50);
            }
            $event->sheet->mergeCells('A1:K1');
            $event->sheet->setCellValue('A1', 'BÁO CÁO TỔNG HỢP TÀI SẢN');
            $event->sheet->getStyle('A1:H1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:K3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);
        },];

    }

    public function title(): string
    {
        return __('research/report.report1-page.title');
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
