<div class="inner-tab">
    <table class="table">
        <thead>
        <tr class="border-radius">
            <th scope="col" class="border-radius-left">{{ __('data_field_name.topic.stt') }}</th>
            <th scope="col">{{ __('data_field_name.topic.member_name') }}</th>
            <th scope="col">{{ __('data_field_name.topic.qualification') }}</th>
            <th scope="col">{{ __('data_field_name.topic.role') }}</th>
            <th scope="col">{{ __('data_field_name.topic.research_way') }}</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($members as $member)
            <tr>
                <td>{{ ($members->currentPage() - 1) * $members->perPage() + $loop->iteration }}</td>
                <td>{{ $member->userInfo->fullname ?? 'name' }}</td>
                <td>{{ $member->userInfo->academicLevel->v_value ?? 'qualification' }}</td>
                <td>{{ $member->role->v_value ?? 'role' }}</td>
                <td>{{ $member->research->v_value ?? 'research' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if(count($members) > 0)
        {{ $members->links() }}
    @else
        <div class="title-approve text-center">
            <span>{{__('data_field_name.system.role.no_result')}}</span>
        </div>
    @endif
</div>
