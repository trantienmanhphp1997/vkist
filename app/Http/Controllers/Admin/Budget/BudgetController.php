<?php

namespace App\Http\Controllers\Admin\Budget;

use App\Enums\EBudgetStatus;
use App\Enums\ERouteName;
use App\Enums\ETypeUpload;
use App\Http\Controllers\Controller;
use App\Models\Audit;
use App\Models\Budget;
use App\Models\BudgetHasExpensePlan;
use App\Models\Department;
use App\Models\ResearchProject;
use App\Models\TopicFee;
use App\Service\Community;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Enums\EValidation;

class BudgetController extends Controller
{
    const MAX_CODE = 'max:30';
    const MAX_CONTENT = 'max:255';
    const MAX_MONEY = 'max:14';

    public function index()
    {
        return view(ERouteName::ROUTE_BUDGET_INDEX);
    }

    public function create($topic_fee)
    {
        $model_name = Budget::class;
        $type = ETypeUpload::BUDGET;
        $folder = app($model_name)->getTable();
        $department = Department::all()->pluck('name', 'id');
        $department->prepend(__('data_field_name.budget.chose_department'), '');
        $researchProject = ResearchProject::all()->pluck('name', 'id');
        $researchProject->prepend(__('data_field_name.budget.chose_project'), '');

        return view('admin.budget.create', ['topic_fee' => $topic_fee, 'model_name' => $model_name,
            'type' => $type, 'folder' => $folder, 'department' => $department, 'researchProject' => $researchProject]);
    }

    public function duplicate($id)
    {
        $data_budget = Budget::findOrFail($id);
        $model_name = Budget::class;
        $type = ETypeUpload::BUDGET;
        $folder = app($model_name)->getTable();
        $department = Department::all()->pluck('name', 'id');
        $department->prepend(__('data_field_name.budget.chose_department'), '');
        $researchProject = ResearchProject::all()->pluck('name', 'id');
        $researchProject->prepend(__('data_field_name.budget.chose_project'), '');

        return view('admin.budget.add.choose_budget', ['data_budget' => $data_budget, 'model_name' => $model_name,
            'type' => $type, 'folder' => $folder, 'department' => $department, 'researchProject' => $researchProject]);

    }

    public function topicBudget($id)
    {
        $topicFee = TopicFee::findOrFail($id);
        $model_name = Budget::class;
        $type = ETypeUpload::BUDGET;
        $folder = app($model_name)->getTable();
        $department = Department::all()->pluck('name', 'id');
        $department->prepend(__('data_field_name.budget.chose_department'), '');
        $researchProject = ResearchProject::all()->pluck('name', 'id');
        $researchProject->prepend(__('data_field_name.budget.chose_project'), '');

        return view('admin.budget.create', ['topicFee' => $topicFee, 'model_name' => $model_name,
            'type' => $type, 'folder' => $folder, 'department' => $department, 'researchProject' => $researchProject]);
    }

    public function store(Request $request)
    {
        if ($request->has('cancel')) {
            File::where('model_name', Budget::class)
                ->where('type', ETypeUpload::BUDGET)
                ->where('admin_id', auth()->id())
                ->where('model_id', null)
                ->delete();
            return redirect()->route(ERouteName::ROUTE_BUDGET_INDEX);
        } else if ($request->has('save')) {

            $this->validate($request, [
                'year_created' => 'required|integer|between:2020,2050',
                'estimated_code' => ['required', 'regex:/^[A-Z0-9' . EValidation::ALPHABET . ']+$/', self::MAX_CODE],
                'code' => ['required', 'unique:budget,code,NULL,id,deleted_at,NULL', 'regex:/^[A-Z0-9' . EValidation::ALPHABET . ']+$/', self::MAX_CODE],
                'name' => ['required', 'unique:budget,name,NULL,id,deleted_at,NULL', 'regex:/^[a-zA-Z0-9' . EValidation::ALPHABET . ']+$/', 'max:100'],
                'total_budget' => ['required', 'regex:/^[0-9\.\,]+$/', self::MAX_MONEY],
                'content' => ['regex:/^[a-zA-Z0-9' . EValidation::ALPHABET . '\.\,\-\(\)\/\\\\ ]+$/', self::MAX_CONTENT, 'nullable'],
                'note' => ['regex:/^[a-zA-Z0-9' . EValidation::ALPHABET . '\.\,\-\(\)\/\\\\ ]+$/', self::MAX_CONTENT, 'nullable'],
                'money_plan.*' => ($request->plan_spending == 'allocated') ? 'required|regex:/^[0-9\.\,]+$/|'.self::MAX_MONEY : '',
            ], [], [
                'year_created' => __('data_field_name.budget.year'),
                'estimated_code' => __('data_field_name.budget.estimate'),
                'code' => __('data_field_name.budget.code'),
                'name' => __('data_field_name.budget.name'),
                'total_budget' => __('data_field_name.budget.total'),
                'content' => __('data_field_name.budget.content'),
                'note' => __('data_field_name.budget.note'),
                'money_plan.*' => __('data_field_name.budget.money_plan'),
            ]);

            $Budget = new Budget;
            $Budget->year_created = $request->year_created;
            $Budget->estimated_code = $request->estimated_code;
            $Budget->code = $request->code;
            $Budget->name = $request->name;
            $Budget->total_budget = Community::getAmount($request->total_budget);
            $Budget->content = $request->content;
            $Budget->note = $request->note;
            $Budget->department_id = $request->department_id;
            $Budget->research_project_id = $request->research_project_id;
            $Budget->status = EBudgetStatus::NEW;
            if ($request->plan_spending == 'allocated') {
                $Budget->allocated = 1;
            } else {
                $Budget->allocated = 0;
            }
            $Budget->save();
            if ($request->plan_spending == 'allocated') {
                $data_month = $request->money_plan;
                $data = [];
                foreach ($data_month as $key => $val) {

                    $data_insert = [
                        'budget_id' => $Budget->id,
                        'month_budget' => $key + 1,
                        'money_plan' => Community::getAmount($val),
                    ];
                    if ($data_insert['money_plan'] == '') {
                        $data_insert = [
                            'budget_id' => $Budget->id,
                            'month_budget' => $key + 1,
                            'money_plan' => 0,
                        ];

                    } else {
                        $data_insert = [
                            'budget_id' => $Budget->id,
                            'month_budget' => $key + 1,
                            'money_plan' => Community::getAmount($val),
                        ];
                    }
                    $data[] = $data_insert;
                }
                if ($data) {
                    BudgetHasExpensePlan::insert($data);
                }
            }
            \App\Models\File::where('model_name', Budget::class)
                ->where('type', ETypeUpload::BUDGET)
                ->where('model_id', null)
                ->update([
                    'model_id' => $Budget->id,
                    'status' => 1
                ]);

            return redirect()->route(ERouteName::ROUTE_BUDGET_INDEX)
                ->with('success', __('notification.common.success.add'));
        }
    }

    public function listBudgetApproved()
    {
        return view('admin.budget.budget-list-approved');
    }


    public function edit($id)
    {
        $data = Budget::findOrFail($id);
        if ($data->status == EBudgetStatus::DONE||$data->status==EBudgetStatus::WAIT_CHANGE){
            if ($data->allocated == 1) {
                $model_name = BudgetHasExpensePlan::class;
                $type = Config::get('common.type_upload.BudgetHasExpensePlan');
                $folder = app($model_name)->getTable();
            } else {
                $model_name = Budget::class;
                $type = Config::get('common.type_upload.BudgetTotalReal');
                $folder = app($model_name)->getTable();
            }
            $department = Department::all()->pluck('name', 'id');
            $department->prepend(__('data_field_name.budget.chose_department'), '');
            $researchProject = ResearchProject::all()->pluck('name', 'id');
            $researchProject->prepend(__('data_field_name.budget.chose_project'), '');

            return view('admin.budget.edit', ['data' => $data, 'model_name' => $model_name, 'type' => $type, 'folder' => $folder, 'department' => $department, 'researchProject' => $researchProject]);
        }else{
            return redirect()->route('admin.budget.list-budget-approved.index')->with('error', __('notification.common.fail.no_edit'));
        }


    }

    public function changeBudget($id)
    {
        $data = Budget::findOrFail($id);
        $total_draft = 0;
        $money_plan_draft = [];
        if ($data->request_gw_data != null) {
            if ($data->allocated == 1) {
                $data_draft = $data->request_gw_data;
                $decode = json_decode($data_draft);

                $money_plan = $decode->money_plan;
                $total_draft = $decode->total_budget;
                $money_plan_draft = explode(" ", $money_plan);
            } else {
                $data_draft = $data->request_gw_data;
                $decode = json_decode($data_draft);
                $total_draft = $decode->total_budget;
            }

        }


        if ($data->allocated == 1) {
            $model_name = BudgetHasExpensePlan::class;
            $type = Config::get('common.type_upload.BudgetHasExpensePlan');
            $folder = app($model_name)->getTable();
        } else {
            $model_name = Budget::class;
            $type = Config::get('common.type_upload.BudgetTotalReal');
            $folder = app($model_name)->getTable();
        }

        $requestId = time() . random_int(0,99);

        return view('admin.budget.change-budget', ['data' => $data, 'model_name' => $model_name,
            'type' => $type, 'folder' => $folder, 'total_draft' => $total_draft, 'money_plan_draft' => $money_plan_draft,
            'requestId' => $requestId]);

    }

    public function updateChange($id, Request $request)
    {
        $this->validate($request, [
            'money_plan.*' => self::MAX_MONEY.'|required',
        ], [], [
            'money_plan.*' => __('data_field_name.budget.money_plan'),
        ]);

        if ($request->has('save')) {
            $budget = Budget::findOrFail($id);
            if ($budget->request_gw_status == \App\Enums\EBudgetStatus::WAIT) {
                $budget->request_gw_data = $budget->request_gw_data;
                $budget->save();
            } else {
                if ($budget->allocated == 1) {
                    $plan = $request->money_plan;
                    $data_plan = implode(" ", $plan);
                    $total = Community::getAmount($request->total_budget);

                    $data_json = ['total_budget' => $total, 'money_plan' => $data_plan];

                    $budget->request_gw_data = json_encode($data_json);
                    $budget->request_gw_status = \App\Enums\EBudgetStatus::WAIT;
                    $budget->save();
                } else {
                    $total = Community::getAmount($request->total_budget);
                    $data_json = ['total_budget' => $total];
                    $budget->request_gw_data = json_encode($data_json);
                    $budget->request_gw_status = \App\Enums\EBudgetStatus::WAIT;
                    $budget->save();
                }

            }

            return redirect()->route(ERouteName::ROUTE_BUDGET_APPROVED_INDEX)->with('success', __('notification.common.success.update'));
        } else if ($request->has('cancel')) {
            return redirect()->route(ERouteName::ROUTE_BUDGET_APPROVED_INDEX);
        }
    }


    public function update($id, Request $request)
    {
        $this->validate($request, [
            'money_real.*' => self::MAX_MONEY,
        ], [], [
            'money_real.*' => __('data_field_name.budget.real_money'),
        ]);

        if ($request->has('save')) {
            $budget = Budget::find($id);
            if ($budget->allocated == 1) {
                foreach ($request->money_real as $key => $value) {
                    BudgetHasExpensePlan::where('budget_id', $id)->where('month_budget', $key + 1)->update([
                        'money_real' => Community::getAmount($value),
                    ]);
                }
            } else {
                $budget->update([
                    'total_real_budget' => Community::getAmount($request->total_real_budget),
                ]);
                \App\Models\File::where('model_name', Budget::class)
                    ->where('type', Config::get('common.type_upload.BudgetTotalReal'))
                    ->where('status', -1)
                    ->where('model_id', $budget->id)
                    ->update([
                        'status' => 1,
                    ]);
            }


            return redirect()->route(ERouteName::ROUTE_BUDGET_APPROVED_INDEX)->with('success', __('notification.common.success.update'));
        } else if ($request->has('cancel')) {
            return redirect()->route(ERouteName::ROUTE_BUDGET_APPROVED_INDEX);
        }
    }

    public function show($id)
    {
        $data = Budget::findOrFail($id);
        $data->hasPlan;
        $configFile = $this->configFile();
        $requestId = time() . random_int(0,99);
        return view('admin.budget.detail', ['data' => $data, 'configFile' => $configFile, 'requestId' => $requestId]);
    }

    public function budgetStatus()
    {

        return view('admin.budget.budget-status');
    }

    public function editBudget($idBudget)
    {
        $data = Budget::with('hasPlan')
            ->findOrFail($idBudget);
        if ($data->status == EBudgetStatus::NEW||$data->status==EBudgetStatus::DENY){
            $configFile = $this->configFile();
            $department = Department::all()->pluck('name', 'id');
            $department->prepend(__('data_field_name.budget.chose_department'), '');
            $researchProject = ResearchProject::all()->pluck('name', 'id');
            $researchProject->prepend(__('data_field_name.budget.chose_project'), '');

            return view('admin.budget.edit-budget', ['data' => $data, 'configFile' => $configFile, 'department' => $department, 'researchProject' => $researchProject]);
        }else{
            return redirect()->route('admin.budget.index')->with('error', __('notification.common.fail.no_edit'));
        }


    }


    public function updateBudget(Request $request, $idBudget)
    {
        $this->validate($request, [
            'year_created' => 'required|integer|between:2020,2050',
            'estimated_code' => ['required', 'regex:/^[A-Z0-9' . EValidation::ALPHABET . ']+$/', self::MAX_CODE],
            'code' => ['required', 'unique:budget,code,' . $idBudget.',id,deleted_at,NULL', 'regex:/^[A-Z0-9' . EValidation::ALPHABET . ']+$/', self::MAX_CODE],
            'name' => ['required', 'unique:budget,name,' . $idBudget.',id,deleted_at,NULL', 'regex:/^[a-zA-Z0-9' . EValidation::ALPHABET . ']+$/', 'max:100'],
            'total_budget' => ['required', 'regex:/^[0-9\.\,]+$/', self::MAX_MONEY],
            'content' => ['regex:/^[a-zA-Z0-9' . EValidation::ALPHABET . '\.\,\-\(\)\/ ]+$/', self::MAX_CONTENT, 'nullable'],
            'note' => ['regex:/^[a-zA-Z0-9' . EValidation::ALPHABET . '\.\,\-\(\)\/ ]+$/', self::MAX_CONTENT, 'nullable'],
            'money_plan.*' => ($request->plan_spending == 'allocated') ? 'required|regex:/^[0-9\.\,]+$/|'.self::MAX_MONEY : '',
        ], [], [
            'year_created' => __('data_field_name.budget.year'),
            'estimated_code' => __('data_field_name.budget.estimate'),
            'code' => __('data_field_name.budget.code'),
            'name' => __('data_field_name.budget.name'),
            'total_budget' => __('data_field_name.budget.total'),
            'content' => __('data_field_name.budget.content'),
            'note' => __('data_field_name.budget.note'),
            'money_plan.*' => __('data_field_name.budget.money_plan'),
        ]);
        $budget = Budget::findOrFail($idBudget);
        $budget->year_created = $request->year_created;
        $budget->estimated_code = $request->estimated_code;
        $budget->code = $request->code;
        $budget->name = $request->name;
        $budget->total_budget = Community::getAmount($request->total_budget);
        $budget->content = $request->content;
        $budget->note = $request->note;
        $budget->department_id = $request->department_id;
        $budget->research_project_id = $request->research_project_id;

        if ($request->plan_spending == 'allocated') {
            $budget->allocated = 1;
            $listMoneyPlan = $request->money_plan;
            $data = [];
            foreach ($listMoneyPlan as $key => $val) {
                $budgetPlan = BudgetHasExpensePlan::where(['budget_id' => $idBudget, 'month_budget' => $key + 1])->first();

                if ($budgetPlan) {
                    $budgetPlan->money_plan = Community::getAmount($val);
                    $budgetPlan->update();
                } else {
                    $data_insert = [
                        'budget_id' => $budget->id,
                        'month_budget' => $key + 1,
                        'money_plan' => Community::getAmount($val),
                    ];
                    if ($data_insert['money_plan'] == '') {
                        $data_insert = [
                            'budget_id' => $budget->id,
                            'month_budget' => $key + 1,
                            'money_plan' => 0,
                        ];

                    } else {
                        $data_insert = [
                            'budget_id' => $budget->id,
                            'month_budget' => $key + 1,
                            'money_plan' => Community::getAmount($val),
                        ];
                    }
                    $data[] = $data_insert;
                }
            }
            if ($data) {
                BudgetHasExpensePlan::insert($data);
            }
        }
        else {
            $budget->allocated = 0;
        }
        $budget->save();

        \App\Models\File::where('model_name', Budget::class)
            ->where('type', ETypeUpload::BUDGET)->where('admin_id', auth()->id())->where('model_id', null)->update([
                'model_id' => $idBudget,
                'status' => 1
            ]);

        return redirect()->route(ERouteName::ROUTE_BUDGET_INDEX)
            ->with('success', __('notification.common.success.update'));
    }

    protected function configFile()
    {
        $model_name = Budget::class;
        return [
            'model_name' => $model_name,
            'type' => ETypeUpload::BUDGET,
            'folder' => app($model_name)->getTable(),
        ];
    }

    //man hinh lich su thay doi ngan sach
    public function historyBudget()
    {
        return view('admin.budget.history-budget');
    }

    public function historyChangeBudget($id)
    {
        $data = Budget::with('hasPlan')->findOrFail($id);

        $auditBudgetDetail = Audit::query()->where(['auditable_type' => Budget::class])->where(['auditable_id' => $id])->orderBy('id', 'desc')->first();

        $auditBudgetMoneyPlan = [];
        if ($data->allocated == 1) {
            $old_value = explode(" ", $auditBudgetDetail->old_values['money_plan'] ?? '0') ;
            $new_value = explode(" ", $auditBudgetDetail->new_values['money_plan'] ?? '0');
            for ($i = 0; $i <= 11; $i++) {
                $auditBudgetMoneyPlan[$i]['old_value'] = $old_value[$i] ?? "0";
                $auditBudgetMoneyPlan[$i]['new_value'] = $new_value[$i] ?? "0";
                $auditBudgetMoneyPlan[$i]['month'] = $i + 1;
                if (count($data->hasPlan) > 0) {
                    $auditBudgetMoneyPlan[$i]['id'] = $data->hasPlan[$i]->id;
                } else {
                    $auditBudgetMoneyPlan[$i]['id'] = ' ';
                }
                if ((int)Community::getAmount($auditBudgetMoneyPlan[$i]['old_value']) == 0) {
                    $auditBudgetMoneyPlan[$i]['change'] = 0;
                } else {
                    $auditBudgetMoneyPlan[$i]['change'] = ((int)Community::getAmount($auditBudgetMoneyPlan[$i]['new_value']) - (int)Community::getAmount($auditBudgetMoneyPlan[$i]['old_value'])) / (int)Community::getAmount($auditBudgetMoneyPlan[$i]['old_value']);
                }
            }
            $model_name = BudgetHasExpensePlan::class;
            $type = Config::get('common.type_upload.BudgetHasExpensePlan');
            $folder = app($model_name)->getTable();
        } else {
            if (isset($auditBudgetDetail->old_values['total_budget'])){
                $data->old_value = $auditBudgetDetail->old_values['total_budget'] ?? 0;
                if (((int)Community::getAmount($data->old_value)) == 0) {
                    $data->new_value = $data->total_budget ?? 0;
                    $data->change_value = 0;
                } else {
                    $data->new_value = $auditBudgetDetail->new_values['total_budget'] ?? "0";
                    $data->change_value = (((int)Community::getAmount($data->new_value) - ((int)Community::getAmount($data->old_value))) / ((int)Community::getAmount($data->old_value)));
                }
            }else{
                $data->old_value = $data->total_budget ?? 0;
                if (((int)Community::getAmount($data->old_value)) == 0) {
                    $data->new_value = $data->total_budget ?? 0;
                    $data->change_value = 0;
                } else {
                    $data->new_value = $data->total_budget ?? "0";
                    $data->change_value = (((int)Community::getAmount($data->new_value) - ((int)Community::getAmount($data->old_value))) / ((int)Community::getAmount($data->old_value)));
                }
            }
            $model_name = Budget::class;
            $type = ETypeUpload::BUDGET;
            $folder = app($model_name)->getTable();
        }

        $configFile = $this->configFile();
    return view('admin.budget.history-budget-change', ['data' => $data, 'configFile' => $configFile, 'auditBudgetMoneyPlan' => $auditBudgetMoneyPlan, 'model_name' => $model_name, 'type' => $type, 'folder' => $folder]);
    }

}

