<div wire:ignore.self class="modal fade" id="createRule" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form wire:submit.prevent="submit">
                <div class="modal-header">
                    <div class="row width_100">
                        <div class="col-11">
                            <h6 class="text_center fontsize_28"
                                id="exampleModalLabel">{{__('data_field_name.asset.maintenance_regulations')}}</h6>
                        </div>
                        <div class="col-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true close-btn">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{__('data_field_name.asset.asset_cate')}}<span class="text-danger">*</span></label>
                        <div class="form-asset">
                            <select class="form-control form-control-lg" wire:model.lazy="asset_category_id">
                                <option value=" ">{{__('data_field_name.asset.asset_cate')}}</option>
                                @foreach ($category as $value)
                                    <option value="{{$value['id']}}">
                                        {!!$value['padLeft'].$value['name']!!}
                                    </option>
                                @endforeach
                            </select>
                            <span><img src="/images/cham.svg" alt=""/></span>
                        </div>
                        @error('asset_category_id') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('data_field_name.asset.content_maintenance')}}<span
                                class="text-danger">*</span></label>
                        <textarea class="form-control" rows="4"
                                  placeholder="{{__('data_field_name.asset.content_maintenance')}}"
                                  wire:model.defer="content_maintenance"
                        ></textarea>
                        @error('content_maintenance') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('data_field_name.asset.frequency')}}<span class="text-danger">*</span> </label>
                        <div class="form-group checkradio">
                            @foreach($assetFrequency as $key => $val)
                                <input type="radio" value="{{$key}}" wire:model.lazy="frequency">
                                <label for="html">{{$val}}</label>&ensp;
                            @endforeach
                        </div>
                        @error('frequency') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('data_field_name.asset.time_choose')}}<span class="text-danger">*</span></label>
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control form-control-lg" wire:model.defer="time_choose">
                                    <option>{{__('data_field_name.asset.selection_time')}}</option>
                                    @foreach($assetMaintenanceTime as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>
                                @error('time_choose') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-4">
                                <input type="number" class="form-control asset-type-date" placeholder=""
                                       wire:model.defer="number_date">
                                @error('number_date') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-2 padding_top">
                                <label for="">{{__('data_field_name.asset.number_date')}}</label>
                            </div>
                        </div>
                    </div>
                    @if($frequency==\App\Enums\EAssetFrequency::PERIODIC)
                        <div class="form-group">
                            <label>{{__('data_field_name.asset.repeat')}}</label>
                            <select class="form-control form-control-lg" wire:model.lazy="repeat">
                                @foreach($assetTimeRepeat as $key => $val)
                                    <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" id="close-rule"
                            data-dismiss="modal">{{__('common.button.cancel')}}
                    </button>
                    <button type="button" class="btn btn-save"
                            wire:click.prevent="save()">{{__('common.button.perform')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


