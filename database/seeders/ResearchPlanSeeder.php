<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Factory;
use DB;

class ResearchPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('research_plan')->insert([
            [
                'name'=>'ccccccc',
                'status'=>0,
                'start_date'=>'10-5-2019',
                'end_date'=>'10-5-2019',
            ],
            [
                'name'=>'jlllllmm',
                'status'=>1,
                'start_date'=>'11-5-2019',
                'end_date'=>'11-5-2019',
            ],
            [
                'name'=>'epppp',
                'status'=>2,
                'start_date'=>'12-5-2019',
                'end_date'=>'12-5-2019',
            ],
            [
                'name'=>'ekkkyooo',
                'status'=>3,
                'start_date'=>'13-5-2019',
                'end_date'=>'13-5-2019',
            ],
            [
                'name'=>'xxxxssss',
                'status'=>2,
                'start_date'=>'14-5-2019',
                'end_date'=>'14-5-2019',
            ],
            [
                'name'=>'vvvvtttt',
                'status'=>1,
                'start_date'=>'15-5-2019',
                'end_date'=>'15-5-2019',
            ],
        ]);

    }
}
