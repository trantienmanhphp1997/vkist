@extends('layouts.master')
@section('title')
    <title>{{__('notification.common.fail.error_404')}}</title>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
      <div class="mx-auto text-center" style="padding: 0px 0px 120px 0px;">
          <img class="img-fluid" src="/images/404.svg" alt="code 404"/>
          <p class="py-16">{{__('notification.common.fail.not_found')}}</p>
          <a href="{{route('home')}}" class="btn btn-save size14">{{__('common.button.back_homepage')}}</a>
      </div>
  </div>
</div>
@endsection