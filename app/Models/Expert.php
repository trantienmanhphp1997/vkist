<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\MasterData;
use App\Enums\EMasterDataType;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\BaseModel;

class Expert extends BaseModel
{
    use HasFactory;

    protected $table='expert';
    protected $fillable = [
        'fullname',
        'address',
        'phone',
        'nationality',
        'department_id',
        'academic_level_id',
        'admin_id',
        'specialized',
        'email',
        'unsign_text',
        'nationality',
        'specialized_id',
        'department',
        'unit',
    ];
    private static $searchable = [
        'fullname',
        'email',
        'phone',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
    
    public function academic() {
        return $this->hasOne(MasterData::class, 'id', 'academic_level_id');
    }

    public function specialize() {
        return $this->hasOne(MasterData::class, 'id', 'specialized_id');
    }
}
