<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Asset;
use App\Models\AssetRevenue;
use App\Models\AssetAllocatedRevoke;
use App\Enums\EAssetStatus;

class AssetRevenueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset-revenue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $quantity = Asset::query()->sum('quantity') - AssetAllocatedRevoke::where('status',  EAssetStatus::REPORT_LOST)->orWhere('status',  EAssetStatus::REPORT_CANCEL)->orWhere('status',  EAssetStatus::REPORT_LIQUIDATION)->sum('implementation_quantity');
        $total_money = 0;
        foreach(Asset::all() as $asset){
            $total_money += $asset->quantity*$asset->asset_value;
        }

        AssetRevenue::create([
            'report_year' => date("Y"),
            'report_month' => date("m"),
            'quantity' => $quantity,
            'total_money' => $total_money,
        ]);
    }
}
