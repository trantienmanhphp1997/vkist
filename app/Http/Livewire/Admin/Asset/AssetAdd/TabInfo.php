<?php

namespace App\Http\Livewire\Admin\Asset\AssetAdd;

use App\Component\AssetCategoryRecursive;
use App\Component\DepartmentRecursive;
use App\Enums\EAssetCategoryTypeManage;
use App\Enums\EAssetGroup;
use App\Enums\EAssetOrigin;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use App\Models\AssetCategory;
use App\Enums\EMasterDataType;
use App\Models\AssetOrigin;
use App\Models\AssetProvider;
use App\Models\ContractDuration;
use App\Models\Department;
use App\Models\MasterData;
use App\Http\Livewire\Base\BaseTrimString;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class TabInfo extends BaseTrimString

{
    public $department_id = '';
    public $manager_id = '';
    public $category_id = '';
    public $name = '';
    public $code = '';
    public $quantity = '';
    public $unit_id = '';
    public $origin = '';
    public $buy_date='';
    public $contract_number = '';
    public $asset_provider_id='';
    public $series_number = '';
    public $asset_value = '';
    public $unit_type = '';
    public $note = '';

    public $assetCategoryTypeManage;
    public $assetOrigin;
    public $fixed_assets = '';

    public $data_quantity='';
    public $buyDate;
    public $increase_code;
    public $code_increase_asset;

    public $assetGroup;
    public $group_asset;
    public $department_name;
    public $show_add_origin =false;
    public $origin_name;
    public $origin_status;
    public $originAsset;
    public $filter_origin_name;

    protected $listeners=[
        'saveTabAdd',
        'set-buy_date' => 'setBuyDate',
    ];
    public function mount()
    {

        $assetDepartment = Department::where('code', 'PQLTS')->first();
        if(empty($assetDepartment)) {
            $assetDepartment = Department::query()->create([
                'name' => 'Phòng quản lý tài sản',
                'note' => 'Quản lý tài sản',
                'status' => 1,
                'code' => 'PQLTS'
            ]);
        }
        $this->department_id = $assetDepartment->id;
        $this->department_name = $assetDepartment->name;

        $this->assetCategoryTypeManage = [
            EAssetCategoryTypeManage::CODE => __('data_field_name.asset.as_code'),
            EAssetCategoryTypeManage::QUANTITY => __('data_field_name.asset.as_quantity'),
        ];
        $this->assetOrigin = [
            EAssetOrigin::ODA => __('data_field_name.asset.oda'),
            EAssetOrigin::NATION => __('data_field_name.asset.nation'),
        ];
        $this->assetGroup = EAssetGroup::getList();
    }

    public function render()
    {

        $units = MasterData::getDataByType(EMasterDataType::UNIT);
        $provider = AssetProvider::query()->pluck('name', 'id');
        $type_manage = '';
        $model_name = Asset::class;
        $type = Config::get('common.type_upload.Asset');
        $folder = app($model_name)->getTable();
        $status = -1;

        if ($this->category_id) {
            $this->emit('loadTypeManage',$this->category_id);
            $type = AssetCategory::findOrFail($this->category_id);
            $type_manage = $type->type_manage;
            $code_category=$type->code;

            $year =Carbon::now()->year;
            $last = Asset::where('category_id',$this->category_id)->orderBy('id','desc')->get()->first();

//lấy mã tsan tạo gần nhất so sánh vs thời điểm hiện tại
            if ($last){
                $lastYear=reFormatDate($last->created_at,'Y');
                if ($year != $lastYear){
                    $lastIncreaseCode = 0;
                }else{
                    $lastIncreaseCode = DB::table('asset')
                            ->whereNotNull('increase_code')
                            ->where('category_id',$this->category_id)
                            ->orderBy('created_at', 'desc')
                            ->first('increase_code')->increase_code ?? 0;
                }
            }else{
                $lastIncreaseCode = 0;
            }
            $this->increase_code = ++$lastIncreaseCode;
            if ($this->code){
                $this->code_increase_asset=$this->code;
            }else{
                $this->code=$code_category.'.'.$year.'.'.$lastIncreaseCode;
                $this->code_increase_asset = $code_category.'.'.$year.'.'.$lastIncreaseCode;
            }

            if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                 $this->quantity=1;
                $this->fixed_assets=1;
                $this->data_quantity=$this->quantity;
            } else if ($type_manage == \App\Enums\EAssetCategoryTypeManage::QUANTITY) {
                if ($this->quantity){
                    $this->data_quantity=$this->quantity;
                }
                $this->fixed_assets= 0;
            } else {
                $this->data_quantity = 0;
            }
        }else{
            $this->quantity=1;
            $this->data_quantity=$this->quantity;
        }
        $data = $this->getHierarchyDepartment();
        $category = $this->getHierarchyAssetCategory();

        if ($this->asset_value){
            $this->emit('loadAssetValue',$this->asset_value);
        }

        $this->originAsset = AssetOrigin::query();
        if (!empty($this->filter_origin_name)) {

            if(strlen($this->filter_origin_name)){
                $this->originAsset->where('asset_origin.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->filter_origin_name)) . '%');
            }
        }


        $this->originAsset = $this->originAsset->orderBy('id', 'desc')->get();


        $this->getValue();
        return view('livewire.admin.asset.asset-add.tab-info', [ 'data' => $data,
            'type_manage' => $type_manage,
            'provider' => $provider,
            'category' => $category, 'units' => $units,
            'model_name' => $model_name, 'type' => $type,
            'folder' => $folder, 'status' => $status,
            ]);
    }

    public function getHierarchyDepartment()
    {
        return DepartmentRecursive::hierarch(Department::all());
    }

    public function getHierarchyAssetCategory()
    {
        return AssetCategoryRecursive::hierarch(AssetCategory::all());
    }

    public function getValue()
    {
        //để lấy default leaveDate là ngày hiện tại
        if(is_null($this->buyDate)) {
            $this->buyDate = date('Y-m-d');
        }


        $this->emit('getValue', $this->fixed_assets, $this->department_id,
            $this->category_id, $this->name,
            $this->code_increase_asset, $this->quantity, $this->unit_id, $this->origin,
            $this->buyDate, $this->contract_number, $this->asset_provider_id,
            $this->series_number, $this->asset_value, $this->unit_type,
            $this->note, $this->data_quantity,$this->increase_code,$this->group_asset
        );
        $this->emit('quantity',$this->quantity);

    }
    public function setBuyDate($data) {
        $this->buyDate = date('Y-m-d', strtotime($data['buy_date']));
    }
    public function validateOrigin() {
        $this->validate([
            'origin_name' => ['required'],
        ],[],[
            'origin_name'=> __('data_field_name.asset.origin'),
        ]);
    }
    public function saveSetting() {
        $this->validateOrigin();
        $origin = new AssetOrigin();
        $origin->origin_name = $this->origin_name;
        $origin->origin_status = $this->origin_status ? 1 : 0;
        $origin->save();
        if ($origin->origin_status == 1) {
            $this->origin = $origin->id;
        } else {
            $this->origin = null;
        }
        $this->origin_name = null;
        $this->origin_status = false;
        $this->show_add_origin = false;
        $this->emit('closeModelSetting');
    }

    public function toggleDisplaySetting($id) {
        $origin = AssetOrigin::findOrFail($id);
        $origin->origin_status = $origin->origin_status == 1 ? 0 : 1;
        if ($origin->origin_status == 1) {
            $this->origin = $id;
        } else {
            $this->origin = null;
        }
        $origin->save();
        $this->emit('closeModelSetting');
    }

    public function deleteSetting($id) {
        $origin = AssetOrigin::findOrFail($id);
        if (!empty($origin)) {
            $origin->delete();
            if ($this->origin == $id) {
                $this->origin = null;
            }
        }
    }

    public function cancelOrigin(){
        $this->origin_name = null;
        $this->origin_status = false;
        $this->show_add_origin = false;
        $this->emit('closeModelSetting');
    }

    public function saveTabAdd(){

        $val = Validator::make([
            'department_id' => $this->department_id,
            'manager_id' => $this->manager_id,
            'category_id' => $this->category_id,
            'name' => $this->name,
            'code' => $this->code,
            'quantity' => $this->quantity,
            'series_number' => $this->series_number,
            'contract_number' => $this->contract_number,
            'unit_type' => $this->unit_type,
            'note' => $this->note,
            'asset_value' => $this->asset_value,
            'origin' => $this->origin
        ], [
            'department_id' => 'required',
            'category_id' => 'required',
            'name' => ['required', 'regex:/^[a-zA-Z' . config('common.special_character.alphabet') . ']+$/', 'max:100'],
            'code'=>['required','regex:/^[A-Z0-9' . config('common.special_character.alphabet') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:25','unique:asset,code'],
            'quantity' => 'max:7',
            'series_number' => ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:20', 'nullable'],
            'contract_number' => ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:20', 'nullable'],
            'unit_type' =>  ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:255', 'nullable'],
            'note' =>  ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:255', 'nullable'],
            'asset_value' => 'max:13',
            'origin' => 'required'
        ], [], [
            'department_id' => __('data_field_name.asset.department'),
            'category_id' => __('data_field_name.asset.asset_cate'),
            'name' => __('data_field_name.asset.asset_name'),
            'code' => __('data_field_name.asset.asset_code'),
            'quantity' => __('data_field_name.asset.quantity'),
            'series_number' => __('data_field_name.asset.serial'),
            'contract_number' => __('data_field_name.asset.contract_number'),
            'unit_type' => __('data_field_name.asset.unit_type'),
            'note' => __('data_field_name.asset.note'),
            'asset_value' => __('data_field_name.asset.asset_value'),
            'origin' => __('data_field_name.asset.origin')
        ]);
        $val->validate();
    }
}
