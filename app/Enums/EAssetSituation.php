<?php


namespace App\Enums;


class EAssetSituation
{
    const SITUATION_USING = 1;
    const SITUATION_NOT_USE = 0;

    const LOSTED = 7;
    const CANCELLED = 8;
    const LIQUIDATION = 9;

    public static function valueToName($value) {
        $result = "";
        switch ($value) {
            case self::SITUATION_USING:
                $result = __('data_field_name.asset.using');
                break;
            case self::SITUATION_NOT_USE:
                $result = __('data_field_name.asset.no_use');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
