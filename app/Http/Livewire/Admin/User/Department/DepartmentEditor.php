<?php

namespace App\Http\Livewire\Admin\User\Department;

use App\Enums\EDepartment;
use App\Models\Department;
use Livewire\Component;

class DepartmentEditor extends Component
{
    public $editable = true;
    public $typeList = [];
    public $department;

    public $parentType;

    protected $listeners = [
        'changed-parent-id-event' => 'setParentId'
    ];

    protected function rules()
    {
        return [
            'department.name' => 'required|max:48|regex:' . config('common.regex.standard_name') . '|unique:department,name' . ($this->department->exists ? ",{$this->department->id}" : ''),
            'department.note' => ['required', 'max:255', 'regex:/[^<>!@#$%^&*(){}\[\]\'\"\/\|]+$/'],
            'department.status' => 'max:100',
            'department.code' => 'required|max:10',
            'department.parent_id' => 'max:100',
            'department.type' => 'required|numeric'
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'department.name' => __('data_field_name.department.name'),
            'department.note' => __('data_field_name.department.note'),
            'department.status' => __('data_field_name.department.status'),
            'department.parent_id' => __('data_field_name.department.under_department'),
            'department.code' => __('data_field_name.department.code'),
            'department.type' => __('data_field_name.department.type')
        ];
    }

    public function mount($id = 0)
    {
        $this->department = Department::query()->findOrNew($id);
        $this->typeList = EDepartment::getTypeList();
        if ($this->department->type == EDepartment::TYPE_CLASS) {
            $this->parentType = null;
        } elseif ($this->department->type == EDepartment::TYPE_GROUP) {
            $this->parentType = EDepartment::TYPE_CLASS;
        } elseif ($this->department->type == EDepartment::TYPE_UNIT) {
            $this->parentType = EDepartment::TYPE_GROUP;
        }
    }

    public function render()
    {
        return view('livewire.admin.user.department.department-editor');
    }

    public function save()
    {
        $this->validate();
        $message = $this->department->exists ? __('notification.common.success.update') : __('notification.common.success.add');
        $this->department->save();
        $this->dispatchBrowserEvent('close-department-editor-event');
        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => $message]);
    }

    public function setParentId($value)
    {
        $this->department->parent_id = $value['id'];
    }

    public function updatedDepartmentType()
    {
        $this->department->parent_id = null;
        if ($this->department->type == EDepartment::TYPE_CLASS) {
            $this->parentType = null;
        } elseif ($this->department->type == EDepartment::TYPE_GROUP) {
            $this->parentType = EDepartment::TYPE_CLASS;
        } elseif ($this->department->type == EDepartment::TYPE_UNIT) {
            $this->parentType = EDepartment::TYPE_GROUP;
        }
    }

    public function resetEditor()
    {
        $this->resetValidation();
        $this->department = ($this->department->exists) ? $this->department->fresh() : new Department();
        $this->parentType = null;
    }
}
