<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumClaimFormIdToAssetAllocatedRevoke extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->foreignId('claim_form_id')->nullable()->constrained('claim_form')->comment('phiếu lưu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->dropColumn('claim_form_id');
        });
    }
}
