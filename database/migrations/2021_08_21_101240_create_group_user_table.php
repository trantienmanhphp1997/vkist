<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',255)->nullable()->comment('ma code nhom nguoi dung');
            $table->string('name', 500)->nullable()->comment('ten nhom nhom nguoi dung');
            $table->string('description', 1000)->nullable()->comment('mo ta');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');

            $table->softDeletes();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->timestamps();
        });

        Schema::create('group_user_has_topic', function (Blueprint $table) {
            $table->id();
            $table->foreignId('group_user_id')->nullable()->constrained('group_user')->comment('map vs bảng group user');
            $table->foreignId('topic_id')->nullable()->constrained('topic')->comment('map vs bảng topic');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_user_has_topic');
        Schema::dropIfExists('group_user');
    }
}
