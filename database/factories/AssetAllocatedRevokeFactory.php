<?php

namespace Database\Factories;

use App\Models\AssetAllocatedRevoke;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AssetAllocatedRevokeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AssetAllocatedRevoke::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'asset_id' => 1,
            'status' => 1,
            'implementation_date' => now(),
            'used_type' => $this->faker->numberBetween(1, 3),
            'number_report' => $this->faker->numberBetween(1000, 9999),
            'user_info_id' => 1,
            'implementation_reason' => 'test',
            'implementation_quantity' => $this->faker->numberBetween(1, 9999),
            'admin_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
