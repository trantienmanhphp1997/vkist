@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.system.role.text_create_role')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div  class="breadcrumbs"><a href="{{route('admin.system.role.index')}}">{{__('data_field_name.system.role.text_role_management')}} </a> \ <span>{{__('data_field_name.system.role.text_create_role')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="detail-task information box-idea create-edit-role">
                    <h4>{{__('data_field_name.system.role.text_create_role')}}</h4>
                    <div class="group-tabs">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                    <span>{{__('data_field_name.common_field.general_information')}}</span>
                                </a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <form class="create-edit-role" method="POST" action="{{route('admin.system.role.store')}}" >
                                    @include('admin.system.role._formRole')
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
