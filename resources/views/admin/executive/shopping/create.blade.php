@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.shopping.list') }}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user bg-light rounded">
        <div class="row">
            <div class="breadcrumbs">
                <a class="text-primary" href="#">{{ __('data_field_name.shopping.shopping_management') }}</a> \ <span class="text-muted">{{ __('data_field_name.shopping.shopping_proposal') }}</span>
            </div>
        </div>

        <form class="mt-3 p-5 bg-white rounded" action="{{ route('admin.executive.shopping.store') }}" method="POST">
            @csrf
            <h4>{{ __('data_field_name.shopping.general') }}</h4>
            <div class="row">
                <div class="form-group col-sm">
                    <label>{{ __('data_field_name.shopping.name') }} <span class="text-danger">(*)</span></label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group col-sm">
                    <label>{{ __('data_field_name.shopping.budget') }} <span class="text-danger">(*)</span></label>
                    {{ Form::select('budget_id', ['' => '- ' . __('data_field_name.shopping.select_budget') . ' -'] + $budgets, old('budget_id'), ['class' => 'form-control']) }}
                    @error('budget_id')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 form-group">
                    <label>{{ __('data_field_name.shopping.estimate_money') }} <span class="text-danger">(*)</span></label>
                    <input type="text" name="estimate_money" min="0" class="form-control format_number" value="{{ old('estimate_money') }}">
                    @error('estimate_money')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-6 form-group">
                    <label>{{ __('data_field_name.shopping.needed_date') }}</label>
                    <input type="text" name="needed_date" class="form-control input-date-kendo" placeholder="dd/mm/yyyy" value="{{ old('needed_date') }}">
                </div>
                <div class="col-sm-6 form-group">
                    <label>{{ __('data_field_name.shopping.estimated_delivery_date') }}</label>
                    <input type="text" name="estimated_delivery_date" class="form-control input-date-kendo" placeholder="dd/mm/yyyy" value="{{ old('estimated_delivery_date') }}">
                </div>
            </div>

            <h3 class="title-asset pt-24">{{ __('data_field_name.shopping.device_list') }} <span class="text-danger">(*)</span></h3>

            <div id="device-list">
                <?php $oldDeviceCount = count(old('device_name') ?? []); ?>
                @for ($i = 0; $i < $oldDeviceCount; $i++)
                    <div class="box-calendar col-md-12 device-info">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.shopping.device_name') }} <span class="text-danger">(*)</span></label>
                                    <input type="text" name="device_name[]" class="form-control" value="{{ old("device_name.$i") }}">
                                    @error("device_name.$i")
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.shopping.device_quantity') }} <span class="text-danger">(*)</span></label>
                                    <input type="text" name="device_quantity[]" class="form-control" value="{{ old("device_quantity.$i") }}">
                                    @error("device_quantity.$i")
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="d-block">&nbsp;</label>
                                <button class="btn btn-danger" type="button" onclick="deleteDevice(this)"><em class="fa fa-trash"></em></button>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
            @error('device_name')
                <div class="text-danger">{{ $message }}</div>
            @enderror

            <button type="button" class="btn btn-main bg-primary text-9 size14 py-10 px-20 text-white" onclick="addDevice()"><img src="{{ asset('/images/plus2.svg') }}" alt="plus"/>{{ __('common.button.create') }}</button>

            <div class="form-group mt-3">
                <label>{{ __('data_field_name.shopping.note') }}</label>
                <textarea name="note" class="form-control" rows="5"></textarea>
                @error('note')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            @livewire('component.files', ['model_name' => $model, 'model_id' => null, 'type' => $type, 'folder' => 'shopping', 'acceptMimeTypes' => config('common.mime_type.general')])

            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.executive.shopping.index') }}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{ __('common.button.cancel') }}</a>
                <button type="submit" class="btn btn-main bg-primary text-9 size16 px-48 py-14">{{ __('common.button.save') }}</button>
            </div>
        </form>
    </div>

    <script>
        function addDevice() {
            $('#device-list').append(`
                <div class="box-calendar col-md-12 device-info">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ __('data_field_name.shopping.device_name') }} <span class="text-danger">(*)</span></label>
                                <input type="text" name="device_name[]" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{ __('data_field_name.shopping.device_quantity') }} <span class="text-danger">(*)</span></label>
                                <input type="text" name="device_quantity[]" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="d-block">&nbsp;</label>
                            <button class="btn btn-danger" type="button" onclick="deleteDevice(this)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            `);
        }

        function deleteDevice(device) {
            console.log(device);
            $(device).parents('.device-info').remove();
        }
    </script>
@endsection
