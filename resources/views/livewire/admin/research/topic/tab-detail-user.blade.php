<div>
    <div class="inner-tab pt0">
        <table class="table">
            <thead>
                <tr class="border-radius text-center">
                    <th scope="col" class="border-radius-left">
                        {{ __('data_field_name.topic.stt') }}
                    </th>
                    <th scope="col" class="text-center">
                        {{ __('data_field_name.topic.member_name') }}
                    </th>
                    <th scope="col" class="text-center blur">
                        {{ __('data_field_name.topic.qualification') }}
                    </th>
                    <th scope="col" class="text-center blur">
                        {{ __('data_field_name.topic.role') }}
                    </th>
                    <th scope="col" class="text-center">
                        {{ __('data_field_name.topic.research_way') }}
                    </th>
                </tr>
            </thead>
            <tbody>
                <div wire:loading class="loader"></div>
                @foreach ($members as $member)
                    <tr>
                        <td>{{ ($members->currentPage() - 1) * $members->perPage() + $loop->iteration }}</td>
                        <td class="center">
                            <a href="#" data-toggle="modal" data-target="#detail-member-{{ $member->id }}">{{ $member->userInfo->fullname ?? '' }}</a>
                        </td>
                        <td class="center">{{ $member->userInfo->academicLevel->v_value ?? '' }}</td>
                        <td class="center">{{ $member->role->v_value ?? '' }}</td>
                        <td class="center">{{ $member->research->v_value ?? '' }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $members->links() }}
    </div>

    @foreach ($members as $member)
        <div class="modal fade" id="detail-member-{{ $member->id }}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ $member->userInfo->fullname ?? '' }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered mt-5">
                            <tr>
                                <td class="text-center">{{ __('data_field_name.topic.research_way_origin') }}</td>
                                <td>{{ $member->research->v_value ?? '' }}</td>
                            </tr>
                            <tr>
                                <td class="text-center" style="width:40%">{{ __('data_field_name.topic.academic_level') }}</td>
                                <td>{{ $member->userInfo->academicLevel->v_value ?? '' }}</td>
                            </tr>

                            <tr>
                                <td class="text-center">{{ __('data_field_name.topic.role_project') }}</td>
                                <td>{{ $member->role->v_value ?? '' }}</td>
                            </tr>
                            <tr>
                                <td class="text-center">{{ __('data_field_name.topic.description_mission') }}</td>
                                <td>{{ $member->mission_note ?? '' }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('common.button.back') }}</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
