<div class="inner-tab">

    <div class="col-md-12 text-center mb-3">
        <h2 class="title-main text-center mb-3">{{ __('data_field_name.topic.member_list') }}</h2>
        <button class="btn btn-primary m-auto" data-toggle="modal" data-target="#add-member-modal">
            <img src="/images/Add-user2.svg" alt="add-user"> {{ __('data_field_name.topic.add_member') }}
        </button>
    </div>

    <table class="table">
        <thead>
            <tr class="border-radius">
                <th scope="col" class="border-radius-left">{{ __('data_field_name.topic.stt') }}</th>
                <th scope="col">{{ __('data_field_name.topic.member_name') }}</th>
                <th scope="col">{{ __('data_field_name.topic.qualification') }}</th>
                <th scope="col">{{ __('data_field_name.topic.role') }}</th>
                <th scope="col">{{ __('data_field_name.topic.research_way') }}</th>
                <th scope="col">{{ __('data_field_name.topic.number_of_months') }}</th>
                <th scope="col" class="border-radius-right"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($readyMembers as $member)
                <tr>
                    <td class="text-danger">*</td>
                    <td class="text-danger">{{ $member['user_info']['fullname'] ?? 'name' }}</td>
                    <td class="text-danger">{{ $member['user_info']['academic_level']['v_value'] ?? 'qualification' }}</td>
                    <td class="text-danger">{{ $member['role']['v_value'] ?? 'role' }}</td>
                    <td class="text-danger">{{ $member['research']['v_value'] ?? 'research' }}</td>
                    <td class="text-danger">{{ calculateMonth($member['user_info']['start_working_date'] ?? 'now', 'now') }}</td>
                    <td>
                        <button wire:loading.attr="disabled" type="btn" class="btn" data-toggle="modal" data-target="#delete-ready-member" onclick="$('#del-1').attr('data-member', {{ $member['user_info_id'] }})"><img src="{{asset('images/trash.svg')}}" alt=""></button>
                    </td>
                </tr>
            @endforeach

            @foreach ($members as $member)
                <tr>
                    <td>{{ ($members->currentPage() - 1) * $members->perPage() + $loop->iteration }}</td>
                    <td>{{ $member->userInfo->fullname ?? 'name' }}</td>
                    <td>{{ $member->userInfo->academicLevel->v_value ?? 'qualification' }}</td>
                    <td>{{ $member->role->v_value ?? 'role' }}</td>
                    <td>{{ $member->research->v_value ?? 'research' }}</td>
                    <td>{{ calculateMonth($member->userInfo->start_working_date ?? 'now', 'now') }}</td>
                    <td>
                        <button wire:loading.attr="disabled" type="btn" class="btn" data-toggle="modal" data-target="#delete-member" onclick="$('#del-2').attr('data-member', {{ $member['user_info_id'] }})"><img src="{{asset('images/trash.svg')}}" alt=""></button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $members->links() }}

    <div class="modal fade" id="add-member-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            @livewire('admin.research.topic.add-member-form', key(date('h-i-s')))
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="delete-ready-member" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <button class="btn float-right" data-dismiss="modal"><em class="fa fa-times"></em></button>
                    <h4 class="modal-title" id="exampleModalLabel">{{ __('common.confirm_message.confirm_title') }}</h4>
                    <div class="mt-3">{{ __('common.confirm_message.are_you_sure_delete') }}</div>
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.cancel') }}</button>
                    <button id="del-1" type="button" class="btn btn-save" data-dismiss="modal" wire:click="deleteReadyMember($event.target.getAttribute('data-member'))">{{ __('common.button.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="delete-member" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <button class="btn float-right" data-dismiss="modal"><em class="fa fa-times"></em></button>
                    <h4 class="modal-title" id="exampleModalLabel">{{ __('common.confirm_message.confirm_title') }}</h4>
                    <div class="mt-3">{{ __('common.confirm_message.are_you_sure_delete') }}</div>
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.cancel') }}</button>
                    <button id="del-2" type="button" class="btn btn-save" data-dismiss="modal" wire:click="deleteAvailableMember($event.target.getAttribute('data-member'))">{{ __('common.button.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

</div>
