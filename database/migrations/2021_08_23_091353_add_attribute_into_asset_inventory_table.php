<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeIntoAssetInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_inventory', function (Blueprint $table) {
            $table->foreignId('period_id')->nullable()->constrained('inventory_periods')->comment('map voi ki kiem ke');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_inventory', function (Blueprint $table) {
            $table->dropColumn('period_id');
        });
    }
}
