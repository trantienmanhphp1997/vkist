<div class="information">
    <div class="inner-tab pt0">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group search-expertise">
                    <div class="search-expertise">
                        <input type="text" placeholder="Tìm kiếm" name="search" class="form-control"
                               wire:model.debounce.1000ms="searchTerm" id='input_vn_name'
                               autocomplete="off">
                        <span><img src="/images/Search.svg" alt="search"/></span>
                    </div>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr class="border-radius">
                <th scope="col" class="text-center"
                    class="border-radius-left">{{__('news/newsManager.menu_name.news_title_table.stt')}}</th>
                <th scope="col" class="text-center">{{__('user/group_user.table.topic-code')}}</th>
                <th scope="col" class="text-center">{{__('user/group_user.table.topic-name')}}</th>
            </tr>
            </thead>
            <tbody>
                @foreach($topics as $key => $topic)
                    <tr>
                        <td class="text-center">
                            {{ ($topics->currentPage() - 1) * $topics->perPage() + $loop->iteration }}
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.research.topic.detail', $topic->id) }}">
                                {!! boldTextSearch($topic->code, $searchTerm) !!}
                            </a>
                        </td>
                        <td class="text-center">
                            {!! boldTextSearch($topic->name, $searchTerm) !!}
                        </td>
                        <td class="text-center">
                        </td>

                    </tr>
                @endforeach
            </tbody>
        </table>
        @if (count($topics) > 0)
            {{ $topics->links() }}
        @else
            <div class="title-approve text-center">
                <span>{{ __('common.message.empty_search') }}</span>
            </div>
        @endif
    </div>
</div>
