<?php

namespace Tests;

use App\Models\User;
use Database\Seeders\UserDBSeeder;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed(UserDBSeeder::class);
        $this->user = User::first();
    }

    public function actingAsAdmin()
    {
        $this->user = empty($this->user) ? User::factory()->create() : $this->user;
        $this->actingAs($this->user);
        return $this;
    }

    public function actingAsSuperAdmin() {
        $this->user = User::query()->first() ?? User::factory()->create();
        $this->actingAs($this->user);
        return $this;
    }
}
