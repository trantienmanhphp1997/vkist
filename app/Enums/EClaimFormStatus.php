<?php


namespace App\Enums;


class EClaimFormStatus
{
    const STATUS_LOST = 1;
    const STATUS_ERROR = 2;
    const STATUS_TRANSFER = 3;
    const STATUS_TRANSFER_SERIES = 4;
    const STATUS_RECALL = 5;
    const STATUS_RECALL_SERIES = 6;
    const STATUS_MAINTAIN = 7;
}
