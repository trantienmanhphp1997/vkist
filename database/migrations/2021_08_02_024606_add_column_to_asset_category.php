<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToAssetCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_category', function (Blueprint $table) {
            //
            $table->tinyInteger('type_manage')->nullable()->comment('Quản lý theo: 0: mã, 1: số lượng ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_category', function (Blueprint $table) {
            //
            $table->dropColumn('type_manage');
        });
    }
}
