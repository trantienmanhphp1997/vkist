<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetHasExpensePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_has_expense_plan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('month_budget')->nullable()->comment('Thang');
            $table->bigInteger('money_plan')->nullable()->comment('So tien chi ke hoach');
            $table->bigInteger('money_real')->nullable()->comment('So tien chi thuc te');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->unsignedBigInteger('budget_id')->nullable()->comment('Map voi budget');
            $table->foreign('budget_id')->references('id')->on('budget');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_has_expense_plan');
    }
}
