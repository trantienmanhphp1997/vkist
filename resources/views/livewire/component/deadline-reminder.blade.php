<div>
    <div class="modal-fix-1 modal fade" id="deadline-reminder-modal" wire:ignore>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <h3 class="mb-2">{{ __('data_field_name.notification.file_deadline') }}</h3>
                <hr>
                <div class="row">
                    <div class="col-sm-6 mb-3 {{ $this->hasGeneralIdeaDeadline ? '' : 'd-none' }}">
                        <div id="general-idea-deadline"></div>
                    </div>

                    <div class="col-sm-6 mb-3 {{ $this->hasGeneralTopicDeadline ? '' : 'd-none' }}">
                        <div id="general-topic-deadline"></div>
                    </div>

                    <div class="col-sm-6 mb-3 {{ $this->hasUserIdeaDeadline ? '' : 'd-none' }}">
                        <div id="user-idea-deadline"></div>
                    </div>

                    <div class="col-sm-6 mb-3 {{ $this->hasUserTopicDeadline ? '' : 'd-none' }}">
                        <div id="user-topic-deadline"></div>
                    </div>
                </div>

                <div class="group-tabs mb-3">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" data-toggle="tab" href="#tab-delivering" role="tab" aria-controls="tab-delivering" aria-selected="false">
                                <span>{{ __('data_field_name.notification.delivering') }} ({{ count($shoppingDeadlines) }})</span>
                            </a>
                            <a class="nav-item nav-link" data-toggle="tab" href="#tab-maintenance" role="tab" aria-controls="tab-maintenance" aria-selected="false">
                                <span>{{ __('data_field_name.notification.maintenance') }}  ({{ count($maintenanceDeadlines) }})</span>
                            </a>
                        </div>
                    </nav>

                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade py-3 active show" id="tab-delivering" role="tabpanel" aria-labelledby="nav-home-tab">
                            @foreach ($shoppingDeadlines as $deadline)
                                <div class="border bg-light rounded p-2 mb-2">
                                    {!! __('notification.user.delivering', ['name' => "<strong>{$deadline->name}</strong>", 'date' => '<strong>' . reformatDate($deadline->estimated_delivery_date) . '</strong>']) !!}
                                </div>
                            @endforeach
                        </div>

                        <div class="tab-pane fade py-3" id="tab-maintenance" role="tabpanel" aria-labelledby="nav-home-tab">
                            @foreach ($maintenanceDeadlines as $deadline)
                                <div class="border bg-light rounded p-2 mb-2">
                                    {!! __('notification.user.maintenance', ['name' => "<strong>" . ($deadline->asset->name ?? '') . "</strong>", 'date' => "<strong>" . reformatDate($deadline->implementation_date) . "</strong>"]) !!}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <a href="{{ route('admin.config.notification.public') }}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{ __('data_field_name.notification.edit_submit') }}</a>
                    <button type="button" data-dismiss="modal" class="btn btn-main bg-primary text-9 size16 px-48 py-14">{{ __('common.button.close') }}</button>
                </div>

                <small class="d-block text-center mt-5">
                    {{ __('data_field_name.notification.can_review_deadline') }}
                    <input type="hidden" id="auto-show-reminder" value="{{ $this->autoShowDeadlineReminder }}">
                </small>
            </div>
        </div>
    </div>

    <script>
        window.addEventListener('livewire:load', function() {

            let label = {
                day: "{{ __('common.time.day') }}",
                hour: "{{ __('common.time.hour') }}",
                minute: "{{ __('common.time.minute') }}",
                second: "{{ __('common.time.second') }}",
                outOfDate: "{{ __('notification.user.out_of_date') }}"
            };

            $('#general-idea-deadline').countDown({
                deadline: "{{ $generalDeadline['idea']['deadline'] ?? '' }}",
                label: {
                    ...label,
                    title: "{{ __('data_field_name.notification.general_idea_deadline') }}"
                }
            });

            $('#general-topic-deadline').countDown({
                deadline: "{{ $generalDeadline['topic']['deadline'] ?? '' }}",
                label: {
                    ...label,
                    title: "{{ __('data_field_name.notification.general_topic_deadline') }}"
                }
            });

            $('#user-idea-deadline').countDown({
                deadline: "{{ $userDeadline['idea']['deadline'] ?? '' }}",
                label: {
                    ...label,
                    title: "{{ __('data_field_name.notification.user_idea_deadline') }}"
                }
            });

            $('#user-topic-deadline').countDown({
                deadline: "{{ $userDeadline['topic']['deadline'] ?? '' }}",
                label: {
                    ...label,
                    title: "{{ __('data_field_name.notification.user_topic_deadline') }}"
                }
            });

            if (document.getElementById('auto-show-reminder').value != '1') return;
            if (localStorage.getItem('remind-deadline-today') != "{{ date('Y-m-d') }}") {
                localStorage.setItem('remind-deadline-today', "{{ date('Y-m-d') }}");
                $('#deadline-reminder-modal').modal('show');
            }

        });
    </script>
</div>
