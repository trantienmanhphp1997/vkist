<div wire:ignore.self class="modal fade modal-fix" id="taisan" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.allocated-revoke.add_asset')}}</h6>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-12 pd-col">
                                <div class="form-group info-request searchbar">
                                    <label>{{__('common.button.search')}}</label>
                                    <div class="position-relative">
                                        <input type="search" placeholder="{{__('common.button.search')}}" wire:model.lazy="assetName">
                                        <img  class="search-icon" src="/images/Search.svg" alt="search">
                                    </div>
                                </div>
                            </div>
                            <table class="table">
                            <thead>
                                <tr class="border-radius">
                                    <th scope="col" class="border-radius-left">
                                        <div class="form-check info-request justify-content-center h-50 mb-0" style="background:transparent;">
                                            <input type="checkbox" class="all-asset-check" onclick="toggleAllAssetCheck(this)">
                                        </div>
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.allocated-revoke.asset_code')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.allocated-revoke.asset_name')}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($modalAssetList as $index => $row)
                                    <tr>
                                        <td class="text-center py-0">
                                            <div class="form-check info-request bg-white justify-content-center h-50 mb-0">
                                                <input type="checkbox" class="asset-check" name="hello" onclick="toggleAssetCheck(this)" value="{{ $row['id'] }}" wire:model.defer="idAssetSelected">
                                            </div>
                                        </td>
                                        <td class="text-center">{!! $row['code'] !!}</td>
                                        <td class="text-center">{!! $row['name'] !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" wire:click="cancelAllAsset" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                <button type="button" class="btn btn-primary" wire:click="saveAsset" data-dismiss="modal">{{__('common.button.save')}}</button>
            </div>
        </div>
    </div>

    <script>
        let $checkboxes = document.getElementsByClassName('asset-check');

        function toggleAllAssetCheck(checkbox) {
            for(let $checkbox of $checkboxes) {
                $checkbox.checked = checkbox.checked;
                $checkbox.dispatchEvent(new Event('change'));
            }
        }

        function toggleAssetCheck(checkbox) {
            let $allAssetCheck = document.querySelector('.all-asset-check');
            let checkedCount = 0;
            for(let $checkbox of $checkboxes) {
                if($checkbox.checked) checkedCount++;
            }

            if(checkedCount == $checkboxes.length) {
                $allAssetCheck.checked = true;
                $allAssetCheck.indeterminate = false;
            } else if(checkedCount == 0) {
                $allAssetCheck.checked = false;
                $allAssetCheck.indeterminate = false;
            } else {
                $allAssetCheck.checked = false;
                $allAssetCheck.indeterminate = true;
            }
        }
    </script>
</div>
