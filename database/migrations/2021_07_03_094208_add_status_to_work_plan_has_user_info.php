<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToWorkPlanHasUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_plan_has_user_info', function (Blueprint $table) {
            $table->tinyInteger('status')->nullable()->comment('trang thai = -1 khoi tao');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_plan_has_user_info', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('admin_id');
        });
    }
}
