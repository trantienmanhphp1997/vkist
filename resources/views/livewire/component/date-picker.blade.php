<div class="form-group">
    <input
    type="date"
    id="{{ $inputId }}"
    class="input-date-kendo form-control mb-0"
    isDatePicker="1"
    style="height: 48px;"
    @if (!empty($name)) name="{{ $name }}" @endif
    @if (!empty($setNullWhenEnterInvalidDate) && $setNullWhenEnterInvalidDate) set-null-when-enter-invalid-date="1" @endif
    @if ($disabled) disabled="true" @endif
    @if (!empty($placeholder)) placeholder="{{ $placeholder }}" @endif
    @if (!empty($value)) value={{ $value }} @endif
    @if (!empty($minDate)) min_date={{ $minDate }} @endif
    @if (!empty($maxDate)) max_date={{ $maxDate }} @endif
    >
    <script>

        if(window.initDatePicker) {
            setTimeout(() => {
                initDatePicker('#{{$inputId}}');
            }, 0);
        } else {
            window.addEventListener('load', function(event) {
                initDatePicker("#{{$inputId}}");
            });
        }


    </script>
</div>
