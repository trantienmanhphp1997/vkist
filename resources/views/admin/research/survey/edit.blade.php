@extends('layouts.master')
<title>
    {{ __('data_field_name.request-survey-list.form_create_edit.edit')}}
</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
@livewire('admin.research.survey.survey-create-and-update',['survey_request' => $data])
@endsection