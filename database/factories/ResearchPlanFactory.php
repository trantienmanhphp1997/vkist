<?php

namespace Database\Factories;

use App\Models\ResearchPlan;
use App\Models\Topic;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResearchPlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ResearchPlan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->namePlan,
            'topic_id' => Topic::all()->random()->id,
        ];
    }
}
