<?php

namespace App\Http\Controllers\Admin\System\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index()
    {
        return view('admin.system.account.index');
    }

    public function create()
    {
        return view('admin.system.account.create');
    }

    public function edit($id)
    {
        return view('admin.system.account.edit', ['id' => $id]);
    }
}
