<?php
return [
    'add-topic' => 'Add topic',
    'title' => [
        'main-title' => 'User Management and Authorization',
        'create' => 'Create group user',
        'update' => 'Update group user',
        'list' => 'Group user list',
        'delete' => 'Do you sure you want to delete this group user?',
        'view' => 'Detail group user',
        'pick-topic' => 'Select topic',
        'pick-research-category' => 'Select research category',
        'pick-all-topic' => 'Select all topic in research category'
    ],
    'tab1' => 'Information',
    'tab2' => 'Topic list',
    'form-data' => [
        'name' => 'Group name',
        'code' => 'Group code',
        'description' => 'Description'
    ],
    'table' => [
        'topic-code' => 'Topic code',
        'topic-name' => 'Topic name',
        'option' => 'Option',
    ],
    'msg' => [
        'success-save' => 'Create new group success',
        'success-update' => 'Update group success',
        'delete-success' => 'Delete group user success',
        'exist-topic' => 'Topic already exist in list below',
        'delete-topic-success' => 'Remove topic success',
        'add-topic-success' => 'Add topic into list success'
    ]
];
