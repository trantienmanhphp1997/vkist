<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskHasUserInfo extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'task_has_user_info';
    protected $fillable = [
        'task_id',
        'user_info_id'
    ];
}
