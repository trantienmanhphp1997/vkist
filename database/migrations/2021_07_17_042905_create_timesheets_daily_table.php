<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeSheetsDailyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheets_daily', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('luu du cham cong user theo ngay');
            $table->tinyInteger('working_day')->comment('ngay lam viec 1-31');
            $table->tinyInteger('working_status')->comment(' trang thai cham congK-1, P-2, X- 3, L-4, H-5, R-6, Ro - 7, Co-8, D-9, 0-10, T11');
            $table->foreignId('timesheets_monthly_id')->nullable()->constrained('timesheets_daily')->comment('map voi timesheets_daily');
            $table->unique(['timesheets_monthly_id', 'working_day']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheets_daily');
    }
}
