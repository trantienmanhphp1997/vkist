<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeIntoAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset', function (Blueprint $table) {
            $table->bigInteger('actual_quantity')->nullable()->comment('So luong thuc te');
            $table->string('actual_situation', 255)->nullable()->comment('Tinh trang thuc te');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset', function (Blueprint $table) {
            $table->dropColumn('actual_quantity');
            $table->dropColumn('actual_situation');
        });
    }
}
