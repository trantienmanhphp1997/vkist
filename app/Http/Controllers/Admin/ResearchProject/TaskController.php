<?php

namespace App\Http\Controllers\Admin\ResearchProject;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index($contract_code = ''){
        return view('admin.researchProject.task-work.index',compact('contract_code'));
    }

    public function create($contract_code = ''){
        return view('admin.researchProject.task-work.create',compact('contract_code'));
    }

    public function edit($id){
        return view('admin.researchProject.task-work.edit',compact('id'));
    }
}
