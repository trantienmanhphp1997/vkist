<?php
return [
    'breadcrumbs' => [
        'executive-management' => 'Management of labor contracts and Salary',
        'user-info-leave-management' => 'Leave management',
        'user-info-leave-list' => 'List of resigned staffs',
    ],
    'table_column' => [
        'code' => 'STAFF CODE',
        'name' => 'STAFF NAME',
        'department' => 'DEPARTMENT',
        'position' => 'POSITION',
        'reason' => 'REASON',
        'leave-date' => 'RESIGN DATE',
        'status' => 'STATUS',
        'action' => 'ACTION'
    ],
    'title' => [
        'create' => 'Create new',
        'edit' => 'Edit',
        'no-result' => 'No results!',
        'part-1' => 'Display',
        'part-2' => 'type of topic in',
    ],
    'empty-record' => 'No records',
    'placeholder' => [
        'search' => 'Search',
    ],
    'button' => [
        'add' => "Add new",
    ],
    'modal' => [
        'title' => [
            'delete' => 'Delete confirmation',
        ],
        'body' => [
            'delete-warning' => 'Do you want to delete? This operation cannot be undone!',
        ],
        'footer' => [
            'back' => 'Back',
            'delete' => 'Delete',
            'close' => 'Close',
        ]
    ],
    'status' => [
        'processing' => "In progress",
        'done' => "Done",
    ],
    'create-update-form' => [
        'title' => 'Confirmation of resign information',
        'general-info' => 'General information',
        'document' => 'Attachment',
        'attach-file' => 'Attach file',
        'name' => "Full name",
        'code' => 'Staff code',
        'department' => 'Department',
        'position' => 'Working position',
        'reason' => 'Reason for resigning',
        'leave_date' => 'Resign date',
        'status' => 'Status',
    ],
    'validate' => [
        'choosedUser' => 'Human resource information',
    ],
];