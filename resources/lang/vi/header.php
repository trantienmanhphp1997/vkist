<?php
return [
    'research_support' => 'Hỗ trợ nghiên cứu',
    'executive' => 'Quản trị điều hành',
    'home'=>'Trang chủ',
    'approved'=>'Phê duyệt',
    'email'=>'Hộp thư đến',
    'home-title'=>'Hệ thống tích hợp ICT'
];
