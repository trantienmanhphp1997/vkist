<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoppingDevice extends Model
{
    use HasFactory;

    protected $table = 'shopping_device';
    protected $fillable = [
        'shopping_id',
        'quantity',
        'status',
        'name'
    ];

}
