@extends('layouts.master')
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div  class="breadcrumbs"><a href="{{route('admin.profile.index')}}">{{__('data_field_name.profile.personal_information_management')}} </a></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="detail-task information box-idea">
                    <h4>{{__('data_field_name.profile.user_information')}}</h4>
                    <div class="group-tabs">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <form class="create-edit-role" method="POST" action="{{route('admin.profile.update')}}" autocomplete="off" enctype="multipart/form-data">
                                    <div class="row inner-tab">
                                        @csrf
                                        {{ method_field('POST') }}
                                        <div class="col-md-12">
                                            <div class="profile-image">
                                                <img id="image-preview" src="{!! $user->info->avatar ?? asset('images/avatar.jpg') !!}" alt="avatar"/>
                                                <input id="input-image-hidden" class="input-change" onchange="showImage(this)" type="file" name="avatar" accept="image/*">
                                                <button type="button" onclick="HandleBrowseClick('input-image-hidden');">{{__('data_field_name.profile.image')}}</button>
                                                <p class="error-upload-img text-danger d-none">{{ __('notification.profile.size_image') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.user.full_name')}}<span class="text-danger">*</span></label>
                                                <input type="text" name="fullname" class="form-control input-change" value="{!! old('fullname') ? old('fullname') : $user->info->fullname ?? '' !!}" />
                                                @error('fullname') <span class="text-danger">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.system.account.user_name')}}</label>
                                                <input type="text" class="form-control" value="{!! $user->username ?? '' !!}" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.system.account.user_code')}}</label>
                                                <input type="text" class="form-control" value="{!! $user->info->code ?? '' !!}" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.common_field.email')}}</label>
                                                <input type="text" class="form-control" value="{!! $user->email ?? '' !!}" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.profile.deparment')}}</label>
                                                <input type="text" class="form-control" value="{!! $user->info->department->name ?? '' !!}" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.profile.position')}}</label>
                                                <input type="text" class="form-control" value="{!! $user->info->position->v_value ?? '' !!}" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.profile.mobile_number')}}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control input-change" value="{!! old('phone') ? old('phone') : $user->info->phone ?? '' !!}" name="phone" />
                                                @error('phone') <span class="text-danger">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.profile.phone')}}</label>
                                                <input type="text" class="form-control input-change" value="{!! old('telephone') ? old('telephone') : $user->info->telephone ?? '' !!}" name="telephone" />
                                                @error('telephone') <span class="text-danger">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.profile.ext')}}</label>
                                                <input type="text" class="form-control input-change" value="{!! old('ext') ? old('ext') : $user->info->ext ?? '' !!}" name="ext" />
                                                @error('ext') <span class="text-danger">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{__('data_field_name.profile.username_groupware')}}</label>
                                                        <input type="text" class="form-control input-change" value="{!! old('username_groupware') ? old('username_groupware') : $user->gw_user_id ?? '' !!}" name="username_groupware" />
                                                        @error('username_groupware') <span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{__('data_field_name.profile.password_groupware')}}</label>
                                                        <input type="password" class="form-control input-change" value="{!! old('password_groupware') ?? '' !!}" name="password_groupware" />
                                                        @error('password_groupware') <span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 ">
                                            <div class="group-btn2 text-center group-btn ">
                                                <button id="submitForm" type="submit" class="btn btn-save">{{__('common.button.save')}}</button>
                                                <button type="button" class="btn btn-viewmore-news mr0 buttonCreate" data-toggle="modal" data-target="#changePassword">{{__('data_field_name.profile.change_password')}}</button>
                                                <a href="{{route('home')}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @livewire('admin.profile.change-password')
@endsection
@section('js')
    <script type="text/javascript">
        function HandleBrowseClick(input_image)
        {
            var fileinput = document.getElementById(input_image);
            fileinput.click();
        }
        function showImage(fileInput)
        {
            files = fileInput.files;
            $('.error-upload-img').addClass('d-none');
            if (files[0].size > 2097152) {
                $('#input-image-hidden').val('');
                $('.error-upload-img').removeClass('d-none');
                return false;
            }

            document.getElementById('image-preview').src = window.URL.createObjectURL(files[0])
        }

        $( document ).ready(function() {
            $(".input-change").on('change paste', function () {
                $('#submitForm').prop("disabled", false);
            });
        });
    </script>
@endsection
