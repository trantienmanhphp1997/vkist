<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contract extends BaseModel
{
    use HasFactory;
    protected $table = "contract";
    protected $guarded=['*'];
    protected $fillable = [
        'user_info_id',
        'contract_code',
        'contract_type',
        'start_date',
        'end_date',
        'basic_salary',
        'social_insurance_salary',
        'bank_account_number',
        'bank_name',
        'working_type',
        'coefficients_salary',
        'note',
        'status',
        'increase_code'
    ];

    public function coefficients() {
        return $this->belongsTo(MasterData::class, 'coefficients_salary_id');
    }

    public function bankName() {
        return $this->belongsTo(MasterData::class, 'bank_name_id');
    }

    public function position() {
        return $this->belongsTo(MasterData::class, 'position_id');
    }
}
