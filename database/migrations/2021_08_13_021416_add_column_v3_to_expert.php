<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnV3ToExpert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expert', function (Blueprint $table) {
            $table->string('department',1000)->nullable()->comment('Tên đơn vị làm việc');
            $table->bigInteger('specialized_id')->nullable()->comment('Tên chuyên ngành map master data type = 21');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expert', function (Blueprint $table) {
            $table->dropColumn('department');
            $table->dropColumn('specialized_id');
        });
    }
}
