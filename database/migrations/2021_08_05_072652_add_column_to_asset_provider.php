<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToAssetProvider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_provider', function (Blueprint $table) {
            //
            $table->string('name', 500)->nullable()->comment('Ten nha cung cap');
            $table->string('code', 255)->nullable()->comment('Ma nha cung cap');
            $table->string('address', 1000)->nullable()->comment('Dia chi');
            $table->string('email', 255)->nullable()->comment('Email');
            $table->string('phone_number', 255)->nullable()->comment('So dien thoai');
            $table->string('website', 255)->nullable()->comment('website');
            $table->string('bank_account_number', 14)->nullable()->comment('Tai khoan ngan hang');
            $table->foreignId('bank_name_id')->nullable()->comment('`Map voi master_data, type = 17`')->constrained('master_data');
            $table->string('tax_code', 255)->nullable()->comment('Ma so thue');
            $table->string('note', 1000)->nullable()->comment('Website');
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_provider', function (Blueprint $table) {
            //
            $table->dropColumn('name');
            $table->dropColumn('code');
            $table->dropColumn('address');
            $table->dropColumn('email');
            $table->dropColumn('phone_number');
            $table->dropColumn('website');
            $table->dropColumn('bank_account_number');
            $table->dropColumn('bank_name_id');
            $table->dropColumn('tax_code');
            $table->dropColumn('note');
            $table->dropIndex('unsign_text');
            $table->dropColumn('unsign_text');
            $table->dropColumn('admin_id');
            $table->dropSoftDeletes();
        });
    }
}
