<?php

namespace App\Http\Livewire\Admin\Research\Cost;

use App\Enums\EResearchPlan;
use App\Enums\ETopicFee;
use App\Enums\ETopicFeeDetail;
use App\Enums\ETopicStatus;
use App\Models\ResearchPlan;
use App\Models\TopicFee;
use App\Models\TopicFeeDetail;
use App\Service\Community;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Topic;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;

class TopicFeeList extends BaseLive
{
    public $searchTerm;
    public $searchStatus;
    public $searchSource;
    public $name;
    public $topic_id;
    public $editTopicFeeId;

    protected $listeners = ['resetData'];

    public function render()
    {
        $query = TopicFee::query();

        $searchTerm = $this->searchTerm;
        $searchStatus = $this->searchStatus;
        $searchSource = $this->searchSource;

        $query->with('admin', 'admin.info');
        if (!empty($this->searchTerm)) {
            $query->where('unsign_text','like','%'. strtolower(removeStringUtf8($searchTerm)).'%');
            $query->with('topic')->orwhereHas('topic', function (Builder $query) use ($searchTerm) {
                return $query->where('unsign_text','like','%'. strtolower(removeStringUtf8($searchTerm)).'%');
            });
        } else {
            $query->with('topic');
        }

        if ($searchStatus != null) {
            $query->where('status', '=', $searchStatus);
        }

        if($searchSource != null) {
            $query->where('source_capital', $searchSource);
        }
        //check permission topic
        if (!empty(Community::listTopicIdAllowed())){
            $query->whereIn('topic_id',Community::listTopicIdAllowed());
        }
        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);

        $getListSource = ETopicFee::getListSource();
        $getListStatus = ETopicFee::getListStatus();
        $getListStatusChose = ETopicFee::getListStatusChose();
        $listTopic =  $this->getTopic();

        return view('livewire.admin.research.cost.topic-fee-list', [
            'data' => $data, 'getListSource' => $getListSource, 'getListStatus' => $getListStatus,
            'listTopic' => $listTopic, 'getListStatusChose' => $getListStatusChose
        ]);
    }

    public function store()
    {
        $this->validation();

        $topic = Topic::findOrFail($this->topic_id);
        $code = Config::get('common.code.topic_fee');

        $topicFee = TopicFee::create([
            'name' => $this->name,
            'topic_id' => $this->topic_id,
            'code' => $topic->code . $code,
            'status' => ETopicFee::STATUS_NOT_SUBMIT,
            'admin_id' => auth()->id()
        ]);

        $getListType = ETopicFeeDetail::getListType();

        foreach ($getListType as $key => $val)
        {
            TopicFeeDetail::create([
                'name' => $val,
                'type' => $key,
                'topic_fee_id' => $topicFee->id,
                'admin_id' => auth()->id()
            ]);
        }

        session()->flash('success', __("notification.common.success.add"));
        return redirect()->route('admin.research.topic-fee.edit', $topicFee->id);
    }

    public function editTopicFee($editTopicFeeId)
    {
        $this->editTopicFeeId = $editTopicFeeId;
        $data = TopicFee::findOrFail($editTopicFeeId);
        $this->name = $data->name;
        $this->topic_id = $data->topic_id;
    }

    public function update()
    {
        $topicFee = TopicFee::findOrFail($this->editTopicFeeId);
        $topicFee->name = $this->name;
        $topicFee->topic_id = $this->topic_id;
        $topicFee->save();

        session()->flash('success', __("notification.common.success.update"));
        return redirect()->route('admin.research.topic-fee.index');
    }

    public function delete()
    {
        $topicFee = TopicFee::findorFail($this->deleteId);
        $topicFee->delete();

        TopicFeeDetail::where('topic_fee_id', $this->deleteId)->delete();

        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
        return true;
    }

    protected function getTopic()
    {
        $researchPlan = ResearchPlan::with('topic')->where('status', '=', EResearchPlan::STATUS_APPROVAL)->get();
        $listTopic = [];
        if ($researchPlan->isNotEmpty()) {
            foreach ($researchPlan as $plan) {
                if (isset($plan->topic->name)) {
                    $topicFee = TopicFee::where('topic_id', $plan->topic->id)->first();
                    if (empty($topicFee)) {
                        $listTopic[$plan->topic->id] = $plan->topic->name;
                    }
                }
            }
        }

        return $listTopic;
    }

    public function validation()
    {
        $this->validate([
            'name' => ['required', 'regex:/^[a-zA-Z0-9'.config('common.special_character.VN').'\.\,\-\(\)\/ ]+$/', 'max:100'],
            'topic_id' => ['required', 'numeric', 'exists:topic,id'],
        ],[],[
            'name' => __('data_field_name.research_cost.name_topic_fee'),
            'topic_id' => __('data_field_name.approval_topic.topic'),
        ]);
    }

    public function resetData()
    {
        $this->name = null;
        $this->topic_id = null;
        $this->resetValidation();
    }
}
