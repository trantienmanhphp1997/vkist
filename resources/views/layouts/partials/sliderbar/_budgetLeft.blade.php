<!--start sidebar-left-->
<div class="col-md-2 col-xl-3 sidebar-left">
    <ul class="nav flex-column memu-main-left">

        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{__('data_field_name.budget.general')}}
            </a>
            <span class="angle-right">
                        <img src="/images/angle-right.svg" alt="angle-right">
                    </span>
            <ul class="dropdown-menu-left">
                @if(checkPermission('admin.budget.index'))
                    <li>
                        <a class="nav-link active" href="{{route('admin.budget.index')}}">
                            <span class="circle"></span> {{__('data_field_name.budget.list')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.budget.report.index'))
                    <li>
                        <a class="nav-link active" href="{{route('admin.budget.report.index')}}">
                            <span class="circle"></span> {{__('data_field_name.budget.status_budget')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.budget.list-budget-approved.index'))
                    <li>
                        <a class="nav-link active" href="{{route('admin.budget.list-budget-approved.index')}}">
                            <span class="circle"></span> {{__('data_field_name.budget.follow_budget')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
    </ul>
</div>
<!--end sidebar-left-->
