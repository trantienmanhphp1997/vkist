<?php

use App\Http\Controllers\Api\ApprovalController;
use App\Http\Controllers\Api\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function(){
    Route::post('approval',[ApprovalController::class, 'approval']);
});

Route::group([
    'prefix' => 'approve',
], function () {
    Route::post('/', 'App\Http\Controllers\Api\LoginGWController@handleResponse')->name('api.login.approval.handleResponse');
});
Route::post('token',[AuthController::class, 'getToken']);
