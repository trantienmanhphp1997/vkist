<?php
return [
    [
        'v_key' => 'religion',
        'v_value' => 'Phật giáo',
        'type' => '3',
        'order_number' => '1',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Công giáo',
        'type' => '3',
        'order_number' => '2',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Tin lành',
        'type' => '3',
        'order_number' => '3',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Cao Đài',
        'type' => '3',
        'order_number' => '4',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Phật giáo Hòa Hảo',
        'type' => '3',
        'order_number' => '5',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Hồi giáo',
        'type' => '3',
        'order_number' => '6',
    ],
    [
        'v_key' => 'religion',
        'v_value' => "Tôn giáo Baha'i",
        'type' => '3',
        'order_number' => '7',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Tịnh độ Cư sỹ Phật hội Việt Nam',
        'type' => '3',
        'order_number' => '8',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Đạo Tứ Ân Hiếu nghĩa',
        'type' => '3',
        'order_number' => '9',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Bửi Sơn Kỳ hương',
        'type' => '3',
        'order_number' => '10',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Giáo hội Phật đường Nam Tông Minh Sư đạo',
        'type' => '3',
        'order_number' => '11',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Hội thánh Minh lý đạo - Tam Tông Miếu',
        'type' => '3',
        'order_number' => '12',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Chăm Bà la môn',
        'type' => '3',
        'order_number' => '13',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Giáo hội Các thành hữu Ngày sau của Chúa Giê su Ky tô (Mormon)',
        'type' => '3',
        'order_number' => '14',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Phật giáo Hiếu Nghĩa Tà Lơn (Cấp đăng ký hoạt động)',
        'type' => '3',
        'order_number' => '15',
    ],
    [
        'v_key' => 'religion',
        'v_value' => 'Giáo hội Cơ đốc Phục lâm Việt Nam',
        'type' => '3',
        'order_number' => '16',
    ],
];
