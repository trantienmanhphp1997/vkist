<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roles', function(Blueprint $table)
		{
			$table->bigIncrements('id', true)->unsigned();
			$table->string('name', 500);
			$table->string('guard_name');
			$table->string('note',1000);
			$table->timestamps();
            $table->string('code', 255)->nullable()->comment('Ma vai tro');
            $table->tinyInteger('status')->comment('1 hoat dong');
			$table->unique(['name','guard_name']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles');
	}

}
