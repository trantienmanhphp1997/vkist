<?php
return [
    "access" => [
        "header" => "Phân quyền truy cập",
        "menus" => [
            "available" => "Danh mục",
            "selected" => "Danh mục có quyền truy cập",
            "add" => "Thêm quyền",
            "remove" => "Gỡ quyền"
        ]
    ],
    "features" => [
        "header" => "Phân quyền chức năng",
        "table" => [
            "menus" => "Danh mục",
            "feature_permissions" => "Quyền chức năng",
            "all" => "Tất cả",
            "create" => "Tạo mới",
            "view" => "Xem",
            "view_any" => "Xem tất cả",
            "update" => "Cập nhật",
            "update_any" =>"Cập nhật tất cả",
            "delete" => "Xóa",
            "delete_any" => "Xóa tất cả",
            "restore" => "Khôi phục"
        ]
    ],
    'my-menu'=>[
        "over-option-in-menu" => 'Không được phép vượt quá 5 lựa chọn tại menu',
        "save-option-menu-success" => 'Cài đặt menu thành công',
        "save-option-menu-fail" => 'Cài đặt menu thất bại',
    ],
];
