<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Facades\Config;


class TemplateForm extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    protected $table = "template_form";
    protected $guarded=['*'];
    protected $fillable = [];

    public function academicLevel() {
        return $this->belongsTo(MasterData::class, 'academic_level_id');
    }
    public static function getListSearchAble(){
        return self::$searchable;
    }
    private static $searchable = [
        'name',
    ];
}
