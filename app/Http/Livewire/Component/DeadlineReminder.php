<?php

namespace App\Http\Livewire\Component;

use App\Enums\ENotificationType;
use App\Enums\EShoppingStatus;
use App\Enums\ESystemConfigType;
use App\Enums\ETopicStatus;
use App\Models\AssetMaintenance;
use App\Models\Shopping;
use App\Models\SystemConfig;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class DeadlineReminder extends Component
{
    public $generalDeadline = [];
    public $userDeadline = [];

    public $shoppingDeadlines = [];
    public $maintenanceDeadlines = [];

    public function mount()
    {
        $userId = auth()->id();
        $userInfoId = auth()->user()->user_info_id ?? null;

        $generalConfig = SystemConfig::query()->where('type', ESystemConfigType::NOTIFICATION)->whereNull('admin_id')->first();
        $this->generalDeadline = $this->getDeadline($generalConfig->content ?? []);

        $userConfig = SystemConfig::query()->where('type', ESystemConfigType::NOTIFICATION)->where('admin_id', $userId)->first();
        $this->userDeadline = $this->getDeadline($userConfig->content ?? []);

        $remindBefore = $generalConfig->content[ENotificationType::DELIVERING]['remind_before'] ?? 3;
        $via = $userConfig->content[ENotificationType::DELIVERING]['via'] ?? [];
        if(in_array('broadcast', $via)) {
            $this->shoppingDeadlines = Shopping::query()
                ->where('estimated_delivery_date', '>=', Carbon::today())
                ->where('estimated_delivery_date', '<=', Carbon::today()->addDays($remindBefore))
                ->where(function (Builder $query) use ($userId) {
                    return $query->where('proposer_id', $userId)
                        ->orWhere('executor_id', $userId)
                        ->orWhere('approver_id', $userId);
                })
                ->whereIn('status', [EShoppingStatus::APPROVED, EShoppingStatus::PENDING, EShoppingStatus::DELIVERING])
                ->orderBy('estimated_delivery_date', 'asc')
                ->get();
        }

        $remindBefore = $generalConfig->content[ENotificationType::MAINTENANCE]['remind_before'] ?? 3;
        $via = $userConfig->content[ENotificationType::MAINTENANCE]['via'] ?? [];
        if(in_array('broadcast', $via)) {
            $this->maintenanceDeadlines = AssetMaintenance::query()
                ->with('asset')
                ->where('implementation_date', '>=', Carbon::today())
                ->where('implementation_date', '<=', Carbon::today()->addDays($remindBefore))
                ->where(function (Builder $query) use ($userInfoId) {
                    return $query->where('user_info_id', $userInfoId)
                    ->orWhereHas('asset', function(Builder $q) use ($userInfoId) {
                        return $q->where('asset.user_info_id', $userInfoId);
                    });
                })
                ->get();
        }
    }

    public function render()
    {
        return view('livewire.component.deadline-reminder');
    }

    public function getDeadline($content)
    {
        return [
            'idea' => $content[ENotificationType::IDEA_DEADLINE] ?? null,
            'topic' => $content[ENotificationType::TOPIC_DEADLINE] ?? null,
        ];
    }

    public function getHasGeneralIdeaDeadlineProperty()
    {
        return !empty($this->generalDeadline['idea']['deadline']);
    }

    public function getHasGeneralTopicDeadlineProperty()
    {
        return !empty($this->generalDeadline['topic']['deadline']);
    }

    public function getHasUserIdeaDeadlineProperty()
    {
        return !empty($this->userDeadline['idea']['deadline']) && in_array('broadcast', $this->userDeadline['topic']['via'] ?? []);
    }

    public function getHasUserTopicDeadlineProperty()
    {
        return !empty($this->userDeadline['topic']['deadline']) && in_array('broadcast', $this->userDeadline['topic']['via'] ?? []);
    }

    public function getIsJoiningAnyTopicProperty()
    {
        $staff = auth()->user()->info;
        if (empty($staff)) {
            return false;
        }
        $topicCount = $staff->topics()->whereIn('status', [ETopicStatus::STATUS_NOT_SUBMIT, ETopicStatus::STATUS_REJECT])->get()->count();
        return $topicCount > 0;
    }

    public function getHasShoppingDeadlineProperty()
    {
        return count($this->shoppingDeadlines) > 0;
    }

    public function getHasMaintenanceDeadlineProperty()
    {
        return count($this->maintenanceDeadlines) > 0;
    }

    public function getAutoShowDeadlineReminderProperty()
    {
        return ($this->isJoiningAnyTopic && ($this->hasGeneralIdeaDeadline || $this->hasGeneralTopicDeadline))
            || $this->hasUserIdeaDeadline
            || $this->hasUserTopicDeadline
            || $this->hasShoppingDeadline
            || $this->hasMaintenanceDeadline;
    }
}
