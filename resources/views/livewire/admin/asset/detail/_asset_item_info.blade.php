<div class="modal fade modal-fix" wire:ignore.self id="item_info" tabindex="-1" aria-labelledby=""
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.asset.asset_info')}}</h6>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 pd-col name-tab">
                            <div class="list-group">
                                <a class="list-group-item active" id="list-1" data-toggle="list" href="#tab-1"
                                   role="tab">{{__('data_field_name.asset.performance_info')}}</a>
                                <a class="list-group-item" id="list-2" data-toggle="list" href="#tab-2" role="tab">
                                    {{__('data_field_name.asset.process_using')}}</a>
                                <a class="list-group-item" id="list-3" data-toggle="list" href="#tab-3" role="tab">
                                    {{__('data_field_name.asset.repair_history')}}
                                </a>
                            </div>
                        </div>
                        <div class="col-12 pd-col">
                            <div class="tab-content content-tab">
                                <!-- tab-1 -->
                                <div class="info-asset tab-pane fade show active" id="tab-1" role="tabpanel"
                                     aria-labelledby="list-1">
                                    <div class="info-item">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name"> {{__('data_field_name.asset.asset_code_')}}</p>
                                                        <p class="content">: {{$dataItem?$dataItem->code:''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name"> {{__('data_field_name.asset.receiver_date')}}</p>
                                                        <p class="content">: </p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name">{{__('data_field_name.asset.asset_name')}}</p>
                                                        <p class="content">: {{$dataItem?$dataItem->name:''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name"> {{__('data_field_name.asset.guarantee_date')}}</p>
                                                        <p class="content">: {{$guarantee_date}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name"> {{__('data_field_name.asset.situation')}}</p>
                                                        <p class="content">:
                                                            {{$assetStatus[$dataItem?$dataItem->situation:-1]??''}}

                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name">{{__('data_field_name.asset.series_number')}}</p>
                                                        <p class="content">
                                                            : {{$dataItem?$dataItem->series_number:''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name">{{__('data_field_name.asset.user_asset')}}</p>
                                                        <p class="content">
                                                            : {{$dataItem?$dataItem->fullname:''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name">{{__('data_field_name.asset.buy_date')}}</p>
                                                        <p class="content">
                                                            : {{$dataItem?$dataItem->buy_date:''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name">{{__('data_field_name.asset.asset_code_')}}</p>
                                                        <p class="content">: {{$dataItem?$dataItem->code:''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name">{{__('data_field_name.asset.unit_type')}}</p>
                                                        <p class="content">
                                                            : {{$dataItem?$dataItem->unit_type:''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- tab-2 -->
                                <div class="info-asset bg-white p-0 tab-pane fade" id="tab-2" role="tabpanel"
                                     aria-labelledby="list-2">
                                    <table class="table table-general">
                                        <thead>
                                        <tr class="border-radius">
                                            <th scope="col" class="border-radius-left text-left text-uppercase">{{__('data_field_name.asset.full_name')}}</th>
                                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.user_info_code')}}</th>
                                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.user_position')}}</th>
                                            <th scope="col" class="border-radius-right text-center text-uppercase">{{__('data_field_name.asset.situation')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
{{--                                            <td>Theresa Webb</td>--}}
{{--                                            <td>A1D124</td>--}}
{{--                                            <td>Strategic Account Manager</td>--}}
{{--                                            <td><span class="badge badge-fix badge-color-1">Đang sử dụng</span></td>--}}
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- tab-3 -->
                                <div class="info-asset bg-white p-0 tab-pane fade" id="tab-3" role="tabpanel"
                                     aria-labelledby="list-3">
                                    <table class="table table-general">
                                        <thead>
                                        <tr class="border-radius">
                                            <th scope="col" class="border-radius-left text-left w-25 text-uppercase">{{__('data_field_name.asset.asset_department')}}</th>
                                            <th scope="col" class="w-25 text-uppercase">{{__('data_field_name.asset.fix_date')}}</th>
                                            <th scope="col" class="w-35 text-uppercase">{{__('data_field_name.asset.error_content')}}</th>
                                            <th scope="col" class="border-radius-right text-center w-15 text-uppercase">{{__('data_field_name.asset.situation')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
{{--                                            <td>Customer Service</td>--}}
{{--                                            <td>07/05/2016</td>--}}
{{--                                            <td>I yelled at your mom in line at the bank because I'm a ninja!</td>--}}
{{--                                            <td><span class="badge badge-fix badge-color-1">Đang sử dụng</span></td>--}}
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
            </div>
        </div>
    </div>
</div>
