<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToRequestLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_leave', function (Blueprint $table) {
            $table->bigInteger('approved_by')->nullable()->comment('Nguoi duyet');
            $table->tinyInteger('type')->nullable()->comment('Loai nghi: 1 nghi phep nam, 2 nghi huong luong thoi gian, 3 nghi le, 4 hoi hop, 5 nghi viec rieng co luong, 6 nghi viec rieng khong luong, 7 nghi om, 8 trong con om, 9 nghi thai san, 10 nghi vo ki luat, 11 tai nan lao dong');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_leave', function (Blueprint $table) {
            $table->dropColumn('approved_by');
            $table->dropColumn('type');
        });
    }
}
