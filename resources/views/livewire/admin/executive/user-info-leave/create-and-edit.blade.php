<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <a href="">{{__('executive/user-info-leave.breadcrumbs.executive-management')}} \
                        </a><span>{{__('executive/user-info-leave.breadcrumbs.user-info-leave-management')}}</span>
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row mx-5 p-4 bg-white">
                    <div class="col-md-12">
                            <h3 class="title">
                                {{__('executive/user-info-leave.create-update-form.title')}}
                            </h3>
                        </div>
                        <div class="col-md-12">
                            <h4 class="size14 text-uppercase mb-24 text-2">
                                {{__('executive/user-info-leave.create-update-form.general-info')}}
                            </h4>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('executive/user-info-leave.create-update-form.name')}}<span class="text-danger">&nbsp*</span></label>
                                            @livewire('component.search-combobox', ['model_name' => App\Models\UserInf::class, 'filters' => ['fullname'], 'value_render_on_combobox' => 'fullname','targetId' => $user_info_id ?? null])
                                        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('executive/user-info-leave.create-update-form.department')}}</label>
                                        <input type="text" class="form-control" readonly wire:model.lazy="department">
                                        @error('user.username') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('executive/user-info-leave.create-update-form.code')}}</label>
                                        <input type="text" class="form-control" placeholder="" wire:model.lazy="code" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('executive/user-info-leave.create-update-form.position')}}</label>
                                        <input type="email" class="form-control" placeholder="" wire:model.lazy="position" readonly>
                                        @error('user.email') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>


                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('executive/user-info-leave.create-update-form.reason')}}<span class="text-danger">&nbsp*</span></label>
                                        <input type="text" class="form-control" placeholder="" wire:model.defer="reason" >
                                        @error('reason') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('executive/user-info-leave.create-update-form.leave_date')}}<span class="text-danger">&nbsp*</span></label>
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'leaveDate', 'place_holder'=>'', 'default_date' => is_null($leaveDate) ? date('Y-m-d') : $leaveDate ])
                                        @error('leaveDate') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('executive/user-info-leave.create-update-form.status')}}<span class="text-danger">&nbsp*</span></label>
                                        <select class="form-control form-control-lg" wire:model.defer="status">
                                            <option value="0">{{App\Enums\EUserInfoLeaveStatus::valueToName(App\Enums\EUserInfoLeaveStatus::PROCESSING)}}</option>
                                            <option value="1">{{App\Enums\EUserInfoLeaveStatus::valueToName(App\Enums\EUserInfoLeaveStatus::DONE)}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12  upload-container">
                            <h4 class="size14 text-uppercase  mb-12 text-2">
                                {{__('executive/user-info-leave.create-update-form.document')}}
                            </h4>
                            <form wire:submit.prevent="submit" enctype="multipart/form-data">
                                <div class="form-group upload-file">
                                    <label></label>
                                    <div class="bd-attached file_form">
                                        <div class="attached-center">
                                            <input type="file" multiple onchange="uploadFile(this, @this)" id="file_create" class="custom-file-input cur_input">
                                            <span class="file" >
                                                {{__('executive/user-info-leave.create-update-form.attach-file')}}
                                                <img src="/images/Clip.svg" alt="" height="24">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="col-md-12">
                                <div class="row">
                                    @foreach ($listFileUpload as $key => $val)
                                        <div class="form-group col-md-4">
                                            <div class="attached-files">
                                                <img src="/images/File.svg" alt="file">
                                                <div class="content-file">
                                                    <p>{{ $val['file_name'] }}</p>
                                                    <p class="kb">{{ $val['size_file']}}</p>
                                                </div>
                                                <span>
                                                <button wire:click.prevent="deleteNewFileUpload({{$key}})"
                                                        style="border: none;background: white" type="button"><img
                                                        src="/images/close.svg" alt="close"/>
                                                </button>
                                            </span>
                                            </div>
                                        </div>
                                    @endforeach
                                    @foreach ($listOldFileTemp as $key => $val)
                                        <div class="form-group col-md-4">
                                            <div class="attached-files">
                                                <img src="/images/File.svg" alt="file">
                                                <div class="content-file" wire:click="download('{{$val['url']}}', '{{$val['file_name']}}')" style="cursor: pointer">
                                                    <p>{{ $val['file_name'] }}</p>
                                                    <p class="kb">{{ $val['size_file']}}</p>
                                                </div>
                                                <span>
                                                    <button wire:click.prevent="deleteFileOldTemp({{$key}})"
                                                            style="border: none;background: white" type="button"><img
                                                            src="/images/close.svg" alt="close"/>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="file-loading position-absolute bg-primary rounded text-center text-white" style="transition: width 0.3s; bottom: 0px; left: 0; height: 5px; width: 0%;">&nbsp;</div>
                                <div class="row">
                                    @error('fileUpload')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                
                                    @error('current_total_file')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror   
                                </div>
                                <div class="row">
                                    <div class="text-danger upload-error"></div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 mt-4">
                            <div class="d-flex justify-content-center group-btn2">
                                <a class="btn btn-cancel" href="{{route('admin.executive.user-info-leave.index')}}" style="color:gray">
                                    {{__('common.button.cancel')}}
                                </a>
                                <button type="button" wire:click="store" id="btn-save-user-info-leave" class="btn btn-save mr-2">
                                    {{__('common.button.save')}}
                                </button>
                            </div>

                        </div>
            </div>

        </div>

    </div>
    <script>
        function uploadFile(input, proxy = null, index = 0) {
                let file = input.files[index];
                if (!file || !proxy) return;
                let $fileLoading = $(input).parents('.upload-container').find('.file-loading');
                let $error = $(input).parents('.upload-container').find('.upload-error');
                $fileLoading.width('0%');
                if(file.size/(1024*1024) >= {{$limit_file_size}}) {
                    console.log($error);
                    $error.html("{{ __('notification.upload.maximum_size', ['value' => $limit_file_size]) }}");
                    return;
                }
                let mimes = {!! json_encode($accept_file) !!};
                let extension = file.name.split('.').pop();
                if (!mimes.includes(extension)) {
                    $error.html("{{ __('notification.upload.mime_type', ['values' => implode(', ', $accept_file)]) }}");
                    return;
                }
                proxy.upload('fileUpload', file, (uploadedFilename) => {
                    setTimeout(() => {
                        $fileLoading.width('0%');
                        uploadFile(input, proxy, index + 1);
                    }, 500);
                }, () => {
                    console.log(error);
                }, (event) => {
                    $fileLoading.width(event.detail.progress + '%');
                });
                
            }
    </script>

</div>


