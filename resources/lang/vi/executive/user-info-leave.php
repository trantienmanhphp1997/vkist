<?php
return [
    'breadcrumbs' => [
        'executive-management' => 'Quản lý hợp đồng lao động và lương',
        'user-info-leave-management' => 'Quản lý nghỉ việc',
        'user-info-leave-list' => 'Danh sách nhân sự nghỉ việc',
    ],
    'table_column' => [
        'code' => 'MÃ NHÂN VIÊN',
        'name' => 'TÊN NHÂN VIÊN',
        'department' => 'PHÒNG CÔNG TÁC',
        'position' => 'VỊ TRÍ CÔNG VIỆC',
        'reason' => 'LÝ DO NGHỈ',
        'leave-date' => 'NGÀY NGHỈ',
        'status' => 'TRẠNG THÁI',
        'action' => 'HÀNH ĐỘNG'
    ],
    'title' => [
        'create' => 'Tạo mới',
        'edit' => 'Chỉnh sửa',
        'no-result' => 'Không có kết quả!',
        'part-1' => 'Hiển thị',
        'part-2' => 'loại đề tài trong',
    ],
    'empty-record' => 'Không có bản ghi nào',
    'placeholder' => [
        'search' => 'Tìm kiếm',
    ],
    'button' => [
        'add' => "Thêm nhân sự nghỉ việc",
    ],
    'modal' => [
        'title' => [
            'delete' => 'Xác nhận xoá',
        ],
        'body' => [
            'delete-warning' => 'Bạn có xóa không? Thao tác này không thể phục hồi!',
        ],
        'footer' => [
            'back' => 'Quay lại',
            'cancel' => 'Huỷ',
            'delete' => 'Xoá',
            'close' => 'Đóng',
        ]
    ],
    'status' => [
        'processing' => "Đang thực hiện",
        'done' => "Đã xử lý",
    ],
    'create-update-form' => [
        'title' => 'Xác nhận thông tin nghỉ việc',
        'general-info' => 'Thông tin chung',
        'document' => 'Tài liệu đính kèm',
        'attach-file' => 'Đính kèm file',
        'name' => "Họ và tên",
        'code' => 'Mã nhân viên',
        'department' => 'Phòng công tác',
        'position' => 'Vị trí công tác',
        'reason' => 'Lý do nghỉ việc',
        'leave_date' => 'Ngày nghỉ việc',
        'status' => 'Trạng thái',
    ],
    'validate' => [
        'choosedUser' => 'Thông tin nhân sự',
    ],
];
