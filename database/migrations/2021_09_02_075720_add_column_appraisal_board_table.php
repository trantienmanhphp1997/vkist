<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAppraisalBoardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_board', function (Blueprint $table) {
            $table->foreignId('topic_fee_id')->nullable()->constrained('topic_fee');
            $table->foreignId('research_plan_id')->nullable()->constrained('research_plan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_board', function (Blueprint $table) {
            $table->dropColumn('topic_fee_id');
            $table->dropColumn('research_plan_id');
        });
    }
}
