<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\UserInf;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserInfControllerTest extends TestCase
{
    use RefreshDatabase;

    public $routeIndex = 'admin.user.index';
    public $routeEdit = 'admin.user.edit';
    public $routeCreate = 'admin.user.create';

    public function testIndexUserInf()
    {
        $this->actingAsAdmin()->get(route($this->routeIndex))->assertOk();
    }

    public function testUpdateUserInfSuccess()
    {
        $data = [
            'fullname' => 'Someone',
            'code' => '127434',
            'department_id' => '1',
            'position_id' => '1',
            'address' => 'Ha Noi',
            'phone' => '0928343588',
            'birthday' => '2000-09-27',
            'gender' => '1',
            'nationality_id' => '1',
            'issued_date' => '2018-09-11',
            'issued_place' => 'vietnam'
        ];

        $userInfo = UserInf::create($data);
        $this->actingAsAdmin()->get(route($this->routeEdit, ['id' => $userInfo->id]))->assertOk();

        $data['fullname'] = 'Hù man';
        $data['code'] = '127888';

        $this->actingAsAdmin()
            ->patch(route('admin.user.update', ['id' => $userInfo->id]), $data)
            ->assertSessionDoesntHaveErrors()
            ->assertRedirect(route($this->routeIndex));
    }

    public function testUpdateUserInfFail() {
        $data = [
            'fullname' => 'Anyone',
            'code' => '127001',
            'department_id' => '1',
            'position_id' => '1',
            'address' => 'Ha Noi',
            'phone' => '036547899',
            'birthday' => '2000-09-28',
            'gender' => '1',
            'nationality_id' => '1',
            'issued_date' => '2018-09-12',
            'issued_place' => 'vietnam'
        ];

        $userInfo = UserInf::create($data);
        $this->actingAsAdmin()->get(route($this->routeEdit, ['id' => $userInfo->id]))->assertOk();

        $data['fullname'] = '';
        $data['code'] = '';

        $this->actingAsAdmin()
            ->patch(route('admin.user.update', ['id' => $userInfo->id]), $data)
            ->assertRedirect(route($this->routeEdit, ['id' => $userInfo->id]))
            ->assertSessionHasErrors(['fullname', 'code']);
    }

    public function testStoreUserInfSuccess()
    {
        $data = [
            'code' => '127434',
            'fullname' => 'Someone',
            'birthday' => '2000-09-29',
            'address' => "HaNoi",
            'issued_date' => '2018-09-13',
            'issued_place' => 'vietnam',
            'department_id' => '1',
            'position_id' => '1',
            'phone' => '0928343588',
            'id_card' => '1',
            'nationality_id' => '1',
        ];

        $this->actingAsAdmin()->get(route($this->routeCreate))->assertOk();

        $this->actingAsAdmin()
            ->post(route('admin.user.store'), $data)
            ->assertRedirect(route($this->routeIndex));
    }

    public function testStoreUserInfFail()
    {
        $data = [
            'fullname' => '',
            'code' => '',
            'department_id' => '1',
            'position_id' => '1',
            'address' => '',
            'phone' => '',
            'birthday' => '',
            'gender' => '1',
            'nationality_id' => '1',
            'issued_date' => '',
            'issued_place' => ''
        ];

        $this->actingAsAdmin()->get(route($this->routeCreate))->assertOk();

        $this->actingAsAdmin()
            ->post(route('admin.user.store'), $data)
            ->assertRedirect(route($this->routeCreate))
            ->assertSessionHasErrors(['fullname', 'code', 'address', 'phone', 'birthday', 'issued_date', 'issued_place']);
    }
}
