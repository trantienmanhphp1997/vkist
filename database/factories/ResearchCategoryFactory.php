<?php

namespace Database\Factories;

use App\Enums\EResearchCategoryType;
use App\Models\ResearchCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResearchCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ResearchCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->colorName,
            'description' => $this->faker->sentence,
            'type' => EResearchCategoryType::FIELD
        ];
    }
}
