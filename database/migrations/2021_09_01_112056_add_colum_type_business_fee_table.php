<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumTypeBusinessFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_fee', function (Blueprint $table) {
            $table->string('tracking_type',255)->nullable()->comment('Loại hình theo dõi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_fee', function (Blueprint $table) {
            $table->dropColumn('tracking_type');
        });
    }
}
