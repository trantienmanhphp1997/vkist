<?php

namespace App\Http\Livewire\Admin\System\Account;

use App\Http\Livewire\Base\BaseTrimString;
use App\Models\GroupUser;
use App\Models\Role;
use App\Models\User;
use App\Models\UserInf;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Mail\CreateUserSuccessfully;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Enums\ESystemConfigType;
use App\Models\SystemConfig;

class AccountEditor extends BaseTrimString
{

    public User $user;
    public array $info = [];
    public array $readyRoleList = [];

    public $countPassed = 0;

    protected $listeners = [
        'valid-data-event' => 'validData',
        'invalid-data-event' => 'invalidData'
    ];

    public function mount($id = 0)
    {
        $this->user = User::query()->findOrNew($id);
    }

    public function render()
    {
        return view('livewire.admin.system.account.account-editor');
    }

    public function submit()
    {
        $this->countPassed = 0;
        $this->emit('validate-data');
    }

    public function validData($tab, $data)
    {
        $this->countPassed += 1;

        if ($tab == 'tab-general') {
            $this->info = $data;
        } elseif ($tab == 'tab-role') {
            $this->readyRoleList = $data;
        }

        if ($this->countPassed == 2) {
            $this->save();
        }
    }

    public function invalidData($tab, $message)
    {
        $this->countPassed = 0;
        $this->dispatchBrowserEvent('failed-tab-envent', $message);
    }

    protected function save()
    {
        // save info
        $this->user->email = $this->info['userInfo']['email'];
        $this->user->username = $this->info['username'];
        $this->user->name = $this->info['userInfo']['fullname'];
        $this->user->user_info_id = $this->info['userInfoId'];
        $this->user->group_id = $this->info['groupId'];
        $this->user->status = $this->info['status'];

        $config = SystemConfig::query()->where('name','Import salary config')->where('type', ESystemConfigType::TYPE_IMPORT_SALARY)->first();
        $password_default = $config->content['password_default']['value'] ?? '123456aA@';
        $isCreatingUser = !$this->user->exists;
        $message = __('notification.common.success.update');

        if ($isCreatingUser) {
            $this->user->password = Hash::make($password_default);
            $message = __('notification.common.success.add');
        }

        if (!empty($this->user->info)) {
            $this->user->info->update([
                'user_id' => null
            ]);
        }

        $this->user->save();

        UserInf::query()->where('id', $this->user->user_info_id)->update([
            'user_id' => $this->user->id
        ]);

        if ($isCreatingUser) {
            try {
                Mail::to($this->user)->send(new CreateUserSuccessfully($this->user->name, $this->user->username, $password_default));
            } catch(Exception $exception) {
               $this->user->password = Hash::make(config('common.default_password'));
               $this->user->save();
            }
        }

        // save role
        if($this->user->exists) {
            $this->user->roles()->syncWithoutDetaching(array_keys($this->readyRoleList));
        }

        session()->flash('success', $message);
        return $this->redirectRoute('admin.system.account.index');
    }
}
