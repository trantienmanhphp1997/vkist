<!-- #section:basics/sidebar -->
<div id="sidebar" class="sidebar responsive">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <em class="ace-icon fa fa-signal"></em>
            </button>

            <button class="btn btn-info">
                <em class="ace-icon fa fa-pencil"></em>
            </button>

            <!-- #section:basics/sidebar.layout.shortcuts -->
            <button class="btn btn-warning">
                <em class="ace-icon fa fa-users"></em>
            </button>

            <button class="btn btn-danger">
                <em class="ace-icon fa fa-cogs"></em>
            </button>

            <!-- /section:basics/sidebar.layout.shortcuts -->
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li {{ setActive('home') }}>
            <a href="{{url('home')}}">
                <em class="menu-icon fa fa-tachometer"></em>
                <span class="menu-text"> Dashboard </span>
            </a>

            <strong class="arrow"></strong>
        </li>
        @hasanyrole('administrator|contract-viettel-buy-list|contract-viettel-sale-list|contract-partner-list|contract-category-list|contract-file-list')
            <li {{ setOpen('contract') }}>
                <a href="#" class="dropdown-toggle">
                    <em class="menu-icon fa fa-book"></em>
                    <span class="menu-text"> Quản lý hợp đồng</span>
                    <strong class="arrow fa fa-angle-down"></strong>
                </a>
                <strong class="arrow"></strong>
                <ul class="submenu">
                    @hasanyrole('administrator|contract-viettel-buy-list')
                        <li {{ setActive('contract/viettel-buy') }}>
                                <a href="{{url('contract/viettel-buy')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Hợp đồng Viettel mua
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|contract-viettel-sale-list')
                        <li {{ setActive('contract/viettel-sell') }}>
                                <a href="{{url('contract/viettel-sell')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Hợp đồng Viettel bán
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|contract-partner-list')
                        <li {{ setActive('contract/viettel-partner') }}>
                                <a href="{{url('contract/viettel-partner')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Hợp đồng của đối tác Viettel
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|contract-category-list')
                        <li {{ setActive('contract/category') }}>
                                <a href="{{url('contract/category')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Quản lý loại hợp đồng
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|contract-file-list')
                        <li {{ setActive('contract/file') }}>
                            <a href="{{url('contract/file')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Quản lý danh sách giấy tờ
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                </ul>
            </li>
        @endhasanyrole
        @hasanyrole('administrator|partner-list|list-employee-list')
            <li  {{ setOpen('partner') }}>
                <a href="#" class="dropdown-toggle">
                    <em class="menu-icon fa fa-users"></em>
                    <span class="menu-text"> Đối tác</span>

                    <strong class="arrow fa fa-angle-down"></strong>
                </a>

                <strong class="arrow"></strong>
                <ul class="submenu">
                    @hasanyrole('administrator|partner-list')
                    <li {{ setActive('partner') }}>
                        <a href="{{route('partner.index')}}">
                            <em class="menu-icon fa fa-caret-right"></em>
                            Danh sách đối tác
                        </a>
                        <strong class="arrow"></strong>
                    </li>
                    @endhasanyrole
                    @hasanyrole('administrator|list-employee-list')
                        <li {{ setActive('businessPartner/listEmployee') }}>
                            <a href="{{route('admin.businessPartner.listEmployee')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Danh sách nhân viên phụ trách hợp đồng
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                </ul>
            </li>
        @endhasanyrole
        @hasanyrole('administrator|film-list|music-list|phim-le-list|contract-media-sale-money-list|manage-list|media-role-list|countries-list')
            <li {{ setOpen('media') }}>
                <a href="#" class="dropdown-toggle">
                    <em class="menu-icon fa fa-desktop"></em>
                    <span class="menu-text"> Sản phẩm media</span>
                    <strong class="arrow fa fa-angle-down"></strong>
                </a>

                <strong class="arrow"></strong>
                <ul class="submenu">
                    @hasanyrole('administrator|film-list')
                        <li {{ setActive('media/films') }}>
                            <a href="{{route('media.films')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Danh sách phim
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|music-list')
                        <li {{ setActive('media/music') }}>
                            <a href="{{route('media.music.index')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Danh sách nhạc
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|phim-le-list')
                        <li {{ setActive('media/phim-le') }}>
                            <a href="{{route('media.phim-le')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Danh sách phim lẻ
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|contract-media-sale-money-list')
                        <li {{ setActive('media/contract-media-sale-money') }}>
                            <a href="{{route('media.contract-media-sale-money')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Danh sách sản phẩm bán
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|manage-list')
                        <li {{ setActive('media/manage') }}>
                            <a href="{{route('media.manage')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Quản trị tư liệu
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|media-role-list')
                        <li {{setActive('media/role')}}>
                            <a href="{{route('media.role.index')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Danh sách quyền hạn
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|countries-list')
                        <li {{setActive('media/countries')}}>
                            <a href="{{route('media.countries')}}">
                            <em class="menu-icon fa fa-caret-right"></em>
                                Danh sách quốc gia
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                </ul>
            </li>
        @endhasanyrole
        @hasanyrole('administrator|users-list|roles-list|logs-list')
        <li {{ setOpen('tool') }}>
            <a href="#" class="dropdown-toggle">
                <em class="menu-icon fa fa-cogs"></em>
                <span class="menu-text"> Hệ thống </span>

                <strong class="arrow fa fa-angle-down"></strong>
            </a>

            <strong class="arrow"></strong>

            <ul class="submenu">
                @hasanyrole('administrator|roles-list')
                <li {{ setActive('tool/roles') }}>
                    <a href="{{route('tool.roles')}}">
                        <em class="menu-icon fa fa-caret-right"></em>
                        Danh sách role
                    </a>

                    <strong class="arrow"></strong>
                </li>
                @endhasanyrole
                @hasanyrole('administrator|users-list')
                <li {{ setActive('tool/createAdmin') }}>
                    <a href="{{route('createAdmin.index')}}">
                        <em class="menu-icon fa fa-caret-right"></em>
                        Quản lý người dùng
                    </a>
                    <strong class="arrow"></strong>
                </li>
                @endhasanyrole
                @hasanyrole('administrator|logs-list')
                <li {{ setActive('action_log') }}>
                    <a href="{{route('action_log')}}">
                        <em class="menu-icon fa fa-caret-right"></em>
                        Log tác động hệ thống
                    </a>

                    <strong class="arrow"></strong>
                </li>
                @endhasanyrole
            </ul>
        </li>
        @endhasanyrole
        @hasanyrole('administrator|revenue-film-list|revenue-music-list|revenue-type-list|revenue-fee-list')
            <li {{ setOpen('revenue') }}>
                <a href="#" class="dropdown-toggle">
                    <em class="menu-icon fa fa-pencil-square-o"></em>
                    <span class="menu-text"> Doanh thu </span>

                    <strong class="arrow fa fa-angle-down"></strong>
                </a>
                <strong class="arrow"></strong>
                <ul class="submenu">
                    @hasanyrole('administrator|revenue-film-list')
                        <li {{ setActive('revenue/film') }}>
                            <a href="{{route('revenue.film')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Thống kê doanh thu phim
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|revenue-music-list')
                        <li {{ setActive('revenue/music') }}>
                            <a href="{{route('revenue.music')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Thống kê doanh thu nhạc
                            </a>

                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|revenue-fee-list')
                        <li {{ setActive('revenue/fee') }}>
                            <a href="{{route('type_fee.index')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Thống kê chi phí
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|revenue-type-list')
                        <li {{ setActive('revenue/type') }}>
                            <a href="{{route('revenue_type.index')}}">
                                <em class="menu-icon fa fa-caret-right"></em>
                                Thống kê loại doanh thu
                            </a>
                            <strong class="arrow"></strong>
                        </li>
                    @endhasanyrole
                </ul>
            </li>
        @endhasanyrole
    </ul><!-- /.nav-list -->

    <!-- #section:basics/sidebar.layout.minimize -->
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <em class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></em>
    </div>

    <!-- /section:basics/sidebar.layout.minimize -->
</div>
<!-- /section:basics/sidebar -->
