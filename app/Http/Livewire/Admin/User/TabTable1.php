<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;
use App\Models\UserResearchs;
use App\Component\Recursive;
use App\Enums\ETopicStatus;
use App\Models\UserInf;
use Route;

class TabTable1 extends Component
{

    public $user_info_id;

    public function render()
    {

        $userInfo = UserInf::query()->with([
            'topicHasUserInfos',
            'topicHasUserInfos.topic',
            'topicHasUserInfos.role'
        ])->find($this->user_info_id);

        $items = $userInfo->topicHasUserInfos ?? [];

        return view('livewire.admin.user.tab-table1', ['userInfo' => $userInfo, 'items' => $items]);
    }

}
