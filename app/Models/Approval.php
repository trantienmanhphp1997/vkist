<?php

namespace App\Models;

use App\Enums\EApproval;
use App\Notifications\Handlers\NotificationHandler;
use App\Notifications\WorkResultNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Approval extends BaseModel
{
    use HasFactory;

    protected $table = 'approval';
    protected $fillable = [
        'name', 'code', 'contract_code', 'type', 'status', 'complete_date', 'topic_id', 'research_plan_id',
        'ideal_id', 'topic_fee_id', 'appraisal_board_id', 'user_info_id', 'admin_id', 'unsign_text'
    ];

    public function ideals()
    {
        return $this->belongsToMany(Ideal::class, 'approval_ideal_detail', 'approval_id', 'ideal_id');
    }

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id', 'id');
    }

    public function topicFee()
    {
        return $this->belongsTo(TopicFee::class, 'topic_fee_id', 'id');
    }

    public function researchPlan()
    {
        return $this->belongsTo(ResearchPlan::class, 'research_plan_id');
    }

    public function appraisal()
    {
        return $this->belongsTo(AppraisalBoard::class, 'appraisal_board_id', 'id');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    private static $searchable = [
        'name',
    ];
    public static function getListSearchAble()
    {
        return self::$searchable;
    }

    protected static function boot()
    {
        parent::boot();

        static::updated(function (Approval $model) {
            // kiểm tra loại hồ sơ
            // hồ sơ = đề tài => thông báo cho tất cả các thành viên & người tạo
            // hồ sơ = ý tưởng => thông báo cho người tạo
            // hồ sơ = dự toán || kế hoạch nghiên cứu => thông báo báo cho chủ nhiệm đề tài ứng với dự toán & người tạo

            $action = "";
            if ($model->status == EApproval::STATUS_APPROVAL) {
                $action = 'approve';
            } elseif ($model->status == EApproval::STATUS_REJECT) {
                $action = 'reject';
            } else {
                return;
            }

            if ($model->type == EApproval::TYPE_IDEAL) {
                $result = $model->ideals()->with('admin')->get();
                foreach ($result as $idea) {
                    NotificationHandler::sendNotification($idea->admin, new WorkResultNotification($idea, $action));
                }
            } elseif ($model->type == EApproval::TYPE_TOPIC) {
                $topic = $model->topic;
                if (empty($topic)) {
                    return;
                }
                $topic->load('userInfos', 'userInfos.account');
                foreach ($topic->userInfos as $userInfo) {
                    NotificationHandler::sendNotification($userInfo->account, new WorkResultNotification($topic, $action));
                }
            } elseif ($model->type == EApproval::TYPE_COST) {
                $fee = $model->topicFee;
                if (empty($fee)) {
                    return;
                }
                $fee->load('topic', 'topic.leader', 'topic.leader.account');
                NotificationHandler::sendNotification($fee->topic->leader->account ?? null, new WorkResultNotification($fee, $action));
            } elseif ($model->type == EApproval::TYPE_PLAN) {
                $plan = $model->researchPlan;
                if (empty($plan)) {
                    return;
                }
                $plan->load('topic', 'topic.leader', 'topic.leader.account');
                NotificationHandler::sendNotification($plan->topic->leader->account ?? null, new WorkResultNotification($plan, $action));
            } // else {
            //     return;
            // }
        });
    }
}
