<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTopicFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_fee', function($table) {
            $table->dropColumn('source_capital');
        });

        Schema::table('topic_fee', function($table) {
            $table->date('expense_date')->nullable()->comment('Thoi gian du chi');
            $table->tinyInteger('source_capital')->nullable()->comment('Nguon von');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_fee', function($table) {
            $table->dropColumn('expense_date');
            $table->dropColumn('source_capital');
        });
    }
}
