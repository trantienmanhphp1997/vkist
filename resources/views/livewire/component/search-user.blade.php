<div>
    <input
        type="text"
        class="form-input"
        placeholder="Search ..."
        wire:model="query"
        wire:keydown.escape="resetInput"
        wire:keydown.tab="resetInput"
        wire:keydown.enter="resetInput"

    />
{{--    <div wire:loading class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg">--}}
{{--        <div class="list-item">Searching...</div>--}}
{{--    </div>--}}
    @if(!empty($query)&&$successful==false)

        <div class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg search_bar_div" style="max-height: 250px; overflow-y: auto;overflow-x: hidden">
            @if(!empty($data))
                @foreach($data as $i => $item)
                    <a wire:click="setInput({{ $item['id'] }},'{{ $item['fullname'] }}')"
                        class="search_bar"
                    >{{ $item['fullname'] }}</a>
                @endforeach
            @else
                <div class="search_bar">{{__('common.message.no_result')}}</div>
            @endif
        </div>
    @endif
</div>
