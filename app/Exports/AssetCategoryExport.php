<?php

namespace App\Exports;

use App\Models\AssetCategory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class AssetCategoryExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $searchTerm;
    public function __construct($searchTerm = '')
    {
        $this->searchTerm = $searchTerm;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $index = 1;
        $query = AssetCategory::query()->with('asset');
        if (!empty($this->searchTerm)) {
            $query->where('asset_category.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')
            ->orWhereRaw("LOWER(code) LIKE LOWER('%{$this->searchTerm}%')");
        }

        $list = $query->get();
        foreach ($list as $data) {
            $data->index = $index++;
        }
        return $list;
    }

    public function headings(): array
    {
        return [
            'STT',
            'MÃ LOẠI TÀI SẢN',
            'TÊN LOẠI TÀI SẢN',
            'CÁCH QUẢN LÝ',
            'GHI CHÚ',
            'SỐ LƯỢNG',
            'CHƯA SỬ DỤNG',
            'ĐANG SỬ DỤNG',
        ];
    }

    public function map($data): array
    {
        return [
            $data->index,
            $data->code,
            $data->name,
            $data->type_manage == 0 ? 'Quản lý theo mã' : 'Quản lý theo số lượng',
            $data->note,
            $data->asset->sum('quantity'),
            $data->asset->sum('quantity') - $data->asset->sum('amount_used'),
            $data->asset->sum('amount_used'),
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);

        
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:H1');
            $event->sheet->setCellValue('A1', 'DANH SÁCH LOẠI TÀI SẢN');
            $event->sheet->getStyle('A1:H1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:H3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        }, ];

    }
    public function title(): string
    {
        return 'DANH SÁCH LOẠI TÀI SẢN';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
