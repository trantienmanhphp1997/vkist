<?php
return [
    'breadcrumbs' => [
        'decision-reward-management' => 'Quản lý khen thưởng',
        'decision-reward-list' => 'Danh sách quyết định khen thưởng',
    ],
    'form' => [
        'general_info' => 'Thông tin chung',
        'document' => 'Tài liệu đính kèm',
        'receiver_list' => 'Danh sách khen thưởng',
        'attach_file' => 'Đính kèm file',
    ],
    'table_column' => [
        'name' => 'QUYẾT ĐỊNH',
        'number_decision' => 'SỐ QUYẾT ĐỊNH',
        'type_of_decision' => 'lOẠI QUYẾT ĐỊNH',
        'decision_date' => 'NGÀY QUYẾT ĐỊNH',
        'type_of_prize' => 'HÌNH THỨC',
        'type_reward' => 'LOẠI HÌNH KHEN THƯỞNG',
        'receiver' => 'ĐỐI TƯỢNG',
        'action' => 'HÀNH ĐỘNG'
    ],
    'receiver_table_column' => [
        'name' => 'Họ và tên',
        'code' => 'Mã nhân viên',
        'department' => 'Đơn vị công tác',
        'position' => 'Vị trí công việc',
        'prize_value' => 'Giá trị khen thưởng',
        'reason' => 'Lý do',
        'action' => 'Hành động',
    ],
    'decision-reward-prize-type' => [
        'money' => 'Thưởng tiền',
        'glorify' => 'Biểu dương',
        'product' => 'Tặng hiện vật'
    ],
    'decision-reward-receiver-type' => [
        'organization' => 'Tổ chức',
        'individual' => 'Cá nhân',
    ],
    'decision-reward-decision-type' => [
        'reward' => 'Khen thưởng',
        'discipline' => 'Kỷ luật',
    ],

    'label' => [
        'name' => "Quyết định khen thưởng",
        'reward_date' => "Ngày khen thưởng",
        'number_decision' => "Số quyết định",
        'user_decision' => 'Người quyết định',
        'type_of_receiver' => 'Đối tượng khen thưởng',
        'reason' => 'Lý do',
        'type_reward' => 'Loại khen thưởng',
        'type_of_decision' => 'Loại quyết định',
        'decision_date' => 'Ngày quyết định',
        'type_of_prize' => 'Hình thức khen thưởng',
        'user_decision_position' => 'Chức vụ người quyết định',
        'total_value' => 'Tổng giá trị',
        'base' => 'Căn cứ'
    ],
    'placeholder' => [
        'search' => 'Tìm kiếm',
    ],
    'title' => [
        'create' => 'Thêm mới quyết định khen thưởng',
        'edit' => 'Chỉnh sửa quyết định khen thưởng',
        'create-receiver' => 'Thêm mới đối tượng khen thưởng',
        'no-result' => 'Không có kết quả!',
        'part-1' => 'Hiển thị',
        'part-2' => 'loại đề tài trong',
    ],
    'empty-record' => 'Không có bản ghi nào',
    
    'modal' => [
        'title' => [
            'delete' => 'Xác nhận xoá',
            'create' => 'Thêm loại đề tài',
            'edit' => 'Sửa loại đề tài',
        ],
        'body' => [
            'delete-warning' => 'Bạn có xóa không? Thao tác này không thể phục hồi!',
            'name' => 'Tên loại đề tài',
            'description' => 'Mô tả',
            'note' => 'Ghi chú',
        ],
        'footer' => [
            'back' => 'Quay lại',
            'delete' => 'Xoá',
            'close' => 'Đóng',
            'save' => 'Lưu',
        ]
    ],
    'validate' => [
        'name' => 'Tên loại đề tài',
        'note' => 'Ghi chú loại đề tài',
        'description' => 'Mô tả loại đề tài',
        'total_value_max_length' => ':attribute không được lớn hơn 10 chữ số'
    ],
    'notification' => [
        'edit-success' => 'Chỉnh sửa thành công',
        'create-success' => 'Thêm mới thành công',
        'add-new-receiver-success' => 'Thêm mới đối tượng khen thưởng thành công',
        'add-new-receiver-fail' => 'Thêm mới đối tượng khen thưởng không thành công, đối tượng khen thưởng đã tồn tại',
    ]
];
