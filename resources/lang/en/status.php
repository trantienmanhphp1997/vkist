<?php
return [
    "record_plan" => [
        "inactive" => "Draft",
        "pending" => "Waiting for confirmation",
        "approved" => "Finished",
        "in_progress" => "In progress",
        "wrap" => "Closed", // Closed, not finished paperwork
        "done" => "Done",
        "cancel" => "Canceled"
    ],
    "record_plan_change_status" => [
        "inactive" => "Draft",
        "pending" => "Waiting for confirmation",
        "approved" => "Close schedule",
        "in_progress" => "Start production",
        "wrap" => "Close the machine", // Closed the machine, not finished the paperwork
        "done" => "Done",
        "cancel" => "Cancel"
    ],
    "propos" => [
        "pending" => "Waiting for approval",
        "approved" => "Assigned",
        "rejected" => "Reject human feces",
        "cancel" => "Cancel"
    ],
    "propos_device" => [
        "pending" => "Waiting for approval",
        "approved" => "Allocated",
        "delivered" => "Delivered device",
        "returned" => "Returned device",
        "rejected" => "Rejected",
        "duplicated" => "Suggested duplicate calendar",
    ],
    "member" => [
        "inactive" => "Inactive",
        "active" => "Active",
    ],
    "car" => [
        "all" => "All",
        "inactive" => "Inactive",
        "active" => "Active",
        "borrowed" => "loaned",
        "instock" => "In stock",
        "broken" => "Broken",
    ],
    "transportation" => [
        "ready" => "Ready",
        "busy" => "There is a flight",
        "broken" => "Busy",
    ],
    "device" => [
        "all" => "All",
        "inactive" => "Inactive",
        "active" => "Active",
        "borrowed" => "loaned",
        "instock" => "In stock",
        "broken" => "broken",
        "request" => "Device return request"
    ],
    "device_category" => [
        "ready" => "Ready",
    ],
    "assignments" => [
        "devices" => [
            "proposal" => "Proposal",
            "accept" => "Agree",
            "cancel" => "Cancel"
        ],
        "members" => [
            "proposal" => "Proposal",
            "accept" => "Agree",
            "cancel" => "Cancel"
        ]
    ],
    "stage" => [
        "active" => "Active",
        "inactive"=> "Inactive"
    ],
    'profile_type' => [
        'ideal' => 'idea',
        'topic' => 'Theme',
        'cost_estimate' => 'Cost estimation',
        'research_plan' => 'Research plan',
    ],
    'status_topic' => [
        'decline_update' => 'Deny and request update',
        'complete_online' => 'Complete online profile',
        'waiting_appraisal' => 'Waiting for verification',
    ],
    'news'=>[
        'empty'=>'News is empty'
    ]
];
