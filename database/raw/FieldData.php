<?php
return [
    [
        'type' => 22,
        'v_key' => 'Field',
        'v_value' => 'IT',
        'order_number' => 0
    ],
    [
        'type' => 22,
        'v_key' => 'Field',
        'v_value' => 'BT',
        'order_number' => 1
    ],
    [
        'type' => 22,
        'v_key' => 'Field',
        'v_value' => 'IT-BT',
        'order_number' => 2
    ],
    [
        'type' => 22,
        'v_key' => 'Field',
        'v_value' => 'IP',
        'order_number' => 3
    ],
    [
        'type' => 22,
        'v_key' => 'Field',
        'v_value' => 'EMPES',
        'order_number' => 4
    ],
];
