<?php

namespace App\Http\Livewire\Admin\Asset\LostCancelLiquidation;

use App\Enums\EAssetSituation;
use App\Enums\EAssetStatus;
use App\Exports\AssetCancelExport;
use App\Models\Asset;
use App\Models\AssetAllocatedRevoke;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class ListCancel extends Component
{
    public $searchTerm;
    public $idConfirm;
    public $assetSituation;
    public $assetStatus;
    public $deleteId;
    public $errorQuantity;
    public function mount()
    {
        $this->assetSituation = [
            EAssetSituation::SITUATION_NOT_USE =>__('data_field_name.asset.no_use'),
            EAssetSituation::SITUATION_USING => __('data_field_name.asset.using'),
        ];
        $this->assetStatus = EAssetStatus::getList();

    }
    public function render()
    {
        $listCancel = AssetAllocatedRevoke::where('status',\App\Enums\EAssetStatus::REPORT_CANCEL)
                                            ->orWhere('status',\App\Enums\EAssetStatus::CANCELLED)
                                            ->with('asset');

        if($this->searchTerm){
            $listCancel->whereHas('asset',function(Builder $q){
                $q->where('asset.unsign_text','like','%'. strtolower(removeStringUtf8($this->searchTerm)).'%');

            });
        }
        $detailNotice=[];
        if($this->idConfirm){
            $this->emit('loadAssetInfo',$this->idConfirm);
            $detailNotice = AssetAllocatedRevoke::findOrFail($this->idConfirm);
        }
        $data=$listCancel->orderBy('asset_allocated_revoke.id', 'DESC')->paginate(25);
        return view('livewire.admin.asset.lost-cancel-liquidation.list-cancel',['data'=>$data,
            'detailNotice'=>$detailNotice]);
    }
    public function confirmNotice($idConfirm){
        $this->idConfirm = $idConfirm;
    }
    public function confirm(){
        if($this->idConfirm){

            $detailNotice = AssetAllocatedRevoke::findOrFail($this->idConfirm);
            $asset= Asset::findOrFail($detailNotice->asset_id);

            if($detailNotice->status==\App\Enums\EAssetStatus::CANCELLED){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" =>
                    __('notification.common.fail.no_confirmed')]);
            }else{
                $quantity = $asset->amount_used;
                if ($quantity==0|| $quantity < $detailNotice->implementation_quantity) {
                    $this->errorQuantity = __('validation.lt.numeric', [
                        'attribute' => __('data_field_name.asset.asset_quantity'),
                        'value' => __('data_field_name.asset.asset_used_quantity')
                    ]);
                }
                if ($quantity >= $detailNotice->implementation_quantity) {
                    AssetAllocatedRevoke::where('id', $this->idConfirm)->update([
                        'status' => \App\Enums\EAssetStatus::CANCELLED,
                    ]);
                    if ($detailNotice) {
                        $assetDetail = Asset::findOrFail($detailNotice->asset_id);
                        $type_manage = $assetDetail->category->type_manage;
                        if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                            $assetDetail->update([
                                'situation' => \App\Enums\EAssetStatus::CANCELLED,
                                'quantity' => 0
                            ]);
                        } else {
                            if ($assetDetail->quantity == $detailNotice->implementation_quantity) {
                                $assetDetail->update([
                                    'situation' => \App\Enums\EAssetStatus::CANCELLED,
                                    'quantity' => 0
                                ]);
                            } else {
                                $assetDetail->update([
                                    'quantity' => $assetDetail->quantity - $detailNotice->implementation_quantity,
                                    'amount_used' => $assetDetail->amount_used - $detailNotice->implementation_quantity
                                ]);
                            }
                        }
                    }
                    $this->emit('closeConfirm');
                    $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
                        __('notification.common.success.add')]);
                }
            }

        }

    }
    public function export() {
        $today = date("d_m_Y");
        return \Maatwebsite\Excel\Facades\Excel::download(new AssetCancelExport($this->searchTerm), 'list-asset-cancel-' . $today . '.xlsx');
    }
    public function deleteIdAssetCancel($id)
    {
        $this->deleteId = $id;
    }

    public function delete(){
        if($this->deleteId){
            $data_lost =AssetAllocatedRevoke::findOrFail($this->deleteId);
            $check = $data_lost->status;
            $asset =Asset::findOrFail($data_lost->asset_id);
            $type_manage = $asset->category->type_manage;
            if ($check == \App\Enums\EAssetStatus::REPORT_CANCEL){
                if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null){
                    $data_lost->delete();
                    $asset->update([
                        'situation'=>\App\Enums\EAssetSituation::SITUATION_USING
                    ]);
                }else{
                    if ($asset->quantity == $data_lost->implementation_quantity){
                        $data_lost->delete();
                        $asset->update([
                            'situation'=>\App\Enums\EAssetSituation::SITUATION_USING
                        ]);
                    }else{
                        $data_lost->delete();
                    }
                }

                $this->emit('closeConfirm');
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
                    __('notification.common.success.delete')] );
            }else{
                $this->emit('closeConfirm');
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" =>
                    __('notification.common.fail.no_delete_confirm')] );
            }

        }
    }
}
