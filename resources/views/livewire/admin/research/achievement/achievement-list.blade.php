<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <a href="">{{__('research/achievement.breadcrumbs.achievement-management')}}</a>
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">
                    @switch($type)
                        @case(App\Enums\EResearchAchievementType::SCIENCE_ARTICLE)
                        {{__('research/achievement.breadcrumbs.science-article-list')}}
                        @if($data->total() === 0) <br> <span>{{__('research/achievement.title.no-result')}}</span>
                        @else
                            <br>
                            <span>
                                    {{__('research/achievement.title.part-1')}}  {{count($data)}} {{__('research/achievement.title.science-article-part-2')}} {{$data->total()}}
                            </span>
                        @endif
                        @break
                        @case(App\Enums\EResearchAchievementType::INTELLECTUAL_PROPERTY)
                        {{__('research/achievement.breadcrumbs.intellectual-property-list')}}
                        @if($data->total() === 0) <br> <span>{{__('research/achievement.title.no-result')}}</span>
                        @else
                            <br>
                            <span>
                                    {{__('research/achievement.title.part-1')}}  {{count($data)}} {{__('research/achievement.title.intellectual-property-part-2')}} {{$data->total()}}
                            </span>
                        @endif
                        @break
                        @case(App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                        {{__('research/achievement.breadcrumbs.technology-transfer-list')}}
                        @if($data->total() === 0) <br> <span>{{__('research/achievement.title.no-result')}}</span>
                        @else
                            <br>
                            <span>
                                    {{__('research/achievement.title.part-1')}}  {{count($data)}} {{__('research/achievement.title.technology-transfer-part-2')}} {{$data->total()}}
                            </span>
                        @endif
                        @break
                    @endswitch
            </h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group search-expertise">
                                <div class="search-expertise inline-block w-xs-100 w-100">
                                    <input type="text" placeholder="{{__('research/achievement.placeholder.search')}}" name="search" class="size13 form-control w-xs-100 w-100"
                                            wire:model.debounce.1000ms="searchTerm"
                                            id='input_vn_name' autocomplete="off">
                                    <span><img src="/images/Search.svg" alt="search"/></span>
                                </div>
                            </div>
                        </div>
                        <div wire:ignore class="col-md-3 px-8">
                            <select class="form-control size13 form-control-lg" wire:model.lazy="filter_category_field_id">
                                <option value=''>
                                    {{__('research/achievement.research_field')}}
                                </option>
                                @foreach ($listCategoryField as $item)
                                    <option value="{{$item->id}}" >{{ $item->name ?? '' }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 px-8">
                            @if($type != App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                            @include('layouts.partials.input._inputDate', ['input_id' => 'from_date','place_holder' => __('research/achievement.label.from'), 'set_null_when_enter_invalid_date' => true ])
                            @endif
                        </div>
                        <div class="col-md-2 px-8">
                            @if($type != App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                            @include('layouts.partials.input._inputDate', ['input_id' => 'to_date' ,'place_holder' => __('research/achievement.label.to'), 'set_null_when_enter_invalid_date' => true ])
                            @endif
                        </div>
                        <div class="col-md-2 px-8">
                            <div class="form-group float-right">
                                @if($checkCreatePermission)
                                    @switch($type)
                                        @case(App\Enums\EResearchAchievementType::SCIENCE_ARTICLE)
                                            <a class="btn btn-viewmore-news mr0"  href="{{route('admin.research.achievement.science-article.create')}}" style="color:white">
                                                <img src="/images/plus2.svg" alt="plus"> {{__('common.button.add')}}
                                            </a>
                                        @break
                                        @case(App\Enums\EResearchAchievementType::INTELLECTUAL_PROPERTY)
                                            <a class="btn btn-viewmore-news mr0"  href="{{route('admin.research.achievement.intellectual-property.create')}}" style="color:white">
                                                <img src="/images/plus2.svg" alt="plus"> {{__('common.button.add')}}
                                            </a>
                                        @break
                                        @case(App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                                            <a class="btn btn-viewmore-news mr0"  href="{{route('admin.research.achievement.technology-transfer.create')}}" style="color:white">
                                                <img src="/images/plus2.svg" alt="plus"> {{__('common.button.add')}}
                                            </a>
                                        @break
                                    @endswitch

                                @endif
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-general">
                                <thead>
                                <tr class="border-radius">
                                    @switch($type)
                                        @case(App\Enums\EResearchAchievementType::SCIENCE_ARTICLE)
                                        <th scope="col"  class="text-center" >{{__('research/achievement.table_column.number')}}</th>
                                        <th scope="col"  class="text-center" >{{__('research/achievement.table_column.achievement_code')}}</th>
                                        <th scope="col"  class="text-center">{{__('research/achievement.table_column.topic_name')}}</th>
                                        <th scope="col"  class="text-center">{{__('research/achievement.table_column.name_of_science_article')}}</th>
                                        <th scope="col"  class="text-center">{{__('research/achievement.table_column.topic_field_name')}}</th>
                                        <th scope="col"  class="text-center">{{__('research/achievement.table_column.topic_leader_name')}}</th>
                                        <th scope="col"  class="text-center">{{__('research/achievement.table_column.topic_code')}}</th>
                                        <th scope="col"  class="text-center">{{__('research/achievement.table_column.magazine_name')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.publish_date')}}</th>
                                        <th scope="col"  class="text-center">{{__('research/achievement.table_column.document')}}</th>
                                        <th scope="col" class="text-center" >{{__('research/achievement.table_column.action')}}</th>
                                        @break
                                        @case(App\Enums\EResearchAchievementType::INTELLECTUAL_PROPERTY)
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.number')}}</th>
                                        <th scope="col" class="text-center" >{{__('research/achievement.table_column.achievement_code')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.topic_name')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.name_of_intellectual_property')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.topic_field_name')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.topic_leader_name')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.topic_code')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.publish_date_intellectual')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.document')}}</th>
                                        <th scope="col" class="text-center" >{{__('research/achievement.table_column.action')}}</th>
                                        @break
                                        @case(App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.number')}}</th>
                                        <th scope="col" class="text-center" >{{__('research/achievement.table_column.achievement_code')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.topic_name')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.name_of_technology_transfer')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.topic_field_name')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.topic_leader_name')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.topic_code')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.receiver_department')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.document')}}</th>
                                        <th scope="col" class="text-center">{{__('research/achievement.table_column.action')}}</th>
                                        @break
                                    @endswitch
                                </tr>
                                </thead>
                                <div wire:loading class="loader"></div>
                                <tbody>
                                @switch($type)
                                    @case(App\Enums\EResearchAchievementType::SCIENCE_ARTICLE)
                                        @forelse($data as $key => $row)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$row->code}}</td>
                                                <td>{!! boldTextSearch($row->topic->name ?? '', $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                                <td>{{$row->topic->researchCategory->name ?? ''}}</td>
                                                <td>{{$row->topic->leader->fullname ?? '' }}</td>
                                                <td>
                                                    <a href="@if($row->topic) {{route('admin.research.topic.detail', $row->topic->id)}} @else#@endif">
                                                        {!! boldTextSearch($row->topic->code ?? '', $searchTerm) !!}
                                                    </a>
                                                </td>
                                                <td>{{$row->magazine_name }}</td>
                                                <td>{{ reFormatDate($row->publish_date) }}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-sm" wire:click="getListFile({{$row->id}})">
                                                        <img src="/images/fileIcon.png" alt="file" width="50px">
                                                    </button>
                                                </td>
                                                <td class="text-center">
                                                    @if($checkDestroyPermission)
                                                        @include('livewire.common.buttons._delete')
                                                    @endif
                                                    @if($checkEditPermission)
                                                    <button type="button" title="{{__('common.button.edit')}}" class="btn-sm border-0 bg-transparent mr-1">
                                                            <a href="{{route('admin.research.achievement.science-article.edit', ['id' => $row->id])}}">
                                                                <img src="/images/edit.png" alt="edit">
                                                            </a>
                                                    </button>
                                                    @endif
                                                </td>
                                            </tr>
                                            @empty
                                            <tr class="text-center text-danger">
                                                <td colspan="12">{{__('common.message.no_data')}}</td>
                                            </tr>
                                        @endforelse
                                    @break
                                    @case(App\Enums\EResearchAchievementType::INTELLECTUAL_PROPERTY)
                                        @forelse($data as $key => $row)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$row->code}}</td>
                                                <td>{!! boldTextSearch($row->topic->name ?? '', $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                                <td>{{$row->topic->researchCategory->name ?? ''}}</td>
                                                <td>{{$row->topic->leader->fullname  ??'' }}</td>
                                                <td>
                                                    <a href="@if($row->topic) {{route('admin.research.topic.detail', $row->topic->id)}} @else#@endif">
                                                        {!! boldTextSearch($row->topic->code ?? '', $searchTerm) !!}
                                                    </a>
                                                </td>
                                                <td>{{ reFormatDate($row->publish_date) }}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-sm" wire:click="getListFile({{$row->id}})">
                                                        <img src="/images/fileIcon.png" alt="file" width="50px">
                                                    </button>
                                                </td>
                                                <td class="text-center">
                                                    @if($checkDestroyPermission)
                                                        @include('livewire.common.buttons._delete')
                                                    @endif
                                                    @if($checkEditPermission)
                                                    <button type="button" title="{{__('common.button.edit')}}" class="btn-sm border-0 bg-transparent mr-1">
                                                            <a href="{{route('admin.research.achievement.intellectual-property.edit', ['id' => $row->id])}}">
                                                                <img src="/images/edit.png" alt="edit">
                                                            </a>
                                                    </button>
                                                    @endif
                                                </td>
                                            </tr>
                                            @empty
                                            <tr class="text-center text-danger">
                                                <td colspan="12">{{__('common.message.no_data')}}</td>
                                            </tr>
                                        @endforelse
                                    @break
                                    @case(App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                                        @forelse($data as $key => $row)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$row->code}}</td>
                                                <td>{{$row->topic->name ?? ''}}</td>
                                                <td>{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                                <td>{{$row->topic->researchCategory->name ?? ''}}</td>
                                                <td>{{$row->topic->leader->fullname ?? ''}}</td>
                                                <td>
                                                    <a href="@if($row->topic) {{route('admin.research.topic.detail', $row->topic->id)}} @else#@endif">
                                                        {!! boldTextSearch($row->topic->code ?? '', $searchTerm) !!}
                                                    </a>
                                                </td>
                                                <td>{{$row->receiver_department }}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-sm" wire:click="getListFile({{$row->id}})">
                                                        <img src="/images/fileIcon.png" alt="file" width="50px">
                                                    </button>
                                                </td>
                                                <td class="text-center">
                                                    @if($checkDestroyPermission)
                                                        @include('livewire.common.buttons._delete')
                                                    @endif
                                                    @if($checkEditPermission)
                                                    <button type="button" title="{{__('common.button.edit')}}" class="btn-sm border-0 bg-transparent mr-1">
                                                            <a href="{{route('admin.research.achievement.technology-transfer.edit', ['id' => $row->id])}}">
                                                                <img src="/images/edit.png" alt="edit">
                                                            </a>
                                                    </button>
                                                    @endif
                                                </td>
                                            </tr>
                                            @empty
                                            <tr class="text-center text-danger">
                                                <td colspan="12">{{__('common.message.no_data')}}</td>
                                            </tr>
                                        @endforelse
                                    @break
                                @endswitch
                                </tbody>
                            </table>
                        </div>
                        {{$data->links()}}

                        @include('livewire.common.modal._modalDelete')
                        <div wire:ignore.self class="modal fade" id="downloadFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;right:15px;top:15px">
                                            <span aria-hidden="true close-btn">×</span>
                                        </button>
                                        <h5 class="modal-title text-uppercase">
                                            {{__('research/achievement.table_column.document')}}
                                        </h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row mt-3">
                                                @forelse ($listFile as $key => $val)
                                                <div class="form-group col-md-12" >
                                                    <div class="attached-files" wire:click="download('{{$val->url}}', '{{$val->file_name}}')" style="cursor: pointer">
                                                        <img src="/images/File.svg" alt="file">
                                                        <div class="content-file" >
                                                            <p>{{ $val->file_name }}</p>
                                                            <p class="kb">{{ $val->size_file }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @empty
                                                    <td colspan="12">{{__('common.message.no_data')}}</td>
                                                @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('openDownloadFileModal', function() {
        $('#downloadFileModal').modal('show');
    })
</script>
