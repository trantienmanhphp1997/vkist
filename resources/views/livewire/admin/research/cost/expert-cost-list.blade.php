<div class="col-md-10 col-xl-11 box-detail box-user list-topic-fee">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="{{ route('admin.research.topic-fee.index') }}">{{ __('data_field_name.research_cost.list_cost') }}</a> \ <span>{{ __('data_field_name.research_cost.text_create_expert_cost') }}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{ __('data_field_name.research_cost.text_cost_2') }}:</h3>
            <h5>{{ __('data_field_name.research_cost.expert_cost') }}</h5>

            <div class="information">
                <div class="inner-tab pt0">
                    <div class="col-md-12 ">
                        @if ($topicFeeStatus == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                            @if($checkCreatePermission)
                                <div class="form-group float-right">
                                    <a wire:click="resetInputFields" data-toggle="modal" data-target="#createMaterialCostDetail" class="btn btn-viewmore-news mr0 ">
                                        <img src="/images/plus2.svg" alt="plus">{{__('common.button.create')}}
                                    </a>
                                </div>
                            @endif
                        @endif
                    </div>
                    <table class="table">
                        <thead>
                            <tr class="border-radius text-center">
                                <th scope="col" class="border-radius-left">
                                    {{ __('data_field_name.research_cost.No') }}
                                </th>
                                <th scope="col" class="text-center">
                                    {{ __('data_field_name.research_cost.expert_name') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.nationality') }}
                                </th>
                                <th scope="col" class="text-center">
                                    {{ __('data_field_name.research_cost.organization') }}
                                </th>
                                <th scope="col" class="text-center">
                                    {{ __('data_field_name.research_cost.content') }}
                                </th>
                                <th scope="col" class="text-center">
                                    {{ __('data_field_name.research_cost.working_time') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.salary') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.total') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.from_state_sources') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.from_state_other') }}
                                </th>
                                <th scope="col" class="border-radius-right">
                                    {{ __('data_field_name.common_field.action') }}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $row)
                                <tr>
                                    <td class="text-center">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                    <td class="text-center">{{ $row->expert_name}}</td>
                                    <td class="text-center">{{ $row->nationality}}</td>
                                    <td class="text-center">{{ $row->organization}}</td>
                                    <td class="text-center">{{ $row->content}}</td>
                                    <td class="text-center">{{ $row->working_time}}</td>
                                    <td class="text-center">{{ $row->salary}}</td>
                                    <td class="text-center">{{ numberFormat($row->total) }}</td>
                                    <td class="text-center">{{ numberFormat($row->state_capital) }}</td>
                                    <td class="text-center">{{ numberFormat($row->other_capital) }}</td>

                                    <td class="text-center">
                                        @if ($topicFeeStatus == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                                            @include('livewire.common.buttons._edit')
                                            @include('livewire.common.buttons._delete')
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td class="text-center">{{ __('data_field_name.research_cost.sum') }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-center">{{ numberFormat($dataTotal['total']) }}</td>
                                <td class="text-center">{{ numberFormat($dataTotal['state_capital']) }}</td>
                                <td class="text-center">{{ numberFormat($dataTotal['other_capital']) }}</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')
                    <div class="col-md-12 text-center mt-5">
                        <a href="{{route('admin.research.topic-fee.edit', $topicFeeId)}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Create -->
    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="createMaterialCostDetail" tabindex="-1" aria-labelledby="createTopicFee" aria-hidden="true">
            <div class="modal-dialog  modal-appraisal">
                <div class="modal-content personnel">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{__('data_field_name.research_cost.create_cost')}}</h4>

                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.expert_name')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="expert_name">
                            @error('expert_name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.nationality')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="nationality">
                            @error('nationality') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.organization')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="organization">
                            @error('organization') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.content')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="content">
                            @error('content') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.working_time')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.lazy="working_time">
                            @error('working_time') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.salary')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.lazy="salary">
                            @error('salary') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.state_capital')}}</label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="state_capital">
                            @error('state_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.other_capital')}}</label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="other_capital">
                            @error('other_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="attached-center">
                            <button type="button" id="close-modal-create-material" class="btn btn-cancel close-btn mt-0" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="store()" class="btn btn-save close-modal">{{__('common.button.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Create -->

    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="target" tabindex="-1" aria-labelledby="createTopicFee" aria-hidden="true">
            <div class="modal-dialog  modal-appraisal">
                <div class="modal-content personnel">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{__('data_field_name.research_cost.create_cost')}}</h4>

                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.expert_name')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="expert_name">
                            @error('expert_name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.nationality')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="nationality">
                            @error('nationality') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.organization')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="organization">
                            @error('organization') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.content')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="content">
                            @error('content') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.working_time')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.lazy="working_time">
                            @error('working_time') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.salary')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.lazy="salary">
                            @error('salary') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.state_capital')}}</label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="state_capital">
                            @error('state_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.research_cost.other_capital')}}</label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="other_capital">
                            @error('other_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="attached-center">
                            <button type="button" id="close-modal-edit-material" class="btn btn-cancel mt-0" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="update()" class="btn btn-save close-modal">{{__('common.button.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>




    <script>
        $("document").ready(function() {
            $('.buttonCreate').click(function() {
                $('.error').remove();
                $('.form-input').val('');
            });

            $('#createMaterialCostDetail').on('hidden.bs.modal', function () {
                $('.error').remove();
                window.livewire.emit('resetData');
            });

            window.livewire.on('close-modal-create-material', () => {
                document.getElementById('close-modal-create-material').click()
            });
            window.livewire.on('close-modal-edit-material', () => {
                document.getElementById('close-modal-edit-material').click()
            });

        });
    </script>
</div>
