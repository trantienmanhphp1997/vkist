<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="breadcrumbs">
        @if ($isEdit == false)
            <div class="breadcrumbs"><a href="{{ route('admin.reality_expense.index') }}">{{ __('data_field_name.reality_expense.research_expense_control') }}</a> \ <span>{{ __('data_field_name.reality_expense.create_expense') }}</span></div>
        @else
            <div class="breadcrumbs"><a href="{{ route('admin.reality_expense.index') }}">{{ __('data_field_name.reality_expense.research_expense_control') }}</a> \ <span>{{ __('data_field_name.reality_expense.edit_expense') }}</span></div>
        @endif
    </div>
    <div class="creat-box bg-content border-20">
        <div class="detail-task pt-44">
            @if ($isEdit == false)
                <h4 class="text-center">{{ __('data_field_name.reality_expense.create_expense') }}</h4>
            @else
                <h4 class="text-center">{{ __('data_field_name.reality_expense.edit_expense') }}</h4>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-asset pt-24">{{ __('data_field_name.reality_expense.topic_infomation') }}</h3>
                </div>
            </div>
            <div class="box-calendar col-md-12 bg-white">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.topic_code') }} <span class="text-danger">*</span></label>
                            @livewire('component.search-combobox', ['model_name' => App\Models\Topic::class, 'filters' => ['code'], 'value_render_on_combobox' => 'code','targetId' => $topic_id ?? null, 'more_conditions' => [['topic_fee.status', '=', \App\Enums\ETopicFee::STATUS_APPROVAL]], 'relations' => [['topic_fee','topic_id']],])
                            @error('topic_id') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.code_project') }}</label>
                            <input type="text" disabled class="form-control " wire:model.defer="topic_code">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.topic_name') }}</label>
                            <input type="text" disabled class="form-control " wire:model.defer="topic_name">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <h3 class="title-asset pt-24">{{ __('data_field_name.reality_expense.approved_budget') }}</h3>
                </div>
            </div>
            <div class="box-calendar col-md-12 bg-white">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.expenses_type') }} <span class="text-danger">*</span></label>
                            <select name="" id="" wire:model.lazy="type_expense" class="form-control ">
                                <option value="">{{ __('data_field_name.reality_expense.select_expense') }}</option>
                                @foreach ($topicFeeList as $value)
                                    @if ($value->type == \App\Enums\ETopicFeeDetail::TYPE_PEOPLE)
                                        <option value="{{ $value->id }}">{{ __('data_field_name.research_cost.type_people') }}</option>
                                    @elseif($value->type == \App\Enums\ETopicFeeDetail::TYPE_MATERIAL)
                                        <option value="{{ $value->id }}">{{ __('data_field_name.research_cost.type_material') }}</option>
                                    @elseif($value->type == \App\Enums\ETopicFeeDetail::TYPE_EQUIPMENT)
                                        <option value="{{ $value->id }}">{{ __('data_field_name.research_cost.type_equipment') }}</option>
                                    @elseif($value->type == \App\Enums\ETopicFeeDetail::TYPE_SHOP_REPAIR)
                                        <option value="{{ $value->id }}">{{ __('data_field_name.research_cost.type_shop_repair') }}</option>
                                    @elseif($value->type == \App\Enums\ETopicFeeDetail::TYPE_OTHER)
                                        <option value="{{ $value->id }}">{{ __('data_field_name.research_cost.type_other') }}</option>
                                    @elseif($value->type == \App\Enums\ETopicFeeDetail::TYPE_EXPERT)
                                        <option value="{{ $value->id }}">{{ __('data_field_name.research_cost.type_expert') }}</option>
                                    @else
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('type_expense') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.payform') }} <span class="text-danger">*</span></label>
                            {{ Form::select('source', ['' => __('data_field_name.reality_expense.select_payform')] + $payform, null, ['class' => 'form-control ', 'wire:model.lazy' => 'payment']) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.souce_capital') }} <span class="text-danger">*</span></label>
                            {{ Form::select('source', ['' => __('data_field_name.reality_expense.select_capital')] + $capital, null, ['class' => 'form-control ', 'wire:model.lazy' => 'source_capital']) }}
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.typepay') }} <span class="text-danger">*</span></label>
                            {{ Form::select('source', ['' => __('data_field_name.reality_expense.select_typepay')] + $typepay, null, ['class' => 'form-control ', 'wire:model.lazy' => 'type_cost']) }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-0">
                            <label>{{ __('data_field_name.reality_expense.browse') }} (VNĐ)</label>
                            <input type="text" wire:model.lazy="approval_costs" class="form-control format_number" readonly>
                            @error('approval_costs') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-0">
                            <label>{{ __('data_field_name.reality_expense.total_money_cost') }} (VNĐ) <span class="text-danger">*</span></label>
                            <input type="text" wire:model.lazy="total_cost" class="form-control format_number ">
                            @error('total_cost') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group  mb-0">
                            <label>{{ __('data_field_name.reality_expense.money_remain') }}(VNĐ)</label>
                            <input type="text" wire:model.lazy="money_remain" class="form-control bg-content format_number" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <h3 class="title-asset pt-24">{{ __('data_field_name.reality_expense.expense_infomation') }}</h3>
                </div>
            </div>
            <div class="box-calendar col-md-12 bg-white">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.requester') }}</label>
                            <input type="text" disabled value="{{ Auth::user()->name }}" class="form-control  bg-content">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.expense_task') }} <span class="text-danger">*</span></label>
                            <select name="" wire:model.lazy="task_id_save" class="form-control  ">
                                <option value="">--{{ __('data_field_name.reality_expense.select_job') }}--</option>
                                @foreach ($expense_task as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                            @error('task_id_save') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{ __('data_field_name.reality_expense.note') }} <span class="text-danger">*</span></label>
                            <select name="" wire:model.lazy="note_id_save" class="form-control  ">
                                <option value="">--{{ __('data_field_name.reality_expense.select_content') }}--</option>
                                @if ($type == \App\Enums\ETopicFeeDetail::TYPE_EXPERT)
                                    @foreach ($note as $value)
                                        <option value="{{ $value->id }}">{{ $value->content }}</option>
                                    @endforeach
                                @elseif($type == \App\Enums\ETopicFeeDetail::TYPE_PEOPLE)
                                    @foreach ($note as $value)
                                        <option value="{{ $value->id }}">{{ $value->task->name ?? '' }}</option>
                                    @endforeach
                                @else
                                    @foreach ($note as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('note_id_save') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>{{ __('data_field_name.reality_expense.money_must_cost') }} (VNĐ) <span class="text-danger">*</span></label>
                            <input type="text" class="form-control format_number " wire:model.lazy="money_cost">
                            @error('money_cost') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group mb-0">
                            <label>{{ __('data_field_name.reality_expense.disbursement_disduration') }}</label>
                            <input type="text" disabled wire:model.defer="disbursement_date" class="form-control bg-content">
                            @error('disbursement_date') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>{{ __('data_field_name.reality_expense.status_dibursed') }} <span class="text-danger">*</span></label>
                            {{ Form::select('source', ['' => __('data_field_name.reality_expense.select_status')] + $status_disbursed, null, ['class' => 'form-control', 'wire:model.lazy' => 'status']) }}
                            @error('status') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="text-center mt-42">
                    <a href="{{ route('admin.reality_expense.index') }}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{ __('common.button.close') }}</a>
                    <button type="button" wire:click.prevent="store()" class="btn btn-main bg-primary text-9 size16 px-48 py-14" @error('total_cost') disabled @enderror>{{ __('common.button.save') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
