<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <div  class="breadcrumbs"><a href="{{route('admin.asset.index')}}">{{__('menu_management.menu_name.asset-management')}}</a> \ <span>{{__('menu_management.menu_name.asset-provider')}}</span></div>
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">
                {{__('asset/provider.breadcrumbs.asset-provider-list')}}
            </h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group search-expertise">
                                <div class="search-expertise">
                                    <input type="text" placeholder="{{__('asset/provider.placeholder.search')}}" name="search" class="form-control"
                                           wire:model.debounce.1000ms="searchTerm"
                                           id='input_vn_name' autocomplete="off">
                                    <span><img src="/images/Search.svg" alt="search"/></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group float-right">
                                @if($checkCreatePermission)
                                {{-- <button class="btn btn-viewmore-news mr0" data-toggle="modal" data-target="#createOrEditProviderModal" wire:click="resetProviderInfo">
                                    <img src="/images/plus2.svg" alt="plus"> {{__('common.button.add')}}
                                </button> --}}
                                <button type="button" title="{{__('common.button.create')}}" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#createOrEditProviderModal" wire:click="resetProviderInfo">
                                        <img src="/images/filteradd.png" alt="filteradd">
                                </button>
                                @endif
                                @if($checkDownloadPermission)
                                    <button type="button" id='btnOpenExportModal' title="{{__('common.button.export_file')}}" style="border-radius: 11px; border:none;"data-target="#export-modal"
                                            data-toggle="modal">
                                        <img src="/images/filterdown.svg" alt="filterdown">
                                    </button>
                                @endif
                                {{-- @if($checkDestroyPermission)
                                <button id="deleteBtn" class="btn bg-content py-10"  data-toggle="modal" data-target="#deleteModal" wire:click="resetDeleteId" style="border-radius: 11px; border:none;"> <img src="/images/delete2.svg" alt="delete" class="pr-0">
                                </button>
                                @endif --}}
                            </div>
                        </div>

                        <table class="table">

                            <thead>
                            <tr class="border-radius">
                                <th class="text-center" scope="col" style="width:10%">{{__('asset/provider.table_column.code')}}</th>
                                <th class="text-center" scope="col" style="width:20%">{{__('asset/provider.table_column.name')}}</th>
                                <th class="text-center" scope="col" style="width:25%">{{__('asset/provider.table_column.address')}}</th>
                                <th class="text-center" scope="col" style="width:20%">{{__('asset/provider.table_column.email')}}</th>
                                <th class="text-center" scope="col" style="width:15%">{{__('asset/provider.table_column.phone_number')}}</th>
                                <th class="text-center" scope="col" style="width:10%">{{__('asset/provider.table_column.action')}}</th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>

                            <tbody>
                            @forelse($data as $row)
                                <tr>
                                    <td class="text-center" >{!! boldTextSearchV2($row->code,$searchTerm) !!}</td>
                                    <td class="text-center" >{!! boldTextSearchV2($row->name,$searchTerm) !!}</td>
                                    <td class="text-center" >{{$row->address }}</td>
                                    <td class="text-center" >{{$row->email }}</td>
                                    <td class="text-center" >{{$row->phone_number}}</td>
                                    <td class="text-center">
                                        @include('livewire.common.buttons._delete')
                                        @if($checkEditPermission)
                                        <button type="button" data-toggle="modal" data-target="#createOrEditProviderModal" title="{{__('common.button.edit')}}"
                                                wire:click="getProviderInfoForUpdateModal({{ $row->id }})" class="btn-sm border-0 bg-transparent mr-1">
                                            <img src="/images/edit.png" alt="edit">
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                    <tr class="text-center text-danger"><td colspan="12">{{__('common.message.no_data')}}</td></tr>
                            @endforelse
                            </tbody>
                        </table>

                        {{$data->links()}}
                        <div class="col-md-12" scope="col colUser">
                            {{__('data_field_name.asset.number_record')}}&nbsp<strong>{{ count($data) }}</strong>
                        </div>
                        @include('livewire.common.modal._modalDelete')
                    </div>
                </div>
            </div>
            @include('livewire.common.modal._modalConfirmExport')

            <!-- Modal Edit -->
            <form wire:submit.prevent="submit">
                <div wire:ignore.self class="modal fade modal-fix-1" id="createOrEditProviderModal" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                    <div class="modal-header">

                        <h5 class="modal-title" id="exampleModalLabel">
                            @if ($isEdit == false)  {{__('asset/provider.title.create')}}
                            @else  {{__('asset/provider.title.edit')}}
                            @endif
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row bd-border">
                                    <div class="col-md-12">
                                        <h4>
                                            {{__('asset/provider.form.general_info')}}
                                        </h4>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.name')}}<span class="text-danger">&nbsp*</span></label>
                                                    <input type="text" class="form-control" placeholder="" name="name" wire:model.defer="name">
                                                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.code')}}<span class="text-danger">&nbsp*</span></label>
                                                    <input type="text" class="form-control" placeholder="" name="code" wire:model.defer="code" @if ($isEdit == true) readonly @endif>
                                                    @error('code') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.address')}}</label>
                                                    <input type="text" class="form-control" placeholder="" name="address" wire:model.defer="address">
                                                    @error('address') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.tax_code')}}</label>
                                                    <input type="text" class="form-control" placeholder="" name="tax_code" wire:model.defer="tax_code">
                                                    @error('tax_code') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                                <div class="form-group">
                                                        <label>{{__('asset/provider.label.website')}}</label>
                                                        <input type="text" class="form-control" placeholder="" name="website" wire:model.defer="website">
                                                        @error('website') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.bank_account_number')}}</label>
                                                    <input type="text" class="form-control" placeholder="" name="bank_account_number" wire:model.defer="bank_account_number">
                                                    @error('bank_account_number') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.phone_number')}}</label>
                                                    <input type="text" class="form-control" placeholder="" name="phone_number" wire:model.defer="phone_number">
                                                    @error('phone_number') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.email')}}</label>
                                                    <input type="text" class="form-control" placeholder="" name="email" wire:model.defer="email">
                                                    @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.bank_name_id')}}</label>
                                                    <select  wire:model.defer="bank_name_id" class="form-control">
                                                            @foreach($list_bank_info as $item)
                                                                <option value="{{$item->id}}">{{$item->v_value}}</option>
                                                            @endforeach
                                                    </select>
                                                    @error('bank_name_id')
                                                        <div class="text-danger mt-1">{{$message}}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{__('asset/provider.label.note')}}</label>
                                                    <textarea type="text" name="note" class="form-control" placeholder="" cols="30" rows="3" wire:model.defer="note"> </textarea>
                                                    @error('note')
                                                    <div class="text-danger mt-1">{{$message}}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer ">
                        <div class="group-btn2">
                            <button type="button" id="close-modal-create-or-edit-provider" class="btn btn-cancel"
                            data-dismiss="modal">{{__('common.button.cancel')}}</button>
                            <button type="button" wire:click.prevent="store()" class="btn btn-save close-modal">
                                {{__('common.button.save')}}
                            </button>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </form>
            <!-- End Modal Edit -->
        </div>
    </div>
</div>

<script>
        $("document").ready(function() {
        window.livewire.on('close-modal-create-or-edit-provider', () => {
            document.getElementById('close-modal-create-or-edit-provider').click()
        });
      })
      </script>


