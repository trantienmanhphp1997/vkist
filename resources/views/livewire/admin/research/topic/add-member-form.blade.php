<div class="modal-content personnel">
    <div class="box-user">
        <h4 class="text-center">
           {{ __('data_field_name.topic.add_member') }}
           <button class="btn float-right" data-dismiss="modal"><em class="fa fa-times"></em></button>
        </h4>
        <div class="form-group">
            <label>{{ __('data_field_name.topic.research_way') }} <span class="text-danger">(*)</span></label>
            {{ Form::select('research_id', ['' => __('common.select-box.choose')] + $researchs, null, ['class' => 'form-control', 'wire:model.defer' => 'member.research_id', 'wire:ignore' => true]) }}
            @error('member.research_id') <div class="text-danger"> {{ $message }} </div> @enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.topic.member_name') }} <span class="text-danger">(*)</span></label>
            @livewire('component.advanced-select', [
                'name' => 'user-info',
                'modelName' => App\Models\UserInf::class,
                'searchBy' => ['fullname'],
                'columns' => ['fullname', 'academic_level_id'],
                'relations' => ['academicLevel'],
                'selectedIds' => [$selectedUserInfo['id'] ?? null]
            ], key(Str::random(5)))
            <div id="user-info-search-box"></div>
            @error('member.user_info_id') <div class="text-danger"> {{ $message }} </div> @enderror
        </div>
        <div class="row">
            <div class="col-sm form-group">
                <label>{{ __('data_field_name.topic.qualification') }} <span class="text-danger">(*)</span></label>
                <input type="text" class="form-control" readonly value="{{ $selectedUserInfo['academic_level']['v_value'] ?? ''}}">
            </div>
            <div class="col-sm form-group">
                <label>{{ __('data_field_name.topic.role') }} <span class="text-danger">(*)</span></label>
                {{ Form::select('role_id', ['' => __('common.select-box.choose')] + $roles, null, ['class' => 'form-control', 'wire:model.defer' => 'member.role_id', 'wire:ignore' => true]) }}
                @error('member.role_id') <div class="text-danger"> {{ $message }} </div> @enderror
            </div>
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.topic.mission_note') }} <span class="text-danger">(*)</span></label>
            <textarea class="form-control" rows="4" wire:model.defer="member.mission_note"></textarea>
            @error('member.mission_note') <div class="text-danger"> {{ $message }} </div> @enderror
        </div>
    </div>
    <div class="group-btn2 text-center ">
        <button type="button" wire:click="resetAll" id="close-add-member-form-btn" data-dismiss="modal" class="btn btn-cancel">{{ __('common.button.cancel') }}</button>
        <button type="button" wire:click="save" class="btn btn-save">{{ __('common.button.save') }}</button>
    </div>

    <script>
        window.addEventListener('close-add-member-form-event', () => {
            document.getElementById('close-add-member-form-btn').click();
        });
    </script>
</div>
