<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreateUserSuccessfully extends Mailable
{
    use Queueable, SerializesModels;

    private string $name;
    private string $username;
    private string $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $name, string $username, string $password)
    {
        $this->name = $name;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.create-user-successfully', [
            'name' => $this->name,
            'username' => $this->username,
            'password' => $this->password,
        ]);
    }
}
