<?php
return [
 
    "car" => [
        "name" => [
            "required" => "Vehicle name is required information",
            "maxlength" => "Vehicle name is too long"
        ],
        "model" => [
            "required" => "Model name is required information",
            "maxlength" => "Model name is too long"
        ],
         "brand" => [
             "required" => "Model name is required information",
             "maxlength" => "Model name is too long"
         ],
         "color" => [
                       "required" => "Model name is required information",
                           "maxlength" => "Model name is too long"
                       ],
        "number_of_seats" => [
            "required" => "Seat number is required information",
            "maxlength" => "The number of seats is too long",
            "numberic" => "Invalid number of seats"
        ],
        "license_plate" => [
            "required" => "The license plate is required information",
            "maxlength" => "License plate is too long",
            "unique" => "The license plate already exists",
            "not_exist" => "The license plate does not exist",
            "has_car" => "Driver has a car",
            "has_driver" => "The vehicle has a driver in charge"
        ],
    ],
    "stage" => [
        "name" => [
            "required" => "Set name is required information",
            "max" => "The set name is too long"
        ],
        "address" => [
            "required" => "Shoot location is required",
            "max" => "Shoot location is too long"
        ],
        "code" => [
            "required" => "Shoot code is required information",
            "max" => "The set code is too long",
            "unique" => "The studio code already exists"
        ],
    ],
 
    "contract" => [
        "title" => [
            "required" => "Contract title is required information",
            "maxlength" => "Contract title is too long"
        ],
        "code" => [
            "required" => "Contract code is required information",
            "maxlength" => "Contract code too long",
            "unique" => "Code already exists"
        ],
        "department_id" => [
            "required" => "Department is required information",
        ],
        "statement_id" => [
            "required" => "The report is required information",
        ],
    ],
    "department" => [
        "name" => [
            "required" => "Department name is required information",
            "maxlength" => "Department name is too long"
        ],
    ],
    "device_category" => [
        "name" => [
            "required" => "Device category name is required information",
            "maxlength" => "Device catalog name is too long",
            "unique" => "Category name already exists"
        ],
        "display_code" => [
            "required" => "Device catalog code is required information",
            "maxlength" => "Device catalog code too long",
            "unique" => "Category code already exists"
        ],
    ],
    "device" => [
        "name" => [
            "required" => "Device name is required information",
            "maxlength" => "Device name is too long"
        ],
        "device_category_id" => [
            "required" => "Device directory is required information",
        ],
        "category" => [
            "required" => "Device type is required"
        ],
        "serial" => [
            "required" => "Device ID is required",
            "unique" => "Device ID already exists"
        ]
    ],
    "manager" => [
        "username" => [
            "required" => "Username is required information",
            "unique" => "Username already exists"
        ],
        "phone_number" => [
            "required" => "Phone number is required",
            "unique" => "Phone number already exists"
        ],
        "full_name" => [
            "required" => "Manager name is required information",
        ],
 
    ],
    "member" => [
        "full_name" => [
            "required" => "Employee name is required information",
            "maxlength" => "Employee name is too long"
        ],
        "phone_number" => [
            "required" => "Phone number is required",
            "unique" => "Phone number already in use in the room :department."
        ],
 
    ],
    "outsource" => [
        "full_name" => [
            "required" => "Device category name is required information",
            "maxlength" => "Device catalog name is too long"
        ],
        "phone_number" => [
            "required" => "Phone number is required",
            "unique" => "Phone number already exists"
        ],
 
    ],
    "outsource_type" => [
        "name" => [
            "required" => "Outsourced type name is required",
            "maxlength" => "Outsourced type name is too long"
        ],
        "code" => [
            "unique" => "Code already exists",
            "required" => "Code is required information",
        ]
 
    ],
    "plan_info" => [
        "title" => [
            "required" => "Plan title is required information",
            "maxlength" => "The plan title is too long"
        ],
        "code" => [
            "required" => "Plan code is required information",
            "maxlength" => "Plan code is too long",
            "unique" => "Code already exists"
        ],
        "department_id" => [
            "required" => "Department is required information",
        ],
        "year" => [
            "required" => "Year is required",
        ],
    ],
    "statement" => [
        "title" => [
            "required" => "Submission title is required information",
            "maxlength" => "The title of the sheet is too long"
        ],
        "code" => [
            "required" => "The submission code is required information",
            "maxlength" => "The application code is too long",
            "unique" => "Code already exists"
        ],
        "department_id" => [
            "required" => "Department is required information",
        ],
    ],
    "record_plan" => [
        "name" => [
            "required" => "Production schedule name is required information",
            "max" => "The plan name is too long",
            "unique" => "Production schedule name is already in use"
        ],
        "address" => [
            "required" => "Production address is required",
        ],
        "start_time" => [
            "required" => "Production schedule start time is required",
        ],
        "end_time" => [
            "required" => "Production end time is required",
        ],
        "start_date" => [
             "required" => "Production schedule start date is required",
        ],
        "end_date" => [
              "required" => "Production end date is required",
        ],
        "department_code" => [
            "required" => "Department of production schedule is required information"
        ],
        "plan_id" => [
            "required" => "The production schedule is required information"
        ],
        "recurring" => [
            "required" => "You have not selected a recurring calendar date"
        ]
 
    ],
    "email" => [
        "format" => "Invalid email format"
    ],
    "file" => [
        "required" => "You have not selected any files",
        "mimes" => "Invalid file format"
    ],
    'ideal' => [
        'required' => ':name required',
        'max' => ':name max :max characters',
        'regex' => ':name has an invalid value'
    ],
    'role' => [
        "name" => [
            "unique" => "Role name already exists"
        ],
        "code" => [
            "unique" => "Role code already exists"
        ],
    ],
    'import-time-sheets' => [
        'required' => 'Please select timesheet data',
        'file-excel' => 'The file is not in the correct format',
        'file-excel2' => 'The file content is not in the correct format',
        'exists' => "This month's timekeeping already exists",
        'user-not-exists' => 'This employee does not exist. Line number ',
        'user-required' => 'The name is obligatory. Line number ',
        'reason' => 'Reason',
        'msg' => [
            'success' => 'Successfully added :value lines',
            'error' => 'Add failure :value lines'
        ]
    ]
];