<div class="col-md-10 col-xl-11 box-detail box-user list-topic-fee">

    <div class="breadcrumbs"><a
            href="{{ route('admin.research.topic-fee.index') }}">{{ __('data_field_name.research_cost.budget_list') }}
        </a> \ <span>{{ __('data_field_name.research_cost.list_cost') }}</span></div>
    <br>
    <h4>{{ __('data_field_name.research_cost.list_cost') }}</h4>
    <div class="table-responsive pb-24">
        <h3 class="title-asset pt-24">{{ __('data_field_name.research_cost.estimation') }}</h3>
        <table class="table table-general table-budget table-topic-fee">
            <thead></thead>
            <tbody>
                <tr>
                    <td class="text-3 size14 border-right">{{ __('data_field_name.topic.name') }}</td>
                    <td class="size14">{!! $dataTopicFee->topic->name ?? '' !!}</td>
                </tr>
                <tr>
                    <td class="text-3 size14 border-right">
                        {{ __('data_field_name.research_cost.topic_manager') }}</td>
                    <td class="size14">{!! $dataTopicFee->topic->leader->fullname ?? '' !!}</td>
                </tr>
                <tr>
                    <td class="text-3 size14 border-right">
                        {{ __('data_field_name.research_cost.execution_time') }}</td>
                    <td class="size14">{!! reFormatDate($dataTopicFee->topic->start_date, 'd/m/Y') ?? '' !!} -
                        {!! reFormatDate($dataTopicFee->topic->end_date, 'd/m/Y') ?? '' !!}
                    </td>
                </tr>
                <tr>
                    <td class="text-3 size14 border-right">
                        {{ __('data_field_name.research_cost.name_topic_fee') }}</td>
                    <td class="size14">{!! $dataTopicFee->name ?? '' !!}</td>
                </tr>
                <tr>
                    <td class="text-3 size14 border-right">
                        {{ __('data_field_name.research_cost.estimate_total_cost') }}</td>
                    <td class="size14">{!! numberFormat($dataTotal['total_capital']) ?? '' !!} </td>
                </tr>
                <tr>
                    <td class="text-3 pl-52  border-right">
                        {{ __('data_field_name.research_cost.state_sources') }}</td>
                    <td><em>{!! numberFormat($dataTotal['state_capital']) ?? '' !!}</em></td>
                </tr>
                <tr>
                    <td class="text-3 pl-52 border-right">
                        {{ __('data_field_name.research_cost.state_other') }}
                    </td>
                    <td><em>{!! numberFormat($dataTotal['other_capital']) ?? '' !!}</em></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h3 class="title-asset pt-24">{{ __('data_field_name.research_cost.list_cost') }}</h3>
        </div>
        <div class="col-md-4 text-right">
            @if ($dataTopicFee->status == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                @if ($checkCreatePermission)
                    <button type="button" class="btn btn-main bg-7 text-5 size14 py-12 px-15" data-toggle="modal"
                        data-target="#createTopicFeeDetail" wire:click="resetInputFields()">
                        <img src="/images/Plus3.svg" alt="plus">{{ __('data_field_name.research_cost.add_cost') }}
                    </button>
                @endif
            @endif
        </div>
    </div>
    <div class="table-responsive list-topic-fee">
        <table class="table table-general w1600 table-th-8" style="min-width:1500px">
            <thead>
                <tr class="border-radius">
                    <th style="width: 10%" rowspan="2" class="" scope=" col" class="border-radius-left text-center">
                        {{ __('data_field_name.common_field.stt') }}</th>
                    <th style="width: 10%" rowspan="2" class="text-center" scope="col">
                        {{ __('data_field_name.research_cost.content_expenses') }}</th>
                    <th style="width: 10%" rowspan="2" class="text-center" scope="col">
                        {{ __('data_field_name.research_cost.total_capital') }}</th>
                    <th style="width: 10%" rowspan="2" class="text-center" scope="col">
                        {{ __('data_field_name.research_cost.ratio') }}</th>
                    <th rowspan="2" class="text-center" style="width: 10%">
                        {{ __('data_field_name.research_cost.prescribed_expenses') }}</th>
                    @for ($i = 0; $i < $passYears; $i++)
                        <th colspan="2" class="text-center">{{ __('data_field_name.research_cost.year')}}
                            {{ $i + 1 }}</th>
                    @endfor
                    <th rowspan="2" class="text-center" scope="col"
                        class="border-radius-right min-width text-center">
                        {{ __('data_field_name.common_field.action') }}</th>
                </tr>
                <tr>
                    @for ($i = 0; $i < $passYears; $i++)
                        <th class="text-center">{{ __('data_field_name.research_cost.expense') }}</th>
                        <th class="text-center" >{{ __('data_field_name.research_cost.prescribed_expenses') }}</th>
                    @endfor
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $row)
                    <tr>
                        <td class="text-center">
                            {{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                        <td class="text-center">
                            @if ($row->type == \App\Enums\ETopicFeeDetail::TYPE_PEOPLE)
                                <a href="{{ route('admin.research.topic-fee.labor', $row->id) }}">
                                    {{ __('data_field_name.research_cost.type_people') }}
                                </a>
                            @elseif ($row->type == \App\Enums\ETopicFeeDetail::TYPE_MATERIAL)
                                <a href="{{ route('admin.research.topic-fee.material', $row->id) }}">
                                    {{ __('data_field_name.research_cost.type_material') }}
                                </a>
                            @elseif ($row->type == \App\Enums\ETopicFeeDetail::TYPE_EQUIPMENT)
                                <a href="{{ route('admin.research.topic-fee.equipment', $row->id) }}">
                                    {{ __('data_field_name.research_cost.type_equipment') }}
                                </a>
                            @elseif ($row->type == \App\Enums\ETopicFeeDetail::TYPE_SHOP_REPAIR)
                                <a href="{{ route('admin.research.topic-fee.asset', $row->id) }}">
                                    {{ __('data_field_name.research_cost.type_shop_repair') }}
                                </a>
                            @elseif ($row->type == \App\Enums\ETopicFeeDetail::TYPE_OTHER)
                                <a href="{{ route('admin.research.topic-fee.other', $row->id) }}">
                                    {{ __('data_field_name.research_cost.type_other') }}
                                </a>
                            @elseif ($row->type == \App\Enums\ETopicFeeDetail::TYPE_EXPERT)
                                <a href="{{ route('admin.research.topic-fee.expert', $row->id) }}">
                                    {{ __('data_field_name.research_cost.type_expert') }}
                                </a>
                            @else
                                <a href="{{ route('admin.research.topic-fee.normal', $row->id) }}">
                                    {!! $row->name ?? '' !!}
                            @endif
                        </td>
                        <td class="text-center">{{ numberFormat($row->total_capital) ?? '' }}</td>
                        <td class="text-center">
                            {{ ratioFormat($row->total_capital, $dataTotal['total_capital'], 2) }}</td>
                        <td class="text-center">{{ numberFormat($row->total_prescribed_expense) }}</td>
                        @for ($i = 0; $i < $passYears; $i++)
                            <td  class="text-center">{{ numberFormat(removeFormatNumber($row->topic_fee_detail_expense['expense_years'][$i] ?? 0))}}</td>
                            <td class="text-center">{{ numberFormat(removeFormatNumber($row->topic_fee_detail_expense['prescribed_expense_years'][$i] ?? 0))}}</td>
                        @endfor
                        <td class="text-center">
                            @if ($dataTopicFee->status == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                                @if ($checkDestroyPermission)
                                    <button title="{{ __('common.button.delete') }}" type="button"
                                        data-toggle="modal" data-target="#clearDataTopicFeeDetail"
                                        wire:click="clear({{ $row->id }})"
                                        class="btn-sm border-0 bg-transparent mr-1">
                                        <img src="/images/clear.png" alt="clear">
                                    </button>
                                @endif
                                @if ($checkEditPermission)
                                    <button title="{{ __('common.button.edit') }}" type="button" data-toggle="modal"
                                        data-target="#editTopicFeeDetail" wire:click="edit({{ $row->id }})"
                                        class="btn-sm border-0 bg-transparent mr-1">
                                        <img src="/images/edit.png" alt="edit">
                                    </button>
                                @endif
                                @if ($row->type == \App\Enums\ETopicFeeDetail::TYPE_NORMAL)
                                    @include('livewire.common.buttons._delete')
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @if (count($data) > 0)
            {{ $data->links() }}
        @else
            <div class="title-approve text-center">
                <span>{{ __('common.message.empty_search') }}</span>
            </div>
        @endif
        @include('livewire.common.modal._modalDelete')

        @if ($dataTopicFee->status == \App\Enums\ETopicFee::STATUS_APPROVAL)
            <div class="row mb-5">
                @livewire('common.list-attach-file', ['modelId' => $dataTopicFee->id, 'type' =>
                \App\Enums\EApiApproval::TYPE_COST])
            </div>
        @endif

        <div class="col-md-12 text-center mt-5">
            <a href="{{ route('admin.research.topic-fee.index') }}"
                class="btn btn-cancel">{{ __('common.button.cancel') }}</a>
            @if ($dataTopicFee->status == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                <button type="button" wire:click.prevent="submitTopicFee()"
                    class="btn btn-save">{{ __('data_field_name.research_cost.text_submit_cost') }}</button>
            @endif
        </div>
    </div>

    <!-- Modal Create -->
    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="createTopicFeeDetail" tabindex="-1"
            aria-labelledby="createTopicFeeDetail" aria-hidden="true">
            <div class="modal-dialog  modal-appraisal">
                <div class="modal-content personnel">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{ __('data_field_name.research_cost.create_content_cost') }}</h4>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.content_expenses') }}<span
                                    class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.defer="name">
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.note') }}</label>
                            <textarea rows="4" class="form-control form-input" wire:model.defer="note"></textarea>
                            @error('note') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="attached-center">
                            <button type="button" id="close-modal-create" class="btn btn-cancel close-btn mt-0"
                                data-dismiss="modal">{{ __('common.button.close') }}</button>
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="store()"
                                class="btn btn-save close-modal">{{ __('common.button.save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Create -->

    <!-- Modal Edit -->
    <div wire:ignore.self class="modal fade modal-custom-create" id="editTopicFeeDetail" tabindex="-1"
        aria-labelledby="editTopicFeeDetail" aria-hidden="true">
        <div class="modal-dialog modal-appraisal" style="min-width: 50%;">
            <div class="modal-content personnel">
                <form wire:submit.prevent="submit" autocomplete="off" class="modal-body box-user">
                    <h4 class="text-center">{{ __('data_field_name.research_cost.edit_content_cost') }}</h4>
                    @if ($type == \App\Enums\ETopicFeeDetail::TYPE_NORMAL)
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.content_expenses') }}</label>
                            <input type="text" class="form-control form-input" wire:model.defer="name">
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                    @endif
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <label>{{ __('data_field_name.research_cost.cost_name') }} : </label>
                        </div>
                        <div class="col-md-8">
                            @if($type == \App\Enums\ETopicFeeDetail::TYPE_PEOPLE )
                            <span>{{ __('data_field_name.research_cost.type_people') }}</span>
                            @elseif($type == \App\Enums\ETopicFeeDetail::TYPE_MATERIAL) 
                            <span>{{ __('data_field_name.research_cost.type_material') }}</span>
                            @elseif($type == \App\Enums\ETopicFeeDetail::TYPE_EQUIPMENT)
                            <span>{{ __('data_field_name.research_cost.type_equipment') }}</span>
                            @elseif($type == \App\Enums\ETopicFeeDetail::TYPE_SHOP_REPAIR)
                            <span>{{ __('data_field_name.research_cost.type_shop_repair') }}</span>
                            @elseif($type == \App\Enums\ETopicFeeDetail::TYPE_OTHER)
                            <span>{{ __('data_field_name.research_cost.type_other') }}</span>
                            @elseif($type == \App\Enums\ETopicFeeDetail::TYPE_EXPERT)
                            <span>{{ __('data_field_name.research_cost.type_expert') }}</span>
                            @else
                            <span>{{$name}}<span>
                            @endif
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <label>{{ __('data_field_name.research_cost.total_capital') }} : </label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" readonly class="form-control"
                                value="{{ numberFormat((int) $total_capital) }}">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <label>{{ __('data_field_name.research_cost.prescribed_expenses') }} : </label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" readonly class="form-control form-input format_number"
                                wire:model.lazy="total_prescribed_expense">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">{{ __('data_field_name.research_cost.details_by_year') }} :</label>
                        @for ($i = 0; $i < $passYears; $i++)
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <label>{{ __('data_field_name.research_cost.expense_year') }}
                                        {{ $i + 1 }} <span class="text-danger">*</span></label>
                                    <input @if ($i == $passYears - 1 && $i !=0)  readonly @endif maxlength="18" type="text"
                                        class="form-control form-input format_number"
                                        wire:model.lazy="expenseYears.{{ $i }}">
                                        @error('expenseYears.' . $i) <span class="error text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-md-6">
                                    <label>{{ __('data_field_name.research_cost.prescribed_expenses_year') }}
                                        {{ $i + 1 }} <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control form-input format_number" maxlength="18" 
                                        wire:model.lazy="prescribedExpenseYears.{{ $i }}">
                                        @error('prescribedExpenseYears.' . $i) <span class="error text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                        @endfor
                    </div>


                    <div class="attached-center">
                        <button type="button" id="close-modal-edit" class="btn btn-cancel close-btn mt-0"
                            data-dismiss="modal">{{ __('common.button.close') }}</button>
                        <button id="buttonCreateBoard" type="button" wire:click.prevent="update"
                            class="btn btn-save close-modal">{{ __('common.button.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal Edit -->

    <!-- Modal Clear -->
    <div wire:ignore.self class="modal fade" id="clearDataTopicFeeDetail" tabindex="-1"
        aria-labelledby="clearDataTopicFeeDetail" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('common.confirm_message.confirm_title') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ __('notification.topic_fee.clear_detail') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                        data-dismiss="modal">{{ __('common.button.no') }}</button>
                    <button type="button" wire:click.prevent="clearData()" class="btn btn-danger"
                        data-dismiss="modal">{{ __('common.button.yes') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Clear -->

    <script>
        $("document").ready(function() {
            $('.buttonCreate').click(function() {
                $('.error').remove();
                $('.form-input').val('');
            });
            $('#createTopicFeeDetail').on('hidden.bs.modal', function() {
                $('.error').remove();
                window.livewire.emit('resetData');
            });

            window.livewire.on('close-modal-create', () => {
                document.getElementById('close-modal-create').click()
            });
            window.livewire.on('close-modal-edit', () => {
                document.getElementById('close-modal-edit').click()
            });
        });
    </script>

</div>
