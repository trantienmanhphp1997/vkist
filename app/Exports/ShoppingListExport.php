<?php

namespace App\Exports;

use App\Models\Shopping;
use App\Enums\EShoppingStatus;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
class ShoppingListExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $getIdSelected;

    function __construct($searchTerm, $currentWork, $searchStatus, $checkApprovePermission) {
        $this->searchTerm = $searchTerm;
        $this->currentWork = $currentWork;
        $this->searchStatus = $searchStatus;
        $this->checkApprovePermission = $checkApprovePermission;
    }

    public function collection()
    {
        $query = Shopping::query();

        if ($this->currentWork) {
            $query->where('executor_id', auth()->id())->where('status', EShoppingStatus::PENDING);
        } else if (!$this->currentWork && strlen($this->searchStatus) > 0) {
            $query->where('status', '=', $this->searchStatus);
        }

        // quản lý mua sắm được xem các đề xuất trừ chưa phê duyệt hoặc bị từ chối
        if (!$this->checkApprovePermission) {
            $query->whereNotIn('status', [EShoppingStatus::NOT_APPROVED, EShoppingStatus::REJECTED]);
        }

        if (strlen($this->searchTerm) > 0) {
            $text = $this->searchTerm;
            $unsignText = strtolower(removeStringUtf8($this->searchTerm));
            $query->whereRaw("unsign_text LIKE '%$unsignText%'")
                ->orWhereHas('budget', function ($query) use ($text, $unsignText) {
                    $query->whereRaw("LOWER(name) LIKE LOWER('%$text%')")
                        ->orWhereRaw("unsign_text LIKE '%$unsignText%'");
                });
        }

        $query->with(['executor:id,name', 'budget:id,name', 'proposer:id,name,user_info_id', 'proposer.info.department:id,name']);

        $query = $query->orderBy('created_at', 'desc')->get();
        $query = $query->map(function ($item, $index) {

            return [
                'index' => $index + 1,
                'created_at' => $item->created_at,
                'name' => $item->name,
                'proposer' => $item->proposer->name ?? null,
                'department' => !empty($item->proposer->info->department) ? $item->proposer->info->department->name : null,
                'executor' => $item->executor->name ?? null,
                'money' => $item->estimate_money,
                'status' => EShoppingStatus::valueToName($item->status),
            ];
        });
        return $query;
    }
    public function headings():array
    {
        return
        [
            __('data_field_name.common_field.stt'),
            __('data_field_name.shopping.needed_date'),
            __('data_field_name.shopping.name'),
            __('data_field_name.shopping.proposer'),
            __('data_field_name.shopping.proposer_department'),
            __('data_field_name.shopping.executor'),
            __('data_field_name.shopping.estimate_money'),
            __('data_field_name.shopping.status'),
        ];
    }
    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();

            $highestRow = $active_sheet->getHighestRow();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
            ];
            $borderStyle = [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [
                       'rgb' => '808080'
                    ]
                ]
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A3:I3');
            $event->sheet->setCellValue('A3', __('data_field_name.shopping.list'));
            $event->sheet->getStyle('A3:I3')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A6:H6';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle('A6:H' . $highestRow)->getBorders()->applyFromArray($borderStyle);
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle('A3:I3')->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        return __('data_field_name.shopping.list');
    }

    public function startCell(): string
    {
        return 'A6';
    }
}
