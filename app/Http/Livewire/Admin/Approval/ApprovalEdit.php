<?php

namespace App\Http\Livewire\Admin\Approval;

use App\Enums\EApproval;
use App\Enums\EIdeaStatus;
use App\Enums\EResearchPlan;
use App\Enums\ETopicFee;
use App\Enums\ETopicStatus;
use App\Models\AppraisalBoard;
use App\Models\Approval;
use App\Http\Livewire\Base\BaseLive;
use App\Models\ApprovalIdealDetail;
use App\Models\Ideal;
use App\Models\ResearchPlan;
use App\Models\Topic;
use App\Models\TopicFee;
use Illuminate\Database\Eloquent\Builder;

class ApprovalEdit extends BaseLive
{

    public $searchTerm;
    public $searchStatus;
    public $searchType;
    public $type;
    public $status;
    public $name;
    public $code;
    public $topic_id;
    public $listTopic;
    public $listTopicFee;
    public $listPlan;
    public $appraisal_board_id;
    public $listIdeal;
    public $ideal_id;
    public $text_ideal_error;
    public $idApproval;
    public $listAppraisal;
    public $readonly;
    public $appraisalName;
    public $topic_fee_id;
    public $research_plan_id;
    public $contract_code;
    public $listIdealApproval;

    protected $listeners = ['loadDataAppraisal'];

    public function mount($approval)
    {
        $this->idApproval = $approval->id;
        $this->name = $approval->name;
        $this->code = $approval->code;
        $this->type = $approval->type;
        $this->status = $approval->status;

        $details = ApprovalIdealDetail::where('approval_id', '=', $this->idApproval)->get();
        if ($details->isNotEmpty()) {
            foreach ($details as $detail) {
                $this->ideal_id[$detail->ideal_id] = 1;
                $this->listIdealApproval[] = $detail->ideal_id;
            }
        }
        $approval = Approval::findOrFail($this->idApproval);
        if ($approval->type != EApproval::TYPE_IDEAL) {
            $this->topic_id = $approval->topic_id;
            $this->topic_fee_id = $approval->topic_fee_id;
            $this->research_plan_id = $approval->research_plan_id;
            $this->appraisal_board_id = $approval->appraisal_board_id;
            $this->contract_code = $approval->contract_code;
        }
    }

    public function render()
    {
        $approval = Approval::findOrFail($this->idApproval);
        $this->getListAppraisal();

        $typeAppraisal = EApproval::getListType();
        $statusAppraisal = $this->getListSataus($approval);

        // Check readonly
        $this->readonly = false;
        if ($this->type != EApproval::TYPE_IDEAL) {
            if ($this->status == EApproval::STATUS_NEW || $this->status == EApproval::STATUS_COMPLETE_PROFILE) {
                $this->readonly = true;
            }

            $this->loadDataAppraisal();
            if ($this->type == EApproval::TYPE_TOPIC) {
                $this->getDataTopic();
            }
            if ($this->type == EApproval::TYPE_COST) {
                $this->getDataTopicFee();
            }
            if ($this->type == EApproval::TYPE_PLAN) {
                $this->getDataResearchPlan();
            }
        }

        if ($this->type == EApproval::TYPE_IDEAL) {
            $this->readonly = true;
            $this->getDataIdeal();
        }

        return view('livewire.admin.approval.approval-edit', [
            'typeAppraisal' => $typeAppraisal,
            'statusAppraisal' => $statusAppraisal,
            'approval' => $approval,
        ]);
    }

    public function update()
    {
        $this->validation();

        if ($this->type == EApproval::TYPE_IDEAL && !empty($this->ideal_id)) {
            $checkData = false;
            if (!empty($this->ideal_id)) {
                foreach ($this->ideal_id as $checked) {
                    if ($checked) {
                        $checkData = true;
                        break;
                    }
                }
            }

            if (!$checkData) {
                $this->text_ideal_error = __('notification.approval.select_ideal');
                return false;
            }
        }

        $approval = Approval::findOrFail($this->idApproval);
        $approval->status = $this->status;

        // Insert data topic, topic_fee, research_plan
        if ($this->type != EApproval::TYPE_IDEAL) {
            if ($this->status == EApproval::STATUS_NEW || $this->status == EApproval::STATUS_COMPLETE_PROFILE) {
                $approval->code = $this->code;
                $approval->name = $this->name;
                if ($approval->type == EApproval::TYPE_TOPIC) {
                    $approval->topic_id = $this->topic_id;
                }
                if ($approval->type == EApproval::TYPE_COST) {
                    $approval->topic_fee_id = $this->topic_fee_id;
                }
                if ($approval->type == EApproval::TYPE_PLAN) {
                    $approval->research_plan_id = $this->research_plan_id;
                }
            }
            if ($this->status == EApproval::STATUS_WAIT_ASSESSMENT || $this->status == EApproval::STATUS_COMPLETE_PROFILE) {
                $approval->appraisal_board_id = $this->appraisal_board_id;
            }
            if ($this->status == EApproval::STATUS_APPROVAL) {
                $approval->contract_code = $this->contract_code;

                if ($approval->type == EApproval::TYPE_TOPIC) {
                    $topic = Topic::findOrFail($approval->topic_id);
                    $topic->contract_code = $this->contract_code;
                    $topic->save();
                }
            }
        }

        // Insert data ideal
        if ($this->type == EApproval::TYPE_IDEAL) {
            $approval->name = $this->name;
            $approval->code = $this->code;
        }

        $approval->save();

        // Insert data ideal
        if ($this->type == EApproval::TYPE_IDEAL && !empty($this->ideal_id)) {
            ApprovalIdealDetail::where('approval_id', '=', $this->idApproval)->delete();
            foreach ($this->ideal_id as $ideaId => $checked) {
                if ($checked) {
                    ApprovalIdealDetail::create([
                        'approval_id' => $this->idApproval,
                        'ideal_id' => $ideaId,
                    ]);
                }
            }
        }

        $this->updateModel();

        session()->flash('success', __("notification.common.success.update"));
        return redirect()->route('admin.approval.index');
    }

    public function getDataTopic()
    {
        $topics = Topic::all();
        if ($topics->isNotEmpty()) {
            foreach ($topics as $topic) {
                $this->listTopic[$topic->id] = [
                    'code' => $topic->code,
                    'name' => $topic->name,
                ];
            }
        }
    }

    public function getDataIdeal()
    {
        $this->listIdeal = Ideal::with('researchField')
            ->where('status', '=', EIdeaStatus::JUST_CREATED)
            ->get()
            ->mapWithKeys(function ($ideal) {
                return [
                    $ideal->id => [
                        'id' => $ideal->id,
                        'code' => $ideal->code,
                        'name' => $ideal->name,
                        'fee' => $ideal->fee,
                        'research_name' => $ideal->researchField->name ?? '',
                        'fullname' => $ideal->admin->info->fullname ?? '',
                        'created_at' => reFormatDate($ideal->created_at, 'd/m/Y') ?? '',
                    ]
                ];
            })
            ->all();
    }

    public function getDataTopicFee()
    {
        $topicFees = TopicFee::all();
        if ($topicFees->isNotEmpty()) {
            foreach ($topicFees as $topicFee) {
                $this->listTopicFee[$topicFee->id] = [
                    'name' => $topicFee->name,
                ];
            }
        }
    }

    public function getDataResearchPlan()
    {
        $plans = ResearchPlan::all();
        if ($plans->isNotEmpty()) {
            foreach ($plans as $plan) {
                $this->listPlan[$plan->id] = [
                    'name' => $plan->name,
                ];
            }
        }
    }

    public function validation()
    {
        $typeAppraisal = [EApproval::TYPE_IDEAL, EApproval::TYPE_TOPIC, EApproval::TYPE_COST, EApproval::TYPE_PLAN];

        $rules = [
            'name' => ['required', 'regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/ ]+$/', 'max:100'],
            'type' => 'required|numeric|in:' . implode(',', $typeAppraisal),
        ];

        if ($this->type == EApproval::TYPE_TOPIC) {
            $rules['topic_id'] = 'required|exists:topic,id';
        }
        if ($this->type == EApproval::TYPE_COST) {
            $rules['topic_fee_id'] = 'required|exists:topic_fee,id';
        }
        if ($this->type == EApproval::TYPE_PLAN) {
            $rules['research_plan_id'] = 'required|exists:research_plan,id';
        }
        if ($this->type != EApproval::TYPE_IDEAL && $this->status == EApproval::STATUS_WAIT_ASSESSMENT) {
            $rules['appraisal_board_id'] = 'required|numeric';
        }

        if ($this->type == EApproval::TYPE_IDEAL || ($this->type != EApproval::TYPE_IDEAL && $this->status == EApproval::STATUS_COMPLETE_PROFILE)) {
            $rules['code'] = ['required', 'regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/ ]+$/', 'max:20'];
        }
        if ($this->type != EApproval::TYPE_IDEAL && $this->status == EApproval::STATUS_APPROVAL) {
            $rules['contract_code'] = ['required', 'regex:/^[a-zA-Z0-9]+$/', 'max:20'];
        }

        $attribute = [
            'name' => __('data_field_name.approval_topic.name_profile'),
            'status' => __('data_field_name.common_field.status'),
            'type' => __('data_field_name.approval_topic.profile_type'),
            'code' => __('data_field_name.approval_topic.profile_code'),
            'topic_id' => __('data_field_name.topic.code'),
            'topic_fee_id' => __('data_field_name.approval_topic.type_cost_estimate'),
            'research_plan_id' => __('data_field_name.approval_topic.type_research_plan'),
            'appraisal_board_id' => __('data_field_name.approval_topic.appraisal_board'),
            'contract_code' => __('data_field_name.contract.code'),
            'ideal.*' => __('data_field_name.approval_topic.type_ideal'),
        ];

        $this->validate($rules, [], $attribute);
    }

    public function changeType()
    {
        $this->resetValidation();
        $this->text_ideal_error = null;
    }

    public function getListAppraisal()
    {
        $appraisals = AppraisalBoard::all();
        if ($appraisals->isNotEmpty()) {
            foreach ($appraisals as $appraisal) {
                $this->listAppraisal[$appraisal->id] = $appraisal->name;
            }
        }
    }

    public function getListSataus($approval)
    {
        $arrStatus = [];
        if ($approval->type != EApproval::TYPE_IDEAL) {
            if ($approval->status == EApproval::STATUS_NEW) {
                $arrStatus = [
                    EApproval::STATUS_NEW => __('data_field_name.approval_topic.status_new'),
                    EApproval::STATUS_COMPLETE_PROFILE => __('data_field_name.approval_topic.status_complete_profile'),
                    EApproval::STATUS_INVALID_PROFILE => __('data_field_name.approval_topic.status_invalid_profile'),
                ];
            } elseif ($approval->status == EApproval::STATUS_COMPLETE_PROFILE) {
                $arrStatus = [
                    EApproval::STATUS_COMPLETE_PROFILE => __('data_field_name.approval_topic.status_complete_profile'),
                    EApproval::STATUS_WAIT_ASSESSMENT => __('data_field_name.approval_topic.status_wait_assessment'),
                ];
            } elseif ($approval->status == EApproval::STATUS_INVALID_PROFILE) {
                $arrStatus = [
                    EApproval::STATUS_COMPLETE_PROFILE => __('data_field_name.approval_topic.status_complete_profile'),
                    EApproval::STATUS_INVALID_PROFILE => __('data_field_name.approval_topic.status_invalid_profile'),
                ];
            } elseif ($approval->status == EApproval::STATUS_WAIT_ASSESSMENT) {
                $arrStatus = [
                    EApproval::STATUS_WAIT_ASSESSMENT => __('data_field_name.approval_topic.status_wait_assessment'),
                ];
            } elseif ($approval->status == EApproval::STATUS_WAIT_APPROVAL) {
                $arrStatus = [
                    EApproval::STATUS_WAIT_APPROVAL => __('data_field_name.approval_topic.status_wait_approval'),
                ];
            } elseif ($approval->status == EApproval::STATUS_REJECT) {
                $arrStatus = [
                    EApproval::STATUS_REJECT => __('data_field_name.approval_topic.status_reject'),
                ];
            } elseif ($approval->status == EApproval::STATUS_APPROVAL) {
                $arrStatus = [
                    EApproval::STATUS_APPROVAL => __('data_field_name.approval_topic.status_approval'),
                ];
            }
        } elseif ($approval->type == EApproval::TYPE_IDEAL) {
            if ($approval->status == EApproval::STATUS_NEW) {
                $arrStatus = [
                    EApproval::STATUS_NEW => __('data_field_name.approval_topic.status_new'),
                ];
            } elseif ($approval->status == EApproval::STATUS_WAIT_APPROVAL) {
                $arrStatus = [
                    EApproval::STATUS_WAIT_APPROVAL => __('data_field_name.approval_topic.status_wait_approval'),
                ];
            } elseif ($approval->status == EApproval::STATUS_REJECT) {
                $arrStatus = [
                    EApproval::STATUS_REJECT => __('data_field_name.approval_topic.status_reject'),
                ];
            } elseif ($approval->status == EApproval::STATUS_APPROVAL) {
                $arrStatus = [
                    EApproval::STATUS_APPROVAL => __('data_field_name.approval_topic.status_approval'),
                ];
            }
        }
        return $arrStatus;
    }

    public function loadDataAppraisal()
    {
        if ($this->type == EApproval::TYPE_TOPIC) {
            $appraisalBoard = AppraisalBoard::where('topic_id', '=', $this->topic_id)->first();
        }
        if ($this->type == EApproval::TYPE_COST) {
            $appraisalBoard = AppraisalBoard::where('topic_fee_id', '=', $this->topic_fee_id)->first();
        }
        if ($this->type == EApproval::TYPE_PLAN) {
            $appraisalBoard = AppraisalBoard::where('research_plan_id', '=', $this->research_plan_id)->first();
        }
        if (!empty($appraisalBoard)) {
            $this->appraisal_board_id = $appraisalBoard->id;
            $this->appraisalName = $appraisalBoard->name;
        }
    }

    protected function updateModel()
    {
        $statusModel = '';
        if ($this->status == EApproval::STATUS_COMPLETE_PROFILE) {
            $statusModel = ETopicStatus::STATUS_COMPLETE_PROFILE;
        }
        if ($this->status == EApproval::STATUS_INVALID_PROFILE) {
            $statusModel = ETopicStatus::STATUS_INVALID_PROFILE;
        }
        if ($this->status == EApproval::STATUS_WAIT_ASSESSMENT) {
            $statusModel = ETopicStatus::STATUS_WAIT_ASSESSMENT;
        }
        if (!empty($statusModel)) {
            if ($this->type == EApproval::TYPE_TOPIC) {
                $topic = Topic::findOrFail($this->topic_id);
                $topic->status = $statusModel;
                $topic->save();
            }
            if ($this->type == EApproval::TYPE_COST) {
                $topicFee = TopicFee::findOrFail($this->topic_fee_id);
                $topicFee->status = $statusModel;
                $topicFee->save();
            }
            if ($this->type == EApproval::TYPE_PLAN) {
                $researchPlan = ResearchPlan::findOrFail($this->research_plan_id);
                $researchPlan->status = $statusModel;
                $researchPlan->save();
            }
        }
    }
}
