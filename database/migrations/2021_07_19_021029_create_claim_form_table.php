<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_form', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('claim_date')->nullable()->comment('Ngày yêu cầu');
            $table->string('content',1000)->nullable()->comment('Ngày bao mat, thuc hien, de xuat');
            $table->string('reason',255)->nullable()->comment('ly do');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->tinyInteger('type')->nullable()->comment('1:bao mat; 2:bao hong; 3: cap phat; 4:cap phat hang loat; 5 thu hoi; 6: thu hoi hang loat, 7 dieu chuyen');
            $table->foreignId('receiver_id')->nullable()->constrained('user_info')->comment('người nhận');
            $table->foreignId('manager_id')->nullable()->constrained('user_info')->comment('người quản lý');

            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_form');
    }
}
