<?php

namespace App\Http\Controllers;

use App\Enums\EApiApproval;
use App\Models\Approval;
use App\Models\Asset;
use App\Models\Budget;
use App\Models\GwApproveResponse;
use App\Models\News;
use App\Models\User;
use App\Models\UserInf;
use App\Models\WorkPlan;
use App\Service\UserHasMenuService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $day = Carbon::today();
        $userCount = User::whereDate('created_at', $day)->count();
        $menu = UserHasMenuService::getMenu();
        $newsCount = News::whereDate('created_at', $day)->count();
        $newsPerDay = News::whereDate('created_at', $day)->get()->toArray();
        $newsPerDayId = [];
        foreach ($newsPerDay as $new){
            $newsPerDayId[] = $new['id'];
        }

        if (empty(session('newsPerDayId'))){
            session(['newsPerDayId' => $newsPerDayId]);
        }

        $assetCount = Asset::whereDate('created_at', $day)->count();
        $budgetCount = Budget::whereDate('created_at', $day)->count();
        $workPlanCount = WorkPlan::whereDate('created_at', $day)->count();
        $userInfoCount = UserInf::whereDate('created_at', $day)->count();
        $shoppingCount = 133 ;
        $listApproval = $this->getListApproval();

        return view('home',compact('shoppingCount','userCount','menu','newsCount','assetCount','budgetCount','workPlanCount','userInfoCount', 'listApproval'));
    }
    public function updateMenu(Request $request){
        $menuChecked = $request->checkedMenu ;

        if ($menuChecked && count($menuChecked) >5){
                session()->flash('error', __('permission.my-menu.over-option-in-menu'));
                return redirect()->back();
        }

        try {
            UserHasMenuService::updateMenu($menuChecked);
            session()->flash('success', __('permission.my-menu.save-option-menu-success'));
        }catch (\Exception $exception){
            session()->flash('error', __('permission.my-menu.save-option-menu-fail'));
        }

        return redirect()->back();
    }

    public function getListApproval()
    {
        $approvalGw = GwApproveResponse::whereNotNull('gw_status')->orderBy('created_at','DESC')->limit(5)->get();
        $listApproval = null;
        if ($approvalGw->isNotEmpty()) {
            $now = date('Y-m-d');
            foreach ($approvalGw as $gw) {
                $created_at = reFormatDate($gw->created_at, 'Y-m-d');
                $timeCreatedAt = strtotime($created_at);
                $timeNow = strtotime($now);
                $datediff = abs($timeNow - $timeCreatedAt);
                $time = floor($datediff / (60*60*24));

                if ($gw->gw_status == EApiApproval::STATUS_COMPLETE) {
                    $status = __('common.status.status_approval');
                } else if ($gw->gw_status == EApiApproval::STATUS_REJECT) {
                    $status = __('common.status.status_reject');
                } else {
                    $status = __('common.status.status_wait_approval');
                }

                $listApproval[] = [
                    'name' => !empty($gw->name) ? $gw->name : '',
                    'status' => $gw->gw_status,
                    'text_status' => $status,
                    'time' => (int)$time
                ];
            }
        }

        return $listApproval;
    }
}
