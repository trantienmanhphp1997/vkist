@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.allocated-revoke.allocated_to_employees')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._assetLeft')
@endsection
@section('content')
    @livewire('admin.asset.allocated-and-revoke.allocated', ['data' => $data])
@endsection
