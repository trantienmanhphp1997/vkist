<?php

namespace Database\Factories;

use App\Models\Department;
use App\Models\MasterData;
use App\Models\UserInf;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserInfFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserInf::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fullname' => $this->faker->name,
            'code' => $this->faker->unique()->numberBetween(100000, 999999),
            'address' => $this->faker->address,
            'phone' => $this->faker->buildingNumber,
            'birthday' => $this->faker->date,
            'gender' => $this->faker->numberBetween(0, 1),
            'email' => $this->faker->email,
            'issued_date' => $this->faker->date,
            'issued_place' => $this->faker->country,
            'department_id' => null,
            'position_id' => null,
            'academic_level_id' => null
        ];
    }
}
