<?php

namespace App\Models;

use App\Http\Livewire\Admin\General\WorkPlan\UserInfo;
use App\Models\UserInf;

class TopicHasUserInfo extends Participant
{
    protected $table = "topic_has_user_info";
    protected $fillable = [
        'academic_level_id',
        'mission_note',
        'created_at',
        'updated_at',
        'user_info_id',
        'topic_id',
        'role_id',
        'research_id'
    ];

    protected $mapRelations = [
        'staff' => 'userInfo',
        'work' => 'topic'
    ];

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    // a Mạnh dùng cái này
    public function user()
    {
        return $this->hasMany(UserInf::class, 'user_info_id');
    }

    public function userInfo()
    {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }

    // vai trò trong đề tài
    public function role()
    {
        return $this->belongsTo(MasterData::class, 'role_id');
    }

    // hướng nghiên cứu trong đề tài
    public function research()
    {
        return $this->belongsTo(MasterData::class, 'research_id');
    }
}
