@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.research_cost.work_detail_content') }}</title>
@endsection
@section('homeLeft')
@include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    @livewire('admin.research.cost.labor-participation-detail-list', ['topicFeeDetailId' => $data->id, 'topicFeeStatus' => $data->topicFee->status])
@endsection
