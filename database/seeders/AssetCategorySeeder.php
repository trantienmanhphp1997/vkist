<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\AssetCategory;

class AssetCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AssetCategory::factory()->times(10)->create();
    }
}
