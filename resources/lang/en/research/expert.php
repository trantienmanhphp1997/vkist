<?php
return [
    'table_column' => [
        'name_expert' => 'NAME EXPERT',
        'unit'=>'UNIT',
        'academic_level' => 'ACADEMIC LEVEL',
        'specialized' => 'SPECIALIZED',
        'work_unit' => 'WORK UNIT',
        'telephone_number' => 'TELEPHONE NUMBER',
        'address' => 'ADDRESS',
        'email' => 'EMAIL',
        'action' => 'ACTION',
    ],
    'title' => [
        'part-1' => 'Display',
        'part-2' => 'field of expert',
    ],
    'list' => 'Expert management',
    'list-new' => 'List of experts',
    'add_expert' => 'Add expert',
    'edit_expert' => 'Edit field of expert',

    'name_expert' => "Enter the expert's name",
    'academic_level' => 'Choose academic level',
    'specialized' => 'Science majors',
    'work_unit' => 'Enter the work unit',
    'telephone_number' => 'Enter the telephone number',
    'address' => 'Enter the address',
    'email' => 'Enter the email address',
    'close' => 'Close',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'create' => 'Create new',

    'validate' => [
        'name_expert' => "Expert's Name",
        'academic_level' => 'Academic level',
        'specialized' => 'Specialized',
        'work_unit' => 'Work unit',
        'telephone_number' => 'Telephone number',
        'address' => 'Address',
        'email' => 'Email',
        // 'telephoneNumberRegex' => 'Phone number must start with 0',
        'telephoneNumberDigits' => 'Phone number must be numeric',
    ],


];