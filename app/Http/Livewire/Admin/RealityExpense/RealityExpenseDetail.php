<?php

namespace App\Http\Livewire\Admin\RealityExpense;

use Livewire\Component;
use App\Models\Topic;
use App\Models\RealityExpense;
use App\Enums\ECapital;
use App\Enums\ETopicFeeDetail;
use App\Enums\EPayForm;
use App\Enums\ETypePay;
use App\Enums\EDisbursedStatus;
use App\Models\TopicFeeDetail;
use App\Http\Livewire\Base\BaseLive;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Auth;

class RealityExpenseDetail extends BaseLive
{

    public $topic_name;
    public $topic_id;
    public $topic_code;
    public $topicFeeList = [];
    public $type_expense;
    public $type_cost;
    public $payment;
    public $source_capital;
    public $approval_costs;
    public $total_cost;
    public $money_remain;
    public $user_request_id;
    public $note =[];
    public $task_id;
    public $expense_task =[];
    public $task_id_save;
    public $note_id_save;
    public $money_cost;
    public $disbursement_date;
    public $reality_expense;
    public $remain_day;
    public $capital;
    public $payform;
    public $typepay;
    public $status;
    public $type;
    public $status_disbursed;
    public $listFileUpload = [];
    public $listOldFileTemp = [];
    protected $listeners = [
        'choose_topic_info' => 'getTopicInfoList'
    ];
    public function mount()
    {
        $this->user_request_id = Auth::user()->id;
        $this->capital = [
            ECapital::ODA => __('data_field_name.reality_expense.ODA'),
            ECapital::STATE_CAPITAL => __('data_field_name.reality_expense.state_capital'),
        ];
        // Hình thức chi
        $this->payform = [
            EPayForm::EXPENSES => __('data_field_name.reality_expense.expenses'),
            EPayForm::NO_EXPENSES => __('data_field_name.reality_expense.no_expenses'),
        ];
        // Loại chi phí
        $this->typepay = [
            ETypePay::DIRECT_EXPENSES => __('data_field_name.reality_expense.direct_expenses'),
            ETypePay::INDIRECT_EXPENSES => __('data_field_name.reality_expense.indirect_expenses'),
        ];
        // Trạng thái giải ngân
        $this->status_disbursed = [
            EDisbursedStatus::DISBURSED => __('data_field_name.reality_expense.disbursed'),
            EDisbursedStatus::NOT_DISBURSED => __('data_field_name.reality_expense.not_disbursed'),
        ];
        $today = Carbon::now();
        $this->topic_id = $this->reality_expense->topic_id;
        $this->type_expense = $this->reality_expense->type_expense;
        $this->type_cost = $this->reality_expense->type_cost;
        $this->payment = $this->reality_expense->payment;
        $this->source_capital = $this->reality_expense->source_capital;
        $this->approval_costs = numberFormat($this->reality_expense->approval_costs);
        $this->total_cost = numberFormat($this->reality_expense->total_cost);
        $this->money_remain = numberFormat($this->reality_expense->money_remain);
        $this->user_request_id = $this->reality_expense->user_request_id;
        $this->task_id_save = $this->reality_expense->expense_task;
        $this->note_id_save = $this->reality_expense->note;
        $this->money_cost = numberFormat($this->reality_expense->money_cost);
        $this->disbursement_date = date('Y-m-d',strtotime($this->reality_expense->disbursement_date));
        $this->disbursement = new Carbon(date('Y-m-d',strtotime($this->reality_expense->disbursement_date)));
        $this->remain_day = strtotime($today) > strtotime($this->disbursement)? (0-$this->disbursement->diffInDays($today)) : $this->disbursement->diffInDays($today);
        $this->status = $this->reality_expense->status;
        $this->getTopicInfoList(['id' => $this->topic_id]);
        $this->updatedTypeExpense();
    }
    public function render()
    {
        // $this->reality_expense = RealityExpense::all();
        // foreach($this->reality_expense as $value){
        //     $this->status
        // }
        $model_name = RealityExpense::class;
        $type_receipt = Config::get('common.type_upload.Receipt');
        $type_license = Config::get('common.type_upload.License');
        $folder = app($model_name)->getTable();

        return view('livewire.admin.reality-expense.reality-expense-detail', ['model_name' => $model_name, 'type_receipt' => $type_receipt, 'type_license' => $type_license, 'folder' => $folder]);
    }
    public function store(){
        session()->flash('success', __("notification.common.success.update"));
        return redirect()->route('admin.reality_expense.index');
    }
    public function getTopicInfoList($data)
    {
        $topic = Topic::with(['topicFeeDetails', 'task_work','approval'])->findOrFail($data['id']);
        $approval = $topic->approval;
        $this->topic_code = 0;
        foreach($approval as $value){
            $this->topic_code = $value->code;
        }
        $this->topic_name = $topic->name;
        $this->expense_task = $topic->task_work;
        $this->topic_id = $topic->id;

        $this->topicFeeList = $topic->topicFeeDetails;
    }
    public function updatedTypeExpense()
    {
        $topicFeeDetail = TopicFeeDetail::with(['topic_detail_work.task','expert_cost','material_cost','equipment_cost',
        'asset_cost','topic_other_fee','topic_normal_cost'])->findOrFail($this->type_expense);
        $this->approval_costs = numberFormat($topicFeeDetail->approval_cost);
        switch ($topicFeeDetail->type) {
            case ETopicFeeDetail::TYPE_EXPERT :
            $this->note = $topicFeeDetail->expert_cost;
            break;
            case ETopicFeeDetail::TYPE_MATERIAL:
            $this->note = $topicFeeDetail->material_cost;
            break;
            case ETopicFeeDetail::TYPE_EQUIPMENT:
            $this->note = $topicFeeDetail->equipment_cost;
            break;
            case ETopicFeeDetail::TYPE_SHOP_REPAIR:
            $this->note = $topicFeeDetail->asset_cost;
            break;
            case ETopicFeeDetail::TYPE_PEOPLE:
            $this->note = $topicFeeDetail->topic_detail_work;
            break;
            case ETopicFeeDetail::TYPE_OTHER:
            $this->note = $topicFeeDetail->topic_other_fee;
            break;
            case ETopicFeeDetail::TYPE_NORMAL:
            $this->note = $topicFeeDetail->topic_normal_cost;
            break;
            default:
                break;
        }
        $this->type = $topicFeeDetail->type;

    }
}
