<div class="col-md-10 col-xl-11 box-detail box-user work-plan">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('data_field_name.work_plan.index.general')}} </a> \
                <span>{{__('data_field_name.work_plan.index.work_plan')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row row_title">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.work_plan.index.title')}}</h3>
                </div>
                <div class="col-md-6">
                    <div class="float-right">
                        @if($checkCreatePermission)
                            <a href="{{route('admin.general.work-plan.create')}}">
                                <button class="btn btn-viewmore-news mr-2"><img src="/images/plus2.svg" alt="plus">
                                    {{__('data_field_name.work_plan.index.create')}}
                                </button>
                            </a>
                        @endif
                        @if(checkRoutePermission('download'))
                            <button title="{{__('common.button.export_file')}}" class="btn btn-fix bg-content px-10 py-10 bg-white mt-2" wire:click="export">
                                <img src="/images/DownloadBlue.svg" alt="download">
                            </button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class=" search-expertise search-plan d-inline-block">
                                <div class="search-expertise inline-block mb-12  w-xs-100">
                                    <input type="text" class="form-control size13" name="searchTerm" autocomplete="off"
                                           wire:model.debounce.1000ms="searchTerm"
                                           placeholder="{{__('data_field_name.work_plan.index.search')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                                <div class="w-170 inline-block mb-12  w-xs-100">
                                    <select class="form-control form-control-lg size13" wire:model.lazy="searchStatus">
                                        <option value="">{{__('data_field_name.work_plan.index.status1')}}</option>
                                        @if (!empty($listStatus))
                                            @foreach($listStatus as $key => $status)
                                                <option value="{{ $key }}">{{ $status }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="w-170 inline-block mb-12  w-xs-100">
                                    <select class="form-control form-control-lg selectSearch size13"
                                            wire:model.lazy="searchPriority">
                                        <option value="">{{__('data_field_name.work_plan.index.priority1')}}</option>
                                        @foreach($priority_list as $key => $val)
                                            <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive ">
                        <table class="table working-plan">
                            <thead>
                            <tr class="border-radius">
                                <th rowspan="2" scope="col"
                                    class="w90 border-radius-left">{{__('data_field_name.work_plan.index.missions')}}</th>
                                <th rowspan="2" scope="col">{{__('data_field_name.work_plan.index.mission_id')}}</th>
                                <th rowspan="2" scope="col"
                                    class="text-center">{{__('data_field_name.work_plan.index.priority')}}</th>
                                <th colspan="2" scope="col">{{__('data_field_name.work_plan.index.plan')}}</th>
                                <th colspan="2" scope="col">{{__('data_field_name.work_plan.index.real')}}</th>
                                <th rowspan="2" scope="col">{{__('data_field_name.work_plan.index.status')}}</th>
                                <th rowspan="2" scope="col">{{__('data_field_name.work_plan.index.lead')}}</th>
                                <th rowspan="2" scope="col"
                                    class="border-radius-right min-width">{{__('data_field_name.work_plan.index.department')}}</th>
                            </tr>
                            <tr class="border-radius">
                                <th scope="col"
                                    class="w100 border-r">{{__('data_field_name.work_plan.index.start')}}</th>
                                <th scope="col" class="w100">{{__('data_field_name.work_plan.index.end')}}</th>
                                <th scope="col"
                                    class="w100 border-r">{{__('data_field_name.work_plan.index.start')}}</th>
                                <th scope="col" class="w100 ">{{__('data_field_name.work_plan.index.end')}}</th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                            @forelse ($data as $key => $val)
                                <tr>
                                    <td class="text-center"><a
                                            href="{{route('admin.general.work-plan.edit',$val->id)}}">{!! boldTextSearch( $val->content,$searchTerm ) !!}</a>
                                    </td>
                                    <td class="text-center"><a
                                            href="{{route('admin.general.work-plan.show',$val->id)}}">{{ $val-> missions_code }}</a>
                                    </td>
                                    <td class="text-center">
                                        <p class="margin_bot_0" style='{{$val->priority_level==\App\Enums\EWorkPlanPriorityLevel::COGENCY?"color:red":($val->priority_level==\App\Enums\EWorkPlanPriorityLevel::HIGH?"color:orange":"color:#32CD32")}}'>{{\App\Enums\EWorkPlanPriorityLevel::valueToName($val->priority_level) }}</p>
                                    </td>
                                    <td class="text-center">
                                        {{reFormatDate($val->start_date)? date('d/m/Y',strtotime($val->start_date)):''}}
                                    </td>
                                    <td class="text-center">
                                        {{reFormatDate($val->end_date)? date('d/m/Y',strtotime($val->end_date)):''}}
                                    </td>
                                    <td class="text-center">
                                        {{reFormatDate($val->real_start_date)? date('d/m/Y',strtotime($val->real_start_date)):''}}
                                    </td>
                                    <td class="text-center">
                                        {{reFormatDate($val->real_end_date)? date('d/m/Y',strtotime($val->real_end_date)):''}}
                                    </td>
                                    <td class="text-center">
                                        @if ($val->status == \App\Enums\EWorkPlan::STATUS_NEW)
                                            <button
                                                class="btn  mr0 btn-plan bg-blue bg-blue"> {{__('data_field_name.work_plan.index.status_new')}}</button>
                                        @elseif ($val->status == \App\Enums\EWorkPlan::STATUS_WAIT_APPROVAL)
                                            <button
                                                class="btn  mr0 btn-plan bg-blue high"> {{__('data_field_name.work_plan.index.wait')}}</button>
                                        @elseif ($val->status == \App\Enums\EWorkPlan::STATUS_APPROVAL)
                                            <button
                                                class="btn  mr0 btn-plan bg-blue medium"> {{__('data_field_name.work_plan.index.done')}}</button>
                                        @elseif ($val->status == \App\Enums\EWorkPlan::STATUS_REJECT)
                                            <button
                                                class="btn  mr0 btn-plan bg-blue emergency"> {{__('data_field_name.work_plan.index.deny')}}</button>
                                        @else
                                            <button class="btn  mr0 btn-plan emergency"></button>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $val-> leader_name }}</td>
                                    <td class="text-center">{{ $val-> leader_department_name }}</td>
                                </tr>

                            @empty
                                <tr>
                                    <td colspan="12">{{__('common.message.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>

                    {{$data->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
<!--end news -->


<!--end main content-->

