<?php

namespace App\Http\Controllers\Admin\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BusinessFee;

class BusinessFeeController extends Controller
{
    public function index(){
        return view('admin.general.business-fee.index');
    }
    public function create(){

        return view('admin.general.business-fee.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'code'=>'required|max:48|regex:/^[A-Z0-9,.]+$/u|unique:business_fee,code',
            'name'=>'required|max:48',
            'accounting_code'=>'required|max:48',
            'tracking_type'=>'required|max:48|regex:/^[a-zA-Z0-9]+$/u',
            'note'=>'max:255',],[],[

            'code'=>__('data_field_name.businesfee.code'),
            'name'=>__('data_field_name.businesfee.fee_name')  ,
            'accounting_code'=>__('data_field_name.businesfee.accountant_code') ,
            'tracking_type'=>__('data_field_name.businesfee.type_of_tracking'),
            'note'=>__('data_field_name.businesfee.note'),
        ]);
        BusinessFee::create([
            'code'=>$request->code,
            'name'=>$request->name,
            'accounting_code'=>$request->accounting_code,
            'tracking_type'=>$request->tracking_type,
            'note'=>$request->note,
        ]);

        return redirect()->route('admin.general.businessfee.index')->with('success',__("notification.common.success.update"));
    }
}
