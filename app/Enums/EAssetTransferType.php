<?php

namespace App\Enums;

class EAssetTransferType {
    const BY_DEPARTMENT = 1;
    const BY_INDIVIDUAL = 2;

    public static function getList()
    {
        return [
            static::BY_DEPARTMENT => __('data_field_name.asset.transfer_type_department'),
            static::BY_INDIVIDUAL => __('data_field_name.asset.transfer_type_individual'),
        ];
    }

    public static function valueToName($value)
    {
        $list = static::getList();
        return ($list[$value] ?? $value);
    }
}

