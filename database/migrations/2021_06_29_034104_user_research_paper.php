<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserResearchPaper extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_research_paper', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name')->nullable()->comment('ten san pham');
            $table->string('author')->nullable()->comment('tac gia');
            $table->string('publishing_company')->nullable()->comment('nha xuat ban');
            $table->string('issn')->nullable();
            $table->dateTime('publishing_year')->nullable()->comment('nam xuat ban');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('map với user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_research_paper');
    }
}
