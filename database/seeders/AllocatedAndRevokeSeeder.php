<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\AssetAllocatedRevoke;

class AllocatedAndRevokeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssetAllocatedRevoke::factory()->count(1)->create();
    }
}
