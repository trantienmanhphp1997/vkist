<?php

namespace Database\Factories;

use App\Models\Contract;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LaborContractFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contract::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => 1,
            'user_info_id' => 2,
            'contract_code' => 'SCA1',
            'contract_type' => 1,
            'start_date' => now(),
            'end_date' => now()->addYears(1),
            'basic_salary' => 10000,
            'social_insurance_salary' => 10000,
            'bank_account_number' => 'ABCDEFGH',
            'bank_name' => 'Saccombank',
            'working_type' => 1,
            'coefficients_salary' => 2,
            'note' => 'Ghi chú',
            'admin_id' => 1,
            'status' => 1,
        ];
    }
}
