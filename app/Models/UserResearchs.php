<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class UserResearchs extends Eloquent
{
    use HasFactory;
    protected $table = "user_research";
    protected $guarded=['*'];
    protected $fillable = [
        'user_info_id',
        'topic_name',
        'role',
        'foundation',
        'budget',
        'achievement',
        'status',
        'year_research',
    ];
}
