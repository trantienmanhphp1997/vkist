<?php

namespace App\Http\Livewire\Admin\Research\Cost;

use App\Enums\ETopicFee;
use App\Enums\ETopicFeeDetail;
use App\Models\AssetCost;
use App\Models\EquipmentCost;
use App\Models\ExpertCost;
use App\Models\MaterialCost;
use App\Models\OtherCost;
use App\Models\TopicDetailWork;
use App\Models\TopicFee;
use App\Models\TopicFeeDetail;
use App\Models\TopicNormalCost;
use App\Http\Livewire\Base\BaseLive;
use App\Service\Community;
use Carbon\Carbon;

class TopicFeeDetailList extends BaseLive
{
    public $topicFeeId;
    public $name;
    public $note;
    public $idTopicFeeDetail;
    public $deleteId;
    public $type;
    public $total_capital;
    public $prescribed_expenses;
    public $total_prescribed_expense;
    public $dataTopicFee;
    public $passYears;

    public $prescribedExpenseYears = [];
    public $expenseYears = [];

    public function mount($topicFeeId)
    {
        $this->topicFeeId = $topicFeeId;
        $this->dataTopicFee = TopicFee::with('topic', 'topic.leader')->findOrFail($this->topicFeeId);
        $start_date = Carbon::parse($this->dataTopicFee->topic->start_date)->format("Y");
        $end_date = Carbon::parse($this->dataTopicFee->topic->end_date)->format("Y");
        $this->passYears = $end_date - $start_date + 1;
    }


    public function render()
    {

        $query = TopicFeeDetail::query();
        $query->where('topic_fee_id', '=', $this->topicFeeId);

        $dataTotal = [
            'total_capital' => $query->sum('total_capital'),
            'state_capital' => $query->sum('state_capital'),
            'other_capital' => $query->sum('other_capital'),
        ];

        $data = $query->orderBy('id', 'ASC')->paginate($this->perPage);
        return view('livewire.admin.research.cost.topic-fee-detail-list', ['data' => $data, 'dataTotal' => $dataTotal]);
    }

    public function store()
    {
        $this->validateData();
        TopicFeeDetail::create([
            'name' => $this->name,
            'note' => $this->note,
            'type' => ETopicFeeDetail::TYPE_NORMAL,
            'topic_fee_id' => $this->topicFeeId,
            'admin_id' => auth()->id(),
        ]);

        $this->resetInputFields();
        $this->emit('close-modal-create');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);
    }

    public function edit($idTopicFeeDetail)
    {
        $topicFeeDetail = TopicFeeDetail::findOrFail($idTopicFeeDetail);
        $this->name = $topicFeeDetail->name;
        $this->total_capital = $topicFeeDetail->total_capital;
        $this->note = $topicFeeDetail->note;
        $this->expenseYears = $topicFeeDetail->topic_fee_detail_expense['expense_years'] ?? array_pad([], $this->passYears, '');
        $this->prescribedExpenseYears = $topicFeeDetail->topic_fee_detail_expense['prescribed_expense_years'] ?? array_pad([], $this->passYears, '');
        $this->total_prescribed_expense = numberFormat((int)$topicFeeDetail->total_prescribed_expense);
        $this->type = $topicFeeDetail->type;
        $this->resetValidation();
        $this->idTopicFeeDetail = $idTopicFeeDetail;
    }

    public function updatedPrescribedExpenseYears()
    {
        $total = 0;
        foreach ($this->prescribedExpenseYears as $value) {
            $total += intval(Community::getAmount($value));
        }

        $this->total_prescribed_expense = numberFormat((int)$total);
    }
    public function updatedExpenseYears()
    {
        $subtraction = 0;
        foreach ($this->expenseYears as $key => $value) {
            if ($key == $this->passYears - 1) continue;
            $subtraction += intval(Community::getAmount($value));
        }
        $this->expenseYears[$this->passYears - 1] = numberFormat((int)$this->total_capital - $subtraction);
    }

    public function update()
    {
        $this->validate([
            'expenseYears' => 'required',
            'prescribedExpenseYears' => 'required',
            'expenseYears.*' => 'required',
            'prescribedExpenseYears.*' => 'required',
        ], [], [
            'expenseYears.*' => 'Kinh phí năm',
            'prescribedExpenseYears.*' => 'Khoán chi theo quy định',
        ]);
        for ($i = 0; $i < $this->passYears; $i++) {
            $this->prescribedExpenseYears[$i] = Community::getAmount($this->prescribedExpenseYears[$i]) ??'';
            $this->expenseYears[$i] = Community::getAmount($this->expenseYears[$i]) ??'';
        }
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->idTopicFeeDetail);
        $topicFeeDetail->name = $this->name;
        $topicFeeDetail->note = $this->note;
        $topicFeeDetail->topic_fee_detail_expense = [
            'prescribed_expense_years' => $this->prescribedExpenseYears,
            'expense_years' => $this->expenseYears,
        ];
        $total = 0;
        foreach ($this->prescribedExpenseYears as $value) {
            $total += intval(Community::getAmount($value));
        }
        $topicFeeDetail->total_prescribed_expense = $total;
        $topicFeeDetail->save();
        session()->flash('success', __("notification.common.success.update"));
        return redirect()->route('admin.research.topic-fee.edit', $this->topicFeeId);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        $topicFeeDetail = TopicFeeDetail::findorFail($this->deleteId);
        $topicFeeDetail->delete();

        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
        return true;
    }

    public function clear($idTopicFeeDetail)
    {
        $this->idTopicFeeDetail = $idTopicFeeDetail;
    }

    public function clearData()
    {
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->idTopicFeeDetail);
        $type = $topicFeeDetail->type;

        switch ($type) {
            case ETopicFeeDetail::TYPE_PEOPLE:
                TopicDetailWork::where('topic_fee_detail_id', $topicFeeDetail->id)->delete();
                break;
            case ETopicFeeDetail::TYPE_EXPERT:
                ExpertCost::where('topic_fee_detail_id', $topicFeeDetail->id)->delete();
                break;
            case ETopicFeeDetail::TYPE_MATERIAL:
                MaterialCost::where('topic_fee_detail_id', $topicFeeDetail->id)->delete();
                break;
            case ETopicFeeDetail::TYPE_EQUIPMENT:
                EquipmentCost::where('topic_fee_detail_id', $topicFeeDetail->id)->delete();
                break;
            case ETopicFeeDetail::TYPE_SHOP_REPAIR:
                AssetCost::where('topic_fee_detail_id', $topicFeeDetail->id)->delete();
                break;
            case ETopicFeeDetail::TYPE_OTHER:
                OtherCost::where('topic_fee_detail_id', $topicFeeDetail->id)->delete();
                break;
            case ETopicFeeDetail::TYPE_NORMAL:
                TopicNormalCost::where('topic_fee_detail_id', $topicFeeDetail->id)->delete();
                break;
            default:
        }

        $topicFeeDetail->total_capital = 0;
        $topicFeeDetail->ratio = 0;
        $topicFeeDetail->state_capital = 0;
        $topicFeeDetail->other_capital = 0;
        $topicFeeDetail->update();

        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }

    public function resetInputFields()
    {
        $this->name = null;
        $this->note = null;
        $this->resetValidation();
    }

    public function validateData()
    {
        $this->validate([
            'name' => 'required|max:100',
            'note' => 'nullable|max:70',
        ], [], [
            'name' => __('data_field_name.research_cost.content_expenses'),
            'note' => __('data_field_name.research_cost.note'),
        ]);
    }

    public function submitTopicFee()
    {
        $topicFee = TopicFee::findOrFail($this->topicFeeId);
        $topicFee->status = ETopicFee::STATUS_SUBMIT;
        $topicFee->save();

        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.topic_fee.submit_topic_fee')]);
    }
}
