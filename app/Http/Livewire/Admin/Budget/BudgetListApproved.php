<?php

namespace App\Http\Livewire\Admin\Budget;

use App\Enums\EBudgetStatus;
use App\Models\Budget;
use App\Models\BudgetHasExpensePlan;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Log;
use App\Component\Recursive;


class BudgetListApproved extends BaseLive
{
    public $searchTerm;
    public $budgetStatus;
    public $ids = [];
    public function mount() {
        $this->budgetStatus = [
            EBudgetStatus::NEW => __('data_field_name.budget.status_new'),
            EBudgetStatus::WAIT => __('data_field_name.budget.status_wait'),
            EBudgetStatus::DONE => __('data_field_name.budget.status_done'),
            EBudgetStatus::DENY => __('data_field_name.budget.status_deny'),
        ];
    }
    public function render()
    {
        $file = \App\Models\File::whereIn('id', $this->ids)->get();
        $query =Budget::query()->where('status',\App\Enums\EBudgetStatus::DONE);

        if(strlen($this->searchTerm)){
            $query->where('budget.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');

        }
        $data=$query->orderBy('budget.id','DESC')->paginate(25);
        return view('livewire.admin.budget.budget-list-approved', ['data'=>$data, 'file' => $file]);
    }

    public function listFile($id)
    {
        $files = \App\Models\File::where('model_id', $id)->where('model_name', Budget::class)->where('type', Config::get('common.type_upload.Budget'))->get();

        $id = [];
        foreach ($files as $file) {
            $id[] = $file->id;
        }
        $this->ids = $id;
    }

    public function download($id)
    {

            $file = \App\Models\File::where('id', $id)->get();

        foreach ($file as $files) {
            $link_file = storage_path('app/' . $files->url);

        }
        return response()->download($link_file);
    }
}
