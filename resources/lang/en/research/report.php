<?php
return [
    'title'=>'Asset report',
    'search-title' => 'Search by asset name or code',
    'select' => [
        'chose' => 'Chose',
        'type' => 'Report type',
        'department' => 'Department',
        'session' => 'Report session',
        'start' => 'From',
        'end' => 'To',
        'process' => 'Process',
        'select-report-type' => 'Select report type'
    ],
    'table' => [
        'stt' => 'STT',
        'code' => 'Asset code',
        'name' => 'Asset name',
        'department' => 'Department use',
        'user' => 'Person in charge',
        'status' => 'Status'
    ],
    'report-title' => 'List of lost and damaged properties ',
    'report-session' => [
        'year' => 'Annual report session in',
        'start' => 'From',
        'to' => 'To'
    ],
    'report1' => 'Report an increase or decrease asset',
    'report1-page' => [
        'title' => 'Report on the increase or decrease of assets',
        'sub-title' => 'Reporting period: This year, Asset management unit: Property management department.',
        'table' => [
            'code' => 'Code',
            'name' => 'Name',
            'begin-period' => 'Begin period',
            'up' => 'increase',
            'added' => 'Buy / Add New',
            'move-to' => 'Move to',
            'down' => 'decrease',
            'move-away' => 'Move away',
            'lost' => 'Losted',
            'cancel' => 'Cancel',
            'liquidation' => 'Liquidation',
            'end-period' => 'End period',
            'sum' => 'Total'
        ]
    ],
    'report2' => 'Status summary report asset',
    'report2-page' => [
        'title' => 'Asset summary report',
        'sub-title' => 'Management unit: Management Department, Statistics by: status, View details by: Subject group.',
        'table' => [
            'code' => 'Code',
            'name' => 'Name',
            'not-use' => 'Not used',
            'in-use' => 'In Using',
            'in-repaired' => 'In repaired',
            'in-maintenance' => 'In maintenance',
            'report-lost' => 'Report lost',
            'losted' => 'Losted',
            'cancel' => 'Cancel',
            'report-liquidation' => 'Report liquidation',
            'liquidation' => 'liquidation',
            'transfer' => 'In transfer',
            'recall' => 'Recall',
            'total' => 'Total'
        ],
        'total' => 'Total'
    ],
    'report3-page' => [
        'title' => 'Summary of asset movements',
        'sub-title' => 'Reporting period: Beginning of next month to present, Management unit: Asset Management Department.',
        'table' => [
            'code' => 'Code type',
            'name' => 'Name type',
            'up' => 'Number increased',
            'added' => 'Added',
            'move-to' => 'Move to',
            'move-away' => 'Move away',
            'begin-period' => 'Number begin period',
            'down' => 'Number decreased',
            'lost' => 'Losted',
            'cancel' => 'Cancel',
            'liquidation' => 'Liquidation',
            'end-period' => 'Number end period',
            'movement-statement' => 'Movement statement in use',
            'sum' => 'Total',
            'inUse' => 'In use',
            'transfer' => 'transfer use',
            'repaired' => 'Repaired',
            'maintenance' => 'Maintenance'
        ]
    ],
    'report3' => 'Asset movement summary report',
    'report4' => 'List asset by department',
    'report4-page'=>[
        'title'=>'Asset summary report',
        'sub-title'=>'Management unit: Asset Management Department, Use department: BT Research Department, Statistics by: Management Unit, View details by: Status.',
        'table'=>[
            'status'=>'Status',
            'department'=>'Department BT',
            'not-use'=>'Not using',
            'in-use'=>'In using',
            'wait-repaired'=>'Wait repaired',
            'in-repaired'=>'In repaired',
            'in-maintenance'=>'In maintenance',
            'wait-maintenance'=>'Wait maintenance',
            'report-lost'=>'repost lost',
            'losted'=>'Losted',
            'report-cancel'=>'Repost cancel',
            'cancel'=>'Cancel',
            'report-liquidation'=>'Report liquidation',
            'liquidation'=>'Liquidation',
            'transfer'=>'Transfer',
            'recall'=>'Recal',
            'total'=>'Total'
        ],
        'total'=>'Total'
    ],
    'error-search-date'=>'Start date not allow to bigger than end date'
];
