<?php

namespace App\Http\Livewire\Admin\User;

use App\Http\Livewire\Admin\User\UserInf as UserUserInf;
use App\Http\Livewire\Base\BaseLive;
use App\Models\MasterData;
use App\Models\UserInf;
use Livewire\Component;
use App\Enums\EMasterDataType;
use DB;
use Illuminate\Support\Facades\Config;
use Route;

class UserAcademicTab1 extends BaseLive
{
    public $user_info_id;
    public $standard;
    public $school;
    public $major;
    public $graduated_year;
    public $degree;
    public $deleteId1 = '';
    public $editId1 = '';
    public $canEdit = true;

    protected $listeners = [
        'set-graduated-year' => 'setGraduatedYear'
    ];

    public function resetInputFields()
    {
        $this->standard = '';
        $this->school = '';
        $this->major = '';
        $this->graduated_year = '';
        $this->degree = '';
        $this->editId1 = '';
        $this->deleteId1 = '';
    }
    protected $rule = [
        'standard' => 'required',
        'major' => 'required',
        'school' => 'required',
        'graduated_year' => 'required',
        'degree' => 'required',
    ];
    public function openModal()
    {
        $this->emit('show');
    }
    public function render()
    {
        $query1 = \App\Models\UserAcademic::where('user_info_id', $this->user_info_id)->leftJoin('master_data a', 'user_academic.standard', '=', 'a.id');
        $query1->leftJoin('master_data b', 'user_academic.degree', '=', 'b.id')->where('user_academic.type', 1);
        $query1->select('user_academic.*', DB::raw('a.v_value as standard_name'), DB::raw('b.v_value as degree_name'));

        $dataType1 = $query1->orderBy('user_academic.id', 'desc')->paginate(5);

        $degrees = MasterData::getDataByType(EMasterDataType::RANK);
        $standards = MasterData::getDataByType(EMasterDataType::EDUCATION_BACKGROUND);
        $model_name = \App\Models\UserAcademic::class;
        $type = Config::get('common.type_upload.UserAcademic_diploma');
        $model_id = $this->user_info_id;
        $folder = app($model_name)->getTable();
        if (checkShowMode()) {
            $this->canEdit = false;
        }
        $this->levelUser();
        $this->dispatchBrowserEvent('render-user-academic-tab1-event');
        return view('livewire.admin.user.user-academic-tab1', ['dataType1' => $dataType1, 'degrees' => $degrees, 'standards' => $standards, 'model_name' => $model_name, 'type' => $type, 'model_id' => $model_id, 'folder' => $folder]);
    }
    public function storeType1()
    {
        $this->validate([
            'standard' => 'required',
            'major' => ['required', 'regex:/[^.][^0-9<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:48'],
            'school' => ['required', 'regex:/[^.][^0-9<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:48'],
            'graduated_year' => 'required',
        ], [], [
            'standard' => __('data_field_name.user.level'),
            'major' => __('data_field_name.user.specialized'),
            'school' => __('data_field_name.user.school'),
            'graduated_year' => __('data_field_name.user.graduation_year'),
        ]);
        if ($this->editId1) {
            \App\Models\UserAcademic::findOrFail($this->editId1)->update([
                'user_info_id' => $this->user_info_id,
                'standard' => $this->standard,
                'major' => $this->major,
                'school' => $this->school,
                'graduated_year' => $this->graduated_year,
                'degree' => $this->degree,
                'type' => 1,
            ]);

            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.update')]);
        } else {
            \App\Models\UserAcademic::create([
                'user_info_id' => $this->user_info_id,
                'standard' => $this->standard,
                'major' => $this->major,
                'school' => $this->school,
                'graduated_year' => $this->graduated_year,
                'degree' => $this->degree,
                'type' => 1,
            ]);

            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.add')]);
        }
        $this->resetInputFields();
        $this->emit('userStore');
    }
    public function deleteId1($id)
    {
        $this->deleteId1 = $id;
    }
    public function delete1()
    {
        $data = \App\Models\UserAcademic::findOrFail($this->deleteId1);
        $data->delete();
    }
    public function editId1($id)
    {
        $this->editId1 = $id;
        $data = \App\Models\UserAcademic::findOrFail($this->editId1);
        $this->standard = $data->standard;
        $this->school = $data->school;
        $this->major = $data->major;
        $this->degree = $data->degree;
        $this->graduated_year = $data->graduated_year ? date('Y-m-d', strtotime($data->graduated_year)) : '';
    }
    public function levelUser()
    {
        $academic_level = \App\Models\UserAcademic::leftJoin('master_data', 'user_academic.standard', '=', 'master_data.id')->where('user_info_id', $this->user_info_id)->where('user_academic.type', 1)->pluck('v_value', 'master_data.id')->toArray();
        $min_order_number = array_key_first($academic_level);
        $user_academic = UserInf::findOrFail($this->user_info_id);
        $user_academic->academic_level_id = $min_order_number;
        $user_academic->save();
    }

    public function setGraduatedYear($value) {
        $this->graduated_year = date('Y-m-d', strtotime($value['graduated-year']));
    }
}
