<div class="advanced-select dropdown show position-relative" tabindex="0">
    <div data-toggle="dropdown" class="selected-items border d-flex flex-wrap align-items-center px-3 {{ $disabled ? 'bg-light' : 'bg-white' }}" style="border-radius: 12px; min-height: 48px; box-sizing: border-box; cursor: pointer;">
        @forelse ($selectedItems as $id => $item)
            @if ($multiple)
                <div class="d-inline-flex align-items-center p-2 rounded m-2 {{ $disabled ? 'bg-white' : 'bg-light' }}" style="max-width: 100%;">
                    <div class="text-truncate">
                        {!! renderFromTemplate($selectedItemTemplate, $item) !!}
                    </div>
                    @if (!$disabled)
                        <button type="button" class="btn" onclick="event.stopPropagation()" wire:click="deselectItem({{ $id }})">
                            <i class="fa fa-times"></i>
                        </button>
                    @endif
                </div>
            @else
                <div class="text-truncate">{!! renderFromTemplate($selectedItemTemplate, $item) !!}</div>
            @endif
        @empty
            <div wire:loading.remove>{{ $placeholder }}</div>
        @endforelse

        <div wire:loading>
            <em class="fa fa-spinner fa-pulse"></em>
        </div>

    </div>

    @if (!$disabled)
        <div class="select-container rounded bg-white dropdown-menu w-100 p-0" wire:ignore.self style="box-shadow: 0px 0px 20px rgb(199, 199, 199);">
            <div class="d-flex">
                <input type="text" class="keyword w-100 border-0 px-3 rounded" style="outline: none;" wire:model.debounce.1000ms="keyword" placeholder="{{ __('data_field_name.common_field.search_by_keyword') }}" {{ $disabled ? 'readonly' : '' }}>
                <button type="button" class="btn"><em class="fa fa-times"></em></button>
            </div>

            <div class="w-100" style="max-height: 300px; overflow-y: auto;">
                @foreach ($selectedItems as $id => $item)
                    <div style="cursor: pointer;" class="w-100 bg-primary text-white p-2 border-top" wire:click="deselectItem({{ $id }})" autofocus="true">
                        {!! renderFromTemplate($itemTemplate, $item) !!}
                    </div>
                @endforeach

                @foreach ($data as $id => $item)
                    @if (!isset($selectedItems[$id]))
                        <div
                            style="cursor: pointer;"
                            class="w-100 p-2 border-top"
                            wire:click="selectItem({{ $id }})"
                        >
                            {!! renderFromTemplate($itemTemplate, $item) !!}
                        </div>
                    @endif
                @endforeach
            </div>

            @if (count($data) == 0)
                <div class="text-danger text-center py-2">{{ __('notification.common.fail.no-data') }}</div>
            @else
                <button class="btn btn-light btn-block" type="button" wire:click="loadMore" onclick="event.stopPropagation();">...</button>
            @endif
        </div>
    @endif
</div>
