<div class="modal fade modal-fix-1" wire:ignore.self id="report_transfer_series" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header mb-32">
                <h6>{{__('data_field_name.asset.report_transfer_and_move')}}</h6>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row justify-content-md-center">
                        <div class="row" style="height:100px;z-index: 3">
                            <div class="col-3 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.mover_name')}}</label>
                                    @livewire('component.search-user',['name'=>'manager_id'])

                                </div>
                            </div>
                            <div class="col-3 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.mover_code')}}</label>
                                    <input type="text" wire:model.lazy="manager_code" disabled>
                                </div>
                            </div>
                            <div class="col-3 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.receiver_name')}} <span class="text-danger">*</span></label>
                                    @livewire('component.search-user',['name'=>'receiver_id'])
                                    @error('receiver_id')
                                    <span class="error text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-3 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.receiver_code')}}</label>
                                    <input type="text" wire:model.lazy="receiver_code" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pd-col">
                            <div class="form-group info-request">
                                <label>{{__('data_field_name.asset.recall_date')}} <span class="text-danger">*</span></label>
                                <input type="date" wire:model.lazy="claim_date">
                                @error('claim_date')
                                <span class="error text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-9 pd-col">
                            <div class="form-group info-request">
                                <label>{{__('data_field_name.asset.recall_reason')}}</label>
                                <textarea wire:model.lazy="reason" cols="30" rows="10"></textarea>
                                @error('reason')
                                <span class="error text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 pd-col">
                            <div class="info-asset bg-white p-0">
                                <table class="table table-general">
                                    <thead>
                                    <tr class="border-radius">
                                        <th scope="col" class="border-radius-left text-left w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_code_')}}</th>
                                        <th scope="col" class="w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_name')}}</th>
                                        <th scope="col" class="w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_cate')}}</th>
                                        <th scope="col" class="w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_quantity')}}</th>
                                        <th scope="col" class="w-15 ts-11 text-uppercase">{{__('data_field_name.asset.department_management')}}</th>
                                        <th scope="col" class="w-15 ts-11 text-uppercase">{{__('data_field_name.asset.user_asset')}}</th>
                                        <th scope="col" class="w-10 ts-11 text-uppercase">{{__('data_field_name.asset.asset_position')}}</th>
                                        <th scope="col" class="border-radius-right text-center w-10 ts-11 text-uppercase">{{__('data_field_name.asset.situation')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataForms as $dataForm)
                                        <tr>
                                            <td>{{$dataForm?$dataForm->code:''}}</td>
                                            <td>{{$dataForm?$dataForm->name:''}}</td>
                                            <td>{{$dataForm?$dataForm->cate_name:''}}</td>
                                            <td>{{$dataForm?$dataForm->quantity:''}}</td>
                                            <td>{{$dataForm?$dataForm->depart_name:''}}</td>
                                            <td>{{$dataForm?$dataForm->fullname:''}}</td>
                                            <td>{{$dataForm?$dataForm->depart_name:''}}</td>
                                            <td><span class="badge badge-fix badge-color-1">
                                                    {{$assetStatus[$dataForm?$dataForm->situation:-1]??''}}
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" id="close-transfer-series"  wire:click.prevent="cancel()" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="saveForm(4,false)">{{__('common.button.perform')}}</button>
            </div>
        </div>
    </div>
</div>
