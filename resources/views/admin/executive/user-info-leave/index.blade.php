@extends('layouts.master')
<title>{{__('executive/user-info-leave.breadcrumbs.user-info-leave-management')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.executive.user-info-leave.user-info-leave-list')
@endsection