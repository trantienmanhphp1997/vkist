<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropUserPerformIdColumnFromSurveyRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_request', function (Blueprint $table) {
            $table->dropColumn('user_perform_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_request', function (Blueprint $table) {
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->nullOnDelete();
        });
    }
}
