<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ideal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ideal', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('danh sach y tuong');
            $table->string('code', 255)->comment('ma y tuong');
            $table->string('name', 500)->nullable()->comment('ten y tuong');
            $table->string('proposer', 48)->comment('nguoi de xuat');
            $table->string('agencies', 48)->nullable()->comment('Co quan truong lop don vi');
            $table->string('necessity', 1000)->nullable()->comment('Tính cấp thiết của đề tài');
            $table->string('target', 1000)->nullable()->comment('Muc tieu cu the cua de tai');
            $table->string('content', 1000)->nullable()->comment('noi dung nghien cuu');
            $table->string('result', 1000)->nullable()->comment('ket qua mong muon');
            $table->string('execution_time', 255)->nullable()->comment('thoi gian thuc hien');
            $table->bigInteger('fee')->nullable()->comment('kinh phi du tinh');
            $table->tinyInteger('status')->nullable()->comment('0 Cho tham dinh, 1 duoc phe duyet, 2 Tu choi');
            $table->bigInteger('admin_id')->comment('Nguoi tao');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ideal');
    }
}
