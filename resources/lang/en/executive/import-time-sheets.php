<?php
return [
    'template-time-sheet' => 'Timesheet import sample file',
     'title' => 'Time tracking data',
     'btn-add' => 'Add time tracking data',
     'result' => 'Result',
     'download_error_list_file' => 'Download error list file',
     'table_column' => [
         'name' => 'Name of the general occipital table',
         'duration' => 'Time',

         'title' => [
             'create' => 'Create new ideas',
             'edit' => 'Edit',
             'detail' => 'Detailed idea'
         ],
     ],
];