<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\UserInf;
use App\Models\WorkPlan;
use Illuminate\Foundation\Testing\WithFaker;
use PhpParser\Node\Expr\FuncCall;
use Tests\TestCase;

class WorkPlanControllerTest extends TestCase
{

     use WithFaker;

     public $routeIndex = 'admin.general.work-plan.index';
     public $routeEdit = 'admin.general.work-plan.edit';
     public $routeCreate = 'admin.general.work-plan.create';

    /** @test */
    public function testIndexWorkPlan()
    {

        $user = $this->user;
        $this->actingAs($user)->get(route($this->routeIndex))->assertOk();
    }

    public function testStoreWorkPlanSuccess()
    {
        $data = [
            'save'=> 'save',
            'content' => 'sfsdfsf',
            'mission_id' => '2',
            'missions' => 'demo test',
            'missions_code' => 'demo',
            'start_date' => '01-JUL-21',
            'end_date' => '10-JUL-21',
            'status' => '1',
            'admin_id' => '6',
            'leader_id' => '',
            'estimated_cost' => '696961',
        ];
        //
        $this->actingAsAdmin()->get(route($this->routeCreate))->assertOk();

        $this->actingAsAdmin()
            ->post(route('admin.general.work-plan.store'), $data)
            // ->assertRedirect(route('admin.general.work-plan.index'))
            ->assertSessionDoesntHaveErrors();
    }
    public function testStoreWorkPlanFail()
    {
        $data = [
            'save'=> 'save',
            'content' => 'ấdaga',
            'mission_id' => '2',
            'missions' => '',
            'missions_code' => 'vzbzx',
            'start_date' => '05-JUL-20',
            'end_date' => '16-JUL-20',
            'status' => '',
            'admin_id' => '12',
            'leader_id' => '',
            'estimated_cost' => '',
        ];

        $this->actingAsAdmin()->get(route($this->routeCreate))->assertOk();

        $this->actingAsAdmin()
            ->post(route('admin.general.work-plan.store'), $data)
             ->assertRedirect(route($this->routeCreate))
            // ->assertSessionDoesntHaveErrors();
            ->assertSessionHasErrors(['missions']);
    }

    public function testUpdateWorkPlanSuccess()
    {
        $data = [
            'save'=> 'save',
            'content' => 'sfsdfsf',
            'missions' => 'demo test 1',
            'missions_code' => 'demo1',
            'start_date' => '02-JUL-21',
            'end_date' => '11-JUL-21',
            'status' => '1',
            'admin_id' => '6',
            'leader_id' => '',
            'estimated_cost' => '696962',
        ];

        $workPlan = WorkPlan::create($data);
        $this->actingAsAdmin()->get(route($this->routeEdit, ['id' => $workPlan->id]))->assertOk();

        $data['missions'] = 'testtest';

        $this->actingAsAdmin()
            ->post(route('admin.general.work-plan.update', ['id' => $workPlan->id]), $data)
            ->assertRedirect(route($this->routeIndex));
    }
    public function testUpdateWorkPlanFail() {
        $data = [
            'save'=> 'save',
            'content' => 'sfsdfsf',
            'missions' => 'demo test 2',
            'missions_code' => 'demo2',
            'start_date' => '02-JUL-21',
            'end_date' => '12-JUL-21',
            'status' => '1',
            'admin_id' => '6',
            'leader_id' => '',
            'estimated_cost' => '696963',
        ];

        $workPlan = WorkPlan::create($data);
        $this->actingAsAdmin()->get(route($this->routeEdit, ['id' => $workPlan->id]))->assertOk();

        $data['start_date'] = '';

        $this->actingAsAdmin()
            ->post(route('admin.general.work-plan.update', ['id' => $workPlan->id]), $data)
            ->assertRedirect(route($this->routeEdit, ['id' => $workPlan->id]))
            ->assertSessionHasErrors([ 'start_date']);
    }
}
