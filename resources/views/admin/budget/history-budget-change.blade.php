@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.budget.history_change')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a>{{__('data_field_name.reality_expense.budget_control')}} </a> / <a
                        href="">{{__('data_field_name.budget.update')}} </a> /
                    <span>{{__('data_field_name.budget.change')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="detail-task information box-idea">
                    <h4 class="border-bottom-header">{{__('data_field_name.budget.change')}}</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.pick_year')}}</label>
                                        <input type="number" readonly id="datePicker" class="form-control " name="year_created" value="{{$data->year_created ?? '' }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.estimate')}}</label>
                                        <input type="text" readonly class="form-control" placeholder="" name="estimated_code" value="{{$data->estimated_code ?? ''}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.code')}}</label>
                                        <input type="text"  readonly class="form-control" placeholder="" name="code" value="{{$data->code ?? ''}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.name')}}</label>
                                <input type="text" readonly class="form-control" placeholder="" name="name" value="{{$data->name ?? ''}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.total')}}</label>
                                        <input readonly type="text" class="form-control format_number" placeholder="" name="total_budget"
                                               value="{{numberFormat($data->total_budget) ?? ''}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.number_change')}}</label>
                                        <input type="number" readonly class="form-control" placeholder="" name="number_change" value="{{$data->version ?? ''}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.content')}}</label>
                                <input type="text" readonly class="form-control" placeholder="" name="content" value="{{ $data->content ?? ''}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.note')}}</label>
                                <textarea readonly class="form-control" rows="4" name="note">{{$data->note ?? '' }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 d-none">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.note')}}</label>
                                <input readonly class="form-control" id="input-draft" name="draft" value="0" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="title-plan"> {{__('data_field_name.budget.plan_spending')}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-calendar col-md-12 bg-white">
                        @if($data->allocated == 1)
                            <div class="row">
                                <div class="col-md-2 mt-16">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.plan-before')}}  </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.plan-after')}}  </label>
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="form-group">
                                        <label >{{__('data_field_name.budget.rate_difference')}} </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.bill')}}  </label>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($data->allocated == 1)
                            @foreach ($auditBudgetMoneyPlan as $key => $val)

                                <div class="row">
                                    <div class="col-md-2 mt-16">
                                        <div class="form-group">
                                            <label>{{__('data_field_name.budget.plan')}} {{__('data_field_name.budget.month')}} {{$val['month']}} </label>
                                        </div>

                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control format_number "
                                                   placeholder="{{$val['old_value']?? ''}}"
                                                   value="{{$val['old_value'] ?? ''}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control format_number bg-default disabled_box"
                                                   placeholder="{{$val['new_value'] ?? ''}}"
                                                   value="{{$val['new_value']??''}}"  readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-16">
                                        @if(round($val['change']) > 0)
                                            +
                                        @endif
                                        {{round($val['change'])}} %
                                    </div>
                                    <div class="col-md-4">
                                        {{--                                                    <label>{{__('common.file')}}</label>--}}
                                        {{--                                    @if($val->file)--}}
                                        @if(trim($val['id']) != null)
                                            @livewire('component.files', ['model_name'=>$model_name,'type'=>$type,'disabled'=>true,
                                            'folder'=>$folder, 'model_id'=>$val['id']])
                                        @endif

                                        {{--                                    @endif--}}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.plan-before')}}</label>
                                        <input type="text" class="form-control format_number"
                                               placeholder="" value="{{numberFormat($data->old_value) ?? 0}}"
                                               disabled
                                               name="total_real_budget">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.plan-after')}} </label>
                                        <input type="text" class="form-control format_number"
                                               disabled
                                               placeholder="" value="{{numberFormat($data->new_value) ?? 0}}"
                                               name="total_real_budget">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.rate_difference')}} </label>
                                        <input type="text" class="form-control format_number"
                                               disabled
                                               placeholder="" value="{{numberFormat($data->change_value) .'%' ?? 0}}"
                                               name="total_real_budget">
                                    </div>
                                </div>
                                <div class="col-md-3 mt-32">
                                    @livewire('component.files', ['model_name'=>$model_name,'type'=>$type,'disabled'=>true,
                                    'folder'=>$folder,'model_id'=>$data->id,'status'=>-1])
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="group-btn2 text-center group-btn ">
                            <a href="{{route('admin.budget.history-budget.index')}}">
                                <button type="button" class="btn btn-cancel"
                                        name="cancel">{{__('common.button.close')}} </button>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
