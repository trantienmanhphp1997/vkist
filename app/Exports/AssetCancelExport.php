<?php

namespace App\Exports;

use App\Enums\EAssetStatus;
use App\Models\AssetAllocatedRevoke;
use App\Models\Department;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class AssetCancelExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $searchName;

    public function __construct($searchName = '')
    {
        $this->searchName = $searchName;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $index = 1;
        $query = AssetAllocatedRevoke::where('status',\App\Enums\EAssetStatus::REPORT_CANCEL)
            ->orWhere('status',\App\Enums\EAssetStatus::CANCELLED)
            ->with('asset');
        if($this->searchName){
            $query->whereHas('asset',function(Builder $q){
                $q->where('asset.unsign_text','like','%'. strtolower(removeStringUtf8($this->searchName)).'%');

            });
        }

        $assetCancels = $query->get();
        foreach ($assetCancels as $assetCancel) {
            $assetCancel->index = $index++;
        }
        return $assetCancels;
    }

    public function headings(): array
    {
        return [
            'STT',
            'MÃ tài sản',
            'Số biên bản',
            'Tên tài sản',
            'Loại tài sản',
            'Số lượng',
            'Số serial',
            'Tình trạng',
            'Ngày báo hủy'
        ];
    }

    public function map($assetCancel): array
    {
        return [
            $assetCancel->index,
            $assetCancel->asset->code ?? '',
            $assetCancel->number_report ?? '',
            $assetCancel->asset->name ?? '',
            $assetCancel->asset->category->name ?? '',
            $assetCancel->implementation_quantity ?? '',
            $assetCancel->asset->series_number ?? '',
            $assetCancel->status == \App\Enums\EAssetStatus::CANCELLED ? 'Đã hủy':'Báo hủy',
            reFormatDate($assetCancel->implementation_date) ?? '',

        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E','F','G','H','I'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:E1');
            $event->sheet->setCellValue('A1', 'DANH SÁCH TÀI SẢN HỦY');
            $event->sheet->getStyle('A1:E1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:I3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        }, ];

    }
    public function title(): string
    {
        return 'DANH SÁCH PHÒNG HỦY';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
