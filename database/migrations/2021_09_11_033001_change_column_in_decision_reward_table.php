<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnInDecisionRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('decision_reward', function (Blueprint $table) {
            //
            $table->dropColumn('type_reward');
            $table->dropColumn('base');
        });
        Schema::table('decision_reward', function (Blueprint $table) {
            //
            $table->string('type_reward', 500)->nullable()->comment('Loai khen thuong');
            $table->string('base', 500)->nullable()->comment('Can cu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('decision_reward', function (Blueprint $table) {
            //
            $table->dropColumn('type_reward');
            $table->dropColumn('base');
        });
        Schema::table('decision_reward', function (Blueprint $table) {
            //
            $table->string('type_reward', 255)->nullable()->comment('Loai khen thuong');
            $table->string('base', 255)->nullable()->comment('Can cu');
        });
    }
}
