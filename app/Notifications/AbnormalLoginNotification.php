<?php

namespace App\Notifications;

use App\Enums\ENotificationType;

class AbnormalLoginNotification extends BaseNotification
{
    public function __construct()
    {
        $this->type = ENotificationType::ABNORMAL_LOGIN;
    }

    protected function message($notifiable)
    {
        return [
            'template_name' => 'notification.user.abnormal_login'
        ];
    }
}
