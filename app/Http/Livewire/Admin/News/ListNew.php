<?php

namespace App\Http\Livewire\Admin\News;

use App\Http\Livewire\Base\BaseLive;
use App\Models\CategoryNews;
use Illuminate\Support\Facades\Config;

class ListNew extends BaseLive
{
    public $categorySlug ;
    protected $listeners = ['addTip'];
    public $newPerpage = 10 ;
    public function mount($categorySlug){
        $this->categorySlug = $categorySlug;
    }
    public function render()
    {
        $query =  \App\Models\News::query()
            ->where(['status'=>\App\Models\News::APPROVED_NEWS])
            ->where(['published'=>1]);
        if (trim($this->categorySlug)){
            $categoryID = CategoryNews::where(['slug'=>$this->categorySlug])->first()->id;
            $query->where(['category_news_id'=>$categoryID]);
        }
        if (trim($this->searchTerm)){
            $query->where('unsign_text','like','%'. strtolower(removeStringUtf8($this->searchTerm)).'%');
        }
        $news = $query->orderBy('id','DESC')
            ->paginate($this->newPerpage);

        return view('livewire.admin.news.list-new',['news'=>$news]);
    }

    public function getDetail($slug,$categorySlug){
        if (!$categorySlug){
            $categorySlug = 'unkhown';
        }
        return redirect(route('news.detail',['slug'=>$slug,'categorySlug'=>$categorySlug]));
    }

    public function addTip($value)
    {
        $this->newPerpage += 10;
    }

}
