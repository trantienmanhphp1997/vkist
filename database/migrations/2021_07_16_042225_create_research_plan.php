<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_plan', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('danh sach ke hoach nghien cuu');
            $table->tinyInteger('status')->nullable()->comment('0 Tu choi, 1 Phe duyet, 2 Chua phe duyet,3 Cho tham dinh');
            $table->string('name', 500)->nullable()->comment('ten ke hoach');
            $table->string('mission_name', 100)->nullable()->comment('ten nhiem vu');
            $table->tinyInteger('type')->nullable()->comment('0 Nhiem vu chinh,1 Nhiem vu con');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();     
            $table->softDeletes();      
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_plan');
    }
}
