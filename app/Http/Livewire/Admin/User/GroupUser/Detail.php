<?php

namespace App\Http\Livewire\Admin\User\GroupUser;

use App\Http\Livewire\Base\BaseLive;
use App\Models\GroupUser;
use App\Models\GroupUserHasTopic;
use App\Models\Topic;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class Detail extends BaseLive
{
    public $topicSelectedList = [];
    public $name ;
    public $code ;
    public $description ;
    public $groupUserId ;

    public $currentTab = 'group-user-tab';
    public function setCurrentTab($tab)
    {
        $this->currentTab = $tab;
    }

    public function mount($id = 0){
        if ($id){
            $this->groupUserId = $id;
            $groupUser = GroupUser::findOrFail($id);
            $this->name = $groupUser->name ;
            $this->code = $groupUser->code ;
            $this->description = $groupUser->description;

            $groupUserHasTopic = $this->getGroupUserHasTopicByGroupId($id);
            foreach ($groupUserHasTopic as $item){
                $this->topicSelectedList[] = $item->topic_id ;
            }
        }
    }
    public function getGroupUserHasTopicByGroupId($groupId){
        return GroupUserHasTopic::query()->where(['group_user_id'=>$groupId])->get();
    }

    public function render()
    {
        $topics = Topic::query()->with(['groupUser'])->whereHas('groupUser', function (Builder $query) {
            return $query->whereIn('topic_id',$this->topicSelectedList);
        })->paginate($this->perPage);

        return view('livewire.admin.user.group-user.detail',compact('topics'));
    }

    public function back(){
        $this->redirect(route('admin.system.group-user.index'));
    }
}
