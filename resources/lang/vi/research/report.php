<?php
return [
    'title'=>'Báo cáo tài sản',
    'search-title'=>'Tìm kiếm tên hoặc mã',
    'select'=>[
        'chose'=>'Chọn',
        'type'=>'Loại báo cáo',
        'department'=>'Đơn vị',
        'session'=>'Kỳ báo cáo',
        'start'=>'Từ',
        'end'=>'Đến',
        'process'=>'Thực hiện',
        'select-report-type'=>'Chọn loại báo cáo'
    ],
    'table'=>[
        'stt'=>'STT',
        'code'=>'Mã tài sản',
        'name'=>'Tên tài sản',
        'department'=>'Phòng sử dụng',
        'user'=>'Người phụ trách',
        'status'=>'Tình trạng'
    ],
    'report-title'=>'Danh sách tài sản mất hỏng',
    'report-session'=>[
        'year'=>'Kỳ báo cáo năm' ,
        'start'=>'thời gian từ',
        'to'=>'Đến'
    ],
    'report1'=>'Báo cáo tình trạng tăng giảm tài sản',
    'report1-page'=>[
      'title'=>'Báo cáo tình hình tăng, giảm tài sản',
        'sub-title'=>'Kỳ báo cáo: Năm nay, Đơn vị quản lý tài sản: Phòng quản lý tài sản.',
        'table'=>[
            'code'=>'Mã',
            'name'=>'Tên',
            'begin-period'=>'Tồn đầu kì',
            'up'=>'Tăng',
            'added'=>'Mua / Thêm mới',
            'move-to'=>'Điều chuyển đến',
            'down'=>'Giảm',
            'move-away'=>'Điều chuyển đi',
            'lost'=>'Đã mất',
            'cancel'=>'Đã hủy',
            'liquidation'=>'Đã thanh lý',
            'end-period'=>'Tồn cuối kì',
            'sum'=>'Tổng cộng'
        ]
    ],
    'report2'=>'Báo cáo tổng hợp tình trạng tài sản',
    'report2-page'=>[
        'title'=>'Báo cáo tổng hợp tài sản',
        'sub-title'=>'Đơn vị quản lý: Phòng QLTS, Thống kê theo: tình trạng, Xem chi iết theo: Nhóm đề tài.',
        'table'=>[
            'code'=>'Mã',
            'name'=>'Tên',
            'not-use'=>'Chưa sử dụng',
            'in-use'=>'Đang sử dụng',
            'in-repaired'=>'Đang sửa chữa',
            'in-maintenance'=>'Đang bảo dưỡng',
            'report-lost'=>'Báo mất',
            'losted'=>'Đã mất',
            'cancel'=>'Đã hủy',
            'report-liquidation'=>'Đề nghị thanh lý',
            'liquidation'=>'Đã thanh lý',
            'transfer'=>'Đang điều chuyển',
            'recall'=>'Đề nghị thu hồi',
            'total'=>'Tổng số lượng'
        ],
        'total'=>'Tổng cộng'
    ],
    'report3-page'=>[
        'title'=>'Tổng hợp biến động tài sản',
        'sub-title'=>'Kỳ báo cáo: Đầu tháng tới hiện tại, Đơn vị quản lý: Phòng QLTS.',
        'table'=>[
            'code'=>'Mã loại',
            'name'=>'Tên loại',
            'up'=>'SL tăng',
            'added'=>'Thêm mới',
            'move-to'=>'Điều chuyển đến',
            'down'=>'SL giảm',
            'lost'=>'Đã mất',
            'begin-period'=>'SL đầu kì',
            'move-away'=>'Điều chuyển đi',
            'cancel'=>'Đã hủy',
            'liquidation'=>'Đã thanh lý',
            'end-period'=>'SL cuối kì',
            'movement-statement'=>'Biến động tình hình sử dụng',
            'sum'=>'Tổng cộng',
            'inUse'=>'Đưa vào sử dụng',
            'transfer'=>'Thay đổi NSD',
            'repaired'=>'Sửa chữa',
            'maintenance'=>'Bảo dưỡng'
        ]
    ],
    'report3'=>'Báo cáo tổng hợp biến động tài sản',
    'report4'=>'Danh sách tài sản theo đơn vị',
    'report4-page'=>[
        'title'=>'Báo cáo tổng hợp tài sản',
        'sub-title'=>'Đơn vị quản lý: Phòng QLTS, Đơn vị sử dụng: Phòng nghiên cứu BT, Thống kê theo: Đơn vị quản lý, Xem chi tiết theo: Tình trạng.',
        'table'=>[
            'status'=>'Tình trạng',
            'department'=>'Phòng nghiên cứu BT',
            'not-use'=>'Chưa sử dụng',
            'in-use'=>'Đang sử dụng',
            'wait-repaired'=>'Chờ sửa chữa',
            'in-repaired'=>'Đang sửa chữa',
            'in-maintenance'=>'Đang bảo dưỡng',
            'wait-maintenance'=>'Chờ bảo dưỡng',
            'report-lost'=>'Báo mất',
            'losted'=>'Đã mất',
            'report-cancel'=>'Báo hủy',
            'cancel'=>'Đã hủy',
            'report-liquidation'=>'Đề nghị thanh lý',
            'liquidation'=>'Đã thanh lý',
            'transfer'=>'Đang điều chuyển',
            'recall'=>'Đề nghị thu hồi',
            'total'=>'Tổng số lượng'
        ],
        'total'=>'Tổng cộng'
    ],
    'error-search-date'=>'Ngày bắt đầu không được phép lớn hơn ngày kết thúc'
];
