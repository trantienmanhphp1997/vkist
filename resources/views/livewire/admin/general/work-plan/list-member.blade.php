<div class="col-md-12">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <h4 class="title-plan text-uppercase">{{__('data_field_name.work_plan.create.title_item')}}</h4>
            </div>
            <div class="col-md-6">
                <div class="form-group float-right">
                    <button class="btn btn-viewmore-news mr0 " wire:click.prevent="storeMember()"><img
                            src="/images/plus2.svg" alt="plus"> {{__('data_field_name.work_plan.create_btn')}} </button>
                </div>
            </div>
        </div>
    </div>
    <div class="box-calendar col-md-12">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>{{__('data_field_name.work_plan.create.name')}}</label>
                    <select class="form-control form-control-lg" wire:model.lazy="user_info_id">
                        <option value="">{{__('data_field_name.work_plan.create.selected_default')}}</option>
                        @foreach($users as $key => $val)
                            <option value="{{$key}}">{{$val}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>{{__('data_field_name.work_plan.create.department')}}</label>
                    <select class="form-control form-control-lg" disabled>
                        <option>{{__('data_field_name.work_plan.create.selected_default')}}</option>
                        @foreach($departmentUser as $key => $val)
                            <option value="{{$key}}" {{($departmentUser) ?'selected' :''}}>{{$val}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>{{__('data_field_name.work_plan.create.mission')}}</label>
                    <select class="form-control form-control-lg" wire:model.lazy="position_id">
                        <option value="">{{__('data_field_name.work_plan.create.selected_default')}}</option>
                        @foreach($position as $key => $val)
                            <option value="{{$key}}">{{$val}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    @foreach($list_members as $item)
        <div class="box-calendar col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.work_plan.create.name')}}</label>
                        <select class="form-control form-control-lg">
                            @foreach($userAll as $key => $val)
                                <option
                                    value="{{$key}}" {{($key==$item->id) ?'selected' :''}}>{{$val}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.work_plan.create.department')}}</label>
                        <select class="form-control form-control-lg" disabled>
                            @foreach($departmentAll as $key => $val)
                                <option
                                    value="{{$key}}" {{($key==$item->department_id) ?'selected' :''}}>{{$val}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.work_plan.create.mission')}}</label>
                        <select class="form-control form-control-lg" disabled>
                            @foreach($position as $key => $val)
                                <option
                                    value="{{$key}}" {{($key==$item->position_id) ?'selected' :''}}>{{$val}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
