<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyRequestHasUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_request_has_user_info', function (Blueprint $table) {
            $table->id();
            $table->foreignId('survey_request_id')->nullable()->constrained('survey_request')->nullOnDelete();
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_request_has_user_info');
    }
}
