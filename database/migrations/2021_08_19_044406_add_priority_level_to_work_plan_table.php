<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriorityLevelToWorkPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_plan', function (Blueprint $table) {
            $table->tinyInteger('priority_level')->nullable()->comment('Muc uu tien');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_plan', function (Blueprint $table) {
            $table->dropColumn('priority_level');
        });
    }
}
