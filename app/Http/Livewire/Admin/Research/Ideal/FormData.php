<?php

namespace App\Http\Livewire\Admin\Research\Ideal;

use App\Models\Department;
use App\Models\User;
use App\Service\Community;
use App\Models\Ideal;
use App\Models\UserInf;
use App\Http\Livewire\Base\BaseTrimString;
use App\Enums\EIdeaStatus;
use Ideal as GlobalIdeal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;

class FormData extends BaseTrimString
{
    public $tab_basic_info = true;
    public $tab_detail_info = false;
    public $ideal;
    public $name;
    public $code;
    public $proposer;
    public $research_field;
    public $agencies;
    public $necessity;
    public $target;
    public $content;
    public $result;
    public $execution_time;
    public $fee;
    public $edit = true;
    public $disabled_save = false;
    public $show_detail = false;
    public $user;
    public $model;
    public $type_upload;
    public $idealId;

    protected $listeners = [
        'setResearchField'
    ];

    public function mount() {
        $this->user = UserInf::get();
        $this->proposer = !empty(auth()->user()->user_info_id) ? auth()->user()->user_info_id : $this->proposer;
        $this->type_upload = Config::get('common.type_upload.Ideal');
        $this->model = Ideal::class;
    }

    public function render() {
        $this->disabled_save = true;
    	if (!empty($this->ideal) && $this->edit) {
    		$this->name = $this->ideal->name;
	        $this->proposer = $this->ideal->proposer;
	        $this->agencies = $this->ideal->agencies;
	        $this->necessity = $this->ideal->necessity;
	        $this->target = $this->ideal->target;
	        $this->content = $this->ideal->content;
	        $this->result = $this->ideal->result;
            $this->execution_time = $this->ideal->execution_time;
            $this->research_field = $this->ideal->researchField->id;
            $this->fee = Community::getAmount($this->ideal->fee);
            $this->idealId = $this->ideal->id;
    		$this->edit = false;
    	}

        if (!empty($this->fee) && is_numeric($this->fee)) {
                $this->fee = number_format(str_replace('.', '', $this->fee), 0, ',', ',');
        }

        $para = ['name', 'proposer', 'agencies', 'necessity', 'target', 'content', 'result', 'execution_time', 'fee'];
        foreach ($para as $item) {
            if (!empty($this->$item)) {
                $this->disabled_save = false;
            }
        }
        return view('livewire.admin.research.ideal.form-data');
    }

    public function saveIdeal() {
        if (empty($this->name) || empty($this->proposer) || empty($this->agencies)) {
            $this->selectTab(true, false);
        } elseif (empty($this->content) || empty($this->research) || empty($this->execution_time)) {
            $this->selectTab(false, true);
        }
        if (!empty($this->fee)) {
            $this->fee = Community::getAmount($this->fee);
        }
        $this->validateData();
        $lastIdeal = Ideal::orderBy('id', 'desc')->first();
        $lastId = ($lastIdeal->id ?? 0) + 1;
        if (!empty($this->ideal)) {
        	$data = Ideal::findOrFail($this->ideal->id);
        } else {
        	$data = new Ideal;

        	//anhta add
            //thay doi ma y tuong
        	$userInfo = UserInf::query()->where(['user_id'=>Auth::id()])->first();
        	if ($userInfo){
        	    $departmentID = $userInfo->department_id ?? '' ;
        	    $departmentCode = Department::findOrFail($departmentID)->code ?? null;

        	    if ($departmentCode){
                    $data->code = ($departmentCode).str_replace('-','',date('d-m-Y')). $lastId ;
                }else{
                    $data->code = str_replace('-','',date('d-m-Y')) . $lastId;
                }
            }else{
                $data->code = 'ID' . $lastId;
            }
        	//end of anhta
//	        if (empty($lastIdeal)) {
//	            $data->code = 'ID001';
//	        } elseif ($lastIdeal->id < 9) {
//	            $data->code = 'ID00' . ($lastIdeal->id + 1);
//	        } elseif ($lastIdeal->id < 99) {
//	            $data->code = 'ID0' . ($lastIdeal->id + 1);
//	        } else {
//	            $data->code = 'ID' . ($lastIdeal->id + 1);
//	        }
        }
    	$data->name = $this->name;
    	$data->proposer = $this->proposer;
    	$data->agencies = $this->agencies;
    	$data->necessity = $this->necessity;
    	$data->target = $this->target;
        $data->content = $this->content;
        $data->result = $this->result;
        $data->execution_time = $this->execution_time;
        $data->research_field_id = $this->research_field;
        $data->fee = $this->fee;
        $data->status = EIdeaStatus::JUST_CREATED;
        $data->save();
        \App\Models\File::where('model_name', Ideal::class)
                ->where('type', $this->type_upload)->where('admin_id', auth()->id())->where('model_id', null)->update([
                    'model_id' => $data->id,
        ]);

        if (!empty($this->ideal)) {
            session()->flash('success', __('notification.common.success.update'));
        } else {
            session()->flash('success', __('notification.common.success.add'));
        }
        return redirect()->route('admin.research.ideal.index');
    }

    public function validateData() {
        $this->validate([
            'name' => ['required', 'max:225', 'regex:/[^.][^!@#$%^&*(){}\[\]\'\"\/\|]+$/'],
            'proposer' => ['required'],
            'agencies' => ['required', 'max:48'],
            'target' => ['nullable', 'max:1000'],
            'necessity' => ['nullable', 'max:1000'],

            'content' => ['required', 'max:1000'],
            'result' => ['required', 'max:1000'],
            'research_field' => 'required',
            'execution_time' => ['required', 'max:225', 'regex:/[^.][^!@#$%^&*(){}\[\]\'\"\/\|]+$/'],
            'fee' => ['required', 'max:14'],
        ],[],[
            'name'=> __('data_field_name.ideal.name'),
            'proposer' => __('data_field_name.ideal.proposer'),
            'agencies' => __('data_field_name.ideal.agencies'),
            'necessity' => __('data_field_name.ideal.necessity'),
            'target' => __('data_field_name.ideal.target'),
            'content' => __('data_field_name.ideal.content'),
            'result' => __('data_field_name.ideal.result'),
            'research_field' => __('data_field_name.ideal.research_field'),
            'execution_time' => __('data_field_name.ideal.execution_time'),
            'fee' => __('data_field_name.ideal.fee'),
        ]);
    }

    public function selectTab($tab_basic_info, $tab_detail_info) {
        $this->tab_basic_info = $tab_basic_info;
        $this->tab_detail_info = $tab_detail_info;
    }

    public function setResearchField($data) {
        $this->research_field = $data;
    }

    public function download($id){
        $file = \App\Models\File::findOrFail($id);
        return Storage::download($file->url, $file->file_name);
    }

}
