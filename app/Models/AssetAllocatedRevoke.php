<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use OwenIt\Auditing\Contracts\Auditable;


class AssetAllocatedRevoke extends BaseModel implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    protected $table='asset_allocated_revoke';
    protected $guarded = ['*'];
    protected $fillable=['asset_id','status',
        'implementation_date','used_type',
        'number_report','user_info_id',
        'implementation_reason',
        'implementation_quantity',
        'compensation_value', 'contract_number',
        'transfer_type',
        'from_department_id',
        'to_department_id'

    ];
    public function user() {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }
    public function asset() {
        return $this->belongsTo(Asset::class, 'asset_id');
    }

    private static $searchable = [
        'number_report',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
}
