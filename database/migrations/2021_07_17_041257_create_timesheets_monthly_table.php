<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeSheetsMonthlyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheets_monthly', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('luu du cham cong user theo thang');
            $table->integer('year_working')->comment('nam');
            $table->tinyInteger('month_working')->comment('Thang');

            $table->unsignedBigInteger('user_info_id')->nullable()->comment('user_info_id');
            $table->foreign('user_info_id')->references('id')->on('user_info');

            $table->string('total_work_day', 5)->nullable()->comment('tổng ngày coogn thực thế AH');
            $table->string('unpaid_day', 5)->nullable()->comment('Nghi khong luong AI');
            $table->string('holiday', 5)->nullable()->comment('nghi le AJ');
            $table->string('leave_day', 5)->nullable()->comment('nghi phep AK');
            $table->string('total_leave_day', 5)->nullable()->comment('nghi phep AL');
            $table->string('monthly_leave_day', 5)->nullable()->comment('nghi phep thang AM');
            $table->string('yearly_leave_day', 5)->nullable()->comment('nghi phep thang AN');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheets_monthly');
    }
}
