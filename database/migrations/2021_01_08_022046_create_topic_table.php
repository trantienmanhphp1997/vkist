<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('danh sach de tai');
            $table->tinyInteger('status')->nullable()->comment('trang thai 0: chua nop; 1 : da nop; 2 cho tham dinh; 3 : tu choi');
            $table->tinyInteger('source_id')->nullable()->comment('nguon de tai = 0: chi dao truc tie(de tai tiep tuc)p; =1');
            $table->tinyInteger('check_send_file')->nullable()->comment('0: chưa nộp bản cứng, 1 da nop');
            $table->string('code')->nullable()->comment('ma de tai');
            $table->string('name', 500)->nullable()->comment('Ten de tai');
            $table->string('urgency_note', 1500)->nullable()->comment('Tinh cap thiet');
            $table->string('overview', 1500)->nullable()->comment('Tong quan');
            $table->string('target', 1500)->nullable()->comment('muc tieu');
            $table->string('present_note', 1500)->nullable()->comment('nội dung thuyết minh');
            $table->bigInteger('level_id')->nullable()->comment('Cap de tai');
            $table->integer('category_research_id')->nullable()->comment('ma de tai');
            $table->tinyInteger('type')->nullable()->comment('0: de tai tao moi; 1: de tai tiep tuc');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->string('range_use', 255)->nullable()->comment('Pham vi khong gian');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('Map voi user_info (chua dung)');
            $table->foreign('user_info_id')->references('id')->on('user_info');

            $table->foreignId('ideal_id')->nullable()->constrained('ideal')->comment('thuoc nhiem vu');
            $table->foreignId('major_id')->nullable()->comment('Chuyên ngành khoa học, type=12 trong master data')->constrained('master_data');
            $table->foreignId('leader_id')->nullable()->constrained('user_info');

            //khoa hoc
            $table->string('achievement_note', 1500)->nullable()->comment('Thanh qua du kien');
            $table->tinyInteger('achievement_paper_check')->nullable()->comment('Thanh qua du kien');
            $table->tinyInteger('achievement_review_check')->nullable()->comment('Tong luan khoa hoc');
            $table->tinyInteger('achievement_announcement_check')->nullable()->comment('thong bao khoa hoc');
            $table->tinyInteger('achievement_conference_check')->nullable()->comment('Hoi nghi khoa hoc');
            $table->tinyInteger('achievement_work_check')->nullable()->comment('Tac pham khoa hoc');
            $table->tinyInteger('achievement_textbook_check')->nullable()->comment('Thanh qua du kien');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topic');
    }
}
