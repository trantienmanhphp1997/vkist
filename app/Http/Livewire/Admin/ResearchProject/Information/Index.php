<?php

namespace App\Http\Livewire\Admin\ResearchProject\Information;

use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchProject;
use App\Service\Community;
use Illuminate\Support\Facades\Config;

class Index extends BaseLive
{
    public $projectID;

    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render()
    {
        $query = ResearchProject::query()->with(['leader', 'topic','topic.leader', 'department'])->whereIn('status', [
            ResearchProject::WAIT_APPROVAL,
            ResearchProject::IN_PROCESSING,
            ResearchProject::COMPLETED,
            ResearchProject::CANCEL
        ]);
        if ($this->searchTerm != '') {
            $searchTerm = $this->searchTerm;
            $query->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8($searchTerm)) . "%'")
                ->orWhereHas('researchCategory', function ($q) use ($searchTerm) {
                    $q->whereRaw("unsign_text LIKE LOWER('%{" . strtolower(removeStringUtf8($searchTerm)) . "}%')");
                })
                ->orWhereHas('leader', function ($q) use ($searchTerm) {
                    $q->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8($searchTerm)) . "%'");
                })
                ->orWhereHas('department', function ($q) use ($searchTerm) {
                    $q->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8($searchTerm)) . "%'");
                })
                ->orWhere('project_code', 'LIKE', '%' . $this->searchTerm . '%');
        }
        //check permission topic
        if (!empty(Community::listTopicIdAllowed())){
            $query->WhereHas('topic', function ($q)  {
                $q->whereIn('id',Community::listTopicIdAllowed());
            });
        }

        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);

        return view('livewire.admin.research-project.information.index', compact('data'));
    }

    public function setProjectID($id)
    {
        $this->projectID = $id;
    }

    public function transferToStorage()
    {
        if ($this->checkEditPermission && $this->projectID != null) {
            ResearchProject::findOrFail($this->projectID)->update(
                [
                    'status' => ResearchProject::DRAFT
                ]
            );
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('research/project.success.success-saving-draft')]);

        }
    }
}
