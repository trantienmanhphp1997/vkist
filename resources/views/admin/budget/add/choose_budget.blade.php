@extends('layouts.master')
<title>{{__('data_field_name.budget.copy_budget')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    @include('admin.budget.create',['data_budget'=>$data_budget, 'department' =>$department, 'researchProject' =>$researchProject])
@endsection
