<style>
    .language {
        margin: 0 16px;
    }

    .nav-link:hover {
        background: #EBF2FF;
        border-radius: 8px;
    }

</style>
<header class="navbar">
    <div class="menu-md">
        <nav class="navbar-expand-lg navbar-light pd-0">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="nav justify-content-end">
                    <li class="nav-item header-navbar">
                        <a class="nav-link" href="/">
                            <span><img src="/images/CombinedShape.svg" alt="combined-shape"></span> {{ __('header.home') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link" href="{{ route('api.login.mail.index') }}" target="_blank">
                            <span><img src="/images/Mail-opened.svg" alt="mail-opened"></span> {{ __('header.email') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link" href="{{ route('api.login.approval.index') }}" target="_blank">
                            <span><img src="/images/Folder-check.svg" alt="folder-check"></span> {{ __('header.approved') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link @if (in_array(Route::currentRouteName(), ['news.index', 'admin.news.posts.index' , 'admin.news.posts.draft' , 'admin.news.posts.need_approval' , 'admin.news.posts.approved' , 'admin.news.posts.create' , 'admin.news.posts.edit' , 'admin.news.category.index' ])) active @endif"
                           href="{{ route('news.index') }}">
                            <span><img
                                    src="/images/Layout-top-panel-2.svg" alt="layout-top-panel"></span> {{ __('menu_management.menu_name.news') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link @if (in_array(Route::currentRouteName(), ['admin.executive.contract-and-salary.contract.index', 'admin.research.ideal.create' , 'admin.research.edit' , 'admin.executive.dashboard.index'])) active @endif"
                           href="{{ route('admin.executive.dashboard.index') }}">
                            <span><img src="/images/Selected-file.svg" alt="selected-file"></span> {{ __('header.executive') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link @if (in_array(Route::currentRouteName(), ['admin.research.ideal.index', 'admin.research.ideal.create' , 'admin.research.edit' ])) active @endif"
                           href="{{ route('admin.research.ideal.index') }}">
                            <span><img src="/images/Commode.svg" alt="commode"></span>{{ __('header.research_support') }}
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!--menu mobile-->
    <div class="box-logo">
        <a class="navbar-brand mr-0 mr-md-2 logo d-xs-block" href="/" aria-label="Bootstrap">
            <span><img src="/images/Logo_VKIST_Transparent_background-32.png" alt="logo"/></span>
        </a>
        <a class="navbar-brand mr-0 mr-md-2 d-lg-none " href="/" aria-label="Bootstrap">
            <span> <img src="/images/Logo_VKIST_Transparent_background-32.png" alt="logo" height="35"/></span>
        </a>
    </div>
    <!--logo -->
    <div class="navbar menu-lg">
        <nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="nav justify-content-end">
                    <li class="nav-item header-navbar">
                        <a class="nav-link @if (in_array(Route::currentRouteName(),['home'])) active @endif" href="/">
{{--                            <span><img src="/images/CombinedShape.svg" alt="combined-shape"></span>--}}
                             {{ __('header.home') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link" href="{{ route('api.login.mail.index') }}" target="_blank">
{{--                            <span><img src="/images/Mail-opened.svg" alt="mail-opened"></span>--}}
                            {{ __('header.email') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link" href="{{ route('api.login.approval.index') }}" target="_blank">
{{--                            <span><img src="/images/Folder-check.svg" alt="folder-check"></span>--}}
                     {{ __('header.approved') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar position-relative">
                        <a class="nav-link @if (in_array(Route::currentRouteName(), ['news.index', 'admin.news.posts.index' , 'admin.news.posts.draft' , 'admin.news.posts.need_approval' , 'admin.news.posts.approved' , 'admin.news.posts.create' , 'admin.news.posts.edit' , 'admin.news.category.index' ])) active @endif"
                           href="{{ route('news.index') }}">
{{--                              <span><img--}}
{{--                                      src="/images/Layout-top-panel-2.svg" alt="layout-top-panel"></span>--}}
                           {{ __('menu_management.menu_name.news') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link @if (in_array(Route::currentRouteName(), ['admin.executive.contract-and-salary.contract.index' , 'admin.research.edit' , 'admin.executive.dashboard.index'])) active @endif"
                           href="{{ route('admin.executive.dashboard.index') }}">
{{--                            <span><img src="/images/Selected-file.svg" alt="selected-file"></span>--}}
                            {{ __('header.executive') }}
                        </a>
                    </li>
                    <li class="nav-item header-navbar">
                        <a class="nav-link @if (in_array(Route::currentRouteName(), ['admin.research.ideal.index', 'admin.research.ideal.create' ,'admin.research.statistic.index', 'admin.research.edit' ,'admin.research-project.information.draft.index','admin.research-project.productivity.index','admin.research-project.task-work.index','admin.research-project.information.index'])) active @endif"
                           href="{{ route('admin.research.statistic.index') }}">
{{--                            <span><img src="/images/Commode.svg" alt="commode"></span>--}}
                         {{ __('header.research_support') }}
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="header-navbar">
            <a class="px-8  position-relative" href="#" data-toggle="modal" data-target="#notification-list">
                <img src="{{ asset('/images/Notifications.svg') }}" alt="notifications" height="24" width="24">
                <span class="badge badge-primary p-1" id="unread-notification-count"></span>
            </a>
            @if(checkPermission('admin.system.account.index'))
                <a href=" {{ route('admin.system.account.index') }}"><img style="width: 35px;"
                                                                          src="{{ asset('/images/Settings.svg') }}"
                                                                          alt="setting"></a>
            @endif
        </div>
        @if (app()->getLocale() == 'vi')
            <a class="px-0" href="{{ route('lang', ['lang' => 'en']) }}"><img style="width: 45px;"
                                                                              src="{{ asset('/images/american.png') }}" alt="american"></a>
        @else
            <a class="px-0" href="{{ route('lang', ['lang' => 'vi']) }}"><img style="width: 45px;"
                                                                              src="{{ asset('/images/vietnam.png') }}" alt="vietnam"></a>
        @endif

        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <img src="{{ auth()->user()->info->avatar ?? asset('images/avatar.jpg') }}" alt="avatar" height="35" width="35"
                     class="pr-0 border-20">
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item"
                   href="{{ route('admin.profile.index') }}">{{ __('data_field_name.profile.personal_information') }}</a>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </div>
    </div>

</header>
<div class="modal fade modal-fix" id="notification-list" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog notifications" role="document">
        <div class="modal-content px-48 py-32 w-600">
            @livewire('component.notification-list')
        </div>
    </div>
</div>
@livewire('component.deadline-reminder')
