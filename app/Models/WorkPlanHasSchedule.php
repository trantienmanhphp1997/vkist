<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkPlanHasSchedule extends Model
{
    use HasFactory;
    protected $table='work_plan_has_schedule';
    protected $guarded=['*'];
    protected $fillable = [
        'work_plan_id',
        'user_info_id',
        'working_day',
        'type',
        'content',
        'status'
    ];

    public function userInfo()
    {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }
}
