$.fn.countDown = function (config = {}) {

    let $title = $(`<b class="count-down-title">${config?.label?.title ?? 'Count down'}</b>`);
    let $remainingTime = $(`<div class="remaining-time d-flex">Hello</div>`);
    $(this).append($title, $remainingTime);
    $(this).addClass('d-flex justify-content-between align-items-center border rounded bg-light p-2');

    this.config = config;
    this.deadline = config.deadline ? new Date(config.deadline) : null;
    this.autoStart = config?.autoStart ?? true;

    this.start = () => {
        this.interval = setInterval(this.count, 1000);
    }

    this.stop = () => {
        clearInterval(this.interval);
    }

    this.count = () => {
        let deadlineTime = this.deadline.getTime();
        let now = Date.now();
        let distance = deadlineTime - now;

        let $remainingTime = $(this).find('.remaining-time');

        if (distance <= 0) {
            this.stop();
            $remainingTime.html(`<i class="text-danger">${this.config?.label?.outOfDate ?? 'Out of Date'}</i>`);
            return;
        }

        let days = Math.floor(distance / (1000 * 60 * 60 * 24)).toString().padStart(2, '0');
        let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)).toString().padStart(2, '0');
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)).toString().padStart(2, '0');
        let seconds = Math.floor((distance % (1000 * 60)) / 1000).toString().padStart(2, '0');

        $remainingTime.html(`
            <div class="mr-2"><span class="d-block p-2 bg-white border rounded">${days}</span> <small class="d-block text-center">${this.config?.label?.day}</small></div>
            <div class="mr-2"><span class="d-block p-2 bg-white border rounded">${hours}</span> <small class="d-block text-center">${this.config?.label?.hour}</small></div>
            <div class="mr-2"><span class="d-block p-2 bg-white border rounded">${minutes}</span> <small class="d-block text-center">${this.config?.label?.minute}</small></div>
            <div class="mr-2"><span class="d-block p-2 bg-white border rounded">${seconds}</span> <small class="d-block text-center">${this.config?.label?.second}</small></div>
        `);
    }

    this.setDeadline = (deadline) => {
        this.deadline = deadline;
    }

    if (this.deadline && this.autoStart) {
        this.start();
    } else {
        $(this).find('.remaining-time').html(`<i class="text-danger">${this.config?.label?.notSetup ?? 'Not setup'}</i>`);
    }

    return this;
}

$.fn.imagePreview = function (config = {}) {
    let $previewer = $(`<div class="w-100 border rounded" style="min-height: 160px; background-size: contain; background-repeat: no-repeat; background-position: center center; cursor: pointer"></div>`);
    $previewer.on('click', () => {
        this.trigger('click');
    });

    this.on('change', () => {
        let file = this[0].files[0];
        let errorMsg = this.attr('data-image-error');
        if (!checkFileExtension(file, ['jpeg', 'jpg', 'png', 'svg', 'gif'], errorMsg)) {
            this.val(null);
            return;
        }
        $previewer.css({
            'background-image': 'url(' + URL.createObjectURL(file) + ')'
        });
    });

    $(this).addClass('d-none');
    $(this).after($previewer);

    let defaultImg = this.attr('data-image');
    if (defaultImg) {
        $previewer.css({
            'background-image': 'url(' + defaultImg + ')'
        });
    }
}

$("document").ready(function () {

    initDatePicker();

    window.addEventListener('setDateForDatePicker', e => {
        if (e.detail.id) {
            let date = new Date()
            if (e.detail.value) {
                date = new Date(e.detail.value);
            }
            if ($("#" + e.detail.id).length) {
                $("#" + e.detail.id).data("kendoDatePicker").value(date);
            }
        }
    })
    window.addEventListener('show-toast', event => {
        if (event.detail.type == "success") {
            toastr.success(event.detail.message);
        } else if (event.detail.type == "error") {
            toastr.error(event.detail.message);
        }
    });
    window.addEventListener('setSelect2', event => {
        $(".select2").select2({
            theme: "bootstrap",
            width: 'resolve'
        });
    });
    $(".select2-user-info").select2({
        theme: "bootstrap",
    }
    );
    window.livewire.on('close-model-create', () => {
        document.getElementById('close-model-create').click();
    });
    window.livewire.on('setSelect2Input', () => {
        $(".select2-user-info").select2({
            theme: "bootstrap",
        }
        );
        $('.select2-user-info').val(null).trigger('change');
    });

    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: 'a1a433f1bc352d795d3e',
        cluster: 'ap1',
        forceTLS: true
    });

    window.addEventListener('count-unread-notification-event', function (event) {
        document.getElementById('unread-notification-count').innerHTML = event.detail.count;
    });

    $("input.format_number").on({
        keyup: function () {
            formatCurrency($(this));
        },
        // blur: function () {
        //     formatCurrency($(this));
        // }
    });

});

function formatNumber(n) {
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.

    // get input value
    let input_val = input.val();

    // don't validate empty input
    if (input_val === "") { return; }

    // original length
    let original_len = input_val.length;

    // initial caret position
    let caret_pos = input.prop("selectionStart");

    // check for decimal
    if (input_val.indexOf(".") >= 0) {
        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
        let decimal_pos = input_val.indexOf(".");

        // split number by decimal point
        let left_side = input_val.substring(0, decimal_pos);
        let right_side = input_val.substring(decimal_pos);

        // add commas to left side of number
        left_side = formatNumber(left_side);

        // validate right side
        right_side = formatNumber(right_side);

        // On blur make sure 2 numbers after decimal
        if (blur === "blur") {
            right_side += "00";
        }

        // Limit decimal to only 2 digits
        right_side = right_side.substring(0, 2);

        // join number by .
        input_val = left_side + "." + right_side;

    } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = formatNumber(input_val);
        input_val = input_val;

        // final formatting
        if (blur === "blur") {
            input_val += ".00";
        }
    }

    // send updated string to input
    input.val(input_val);
    // put caret back in the right position
    let updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
}

function initDatePicker(target = '.input-date-kendo') {
    $(target).each(function () {
        let id = $(this).attr('id');
        let min_date = $(this).attr('min_date');
        let max_date = $(this).attr('max_date');
        let checkSetNullWhenEnterInvalidDate = $(this).attr('set-null-when-enter-invalid-date');
        $(this).kendoDatePicker({
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 300
                },
                close: {
                    effects: "zoom:out",
                    duration: 300
                }
            },
            format: 'dd/MM/yyyy',
            max: max_date ?? new Date('2199-12-31'),
            min: min_date ?? new Date('1900-01-01'),
            type: 'number',
            change: function () {
                if (checkSetNullWhenEnterInvalidDate == 1) {
                    if (!this.value()) {
                        this.value(null);
                    }
                    window.livewire.emit('set-' + id, {
                        [id]: this.value() ? this.value().toLocaleDateString('en-US') : null
                    });
                } else {
                    if (!this.value()) {
                        this.value(new Date());
                    }
                    window.livewire.emit('set-' + id, {
                        [id]: this.value().toLocaleDateString('en-US')
                    });
                }

            }
        });
        $(this).blur(() => {
            let datepicker = $(this).data("kendoDatePicker");
            if ($(this).val() != "") {
                datepicker.trigger("change");
            }
        })
    });
}

function countCharacter(input) {
    let value = $(input).val();
    let maxlength = $(input).attr('maxlength') || 255;
    $(input).parents('.form-group').find('.character-count').html(value.length + '/' + maxlength);
}

function checkFileExtension(file, accepts = [], msg = "Wrong file format") {
    if (!file) return false;

    let extension = file.name.split('.').pop();

    if (!accepts.includes('*') && !accepts.includes(extension)) {
        toastr.error(msg);
        return false;
    }

    return true;
}
