<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkPlanHasUserInfo extends Participant
{
    use HasFactory;
    protected $table = 'work_plan_has_user_info';
    protected $fillable = ['work_plan_id', 'user_info_id', 'position_id', 'working_day', 'type', 'content', 'status', 'admin_id'];
    protected $mapRelations = [
        'staff' => 'userInfo',
        'work' => 'workPlan'
    ];

    public function userInfo()
    {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }

    public function workPlan()
    {
        return $this->belongsTo(WorkPlan::class, 'work_plan_id');
    }
    public function position() {
        return $this->belongsTo(MasterData::class, 'position_id');
    }
}
