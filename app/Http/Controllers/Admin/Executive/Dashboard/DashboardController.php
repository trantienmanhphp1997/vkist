<?php

namespace App\Http\Controllers\Admin\Executive\Dashboard;

use App\Http\Controllers\Controller;
use DB;

class DashboardController extends Controller {
    public function index() {
        return view('admin.executive.dashboard.index');
    }
}
