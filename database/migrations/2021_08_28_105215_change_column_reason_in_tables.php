<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnReasonInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('decision_reward_receiver', function (Blueprint $table) {
            $table->dropColumn('reason');
        });

        Schema::table('decision_reward', function (Blueprint $table) {
            $table->dropColumn('reason');
        });

        Schema::table('claim_form', function (Blueprint $table) {
            $table->dropColumn('reason');
        });

        Schema::table('user_info_leave', function (Blueprint $table) {
            $table->dropColumn('reason');
        });

        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->dropColumn('implementation_reason');
        });

        Schema::table('decision_reward_receiver', function (Blueprint $table) {
            $table->string('reason', 1000)->nullable()->comment('Ly do');
        });

        Schema::table('decision_reward', function (Blueprint $table) {
            $table->string('reason', 1000)->nullable()->comment('Ly do');
        });

        Schema::table('claim_form', function (Blueprint $table) {
            $table->string('reason', 1000)->nullable()->comment('Ly do');
        });

        Schema::table('user_info_leave', function (Blueprint $table) {
            $table->string('reason', 1000)->nullable()->comment('Ly do');
        });

        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->string('implementation_reason', 1000)->nullable()->comment('Ly do');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('decision_reward_receiver', function (Blueprint $table) {
            $table->dropColumn('reason');
        });

        Schema::table('decision_reward', function (Blueprint $table) {
            $table->dropColumn('reason');
        });

        Schema::table('claim_form', function (Blueprint $table) {
            $table->dropColumn('reason');
        });

        Schema::table('user_info_leave', function (Blueprint $table) {
            $table->dropColumn('reason');
        });

        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->dropColumn('implementation_reason');
        });
    }
}
