<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApprovalIdealDetail extends Model
{
    use HasFactory;

    protected $table = 'approval_ideal_detail';
    protected $fillable = [
    	'status', 'ideal_id', 'approval_id', 'admin_id'
    ];

    public function ideal(){
        return $this->belongsTo(Ideal::class, 'ideal_id', 'id');
    }
}
