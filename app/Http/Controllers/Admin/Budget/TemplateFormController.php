<?php

namespace App\Http\Controllers\Admin\Budget;

use App\Http\Controllers\Controller;
use App\Service\Community;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use DB;
use Validator;

class TemplateFormController extends Controller
{
    public function index()
    {
        return view('admin.budget.form.index');
    }

}

