<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('data_field_name.budget.general')}}</a> \
                <span>{{__('data_field_name.budget.add')}}</span></div>
        </div>
    </div>
    <div class="row bd-border" wire:ignore>
        <div class="col-md-12">
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block">
                                    <p>{{ __('data_field_name.budget.choose_code') }}</p>
                                </div>
                                <div class="w-170 inline-block">
                                    <select class="form-control form-control-lg" wire:model.lazy="searchBudget">
                                        <div class="style_option">
                                            @foreach($list_budget_code as $key => $val)
                                                <option value="{{$key}}" style="height: 40px;">{{$val}}</option>
                                            @endforeach
                                        </div>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="chart">
                        <figure class="highcharts-figure">
                            <div id="budget-chart" class="container"></div>
                        </figure>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div wire:loading class="loader"></div>
    <script src="{!! asset('assets/js/charts.js') !!}">
        
    </script>
    @if($data_arr)
    <script type="text/javascript">
            $('#budget-chart').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: '<?php echo __("data_field_name.budget.highchart_title") ?>'
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                            }
                        }
                    },
                    xAxis: {
                        type: 'category',
                        lineWidth: 0,
                        tickWidth: 0
                    },

                    yAxis: {
                        title: {
                            text: 'VND'
                        }
                    },

                    series: [{
                        dataLabels: [{
                            align: 'left',
                            format: ''
                        }, {
                            align: 'left',
                            format: ''
                        }],
                        color: '#6AD8A7',
                        name: '<?php echo __("data_field_name.budget.highchart_money_plan") ?>',
                        data:  <?php echo json_encode($data_arr[0])?>,
                    },
                    {
                        dataLabels: [{
                            align: 'center',
                            format: '{point.percent}',
                        }],
                        color: '#FF8064',
                        name: '<?php echo __("data_field_name.budget.highchart_money_real") ?>',
                        data:  <?php echo json_encode($data_arr[1])?>,
                    }]
                });
    </script>
    @endif
</div>

