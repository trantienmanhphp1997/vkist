<?php

namespace App\Http\Controllers\Admin\Research\Survey;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\SurveyRequest;

class SurveyController extends Controller
{
	public function index(Request $request) {
		return view('admin.research.survey.index');
	}

	public function create(){
        return view('admin.research.survey.create');
    }

    public function edit($id){
        $data = SurveyRequest::findOrFail($id);
        if(is_null($data)) {
            return redirect()->route('admin.research.survey.index');
        }
        return view('admin.research.survey.edit', compact('data'));
    }
}
