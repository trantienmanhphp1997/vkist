<div wire:ignore.self class="modal fade" id="deleteSelectModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body box-user">
              <h4 class="modal-title" id="exampleModalLabel">{{__('notification.member.warning.warning')}}</h4>
              <p>{{__('notification.member.warning.warning-1')}}</p>
            </div>
            <div class="group-btn2 text-center pt-12">
              <button type="button" class="btn btn-cancel " data-dismiss="modal">{{__('common.button.back')}}</button>
              <button type="button" class="btn btn-save" data-dismiss="modal" wire:click.prevent="delete()">{{__('common.button.delete')}}</button>
            </div>
          </div>
        </div>
      </div>
