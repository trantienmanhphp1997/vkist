<?php


namespace App\Enums;

class EDisbursedStatus
{
    const DISBURSED = 1;
    const NOT_DISBURSED = 2;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::DISBURSED:
                $result = __('data_field_name.reality_expense.disbursed');
                break;
            case self::NOT_DISBURSED:
                $result = __('data_field_name.reality_expense.not_disbursed');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
