<?php

namespace App\Http\Livewire\Admin\Research\Cost;


use App\Http\Livewire\Base\BaseLive;
use App\Models\SalaryBasic;
use App\Models\Task;
use App\Models\TopicDetailWork;
use App\Models\TopicFeeDetail;
use App\Models\TopicHasUserInfo;
use Carbon\Carbon;

class LaborParticipationDetailList extends BaseLive
{

    // on mount
    public $topicFeeDetailId;
    public $topicFeeStatus;

    public $topicDetailWork;

    // collection
    public $tasks;
    public $topicMembers;

    // selected
    public $selectedUserInfoId;
    public $selectedUserInfoRole;

    public function mount($topicFeeDetailId, $topicFeeStatus)
    {
        $this->topicFeeDetailId = $topicFeeDetailId;
        $this->topicFeeStatus = $topicFeeStatus;

        $this->topicDetailWork = new TopicDetailWork();

        // load topic
        $this->topicFeeDetail = TopicFeeDetail::query()->with('topicFee.topic')->findOrFail($topicFeeDetailId);
        $this->topic = $this->topicFeeDetail->topicFee->topic;
        if (!empty($this->topic)) {
            $this->topic->load('members', 'members.userInfo', 'members.role');
            $this->topicMembers = $this->topic->members;
        }

        // load task
        $this->tasks = Task::query()->where('topic_id', $this->topic->id ?? null)->with('userInfos')->get();
    }

    public function render()
    {

        $query = TopicDetailWork::query()
            ->with('topicHasUserInfo.userInfo', 'topicHasUserInfo.role', 'task')
            ->where('topic_fee_detail_id', '=', $this->topicFeeDetailId);

        $dataTotal = [
            'total' => $query->sum('total'),
            'state_capital' => $query->sum('state_capital'),
            'other_capital' => $query->sum('other_capital'),
        ];

        $this->calculateTotal();

        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);
        return view('livewire.admin.research.cost.labor-participation-detail-list', ['data' => $data, 'dataTotal' => $dataTotal]);
    }

    protected function rules()
    {
        $regex = 'regex:/^[0-9,]+$/';
        return [
            'topicDetailWork.task_id' => ['required'],
            'topicDetailWork.expected_result' => ['required', 'max:100'],
            'topicDetailWork.wage_coefficient' => ['required', 'regex:/^(([0-9]{1}[.]{1}[0-9]{1})|([0-9]{1}))$/', 'max:3'],
            'topicDetailWork.number_workday' => ['required', $regex, 'max:6'],
            'topicDetailWork.state_capital' => ['required', $regex, 'max:18'],
            'topicDetailWork.other_capital' => ['required', $regex, 'max:18'],
            'topicDetailWork.base_salary' => ['required', $regex, 'max:15'],
            'topicDetailWork.topic_has_user_info_id' => ['required'],
            'topicDetailWork.total' => 'required'
        ];
    }

    protected function getMessages()
    {
        return [
            'topicDetailWork.base_salary.max' => __('client_validation.form.research_cost.max.base_salary'),
            'topicDetailWork.state_capital.max' => __('client_validation.form.research_cost.max.state_capital'),
            'topicDetailWork.other_capital.max' => __('client_validation.form.research_cost.max.other_capital'),
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'topicDetailWork.task_id' => __('data_field_name.research_cost.work_content'),
            'topicDetailWork.expected_result' => __('data_field_name.research_cost.expected_result'),
            'topicDetailWork.wage_coefficient' => __('data_field_name.research_cost.wage_coefficient'),
            'topicDetailWork.number_workday' => __('data_field_name.research_cost.number_workday'),
            'topicDetailWork.state_capital' => __('data_field_name.research_cost.state_capital'),
            'topicDetailWork.other_capital' => __('data_field_name.research_cost.other_capital'),
            'topicDetailWork.base_salary' => __('data_field_name.research_cost.base_salary'),
            'topicDetailWork.topic_has_user_info_id' => __('data_field_name.research_cost.fullname_perform'),
            'topicDetailWork.total' =>  __('data_field_name.research_cost.total_wages'),
        ];
    }

    public function calculateTotal()
    {
        $wage_coefficient = (float)$this->topicDetailWork->wage_coefficient;
        $base_salary = $this->topicDetailWork->base_salary;
        $number_workday = $this->topicDetailWork->number_workday;
        if (!empty($wage_coefficient) && !empty($base_salary) && !empty($number_workday)) {
            $total = $number_workday * removeFormatNumber($base_salary) * $wage_coefficient;
            $this->topicDetailWork->total = numberFormat((int)$total);
        }
    }

    public function updatedTopicDetailWorkTaskId($taskId)
    {
        $this->selectedUserInfoId = '';
        $this->updatedSelectedUserInfoId();

        $taskFound = $this->tasks->firstWhere('id', $taskId);
        if(!empty($taskFound)) {
            $this->topicDetailWork->number_workday = $this->calculateWorkingDays($taskFound->start_date, $taskFound->end_date);
        }
    }

    public function updatedSelectedUserInfoId()
    {
        $this->selectedUserInfoRole = '';
        $this->topicDetailWork->topic_has_user_info_id = '';
        foreach ($this->topicMembers as $member) {
            if ($member->user_info_id == $this->selectedUserInfoId) {
                $this->selectedUserInfoRole = $member->role->v_value;
                $basicSalary = $member->userInfo->contract->basic_salary ?? null;
                if(!empty($basicSalary)) {
                    $this->topicDetailWork->base_salary = $basicSalary;
                } else {
                    $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.member.warning.warning_salary')]);
                }

                $this->topicDetailWork->topic_has_user_info_id = $member->id;
                break;
            }
        }
    }

    public function create()
    {
        $this->resetInputFields();
        $this->topicDetailWork = new TopicDetailWork();
        $this->dispatchBrowserEvent('open-detail-work-modal-event');
    }

    public function edit($topicDetailWorkId)
    {
        $this->resetInputFields();
        $this->topicDetailWork = TopicDetailWork::findOrFail($topicDetailWorkId);

        $this->topicDetailWork->base_salary = numberFormat($this->topicDetailWork->base_salary);
        $this->topicDetailWork->total = numberFormat($this->topicDetailWork->total);
        $this->topicDetailWork->state_capital = numberFormat($this->topicDetailWork->state_capital);
        $this->topicDetailWork->other_capital = numberFormat($this->topicDetailWork->other_capital);

        $this->dispatchBrowserEvent('open-detail-work-modal-event');
    }

    public function submit()
    {
        $this->validate();
        $this->topicDetailWork->base_salary = removeFormatNumber($this->topicDetailWork->base_salary);
        $this->topicDetailWork->total = removeFormatNumber($this->topicDetailWork->total);
        $this->topicDetailWork->state_capital = removeFormatNumber($this->topicDetailWork->state_capital);
        $this->topicDetailWork->other_capital = removeFormatNumber($this->topicDetailWork->other_capital);
        $this->topicDetailWork->topic_fee_detail_id = $this->topicFeeDetailId;
        $this->topicDetailWork->save();
        $this->updateTopicFeeDetail();

        session()->flash('success', $this->topicDetailWork->exists ? __("notification.common.success.update") : __("notification.common.success.add"));
        return redirect()->route('admin.research.topic-fee.labor.detail', $this->topicFeeDetailId);
    }

    public function updateTopicFeeDetail()
    {
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->topicFeeDetailId);
        $topicFeeDetail->total_capital = TopicDetailWork::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('total');
        $topicFeeDetail->state_capital = TopicDetailWork::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('state_capital');
        $topicFeeDetail->other_capital = TopicDetailWork::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('other_capital');
        $topicFeeDetail->save();
    }

    public function resetInputFields()
    {
        $this->resetValidation();
        $this->topicDetailWork = new TopicDetailWork();
        $this->selectedUserInfoId = '';
        $this->selectedUserInfoRole = '';
    }

    public function calculateWorkingDays($startDate, $endDate)
    {
        $workingDays = 0;
        $startTimestamp = strtotime($startDate);
        $endTimestamp = strtotime($endDate);
        for ($i = $startTimestamp; $i <= $endTimestamp; $i = $i + (60 * 60 * 24)) {
            if (date("N", $i) <= 5) {
                $workingDays = $workingDays + 1;
            }
        }
        return $workingDays;
    }
}
