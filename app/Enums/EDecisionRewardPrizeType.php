<?php


namespace App\Enums;


class EDecisionRewardPrizeType
{
    const MONEY = 1;
    const GLORIFY = 2;
    const PRODUCT = 3;


    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::MONEY:
                $result = __('decision-reward/decision-reward.decision-reward-prize-type.money');
                break;
            case self::GLORIFY:
                $result = __('decision-reward/decision-reward.decision-reward-prize-type.glorify');
                break;
            case self::PRODUCT:
                $result = __('decision-reward/decision-reward.decision-reward-prize-type.product');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
