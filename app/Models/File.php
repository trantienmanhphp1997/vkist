<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends BaseModel
{
    use SoftDeletes;
    use HasFactory;
    protected $table='files';
    protected $fillable=['url','file_name','model_name','size_file','model_id','type', 'admin_id'];
}
