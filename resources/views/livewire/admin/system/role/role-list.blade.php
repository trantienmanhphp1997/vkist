<div class="col-md-10 col-xl-11 box-detail box-user list-role">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="">{{__('data_field_name.system.role.text_role_management')}} </a> \ <span>{{__('data_field_name.system.role.text_role_list')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('data_field_name.system.role.text_role_list')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="form-control size13" placeholder="{{__('data_field_name.system.role.text_placeholder')}}">
                                    <span>
										<img src="/images/Search.svg" alt="search"/>
									</span>
                                </div>
                                <div class="w-170 inline-block">
                                    <select wire:model.debounce.1000ms="filterStatus" class="form-control form-control-lg">
                                        <option value=''>{{__('data_field_name.system.role.status')}}</option>
                                        <option value="1">{{__('data_field_name.system.role.active')}}</option>
                                        <option value="0">{{__('data_field_name.system.role.inactive')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group float-right">
                                @if($checkCreatePermission)
                                <a class="btn btn-viewmore-news mr0" href="{{route('admin.system.role.create')}}"><img src="/images/plus2.svg" alt="plus">{{__('data_field_name.system.role.create')}}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                                <tr class="border-radius">
                                    <th scope ="col" class="text-center border-radius-left">{{__('data_field_name.common_field.stt')}}</th>
                                    <th scope ="col" class="text-center">{{__('data_field_name.common_field.code')}}</th>
                                    <th scope ="col" class="text-center">{{__('data_field_name.system.role.name')}}</th>
                                    <th scope="col">{{__('data_field_name.common_field.descriptions')}}</th>
                                    <th scope ="col" class="text-center">{{__('data_field_name.common_field.status')}}</th>
                                    <th scope ="col" class="border-radius-right text-center">{{__('data_field_name.common_field.action')}}</th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                                @foreach($data as $row)
                                    <tr>
                                        <td class="text-center">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                        <td class="text-center">
                                            @if($checkEditPermission)
                                                <a href="{{route('admin.system.role.edit', $row->id)}}">{!! boldTextSearchV2($row->code, $searchTerm) !!}</a>
                                            @else
                                                {!! boldTextSearchV2($row->code, $searchTerm) !!}
                                            @endif
                                        </td>
                                        <td class="text-center">{!! boldTextSearchV2($row->name, $searchTerm) !!}</td>
                                        <td>{!! $row->note !!}</td>
                                        <td class="text-center">
                                            <label class="status-role">
                                                @if($checkEditPermission)
                                                    @if ($row->status == 1)
                                                        <input type="checkbox" class="input-role" value="1" checked wire:click="updateStatusRole({{$row->id}})" />
                                                    @else
                                                        <input type="checkbox" class="input-role" value="1" wire:click="updateStatusRole({{$row->id}})" />
                                                    @endif
                                                        <span class="slider-role round"></span>
                                                @else
                                                    @if ($row->status == 1)
                                                        <span class="slider-role round check"></span>
                                                    @else
                                                        <span class="slider-role round"></span>
                                                    @endif
                                                @endif
                                            </label>
                                        </td>
                                        <td class="text-center">
                                            @php $id = $row->id; @endphp
                                            @if($checkEditPermission)
                                                <a title="{{__('common.button.edit')}}" class="btn par6" href="{{route('admin.system.role.edit', [$row->id])}}"><img src="/images/pent2.svg" alt=""></a>
                                            @endif
                                            @include('livewire.common.buttons._delete')
                                            @if($checkEditPermission)
                                            <a title="{{__('common.button.add_permission')}}" class="btn par6" href="{{route('admin.system.role.edit', [$row->id, 'user'])}}"><img src="/images/Add-user.svg" alt="add-user"></a>
                                            @endif
                                            @if($checkCreatePermission)
                                            <button title="{{__('common.button.add_role')}}" class="btn par6" type="button" data-toggle="modal" data-target="#createDuplicate" wire:click="showPopupDuplicate({{$row->id}})">
                                                <img src="/images/Duplicate.svg" alt="duplicate">
                                            </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center">
                            <span>{{__('data_field_name.system.role.no_result')}}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete', ['title' => __('notification.role.warning_delete')])
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Duplicate -->
    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="createDuplicate" tabindex="-1" aria-labelledby="createDuplicate" aria-hidden="true">
            <div class="modal-dialog  ">
                <div class="modal-content ">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{__('data_field_name.system.role.text_create_role')}}</h4>
                        <div class="form-group">
                            <label>{{__('data_field_name.system.role.name')}}</label>
                            <input type="text" class="form-control form-input" wire:model.lazy="name">
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.common_field.code')}}</label>
                            <input type="text" class="form-control form-input" wire:model.lazy="code">
                            @error('code') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="group-btn2 text-center  pt-12">
                            <button type="button" id="close-modal-duplicate" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.close')}}</button>
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="duplicate()" class="btn btn-save">{{__('common.button.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Duplicate -->

    <script>
        $("document").ready(function() {
            window.livewire.on('close-modal-duplicate', () => {
                document.getElementById('close-modal-duplicate').click()
            });
        });
    </script>

</div>
