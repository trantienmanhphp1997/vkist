<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs">
                <a href="">{{ __('data_field_name.system.account.user_control') }} </a> \
                <span>
                    @if ($user->exists)
                        {{ __('data_field_name.system.account.edit_user') }}
                    @else
                        {{ __('data_field_name.system.account.create_user') }}
                    @endif
                </span>
            </div>
        </div>
    </div>
    <div class="row bd-border" wire:ignore>
        <div class="col-md-12">
            <div class="detail-task information box-idea">
                <h4>
                    @if ($user->exists)
                        {{ __('data_field_name.system.account.edit_user') }}
                    @else
                        {{ __('data_field_name.system.account.create_user') }}
                    @endif
                </h4>
                <div wire:loading class="loader" style="z-index: 999;"></div>
                <div class="group-tabs">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" data-toggle="tab" href="#tab-general" role="tab" aria-controls="tab-general" aria-selected="false">
                                <span>{{ __('data_field_name.system.account.general_info') }}</span>
                            </a>
                            @if ($user->exists)
                                <a class="nav-item nav-link" data-toggle="tab" href="#tab-role" role="tab" aria-controls="tab-role" aria-selected="false">
                                    <span>{{ __('data_field_name.system.account.role_list') }}</span>
                                </a>
                            @endif
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade active show" id="tab-general" role="tabpanel" aria-labelledby="tab-general-tab">
                            <div class="alert alert-danger mt-3 d-none tab-general-error"></div>

                            @livewire('admin.system.account.tab-general', ['user' => $user])
                        </div>
                        <div class="tab-pane fade" id="tab-role" role="tabpanel" aria-labelledby="tab-role-tab">
                            @livewire('admin.system.account.tab-role', ['user' => $user])
                        </div>
                    </div>
                </div>
                <div class="group-btn2 text-center group-btn">
                    <button type="button" onclick="window.history.back()" class="btn btn-cancel">{{ __('common.button.cancel') }}</button>
                    <button type="button" wire:click="submit" wire:loading.attr="disabled" class="btn btn-save">{{ __('common.button.save') }}</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        window.addEventListener('failed-tab-envent', function(event) {
            $("a[href='#tab-general']").trigger('click');
            $('.tab-general-error').removeClass('d-none');
            $('.tab-general-error').html(event.detail);
        });
    </script>
</div>
