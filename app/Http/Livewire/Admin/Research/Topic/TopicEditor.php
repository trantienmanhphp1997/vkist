<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use App\Enums\ETopicStatus;
use App\Enums\ETypeUpload;
use App\Models\File;
use App\Models\GroupUserHasTopic;
use App\Models\Topic;
use App\Service\Community;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Illuminate\Support\Str;

class TopicEditor extends Component
{
    public Topic $topic;
    public array $data = [];
    public array $readyMembers = [];

    public bool $shouldSaveAndSubmit = false;
    public int $countPassed = 0;
    public array $failedTabs = [];

    protected $listeners = [
        'validated-data-event' => 'gatherData',
        'invalid-data-event' => 'invalidData'
    ];

    public function mount($topicId = 0)
    {
        $this->topic = Topic::query()->findOrNew($topicId);

        if($this->topic->exists && $this->topic->status != ETopicStatus::STATUS_NOT_SUBMIT) {
            return $this->redirectRoute('errors.404');
        }
    }

    public function render()
    {
        if(count($this->failedTabs) > 0) {
            $this->dispatchBrowserEvent('first-failed-tab-event', $this->failedTabs);
        }
        return view('livewire.admin.research.topic.topic-editor');
    }

    public function submit($shouldSaveAndSubmit = false)
    {
        $this->countPassed = 0;
        $this->failedTabs = [];
        $this->shouldSaveAndSubmit = $shouldSaveAndSubmit;
        $this->emit('validate-data-event');
    }

    public function gatherData($tab, $data)
    {
        if ($tab == 'tab-member') {
            $this->readyMembers = $data;
        } else {
            $this->data = array_merge($this->data, $data);
        }
        $this->countPassed += 1;

        if ($this->countPassed == 4) {
            $this->save();
        }
    }

    public function invalidData($tab, $message) {
        $this->countPassed = 0;
        $this->failedTabs[] = [
            'name' => $tab,
            'message' => $message
        ];
    }

    protected function save()
    {

        $this->topic->fill($this->data);
        $message = $this->topic->exists ? __('notification.common.success.update') : __('notification.common.success.add');
        // save info
        $this->topic->submit_date = ($this->shouldSaveAndSubmit) ? date('Y-m-d') : null;
        $this->topic->admin_id = auth()->user()->id;
        $this->topic->status = ($this->shouldSaveAndSubmit) ? ETopicStatus::STATUS_SUBMIT : ETopicStatus::STATUS_NOT_SUBMIT;
        $this->topic->save();
        //neu la user duoc phan quyen tao de tai thi se them de tai vao nhom cua user do
        if (!Auth::user()->hasRole('administrator')){
            $groupHasUserId = Auth::user()->group_id ?? '';
            if ($groupHasUserId){
                GroupUserHasTopic::create([
                    'group_user_id'=>$groupHasUserId ,
                    'topic_id'=>$this->topic->id,
                ]);
            }
        }
        // save file
        File::query()
            ->where('admin_id', auth()->user()->id)
            ->whereNull('model_id')
            ->where('model_name', Topic::class)
            ->whereIn('type', [ETypeUpload::TOPIC_OVERVIEW, ETypeUpload::TOPIC_ATTACHMENT, ETypeUpload::TOPIC_PRESENTATION])
            ->update([
                'model_id' => $this->topic->id
            ]);

        // save member
        $this->topic->members()->createMany($this->readyMembers);
        $leaderId = array_key_first(array_filter($this->readyMembers, fn ($item) => $item['role']['order_number'] == 0));
        $this->topic->update([
            'leader_id' => $leaderId
        ]);

        session()->flash('success', $message);
        return $this->redirectRoute('admin.research.topic.index');
    }

    public function cancel()
    {
        File::query()
            ->where('admin_id', auth()->user()->id)
            ->whereNull('model_id')
            ->where('model_name', Topic::class)
            ->delete();

        return $this->redirectRoute('admin.research.topic.index');
    }
}
