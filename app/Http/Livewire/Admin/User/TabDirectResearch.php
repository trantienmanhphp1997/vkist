<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\UserInf;
use Route;
use App\Models\MasterData;
class TabDirectResearch extends Component
{

    public $user_info_id;
    public $research;
    public $canEdit = true;
    protected $rule =[
        'research' => 'max:255',
    ];
    public function render()
    {
        $masterdata = MasterData::where('type',19)->get();

        $data =  UserInf::where('id',$this->user_info_id)
                    ->orderBy('user_info.id','desc')->get();
        if(checkShowMode()) {
            $this->canEdit = false;
        }
        return view('livewire.admin.user.tab-direct-research',['data'=>$data],['masterdata'=> $masterdata]);
    }

    public function edit(){

            $data= UserInf::findOrFail($this->user_info_id);
            $this->research = $data->research;
    }
    public function update(Request $request){
        $this->validate([
            'research' => 'max:255',
        ],[],[

            'research'=>'hướng nghiên cứu',

        ]);

            $data = UserInf::findOrFail($this->user_info_id);

            $data->research = $this->research;

            $data->save();

            $this->emit('userStore');
    }



}
