<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUserInfoIdToRequestLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_leave', function (Blueprint $table) {
            $table->dropColumn('user_info_id');
        });
        Schema::table('request_leave', function (Blueprint $table) {
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('Nguoi xin nghi phep');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_leave', function (Blueprint $table) {
            $table->dropColumn('user_info_id');
        });
    }
}
