<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnInResearchProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_project', function (Blueprint $table) {
            $table->dropColumn('note');
        });
        Schema::table('research_project', function (Blueprint $table) {
            $table->string('note', 2000)->nullable()->comment('Ghi chú');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_project', function (Blueprint $table) {
            //
            $table->dropColumn('note');
        });
        Schema::table('research_project', function (Blueprint $table) {
            $table->string('note', 1000)->nullable()->comment('Ghi chú');
        });
    }
}
