<?php

namespace App\Http\Livewire\Admin\Asset\AssetAdd;

use App\Enums\EAssetTypeDate;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use App\Service\Community;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use App\Http\Livewire\Base\BaseTrimString;

class TabDistribute extends BaseTrimString

{
    public $assetTypeDate;
    public $distribute_value = '';
    public $distribute_start_date;
    public $asset_value;
    public $value_origin;
    public $number_distribute_month;
    public $depreciation_year;
    public $depreciation_month;
    public $depreciation_value_previous;
    public $depreciation_value_left;
    public $asset_allocated_date;
    public $distribute_date;

    protected $listeners = [
        'loadAssetValue',
        'loadAssetDate',
        'saveTabAdd',
        'set-distribute_start_date' => 'setDistributeStart',
    ];
    public $configFile;

    public function mount()
    {
        $this->assetTypeDate = [
            EAssetTypeDate::DAY => __('data_field_name.asset.day'),
            EAssetTypeDate::MONTH => __('data_field_name.asset.month'),
            EAssetTypeDate::YEAR => __('data_field_name.asset.year'),
        ];
    }

    public function render()
    {
        $current = Carbon::now();
        if ($this->asset_allocated_date){
            $this->distribute_start_date = reFormatDate($this->asset_allocated_date, 'Y-m-d');

            $this->dispatchBrowserEvent('setDateForDatePicker', ["id" => "distribute_start_date", "value" => $this->distribute_start_date]);
        }

        if ($this->asset_value) {
            $this->value_origin = $this->asset_value;
        }
        if ($this->distribute_value) {
            $this->number_distribute_month = numberFormat($this->distribute_value * 12);

        }
        if ($this->asset_value && $this->distribute_value) {
            $this->depreciation_year = numberFormat(round((Community::getAmount($this->asset_value) / $this->distribute_value), 1, PHP_ROUND_HALF_UP));
        }

        if ($this->asset_value && $this->number_distribute_month) {
            $this->depreciation_month =numberFormat(round((Community::getAmount($this->asset_value) / Community::getAmount($this->number_distribute_month)), 1, PHP_ROUND_HALF_UP));

            $this->depreciation_value_previous =0;
            if ($this->asset_allocated_date&&$this->depreciation_month) {
                    $allocated_date = $this->asset_allocated_date;
                    $this->depreciation_value_previous = numberFormat(($current->diffInMonths($allocated_date)) *Community::getAmount($this->depreciation_month));
            }

            $this->depreciation_value_left = numberFormat(Community::getAmount($this->asset_value)-Community::getAmount($this->depreciation_value_previous));

        }


        $this->getValueDistribute();
        return view('livewire.admin.asset.asset-add.tab-distribute');
    }

    public function loadAssetValue($asset_value)
    {
        $this->asset_value = $asset_value;
    }

    public function loadAssetDate($allocated_date)
    {
        $this->asset_allocated_date = $allocated_date;
    }

    public function getValueDistribute()
    {
        $this->emit('getValueDistribute', $this->distribute_value, $this->distribute_start_date,$this->depreciation_value_previous);
    }

    public function configFile()
    {
        $model_name = Asset::class;
        return [
            'model_name' => $model_name,
            'type' => Config::get('common.type_upload.Asset'),
            'folder' => app($model_name)->getTable(),
        ];
    }

    public function resetInput()
    {
        $this->name_other_info = '';
        $this->value_other_info = '';
    }

    public function setDistributeStart($data)
    {
        $this->distribute_start_date = $data['distribute_start_date']?date('Y-m-d', strtotime($data['distribute_start_date'])):null;

    }

    public function saveTabAdd()
    {
        $this->validate([
            'distribute_value' => ['regex:/^[0-9]+$/', 'max:2'],
        ], [], [
            'distribute_value' => __('data_field_name.asset.number_distribute_year'),
        ]);
    }
}
