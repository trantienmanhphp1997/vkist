{{-- Cho viện trưởng --}}
<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="">{{__('research/statistic.breadcrumbs.research_project_management')}} </a> \ <span>{{__('research/statistic.breadcrumbs.research_statistic')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('research/statistic.title.general')}}</h3>
                </div>
            </div>
            <div class="dashboard-asset">
                <div class="row">
                    <div class="col-md-4 pd8">
                        <div class="card">
                            <div class="widget-chart-content">
                                <div class="subheading" style="color: #377DFF">
                                        {{__('research/statistic.title.project.in-progress')}}
                                    <div class="float-right">
                                        <img src="/images/blocks.svg" alt="block"/>
                                    </div>
                                </div>
                                <div class="widget-numbers" style="color: #4E5D78">
                                    {{$inprogress_project}}
                                </div>
                                <div class="widget-description">
                                {{ $total_project > 0 ? round($inprogress_project*100/$total_project, 2) : 0}}% {{__('research/statistic.title.project.total')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pd8">
                        <div class="card">
                            <div class="widget-chart-content">
                                <div class="subheading" style="color: #38CB89">
                                        {{__('research/statistic.title.project.done')}}
                                    <div class="float-right">
                                        <img src="/images/blocks.svg" alt="block"/>
                                    </div>
                                </div>
                                <div class="widget-numbers" style="color: #4E5D78">
                                    {{$complete_project}}
                                </div>
                                <div class="widget-description">
                                    {{$total_project > 0 ? round($complete_project*100/$total_project,2) : 0}}% {{__('research/statistic.title.project.total')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pd8">
                        <div class="card">
                            <div class="widget-chart-content">
                                <div class="subheading" style="color: #FFAB00">
                                        {{__('research/statistic.title.project.waiting')}}
                                    <div class="float-right">
                                        <img src="/images/blocks.svg" alt="block"/>
                                    </div>
                                </div>
                                <div class="widget-numbers" style="color: #4E5D78">
                                    {{$waiting_project}}
                                </div>
                                <div class="widget-description">
                                    {{$total_project > 0 ? round($waiting_project*100/$total_project,2) : 0}}% {{__('research/statistic.title.project.total')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pd8">
                        <div class="card">
                            <div class="widget-chart-content">
                                <div class="subheading" style="color: #377DFF">
                                        {{__('research/statistic.title.task.in-progress')}}
                                    <div class="float-right">
                                        <img src="/images/blocks.svg" alt="block"/>
                                    </div>
                                </div>
                                <div class="widget-numbers" style="color: #4E5D78">
                                    {{$inprogress_task}}
                                </div>
                                <div class="widget-description">
                                    {{$total_task > 0 ? round($inprogress_task*100/$total_task,2) : 0}}% {{__('research/statistic.title.task.total')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pd8">
                        <div class="card">
                            <div class="widget-chart-content">
                                <div class="subheading" style="color: #38CB89">
                                        {{__('research/statistic.title.task.done')}}
                                    <div class="float-right">
                                        <img src="/images/blocks.svg" alt="block"/>
                                    </div>
                                </div>
                                <div class="widget-numbers" style="color: #4E5D78">
                                    {{$complete_task}}
                                </div>
                                <div class="widget-description">
                                    {{$total_task > 0 ? round($complete_task*100/$total_task,2) : 0}}% {{__('research/statistic.title.task.total')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pd8">
                        <div class="card">
                            <div class="widget-chart-content">
                                <div class="subheading" style="color: #FFAB00">
                                        {{__('research/statistic.title.task.waiting')}}
                                    <div class="float-right">
                                        <img src="/images/blocks.svg" alt="block"/>

                                    </div>
                                </div>
                                <div class="widget-numbers" style="color: #4E5D78">
                                    {{$waiting_task}}
                                </div>
                                <div class="widget-description">
                                    {{$total_task > 0 ? round($waiting_task*100/$total_task,2) : 0}}% {{__('research/statistic.title.task.total')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row chart-asset" style="display: flex; flex-wrap: wrap;">
                    <div class="col-md-6 pd8">
                        <div class="card">
                            <div class="widget-chart-content card_title">
                                <div class="subheading">
                                        {{__('research/statistic.title.ideal_statistic_by_status')}}
                                    <div class="float-right sum-mumber ">
                                        {{__('research/statistic.title.total_ideal')}}: {{$total_ideal}}
                                    </div>
                                </div>
                            </div>
                            <figure class="highcharts-figure w-100">
                                <div id="pieChartIdeal"></div>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-6 pd8">
                        <div class="card">
                            <div class="widget-chart-content">
                                <div class="subheading">
                                        <div class="d-flex justify-content-between pb-2">
                                            <div style="line-height: 38px">
                                                {{__('research/statistic.title.task_in_progress')}}
                                            </div>
                                            <div >
                                            <a class="btn btn-outline-primary" href="{{route('admin.research-project.task-work.index')}}">
                                                    {{__('common.button.view_all')}}
                                                </a>
                                            </div>
                                        </div>
                                        <div class="approve p-0 overflow-scroll-y pr-8 mt-12" style="max-height:450px;min-height:450px; overflow-y: scroll">
                                            @foreach ($listInProgressTask as $item)
                                                <div class="col-md-12 item p-0 pt-3">
                                                    <a class="media align-items-center" href="{{ route('admin.research-project.task-work.edit', $item->id) }}" style="color:#4E5D78"> 
                                                        <img src="/images/imgdemo.jpg" alt="" style="width: 40px; height:40px">
                                                        <div class="media-body">
                                                            <h4 class="mb-0 size13 font-weight-normal">{{$item->name}}
                                                            </h4>
                                                        <span class="date size11">{{reFormatDate($item->start_date)}}</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    let idealData = [];
    idealData.push(<?php echo $total_ideal > 0 ? round($just_create_ideal*100/$total_ideal,1) : 0 ?>);
    idealData.push(<?php echo $total_ideal > 0 ? round($wait_approve_ideal*100/$total_ideal,1) : 0 ?>);
    idealData.push(<?php echo $total_ideal > 0 ? round($rejected_ideal*100/$total_ideal,1) : 0 ?>);
    idealData.push(<?php echo $total_ideal > 0 ? round($approved_ideal*100/$total_ideal,1) : 0 ?>);

    let nameJustCreated = '<?php echo __('research/ideal.status.just_created') ?>'
	let nameWaitApprove = '<?php echo __('research/ideal.status.waiting_for_appraisal') ?>'
	let nameRejected = '<?php echo __('research/ideal.status.refuse') ?>'
	let nameApproved = '<?php echo __('research/ideal.status.approved') ?>'

        Highcharts.chart('pieChartIdeal', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            colors: [
                '#FFAB00',
                '#377DFF',
                '#FF5630',
                '#6AD8A7',
            ],
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            title: '',
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                    },
                    showInLegend: true,
                    size: '75%'
                }
            },
            legend: {
                symbolHeight: 20,
                symbolWidth: 20,
                symbolRadius: 4,
                itemMarginTop: 20,
                labelFormatter: function() {
                    return '<div style="text-align: left; width:130px; font-family: Arial">' + this.name + '</div><div style="width:40px; text-align:right;font-family: Arial">&nbsp' + this.y + '%</div>';
                },
                align: 'right',
                verticalAlign: 'middle',
            },
            series: [{
                name: 'Task',
                colorByPoint: true,
                data: [{
                    name: nameJustCreated,
                    y: idealData[0],
                }, {
                    name: nameWaitApprove,
                    y: idealData[1]
                }, {
                    name: nameRejected,
                    y: idealData[2]
                }, {
                    name: nameApproved,
                    y: idealData[3]
                },],
                
            }]
        });
</script>