<div class="tab-pane active" id="profile" wire:ignore.self>
    <h3 class="title-asset">{{__('data_field_name.asset.info_general')}}</h3>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>{{__('data_field_name.asset.department')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" placeholder="" value="{{$department_name ?? ''}}" disabled>
                @error('department_id') <span class="text-danger">{{ $message }}</span>@enderror

            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_cate')}}<span class="text-danger">*</span></label>
                <div class="form-asset" wire:ignore>
                    <select class="form-control form-control-lg" wire:model.lazy="category_id">
                        <option value="">{{__('data_field_name.asset.choose_category')}}</option>
                        @foreach ($category as $value)
                            <option value="{{$value['id']}}">
                                {!!$value['padLeft'].$value['name']!!}
                            </option>
                        @endforeach
                    </select>

                    <span><img src="/images/cham.svg" alt=""/></span>
                </div>
                @error('category_id') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_name')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" placeholder="" wire:model.lazy="name">
                @error('name') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_code')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" placeholder=""
                       wire:model.lazy="code">

                @error('code') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_quantity')}}<span class="text-danger">*</span></label>
                <input type="number" class="form-control"
                       @if ($type_manage==\App\Enums\EAssetCategoryTypeManage::CODE)
                       disabled
                       @endif
                       wire:model.lazy="quantity">
                @error('quantity') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.unit')}}</label>
                <div class="form-asset">
                    <select class="form-control form-control-lg" wire:model.lazy="unit_id">
                        <option value="">{{__('data_field_name.asset.choose_unit')}}</option>
                        @foreach ($units as $key=> $val)
                            <option value="{{$key}}">
                                {{$val}}
                            </option>
                        @endforeach
                    </select>
                    <span><img src="/images/cham.svg" alt=""/></span>
                    @error('unit_id') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>{{__('data_field_name.asset.origin')}}<span class="text-danger">*</span></label>
                <div class="form-asset">
                    <select class="form-control form-control-lg" wire:model.lazy="origin">
                        <option value="">{{__('data_field_name.asset.choose_origin')}}</option>
                        @foreach($originAsset as $val)
                            @if($val->origin_status == 1)
                                <option value="{{$val->id}}">{{$val->origin_name}}</option>
                            @endif
                        @endforeach
                    </select>
                    <button type="button" class="btn btn-add" data-toggle="modal" data-target="#origin_asset"><img src="/images/addblue.svg" class="p-0" alt=""></button>
                </div>
                @error('origin') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.buy_date')}}<span class="text-danger">*</span></label>
                @include('layouts.partials.input._inputDate', ['input_id' => 'buy_date', 'place_holder'=>'', 'default_date' => is_null($buyDate) ? date('Y-m-d') : $buyDate ])
                @error('buy_date') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.contract_number')}}</label>
                <input type="text" class="form-control" placeholder="" wire:model.lazy="contract_number"
                       value="{{old('contract_number')}}">
                @error('contract_number') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.provider')}}</label>
                <div class="form-asset">
                    <select class="form-control form-control-lg" wire:model.lazy="asset_provider_id">
                        <option value="">{{__('data_field_name.asset.choose_provider')}}</option>
                        @foreach ($provider as $key=> $val)
                            <option value="{{$key}}">
                                {{$val}}
                            </option>
                        @endforeach
                    </select>
                    <span><img src="/images/cham.svg" alt=""/></span>
                </div>
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.serial')}}</label>
                <input type="text" class="form-control" placeholder="" wire:model.lazy="series_number"
                       value="{{old('series_number')}}">
                @error('series_number')<span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label>{{__('data_field_name.asset.asset_value')}}</label>
                <input type="text" class="form-control format_number" placeholder="" wire:model.lazy="asset_value"
                       value="{{old('asset_value')}}" maxlength="13">
                @error('asset_value')<span class="text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>{{__('data_field_name.asset.unit_type')}}</label>
                <textarea class="form-control" rows="4" wire:model.lazy="unit_type"
                          value="{{old('unit_type')}}"></textarea>
                @error('unit_type') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>{{__('data_field_name.asset.note')}}</label>
                <textarea class="form-control" rows="4" wire:model.lazy="note"></textarea>
                @error('note') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <input type="checkbox" wire:model="fixed_assets"

                   disabled
            >
            <label for="a-1">{{__('data_field_name.asset.fix_asset')}}</label>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>{{__('data_field_name.asset.group_asset')}}</label>
                <div class="form-group checkradio">
                    @foreach($assetGroup as $key => $val)
                        <input type="radio" value="{{$key}}" wire:model.lazy="group_asset">
                        <label for="">{{$val}}</label>&ensp;
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-12 pd-col">
            <div id="file_right">
                <label>{{__('data_field_name.asset.file')}}</label>
                @livewire('component.files',[ 'model_name'=>$model_name,
                'type'=>$type,'folder'=>$folder,'status'=>$status])
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade modal-fix" id="origin_asset" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6>{{__('data_field_name.labor-contract.set_value')}}</h6>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="container-fluid">
                            <div class="row">

                                <div class="col-12 pd-col">
                                    <div class="form-group info-request searchbar">
                                        <label>{{__('common.place_holder.search')}}</label>
                                        <div class="position-relative">
                                            <input type="search" placeholder="{{__('data_field_name.labor-contract.enter_info')}}" wire:model.debounce.1000ms="filter_origin_name">
                                            <img  class="search-icon" src="/images/Search.svg" alt="search">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 pd-col">
                                    <div class="form-group info-request mb-12">
                                        <div class="have-btn">
                                            <label class="m-0">{{__('data_field_name.asset.asset_origin')}}</label>
                                            <button type="button" class="btn btn-add ml-auto" wire:click="$set('show_add_origin', true)"><img src="/images/addblue.svg" alt="">{{__('data_field_name.labor-contract.add_duration')}}</button>
                                        </div>
                                    </div>
                                </div>
                                @forelse($originAsset as $index => $item)
                                    <div class="col-12 pd-col">
                                        <div class="form-check info-request">
                                            @if($item->origin_status == 1)
                                                <input type="checkbox" checked="" wire:click="toggleDisplaySetting({{$item->id}})" id="{{$index}}">
                                            @else
                                                <input type="checkbox" wire:click="toggleDisplaySetting({{$item->id}})" id="{{$index}}">
                                            @endif
                                            <label class="m-0" for="{{$index}}">{{$item->origin_name}}</label>
                                            <a class="btn btn-remove ml-auto" href="#" wire:click="deleteSetting({{$item->id}})"><img src="/images/close.svg" alt=""></a>
                                        </div>
                                    </div>
                                    @empty
                                        <label class="text-danger">
                                            {{__('common.message.no_data')}}
                                        </label>
                                @endforelse
                                @if($show_add_origin)
                                    <div class="col-12 pd-col">
                                        <div class="form-check info-request">
                                            <input type="checkbox" value="" wire:model.defer="origin_status">
                                            <input type="text" wire:model.defer="origin_name" class="h-40">
                                        </div>
                                        @error('origin_name')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal" wire:click.prevent="cancelOrigin">{{__('common.button.cancel')}}</button>
                    @if($show_add_origin)
                        <button id="btn-save-setting" type="button" class="btn btn-primary" wire:click.prevent="saveSetting">{{__('common.button.save')}}</button>
                    @else
                        <button type="button" id="disabled-btn-save" class="btn btn-primary" disabled="">{{__('common.button.save')}}</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(() => {
        window.livewire.on('closeModelSetting', () => {
            $('#origin_asset').modal('hide');
        });
    });
</script>
