<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCloumnToRequestLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_leave', function (Blueprint $table) {
            $table->integer('status')->nullable()->comment('Trạng thái yêu cầu: 1: chờ phê duyệt, 2: Phê duyệt, 3: Từ chối');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_leave', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
