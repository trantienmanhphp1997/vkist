@extends('layouts.master')
@section('homeLeft')
@include('layouts.partials.sliderbar._systemLeft')
@endsection

@section('content')
    @livewire('admin.system.account.account-editor')
@endsection
