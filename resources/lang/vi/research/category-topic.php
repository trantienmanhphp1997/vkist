<?php
return [
    'breadcrumbs' => [
        'category-topic-management-1' => 'Quản lý danh mục',
        'category-topic-management-2' => 'Quản lý loại đề tài',
        'category-topic-list' => 'Danh sách loại đề tài',
    ],
    'table_column' => [
        'code' => 'MÃ',
        'name' => 'TÊN LOẠI ĐỀ TÀI',
        'description' => 'MÔ TẢ',
        'note' => 'GHI CHÚ',
        'action' => 'HÀNH ĐỘNG',
    ],
    'title' => [
        'create' => 'Tạo mới',
        'edit' => 'Chỉnh sửa',
        'no-result' => 'Không có kết quả!',
        'part-1' => 'Hiển thị',
        'part-2' => 'loại đề tài trong',
    ],
    'empty-record' => 'Không có bản ghi nào',
    'placeholder' => [
        'search' => 'Tìm kiếm',
        'code' => 'Nhập mã loại đề tài',
        'name' => "Nhập tên loại đề tài",
        'description' => "Nhập mô tả loại đề tài",
        'note' => "Nhập ghi chú loại đề tài",
    ],
    'modal' => [
        'title' => [
            'delete' => 'Xác nhận xoá',
            'create' => 'Thêm loại đề tài',
            'edit' => 'Sửa loại đề tài',
            'detail' => 'Chi tiết loại đề tài',
        ],
        'body' => [
            'delete-warning' => 'Bạn có xóa không? Thao tác này không thể phục hồi!',
            'code' => 'Mã loại đề tài',
            'name' => 'Tên loại đề tài',
            'description' => 'Mô tả',
            'note' => 'Ghi chú',
        ],
        'footer' => [
            'back' => 'Quay lại',
            'delete' => 'Xoá',
            'close' => 'Đóng',
            'save' => 'Lưu',
        ]
    ],
    'validate' => [
        'code' => 'Mã loại đề tài',
        'name' => 'Tên loại đề tài',
        'note' => 'Ghi chú loại đề tài',
        'description' => 'Mô tả loại đề tài',
    ],
    'notification' => [
        'edit-success' => 'Chỉnh sửa thành công',
        'create-success' => 'Thêm mới thành công'
    ]
];
