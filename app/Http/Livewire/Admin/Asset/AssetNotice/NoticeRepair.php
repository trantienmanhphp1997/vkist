<?php

namespace App\Http\Livewire\Admin\Asset\AssetNotice;

use App\Enums\EAssetPlaceMaintenance;
use App\Models\Asset;
use App\Models\AssetAllocatedRevoke;
use App\Models\AssetMaintenance;
use App\Models\AssetProvider;
use App\Models\UserInf;
use App\Service\Community;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class NoticeRepair extends Component
{
    public $assetDetail;
    public $placeMaintenance;
    public $number_report;
    public $implementation_date;
    public $estimated_completion_date;
    public $user_info_id;
    public $asset_provider_id;
    public $finished;
    public $estimated_cost;
    public $content_repair;
    public $place;
    public $implementation_quantity;
    public $errorQuantity;
    public $implementationDate;
    protected $listeners=[
        'set-implementation_date' => 'setImplementationDate',
        'set-estimated_completion_date'=>'setEstimatedCompletionDate',
    ];
    protected $rule =[
        'number_report' => 'required|unique:asset_maintenance,number_report|max:10',
        'implementation_date' => 'required',
        'estimated_cost' => 'max:13|regex:/^[0-9]+$/',
        'content_repair'=>'max:255',
    ];

    public function mount() {
        $this->placeMaintenance = EAssetPlaceMaintenance::getList();
    }
    public function render()
    {
        $provider = AssetProvider::query()->pluck('name', 'id');
        $users = UserInf::query()->pluck('fullname', 'id');

        $model_name = AssetMaintenance::class;
        $type = Config::get('common.type_upload.AssetMaintenance');
        $folder = app($model_name)->getTable();
        $status = -1;

        $type_manage = '';

        if ($this->assetDetail) {
            $asset_Detail = $this->assetDetail;
            $type_manage = $asset_Detail->category->type_manage;

            if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                $this->implementation_quantity = 1;
            }
        }

        return view('livewire.admin.asset.asset-notice.notice-repair',
        ['user'=>$users,'provider'=>$provider,
            'model_name'=>$model_name, 'type'=>$type,
            'folder'=>$folder,'status'=>$status, 'type_manage' => $type_manage]
        );
    }

    public function setImplementationDate($data) {
        $this->implementationDate = date('Y-m-d', strtotime($data['implementation_date']));
    }
    public function setEstimatedCompletionDate($data) {
        $this->estimated_completion_date = date('Y-m-d', strtotime($data['estimated_completion_date']));
    }

    public function save(){
        $asset =$this->assetDetail;
        $type_manage = $asset->category->type_manage;
        $quantity = $asset ->amount_used;
        //để lấy default leaveDate là ngày hiện tại
        if(is_null($this->implementationDate)) {
            $this->implementationDate = date('Y-m-d');
            $this->implementation_date= date('Y-m-d');
        }else{
            $this->implementation_date=$this->implementationDate;
        }
        if (!empty($quantity) && $quantity < $this->implementation_quantity) {
            $this->errorQuantity = __('validation.lt.numeric', [
                'attribute' => __('data_field_name.asset.asset_quantity'),
                'value' => __('data_field_name.asset.asset_used_quantity')
            ]);
        }

        $this->validate([
            'number_report' => ['required','regex:/^[A-Z0-9' . config('common.special_character.alphabet') .']+$/', 'max:10','unique:asset_maintenance,number_report'],
            'estimated_cost' => ['regex:/^[0-9\.\,]+$/','max:13','nullable'],
            'content_repair'=>['regex:/^[a-zA-Z0-9' . config('common.special_character.alphabet') . '\.\,\-\(\)\/ ]+$/', 'max:255','nullable'],
            'implementation_quantity' => ['required','regex:/^[1-9\.\,]+$/','max:3'],
            'implementation_date'=>'required|before_or_equal:estimated_completion_date',
            'estimated_completion_date'=>'required',
        ],[],[
            'implementation_date'=>__('data_field_name.asset.repair_date'),
            'estimated_completion_date'=>__('data_field_name.asset.estimated_completion_date'),
            'number_report' => __('data_field_name.asset.number_report'),
            'estimated_cost' => __('data_field_name.asset.estimated_cost'),
            'content_repair'=>__('data_field_name.asset.content_repair'),
            'implementation_quantity' => __('data_field_name.asset.asset_quantity'),
        ]);

        if ($quantity >= $this->implementation_quantity) {
            $data = AssetMaintenance::create([
                'asset_id' => $asset->id,
                'number_report' => $this->number_report,
                'implementation_date' => $this->implementationDate,
                'estimated_completion_date' => $this->estimated_completion_date,
                'user_info_id' => $this->user_info_id,
                'asset_provider_id' => $this->asset_provider_id,
                'estimated_cost' => (int)Community::getAmount($this->estimated_cost),
                'content_repair' => $this->content_repair,
                'place' => $this->place,
                'status' => \App\Enums\EAssetStatus::REPORT_REPAIR,
                'implementation_quantity' => $this->implementation_quantity
            ]);
            if ($data) {
                \App\Models\File::where('model_name', AssetMaintenance::class)
                    ->where('type', Config::get('common.type_upload.AssetMaintenance'))
                    ->where('admin_id', auth()->id())
                    ->where('model_id', null)
                    ->update([
                        'model_id' => $data->id,
                        'status' => 1
                    ]);
                if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                    Asset::where('id', $data->asset_id)->update([
                        'situation' => \App\Enums\EAssetStatus::REPORT_REPAIR
                    ]);
                } else {
                    $asset_quantity = Asset::findOrFail($data->asset_id);
                    if ($asset_quantity->quantity == $this->implementation_quantity) {
                        Asset::where('id', $data->asset_id)->update([
                            'situation' => \App\Enums\EAssetStatus::REPORT_REPAIR
                        ]);
                    }
                }
                $this->emit('loadStatus');
            }
            $this->emit('closeNotice');
            $this->resetInput();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
                __('notification.asset.success.repair_success')]);
        }
    }
    public function cancel(){
        \App\Models\File::where('model_name', AssetMaintenance::class)
            ->where('type', Config::get('common.type_upload.AssetMaintenance'))
            ->where('admin_id', auth()->id())
            ->where('status',-1)
            ->where('model_id', null)
            ->delete();
        $this->resetInput();
    }
    public function resetInput()
    {
        $this->number_report = '';
        $this->implementation_date = '';
        $this->implementation_quantity = '';
        $this->estimated_completion_date = '';
        $this->user_info_id = '';
        $this->asset_provider_id = '';
        $this->finished = '';
        $this->estimated_cost = '';
        $this->content_repair = '';
        $this->place = '';
        $this->resetValidation();
    }
}
