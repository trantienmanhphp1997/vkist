<?php

namespace App\Http\Livewire\Admin\News;

use App\Http\Livewire\Base\BaseLive;
use App\Models\News;
use App\Service\Community;

class Detail extends BaseLive
{
    public $new ;
    public $files = [];

    public function mount($new){
        $this->new = $new;
        $this->files = \App\Models\File::where('model_id', $this->new->id)->where(['model_name'=>News::class])->get();
    }
    public function render()
    {

        $amountFile = count($this->files);
        $new = $this->new;
        $news = \App\Models\News::query()->where(['status'=>\App\Models\News::APPROVED_NEWS])
            ->with(['admin','category'])
            ->where(['published'=>1])
            ->orderBy('id','DESC')->take(5)
            ->get();
        return view('livewire.admin.news.detail',['new'=>$new,'amountFile'=>$amountFile,'news'=>$news]);
    }

    public function getDetail($slug,$categorySlug){

        return redirect(route('news.detail',['slug'=>$slug,'categorySlug'=>$categorySlug]));
    }

    public function download($fileUrl)
    {
        try {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('news/newsManager.menu_name.download-file-success')]);
            return Community::downloadFile($fileUrl);
        } catch (\Exception $e) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" =>  __('news/newsManager.menu_name.download-file-fail')]);
        }
    }
}
