@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.shopping.list') }}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    @livewire('admin.executive.shopping.shopping-list')
@endsection
