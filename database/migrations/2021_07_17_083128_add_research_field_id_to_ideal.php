<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResearchFieldIdToIdeal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ideal', function (Blueprint $table) {
            $table->foreignId('research_field_id')->nullable()->constrained('research_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ideal', function (Blueprint $table) {
            $table->dropColumn('research_field_id');
        });
    }
}
