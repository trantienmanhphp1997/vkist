<?php

namespace App\Http\Livewire\Admin\Asset\AllocatedAndRevoke;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use App\Models\AssetAllocatedRevoke;
use App\Models\Asset;
use App\Models\UserInf;
use DB;
use Illuminate\Support\Carbon;
use App\Models\File;
use App\Component\FileUpload;
use Livewire\WithFileUploads;
use App\Models\ClaimForm;

class Allocated extends BaseLive
{
    use WithFileUploads;

    public $data;
    public $user;
    public $modalUserList = [];
    public $modalAssetList = [];
    public $asset;
    public $indexSelected = 0;
    public $idAssetSelected = [];
    public $deleteSelected;
    public $disable = false;
    public $chooseAll = 2;
    public $chooseAllBy;

    public $name;
    public $assetName;

    public $fileUpload = [];
    public $day;
    public $reason;
    public $number_report;

    public $type;
    public $model_name;
    public $folder;

    public $errorFile;
    public $errorUser;
    public $errorAsset;
    public $errorQuantity;
    public $export_file = false;

    protected $listeners = ['export' => 'export', 'resetData' => 'resetData', 'set-allocatedDay' => 'setAllocatedDay'];

    public function mount() {
        $this->data = json_decode(base64_decode($this->data));
        $this->day = now()->format(Config::get('common.year-month-day'));
        $this->getDataList();
    }

    public function render() {
        $this->getUserlist();
        $this->getAssetlist();

        $this->type = Config::get('common.type_upload.Allocated');
        $this->model_name = AssetAllocatedRevoke::class;
        $this->folder = app($this->model_name)->getTable();

        if (!empty($this->fileUpload) && count($this->fileUpload) > 5) {
            $this->errorFile = __('validation.number_of_file.max', [
                'max' => 5
            ]);
            array_splice($this->fileUpload, 5, 1);
        } elseif (!empty($this->fileUpload)) {
            $this->validateFile();
        }

        return view('livewire.admin.asset.allocated-and-revoke.allocated', ['dataList' => $this->user, 'userList' => $this->modalUserList]);
    }

    public function validateFile() {
        $file = new FileUpload();
        $arr = ['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z'];
        foreach ($this->fileUpload as $index => $item) {
            $dataUpload = $file->uploadFile($item, $this->folder, $this->model_name);
            if (!$item->getSize() > 255 * 1024 * 1024) {
                $this->errorFile = __('common.file_size', [
                    'value' => '255MB'
                ]);
            }
            $tmp = explode('.', $dataUpload['file_name']);
            if (count($tmp) > 0 && !in_array(end($tmp), $arr)) {
                $this->errorFile = __('data_field_name.allocated-revoke.invalid_file');
                array_splice($this->fileUpload, $index, 1);
                break;
            }
        }

    }

    public function getAssetlist() {
        $currentUserId = !empty($this->user[$this->indexSelected]['id']) ? $this->user[$this->indexSelected]['id'] : null;
        $assetOfUser = Asset::query()
            ->select('asset.id', 'asset.code', 'asset.name', 'unit_type', 'quantity', 'amount_used')
            ->where('user_info_id',  $currentUserId);

        $modal_asset_list = Asset::query()
            ->select('asset.id', 'asset.code', 'asset.name', 'unit_type', 'quantity', 'amount_used')
            ->whereNull('user_info_id')
            ;

        if (strlen($this->assetName)) {
            $assetOfUser->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->assetName)) . '%');
            $modal_asset_list->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->assetName)) . '%')->union($assetOfUser);
        }
        $modal_asset_list = $modal_asset_list->limit(10)->get();
        $modal_asset_list = $modal_asset_list->map(function ($item) {
            $choosed = null;
            $checked = 0;
            $owned = false;
            if (count($this->user) > 0) {
                foreach ($this->user[$this->indexSelected]['asset'] as $indx => $value) {
                    if ($item->id == $value['id']) {
                        $checked = 1;
                        break;
                    }
                }
                foreach ($this->user as $indx => $user) {
                    foreach ($user['asset'] as $asset) {
                        if ($indx != $this->indexSelected && $asset['id'] == $item->id) {
                            $choosed = __('data_field_name.allocated-revoke.selected_for') . $user['fullname'];
                            $owned = true;
                            break;
                        } else if ($item->id == $asset['id']) {
                            $checked = 1;
                            break;
                        }
                    }
                }
            }
            return [
                'id' => $item->id,
                'code' => $item->code,
                'checked' => $checked,
                'name' => $item->name,
                'amount_used' => $item->amount_used,
                'quantity' => $item->quantity,
                'unit_type' => $item->unit_type,
                'implementation_quantity' => 1,
                'error' => null,
                'choosed' => $choosed,
                'owned' => $owned
            ];
        });
        $this->modalAssetList = collect($modal_asset_list)->all();
    }

    public function getUserlist() {
        $modal_user_list = UserInf::query();
        if (strlen($this->name)) {
            $modal_user_list->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->name)) . '%');
        }
        $modal_user_list = $modal_user_list->limit(10)->get();
        $modal_user_list = $modal_user_list->map(function ($item) {
            $checked = 0;
            foreach ($this->user as $value) {
                if ($item->id == $value['id']) {
                    $checked = 1;
                    break;
                }
            }
            Carbon::parse($this->day);
            $currentUser = UserInf::where('id', $item->id)->first();
            $department = !empty($currentUser->department) ? $currentUser->department->name : null;
            return [
                'id' => $item->id,
                'checked' => $checked,
                'fullname' => $item->fullname,
                'department' => $department,
                'asset' => [],
            ];
        });
        $this->modalUserList = collect($modal_user_list)->all();
    }

    public function getDataList() {
        if (count($this->data) > 0) {
            $user_allocated = UserInf::whereIn('id', $this->data)->get();
            $user_allocated = $user_allocated->map(function ($item) {
                Carbon::parse($this->day);
                $currentUser = UserInf::where('id', $item->id)->first();
                $department = !empty($currentUser->department) ? $currentUser->department->name : null;
                return [
                    'id' => $item->id,
                    'code' => $item->code,
                    'fullname' => $item->fullname,
                    'asset' => [],
                    'checked' => 0,
                    'department' => $department
                ];
            });
            $user_allocated = collect($user_allocated)->all();
            $this->user = [];
            foreach ($this->data as $index => $id) {
                foreach ($user_allocated as $item) {
                    if ($id == $item['id']) {
                        array_unshift($this->user, $user_allocated[$index]);
                    }
                }
            }
        } else {
            $this->user = [];
        }
    }

    public function deleteUser($index) {
        $this->deleteSelected = $index;
        if (count($this->user) == 1) {
           $this->user = [];
        }else {
            array_splice($this->user, $index, 1);
        }
        $this->indexSelected = 0;
    }

    public function openModalUser () {
        $this->emit('showModalUser');
    }

    public function openModalAsset() {
        $this->emit('showModalAsset');
    }

    public function chooseUser($id) {
        foreach ($this->modalUserList as $index => $value) {
            if ($this->modalUserList[$index]['id'] == $id) {
                if (count($this->user) > 0) {
                    $exist = false;
                    foreach ($this->user as $ind => $user) {
                        if ($user['id'] == $id) {
                            $this->deleteUser($ind);
                            $exist = true;
                            array_push($this->user, $this->modalUserList[$index]);
                            break;
                        }
                    }
                    if (!$exist) {
                        array_push($this->user, $this->modalUserList[$index]);
                    }
                } else {
                    array_push($this->user, $this->modalUserList[$index]);
                }
            }
        }
    }

    public function chooseAsset($id) {
        $this->chooseAll = 2;
        if (!in_array($id, $this->idAssetSelected)) {
            array_push($this->idAssetSelected, $id);
        } else {
            array_splice($this->idAssetSelected, array_search($id, $this->idAssetSelected), 1);
        }
    }

    public function cancelAllAsset() {
        $this->idAssetSelected = [];
    }

    public function saveAsset() {
        $this->errorAsset = null;
        $this->user[$this->indexSelected]['asset'] = [];
        foreach ($this->modalAssetList as $index => $asset) {
            $this->modalAssetList[$index]['checked'] = 0;
        }
        if (count($this->idAssetSelected) > 0) {
            foreach ($this->idAssetSelected as $item) {
                foreach ($this->modalAssetList as $index => $asset) {
                    if ($asset['id'] == $item && !$this->modalAssetList[$index]['owned']) {
                        array_push($this->user[$this->indexSelected]['asset'], $this->modalAssetList[$index]);
                        $this->modalAssetList[$index]['checked'] = 1;
                        $this->modalAssetList[$index]['owner'] = true;
                    }
                }
            }
        }
    }

    public function deleteFileUpload($index) {
        array_splice($this->fileUpload, $index, 1);
    }

    public function validateData() {
        if (count($this->user) == 0) {
            $this->errorUser = __('validation.required', [
                'attribute' => __('data_field_name.allocated-revoke.employee')
            ]);
        }
        if (count($this->user) > 0) {
            foreach ($this->user as $index => $item) {
                if (count($item['asset']) == 0) {
                    $this->indexSelected = $index;
                    $this->errorAsset = __('validation.required', [
                        'attribute' => __('data_field_name.allocated-revoke.asset')
                    ]);
                } else {
                    foreach ($item['asset'] as $ind => $value) {
                        if ($value['implementation_quantity'] <= 0) {
                            $this->indexSelected = $index;
                            $this->user[$index]['asset'][$ind]['error'] = __('validation.gt.numeric', [
                                'attribute' => __('data_field_name.allocated-revoke.quantity'),
                                'value' => 0
                            ]);
                            $this->errorQuantity = __('validation.gt.numeric', [
                                'attribute' => __('data_field_name.allocated-revoke.quantity'),
                                'value' => 0
                            ]);
                        }
                        if (!empty($value['quantity']) && $value['quantity'] < $value['implementation_quantity'] || empty($value['quantity'])) {
                            $this->indexSelected = $index;
                            $this->user[$index]['asset'][$ind]['error'] = __('validation.lt.numeric', [
                                'attribute' => __('data_field_name.allocated-revoke.quantity'),
                                'value' => __('data_field_name.allocated-revoke.unused_quantity')
                            ]);
                            $this->errorQuantity = __('validation.lt.numeric', [
                                'attribute' => __('data_field_name.allocated-revoke.quantity'),
                                'value' => __('data_field_name.allocated-revoke.unused_quantity')
                            ]);
                        }
                    }
                }
            }
            $this->disable = false;
        }
        $this->validate([
            'day' => ['required'],
            'reason' => ['max:255'],
            'number_report' => ['required', 'max:10', 'regex:/^[A-Z0-9]+$/u'],
        ],[],[
            'day'=> __('data_field_name.allocated-revoke.allocated_date'),
            'reason'=> __('data_field_name.allocated-revoke.reason'),
            'number_report'=> __('data_field_name.allocated-revoke.number_report'),
            'fileUpload' => 'File'
        ]);
    }

    public function saveData($export) {
        $this->errorFile = null;
        $this->errorUser = null;
        $this->errorAsset = null;
        $this->errorQuantity = null;
        $this->disable = true;
        $this->validateData();
        if (empty($this->errorQuantity)) {
            return DB::transaction(function() use ($export) {
                $day_allocated = Carbon::parse($this->day);
                $claimForm = new ClaimForm();
                $claimForm->claim_date = $day_allocated;
                $claimForm->reason = $this->reason;
                $claimForm->type = 4;
                $claimForm->save();
                if (empty($this->errorUser) && empty($this->errorAsset)) {
                    foreach ($this->user as $user) {
                        foreach ($user['asset'] as $asset) {
                            $modalAsset = Asset::findOrFail($asset['id']);
                            $modalAsset->situation = 1;
                            $modalAsset->status = 1;
                            $modalAsset->user_info_id = $user['id'];
                            $modalAsset->amount_used = $asset['implementation_quantity'];
                            $modalAsset->save();

                            DB::table('asset_claim_form')->insert(
                                [
                                    'form_id' => $claimForm->id,
                                    'asset_id' => $asset['id'],
                                    'quantity' => $asset['implementation_quantity'],
                                    'user_info_id' => $user['id'],
                                ]
                            );

                            $allocated = new AssetAllocatedRevoke();

                            $allocated->asset_id = $asset['id'];
                            $allocated->status = 1;
                            $allocated->implementation_date = $day_allocated;
                            $allocated->used_type = 1;
                            $allocated->number_report = $this->number_report;
                            $allocated->user_info_id = $user['id'];
                            $allocated->implementation_reason = $this->reason;
                            $allocated->implementation_quantity = $asset['implementation_quantity'];
                            $allocated->claim_form_id = $claimForm->id;
                            $allocated->save();
                        }
                    }
                    $this->storeFile($claimForm->id);
                    if ($export) {
                        $this->emit('doExport');
                    } else {
                        $this->resetForm();
                        $this->getDataList();
                        session()->flash('success', __('data_field_name.allocated-revoke.success.allocated'));
                        return redirect()->route('admin.asset.allocated-revoke.index');
                    }
                }
            });
        }
    }

    public function resetForm() {
        $this->reason = null;
        $this->number_report = null;
        $this->fileUpload = [];
    }

    public function storeFile($id_model) {
        foreach ($this->fileUpload as $item) {
            $file = new FileUpload();
            $dataUpload= $file->uploadFile($item, $this->folder, $this->model_name);
            $file_upload = new File();
            $file_upload->url = $dataUpload['url'];
            $file_upload->size_file = $dataUpload['size_file'];
            $file_upload->file_name = $dataUpload['file_name'];
            $file_upload->model_name = $this->model_name;
            $file_upload->model_id = $id_model;

            $file_upload->type = $this->type;
            $file_upload->save();
        }
    }

    public function updateQuantity($userIndex, $assetIndex, $value) {
        $this->user[$userIndex]['asset'][$assetIndex]['implementation_quantity'] = $value;
    }

    public function export() {
        $total = 0;
        $value = [];
        $data_export = $this->user;
        $numberReport = $this->number_report;
        $reason_allocated = $this->reason;
        $this->emit('doResetData');
        foreach ($data_export as $user) {
            $modelUser = UserInf::findOrFail($user['id']);
            if (!empty($modelUser)) {
                $userName = $modelUser->fullname;
                $position = !empty($modelUser->position) ? $modelUser->position->v_value : null;
                $department = !empty($modelUser->department) ? $modelUser->department->name : null;
                $code = $modelUser->code;
            } else {
                $userName =  null;
                $position = null;
                $department = null;
                $code = null;
            }

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template/template_allocated.docx');
            $templateProcessor->setValue('numberReport', $numberReport);
            $templateProcessor->setValue('day', now()->format(Config::get('common.formatDate')));
            $templateProcessor->setValue('userName', $userName);
            $templateProcessor->setValue('position', $position);
            $templateProcessor->setValue('department', $department);
            $templateProcessor->setValue('userCode', $code);
            $templateProcessor->setValue('reason', $reason_allocated);
            foreach ($user['asset'] as $ind => $asset) {
                $modelAsset = Asset::findOrFail($asset['id']);
                $total += $asset['implementation_quantity'];
                $data_export = [
                    'index' => $ind + 1,
                    'assetName' => $modelAsset->name,
                    'assetCode' => $modelAsset->code,
                    'assetSerial' => $modelAsset->series_number,
                    'assetQuantity' => $asset['implementation_quantity'],
                    'assetUnitType' => $modelAsset->unit_type,
                    'assetNote' => $modelAsset->note
                ];
                array_push($value, $data_export);
            }
        }

        $templateProcessor->cloneRowAndSetValues('index', $value);
        $templateProcessor->setValue('total', $total);
        $today = date("d_m_Y");
        $docxFile = 'allocated_' . $today . '.docx';
        $path  = storage_path("app/uploads/" . $docxFile);
        $templateProcessor->saveAs($path);
        $this->export_file = true;
        return response()->download($path);
    }
    public function resetData() {
        if ($this->export_file) {
            $this->export_file = false;
            session()->flash('success', __('data_field_name.allocated-revoke.success.revoke'));
            return redirect()->route('admin.asset.allocated-revoke.index');
        }
        $this->resetForm();
        $this->getDataList();
        $this->disable = false;
    }

    public function chooseAllAsset($value) {
        $this->chooseAll = $value;
        $this->chooseAllBy = $this->indexSelected;
        if ($value == 1) {
            foreach ($this->modalAssetList as $asset) {
                if (!in_array($asset['id'], $this->idAssetSelected)) {
                    array_push($this->idAssetSelected, $asset['id']);
                }
            }
        } else {
            $this->idAssetSelected = [];
        }
    }

    public function setAllocatedDay($data) {
        $this->day= date('Y-m-d', strtotime($data['allocatedDay']));
    }

    public function selectUser($index) {
        $this->indexSelected = $index;
    }
}
