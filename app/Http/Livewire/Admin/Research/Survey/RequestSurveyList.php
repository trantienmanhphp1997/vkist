<?php

namespace App\Http\Livewire\Admin\Research\Survey;

use App\Models\SurveyRequest;
use App\Models\UserInf;
use App\Models\MasterData;
use App\Component\Recursive;
use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Enums\ESurveyRequestStatus;
use App\Enums\ESurveyRequestResult;

class RequestSurveyList extends BaseLive {

    public $searchTerm;
    public $result;
    public $status;
    public $field;
    public $deleteId='';

    public function render() {
        $query = SurveyRequest::query();
        $searchTerm = $this->searchTerm;

        if (strlen($this->searchTerm)) {
            $query->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($searchTerm)) . '%');
        }

        if (!is_null($this->result)) {
            if ($this->result == ESurveyRequestResult::DEFAULT) {
                $query->where('result', null);
            } else {
                $query->where('result', $this->result);
            }
        }



        if (!empty($this->status)) {
            $query->where('status', $this->status);
        }

        if (!empty($this->field)) {
            $query->where('field_id', $this->field);
        }

        $data = $query->orderBy('id','DESC')->paginate($this->perPage);
        $tmp = $data->map(function ($item, $index) {
            $field = MasterData::where('id', $item->field_id)->first();
            $performerText = $item->userInfos->pluck('fullname')->join(', ');
            return [
                'id' => $item->id,
                'name' => $item->name,
                'content' => $item->survey_content,
                'field' => !empty($field) ? $field->v_value : null,
                'perfomer' => $performerText,
                'statusStr' => ESurveyRequestStatus::valueToName($item->status),
                'status' => $item->status,
                'result' => ESurveyRequestResult::valueToName($item->result),
            ];
        });
        $data->setCollection($tmp);
        $field_data = MasterData::where('type', 22)->get();
        return view('livewire.admin.research.survey.request-survey-list',['data' => $data, 'field_data' => $field_data]);

    }
    //delete
    public function delete() {
        SurveyRequest::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }

}
