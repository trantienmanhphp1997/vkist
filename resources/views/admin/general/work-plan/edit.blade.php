@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.work_plan.edit.title')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')

    <div class="col-md-10 col-xl-11 box-detail box-user">
        {{ Form::model($data, ['route' => ['admin.general.work-plan.update', $data->id], 'method' => 'post']) }}
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.work_plan.title')}} </a> \
                    <span>{{__('data_field_name.work_plan.edit.title')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">

                <div class="detail-task information box-idea">
                    <h4>{{__('data_field_name.work_plan.edit.title')}}</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.work_plan.create.content')}}</label>
                                <input type="text" class="form-control" value="{{$data->content}}" name="contents">
                                @error('contents') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label>{{__('data_field_name.work_plan.create.mission_id')}}</label>
                                <input type="text" class="form-control" placeholder="" name="missions" value="{{old('missions') ?? $data->missions?? '' }}">
                                @error('missions') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{__('data_field_name.work_plan.create.MISSIONS_CODE')}}</label>
                                <input type="text" class="form-control" value="{{$data->missions_code}}"
                                       name="missions_code">
                                @error('missions_code') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{__('data_field_name.work_plan.create.start_time')}} <span
                                        class="text-danger">*</span></label>
                                @include('layouts.partials.input._inputDate', ['input_id' => 'start_date','name'=>"start_date", 'place_holder'=>'', 'default_date' => strtotime(str_replace('/', '-', old('start_date'))) ? date('Y-m-d',strtotime(str_replace('/', '-', old('start_date')))) : ($data->real_start_date ? date('Y-m-d',strtotime($data->real_start_date)): ($data->start_date ? date('Y-m-d',strtotime($data->start_date)): null) ) ])
                                @error('start_date') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{__('data_field_name.work_plan.create.end_time')}} <span
                                        class="text-danger">*</span></label>
                                @include('layouts.partials.input._inputDate', ['input_id' => 'end_date','name'=>"end_date",'set_null_when_enter_invalid_date' => true, 'place_holder'=>'', 'default_date' => strtotime(str_replace('/', '-', old('end_date'))) ? date('Y-m-d',strtotime(str_replace('/', '-', old('end_date')))) : ($data->real_end_date ? date('Y-m-d',strtotime($data->real_end_date)): ($data->end_date ? date('Y-m-d',strtotime($data->end_date)):null)) ])
                                @error('end_date') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.work_plan.create.fee_type')}}</label>
                                <select class="form-control form-control-lg" name="budget_id">
                                    @foreach($source_cost as $value)
                                        <option
                                            value="{{$value->id}}" {{ $data->budget_id == $value->id? 'selected': '' }} >{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.work_plan.create.fee')}}</label>
                                        <input type="text" class="form-control format_number" name="estimated_cost"
                                               value="{{numberFormat($data->estimated_cost)}}">
                                        @error('estimated_cost') <span
                                            class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.work_plan.index.priority1')}}</label>
                                        <select class="form-control form-control-lg" name="priority_level">
                                            @foreach($priority_list as $key => $value)
                                                <option
                                                    value="{{$key}}" {{ $data->priority_level == $key? 'selected': '' }} >{{$value}}</option>
                                            @endforeach
                                        </select>
                                        @error('priority_level') <span
                                            class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        @livewire('admin.general.work-plan.user-info',['work_plan_id'=>$data->id])
                        @livewire('admin.general.work-plan.business-fee',['work_plan_id'=>$data->id])
                        <div class="col-md-12">
                            <h4 class="title-plan text-uppercase">{{__('data_field_name.work_plan.edit.result')}}</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.work_plan.edit.result')}}</label>
                                <textarea type="text" class="form-control" name="result"
                                          rows="4">{{$data->result}}</textarea>
                                @error('result') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        {{--                        @livewire('admin.general.work-plan.file-upload-edit',['work_plan_id'=>$data->id])--}}
                        <div class="col-md-12">
                            @livewire('component.files', ['model_name'=>$model_name, 'type'=>$type,
                            'folder'=>$folder,'status'=>$status,'model_id'=>$data->id])
                        </div>
                        <div class="col-md-12 border-top">
                            <div class="group-btn2 text-center">
                                <button name="cancel"
                                        class="btn btn-cancel">{{__('data_field_name.work_plan.create.btn_cancel')}}</button>
                                <button type="submit" name="save"
                                        class="btn btn-save">{{__('data_field_name.work_plan.create.btn_send2')}}</button>
                                {{--<button type="submit"
                                        class="btn  float-right kb">{{__('data_field_name.work_plan.create.btn_view')}}
                                    <img src="/images/eye.svg" alt="eye"/></button>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection
