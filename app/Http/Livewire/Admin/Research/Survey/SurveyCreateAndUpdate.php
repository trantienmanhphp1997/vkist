<?php

namespace App\Http\Livewire\Admin\Research\Survey;

use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use App\Models\SurveyRequest;
use App\Models\MasterData;
use App\Component\SizeFile;
use Livewire\WithFileUploads;
use App\Component\FileUpload;
use App\Enums\EMasterDataType;
use App\Enums\ESurveyRequestStatus;
use App\Models\SurveyRequestHasUserInfo;

class SurveyCreateAndUpdate extends BaseLive
{
    public $isEdit = false;
    public $list_field = [];

    //survey info
    public $survey_request;
    public $name;
    public $field_id;
    public $status;
    public $start_date;
    public $end_date;
    public $user_perform_id;
    public $survey_target;
    public $survey_content;
    public $surveyed_unit;
    public $result;
    public $technology_demand;
    public $economic_efficiency;

    public $userInfoIds = [];

    //file upload
    public $fileUpload;
    public $listFileUpload = [];
    public $limit_total_file = 5;
    public $limit_file_size = 255; //MB
    public $accept_file;

    //file cũ đã có sẵn của survey
    public $listOldFile = [];
    //file cũ đã có sẵn của survey, đây là bản tạm dùng để hiển thị cho user xem, xoá trên list nay, khi nào lưu thì mới áp dụng thay đổi vô list chính
    public $listOldFileTemp = [];
    //danh sách các file cũ mà user muốn xoá
    public $listIdOfOldFileNeedDelete = [];
    public $current_total_file;


    use WithFileUploads;


    protected $listeners = [
        'set-start_date' => 'setStartDate',
        'set-end_date' => 'setEndDate',
        'changed-user-info-ids-event' => 'setUserInfoIds'
    ];

    public function setStartDate($data) {
        $this->start_date = $data['start_date'] ? date('Y-m-d', strtotime($data['start_date'])) : null;
    }
    public function setEndDate($data) {
        $this->end_date = $data['end_date'] ? date('Y-m-d', strtotime($data['end_date'])) : null;
    }

    public function setUserInfoIds($data) {
        $this->userInfoIds = array_keys($data);
    }

    public function updatedStatus() {
        if($this->isEdit && $this->status == ESurveyRequestStatus::DONE) {
            $this->emit('set_disable_search_combobox', true);
        } else {
            $this->emit('set_disable_search_combobox', false);
        }
    }

    public function mount() {
        $this->accept_file = Config::get('common.mime_type.general');
        if($this->survey_request){
            $this->isEdit=true;
            $this->name = $this->survey_request->name;
            $this->field_id =$this->survey_request->field_id;
            $this->status = $this->survey_request->status;
            $this->start_date = $this->survey_request->start_date ? date('Y-m-d', strtotime($this->survey_request->start_date)) : null;
            $this->end_date = $this->survey_request->end_date ? date('Y-m-d', strtotime($this->survey_request->end_date)) :null;

            $this->survey_target = $this->survey_request->survey_target;
            $this->survey_content = $this->survey_request->survey_content;
            $this->surveyed_unit = $this->survey_request->surveyed_unit;

            $this->result = $this->survey_request->result;
            $this->technology_demand = $this->survey_request->technology_demand;
            $this->economic_efficiency = $this->survey_request->economic_efficiency;

            $this->userInfoIds = $this->survey_request->userInfos->pluck('id')->all();

            //get document
            $model_name = SurveyRequest::class;
            $type = Config::get('common.type_upload.SurveyRequest');
            $model_id = $this->survey_request->id;
            $this->listOldFile=\App\Models\File::where('model_name',$model_name)->where('type', $type)->where('model_id',$model_id)->get();

             //đẩy list file cũ qua list tạm
            foreach($this->listOldFile as $item) {
                $infoOldFile = [
                    'id' => $item->id,
                    'file_name' => $item->file_name,
                    'size_file' => $item->size_file,
                    'url' => $item->url,
                ];
                array_push($this->listOldFileTemp, $infoOldFile);
            }
        }

        $this->list_field = MasterData::where('type', EMasterDataType::FIELD)->get();
    }

    public function deleteNewFileUpload($index) {
        unset($this->listFileUpload[$index]);
    }

    public function deleteFileOldTemp($index) {
        if(isset($this->listOldFileTemp[$index])) {
            array_push($this->listIdOfOldFileNeedDelete, $this->listOldFileTemp[$index]['id']);
            unset($this->listOldFileTemp[$index]);
        }
    }

    public function render()
    {
        return view('livewire.admin.research.survey.survey-create-and-update');
    }

    public function validateData() {

        $rules= [
            'name' => 'required | max:100',
            'field_id' => 'required',
            'status' => 'required',
            'start_date' => 'required',
            'end_date' => $this->start_date ? 'required | after_or_equal:' .$this->start_date : 'required',
            'survey_target' => 'required | max:1000',
            'survey_content'  => 'required | max:1000',
            'surveyed_unit' => 'required | max:1000',
            'userInfoIds' => 'required|array'
        ];
        $att = [
            'name' => __('data_field_name.request-survey-list.form_create_edit.name'),
            'field_id' => __('data_field_name.request-survey-list.form_create_edit.field'),
            'status' => __('data_field_name.request-survey-list.form_create_edit.status'),
            'start_date' => __('data_field_name.request-survey-list.form_create_edit.start_date'),
            'end_date' => __('data_field_name.request-survey-list.form_create_edit.end_date'),
            'survey_target' => __('data_field_name.request-survey-list.form_create_edit.survey_target'),
            'survey_content'  => __('data_field_name.request-survey-list.form_create_edit.survey_content'),
            'surveyed_unit' => __('data_field_name.request-survey-list.form_create_edit.surveyed_unit'),
            'userInfoIds'  => __('data_field_name.request-survey-list.perfomer'),
        ];
        if($this->status == ESurveyRequestStatus::DONE) {
            $rules['result'] = 'required';
            $rules['technology_demand'] = 'required | numeric | max:100 |min:0';
            $rules['economic_efficiency'] = 'required | numeric | max:100 |min:0';

            $att['result'] = __('data_field_name.request-survey-list.form_create_edit.result');
            $att['technology_demand'] = __('data_field_name.request-survey-list.form_create_edit.technology_demand_error_message');
            $att['economic_efficiency'] = __('data_field_name.request-survey-list.form_create_edit.economic_efficiency_error_message');
        }

        $this->validate($rules, ['end_date.after_or_equal' => __('data_field_name.request-survey-list.form_create_edit.end_date_after_error_message')], $att);
    }

    public function download($url, $name) {
        return response()->download('storage/'.$url, $name);
    }


    public function updatedFileUpload() {
        $this->current_total_file = count($this->listFileUpload) + count($this->listOldFileTemp);
        $rules = [
            'fileUpload' => [
                'file',
                'mimes:' . implode(',', $this->accept_file),
                'max:' . $this->limit_file_size * 1024
            ],
            'current_total_file' => 'numeric | max:'. ($this->limit_total_file - 1)
        ];
        $error_message = [
            'fileUpload.mimes' => __('notification.upload.mime_type'),
            'fileUpload.max' => __('notification.upload.maximum_size', ['value' => $this->limit_file_size]),
            'current_total_file.max' => __('notification.upload.maximum_uploads')
        ];
        $this->validate($rules,$error_message);
        $size = new SizeFile();
        $size=$size->sizeFile($this->fileUpload->getSize());
        $filenameOrigin = $this->fileUpload->getClientOriginalName();
        $fileUploadInfo = [
            'file_name' => $filenameOrigin,
            'size_file' => $size,
            'file' => $this->fileUpload,
        ];
        array_push($this->listFileUpload, $fileUploadInfo);
        $this->fileUpload = null;
    }

    public function store(){

        $this->technology_demand = intval($this->technology_demand);
        $this->economic_efficiency = intval($this->economic_efficiency);
        if($this->isEdit) {
            parent::edit($this->survey_request->id);
        } else {
            parent::store();
        }
        $this->validateData();

        if(is_null($this->survey_request)) {
            $this->survey_request = new SurveyRequest();
        }

        $this->survey_request->name = $this->name;
        $this->survey_request->field_id = $this->field_id;
        $this->survey_request->status = $this->status;
        $this->survey_request->start_date = $this->start_date;
        $this->survey_request->end_date = $this->end_date;
        $this->survey_request->survey_target = $this->survey_target;
        $this->survey_request->survey_content = $this->survey_content;
        $this->survey_request->surveyed_unit = $this->surveyed_unit;
        $this->survey_request->result = $this->result;
        $this->survey_request->technology_demand = $this->technology_demand;
        $this->survey_request->economic_efficiency = $this->economic_efficiency;

        $this->survey_request->save();

        // //save file
        $fileUploadComponent=new FileUpload();
        $model_name = SurveyRequest::class;
        $type = Config::get('common.type_upload.SurveyRequest');
        $model_id = $this->survey_request->id;
        $folder = app($model_name)->getTable();
        foreach($this->listFileUpload as $file) {
            $dataUpload= $fileUploadComponent->uploadFile($file['file'],$folder);
            $file_upload = new \App\Models\File();
            $file_upload->url = $dataUpload['url'];
            $file_upload->size_file = $dataUpload['size_file'];
            $file_upload->file_name = $dataUpload['file_name'];
            $file_upload->model_name = $model_name;
            $file_upload->model_id = $model_id;
            $file_upload->type = $type;
            $file_upload->save();
        }

        SurveyRequestHasUserInfo::query()->where('survey_request_id', $this->survey_request->id)->delete();
        $this->survey_request->userInfos()->attach($this->userInfoIds);

        //xoá các file đính kèm cần xoá
        foreach($this->listOldFile as $item) {
            if(in_array($item->id, $this->listIdOfOldFileNeedDelete)) {
                $item->delete();
            }
        }

        if($this->isEdit) {
            session()->flash('success', __('notification.common.success.edit'));
        } else {
            session()->flash('success', __('notification.common.success.add'));
        }

        return redirect()->route('admin.research.survey.index');
    }

}
