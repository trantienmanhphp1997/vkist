<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetClaimFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_claim_form', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('form_id');
            $table->bigInteger('asset_id');
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('Người sư dụng');
            $table->foreignId('department_id')->nullable()->constrained('department')->comment('Đơn vị sử dụng');
            $table->integer('quantity')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('asset_claim_form');
    }
}
