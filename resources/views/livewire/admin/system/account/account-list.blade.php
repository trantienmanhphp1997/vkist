<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{ __('data_field_name.system.account.user_control') }}</a> \<span>{{ __('data_field_name.system.account.user_list') }}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{ __('data_field_name.system.account.user_list') }}</h3>
            <div class="col-md-12 approve">
                <div id="search" class="w-100 ">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="search-expertise inline-block mb-24 w-xs-100 ">
                                <input type="text" class="form-control w-100 size13" placeholder="{{ __('data_field_name.system.account.search') }}" name="searchTerm" autocomplete="off" wire:model.debounce.1000ms="searchTerm">
                                <span>
                                    <img src="/images/Search.svg" alt="search">
                                </span>
                            </div>


                        <div class="w-170 inline-block position-relative">
                            <select id="" class="form-control size13" wire:model.debounce.1000ms="searchStatus">
                                <option value="">{{ __('data_field_name.system.account.status') }}</option>
                                <option value="1">{{ __('data_field_name.system.account.active') }}</option>
                                <option value="0">{{ __('data_field_name.system.account.not_active') }}</option>

                            </select>
                        </div>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                            <a class="btn btn-viewmore-news mr0 w-xs-100 mb-24" href="{{ route('admin.system.account.create') }}">
                                <img src="/images/plus2.svg" alt="plus">
                                {{ __('data_field_name.system.account.create') }}
                            </a>
                        </div>

                    </div>
                </div>
                <div class="table-responsive">
                <table class="table table-general" id="coUser" style="height:100%;">
                    <thead>
                        <tr class="title-approve">
                            <th class="rounded-left" scope="col colUser">{{ __('data_field_name.system.account.stt') }}</th>
                            <th scope="col colUser">{{ __('data_field_name.system.account.user_name') }}</th>
                            <th scope="col colUser">{{ __('data_field_name.system.account.user_code') }}</th>
                            <th scope="col colUser">{{ __('data_field_name.system.account.name') }}</th>
                            <th scope="col colUser">{{ __('data_field_name.system.account.email') }}</th>
                            <th scope="col colUser">{{ __('data_field_name.system.account.phone') }}</th>
                            <th scope="col colUser">{{ __('data_field_name.system.account.user_group') }}</th>
                            <th scope="col colUser">{{ __('data_field_name.system.account.status') }}</th>
                            <th class="rounded-right" scope="col colUser">{{ __('data_field_name.system.account.action') }}</th>
                        </tr>
                    </thead>
                    <div wire:loading class="loader"></div>
                    <tbody>
                        @forelse($data as $row)
                            <tr>
                                <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                <td><a href="" data-toggle="modal" data-target="#detailAccount" wire:click="detail({{ $row->id }})">{!! boldTextSearch($row->username, $searchTerm) !!}</a></td>
                                <td>{!! boldTextSearch($row->info->code ?? null, $searchTerm) !!}</td>
                                <td>{!! boldTextSearch($row->name, $searchTerm) !!}</td>
                                <td>{!! boldTextSearch($row->email, $searchTerm) !!}</td>
                                <td>{!! boldTextSearch($row->info->phone ?? '', $searchTerm) !!}</td>
                                <td>
                                    {{ $row->group->name ?? '' }}
                                </td>
                                <td><?php
                                if ($row->status == 0) {
                                    echo __('data_field_name.system.account.not_active');
                                } elseif ($row->status == 1) {
                                    echo __('data_field_name.system.account.active');
                                } else {
                                    echo '';
                                }
                                ?></td>
                                <td>
                                    @if ($checkEditPermission)
                                        <a title="{{__('common.button.edit')}}" class="btn-sm border-0 bg-transparent mr-1" href="{{ route('admin.system.account.edit', ['id' => $row->id]) }}"><img src="/images/edit.png" alt="edit"></a>
                                        <button type="button" class="btn par6" wire:click="toggleLockUser({{ $row->id }})" title="@if (!is_null($row->status) && $row->status == 0){{ __('data_field_name.system.account.unlock') }} @else {{ __('data_field_name.system.account.lock') }} @endif">
                                            <img src="@if (!is_null($row->status) && $row->status == 0) /images/Unlock.svg @else /images/Lock.svg @endif" alt="">
                                        </button>
                                        <button type="button" class="btn btn-sm" wire:click="recoveryPassword({{ $row->id }})" title="{{ __('data_field_name.system.account.recovery_password') }}">
                                            <img src="/images/Key.svg" alt="">
                                        </button>
                                    @endif
                                    @if ($checkDestroyPermission)
                                        <button type="button" data-toggle="modal" data-target="#deleteModal" title="Xóa" wire:click="getIdDelete({{ $row->id }})" class="btn-sm border-0 bg-transparent"><img src="/images/trash.svg" alt="trash"></button>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr class="text-center text-danger">
                                <td colspan="8">{{ __('data_field_name.common_field.empty') }}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
                {{ $data->links() }}
                <div id="pageUser" class="title-approve "></div>
            </div>
        </div>
    </div>
    <!-- Modal Delete -->
    <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title" id="exampleModalLabel">{{ __('notification.member.warning.warning') }}</h4>
                    {{ __('notification.member.warning.warning_delete?') }}
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.back') }}</button>
                    <button type="button" class="btn btn-save" data-dismiss="modal" wire:click="delete()">{{ __('common.button.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Delete -->
    <!-- Modal Detail -->
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="detailAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ __('data_field_name.system.account.detail_account') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{ __('data_field_name.system.account.user_name') }}</label>
                                <input type="text" readonly class="form-control" id="exampleFormControlInput1" placeholder="Nhập tên...." wire:model.lazy="username">
                                @error('username') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{ __('data_field_name.system.account.user_code') }}</label>
                                <input type="text" readonly class="form-control" id="exampleFormControlInput1" placeholder="Nhập tên...." wire:model.lazy="code">
                                @error('code') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{ __('data_field_name.system.account.name') }}</label>
                                <input type="text" readonly class="form-control" id="exampleFormControlInput1" placeholder="Nhập tên...." wire:model.lazy="name">
                                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{ __('data_field_name.system.account.email') }}</label>
                                <input type="text" readonly class="form-control" id="exampleFormControlInput1" placeholder="Nhập tên...." wire:model.lazy="email">
                                @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{ __('data_field_name.system.account.phone') }}</label>
                                <input type="text" readonly class="form-control" id="exampleFormControlInput1" placeholder="Nhập tên...." wire:model.lazy="phone">
                                @error('phone') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput3">{{ __('data_field_name.system.account.status') }}</label>
                                <select name="status" disabled id="exampleFormControlInput3" class="form-control" wire:model.lazy="status">
                                    <option value="0">{{ __('data_field_name.system.account.not_active') }}</option>
                                    <option value="1">{{ __('data_field_name.system.account.active') }}</option>
                                </select>
                                @error('status') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="close-modal-create-department" class="btn btn-secondary close-btn" data-dismiss="modal">{{ __('common.button.close') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Detail -->
</div>
