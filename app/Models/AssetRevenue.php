<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetRevenue extends Model
{
    use HasFactory;
    protected $table = 'asset_revenue';
    protected $guarded = ['*'];
    protected $fillable = ['report_year', 'report_month', 'quantity', 'total_money'];
}
