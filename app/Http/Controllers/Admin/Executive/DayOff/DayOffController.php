<?php

namespace App\Http\Controllers\Admin\Executive\DayOff;

use App\Http\Controllers\Controller;
use DB;

class DayOffController extends Controller {

    public function MyDayOff() {
        return view('admin.executive.my-day-off.index');
    }

    public function ApplyForLeave() {
        return view('admin.executive.my-day-off.apply-for-leave');
    }
    public function ApplyForLeaveEdit($id) {
        return view('admin.executive.my-day-off.apply-for-leave-edit',['id' => $id]);
    }
}
