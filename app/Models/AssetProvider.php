<?php

namespace App\Models;


use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AssetProvider extends BaseModel
{
    use HasFactory;

    protected $table = 'asset_provider';
    protected $guarded = ['*'];
    protected $fillable = ['code', 'name'];

    private static $searchable = [
        'name', 'code'
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }

    public function bankName() {
        return $this->belongsTo(MasterData::class, 'bank_name_id');
    }

}
