<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Str;

class ContractLivewireTest extends TestCase {

    use WithFaker, RefreshDatabase;

    /** @test  */
    public function testSaveSuccessContract() {
        $user = User::first();
        if (empty($user)) {
            $user = User::factory()->create();
        }
        Livewire::actingAs($user)->test('admin.executive.contract-and-salary.labor-contract.list-data', [
            'user_info_id' =>'1',
            'contract_type' => '1',
            'contract_code' => Str::random(6),
            'duration_id' => 1,
            'status' => 1,
            'working_type' => 2,
            'start_date' => now(),
            'end_date' => now(),
            'basic_salary' => '1000',
            'social_insurance_salary' => '2',
            'coefficients_salary' => '2',
            'bank_account_number' => '22311231123111',
            'bank_name' => 'ACB',
            'note' => 'content of contract',
        ])
        ->call('saveData')->assertHasNoErrors(['user_info_id', 'contract_type', 'contract_code', 'duration_id', 'status', 'working_type', 'start_date', 'end_date', 'basic_salary', 'social_insurance_salary', 'bank_account_number', 'bank_name', 'note']);
    }

    public function testListContractLivewireComponent() {
        $user = User::first();
        if (empty($user)) {
            $user = User::factory()->create();
        }
        Livewire::actingAs($user)->test('admin.executive.contract-and-salary.labor-contract.list-data')
        ->call('render');
    }
}
