<?php

namespace App\Http\Livewire\Admin\Budget;

use App\Enums\EBudgetStatus;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Audit;
use App\Models\Budget;
use App\Models\File;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class BudgetHistoryChange extends BaseLive
{
    public $ids;

    public function render()
    {
        $query = Budget::query()->whereIn('status',[EBudgetStatus::DONE,EBudgetStatus::WAIT_CHANGE]);
        $file = [];
        if ($this->ids) {
            $file = File::whereIn('id', $this->ids)->get();
        }


        $data = $query->with(['department', 'researchProject'])->orderBy('budget.id', 'DESC')->paginate($this->perPage);

        return view('livewire.admin.budget.budget-history-change', ['data' => $data, 'file' => $file]);
    }

    public function listFile($id)
    {
        $files = File::where('model_id', $id)->where('model_name', Budget::class)->where('type', Config::get('common.type_upload.Budget'))->get();
        $id = [];
        foreach ($files as $file) {
            $id[] = $file->id;
        }
        $this->ids = $id;
    }

    public function download($id)
    {

        $file = File::where('id', $id)->get();

        foreach ($file as $files) {
            $link_file = storage_path('app/' . $files->url);

        }
        return response()->download($link_file);
    }
}
