<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="breadcrumbs">
        <div class="breadcrumbs"><a href="{{ route('admin.reality_expense.index') }}">{{__('data_field_name.reality_expense.research_expense_control')}}</a> \ <span>{{__('data_field_name.reality_expense.detail')}}</span></div>
    </div>
    <div class="creat-box bg-content  border-20">
        <div class="detail-task pt-44">
            <h4 class="text-center">{{__('data_field_name.reality_expense.expense_detail')}}</h4>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-asset pt-24">{{__('data_field_name.reality_expense.topic_infomation')}}</h3>
                </div>
            </div>
            <div class="box-calendar col-md-12 bg-white">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.topic_code')}} <span class="text-danger">*</span></label>
                            @livewire('component.search-combobox', ['model_name' => App\Models\Topic::class, 'filters' => ['code'], 'value_render_on_combobox' => 'code','targetId' => $topic_id ?? null])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.code_project')}}</label>
                            <input type="text" disabled class="form-control " wire:model.defer="topic_code">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.topic_name')}}</label>
                            <input type="text" disabled class="form-control " wire:model.defer="topic_name">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <h3 class="title-asset pt-24">{{__('data_field_name.reality_expense.approved_budget')}}</h3>
                </div>
                <div class="col-md-4 text-right">
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#listexpense" class="tooglemp">
                        <img class="pr-0 btn btn-main bg-7  text-9 size14 py-24 px-20 btn-minges" src="/images/minus2.svg" alt=""/>
                        <img class="pr-0 btn btn-main bg-7  text-9 size14 py-12 px-16 btn-plus" src="/images/plususer.svg" alt="plus"/>
                    </a>
                </div>
            </div>
            <div class="box-calendar col-md-12 bg-white " id="listexpense">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.expenses_type')}} <span class="text-danger">*</span></label>
                            <select disabled name="" id="" wire:model.lazy="type_expense" class="form-control ">
                                <option value="">{{__('data_field_name.reality_expense.select_expense')}}</option>
                                @foreach($topicFeeList as $value)
                                <option value="{{$value->id}}">--{{$value->name}}--</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.payform')}} <span class="text-danger">*</span></label>
                            {{ Form::select('source', ['' =>__('data_field_name.reality_expense.select_payform')] + $payform, null, ['class' => 'form-control ', 'wire:model.lazy' => 'payment','disabled']) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.souce_capital')}} <span class="text-danger">*</span></label>
                            {{ Form::select('source', ['' =>__('data_field_name.reality_expense.select_capital')] + $capital, null, ['class' => 'form-control ', 'wire:model.lazy' => 'source_capital','disabled']) }}
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.typepay')}} <span class="text-danger">*</span></label>
                            {{ Form::select('source', ['' =>__('data_field_name.reality_expense.select_typepay')] + $typepay, null, ['class' => 'form-control ', 'wire:model.lazy' => 'type_cost','disabled']) }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-0">
                            <label>{{__('data_field_name.reality_expense.browse')}} (VNĐ)</label>
                            <input type="text" wire:model.lazy="approval_costs" class="form-control format_number  ">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-0">
                            <label>{{__('data_field_name.reality_expense.total_money_cost')}} (VNĐ) <span class="text-danger">*</span></label>
                            <input type="text" wire:model.lazy="total_cost" class="form-control format_number ">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group  mb-0">
                            <label>{{__('data_field_name.reality_expense.money_remain')}}(VNĐ)</label>
                            <input type="text" disabled wire:model.lazy="money_remain" class="form-control  bg-content format_number">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <h3 class="title-asset pt-24">{{__('data_field_name.reality_expense.expense_infomation')}}</h3>
                </div>
                <div class="col-md-4 text-right">
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#listexpense2" class="tooglemp">
                        <img class="pr-0 btn btn-main bg-7  text-9 size14 py-24 px-20 btn-minges" src="/images/minus2.svg" alt=""/>
                        <img class="pr-0 btn btn-main bg-7  text-9 size14 py-12 px-16 btn-plus" src="/images/plususer.svg" alt="plus"/>
                    </a>

                </div>
            </div>
            <div class="box-calendar col-md-12 bg-white" id="listexpense2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.requester')}}</label>
                            <input type="text" disabled value="{{Auth::user()->name}}" class="form-control bg-content">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.expense_task')}} <span class="text-danger">*</span></label>
                            <select name="" disabled wire:model.lazy="task_id_save" class="form-control  ">
                                <option value="">--Chọn công việc--</option>
                                @foreach($expense_task as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('data_field_name.reality_expense.note')}} <span class="text-danger">*</span></label>
                            <select name="" wire:model.lazy="note_id_save" class="form-control  ">
                                <option value="">--{{__('data_field_name.reality_expense.select_content')}}--</option>
                                @if($type == \App\Enums\ETopicFeeDetail::TYPE_EXPERT)
                                @foreach($note as $value)
                                <option value="{{$value->id}}">{{$value->content}}</option>
                                @endforeach
                                @elseif($type == \App\Enums\ETopicFeeDetail::TYPE_PEOPLE)
                                @foreach($note as $value)
                                <option value="{{$value->id}}">{{$value->task->name ?? ''}}</option>
                                @endforeach
                                @else
                                @foreach($note as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>{{__('data_field_name.reality_expense.money_must_cost')}} (VNĐ) <span class="text-danger">*</span></label>
                            <input type="text" class="form-control format_number " wire:model.lazy="money_cost">
                        </div>
                        <div class="form-group mb-0">
                            <label>{{__('data_field_name.reality_expense.disbursement_disduration')}}</label>
                            <input type="date" disabled wire:model.defer="disbursement_date" class="form-control ">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>{{__('data_field_name.reality_expense.status_dibursed')}} <span class="text-danger">*</span></label>
                            {{ Form::select('source', ['' =>__('data_field_name.reality_expense.select_status')] + $status_disbursed, null, ['class' => 'form-control', 'wire:model.lazy' => 'status','disabled']) }}
                        </div>
                        @if($status ==\App\Enums\EDisbursedStatus::NOT_DISBURSED)
                        <div class="form-group mb-0">
                            <label>{{__('data_field_name.reality_expense.remain_day')}} ({{__('data_field_name.reality_expense.day')}})</label>
                            <input type="text" disabled wire:model.defer="remain_day" class="form-control ">
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @if($status ==\App\Enums\EDisbursedStatus::DISBURSED)
            <div class="box-calendar col-md-12 bg-white">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="title-asset pt-24">{{__('data_field_name.reality_expense.receipt_and_license')}}</h3>
                    </div>
                </div>
                <div class="box-calendar col-md-12 bg-white">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__('data_field_name.reality_expense.receipt')}}</label>
                                @livewire('component.files',['model_name'=>$model_name,'canUpload'=> true,'uploadOnShow'=>1, 'type'=>$type_receipt,'folder'=>$folder, 'model_id'=>$reality_expense->id,'acceptMimeTypes' => config('common.mime_type.general')])
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__('data_field_name.reality_expense.license')}}</label>
                                @livewire('component.files',['model_name'=>$model_name,'canUpload'=> true,'uploadOnShow'=>1, 'type'=>$type_license,'folder'=>$folder,'model_id'=>$reality_expense->id,'acceptMimeTypes' => config('common.mime_type.general')])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="text-center mt-42">
                <a href="{{route('admin.reality_expense.index')}}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{__('common.button.close')}}</a>
                @if($status == \App\Enums\EDisbursedStatus::DISBURSED)
                <button type="button" wire:click.prevent="store()" class="btn btn-main bg-primary text-9 size16 px-48 py-14">{{__('common.button.save')}}</button>
                @endif
            </div>
            <script>
                $("document").ready(function() {
                    $('#search_topic_info_list').select2();
                    window.addEventListener('run-select2', (e) => {
                        $('#search_topic_info_list').select2();
                    });
                    $('#search_topic_info_list').on('select2:select', function(e) {
                        var data = e.params.data;
                        Livewire.emit('choose_topic_info', data);
                    });
                });
            </script>
        </div>
    </div>
</div>