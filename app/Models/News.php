<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class News extends BaseModel
{
    use HasFactory;
    const NEWS_POSTED = 1;
    const DRAFT_NEWS = 0;
    const NEWS_NEED_APPROVAL = 2;
    const APPROVED_NEWS = 3;

    protected $fillable = ['name','image_path','admin_id','status','description','content','slug','category_news_id','status','published','is_trending','read_count','title'];

    public function category(){
        return $this->belongsTo(CategoryNews::class, 'category_news_id', 'id');
    }

    public function admin(){
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }
    public function file(){
        return $this->hasMany(File::class,'model_id','id')->where(['model_name'=>'App\Models\News']);
    }
    private static $searchable = [
        'name','content'
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
}
