<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AssetImport implements WithMultipleSheets
{

    public function sheets(): array
    {
        return [
            new FirstSheetImport()
        ];
    }
}
