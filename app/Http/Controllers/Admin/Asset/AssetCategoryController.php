<?php

namespace App\Http\Controllers\Admin\Asset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AssetCategoryController extends Controller
{
    public function index(){
        return view('admin.asset.category.index');
    }
}
