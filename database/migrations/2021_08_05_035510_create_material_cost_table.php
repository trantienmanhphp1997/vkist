<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_cost', function (Blueprint $table) {
            $table->id()->comment('Danh sach chi thue nguyen vat lieu');
            $table->string('name', 500)->nullable()->comment('Khoan chi, noi dung');
            $table->string('unit', 255)->nullable()->comment('Don vi tinh');
            $table->integer('quantity')->nullable()->comment('So luong');
            $table->bigInteger('price')->nullable()->comment('Don gia');
            $table->bigInteger('total')->nullable()->comment('Tong kinh phi');
            $table->bigInteger('state_capital')->nullable()->comment('Nguon von nha nuoc');
            $table->bigInteger('other_capital')->nullable()->comment('Nguon khac');
            $table->foreignId('topic_fee_detail_id')->nullable()->constrained('topic_fee_detail');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_cost');
    }
}
