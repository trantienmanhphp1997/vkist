<div class="col-md-10 col-xl-11 box-detail box-user list-role">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="#">{{__('news/newsManager.menu_name.news')}} </a> \
                <span>{{__('news/newsManager.menu_name.category_page')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('news/newsManager.menu_name.category_page')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="search-expertise" style="    position: relative;" class="mb-12">
                                        <input type="text" placeholder="Tìm kiếm" name="search" class="form-control"
                                               wire:model.debounce.1000ms="searchName" id='input_vn_name'
                                               autocomplete="off">
                                        <span style="left: 11px;"><img src="/images/Search.svg" alt="search"/></span>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    @if($checkCreatePermission)
                                        <button title="{{__('common.button.create')}}" class="float-right mb-24" type="button"
                                                style="border-radius: 11px; border:none;" data-toggle="modal"
                                                data-target="#create_modal" wire:click="resetInputFields">
                                            <a>
                                                <img src="/images/filteradd.png" alt="filteradd">
                                            </a>
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-genera">
                            <thead>
                            <tr class="border-radius">
                                <th scope="col" class="border-radius-left text-center">{{__('news/newsManager.menu_name.category_title_table.name')}}</th>
                                <th scope="col" class="text-center">{{__('news/newsManager.menu_name.category_title_table.author')}}</th>
                                <th scope="col" class="text-center">{{__('news/newsManager.menu_name.category_title_table.publication_time')}}</th>
                                <th scope="col" class="border-radius-right text-center">{{__('news/newsManager.menu_name.category_title_table.action')}}</th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                            @foreach($data as $row)
                                <tr>
                                    </td>
                                    <td class="text-center">{!! $row->name ?? '' !!}</td>
                                    <td class="text-center">{!! $row->admin->name ?? ' ' !!}</td>
                                    <td class="text-center">{{reFormatDate($row->created_at,"j/n/Y , H:i A")}}</td>
                                    <td class="text-center">
                                        @if($checkEditPermission)
                                            <a title="{{__('common.button.edit')}}" class="btn par6" data-target="#updateCategory" data-toggle="modal"
                                               wire:click="getCategoryInfo({{$row->id}})"><img src="/images/pent2.svg" alt=""></a>
                                        @endif
                                        @if($checkDestroyPermission)
                                            <button title="{{__('common.button.delete')}}" type="button" data-target="#exampleDelete" class="btn par6"
                                                    wire:click="setIdCategory({{$row->id}})" data-toggle="modal">
                                                <img src="/images/trash.svg" alt="trash">
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center">
                            <span>{{__('data_field_name.system.role.no_result')}}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{--dialog box--}}

    <div wire:ignore.self class="modal fade" id="create_modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="
    padding-left: 0;
">
                    <h5 class="modal-title"
                        id="exampleModalLabel">{{__('news/newsManager.menu_name.create-category.title')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    {{__('news/newsManager.menu_name.create-category.name')}}
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="proposer" class="form-control" wire:model.lazy="name"
                                       placeholder="{{__('news/newsManager.menu_name.create-category.name')}}">
                                @error('name')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-academic-create1" wire:click.prevent="resetInputFields()"
                            class="btn btn-secondary close-btn" data-dismiss="modal">{{ __('common.button.cancel') }}
                    </button>
                    <button type="button" class="btn btn-primary close-modal"
                            wire:click.prevent="store()">{{__('common.button.save')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="exampleDelete" tabindex="-1" aria-labelledby="exampleModal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{__('data_field_name.common_field.confirm_delete')}}</h4>
                    {{__('notification.member.warning.warning-1')}} <br>
                    <span class="text-danger">{{__('news/newsManager.menu_name.delete-category.warning')}}</span>
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal"
                            wire:click="resetInputFields">{{ __('common.button.cancel') }}
                    </button>
                    <button type="button" wire:click.prevent="delete()" class="btn btn-save" data-dismiss="modal">
                        {{__('common.button.delete')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="updateCategory" tabindex="-1" aria-labelledby="exampleModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="
    padding-left: 0;
">
                    <h5 class="modal-title"
                        id="exampleModalLabel">{{__('news/newsManager.menu_name.update-category.title')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    {{__('news/newsManager.menu_name.create-category.name')}}
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="hidden" name="proposer" class="form-control" wire:model.lazy="originName">
                                <input type="text" name="proposer" class="form-control" wire:model.lazy="name"
                                       placeholder="{{__('news/newsManager.menu_name.create-category.name')}}">
                                @error('name')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-academic-create1" wire:click.prevent="resetInputFields()"
                            class="btn btn-secondary close-btn" data-dismiss="modal">{{ __('common.button.cancel') }}
                    </button>
                    <button type="button" class="btn btn-primary close-modal"
                            wire:click.prevent="update()">   {{__('common.button.save')}}
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $("document").ready(() => {
        window.livewire.on('categoryCreate', () => {
            $('#create_modal').modal('hide');
            $('#create_modal').modal('hide').data('bs.modal', null);
            $('#create_modal').remove();
            $('.modal-backdrop').remove();
        });
        window.livewire.on('categoryUpdate', () => {
            $('#updateCategory').modal('hide');
            $('#updateCategory').modal('hide').data('bs.modal', null);
            $('#updateCategory').remove();
            $('.modal-backdrop').remove();
        });
    });

</script>
