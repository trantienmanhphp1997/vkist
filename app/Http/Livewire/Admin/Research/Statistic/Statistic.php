<?php

namespace App\Http\Livewire\Admin\Research\Statistic;
use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchCategory;
use App\Models\Ideal;
use App\Enums\EIdeaStatus;


use App\Models\ResearchProject;
use App\Models\TaskWork;

class Statistic extends BaseLive
{
    //project
    public $total_project = 0;
    public $complete_project;
    public $inprogress_project;
    public $waiting_project;

    //task
    public $total_task = 0;
    public $complete_task;
    public $inprogress_task;
    public $waiting_task;

    public $listInProgressTask;
    //ideal
    public $total_ideal = 0;
    public $just_create_ideal;
    public $wait_approve_ideal;
    public $rejected_ideal;
    public $approved_ideal;

    public function render()
    {

        $data_task = TaskWork::whereIn('status', [TaskWork::STATUS_UNDO_TASK, TaskWork::STATUS_ON_WORKING_TASK,TaskWork::STATUS_DONE_TASK])->get();
        $data_project = ResearchProject::whereIn('status', [ResearchProject::WAIT_APPROVAL, ResearchProject::IN_PROCESSING,ResearchProject::COMPLETED])->get();
        $data_ideal = Ideal::whereIn('status', [EIdeaStatus::JUST_CREATED, EIdeaStatus::WAIT_APPROVE,EIdeaStatus::REJECTED, EIdeaStatus::APPROVED])->get();
        $this->total_project = $data_project->count();
        $this->total_task = $data_task->count();
        $this->total_ideal = $data_ideal->count();

        $total_project_by_status = $data_project->countBy(function($element) {
            return $element->status;
        });
        $this->complete_project = $total_project_by_status[ResearchProject::COMPLETED]?? 0;
        $this->waiting_project = $total_project_by_status[ResearchProject::WAIT_APPROVAL]?? 0;
        $this->inprogress_project = $total_project_by_status[ResearchProject::IN_PROCESSING]?? 0;


        $total_task_by_status = $data_task->countBy(function($element) {
            return $element->status;
        });
        $this->complete_task = $total_task_by_status[TaskWork::STATUS_DONE_TASK]?? 0;
        $this->waiting_task = $total_task_by_status[TaskWork::STATUS_UNDO_TASK]?? 0;
        $this->inprogress_task = $total_task_by_status[TaskWork::STATUS_ON_WORKING_TASK]?? 0;


        $total_ideal_by_status = $data_ideal->countBy(function($element) {
            return $element->status;
        });

        $this->just_create_ideal = $total_ideal_by_status[EIdeaStatus::JUST_CREATED]?? 0;
        $this->wait_approve_ideal = $total_ideal_by_status[EIdeaStatus::WAIT_APPROVE]?? 0;
        $this->rejected_ideal = $total_ideal_by_status[EIdeaStatus::REJECTED]?? 0;
        $this->approved_ideal = $total_ideal_by_status[EIdeaStatus::APPROVED]?? 0;

        $this->listInProgressTask = TaskWork::where('status', TaskWork::STATUS_ON_WORKING_TASK)->orderBy('start_date', 'desc')->with('assigner')->limit(10)->get();
        return view('livewire.admin.research.statistic.index');
    }
}
