@extends('layouts.app')
    <title>{{ "Login - Hệ thống tích hợp ICT" }}</title>

@section('content')
<div class="box-login col-md-12">
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <a class="logo-login" href="/" >
                    <img style="width:180px" src="../images/Logo_VKIST_Transparent_background-08f.png" alt="logo">
                </a>
                <div class="bg-login">
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-7">
            <div class="login-box">
                <h2 >{{Cookie::get('lang') === 'vi' ? __('auth.vi.login'):__('auth.en.login')}}</h2>
                <p >{{Cookie::get('lang') === 'vi' ? __('auth.vi.title'):__('auth.en.title')}}</p>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <span><img src="/images/mail-icon.svg" alt="mail-icon"/></span>
                        <input id="username" type="username" placeholder="{{Cookie::get('lang') === 'vi' ? __('auth.vi.name'):__('auth.en.name')}}" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                        @error('username')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <span><img src="/images/Lock.svg" alt="lock"/></span>
                        <input id="password" type="password" placeholder="{{Cookie::get('lang') === 'vi' ? __('auth.vi.password'):__('auth.en.password')}}" maxlength="50" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        <img src="/images/eye.svg" alt="eye" class="eye-login" id="eyeSlash" style="display: none;" onclick="visibility()">
                        <img src="/images/eye.svg" alt="eye" class="eye-login" id="eyeShow" onclick="visibility()">
                        @error('password')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="box-checkbox checkbox-analysis left0"> {{Cookie::get('lang') === 'vi' ? __('auth.vi.remember-me'):__('auth.en.remember-me')}}
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span class="checkmark"></span>
                            </label>

                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ route('password.request') }}">
                                {{Cookie::get('lang') === 'vi' ? __('auth.vi.forgot-password'):__('auth.en.forgot-password')}}
                            </a>
                        </div>
                    </div>
                    <button type="submit" class="btn  btn-block">{{Cookie::get('lang') === 'vi' ? __('auth.vi.login'):__('auth.en.login')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function visibility() {
        var x = document.getElementById('password');
        if (x.type === 'password') {
            x.type = "text";
            $('#eyeShow').hide();
            $('#eyeSlash').show();
        }else {
            x.type = "password";
            $('#eyeShow').show();
            $('#eyeSlash').hide();
        }
    }
</script>
<script src="https://kit.fontawesome.com/02129e47ff.js"></script>
@endsection
