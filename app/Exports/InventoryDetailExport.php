<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use App\Models\AssetInventory;

class InventoryDetailExport implements FromCollection, WithHeadings, WithMapping, WithEvents, ShouldAutoSize, WithCustomStartCell
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public $inventory_id;
    public $listCategory='';

    public function __construct($inventory_id)
    {
        $this->inventory_id = $inventory_id;
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $inventory = AssetInventory::findorFail($this->inventory_id);
        $data = $inventory->assets;


        foreach($inventory->category as $category) {
            $this->listCategory.=$category->name.', ';
        }
        $this->listCategory = rtrim($this->listCategory, ', ');
        return $data;
    }

    public function headings(): array
    {
        return [
            'Code',
            'Tên tài sản',
            'Phòng/Người sử dụng',
            'Số lượng',
            'Tình trạng',
            'Số lượng',
            'Tình trạng',
            'Chêch lệch'
        ];
    }
    public function map($data): array
    {
        return [
            $data->code,
            $data->name,
            $data->department?$data->department->name:'',
            $data->quantity,
            $data->situation,
            $data->actual_quantity,
            $data->actual_situation,
            abs($data->actual_quantity-$data->quantity)?abs($data->actual_quantity-$data->quantity):'0',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells([
                        'A2:H2',
                        'A3:H3',
                        'A5:A6',
                        'B5:B6',
                        'C5:C6',
                        'D5:E5',
                        'F5:G5',
                        'H5:H6',
                    ]);
                    // ->freezePane('A3');
                $event->sheet->getDelegate()->setCellValue('A2', 'CHI TIẾT BIÊN BẢN KIỂM KÊ');
                $event->sheet->getDelegate()->setCellValue('A3', 'Nhóm sản phẩm: '.$this->listCategory);
                $event->sheet->getDelegate()->setCellValue('A5', 'Code');
                $event->sheet->getDelegate()->setCellValue('B5', 'Tên tài sản');
                $event->sheet->getDelegate()->setCellValue('C5', 'Phòng/Người sử dụng');
                $event->sheet->getDelegate()->setCellValue('D5', 'Sổ sách');
                $event->sheet->getDelegate()->setCellValue('F5', 'Thực tế');
                $event->sheet->getDelegate()->setCellValue('H5', 'Chênh lệch');


                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 9, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];

                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $newCell =  $alphabet.'5:'.$alphabet.'6';
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                    $active_sheet->getStyle($newCell)->getAlignment()->applyFromArray(
                        array('horizontal' => 'center', 'vertical'=>'center')
                    );
                    $active_sheet->getStyle($newCell)->applyFromArray($default_font_style_title);
                }
                $active_sheet->getStyle('A5:H6')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A2:H2')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('A3:H3')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('A2:H2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center'));
                $active_sheet->getStyle('A3:H3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center'));

            },
        ];
    }
    public function startCell(): string
    {
        return 'A6';
    }
}
