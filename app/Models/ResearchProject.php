<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ResearchProject extends BaseModel
{
    const WAIT_APPROVAL = 0 ;
    const IN_PROCESSING = 1 ;
    const COMPLETED = 2 ;
    const CANCEL = 3 ;
    const DRAFT = 4 ;
    const UNFINISHED = 5;
    const ACCEPTANCE = 6;

    const BEYOND_PROCESS = 1;
    const ON_TIME = 2 ;
    const EXPIRED = 3 ;

    use HasFactory;
    protected $table = "research_project";
    protected $fillable = [
        'project_code',
        'name',
        'contract_code',
        'research_category_id',
        'status',
        'start_date',
        'end_date',
        'description',
        'project_scale',
        'leader_id',
        'admin_id',
        'note',
        'department_id'
    ];

    public function leader(){
        return $this->hasOne(UserInf::class,'id','leader_id');
    }

    public function researchCategory() {
        return $this->belongsTo(ResearchCategory::class, 'research_category_id');
    }

    public function topic(){
        return $this->belongsTo(Topic::class, 'contract_code','contract_code');
    }

    public function admin(){
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function department() {
        return $this->belongsTo(Department::class, 'department_id');
    }

    private static $searchable = [
        'name','project_code'
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
}
