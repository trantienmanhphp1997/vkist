<div class="modal-content">
    <div class="modal-body box-user">
        <h4 class="modal-title" id="exampleModalLabel">
            @if (!$editable)
                {{ __('data_field_name.department.detail_department') }}
            @elseif ($department->exists)
                {{ __('data_field_name.department.edit_department') }}
            @else
                {{ __('data_field_name.department.create_department') }}
            @endif
        </h4>
        <div class="form-group">
            <label>{{ __('data_field_name.department.name') }}<span class="text-danger">(*)</label>
            <input type="text" class="form-control" placeholder="{{ __('data_field_name.department.input_name') }}" wire:model.defer="department.name" {{ $editable ? '' : 'disabled' }}>
            @error('department.name') <span class="text-danger error">{{ $message }}</span>@enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.department.code') }}<span class="text-danger">(*)</label>
            <input type="text" class="form-control" wire:model.defer="department.code" {{ $editable ? '' : 'disabled' }}>
            @error('department.code') <span class="text-danger error">{{ $message }}</span>@enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.department.type') }}<span class="text-danger">(*)</label>
            {{ Form::select('type', ['' => __('common.select-box.choose')] + $typeList, null, ['class' => 'form-control', 'wire:model.lazy' => 'department.type', 'disabled' => !$editable]) }}
            @error('department.type') <span class="text-danger error">{{ $message }}</span>@enderror
        </div>
        @if (!empty($department->type) && $department->type != \App\Enums\EDepartment::TYPE_CLASS)
            <div class="form-group">
                <label for="exampleFormControlInput4">{{ __('data_field_name.department.under_department') }}</label>
                @livewire('component.advanced-select', [
                    'name' => 'parent-id',
                    'modelName' => \App\Models\Department::class,
                    'columns' => ['name', 'code'],
                    'searchBy' => ['name', 'code'],
                    'conditions' => [
                        ['type', '=', $parentType]
                    ],
                    'placeholder' => __('common.select-box.choose'),
                    'disabled' => !$editable,
                    'selectedIds' => [$department->parent_id]
                ], key($department->type))
                @error('department.parent_id') <span class="text-danger error">{{ $message }}</span>@enderror
            </div>
        @endif
        <div class="form-group">
            <label>{{ __('data_field_name.department.note') }}<span class="text-danger">(*)</label><br>
            <textarea name="note" class="form-control" cols="30" rows="5" wire:model.defer="department.note" placeholder="{{ __('data_field_name.department.input_function_description') }}" {{ $editable ? '' : 'disabled' }}></textarea>
            @error('department.note') <span class="text-danger error">{{ $message }}</span>@enderror
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput3">{{ __('data_field_name.department.status') }}</label>
            <select name="status" id="exampleFormControlInput3" class="form-control" wire:model.defer="department.status" {{ $editable ? '' : 'disabled' }}>
                <option value="0">{{ __('data_field_name.system.role.active') }}</option>
                <option value="1">{{ __('data_field_name.system.role.inactive') }}</option>
            </select>
            @error('department.status') <span class="text-danger error">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="group-btn2 text-center pt-12">
        <button type="button" wire:click="resetEditor" id="close-department-editor-btn" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.close') }}</button>
        @if ($editable)
            <button type="submit" wire:click.prevent="save" class="btn btn-save">{{ __('common.button.save') }}</button>
        @endif
    </div>
</div>
