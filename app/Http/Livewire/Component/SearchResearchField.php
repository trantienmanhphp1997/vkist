<?php

namespace App\Http\Livewire\Component;

use App\Enums\EResearchCategoryType;
use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchCategory;
use Livewire\Component;

class SearchResearchField extends Component
{
    public $query='';
    public $data;
    public $successful=false;
    public $name;
    public $key;
    public $page = 1;
    public $perPage = 10;
    public $displayOption = false;
    public $researchFieldId ;
    public $disable;

    protected $listeners = [
        'loadMore',
        'displayOption',
        'hideOption',
    ];

    public function loadMore() {
        $this->page = $this->page+1;
        $this->getData();
    }
    public function displayOption() {
        $this->displayOption = true;
    }

    public function hideOption() {
        $this->displayOption = false;
    }

    public function resetInput()
    {
        $this->query = '';
        $this->data = [];
    }

    public function updatedQuery()
    {
        $this->page = 1;
        $this->getData();
        $this->dispatchBrowserEvent('reset-height-scroll');
    }
    public function getData() {
        $this->data = ResearchCategory::where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->query)) . '%')->where('type', '=',EResearchCategoryType::FIELD)
        ->orderBy('unsign_text', 'asc')
        ->limit($this->page*$this->perPage)
        ->get()
        ->toArray();
    }
    public function mount() {
        if($this->researchFieldId) {
            $this->query = ResearchCategory::findOrFail($this->researchFieldId)->name;
        }
        $this->getData();
    }
    public function render()
    {
        if(!$this->query) {
            $this->successful=false;
        }


        return view('livewire.component.search-research-field');
    }
    public function setInput($id,$name){
        $this->query=$name;
        $this->displayOption=false;
        $this->emit('setResearchField', $id);
    }
}
