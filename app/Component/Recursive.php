<?php

namespace App\Component;

use Illuminate\Support\Collection;

class Recursive
{
    public Collection $rawData;
    public array $data;

    public function __construct(Collection $rawData)
    {
        $this->rawData = $rawData;
        $this->handleRawData();
    }

    public function handleRawData()
    {
        $this->rawData->each(function (&$item) {
            $item->children = collect([]);
        });

        $result = $this->rawData->each(function (&$item) {
            $parentId = $item->parent_id;
            if (empty($parentId)) {
                return;
            }
            $parent = $this->rawData->firstWhere('id', $parentId);
            if ($parent != null) {
                $parent->children->push($item);
            } else {
                $item->parent_id = null;
            }
        })->filter(function ($item) {
            return empty($item->parent_id);
        });

        $this->data = json_decode($result->toJson(), true);
        return $this;
    }

    public static function hierarch(Collection $collection)
    {
        $recursive = new static($collection);
        return $recursive->getHierarchy();
    }

    public function getHierarchy(array $array = null, $level = 0)
    {
        if ($array === null) {
            $array = $this->data;
        }
        $output = [];
        foreach ($array as $item) {
            array_push($output, $this->itemPattern($item, $level));
            if (isset($item['children']) && is_array($item['children'])) {
                $childrenOutput = $this->getHierarchy($item['children'], $level + 1);
                array_push($output, ...$childrenOutput);
            }
        }
        return $output;
    }
    
    // override this method to display item
    public function itemPattern($item, $level) {
        return [
            'item'=>$item,
            'level'=>$level
        ];
    }
}
