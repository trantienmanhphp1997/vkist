<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkPlanHasScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_plan_has_schedule', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('lich làm việc');
            $table->foreignId('work_plan_id')->nullable()->constrained('work_plan')->comment('map voi work_plan');
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('nhân viên có trong work-plan');
            $table->date('working_day')->nullable();
            $table->tinyInteger('type')->nullable()->comment('1 sang; 2 chieu, 3 toi');
            $table->string('content', 1000)->nullable()->comment('noi dung cong tac');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_plan_has_schedule');
    }
}
