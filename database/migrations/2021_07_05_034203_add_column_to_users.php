<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('status')->nullable()->comment('trang thai = -1 khoi tao');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->string('username', 48)->unique()->comment('Ten dang nhap');
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('Map voi user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('admin_id');
            $table->dropColumn('status');
            $table->dropColumn('user_info_id');
            $table->dropColumn('username');

        });
    }
}
