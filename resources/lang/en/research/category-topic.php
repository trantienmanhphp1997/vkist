<?php
return [
    'breadcrumbs' => [
        'category-topic-management-1' => 'Category management',
        'category-topic-management-2' => 'Manage topics',
        'category-topic-list' => 'List of topic categories',
    ],
    'table_column' => [
        'code' => 'CODE',
        'name' => 'THE TYPE NAME',
        'description' => 'DESCRIPTION',
        'note' => 'NOTE',
        'action' => 'Action',
    ],
    'title' => [
        'create' => 'Create new',
        'edit' => 'Edit',
        'no-result' => 'No results!',
        'part-1' => 'Display',
        'part-2' => 'type of topic in',
    ],
    'empty-record' => 'No records',
    'placeholder' => [
        'search' => 'Search',
        'name' => "Enter the name of the topic type",
        'code' => "Enter the code of the topic type",
        'description' => "Enter a description of the topic",
        'note' => "Enter notes of topic type",
    ],
    'modal' => [
        'title' => [
            'delete' => 'Delete confirmation',
            'create' => 'Add topic type',
            'edit' => 'Edit topic type',
            'detail' => 'Detail of topic type',
        ],
        'body' => [
            'delete-warning' => 'Did you delete? This operation cannot be undone!',
            'code' => 'Code of topic type',
            'name' => 'Name of topic type',
            'description' => 'Description',
            'note' => 'Note',
        ],
        'footer' => [
            'back' => 'Back',
            'delete' => 'Delete',
            'close' => 'Close',
            'save' => 'Save',
        ]
    ],
    'validate' => [
        'code' => 'Code of topic type',
        'name' => 'Name of topic type',
        'note' => 'Note type of topic',
        'description' => 'Description of the topic',
    ],
    'notification' => [
        'edit-success' => 'Edit successful',
        'create-success' => 'New successfully added'
    ]
];