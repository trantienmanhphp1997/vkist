<?php

namespace App\Http\Livewire\Admin\Executive\ContractAndSalary\LaborContract;

use App\Models\Contract;
use App\Models\User;
use App\Models\UserInf;
use App\Models\MasterData;
use App\Models\ContractDuration;
use App\Enums\EContractType;
use App\Http\Livewire\Base\BaseLive;
use Livewire\WithFileUploads;
use App\Component\SizeFile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use App\Component\FileUpload;
use App\Models\File;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exports\ContractExport;
use Excel;
class ListData extends BaseLive {

    use WithFileUploads;

    public $filterStatus;
    public $id_edit;
    public $filter_duration_name;
    public $isEdit = false;
    public $user_data_list = [];
    public $disabled_end_date = false;
    public $disabled_social_insurance_salary = false;
    public $errorFile;
    public $type;
    public $model_name;
    public $folder;

    //filter date
    public $from_date;
    public $to_date;

    //Dữ liệu thời hạn
    public $show_add_duration = false;
    public $duration_name;
    public $duration_status;
    public $duration;

    //Dữ liệu hợp đồng thêm, sửa
    public $bank_info;
    public $coefficients_info;
    public $user_info_id;
    public $user_name;
    public $user_code;
    public $user_position;
    public $contract_type;
    public $contract_code;
    public $duration_id;
    public $status = 1;
    public $working_type;
    public $start_date;
    public $end_date;
    public $basic_salary;
    public $social_insurance_salary;
    public $coefficients_salary;
    public $bank_account_number;
    public $bank_name;
    public $note;
    public $itemFile = [];
    public $fileLists = [];
    public $model_id;
    public $increase_code;
    protected $listeners = [
        'set-startDate' => 'setStartDate',
        'set-endDate' => 'setEndDate',
        'set-from_date' => 'setFromDate',
        'set-to_date' => 'setToDate',
    ];
    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render() {
        $this->errorFile = null;
        $this->contract_code = trim($this->contract_code);
        $this->note = trim($this->note);
        $this->type = Config::get('common.type_upload.LaborContract');
        $this->model_name = Contract::class;
        $this->folder = app($this->model_name)->getTable();
        $this->bank_info = MasterData::where('type', 17)->get();
        $this->coefficients_info = MasterData::where('type', 18);
        if ($this->contract_type){
//            dd($this->contract_type);

            $lastIncreaseCode = DB::table('contract')
                    ->whereNotNull('increase_code')
                    ->where('contract_type',$this->contract_type)
                    ->orderBy('created_at', 'desc')
                    ->first('increase_code')->increase_code ?? 0;

            $this->increase_code = ++$lastIncreaseCode;
            $number='';
            if ($lastIncreaseCode <10){
                $number = '000';
            }elseif ($lastIncreaseCode <1000){
                $number = '00';
            }elseif ($lastIncreaseCode <10000){
                $number = '0';
            }
            if ($this->contract_type==1){
                $this->contract_code = 'HDLV' .$number. $lastIncreaseCode;
            }else{
                $this->contract_code = 'HDLD' .$number. $lastIncreaseCode;
            }

        }
        if (!empty($this->coefficients_salary)) {
            $this->coefficients_info->where(function($q) {
                $q->whereRaw('lower(v_value) like ? ', ['%' . trim(mb_strtolower($this->coefficients_salary, 'UTF-8')) . '%']);
            });
        }
        $this->coefficients_info = $this->coefficients_info->limit(10)
            ->get();

        if (empty($this->start_date)) {
            $this->start_date = Carbon::now()->format(Config::get('common.year-month-day'));
        }
        if (empty($this->end_date)) {
            $this->end_date = Carbon::now()->format(Config::get('common.year-month-day'));
        }
        if (!empty($this->user_code)) {
            $currentStaff = UserInf::where('code', $this->user_code)->first();
            if (!empty($currentStaff)) {
                $this->user_info_id = $currentStaff->id;
                $this->user_name = $currentStaff->fullname;
                $this->user_position = !empty($currentStaff->position) ? $currentStaff->position->v_value : null;
            }
        }

        $this->getUserDatalist();
        $this->searchContractDuration();
        return view('livewire.admin.executive.contract-and-salary.labor-contract.list-data',['data' => $this->searchContract()]);

    }

    public function getListFile() {
        $this->fileLists = File::where('model_id', $this->id_edit)
                ->where('model_name', $this->model_name)
                ->where('type', $this->type)
                ->where('model_id', $this->model_id)
                ->get();
    }

    public function searchContract() {
        $query = Contract::query()
            ->leftJoin('user_info', 'user_info.id', 'contract.user_info_id')
            ->leftJoin('master_data', 'master_data.id', 'user_info.position_id')
            ->leftJoin('contract_duration', 'contract_duration.id', 'contract.duration_id')
            ->whereIn('contract_type', [EContractType::WORK, EContractType::LABOR])
            ->select('contract.*', 'user_info.fullname as user_name', 'master_data.v_value as position_value', 'contract_duration.name as duration');
        $searchTerm = $this->searchTerm;
        $status = $this->filterStatus;

        if ($status != null) {
            if (!in_array($status, [1, -1])) {
                $query->onlyTrashed();
            } else {
                $query->where('contract.status', $status);
            }
        }

        if (!empty($searchTerm)) {
            $query->where(function($q) use ($searchTerm) {
                $q->whereRaw('lower(user_info.fullname) like ? ', ['%' . trim(mb_strtolower($searchTerm, 'UTF-8')) . '%'])
                    ->orWhereRaw('lower(contract.contract_code) like ? ', ['%' . trim(mb_strtolower($searchTerm, 'UTF-8')) . '%']);
                });
        }
        if(!is_null($this->from_date)) {
            $query->where('start_date', '>=', $this->from_date);
        }
        if(!is_null($this->to_date)) {
            $query->where('end_date', '<=', $this->to_date);
        }

        return $query->orderBy('contract.id','DESC')->paginate(25);
    }

    public function searchContractDuration() {
        $this->duration = ContractDuration::query();
        if (!empty($this->filter_duration_name)) {
            $this->duration->where(function($q) {
                $q->whereRaw('lower(name) like ? ', ['%' . trim(mb_strtolower($this->filter_duration_name, 'UTF-8')) . '%']);
                });
        }
        $this->duration = $this->duration->orderBy('id', 'desc')->get();
    }

    public function initFormData($id) {
        if (!empty($id)) {
            $this->id_edit = $id;
            $this->model_id = $id;
            $currentContract = Contract::findOrFail($this->id_edit);
            $user = UserInf::findOrFail($currentContract->user_info_id);
            $this->user_name = $user->fullname;
            $this->user_code = $user->code;
            $this->user_position = !empty($user->position) ? $user->position->v_value : null;
            $this->contract_type = $currentContract->contract_type;
            $this->contract_code = $currentContract->contract_code;
            $this->duration_id = $currentContract->duration_id;
            $this->status = $currentContract->status;
            $this->working_type = $currentContract->working_type;
            $this->start_date = Carbon::parse($currentContract->start_date)->format(Config::get('common.year-month-day'));
            $this->end_date = !empty($currentContract->end_date) ? Carbon::parse($currentContract->end_date)->format(Config::get('common.year-month-day')) : null;
            $this->basic_salary = $currentContract->basic_salary;
            $this->social_insurance_salary = $currentContract->social_insurance_salary;
            $this->coefficients_salary = $currentContract->coefficients->v_value;
            $this->bank_account_number = $currentContract->bank_account_number;
            $this->bank_name = $currentContract->bank_name_id;
            $this->note = $currentContract->note;
            $this->getListFile();
        } else {
            $this->model_id = null;
            $this->id_edit = null;
            $this->user_name = null;
            $this->user_code = null;
            $this->user_position = null;
            $this->contract_type = null;
            $this->contract_code = null;
            $this->duration_id = null;
            $this->status = 1;
            $this->working_type = null;
            $this->start_date = null;
            $this->end_date = null;
            $this->basic_salary = null;
            $this->social_insurance_salary = null;
            $this->coefficients_salary = null;
            $this->bank_account_number = null;
            $this->bank_name = null;
            $this->note = null;
            $this->file = [];
            $this->fileLists = [];
            $this->emit('updateFile');
        }
    }

    public function delete() {
        $contract = Contract::findOrFail($this->deleteId);
        if (!empty($contract)) {
            $contract->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
        }
    }

    public function storeFile($model_id) {
        $file_upload = File::where('model_name', $this->model_name)
            ->where('type', $this->type)
            ->where('status', -1)
            ->first();
        if (!empty($file_upload)) {
            $file_upload->status = 1;
            $file_upload->model_id = $model_id;
            $file_upload->save();
        }
    }

    public function validateDurationData() {
        $this->validate([
            'duration_name' => ['required'],
        ],[],[
            'duration_name'=> __('data_field_name.labor-contract.duration_name'),
        ]);
    }

    public function validateContractData() {
        $this->validate([
            'user_code' => ['required', function($attribute, $value, $fail) {
                $checkExist = UserInf::where('code', $value)->exists();
                if (!$checkExist) {
                    return $fail(__('validation.exists', [
                        'attribute' => __('data_field_name.labor-contract.user_code'),
                    ]));
                }
            }],
            'contract_type' => ['required'],
            'contract_code' => ['required', 'max:15', 'regex:/[^a-z]+$/u', function($attribute, $value, $fail) {
                $checkExist = Contract::where('contract_code', $value)->exists();
                if ($checkExist && empty($this->id_edit)) {
                    return $fail(__('validation.unique', [
                        'attribute' => __('data_field_name.labor-contract.contract_code'),
                    ]));
                }
            }],
            'user_position' => ['required'],
            'duration_id' => ['required'],
            'status' => ['required'],
            'working_type' => ['required'],
            'start_date' => $this->disabled_end_date ? ['required', 'date'] : ['required', 'date', 'before_or_equal:end_date'],
            'end_date' => $this->disabled_end_date ? ['nullable'] : ['required', 'date'],
            'basic_salary' => ['required', 'max:9'],
            'social_insurance_salary' => $this->disabled_social_insurance_salary ? ['nullable'] : ['required', 'max:9'],
            'coefficients_salary' => ['required', function($attribute, $value, $fail) {
                $checkExist = MasterData::where('v_value', $value)->exists();
                if (!$checkExist) {
                    return $fail(__('validation.exists', [
                        'attribute' => __('data_field_name.labor-contract.coefficients_salary'),
                    ]));
                }
            }],
            'bank_account_number' => ['required', 'max:14', 'regex:/[0-9]+$/'],
            'bank_name' => ['required'],
            'note' => ['max:225'],
        ],[],[
            'user_position' => __('data_field_name.labor-contract.user_position'),
            'user_code' => __('data_field_name.labor-contract.user_code'),
            'contract_type' => __('data_field_name.labor-contract.contract_type'),
            'contract_code' => __('data_field_name.labor-contract.contract_code'),
            'duration_id' => __('data_field_name.labor-contract.duration_id'),
            'status' => __('data_field_name.labor-contract.status'),
            'working_type' => __('data_field_name.labor-contract.working_type'),
            'start_date' => __('data_field_name.labor-contract.start_date'),
            'end_date' => __('data_field_name.labor-contract.end_date'),
            'basic_salary' => __('data_field_name.labor-contract.basic_salary'),
            'social_insurance_salary' => __('data_field_name.labor-contract.social_insurance_salary'),
            'coefficients_salary' => __('data_field_name.labor-contract.coefficients_salary'),
            'bank_account_number' => __('data_field_name.labor-contract.bank_account_number'),
            'bank_name' => __('data_field_name.labor-contract.bank_name'),
            'note' => __('data_field_name.labor-contract.note'),
        ]);
    }

    public function saveData() {
        $this->validateContractData();
        if (empty($this->errorFile)) {
            return DB::transaction(function() {
                if (!empty($this->id_edit)) {
                    $laborContract = Contract::findOrFail($this->id_edit);
                } else {
                    $laborContract = new Contract;
                }
                $laborContract->user_info_id = $this->user_info_id;
                $laborContract->contract_type = $this->contract_type;
                $laborContract->contract_code = $this->contract_code;
                $laborContract->duration_id = $this->duration_id;
                $laborContract->status = $this->status;
                $laborContract->working_type = $this->working_type;
                $laborContract->start_date = $this->start_date;
                $laborContract->end_date = $this->end_date;
                $laborContract->basic_salary = $this->basic_salary;
                $laborContract->social_insurance_salary = empty($this->social_insurance_salary) ? 0 : $this->social_insurance_salary;
                $masterData = MasterData::where('v_value', $this->coefficients_salary)->first();
                $laborContract->coefficients_salary_id = !empty($masterData) ? $masterData->id : null;
                $laborContract->bank_account_number = $this->bank_account_number;
                $laborContract->bank_name_id = $this->bank_name;
                $laborContract->note = $this->note;
                $laborContract->increase_code = $this->increase_code;
                $laborContract->save();

                $this->storeFile($laborContract->id);
                if (!empty($this->id_edit)) {
                    $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')]);
                } else {
                    $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);
                }
                $this->emit('closeModalCreate');
            });
        }
    }

    public function openModalCreate($id = null) {
        $this->initFormData($id);
        $this->emit('showModalCreate', Carbon::parse($this->start_date)->format(Config::get('common.formatDate')),  Carbon::parse($this->end_date)->format(Config::get('common.formatDate')));
        $this->resetValidation();
    }

    public function editContract($id) {
        $this->id_edit = $id;
        $this->isEdit = true;
    }

    public function openModalSetting() {
        $this->show_add_duration = false;
        $this->emit('showModalSetting');
    }

    public function saveSetting() {
        $this->validateDurationData();
        $contractDuration = new ContractDuration();
        $contractDuration->name = $this->duration_name;
        $contractDuration->status = $this->duration_status ? 1 : 0;
        $contractDuration->save();
        if ($contractDuration->status == 1) {
            $this->duration_id = $contractDuration->id;
        } else {
            $this->duration_id = null;
        }
        $this->duration_name = null;
        $this->duration_status = false;
        $this->show_add_duration = false;
        $this->emit('closeModelSetting');
    }

    public function toggleDisplaySetting($id) {
        $contractDuration = ContractDuration::findOrFail($id);
        $contractDuration->status = $contractDuration->status == 1 ? 0 : 1;
        if ($contractDuration->status == 1) {
            $this->duration_id = $id;
        } else {
            $this->duration_id = null;
        }
        $contractDuration->save();
        $this->emit('closeModelSetting');
    }

    public function deleteSetting($id) {
        $contractDuration = ContractDuration::findOrFail($id);
        if (!empty($contractDuration)) {
            $contractDuration->delete();
            if ($this->duration_id == $id) {
                $this->duration_id = null;
            }
        }
    }

    public function getUserDatalist() {
        $data = UserInf::query();
        $data->where(function($q) {
            $q->whereRaw('lower(code) like ? ', ['%' . trim(mb_strtolower($this->user_code, 'UTF-8')) . '%']);
            });
        $this->user_data_list = $data->limit(10)->get();
    }

    public function deleteFile($index) {
        $file = File::findOrFail($index);
        if(!is_null($file)) {
            $file->delete();
        }
        $this->getListFile();
    }

    public function setStartDate($data) {
        $this->start_date= date('Y-m-d', strtotime($data['startDate']));
    }
    public function setEndDate($data) {
        $this->end_date= date('Y-m-d', strtotime($data['endDate']));
    }
    public function setFromDate($data) {
        $this->from_date = $data['from_date'] ? date('Y-m-d', strtotime($data['from_date'])) :null;
    }
    public function setToDate($data) {
        $this->to_date = $data['to_date'] ? date('Y-m-d', strtotime($data['to_date'])) : null;
    }

    public function export() {
        return Excel::download(new ContractExport($this->searchTerm, $this->filterStatus, $this->from_date, $this->to_date), 'contract-list-' . now()->format(Config::get('common.year-month-day')) . '-' . '.xlsx');
    }
}
