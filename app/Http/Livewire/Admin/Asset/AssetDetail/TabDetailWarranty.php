<?php

namespace App\Http\Livewire\Admin\Asset\AssetDetail;

use App\Enums\EAssetTypeDate;
use App\Models\AssetAllocatedRevoke;
use Livewire\Component;

class TabDetailWarranty extends Component
{
    public $assetDetail;
    public $assetTypeDate;

    public function mount() {
        $this->assetTypeDate = [
            EAssetTypeDate::DAY => __('data_field_name.asset.day'),
            EAssetTypeDate::MONTH => __('data_field_name.asset.month'),
            EAssetTypeDate::YEAR => __('data_field_name.asset.year'),
        ];
    }
    public function render()
    {
        $asset_Detail =$this->assetDetail;

        if($asset_Detail){
            $warranty_date=[];
            $maintenance_date=[];

            if ($asset_Detail->warranty_date_json){
                $warranty_date =json_decode($asset_Detail->warranty_date_json, 2);
            }
            if ($asset_Detail->maintenance_date_json){
                $maintenance_date =json_decode($asset_Detail->maintenance_date_json, 2);
            }

        }

        return view('livewire.admin.asset.asset-detail.tab-detail-warranty',
            [
                'warranty_date'=>$warranty_date,
                'maintenance_date'=>$maintenance_date
            ]);
    }
}
