<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrendingToNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_news', function (Blueprint $table) {
            $table->string('slug',1500)->nullable()->unique();
        });
        Schema::table('news', function (Blueprint $table) {
            $table->tinyInteger('is_trending')->default(0)->comment('1:turn on to trending new|0: turn off trending');
            $table->bigInteger('count_read')->default(0)->comment('amount of view');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_news', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('is_trending');
            $table->dropColumn('count_read');
        });
    }
}
