<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 500)->comment('ten nhiem vu');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->foreignId('topic_id')->nullable()->constrained('topic')->comment('de tai');
            $table->foreignId('parent_id')->nullable()->constrained('task')->comment('Nhiem vu cha');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('Nguoi thuc hien');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task');
    }
}
