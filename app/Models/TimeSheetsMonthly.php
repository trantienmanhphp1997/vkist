<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class TimeSheetsMonthly extends Eloquent
{
    use HasFactory;
    protected $table = 'timesheets_monthly';
    protected $fillable = [];
}
