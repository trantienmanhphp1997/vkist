@extends('layouts.master')
<title>{{__('menu_management.menu_name.reward_and_discipline')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.decision-reward.decision-reward-list')
@endsection