<?php

namespace App\Http\Livewire\Admin\Asset\Category;

use Livewire\Component;
use App\Models\AssetCategory;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use App\Component\Recursive;
use Illuminate\Support\Facades\DB;
use App\Exports\AssetCategoryExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Component\AssetCategoryRecursive;

class AssetCategoryList extends BaseLive
{

    public $searchTerm;

    public $name, $note, $quantity, $code, $type_manage, $using, $ac_id;

    public function render(){
        $query = AssetCategory::query();
        if (!empty($this->searchTerm)) {
            $query->where('asset_category.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')
            ->orWhereRaw("LOWER(code) LIKE LOWER('%{$this->searchTerm}%')");
        }
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 25;
        $data = $query->orderBy('id', 'asc')->paginate($perPage);
        return view('livewire.admin.asset.category.asset-category-list', [
            'data' => $data,
        ]);
    }

    public function store(){
        $asset_category = new AssetCategory;
        $this->validate([
            'code' => 'required|max:13|unique:asset_category|regex:/^[^a-z]+$/',
            'name' => 'required|max:100|regex:/^([a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]+)$/i',
            'note' => 'max:255|nullable',],[],[

            'code' => __('data_field_name.asset_category.code'),
            'name' => __('data_field_name.asset_category.name'),
            'note' => __('data_field_name.asset_category.note'),
        ]);
        $asset_category->code = $this->code;
        $asset_category->name = $this->name;
        $asset_category->note = $this->note;
        $asset_category->type_manage= $this->type_manage;
        $asset_category->save();
        $this->resetInputFields();
        $this->emit('close-modal-create-asset-category');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function resetInputFields(){
        $this->name='';
        $this->note='';
        $this->code='';
        $this->type_manage='';
    }

    public function edit($id){
        $this->updateMode = true;
        $asset_category = AssetCategory::findOrFail($id);
        $this->ac_id = $asset_category->id;
        $this->name = $asset_category->name;
        $this->note = $asset_category->note;
        $this->code = $asset_category->code;
        $this->type_manage = $asset_category->type_manage;
        $this->resetValidation();

    }
    public function update(){

        $this->validate([
            'code' => 'required|max:15|regex:/^[^a-z]+$/|unique:asset_category,code,'.$this->ac_id,
            'name' => 'required|max:100|regex:/^([a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]+)$/i',
            'note' => 'max:255|nullable',],[],[

            'code' => __('data_field_name.asset_category.code'),
            'name' => __('data_field_name.asset_category.name'),
            'note' => __('data_field_name.asset_category.note'),

        ]);

        $asset_category = AssetCategory::findOrFail($this->ac_id);
        $asset_category->name = $this->name;
        $asset_category->note = $this->note;
        $asset_category->code = $this->code;
        $asset_category->type_manage = $this->type_manage;
        $asset_category->save();

        $this->resetInputFields();
        $this->emit('close-modal-edit-asset-category');
        $this->updateMode = false;

        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }

    public function getIdDelete($id){
        $this->deleteId = $id;
    }

    public function delete(){
        $asset_category = AssetCategory::findOrFail($this->deleteId);
        $count = $asset_category->asset()->count();
        if($count > 0) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('data_field_name.asset_category.error_delete')] );
        }else{
            $asset_category->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
        }
    }

    public function export() {
        $today = date("d_m_Y");
        return Excel::download(new AssetCategoryExport($this->searchTerm), 'list-asset-category-' . $today . '.xlsx');
    }
}
