<?php

namespace App\Http\Controllers\Admin\Profile;

use App\Enums\ERouteName;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Models\User;
use App\Models\UserInf;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class ProfileController extends Controller
{

    public function index()
    {
        $user = User::with('info', 'info.department', 'info.position')->findOrFail(auth()->id());
        return view(ERouteName::ROUTE_PROFILE_INDEX, compact('user'));
    }

    public function update(ProfileRequest $request)
    {

        $user = User::findOrFail(auth()->id());
        if(!$user->user_info_id || !UserInf::find($user->user_info_id)) {
            return redirect(route(ERouteName::ROUTE_PROFILE_INDEX))->with('error',__("notification.common.fail.user_info_not_exist"));
        }
        $userInfo = UserInf::findOrFail($user->user_info_id);
        $userInfo->avatar ?? '';
        $userInfo->fullname = $request->fullname;
        $userInfo->phone = $request->phone;
        $userInfo->telephone = $request->telephone;
        $userInfo->ext = $request->ext;
        $user->gw_user_id = $request->username_groupware;
        if($request->password_groupware) {
            $user->gw_pass = $request->password_groupware;
        }
        if($request->hasFile('avatar')) {
            $userInfo->avatar = $this->uploadAvatar($request->file('avatar'));
        }

        $userInfo->save();
        $user->save();
        return redirect(route(ERouteName::ROUTE_PROFILE_INDEX))->with('success',__("notification.common.success.update"));
    }

    protected function uploadAvatar(UploadedFile $file) {
        $fileHashName = $file->hashName();
        Storage::put('/uploads/avatars', $file);
        return asset('storage/uploads/avatars/' . $fileHashName);
    }
}
