<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('menu_management.menu_name.infomation-management')}}</a> \
                <span>{{__('executive/my-day-off.title')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('executive/my-day-off.management')}}</h3>
                </div>
                <div class="col-md-6">
                    <div class=" float-right">
                        <form action="{{ route('admin.executive.my-day-off.apply-for-leave') }}"
                              class="ml-auto d-flex flex-row">
                            <button type="submit" class="btn bg-primary btn-fix mr-0 px-24 py-12 ml-12"><img
                                    src="../images/PlusWhite.svg" alt="">{{__('executive/my-day-off.register')}}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <!-- Start leave information -->
                    <div class="row mb-32 row-m8">
                        <div class="col-md-4 col-p8">
                            <div class="bg-3 px-24 py-24 bd-rds-12 w-100">
                                <p class="body-4 text-2 text-uppercase mb-12">{{__('executive/my-day-off.vacation_days_last_year')}}</p>
                                <div class="d-flex bd-t body-5 flex-column">
                                    <div class="d-flex w-100 mt-12">
                                        <p class="m-0">{{__('executive/my-day-off.used')}}</p>
                                        <p class="mb-0 ml-auto fw-600">{{$daysUsedLastYear}}</p>
                                    </div>
                                    <div class="d-flex w-100 mt-12">
                                        <p class="m-0">{{__('executive/my-day-off.expiry_date')}}</p>
                                        <p class="mb-0 ml-auto fw-600">{{$daysExpiryLastYear}}</p>
                                    </div>
                                    <div class="d-flex w-100 mt-12">
                                        <p class="m-0">{{__('executive/my-day-off.expiration_date')}}</p>
                                        <p class="mb-0 ml-auto fw-600">{{$dateMoveLastYear}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-p8">
                            <div class="bg-3 px-24 py-24 bd-rds-12 w-100">
                                <p class="body-4 text-2 text-uppercase mb-12">{{__('executive/my-day-off.this_year_leave')}}</p>
                                <div class="d-flex bd-t body-5 flex-column">
                                    <div class="d-flex w-100 mt-12">
                                        <p class="m-0">{{__('executive/my-day-off.used')}}</p>
                                        <p class="mb-0 ml-auto fw-600">{{$daysUsedCurrentYear}}</p>
                                    </div>
                                    <div class="d-flex w-100 mt-12">
                                        <p class="m-0">{{__('executive/my-day-off.expiry_date')}}</p>
                                        <p class="mb-0 ml-auto fw-600">{{$daysExpiryCurrentYear}}</p>
                                    </div>
                                    <div class="d-flex w-100 mt-12">
                                        <p class="m-0">{{__('executive/my-day-off.expiration_date')}}</p>
                                        <p class="mb-0 ml-auto fw-600">{{$dateMoveCurrentYear}}</p>
                                    </div>
                                    <div class="d-flex w-100 mt-12">
                                        <p class="m-0">{{__('executive/my-day-off.seniority')}}</p>
                                        <p class="mb-0 ml-auto fw-600">{{$dataFromYearSeniority}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-p8">
                            <div class="bg-3 px-24 py-24 bd-rds-12 w-100">
                                <p class="body-4 text-2 text-uppercase mb-12">{{__('executive/my-day-off.total_days_of_license_expired')}}</p>
                                <div class="d-flex bd-t body-5 flex-column">
                                    <p class="head-6 mb-0 mt-12 text-center text-2 py-24">{{$daysExpiryCurrentYear+$daysExpiryLastYear}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End leave information -->

                    <!-- Start leave day table -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex align-items-center">
                                <p class="m-0 px-0 pt-0 pb-12 head-9 text-2">{{__('executive/my-day-off.check_license_days')}}</p>
                                <div class="ml-auto d-flex flex-row">
                                    <div class="form-group field-fix d-flex flex-row align-items-center mr-12 mb-0">
                                        <label for="" class="mb-4 mr-12">{{__('executive/my-day-off.from')}}</label>
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'fromdDate', 'place_holder'=>'', 'default_date' => is_null($fromdDate2) ? date('Y-m-d') : $fromdDate2])
                                    </div>
                                    <div class="form-group field-fix d-flex flex-row align-items-center mr-12 mb-0">
                                        <label for="" class="mb-4 mr-12">{{__('executive/my-day-off.to')}}</label>
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'toDate', 'place_holder'=>'', 'default_date' => is_null($toDate) ? date('Y-m-d') : $toDate])
                                    </div>
                                    <button type="button" id='btn_submit_get_date'
                                            class="btn btn-fix bg-primary h-44 px-24 py-12">{{__('executive/my-day-off.check')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-general text-2">
                        <thead>
                        <tr class="border-radius">
                            <th scope="col" class="text-center border-radius-left">STT</th>
                            <th scope="col" class="text-center">{{__('executive/my-day-off.start_date2')}}</th>
                            <th scope="col" class="text-center">{{__('executive/my-day-off.type_of_day_leave')}}</th>
                            <th scope="col" class="text-center">{{__('executive/my-day-off.number_of_days')}}</th>
                            <th scope="col" class="text-center"
                                style='max-height:200px;'>{{__('executive/my-day-off.reason_for_leave')}}</th>
                            <th scope="col"
                                class="text-center border-radius-right text-center">{{__('executive/my-day-off.status')}}</th>
                        </tr>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>
                        {{--handleRequestLeave(43)--}}
                        @foreach($data as $key => $row)
                            <tr>
                                <td class='text-center'>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                <td class="text-center"><a
                                        href="{{route('admin.executive.my-day-off.apply-for-leave-edit', ['id'=>$row->id]) }}">{{reFormatDate($row->start_date, 'H:i d-m-Y')}}
                                        - {{reFormatDate($row->end_date, 'H:i d-m-Y')}} </a></td>
                                <td class="text-center">

                                    @if($row->type==1)  {{__('executive/my-day-off.leave_for_the_year')}}
                                    @elseif($row->type==2)  {{__('executive/my-day-off.paid_time_off')}}
                                    @elseif($row->type==3)  {{__('executive/my-day-off.holiday')}}
                                    @elseif($row->type==4)  {{__('executive/my-day-off.meeting')}}
                                    @elseif($row->type==5)  {{__('executive/my-day-off.paid_personal_leave')}}
                                    @elseif($row->type==6)  {{__('executive/my-day-off.unpaid_personal_leave')}}
                                    @elseif($row->type==7)  {{__('executive/my-day-off.sick_leave')}}
                                    @elseif($row->type==8)  {{__('executive/my-day-off.sick_child_leave')}}
                                    @elseif($row->type==9)  {{__('executive/my-day-off.maternity_leave')}}
                                    @elseif($row->type==10)  {{__('executive/my-day-off.discipline')}}
                                    @elseif($row->type==11)  {{__('executive/my-day-off.labor_accident')}}
                                    @elseif($row->type==12){{__('executive/my-day-off.working')}}
                                    @elseif($row->type==13){{__('executive/my-day-off.training')}}
                                    @elseif($row->type==14){{__('executive/my-day-off.meet')}}
                                    @elseif($row->type==15){{__('executive/my-day-off.wedding_funeral')}}
                                    @elseif($row->type==16){{__('executive/my-day-off.difference')}}
                                    @endif
                                </td>
                                <td class="text-center">
                                    {{ $row->leave_day?$row->leave_day:getLeaveDate($row->end_date, $row->start_date)}}
                                </td>
                                <td class="text-center">{{$row->content}}</td>
                                <td class="text-center">{{ $listStatus[$row->status] ?? ''}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center">
                            <span>{{__('common.message.empty_search')}}</span>
                        </div>
                @endif
                <!-- End leave day table -->
                </div>
            </div>
        </div>
    </div>
</div>
