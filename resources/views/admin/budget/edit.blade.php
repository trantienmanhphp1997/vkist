@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.budget.plan_budget')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    <!--start news -->
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.budget.general')}} </a> \
                    <span>{{__('data_field_name.budget.update')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="detail-task information box-idea">
                    <h4 class="border-bottom-header">{{__('data_field_name.budget.update')}}</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.year')}}</label>
                                        <input type="text" class="form-control bg-default disabled_box"
                                               placeholder="" value="{{$data->year_created}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.estimate')}}</label>
                                        <input type="text" class="form-control bg-default disabled_box"
                                               placeholder="" value="{{$data->estimated_code}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.code')}}</label>
                                        <input type="text" class="form-control bg-default disabled_box"
                                               placeholder="" value="{{$data->code}}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.name')}}</label>
                                <input type="text" class="form-control bg-default disabled_box" placeholder=""
                                       value="{{$data->name}}" name="name" disabled>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.total')}}</label>
                                        <input type="text" class="form-control bg-default disabled_box"
                                               placeholder="" value="{{numberFormat($data->total_budget) ?? ''}}"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.note')}}</label>
                                <textarea class="form-control bg-default disabled_box" rows="4"
                                          value="" disabled>{{$data->note}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.content')}}</label>
                                <textarea class="form-control bg-default disabled_box" rows="4"
                                          value="" disabled>{{$data->content}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.research_project')}}</label>
                                        {!! Form::select('research_project_id', $researchProject, $data->research_project_id, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.department')}}</label>
                                        {!! Form::select('department_id', $department, $data->department_id, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="title-plan">{{__('data_field_name.budget.plan_budget')}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::model($data, ['route' => ['admin.budget.update', $data->id], 'method' => 'post']) }}
                    <div class="box-calendar col-md-12 bg-white">
                        @if($data->allocated == 1)
                            @foreach ($data->hasPlan as $key => $val)
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{__('data_field_name.budget.month')}} {{$val->month_budget}}</label>
                                            <input type="text"
                                                   class="form-control format_number bg-default disabled_box"
                                                   placeholder="{{numberFormat($val->money_plan) ?? ''}}"
                                                   value="{{numberFormat($val->money_plan) ?? ''}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{__('data_field_name.budget.month')}} {{$val->month_budget}} {{__('data_field_name.budget.real')}} </label>

                                            <input type="text" class="form-control format_number " placeholder=""
                                                   value="{{numberFormat($val->money_real)??''}}"
                                                   name="money_real[]">
                                            @error('money_real.' . $key) <span
                                                class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4 mt-32">
                                        @livewire('component.files', ['model_name'=>$model_name,'type'=>$type,
                                        'folder'=>$folder, 'model_id'=>$val->id])
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.total_real')}}</label>
                                        <input type="text" class="form-control format_number"
                                               placeholder="" value="{{numberFormat($data->total_real_budget) ?? ''}}"
                                               name="total_real_budget">
                                    </div>
                                </div>
                                <div class="col-md-6 mt-32">
                                    @livewire('component.files', ['model_name'=>$model_name,'type'=>$type,
                                    'folder'=>$folder,'model_id'=>$data->id,'status'=>-1])
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="col-md-12">
                        <div class="group-btn2 text-center group-btn ">
                            @if($data -> request_gw_status == \App\Enums\EBudgetStatus::WAIT)
                                <button type="button" class="btn btn-save"
                                    data-target="#confirmAgain"
                                    data-toggle="modal">{{__('data_field_name.budget.require_change')}}</button>
                            @else
                                <a href="{{route('admin.budget.change-budget', $data->id)}}">
                                    <button type="button" class="btn btn-save" name="">
                                        {{__('data_field_name.budget.require_change')}}
                                    </button>
                                </a>
                            @endif
                            <button type="submit" class="btn btn-save"
                                    name="save">{{__('common.button.save')}}</button>
                            <a href="{{route('admin.budget.list-budget-approved.index')}}">
                                <button type="button" class="btn btn-cancel"
                                        name="cancel">{{__('common.button.cancel')}} </button>
                            </a>

                        </div>
                    </div>
                    <div wire:ignore.self class="modal fade" id="confirmAgain" tabindex="-1"
                         aria-labelledby="exampleModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body box-user">
                                    <h4 class="modal-title">{{__('common.confirm_message.confirm_title')}}</h4>
                                    <p>{{__('common.confirm_message.are_you_sure_confirm_again')}}</p>
                                </div>
                                <div class="group-btn2 text-center  pt-24">
                                    <button type="button" class="btn btn-cancel" data-dismiss="modal"
                                            id="close-delete-need">{{__('common.button.no')}}</button>
                                    <a href="{{route('admin.budget.change-budget', $data->id)}}">
                                        <button type="button" class="btn btn-save" name="">
                                            {{__('common.button.yes')}}
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        </div>
    </div>

    <!--end news -->
@endsection
