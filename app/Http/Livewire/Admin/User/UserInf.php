<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;

class UserInf extends Component
{
    public function render()
    {
        return view('livewire.admin.user.user-inf');
        
    }
}
