<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;
    protected $table = 'users';
    protected $fillable = [
    	'name','email', 'email_verified','password','remember_token','created_at','updated_at',
        'status','admin_id','user_id','user_name'
    ];
    private static $searchable = [
        'name',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
}
