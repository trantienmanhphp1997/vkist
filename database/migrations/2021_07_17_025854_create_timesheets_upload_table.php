<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeSheetsUploadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheets_upload', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('luu du lieu preview');
            $table->string('user_info_code', 255)->nullable()->comment('ghi chu');
            $table->string('user_info_name', 255)->nullable()->comment('ghi chu');
            for($i = 1; $i < 32; $i++){
              $table->string('day' . $i, 5)->nullable()->comment('ngay');
            }
            $table->string('total_work_day', 5)->nullable()->comment('tổng ngày coogn thực thế AH');
            $table->string('unpaid_day', 5)->nullable()->comment('Nghi khong luong AI');
            $table->string('holiday', 5)->nullable()->comment('nghi le AJ');
            $table->string('leave_day', 5)->nullable()->comment('nghi phep AK');
            $table->string('total_leave_day', 5)->nullable()->comment('nghi phep AL');
            $table->string('monthly_leave_day', 5)->nullable()->comment('nghi phep thang AM');
            $table->string('yearly_leave_day', 5)->nullable()->comment('nghi phep thang AM');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->tinyInteger('status')->nullable()->comment('trang thai upload');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheets_upload');
    }
}
