<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use App\Models\MasterData;
use App\Models\TopicHasUserInfo as Member;
use App\Models\UserInf;
use Livewire\Component;

class AddMemberForm extends Component
{

    public $researchs;
    public $roles;
    public $userInfos;

    public $searchUserInfo;
    public $selectedUserInfo;

    public ?Member $member;

    protected $listeners = [
        'changed-user-info-event' => 'selectUserInfo'
    ];

    protected function rules()
    {
        return [
            'member.research_id' => 'required',
            'member.user_info_id' => 'required',
            'member.role_id' => 'required',
            'member.mission_note' => 'required',
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'member.research_id' => __('data_field_name.topic.research_way'),
            'member.user_info_id' => __('data_field_name.topic.member_name'),
            'member.role_id' => __('data_field_name.topic.role'),
            'member.mission_note' => __('data_field_name.topic.mission_note'),
        ];
    }

    public function mount()
    {
        $this->researchs = MasterData::query()->where('type', 19)->get()->mapWithKeys(fn ($item) => [$item['id'] => $item['v_value']])->all();
        $this->roles = MasterData::query()->where('type', 13)->get()->mapWithKeys(fn ($item) => [$item['id'] => $item['v_value']])->all();
        $this->userInfos = UserInf::query()->with('department')->limit(100)->get();
        $this->member = new Member();
    }


    public function render()
    {
        $this->dispatchBrowserEvent('init-select2-user-info-event');
        return view('livewire.admin.research.topic.add-member-form');
    }

    public function save()
    {
        $this->validate();
        $this->resetErrorBag();

        // chỗ này cần tối ưu hoá
        $this->member->load(['userInfo', 'userInfo.academicLevel', 'role', 'research']);

        $this->emitUp('add-member-event', $this->member);
        $this->dispatchBrowserEvent('close-add-member-form-event');
    }

    public function selectUserInfo($data)
    {
        $this->member->user_info_id = $data['id'] ?? null;
        $this->selectedUserInfo = $data;
    }

    public function resetAll() {
        $this->member = new Member();
        $this->selectedUserInfo = [];
    }
}
