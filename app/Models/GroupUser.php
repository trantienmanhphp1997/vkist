<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupUser extends BaseModel
{
    use HasFactory;
    protected $table = 'group_user';
    protected $fillable = [
        'name', 'code', 'description', 'admin_id'
    ];

    public function admin()
    {
        return $this->hasOne(User::class, 'admin_id', 'id');
    }

    public function topics()
    {
        return $this->belongsToMany(Topic::class, 'group_user_has_topic', 'group_user_id', 'topic_id');
    }

    private static $searchable = [
        'name', 'code'
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }
}
