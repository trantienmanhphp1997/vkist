<?php

namespace App\Http\Livewire\Admin\Executive\ContractAndSalary\MySalary;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Contract;
use Illuminate\Support\Facades\Auth;
use App\Models\TimeSheetsMonthly;
use App\Models\SalarySheetsFile;
use App\Models\SalaryBasic;
use App\Models\SalaryPosition;
use App\Models\UserInf;
use Illuminate\Support\Carbon;
use App\Enums\ESalarySheetsFileType;
use Illuminate\Support\Facades\Config;

class Index extends BaseLive
{
	public $filter_month_year;
    public $startOfMonth;
    public $endOfMonth;
    public $daily;
    public $data;

    public function mount() {
        $salarySheetsFile = SalarySheetsFile::select('month_working', 'year_working')->distinct()->orderBy('month_working', 'desc')->get();
        if (!empty($salarySheetsFile) && count($salarySheetsFile) > 0) {
            $this->filter_month_year = $salarySheetsFile[0]->month_working . '/' . $salarySheetsFile[0]->year_working;
        } else {
            $this->filter_month_year = now()->format('m/Y');
        }
    }

    public function render() {
    	$monthYear = explode('/', $this->filter_month_year);
        $startOfWeek = Carbon::create($monthYear[1], $monthYear[0])->startOfWeek();
        
        $this->startOfMonth = Carbon::create($monthYear[1], $monthYear[0])->startOfMonth();
        $this->endOfMonth = Carbon::create($monthYear[1], $monthYear[0])->endOfMonth();
        
        $startOfWeek->diffInDays($this->endOfMonth);

        $user = UserInf::where('id', auth()->user()->user_info_id)->first();
        if (!empty($user)) {
            $userId = $user->id;
        } else {
            $userId = null;
        }
        $monthly = TimeSheetsMonthly::where('user_info_id', $userId)
            ->where('month_working', $monthYear[0])
            ->where('year_working', $monthYear[1])
            ->first();

        $contract = Contract::where('user_info_id', $userId)->orderBy('id', 'desc')->first();

        $salarySheetsFile = SalarySheetsFile::select('month_working', 'year_working')->distinct()->orderBy('month_working', 'desc')->get();
        $allSalarySheetsFile = SalarySheetsFile::select('id')->orderBy('month_working', 'desc')->where('type', ESalarySheetsFileType::BASIC_SALARY)->get();
        $salaryBasic = [
            'salary' => 0,
            'position_allowance' => 0,
            'responsibility_allowance' => 0,
            'other_costs' => 0,
            'lunch_allowance' => 0,
            'insurance' => 0,
            'arrears' => 0,
            'pursuit_salary' => 0,
            'personal_income_tax' => 0,
            'coefficients_salary' => 0
        ];
        $salaryPosition = [
            'salary' => 0,
            'provisional_tax' => 0,
            'incentive_allowance' => 0,
            'arrears' => 0,
            'pursuit' => 0
        ];
        foreach ($allSalarySheetsFile as $item) {
        	$modelSalaryBasic = SalaryBasic::where('salary_sheets_file_id', $item->id)
	        	->where('user_info_id', $userId)
	        	->orderBy('id', 'desc')
	        	->first();
	        if (!empty($modelSalaryBasic)) {
	        	$social_insurance = json_decode($modelSalaryBasic->social_insurance);
	        	$health_insurance = json_decode($modelSalaryBasic->health_insurance);
	        	$accident_insurance = json_decode($modelSalaryBasic->accident_insurance);
	        	$salaryBasic = [
	        		'salary' => $modelSalaryBasic->salary + $modelSalaryBasic->salary_special,
	        		'position_allowance' => json_decode($modelSalaryBasic->allowance)->position_allowance,
	        		'responsibility_allowance' => json_decode($modelSalaryBasic->allowance)->responsibility_allowance,
	        		'other_costs' => $modelSalaryBasic->other_costs,
	        		'lunch_allowance' => $modelSalaryBasic->lunch_allowance,
	        		'insurance' => $social_insurance->service1 + $social_insurance->service2 + $social_insurance->laborer + $health_insurance->service + $health_insurance->laborer + $accident_insurance->service + $accident_insurance->laborer,
	        		'arrears' => $modelSalaryBasic->arrears,
	        		'pursuit_salary' => $modelSalaryBasic->pursuit_salary,
	        		'personal_income_tax' => $modelSalaryBasic->personal_income_tax,
                    'coefficients_salary' => $modelSalaryBasic->coefficients_salary
	        	];
	        }
        }

        $allSalarySheetsFile = SalarySheetsFile::select('id')->orderBy('month_working', 'desc')->where('type', ESalarySheetsFileType::POSITION_SALARY)->get();
        foreach ($allSalarySheetsFile as $item) {
	        $modelSalaryPosition = SalaryPosition::where('salary_sheets_file_id', $item->id)
	        	->where('user_info_id', $userId)
	        	->orderBy('id', 'desc')
	        	->first();
	        if (!empty($modelSalaryPosition)) {
	        	$salaryPosition = [
	        		'salary' => json_decode($modelSalaryPosition->collect_by_position)->money,
	        		'provisional_tax' => $modelSalaryPosition->provisional_tax,
	        		'incentive_allowance' => json_decode($modelSalaryPosition->special_allowance)->money,
	        		'arrears' => $modelSalaryPosition->arrears,
	        		'pursuit' => json_decode($modelSalaryPosition->collect_by_position)->pursuit + json_decode($modelSalaryPosition->special_allowance)->pursuit
	        	];
	        }
        }
        $incomes = $salaryBasic['salary'] + $salaryPosition['salary'] + $salaryBasic['position_allowance'] + $salaryBasic['responsibility_allowance'] + $salaryBasic['other_costs'] + $salaryPosition['incentive_allowance'] + $salaryBasic['lunch_allowance'] + $salaryBasic['pursuit_salary'] + $salaryPosition['pursuit'];
        $deductions = $salaryBasic['insurance'] + $salaryBasic['personal_income_tax'] + $salaryPosition['provisional_tax'] + $salaryBasic['arrears'] + $salaryPosition['arrears'];
        $this->data = [
            'user' => $user,
            'monthly' => $monthly,
            'contract' => $contract,
            'salaryBasic' => $salaryBasic,
            'salaryPosition' => $salaryPosition,
            'incomes' => $incomes,
            'deductions' => $deductions
        ];
        return view('livewire.admin.executive.contract-and-salary.my-salary.index',[
        	'user' => $user,
            'monthly' => $monthly,
            'contract' => $contract,
            'salarySheetsFile' => $salarySheetsFile,
            'salaryBasic' => $salaryBasic,
            'salaryPosition' => $salaryPosition,
            'incomes' => $incomes,
            'deductions' => $deductions
        ]);
    }

    public function dowloadSalarySheetFile() {
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template/salary.docx');
        $templateProcessor->setValue('month', $this->filter_month_year);
        $templateProcessor->setValue('name', !empty($this->data['user']) ? $this->data['user']['fullname'] : '');
        $templateProcessor->setValue('code', !empty($this->data['user']) ? $this->data['user']['code'] : '');
        $templateProcessor->setValue('coefficient', $this->data['salaryBasic']['coefficients_salary']);
        $templateProcessor->setValue('day', !empty($this->data['monthly']) ? (int)$this->data['monthly']->total_work_day : null);
        $templateProcessor->setValue('basic_salary', $this->data['salaryBasic']['salary']);
        $templateProcessor->setValue('position_salary', $this->data['salaryPosition']['salary']);
        $templateProcessor->setValue('allowance', 
            $this->data['salaryBasic']['position_allowance'] + 
            $this->data['salaryBasic']['responsibility_allowance'] + 
            $this->data['salaryBasic']['other_costs'] + 
            $this->data['salaryPosition']['incentive_allowance'] + 
            $this->data['salaryBasic']['lunch_allowance']
        );
        $templateProcessor->setValue('position_allowance', $this->data['salaryBasic']['position_allowance']);
        $templateProcessor->setValue('responsibility_allowance', $this->data['salaryBasic']['responsibility_allowance']);
        $templateProcessor->setValue('work_allowance', $this->data['salaryBasic']['other_costs']);
        $templateProcessor->setValue('incentive_allowance', $this->data['salaryPosition']['incentive_allowance']);
        $templateProcessor->setValue('lunch_allowance', $this->data['salaryBasic']['lunch_allowance']);
        $templateProcessor->setValue('pursuit', 
            $this->data['salaryBasic']['pursuit_salary'] + 
            $this->data['salaryPosition']['pursuit']
        );
        $templateProcessor->setValue('total_incomes', $this->data['incomes']);
        $templateProcessor->setValue('compulsory_insurance', $this->data['salaryBasic']['insurance']);
        $templateProcessor->setValue('tax', 
            $this->data['salaryBasic']['personal_income_tax'] + 
            $this->data['salaryPosition']['provisional_tax']
        );
        $templateProcessor->setValue('arrears', 
            $this->data['salaryBasic']['arrears'] + 
            $this->data['salaryPosition']['arrears']
        );
        $templateProcessor->setValue('total_deductions', $this->data['deductions']);
        $templateProcessor->setValue('total', $this->data['incomes'] - $this->data['deductions']);
        $today = date("d_m_Y");
        $docxFile = 'salary_' . $today . '.docx';
        $path  = storage_path("app/uploads/" . $docxFile);
        $templateProcessor->saveAs($path);
        return response()->download($path);
    }
}
