@extends('layouts.master')
@section('title')
    <title>Quản lý thông báo</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    @livewire('admin.config.notification.notification-list')
@endsection
