<?php

namespace App\Exports;

use App\Models\WorkPlan;
use App\Models\MasterData;
use App\Models\UserInf;
use App\Enums\EWorkPlanPriorityLevel;
use App\Enums\EWorkPlan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
class WorkPlanListExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $getIdSelected;

    function __construct($searchTerm, $searchPriority, $searchStatus) {
        $this->searchTerm = $searchTerm;
        $this->searchPriority = $searchPriority;
        $this->searchStatus = $searchStatus;
    }

    public function collection()
    {
        $query= WorkPlan::leftjoin('user_info', function($join){
            $join->on('work_plan.leader_id','=','user_info.id');
        });
        $query->leftjoin('department', function($join){
            $join->on('department.id','=','user_info.department_id');
        });
        $query->leftjoin('master_data', function($join){
            $join->on('master_data.id','=','work_plan.priority_id');
        });
        // ->where('master_data.id','=','work_plan.work_type_id')
        $query->leftjoin(DB::raw('master_data b'), function($join){
            $join->on('b.id','=','work_plan.work_type_id');
        });
        $query->select('work_plan.*',DB::raw('user_info.fullname as leader_name'),DB::raw('department.name as leader_department_name'),DB::raw('master_data.v_value as value_priority'),DB::raw('b.v_value as work_type'),DB::raw('master_data.order_number as order_number'))->get();

        $priority=MasterData::where('type',9)->pluck('v_value','id');
        $priority->prepend('Mức ưu tiên', '');

        if (!empty($this->searchTerm)) {
            $query->where('work_plan.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        if ($this->searchPriority) {
            $query->where('work_plan.priority_level','=',$this->searchPriority);
        }
        if ($this->searchStatus) {
            $query->where('work_plan.status','=',$this->searchStatus);
        }
        $query = $query->orderBy('work_plan.id','DESC')->get();
        $query = $query->map(function ($item, $index) {
            if ($item->status == EWorkPlan::STATUS_NEW) {
                $status = __('data_field_name.work_plan.index.status_new');
            } else if ($item->status == EWorkPlan::STATUS_WAIT_APPROVAL) {
                $status = __('data_field_name.work_plan.index.wait');
            } else if ($item->status == EWorkPlan::STATUS_APPROVAL) {
                $status = __('data_field_name.work_plan.index.done');
            } else if ($item->status == EWorkPlan::STATUS_REJECT) {
                $status = __('data_field_name.work_plan.index.deny');
            } else {
                $status = null;
            }
            return [
                'index' => $index + 1,
                'code' => $item->missions_code,
                'content' => $item->content,
                'priority' => EWorkPlanPriorityLevel::valueToName($item->priority_level),
                'leader' => $item->leader_name,
                'start_date' => $item->start_date,
                'end_date' => $item->end_date,
                'estimated_cost' => $item->estimated_cost,
                'status' => $status
            ];
        });
        return $query;
    }
    public function headings():array
    {
        return
        [
            'STT',
            'Mã kế hoạch',
            'Nội dung công tác',
            'Mức ưu tiên',
            'Trưởng đoàn',
            'Ngày bắt đầu (KH)',
            'Ngày kết thúc (KH)',
            'Kinh phí dự kiến',
            'Trạng thái',
        ];
    }
    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $highestRow = $active_sheet->getHighestRow();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            ];
            $borderStyle = [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [
                       'rgb' => '808080'
                    ]
                ]
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A3:I3');
            $event->sheet->setCellValue('A3','DANH SÁCH KẾ HOẠCH CÔNG TÁC');
            $event->sheet->getStyle('A3:I3')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A6:I6';
            
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle('A6:I' . $highestRow)->getBorders()->applyFromArray($borderStyle);
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle('A3:I3')->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        return 'DANH SÁCH KẾ HOẠCH CÔNG TÁC';
    }

    public function startCell(): string
    {
        return 'A6';
    }
}
