<div>
    <div class="row">
        <div class="col-md-6">
            <h3 class="title-personal">{{ __('data_field_name.working_process.themes_list') }}</h3>
        </div>
    </div>

    {{-- @dump($userInfo) --}}

    <table class="table border-bottom">
        <thead>
            <tr class="border-radius">
                <th class="align-middle" scope="col"> {{ __('data_field_name.working_process.theme_name') }}</th>
                <th class="align-middle" scope="col">{{ __('data_field_name.working_process.role') }}</th>
                <th class="align-middle" scope="col">{{ __('data_field_name.working_process.sponsoring_organization') }}</th>
                <th class="align-middle" scope="col">{{ __('data_field_name.working_process.expense') }}</th>
                <th class="align-middle" scope="col">{{ __('data_field_name.working_process.achievement') }}</th>
                <th class="align-middle" scope="col">{{ __('data_field_name.common_field.status') }}</th>
                <th class="align-middle" scope="col">{{ __('data_field_name.working_process.research_year') }}</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($items as $item)
                <tr class="border-radius">
                    <td >{{ $item->topic->name ?? '' }}</td>
                    <td >{{ $item->role->v_value ?? '' }}</td>
                    <td ></td>
                    <td >{{ number_format($item->topic->expected_fee ?? 0) }}</td>
                    <td >{{ $item->topic->achievement_note ?? '' }}</td>
                    <td >{{ App\Enums\ETopicStatus::valueToName($item->topic->status ?? 0) }}</td>
                    <td >{{ !empty($item->created_at) ? date('Y', strtotime($item->created_at)) : '' }}</td>
                </tr>
            @endforeach

            @if (count($items) == 0)
                <tr>
                    <td colspan="6">{{ __('data_field_name.working_process.empty') }}</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
