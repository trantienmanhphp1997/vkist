<?php
return [
	'template-salary-basic' => 'Basic salary template file',
	'template-salary-position' => 'Position salary template file',
    'title' => 'Salary data',
    'btn-add' => 'Add salary data',
    'result' => 'Result',
    'table_column' => [
        'name' => 'Name',
        'duration' => 'Duration',
        'type' => 'Type'
    ],
];