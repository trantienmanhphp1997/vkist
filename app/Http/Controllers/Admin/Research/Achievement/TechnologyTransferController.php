<?php

namespace App\Http\Controllers\Admin\Research\Achievement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Enums\EResearchAchievementType;
use App\Models\User;
use App\Models\ResearchAchievement;
use DB;

class TechnologyTransferController extends Controller
{
    //
    public function index(Request $request){
        $type = EResearchAchievementType::TECHNOLOGY_TRANSFER;
        return view('admin.research.achievement.index', compact('type'));
    }

    public function create(Request $request){
        $type = EResearchAchievementType::TECHNOLOGY_TRANSFER;
        return view('admin.research.achievement.create', compact('type'));
    }

    public function edit($id){
        $type = EResearchAchievementType::TECHNOLOGY_TRANSFER;
        $data = ResearchAchievement::where('type', $type)->findOrFail($id);
        if(is_null($data)) {
            return redirect()->route('admin.research.achievement.technology-transfer.index');
        }
        return view('admin.research.achievement.edit', compact(['type','data']));
    }
}
