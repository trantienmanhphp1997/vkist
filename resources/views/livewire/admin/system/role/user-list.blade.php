<div class="list-user-role">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group search-expertise search-plan">
                <div class="search-expertise inline-block">
                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="form-control size13" placeholder="{{__('data_field_name.system.role.text_placeholder_user')}}" />
                    <span>
                        <img src="/images/Search.svg" alt="search"/>
                    </span>

                </div>
            </div>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr class="border-radius">
                <th scope="col" class="border-radius-left">{{__('data_field_name.common_field.stt')}}</th>
                <th scope="col">{{__('data_field_name.system.role.name_user')}}</th>
                <th scope="col">{{__('data_field_name.system.role.department')}}</th>
                <th scope="col" class="border-radius-right text-center">{{__('data_field_name.system.role.selection')}}</th>
            </tr>
        </thead>
        <div wire:loading class="loader"></div>
        <tbody>
            @foreach($data as $row)
                <tr>
                    <td>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                    <td>{!! $row->name !!}</td>
                    <td>{!! $row->info->position->v_value ?? null !!}</td>
                    <td>
                        <label class="box-checkbox checkbox-analysis">
                            @if (!empty($listIdUser) && in_array($row->id, $listIdUser))
                            <input name="user[{{$row->id}}]" type="checkbox" value="1" checked="checked"  wire:click="userClicked" wire:model.lazy="userId.{{$row->id}}" />
                            @else
                            <input name="user[{{$row->id}}]" type="checkbox" value="1" wire:click="userClicked" wire:model.lazy="userId.{{$row->id}}" />
                            @endif
                            <span class="checkmark"></span>
                        </label>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @if(count($data) > 0)
        {{$data->links()}}
    @else
        <div class="title-approve text-center">
            <span>{{__('data_field_name.system.role.no_result')}}</span>
        </div>
    @endif
</div>
