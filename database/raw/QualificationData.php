<?php
return [
    [
        'type' => '5',
        'v_key' => 'education_background',
        'v_value' => 'Tiến sĩ',
        'v_value_en' => 'Doctor',
        'order_number' => '1',
    ],
    [
        'type' => '5',
        'v_key' => 'education_background',
        'v_value' => 'Thạc sĩ',
        'v_value_en' => 'Master',
        'order_number' => '2',
    ],
    [
        'type' => '5',
        'v_key' => 'education_background',
        'v_value' => 'Đại học',
        'v_value_en' => 'University',
        'order_number' => '3',
    ],
    [
        'type' => '5',
        'v_key' => 'education_background',
        'v_value' => 'Cao đẳng',
        'v_value_en' => 'College',
        'order_number' => '4',
    ],
    [
        'type' => '5',
        'v_key' => 'education_background',
        'v_value' => 'Trung cấp',
        'v_value_en' => 'Intermediate',
        'order_number' => '5',
    ],
    [
        'type' => '5',
        'v_key' => 'education_background',
        'v_value' => 'Cấp 3',
        'v_value_en' => 'High School',
        'order_number' => '6',
    ],
];
