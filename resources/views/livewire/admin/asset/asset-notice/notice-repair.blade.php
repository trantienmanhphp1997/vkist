<div class="modal fade modal-fix" wire:ignore.self id="notice_repair" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form wire:submit.prevent="submit">
                <div class="modal-header">
                    <div class="row width_100">
                        <div class="col-11">
                            <h6 class="text_center">{{__('data_field_name.asset.repair_warranty')}}</h6>
                        </div>
                        <div class="col-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true close-btn">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-header">
                    <div class="form-group info-request">
                        <label class="text_center">{{__('data_field_name.asset.number_report')}}<span class="text-danger">*</span></label>
                        <input type="text" wire:model.defer="number_report" maxlength="10">
                        @error('number_report') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pd-col">
                                <div class="info-asset">
                                    @livewire('admin.asset.asset-notice.asset-info',['assetDetail'=>$assetDetail])
                                </div>
                            </div>
                            <div class="col-6 pd-col-15">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.asset_quantity')}}
                                        <span class="text-danger">*</span></label>
                                    <input type="number" wire:model.defer="implementation_quantity" class="num"
                                           maxlength="3"
                                           @if ($type_manage==\App\Enums\EAssetCategoryTypeManage::CODE)
                                           disabled
                                        @endif
                                    >
                                    @error('implementation_quantity') <span class="text-danger">{{ $message }}</span>@enderror
                                    @if(!empty($errorQuantity))
                                        <div class="text-danger mb-3">{{$errorQuantity}}</div>
                                    @endif
                                </div>
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.repair_date')}}<span class="text-danger">*</span></label>
                                    @include('layouts.partials.input._inputDate', ['input_id' => 'implementation_date', 'place_holder'=>'', 'default_date' => is_null($implementationDate) ? date('Y-m-d') : $implementationDate ])
                                    @error('implementation_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.estimated_completion_date')}}</label>
                                    @include('layouts.partials.input._inputDate', ['input_id' => 'estimated_completion_date', 'place_holder'=>'','default_date'=>'$estimated_completion_date'])
                                    @error('estimated_completion_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.allocated-revoke.performer')}}</label>
                                    <select class="form-control form-control-lg" wire:model.lazy="user_info_id">
                                        <option>--{{__('data_field_name.allocated-revoke.select_performer')}}--</option>
                                        @foreach ($user as $key=> $val)
                                            <option value="{{$key}}">
                                                {{$val}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.asset_provider_id')}}</label>
                                    <select class="form-control form-control-lg" wire:model.lazy="asset_provider_id">
                                        <option>--{{__('data_field_name.asset.select_asset_provider_id')}}--</option>
                                        @foreach ($provider as $key=> $val)
                                            <option value="{{$key}}">
                                                {{$val}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
{{--                                <div class="form-group ">--}}
{{--                                    <input type="checkbox" name="achievement_paper_check" id="a-1" value="1"--}}
{{--                                           wire:model.lazy="finished">--}}
{{--                                    <label for="a-1">{{__('data_field_name.asset.status_completed')}} </label>--}}
{{--                                </div>--}}
                            </div>
                            <div class="col-6 pd-col-15">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.estimated_cost')}}</label>
                                    <input type="text" wire:model.defer="estimated_cost"
                                           value="" maxlength="13" class="num form-control format_number ">
                                    @error('estimated_cost') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.content_repair')}}</label>
                                    <textarea class="form-control" rows="3" wire:model.defer="content_repair" ></textarea>
                                    @error('content_repair') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label>{{__('data_field_name.asset.repair_place')}}</label>
                                    <div class="form-group checkradio">
                                        @foreach($placeMaintenance as $key => $val)
                                            <input type="radio" value="{{$key}}" wire:model.lazy="place">
                                            <label for="">{{$val}}</label>&ensp;
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div id="file_right">
                                    <label>{{__('data_field_name.asset.file')}}</label>
                                    @livewire('component.files',['uploadOnShow' => 1, 'model_name'=>$model_name, 'type'=>$type,'folder'=>$folder,'status'=>$status])

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" id="close-notice-repair"
                            data-dismiss="modal" wire:click.prevent="cancel()">{{__('common.button.cancel')}}
                    </button>
                    <button type="button" class="btn btn-primary"
                            wire:click.prevent="save()">{{__('common.button.perform')}}
                    </button>
                </div>
            </form>
        </div>
    </div>

</div>
