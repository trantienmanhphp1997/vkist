<?php

namespace App\Http\Controllers\Admin\Errors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ErrorsController extends Controller
{
    public function indexErrors()
    {
        return view('admin.errors.error404');
    }
    public function indexDisconect()
    {
        return view('admin.errors.disconect_error');
    }
    public function indexWarning()
    {
        return view('admin.errors.alert');
    }
}
