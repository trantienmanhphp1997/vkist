@extends('layouts.master')
@section('title')
    <title>{{__('research/project.name')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    <livewire:admin.research-project.information.draft-list />
@endsection
