<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('data_field_name.budget.general')}} </a> \
                <span>{{__('data_field_name.budget.update')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="detail-task information box-idea">
                <form wire:submit.prevent="submit">
                    <h4 class="border-bottom-header">{{__('data_field_name.budget.update')}}</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.year')}}</label>
                                        <input type="text" class="form-control bg-default disabled_box"
                                               placeholder="" value="{{$data->year_created}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.estimate')}}</label>
                                        <input type="text" class="form-control bg-default disabled_box"
                                               placeholder="" value="{{$data->estimated_code}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.code')}}</label>
                                        <input type="text" class="form-control bg-default disabled_box"
                                               placeholder="" value="{{$data->code}}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.name')}}</label>
                                <input type="text" class="form-control bg-default disabled_box" placeholder=""
                                       value="{{$data->name}}" name="name" disabled>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.total')}}<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control format_number" placeholder=""
                                               wire:model.defer="total_budget"
                                               @if ($data->request_gw_status == \App\Enums\EBudgetStatus::WAIT)
                                                   disabled
                                               @endif
                                        >
                                        @error('total_budget') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.content')}}</label>
                                        <input type="text" class="form-control bg-default disabled_box"
                                               placeholder="" value="{{$data->content}}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.budget.note')}}</label>
                                <textarea class="form-control bg-default disabled_box" rows="4"
                                          value="" disabled>{{$data->note}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="title-plan">{{__('data_field_name.budget.plan_budget')}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-calendar col-md-12 bg-white">
                        @if($data->allocated == 1)
                            @if($data -> request_gw_status == \App\Enums\EBudgetStatus::WAIT)
                                <div class="row">
                                    <div class="col-md-4">
                                        @foreach ($money_plan_draft as $key => $item)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{__('data_field_name.budget.month')}} {{$key +1}}</label>
                                                        <input type="text" class="form-control format_number "
                                                               placeholder=""
                                                               value="{{$item ?? ''}}" disabled
                                                               name="money_plan[]"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-8">
                                        @foreach ($data->hasPlan as $key => $val)
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{__('data_field_name.budget.month')}} {{$val->month_budget}} {{__('data_field_name.budget.real')}} </label>

                                                        <input type="text"
                                                               class="form-control format_number bg-default disabled_box"
                                                               placeholder="{{numberFormat($val->money_real) ?? ''}}"
                                                               value="{{numberFormat($val->money_real)??''}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mt-32" wire:ignore>
                                                    @livewire('component.files', ['model_name'=>$model_name,'type'=>$type,
                                                    'folder'=>$folder, 'model_id'=>$val->id,'canUpload'=>false])
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                @foreach ($money_plan as $key => $val)
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.budget.month')}} {{$key +1}}</label>
                                                <input type="text" class="form-control format_number "
                                                       placeholder=""
                                                       wire:model.defer="money_plan.{{$key}}">
                                                @error('money_plan.' . $key) <span
                                                    class="text-danger">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.budget.month')}} {{$key +1}} {{__('data_field_name.budget.real')}} </label>

                                                <input type="text"
                                                       class="form-control format_number bg-default disabled_box"
                                                       placeholder="{{numberFormat($money_real[$key]) ?? ''}}"
                                                       value="{{numberFormat($money_real[$key])??''}}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mt-32" wire:ignore>
                                            @livewire('component.files', ['model_name'=>$model_name,'type'=>$type,
                                            'folder'=>$folder, 'model_id'=>$idBudget[$key],'canUpload'=>false])
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @else
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.budget.total_real')}}</label>
                                        <input type="text" class="form-control format_number bg-default disabled_box"
                                               placeholder="" value="{{numberFormat($data->total_real_budget) ?? ''}}"
                                               disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-32" wire:ignore>
                                    @livewire('component.files', ['model_name'=>$model_name, 'type'=>$type,'folder'=>$folder,
                                     'model_id'=>$data->id,'canUpload'=>false])
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="group-btn2 text-center group-btn ">
                            <button type="button" wire:click="save({{$data->id}})" class="btn btn-save"
                            >{{__('common.button.send_proposal')}}</button>

                            <a href="{{route('admin.budget.list-budget-approved.index')}}">
                                <button type="button" class="btn btn-cancel"
                                        name="cancel">{{__('common.button.cancel')}} </button>
                            </a>

                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>
    @if ($request_change)
    @livewire('common.api-approval', ['modelId' => $data->id, 'type' => \App\Enums\EApiApproval::TYPE_BUDGET,
    'requestId' => $requestId, 'approvalId' => '', 'idModelDraft' => $data->id])
    @endif
</div>

