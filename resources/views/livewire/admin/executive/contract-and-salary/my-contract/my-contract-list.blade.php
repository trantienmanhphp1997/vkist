<div class="col-md-10 col-xl-11 box-detail box-user">
	<div class="row">
		<div class="col-md-12">
				<div  class="breadcrumbs"><a href="{{route('admin.research.ideal.index')}}">{{__('executive/contract.breadcrumbs.activity-management')}}</a> \ <span>{{__('executive/contract.list')}}</span></div> 
		</div>
	</div>
	<div class="row bd-border">
		<div class="col-md-12">
			<h3 class="title">{{__('executive/contract.list')}}</h3>
			<div class="information">
				<div class="inner-tab pt0">
					<div class="table-responsive">
						<table class="table table-general">
							<thead>
								<tr class="border-radius">
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.code')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.contract_type')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.position')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.start_date')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.end_date')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.coefficients_salary')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.bank_account')}}
									</th>
									<th scope="col" class="text-center border-radius-right">
										{{__('executive/contract.table_column.bank_name')}}
									</th>
								</tr>
							</thead>
							<div wire:loading class="loader"></div>
							<tbody>
								@foreach($data as $row)
									<tr>
										<td class="text-center">{!! $row->contract_code !!}</td>
										<td class="text-center">{{App\Enums\EContractType::valueToName($row->contract_type)}}</td>
										<td class="text-center">{!! $row->position->v_value ?? '' !!}</td>
										<td class="text-center">{!! date('d/m/Y', strtotime($row->start_date)) !!}</td>
										<td class="text-center">{!! $row->contract_type != App\Enums\EContractType::INDEFINITE_TERM ? date('d/m/Y', strtotime($row->end_date)) : null !!}</td>
										<td class="text-center">{!! $row->coefficients->v_value ?? '' !!}</td>
										<td class="text-center">{!! $row->bank_account_number !!}</td>
										<td class="text-center">{!! $row->bankName->v_value ?? '' !!}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@if(count($data) > 0)
						{{$data->links()}}
					@else
						<div class="title-approve text-center ml-0">
							<span>{{__('common.message.empty_search')}}</span>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>