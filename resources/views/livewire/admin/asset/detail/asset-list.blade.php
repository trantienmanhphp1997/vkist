
<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('data_field_name.asset.list_title')}} </a> \
                <span>{{__('data_field_name.asset.list_sub_title')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.asset.list_sub_title')}}</h3>
                </div>

                <div class="col-md-6 ">
                    <div class=" float-right">
                        @if($checkCreatePermission)
                            <div class="dropdown navbar manipulation bg_blue">
                                <a class="btn  dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{__('data_field_name.asset.add_btn')}}
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#import">
                                        {{__('data_field_name.asset.file')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('admin.asset.create')}}">
                                        {{__('data_field_name.asset.add_btn')}}
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block">
                                    <input type="text" class="form-control size13" name="searchTerm" autocomplete="off"
                                           wire:model.debounce.1000ms="searchTerm"
                                           placeholder="{{__('data_field_name.asset.search_place')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3 bg-light py-3" style="border-radius: 12px;" wire:ignore>
                        <div class="col-md-3">
                            {{ Form::select('asset_category', ['' => __('data_field_name.asset.asset_cate')] + $assetCategories, $searchAssetCategory, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchAssetCategory']) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::select('department', ['' => __('data_field_name.asset.choose_department')] + $departments, $searchDepartment, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchDepartment']) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::select('origin', ['' => __('data_field_name.asset.origin')] + $assetOrigins, $searchOrigin, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchOrigin']) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::select('owner', ['' => __('data_field_name.asset.owner')] + $owners, $searchOwner, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchOwner']) }}
                        </div>
                    </div>

                    <table class="table table-general" id="dataTable">
                        <thead>
                        <tr class="border-radius">
                            <th scope="col" class="text-uppercase border-radius-left">{{__('data_field_name.asset.asset_code')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.asset_name')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.asset_cate')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.asset_quantity')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.units')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.unit_price')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.asset_value')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.status')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.origin')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.department')}}</th>
                            <th scope="col" class="text-uppercase">{{__('data_field_name.asset.owner')}}</th>
                            <th class="text-uppercase border-radius-right" scope="col">
                                {{__('data_field_name.common_field.action')}}</th>
                        </tr>
                        </thead>
                        <div wire:loading class="loader"></div>

                        <tbody>
                        @forelse($data as $key=> $val)
                            <tr>
                                <td><a class="text-primary" href="{{route('admin.asset.show',$val->id)}}">
                                        {!! boldTextSearch($val->code,$searchTerm) !!}
                                    </a>
                                </td>
                                <td> {!! boldTextSearch($val->name,$searchTerm) !!}</td>
                                <td>{{$val->category->name ?? ''}}</td>
                                <td>{{$val->quantity}}</td>
                                <td>{{$val->unit->v_value ?? ''}}</td>
                                <td>{{number_format($val->asset_value)}}</td>
                                <td>
                                    {{number_format($val->quantity*$val->asset_value)}}
                                </td>
                                <td>
                                    @if($val->situation == \App\Enums\EAssetSituation::SITUATION_USING|$val->situation == \App\Enums\EAssetSituation::SITUATION_NOT_USE)
                                        {{$assetSituation[$val->situation] ?? ''}}
                                    @else
                                        {{$assetStatuss[$val->situation] ?? ''}}
                                    @endif
                                </td>
                                <td>{{ $val->originAsset->origin_name ?? '' }}</td>
                                <td>{{ $val->department->name ?? ''}}</td>
                                <td>{{ $val->user->fullname ?? '' }}</td>
                                <td class="text-center">
                                    @if($checkDestroyPermission)
                                    <button type="button" title="{{__('common.button.delete')}}" style="background-color: white; border:none;"
                                            wire:click="deleteIdAsset({{$val->id}})" data-target="#Delete"
                                            data-toggle="modal">
                                        <img src="/images/trash.svg" alt="trash">
                                    </button>
                                    @endif
                                </td>
                            </tr>

                        @empty
                            <tr class="text-danger">
                                <td colspan="12">{{__('common.message.no_data')}}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{$data->links()}}
                    <div class="col-md-12" scope="col colUser">
                        {{__('data_field_name.asset.number_record')}}
                        <strong>{{ count($data) }}</strong>&emsp;
                        {{__('data_field_name.asset.total_asset')}}
                        <strong>{{ $data->sum('quantity') }}</strong>&emsp;
                        {{__('data_field_name.asset.value_asset')}}
                        <strong>{{ numberFormat($total) }}</strong>&nbsp;
                        <span>
                            <a href="{{route('admin.asset.dashboard.index')}}">
                                {{__('data_field_name.asset.status_statistics')}}
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    <!-- Modal -->--}}
    {{--    @include('livewire.admin.asset.detail._add_asset')--}}
    {{--    --}}{{--    thong tin taisan --}}

    {{--    @include('livewire.admin.asset.detail._asset_item_info')--}}
    {{--    --}}{{--    bao mat tai san--}}
    {{--    @include('livewire.admin.asset.detail._report_lost')--}}
    {{--    @include('livewire.admin.asset.detail._report_error')--}}

    {{--    @include('livewire.admin.asset.detail._report_transfer')--}}
    {{--    @include('livewire.admin.asset.detail._report_transfer_series')--}}
    {{--    @include('livewire.admin.asset.detail._report_recall')--}}
    {{--    @include('livewire.admin.asset.detail._report_recall_series')--}}
    @include('livewire.admin.asset.detail._modal_import')
    <div wire:ignore.self class="modal fade" id="Delete" tabindex="-1"
         aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{__('common.confirm_message.confirm_title')}}</h4>
                    <p>{{__('common.confirm_message.are_you_sure_delete')}}</p>
                </div>
                <div class="group-btn2 text-center  pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal" id="close-delete">{{__('common.button.no')}}</button>
                    <button type="button" wire:click.prevent="delete()" class="btn btn-save"
                            data-dismiss="modal">{{__('common.button.yes')}}</button>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $("document").ready(function() {
        window.livewire.on('closeDelete', () => {
            document.getElementById('close-delete').click();
        });
    });
</script>
