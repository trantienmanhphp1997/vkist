<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEstimateCostToWorkPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_plan', function (Blueprint $table) {
            $table->bigInteger('estimated_cost')->nullable()->comment('kinh phi du kien');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_plan', function (Blueprint $table) {
            $table->dropColumn('estimated_cost');
        });
    }
}
