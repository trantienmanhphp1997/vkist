@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.topic.edit') }}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    @livewire('admin.research.topic.topic-editor', ['topicId' => $topicId])
@endsection
