<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content={{ csrf_token() }}>
    @yield('title')
    <link rel="stylesheet" href="{{ asset('/css/trix.css') }}">
    <script src="{{ asset('js/trix.js') }}"></script>
    <script src="{{ asset('js/attachments.js') }}"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.css') }}">
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    @livewireStyles
    @livewireScripts
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/kendo.bootstrap-v4.min.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('assets/js/highcharts.js') }}"></script>
    <script src="https://code.highcharts.com/modules/xrange.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/laravel-echo/1.11.1/echo.iife.min.js" integrity="sha512-vkud3zbr/2ph4poXT2D78nVaOc8JPXWdwDFHg1JeCrZp+AXsND9xcL/3bey1ah+97gAK19QiwaBwk40m8x+IQg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pusher/7.0.3/pusher.min.js" integrity="sha512-XVnzJolpkbYuMeISFQk6sQIkn3iYUbMX3f0STFUvT6f4+MZR6RJvlM5JFA2ritAN3hn+C0Bkckx2/+lCoJl3yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip()
            });
        </script>
</head>
