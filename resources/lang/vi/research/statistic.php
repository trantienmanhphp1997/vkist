<?php
return [
    'breadcrumbs' => [
        'research_project_management' => 'Quản lý dự án nghiên cứu',
        'research_statistic' => 'Thống kê nghiên cứu',
    ],
    'title' => [
        'general' => 'Tổng quan',
        'project_statistic_by_status' => 'Thống kê dự án theo trạng thái',
        'task_statistic_by_status' => 'Thống kê công việc theo trạng thái',
        'ideal_statistic_by_status' => 'Thống kê ý tưởng theo trạng thái',
        'task_statistic' => 'Thông kê công việc',
        'task_in_progress' => 'Công việc đang thực hiện',
        'total_task' => 'Tổng công việc',
        'total_ideal' => 'Tổng ý tưởng',
        'total_project' => 'Tổng dự án',
        'project_overview' => [
            'project_overview' => 'Tổng quan tình hình dự án',
            'done' => 'Hoàn thành',
            'in-progress' => 'Đang thực hiện',
            'out_of_date' => 'Quá hạn',
            'not_started' => 'Chưa khởi động',
        ],
        'project_schedule' => [
            'project_schedule' => 'Lịch trình dự án',
            'implementation_meeting' => 'Họp triển khai',
            'unified_scope' => 'Duyệt mục tiêu',
            'finance' => 'Tài chính',
            'software_requirements' => 'Yêu cầu phần mềm',
            'hardware_requirements' => 'Yêu cầu phần cứng',
            'resource_plan' => 'Kế hoạch nguồn lực',
            'human_recource' => 'Nhân sự',
            'technical_requirements' => 'Yêu cầu kĩ thuật',
            'testing_1' => 'Chạy thử 1',
            'testing_2' => 'Chạy thử 2',
            'release' => 'Tung sản phẩm',
        ],
        'budget' => [
            'budget' => 'Ngân sách',
            'plan' => 'Kế hoạch',
            'reality' => 'Thực tế',
        ],
        'project' => [
            'in-progress' => 'Dự án đang thực hiện',
            'waiting' => 'Dự án chưa thực hiện',
            'done' => 'Dự án đã hoàn thành',
            'total' => 'tổng dự án',
            'status' => [
                'on_schedule' => 'Đúng tiến độ',
                'behind_schedule' => 'Nguy cơ chậm tiến độ',
                'ahead_of_schedule' => 'Vượt tiến độ'
            ]
        ],
        'task' => [
            'in-progress' => 'Công việc đang thực hiện',
            'waiting' => 'Công việc chưa thực hiện',
            'done' => 'Công việc đã hoàn thành',
            'total' => 'tổng công việc',
            'status' => [
                'on_schedule' => 'Đúng tiến độ',
                'behind_schedule' => 'Nguy cơ chậm tiến độ',
                'ahead_of_schedule' => 'Vượt tiến độ'
            ],
            'task_status' => [
                'done' => 'Hoàn thành',
                'waiting' => 'Chưa thực hiện',
                'in-progress' => 'Đang thực hiện',
            ]
        ]
    ],

    'table_column' => [
        'mission' => 'Nhiệm vụ',
        'assignee' => 'Giao cho',
        'priority' => 'Ưu tiên',
        'status' => 'Tình trạng',
    ]
];
