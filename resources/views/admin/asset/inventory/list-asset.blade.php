@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.asset_inventory.list_asset')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    @livewire('admin.asset.inventory.list-asset', ['inventory_id' => $inventory_id])
@endsection
