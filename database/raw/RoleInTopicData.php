<?php
$text_role = 'Role in topic';
return [
    [
        'type' => 13,
        'v_key' => $text_role,
        'v_value' => 'Chủ nhiệm đề tài',
        'order_number' => 0
    ],
    [
        'type' => 13,
        'v_key' => $text_role,
        'v_value' => 'Nghiên cứu viên',
        'order_number' => 1
    ],
    [
        'type' => 13,
        'v_key' => $text_role,
        'v_value' => 'Thành viên',
        'order_number' => 2
    ]
];
