document.addEventListener('DOMContentLoaded', function(){
    window.onload=function(){
            Livewire.on('showBudgetData',function(data_arr, lang){
                Highcharts.chart('budget-chart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: lang.title
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                            }
                        }
                    },
                    xAxis: {
                        type: 'category',
                        lineWidth: 0,
                        tickWidth: 0
                    },

                    yAxis: {
                        title: {
                            text: 'VND'
                        }
                    },

                    series: [{
                        dataLabels: [{
                            align: 'left',
                            format: ''
                        }, {
                            align: 'left',
                            format: ''
                        }],
                        color: '#6AD8A7',
                        name: lang.money_plan,
                        data:  data_arr[0],
                    },
                    {
                        dataLabels: [{
                            align: 'center',
                            format: '{point.percent}',
                        }],
                        color: '#FF8064',
                        name: lang.money_real,
                        data:  data_arr[1],
                    }]
                });
            })

    }
})




