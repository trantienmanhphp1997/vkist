<?php

namespace App\Models;

use App\Enums\EBudgetStatus;
use App\Models\BaseModel;
use App\Notifications\Handlers\NotificationHandler;
use App\Notifications\WorkResultNotification;
use App\Service\Community;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Facades\Config;
class Budget extends BaseModel implements Auditable
{
    use HasFactory;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $table='budget';
    protected $guarded=['*'];
    protected $fillable = [
        'year_create',
        'code',
        'name',
        'content',
        'estimated_code',
        'status',
        'total_budget',
        'admin_id',
        'real_end_date',
        'status',
        'result',
        'leader_id',
        'estimated_cost',
        'result',
        'version',
        'request_gw_data',
        'request_gw_status',
        'total_real_budget',
        'gw_attach_file'
    ];

    protected $auditInclude = [
        'total_budget',
        'version',

    ];
    public function hasPlan()
    {
        return $this->hasMany(BudgetHasExpensePlan::class, 'budget_id');
    }
    public function rate()
    {
        return $this->hasMany(Audit::class, 'auditable_id')
            ->where('auditable_type', 'App\Models\Budget')
            ->where('event', 'created');
    }

    public function file()
    {
        return $this->hasMany(File::class, 'model_id')
            ->where('type', Config::get('common.type_upload.BudgetHasExpensePlan'))
            ->where('model_name', BudgetHasExpensePlan::class);
    }

    private static $searchable = [
        'name', 'code', 'content'
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }

    public function transformAudit(array $data): array
    {
        $dataRequest = request()->all();
        $request_gw_data = $this->request_gw_data;
        $note = '';
        if(isset($dataRequest['total_budget'])||  isset($dataRequest['version'])){
            $total = Community::getAmount($dataRequest['total_budget']);
            //tao ngan sach
            if (isset($dataRequest['money_plan'])){
                $plan = $dataRequest['money_plan'];
                $data_plan = implode(" ", $plan);
                $data_json = ['total_budget'=>$total,'money_plan'=>$data_plan];
                $note = json_encode($data_json);

                $data['note'] =  $note;
            }else{
                $data_json = ['total_budget'=>$total];
                $note = json_encode($data_json);
                $data['note'] =  $note;

            }
        }
        $arr_request_gw_data = json_decode($request_gw_data, TRUE);
        if(isset($arr_request_gw_data["money_plan"])){
            $data['new_values']["money_plan"] = $arr_request_gw_data["money_plan"];
        }
        if(isset($arr_request_gw_data["money_plan_old"])){
            $data['old_values']["money_plan"] = $arr_request_gw_data["money_plan_old"];
        }
        return $data;

    }

    public function researchProject()
    {
        return $this->belongsTo(ResearchProject::class, 'research_project_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function numberOfUpdates()
    {
        return Audit::where('auditable_type', get_class($this))->where('auditable_id', $this->id)->where('event', 'updated')->count();
    }
    public static function updateApproveBudget($idBudget, $status){

        $budget= self::findOrFail($idBudget);
        if ($budget->status == EBudgetStatus::NEW || $budget->status == EBudgetStatus::WAIT || $budget->status == EBudgetStatus::DENY){
            $budget->status = $status;
            $budget->save();
        }else{
            if ($status ==  EBudgetStatus::DONE){
                $data = $budget -> request_gw_data;
                $decode = json_decode($data);

                if ($budget->allocated==1){
                    $money_plan =$decode->money_plan;
                    $data_money = explode(" ", $money_plan );
                    foreach ($data_money as $key => $value) {
                        BudgetHasExpensePlan::where('budget_id', $budget->id)->where('month_budget', $key+ 1)->update([
                            'money_plan' => Community::getAmount($value),
                        ]);
                    }
                }
//                else{
//                    $budget->update([
//                        'total_budget' => Community::getAmount($decode->total_budget),
//                        'request_gw_status' => EBudgetStatus::DONE,
//                        'status' => EBudgetStatus::DONE,
//                        'version' => $budget->version + 1,
//                    ]);
//                }
                $budget->total_budget = Community::getAmount($decode->total_budget);
                $budget->request_gw_status = EBudgetStatus::DONE;
                $budget->version =  $budget->version + 1;
                $budget->status = EBudgetStatus::DONE;

            }elseif($status ==  EBudgetStatus::DENY){
                $budget->request_gw_status = EBudgetStatus::DENY;
                $budget->status = $status;
            }else{
                $budget->request_gw_status = EBudgetStatus::WAIT_CHANGE;
                $budget->status = EBudgetStatus::WAIT_CHANGE;
            }
            $budget->save();
        }
    }

    public function admin() {
        return $this->belongsTo(User::class, 'admin_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::updated(function(Budget $budget) {
            $action = '';
            if($budget->status == EBudgetStatus::DENY) {
                $action = 'reject';
            } elseif($budget->status == EBudgetStatus::DONE) {
                $action = 'approve';
            }

            if(empty($action)) {
                return;
            }
            NotificationHandler::sendNotification($budget->admin, new WorkResultNotification($budget, $action));
        });
    }
}
