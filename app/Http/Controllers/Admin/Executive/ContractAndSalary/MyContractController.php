<?php

namespace App\Http\Controllers\Admin\Executive\ContractAndSalary;

use App\Http\Controllers\Controller;
use App\Models\LaborContract;
use DB;

class MyContractController extends Controller {
    public function index() {
        return view('admin.executive.contract-and-salary.my-contract.index');
    }
}
