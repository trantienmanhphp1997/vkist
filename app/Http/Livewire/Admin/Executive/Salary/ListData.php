<?php

namespace App\Http\Livewire\Admin\Executive\Salary;

use App\Enums\ESystemConfigType;
use App\Models\SystemConfig;
use Livewire\Component;

use App\Models\SalaryBasic;
use App\Models\SalaryPosition;
use App\Models\SalarySheetsFile;

use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use App\Models\UserInf;
use App\Enums\EWorkingStatus;
use Illuminate\Support\Facades\Config;
use App\Component\FileUpload;
use App\Models\File;
use App\Exports\ErrorFileExport;
use App\Enums\ESalarySheetsFileType;
use Excel;
use App\Enums\EMonth;
use App\Http\Livewire\Base\BaseLive;

class ListData extends BaseLive
{
    use WithFileUploads;

    public $file;
    public $month;
    public $name;
    public $deleteId;

    public $fileErrorMessage = [];
    public $fileErrorExport = [];
    public $fileSuccessMessage = [];
    public $numItem = 0;
    public $numItemSaved = 0;
    public $showResult = false;
    public $disableSave = false;
    public $disableInput = false;
    public $numItemData = 0;
    public $type;
    public $model_name;
    public $folder;
    public $typeSalary;
    public $checkCreatePermission;
    public $checkDestroyPermission;
    public $year;

    public $searchTerm;
    public $monthFilter;
    public $yearFilter;
    public $fromMonth;
    public $toMonth;
    public $searchFromMonth;
    public $searchToMonth;

    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->fromMonth = [
            EMonth::JANUARY => __('common.month.jan'),
            EMonth::FEBRUARY => __('common.month.feb'),
            EMonth::MARCH => __('common.month.mar'),
            EMonth::APRIL => __('common.month.apr'),
            EMonth::MAY => __('common.month.may'),
            EMonth::JUNE => __('common.month.jun'),
            EMonth::JULY => __('common.month.jul'),
            EMonth::AUGUST => __('common.month.aug'),
            EMonth::SEPTEMBER => __('common.month.sep'),
            EMonth::OCTOBER => __('common.month.oct'),
            EMonth::NOVEMBER => __('common.month.nov'),
            EMonth::DECEMBER => __('common.month.dec'),
        ];
        $this->toMonth = [
            EMonth::JANUARY => __('common.month.jan'),
            EMonth::FEBRUARY => __('common.month.feb'),
            EMonth::MARCH => __('common.month.mar'),
            EMonth::APRIL => __('common.month.apr'),
            EMonth::MAY => __('common.month.may'),
            EMonth::JUNE => __('common.month.jun'),
            EMonth::JULY => __('common.month.jul'),
            EMonth::AUGUST => __('common.month.aug'),
            EMonth::SEPTEMBER => __('common.month.sep'),
            EMonth::OCTOBER => __('common.month.oct'),
            EMonth::NOVEMBER => __('common.month.nov'),
            EMonth::DECEMBER => __('common.month.dec'),
        ];
    }

    public function render()
    {
        $this->type = Config::get('common.type_upload.ImportSalarySheets');
        $this->model_name = SalarySheetsFile::class;
        $this->folder = app($this->model_name)->getTable();
        if (!empty($this->file)) {
            $this->validateFile();
        }

        $timeSheetsList = SalarySheetsFile::query();
        if (strlen($this->searchTerm)) {
            $timeSheetsList->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        if(!empty($this->searchFromMonth)){
            $timeSheetsList->where('month_working','>=',$this->searchFromMonth);
        }
        if(!empty($this->searchToMonth)){
            $timeSheetsList->where('month_working','<=',$this->searchToMonth);
        }
        if(!empty($this->searchFromMonth) && !empty($this->searchToMonth)){
            $timeSheetsList->where('month_working','>=',$this->searchFromMonth);
            $timeSheetsList->Where('month_working','<=',$this->searchToMonth);
        }
        if (!empty($this->yearFilter)) {
            $timeSheetsList->where('year_working', $this->yearFilter);
        }
        if(!empty($this->searchFromMonth) && !empty($this->searchToMonth) && !empty($this->yearFilter)){
            $timeSheetsList->where('month_working','>=',$this->searchFromMonth);
            $timeSheetsList->where('month_working','<=',$this->searchToMonth);
            $timeSheetsList->where('year_working', $this->yearFilter);
        }
        $timeSheetsList = $timeSheetsList->OrderBy('id', 'desc')->paginate($this->perPage);
        $systemConfig = SystemConfig::query()
            ->where('model_name', SalaryBasic::class)
            ->where('type', ESystemConfigType::TYPE_IMPORT_SALARY)->first();

        $dateImportBasicSalary = $dateImportPositionSalary = null;
        $checkBasicSalary = false;
        $checkPositionSalary = false;
        if (!empty($systemConfig) && !empty($systemConfig->content)) {
            $basicSalary = $systemConfig->content['date_import_basic_salary']['value'];
            $positionSalary = $systemConfig->content['date_import_position_salary']['value'];

            if (!empty($basicSalary)) {
                $basicSalary = str_replace(' ', '', $basicSalary);
                $basicSalary = trim($basicSalary, ',');
                $dateImportBasicSalary = explode(',', $basicSalary);

                $dateNow = date('d');
                if (!empty($dateImportBasicSalary)) {
                    foreach ($dateImportBasicSalary as $date) {
                        if ((int)$date == (int)$dateNow) {
                            $checkBasicSalary = true;
                            break;
                        }
                    }
                }
            }
            if (!empty($positionSalary)) {
                $positionSalary = str_replace(' ', '', $positionSalary);
                $positionSalary = trim($positionSalary, ',');
                $dateImportPositionSalary = explode(',', $positionSalary);

                $dateNow = date('d');

                if (!empty($dateImportPositionSalary)) {
                    foreach ($dateImportPositionSalary as $date) {
                        if ((int)$date == (int)$dateNow) {
                            $checkPositionSalary = true;
                            break;
                        }
                    }
                }
            }
        }
        if (!$checkBasicSalary && !$checkPositionSalary) {
            $this->disableSave = true;
        }
        $monthDataFilter = SalarySheetsFile::select('month_working')->orderBy('month_working', 'asc')->get()->unique('month_working');
        $yearDataFilter = SalarySheetsFile::select('year_working')->orderBy('year_working', 'asc')->get()->unique('year_working');
        return view('livewire.admin.executive.salary.list-data', [
            'data' => $timeSheetsList,
            'checkBasicSalary' => $checkBasicSalary,
            'checkPositionSalary' => $checkPositionSalary,
            'monthDataFilter' => $monthDataFilter,
            'yearDataFilter' => $yearDataFilter
        ]);
    }
    public function resetData()
    {
        $this->file = null;
        $this->month = null;
        $this->year = null;
        $this->name = null;
        $this->fileErrorMessage = [];
        $this->fileSuccessMessage = [];
        $this->fileErrorExport = [];
        $this->showResult = false;
        $this->numItem = 0;
        $this->numItemSaved = 0;
        $this->numItemData = 0;
    }
    public function validateFile()
    {
        if (
            $this->file->getMimeType() != 'application/vnd.ms-excel' &&
            $this->file->getMimeType() != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
            $this->file->getMimeType() != 'application/vnd.ms-excel.sheet.macroEnabled.12'
        ) {
            array_push($this->fileErrorMessage, [
                'error' => __('server_validation.import-time-sheets.file-excel2')
            ]);
            $this->file = null;
            return false;
        }
    }
    public function validateData()
    {
        $this->validate([
            'file' => ['required'],
            'name' => ['required', 'max:48', 'regex:/[^.!@#$%^&*(){}\[\]\'\"\/\|]+$/'],
            'month' => ['required'],
            'year' => ['required'],
            'typeSalary' => ['required'],
        ], [], [
            'file' => __('data_field_name.import-time-sheets.file'),
            'name' => __('data_field_name.import-time-sheets.name'),
            'month' => __('data_field_name.import-salary.month_placeholder'),
            'year' => __('data_field_name.import-salary.year_placeholder'),
            'typeSalary' => __('executive/import-salary.table_column.type')
        ]);
    }
    public function saveData()
    {
        $this->validateData();
        $this->fileErrorMessage = [];
        $this->importTimeSheets($this->file);
    }
    public function storeFile($id_model)
    {
        $fileUpload = new FileUpload();
        $dataUpload = $fileUpload->uploadFile($this->file, $this->folder, $this->model_name);
        $file_upload = new File();
        $file_upload->url = $dataUpload['url'];
        $file_upload->size_file = $dataUpload['size_file'];
        $file_upload->file_name = $dataUpload['file_name'];
        $file_upload->model_name = $this->model_name;
        $file_upload->model_id = $id_model;

        $file_upload->type = $this->type;
        $file_upload->save();
    }
    public function importTimeSheets($file)
    {
        return DB::transaction(function () use ($file) {
            $this->disableSave = true;

            if ($file->getMimeType() == 'application/vnd.ms-excel') {
                $excelReader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            } else {
                $excelReader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }
            $spreadsheet = $excelReader->load($file->getRealPath());
            $worksheet = $spreadsheet->getSheet(0);
            $rowIterator = $worksheet->getRowIterator();
            if ($this->typeSalary == ESalarySheetsFileType::BASIC_SALARY && (count($worksheet->getColumnDimensions()) <= 29 || count($worksheet->getColumnDimensions()) >= 33)) {
                array_push($this->fileErrorMessage, [
                    'error' => __('server_validation.import-time-sheets.file-excel2')
                ]);
                $this->disableSave = false;
                return false;
            }
            if ($this->typeSalary == ESalarySheetsFileType::POSITION_SALARY && (count($worksheet->getColumnDimensions()) <= 16 || count($worksheet->getColumnDimensions()) >= 24)) {
                array_push($this->fileErrorMessage, [
                    'error' => __('server_validation.import-time-sheets.file-excel2')
                ]);
                $this->disableSave = false;
                return false;
            }
            SalarySheetsFile::where('year_working',  now()->year)->where('month_working', $this->month)->where('type', $this->typeSalary)->OrderBy('id', 'desc')->first();
            $salarySheets = new SalarySheetsFile();
            $salarySheets->month_working = $this->month;
            $salarySheets->year_working = $this->year;
            $salarySheets->name = $this->name;
            $salarySheets->type = $this->typeSalary;
            $salarySheets->save();

            $this->storeFile($salarySheets->id);

            $isRowEmpty = function (\PhpOffice\PhpSpreadsheet\Worksheet\Row $row) {
                $is_row_empty = true;
                foreach ($row->getCellIterator() as $index => $cell) {
                    if ($cell->getValue() !== null && $cell->getValue() !== "" && $index != "A") {
                        $is_row_empty = false;
                        break;
                    }
                }
                return $is_row_empty;
            };

            while ($rowIterator->valid()) {
                $this->numItem++;
                $arrRow = $this->typeSalary == ESalarySheetsFileType::BASIC_SALARY ? [1, 2, 3, 4, 5, 6] : [1, 2, 3, 4, 5, 6, 7];
                if (!in_array($rowIterator->current()->getRowIndex(), $arrRow)) {
                    if ($isRowEmpty($rowIterator->current())) {
                        break;
                    }
                    $this->numItemData++;
                    $data = [];
                    $cellIterator = $rowIterator->current()->getCellIterator();

                    for ($i = 0; $i < count($worksheet->getColumnDimensions()); $i++) {
                        $cellValue = trim((string)$cellIterator->current()->getFormattedValue());
                        $validRow = $this->typeSalary == ESalarySheetsFileType::BASIC_SALARY ? $cellIterator->getCurrentColumnIndex() > 1 && $cellIterator->getCurrentColumnIndex() <= 29 : $cellIterator->getCurrentColumnIndex() > 1 && $cellIterator->getCurrentColumnIndex() <= 19;

                        if ($validRow) {
                            array_push($data, $cellValue);
                            if ($cellValue == null && $cellValue == "") {
                                $data = [];
                                break;
                            }
                            if ($cellIterator->getCurrentColumnIndex() == 2) {
                                if (empty($cellValue)) {
                                    array_push($this->fileErrorExport, [
                                        'error' => 'Nhân viên ' . $cellValue . ' không tồn tại. Dòng số ' . $this->numItem
                                    ]);
                                }
                                $userInf = UserInf::where('code', str_replace(',', '', $cellValue))->first();
                                if (empty($userInf)) {
                                    $data = [];
                                    array_push($this->fileErrorExport, [
                                        'error' => 'Nhân viên ' . $cellValue . ' không tồn tại. Dòng số ' . $this->numItem
                                    ]);
                                    break;
                                }
                            }
                        }
                        $cellIterator->next();
                    }
                    if (!empty(count($data))) {
                        if ($this->typeSalary == ESalarySheetsFileType::BASIC_SALARY) {
                            $allowance = [
                                'position_allowance' => str_replace(',', '', $data[5]),
                                'responsibility_allowance' => str_replace(',', '', $data[6]),
                            ];
                            $social_insurance = [
                                'service1' => str_replace(',', '', $data[7]),
                                'service2' => str_replace(',', '', $data[8]),
                                'laborer' => str_replace(',', '', $data[9]),
                            ];
                            $health_insurance = [
                                'service' => str_replace(',', '', $data[10]),
                                'laborer' => str_replace(',', '', $data[11]),
                            ];
                            $accident_insurance = [
                                'service' => str_replace(',', '', $data[12]),
                                'laborer' => str_replace(',', '', $data[13]),
                            ];
                            $salaryBasic = new SalaryBasic();
                            $salaryBasic->user_info_id = $userInf->id;
                            $salaryBasic->admin_id = auth()->id();
                            $salaryBasic->salary_sheets_file_id = $salarySheets->id;
                            $salaryBasic->actual_workday = $data[2];
                            $salaryBasic->coefficients_salary = $data[3];
                            $salaryBasic->salary = str_replace(',', '', $data[4]);
                            $salaryBasic->allowance = json_encode($allowance);
                            $salaryBasic->social_insurance = json_encode($social_insurance);
                            $salaryBasic->health_insurance = json_encode($health_insurance);
                            $salaryBasic->accident_insurance = json_encode($accident_insurance);
                            $salaryBasic->salary_allowance_insurance = str_replace(',', '', $data[14]);
                            $salaryBasic->salary_special = str_replace(',', '', $data[15]);
                            $salaryBasic->lunch_allowance = str_replace(',', '', $data[16]);
                            $salaryBasic->other_costs = str_replace(',', '', $data[17]);
                            $salaryBasic->arrears = str_replace(',', '', $data[18]);
                            $salaryBasic->pursuit_salary = str_replace(',', '', $data[19]);
                            $salaryBasic->bonus = str_replace(',', '', $data[20]);
                            $salaryBasic->amount_still_received = str_replace(',', '', $data[21]);
                            $salaryBasic->reduce_yourself = str_replace(',', '', $data[22]);
                            $salaryBasic->number_of_dependents = str_replace(',', '', $data[23]);
                            $salaryBasic->taxable_income = str_replace(',', '', $data[24]);
                            $salaryBasic->personal_income_tax = str_replace(',', '', $data[25]);
                            $salaryBasic->total_salary_received = str_replace(',', '', $data[26]);
                            $salaryBasic->note = $data[27];
                            $salaryBasic->save();
                        } else {
                            $base_salary_coefficient = [
                                'coefficients_salary' => str_replace(',', '', $data[3]),
                                'money' => str_replace(',', '', $data[4]),
                            ];
                            $collect_by_position = [
                                'coefficient' => str_replace(',', '', $data[6]),
                                'money' => str_replace(',', '', $data[7]),
                                'pursuit' => str_replace(',', '', $data[8]),
                            ];
                            $pay = [
                                'r&d' => str_replace(',', '', $data[9]),
                                'oda' => str_replace(',', '', $data[10]),
                            ];
                            $special_allowance = [
                                'money' => str_replace(',', '', $data[11]),
                                'pursuit' => str_replace(',', '', $data[12]),
                            ];
                            $salaryPosition = new SalaryPosition();
                            $salaryPosition->admin_id = auth()->id();
                            $salaryPosition->user_info_id = $userInf->id;
                            $salaryPosition->salary_sheets_file_id = $salarySheets->id;
                            $salaryPosition->base_salary_coefficient = json_encode($base_salary_coefficient);
                            $salaryPosition->actual_workday = $data[5];
                            $salaryPosition->collect_by_position = json_encode($collect_by_position);
                            $salaryPosition->pay = json_encode($pay);
                            $salaryPosition->special_allowance = json_encode($special_allowance);
                            $salaryPosition->arrears = str_replace(',', '', $data[13]);
                            $salaryPosition->pursuing_tax_finalization = str_replace(',', '', $data[14]);
                            $salaryPosition->provisional_tax = str_replace(',', '', $data[15]);
                            $salaryPosition->actually_received = str_replace(',', '', $data[16]);
                            $salaryPosition->note = $data[17];
                            $salaryPosition->save();
                        }
                        $this->numItemSaved++;
                    }
                }
                $rowIterator->next();
            }
            $this->fileSuccessMessage = __('server_validation.import-time-sheets.msg.success', ['value' => $this->numItemSaved]);
            $this->file = null;
            $this->showResult = true;
            $this->disableSave = false;
            $this->emit('closeModalImport');
        });
    }
    public function deleteFile()
    {
        $this->file = null;
        $this->fileErrorMessage = [];
        $this->fileErrorExport = [];
    }
    public function deleteId($id)
    {
        $this->deleteId = $id;
    }
    public function delete()
    {
        return DB::transaction(function () {
            $salarySheetsFile = SalarySheetsFile::where('id', $this->deleteId)->first();
            if (!empty($salarySheetsFile)) {
                $salaryBasic = SalaryBasic::where('salary_sheets_file_id', $this->deleteId)->get();
                foreach ($salaryBasic as $item) {
                    SalaryBasic::where('salary_sheets_file_id', $this->deleteId)->delete();
                    $item->delete();
                }

                $salaryPosition = SalaryPosition::where('salary_sheets_file_id', $this->deleteId)->get();
                foreach ($salaryPosition as $item) {
                    SalaryBasic::where('salary_sheets_file_id', $this->deleteId)->delete();
                    $item->delete();
                }
                $salarySheetsFile->delete();
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
            } else {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.delete')]);
            }
            
        });
    }
    public function downloadErrorFile()
    {
        return Excel::download(new ErrorFileExport($this->fileErrorExport, 'salary'), 'error-import-salary' . now()->format(Config::get('common.year-month-day')) . '-' . '.xlsx');
    }
}
