@extends('layouts.master')
<title>{{__('decision-reward/decision-reward.title.edit')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.decision-reward.decision-reward-create-and-update', ['decision_reward' => $data])
@endsection