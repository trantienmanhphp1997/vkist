<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskWorkHasResultPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_work_has_result_plan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 500)->nullable()->comment('Tên sản phẩm');
            $table->foreignId('task_work_detail_id')->nullable()->constrained('task_work_detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_work_has_result_plan');
    }
}
