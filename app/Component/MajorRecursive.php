<?php

namespace App\Component;

class MajorRecursive extends Recursive
{
    public function itemPattern($item, $level) {
        return [
            'id' => $item['id'],
            'name' => $item['v_value'],
            'padLeft' => str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level)
        ];
    }
}
