<?php
return [
    'breadcrumbs' => [
        'activity-management' => 'Activity management',
    ],
    'list' => 'List of labor contracts',
    'filter' => [
        'select-box' => [
            'option' => [
                'status' => 'Status',
            ],
        ],
    ],
    'status' => [
        'active' => 'Working',
        'off' => 'Stop working',
        'deleted' => 'Deleted',
    ],
    'table_column' => [
        'user_name' => 'Full Name',
        'code' => 'Contract number',
        'position' => 'Title',
        'contract_type' => 'Contract type',
        'duration' => 'Term of contract',
        'start_date' => 'Start date',
        'end_date' => 'End date',
        'status' => 'Status',
        'action' => 'Action',
        'coefficients_salary' => 'Salary coefficient',
        'bank_account' => 'Bank account number',
        'bank_name' => 'Bank',
    ],
    'title' => [
        'create' => 'Create new ideas',
        'edit' => 'Edit',
        'detail' => 'Detailed idea'
    ],
];