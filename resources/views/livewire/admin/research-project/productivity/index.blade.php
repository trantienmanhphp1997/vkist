<div class="col-md-10 col-xl-11 box-detail box-user">
    <style>
        .select-status-project {
            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        .title-approve {
            margin: 0 !important;
        }
        .input-date-kendo{
            font-size: 13px !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a
                    href="{{ route('admin.research-project.information.index') }}">{{__('research/project.name')}}</a> \
                <span>{{__('research/productivity.title')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('research/productivity.title')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        {{--<div class="col-md-6">
                            <div class="form-group search-expertise">
                                <div class="search-expertise inline-block">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text"
                                           class="size13 form-control"
                                           placeholder="{{__('research/productivity.search')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>--}}
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="type_report" class="form-control size13" wire:model.lazy="type_report">
                                    <option value="1"> {{__('research/productivity.type.project_report')}} </option>
                                    <option value="2"> {{__('research/productivity.type.project_cost_report')}} </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control select2 size13" wire:model.lazy="implementing_agencies" id="select-topic-major">
                                    <option value="">{{__('research/productivity.implementing_agencies')}}</option>
                                    @foreach($department as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'start_date_filter','place_holder' => __('research/productivity.table.start-date'), 'set_null_when_enter_invalid_date' => true ])
                        </div>
                        <div class="col-md-3">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'end_date_filter' ,'place_holder' => __('research/productivity.table.end-date'), 'set_null_when_enter_invalid_date' => true ])
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="department_id" id="exampleFormControlInput4" class="form-control size13" wire:model.lazy="topicType">
                                    <option value="">-- {{__('research/productivity.topic')}} --</option>
                                    @foreach($researchCategoryLevel as $research)
                                        <option value="{{$research->id}}">{{ $research->v_value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control select2 size13" wire:model.lazy="field" id="select-topic-major">
                                    <option value="">{{__('research/productivity.table.category')}}</option>
                                    @foreach($researchCategory as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control select2 size13" wire:model.lazy="projectStatusId" id="select-topic-major">
                                    <option value="">{{__('research/productivity.period')}}</option>
                                    @foreach ($statusProjectList as $statusOption)
                                        <option value="{{ $statusOption['id'] }}" >{{ $statusOption['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <div class="form-group">
                                <div class=" float-right">
                                    @if(checkRoutePermission('download'))
                                        <button title="{{__('common.button.export_file')}}" type="button" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#exampleModal"> <img src="/images/filterdown.svg" alt="filterdown"> </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-md-3">
                            <div class="form-group">
                                <select name="department_id" id="exampleFormControlInput4" class="form-control size13" wire:model.lazy="projectScheduleStatus">
                                    <option value="">-- {{__('research/productivity.status')}} --</option>
                                    <option value="1"> {{__('research/productivity.beyond-process')}} </option>
                                    <option value="2"> {{__('research/productivity.on-time')}} </option>
                                    <option value="3"> {{__('research/productivity.expired')}} </option>
                                </select>
                            </div>
                        </div>--}}
                    </div>
                    <div class="row">
                        <div class="col-md-6" >
                            <div class="row bd-border " style="    margin: 0px;    margin-bottom: 25px;">
                                <div class="col-md-12 ">
                                    <h5 class="mb-3" style="border-bottom: 1px #F3F3F3;padding-bottom: 12px">     {{ __('data_field_name.topic.topic_group') }}</h5>
                                    <div class="row">
                                        <div class="col-md-3 col-xl-4 ">
                                            {{__('research/productivity.cadres')}}: <strong>  {{$countProjectInCadres}}</strong>
                                        </div>
                                        <div class="col-md-3 col-xl-4">
                                            {{__('research/productivity.base')}}: <strong>{{$countProjectInBaseLevel}}</strong>
                                        </div>
                                        <div class="col-md-3 col-xl-4">
                                            {{__('research/productivity.state')}}: <strong>{{$countProjectInStateLevel}}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" >
                            <div class="row bd-border " style="    margin: 0px;    margin-bottom: 25px;">
                                <div class="col-md-12 ">
                                    <h5 class="mb-3" style="border-bottom: 1px #F3F3F3;padding-bottom: 12px">{{__('research/productivity.table.current-period')}}</h5>
                                    <div class="row">
                                        <div class="col-md-3 col-xl-4">
                                            {{__('research/productivity.beyond-process')}}: <strong>{{$countProjectBeyondTime}}</strong>
                                        </div>
                                        <div class="col-md-3 col-xl-4">
                                            {{__('research/productivity.on-time')}}:  <strong>{{$countProjectOnTime}}</strong>
                                        </div>
                                        <div class="col-md-3 col-xl-4">
                                            {{__('research/productivity.expired')}}: <strong>{{$countProjectExpired}}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div wire:loading class="loader" style="z-index: 10"></div>
                    @if($type_report == 1)
                        <div class="table-responsive">
                            <table class="table table-general">
                                <thead>
                                <tr class="border-radius text-center">
                                    <th scope="col" class="border-radius-left">
                                        {{__('research/productivity.table.stt')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.code')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('research/productivity.table.implementing_agencies')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('research/productivity.table.topic_name')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('research/productivity.table.topic_group')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('research/productivity.table.category')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.chairperson')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.start-date')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.end-date')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.period')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.work-delay')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.work-delay-for-disbursement')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.progress')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.different-day')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.kpi')}}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)
                                    <tr class="border-radius text-center">
                                        <td  class="border-radius-left">
                                            {{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}
                                        </td>
                                        <td >
                                            @if($row->topic->projectResearch->contract_code ?? '')
                                                <span style="color: blue; cursor:pointer;"
                                                      wire:click="getListTask('{{$row->topic->projectResearch->contract_code ?? ''}}')">
                                                {!! boldTextSearch($row->topic->projectResearch->project_code ?? __('research/taskwork.not-khown-project'), $searchTerm) !!}
                                                </span>
                                            @else
                                                <span style="color: blue; cursor:pointer;" >
                                                {!! boldTextSearch($row->topic->projectResearch->project_code ?? __('research/taskwork.not-khown-project'), $searchTerm) !!}
                                                </span>
                                            @endif

                                        </td>
                                        <td  class="text-center">
                                          {{$row->department->name ?? null}}
                                        </td>
                                        <td  class="text-center">
                                            {!! boldTextSearch(($row->name ?? 'Chưa xác định'), $searchTerm) !!}
                                        </td>
                                        <td class="text-center">
                                            @foreach($researchCategoryLevel as $research)
                                                @if(!empty($row->topic) && $research->id == $row->topic->level_id)
                                                    {{ $research->v_value}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td class="text-center">{!! boldTextSearch(($row->researchCategory->name ?? 'Chưa xác định'), $searchTerm) !!}</td>
                                        <td class="text-center">
                                            @if(!empty($row->topic))
                                                {{!empty($row->topic->leader) ? $row->topic->leader->fullname : null}}
                                            @endif
                                        </td>
                                        <td >
                                            {{reFormatDate($row->start_date ?? '')}}
                                        </td>
                                        <td >
                                            {{reFormatDate($row->end_date ?? '')}}
                                        </td>
                                        <td >
                                            {{
                                                  Form::select('status',
                                                  array(
                                                      0=> __('research/project.status.wait_approval') ,
                                                      1=>__('research/project.status.in_process'),
                                                      2=>__('research/project.status.completed'),
                                                      3=>__('research/project.status.cancel')
                                                      ),  $row->status,
                                                      ['class'=> ($row->status == 0 ? 'wait-approval' :
                                                      ($row->status == 1 ? 'in-process' :
                                                      ($row->status == 2 ? 'completed' : (
                                                          $row->status == 3 ?  'cancel' : ''
                                                      ))).'').' select-status-project',
                                                      'disabled'
                                                      ]
                                                  )}}
                                        </td>
                                        <td >
                                            {{$row->countTaskExpiredByFee ?? 0}}
                                        </td>
                                        <td >
                                            {{$row->countTaskExpired ?? 0}}
                                        </td>
                                        <td  class="text-center">
                                            {{round($row->progress, 2)}} %
                                        </td>
                                        <td >
                                            {{$row->different_date ?? 0}}
                                        </td>
                                        <td >
                                            <a href="#" wire:click="getKpiAnalysis({{$row->topic->projectResearch->id ?? ''}})" data-toggle="modal" data-target="#kpimodal">
                                                {{__('research/productivity.table.see-kpi')}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @include('admin.researchProject.productivity.kpiModal')
                        @if (count($data) > 0)
                            {{ $data->links() }}
                        @else
                            <div class="title-approve text-center">
                                <span>{{ __('common.message.empty_search') }}</span>
                            </div>
                        @endif
                    @else
                        <div class="table-responsive">
                            <table class="table table-general">
                                <thead>
                                <tr class="border-radius text-center">
                                    <th scope="col" class="border-radius-left">
                                        {{__('research/productivity.table.stt')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.code')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('research/productivity.table.implementing_agencies')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('research/productivity.table.topic_name')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('research/productivity.table.topic_group')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.category')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.chairperson')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.start-date')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.end-date')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.grant_budget')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.budget_used')}}
                                    </th>
                                    <th scope="col">
                                        {{__('research/productivity.table.budget_remaining')}}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)
                                    <tr class="border-radius text-center">
                                        <td  class="border-radius-left">
                                            {{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}
                                        </td>
                                        <td >
                                            @if($row->topic->projectResearch->contract_code ?? '')
                                                <span style="color: blue; cursor:pointer;"
                                                      wire:click="getListTask('{{$row->topic->projectResearch->contract_code ?? ''}}')">
                                                {!! boldTextSearch($row->topic->projectResearch->project_code ?? __('research/taskwork.not-khown-project'), $searchTerm) !!}
                                                </span>
                                            @else
                                                <span style="color: blue; cursor:pointer;" >
                                                {!! boldTextSearch($row->topic->projectResearch->project_code ?? __('research/taskwork.not-khown-project'), $searchTerm) !!}
                                                </span>
                                            @endif

                                        </td>
                                        <td >
                                            {{$row->department->name ?? null}}
                                        </td>
                                        <td >
                                            {!! boldTextSearch(($row->name ?? 'Chưa xác định'), $searchTerm) !!}
                                        </td>
                                        <td >
                                            @foreach($researchCategoryLevel as $research)
                                                @if(!empty($row->topic) && $research->id == $row->topic->level_id)
                                                    {{ $research->v_value}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td class="text-center">{!! boldTextSearch(($row->researchCategory->name ?? 'Chưa xác định'), $searchTerm) !!}</td>
                                        <td >
                                            @if(!empty($row->topic))
                                                {{!empty($row->topic->leader) ? $row->topic->leader->fullname : null}}
                                            @endif
                                        </td>
                                        <td >
                                           {{reFormatDate($row->start_date ?? '')}}
                                        </td>
                                        <td >
                                            {{reFormatDate($row->end_date ?? '')}}
                                        </td>
                                        <td >
                                            {{numberFormat($row->grant_budget, '.', '.')}}
                                        </td>
                                        <td >
                                            {{numberFormat($row->budget_used, '.', '.')}}
                                        </td>
                                        <td >
                                            {{numberFormat($row->grant_budget - $row->budget_used, '.', '.')}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @include('admin.researchProject.productivity.kpiModal')
                        @if (count($data) > 0)
                            {{ $data->links() }}
                        @else
                            <div class="title-approve text-center">
                                <span>{{ __('common.message.empty_search') }}</span>
                            </div>
                        @endif
                    @endif
                    @include('livewire.common._modalExportFile')
                </div>
            </div>
        </div>
    </div>
</div>

