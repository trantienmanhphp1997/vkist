<div class="col-md-10 col-xl-11 box-detail box-user">
            <div class="row">
                <div class="col-md-12">
                    <div  class="breadcrumbs"><a href="">{{__('menu_management.menu_name.infomation-management')}} </a> \ <span>{{__('executive/my-time-salary.salary-infomation')}}</span></div>
                </div>
            </div>
            <div class="row bd-border">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="title">{{__('executive/my-time-salary.salary-sheets', [
                            'month' => $filter_month_year
                        ])}}</h3>
                        </div>
                        <div class="col-md-6">
                            <div class=" float-right">
                                <form action="" class="d-flex">
                                    <div class="form-group field-fix w-160 h-44 m-0">
                                        <select class="w-100" wire:model.lazy="filter_month_year">
                                            @foreach($salarySheetsFile as $item)
                                                <option value="{{$item->month_working . '/' . $item->year_working}}">@if(($item->month_working)<10) {{0 . $item->month_working . '/' . $item->year_working}}@else{{$item->month_working . '/' . $item->year_working}}@endif</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn bg-primary btn-fix mr-0 px-24 py-12 ml-12" >{{__('common.button.view')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="information">
                        <div class="inner-tab pt0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group float-right">
                                        <button class="btn btn-fix bg-content px-10 py-10" wire:click="dowloadSalarySheetFile"><img src="/images/DownloadBlue.svg" alt="download"></button>
                                    </div>
                                </div>
                            </div>
                            <!-- Start staff information -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="w-100 body-4 text-2 text-uppercase mb-12">
                                        {{__('executive/my-time-sheets.personal-info.title')}}
                                    </div>
                                    <div class="bg-3 w-100 py-12 px-16 mb-32 bd-rds-12 info-box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered w-100 m-0">
                                                        <thead></thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="w-ratio-33 p-0 bd-r bd-b">
                                                                    <div class="d-flex py-8 pr-16 ">
                                                                        <p class="mb-0">{{__('executive/my-time-sheets.personal-info.name')}}</p>
                                                                        <p class="mb-0 ml-auto fw-600">{{!empty($user) ? $user->fullname : null}}</p>
                                                                    </div>
                                                                </td>
                                                                <td class="w-ratio-33 p-0 bd-b">
                                                                    <div class="d-flex py-8 px-16 ">
                                                                        <p class="mb-0">{{__('executive/my-time-sheets.personal-info.position')}}</p>
                                                                        <p class="mb-0 ml-auto fw-600">{{!empty($user->position) ? $user->position->v_value : null}}</p>
                                                                    </div>
                                                                </td>
                                                                <td class="w-ratio-33 p-0 bd-l bd-b">
                                                                    <div class="d-flex py-8 px-16 ">
                                                                        <p class="mb-0">{{__('executive/my-time-sheets.personal-info.code')}}</p>
                                                                        <p class="mb-0 ml-auto fw-600">{{!empty($user) ? $user->code : null}}</p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w-ratio-33 p-0 bd-r bd-t">
                                                                    <div class="d-flex py-8 pr-16 ">
                                                                        <p class="mb-0">{{__('executive/my-time-sheets.personal-info.base_salary_coefficient')}}</p>
                                                                        <p class="mb-0 ml-auto fw-600">{{$salaryBasic['coefficients_salary']}}</p>
                                                                    </div>
                                                                </td>
                                                                <td class="w-ratio-33 p-0 bd-t">
                                                                    <div class="d-flex py-8 px-16 ">
                                                                        <p class="mb-0">{{__('executive/my-time-sheets.personal-info.bank_account')}}</p>
                                                                        <p class="mb-0 ml-auto fw-600">{{!empty($contract) ? $contract->bank_account_number : null}}</p>
                                                                    </div>
                                                                </td>
                                                                <td class="w-ratio-33 p-0 bd-l bd-t">
                                                                    <div class="d-flex py-8 px-16 ">
                                                                        <p class="mb-0">{{__('executive/my-time-sheets.personal-info.bank_name')}}</p>
                                                                        <p class="mb-0 ml-auto fw-600">{{!empty($contract) ? $contract->bankName->v_value : null}}</p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End staff information -->

                            <!-- Start timekeeping information -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="w-100 body-4 text-2 text-uppercase mb-12">
                                        {{__('executive/my-time-sheets.timekeeping-info.title')}}
                                    </div>
                                    <div class="bg-3 w-100 py-12 px-16 mb-32 bd-rds-12 info-box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered w-100 m-0">
                                                        <thead></thead>
                                                        <tbody>
                                                            <tr>
                                                        <td class="w-ratio-33 p-0 bd-r bd-b">
                                                            <div class="d-flex py-8 pr-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.period')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{$startOfMonth->format(Config::get('common.formatDate'))}}<span class="font-weight-normal"> {{__('common.to')}} </span>{{$endOfMonth->format(Config::get('common.formatDate'))}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-b">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.number-of-paid-leave-days')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? $monthly->leave_day + $monthly->holiday : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-l bd-b">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.number-of-days-of-unpaid-leave')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? $monthly->unpaid_day : null}}</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="w-ratio-33 p-0 bd-r bd-t">
                                                            <div class="d-flex py-8 pr-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.standard-work-by-month')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? $monthly->leave_day + $monthly->holiday + $monthly->total_work_day : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-t">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.actual-number-of-working-days')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? (int)$monthly->total_work_day : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-l bd-t">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.total-number-of-paid-working-days')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? $monthly->leave_day + $monthly->holiday + $monthly->total_work_day : null}}</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End timekeeping information -->

                            <!-- Start Timekeeping table -->
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-general text-2 total-amount-table">
                                        <!-- Cham cong tuan -->
                                        <thead>
                                            <tr class="border-radius">
                                                <th  scope="col" class="w50 hl-tb border-radius-left text-left">{{__('executive/my-time-salary.index')}}</th>
                                                <th  scope="col" class="text-left">{{__('executive/my-time-salary.incomes')}}</th>
                                                <th  scope="col" class="border-radius-right text-right">{{__('executive/my-time-salary.detail')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody class="border-bottom">
                                            <tr>
                                                <td class="hl-tb text-left">1</td>
                                                <td class="text-left">{{__('executive/my-time-salary.basic-salary')}}</td>
                                                <td class="text-right">{{$salaryBasic['salary']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">2</td>
                                                <td class="text-left">{{__('executive/my-time-salary.position-salary')}}</td>
                                                <td class="text-right">{{$salaryPosition['salary']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">3</td>
                                                <td class="text-left">{{__('executive/my-time-salary.allowance')}}</td>
                                                <td class="text-right">
                                                    {{$salaryBasic['position_allowance'] + $salaryBasic['responsibility_allowance'] + $salaryBasic['other_costs'] + $salaryPosition['incentive_allowance'] + $salaryBasic['lunch_allowance']}}
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">3.1</td>
                                                <td class="text-left">{{__('executive/my-time-salary.position_allowance')}}</td>
                                                <td class="text-right">{{$salaryBasic['position_allowance']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">3.2</td>
                                                <td class="text-left">{{__('executive/my-time-salary.responsibility_allowance')}}</td>
                                                <td class="text-right">{{$salaryBasic['responsibility_allowance']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">3.3</td>
                                                <td class="text-left">{{__('executive/my-time-salary.work_allowance')}}</td>
                                                <td class="text-right">{{$salaryBasic['other_costs']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">3.4</td>
                                                <td class="text-left">{{__('executive/my-time-salary.incentive_allowance')}}</td>
                                                <td class="text-right">{{$salaryPosition['incentive_allowance']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">3.5</td>
                                                <td class="text-left">{{__('executive/my-time-salary.lunch_allowance')}}</td>
                                                <td class="text-right">{{$salaryBasic['lunch_allowance']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">4</td>
                                                <td class="text-left">{{__('executive/my-time-salary.other')}}</td>
                                                <td class="text-right">{{$salaryBasic['pursuit_salary'] + $salaryPosition['pursuit']}}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">{{__('executive/my-time-salary.total')}}</td>
                                                <td class="text-right font-bold">{{$incomes}}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-general text-2 total-amount-table">
                                        <!-- Cham cong tuan -->
                                        <thead>
                                            <tr class="border-radius">
                                                <th  scope="col" class="w50 hl-tb border-radius-left text-left">{{__('executive/my-time-salary.index')}}</th>
                                                <th  scope="col" class="text-left">{{__('executive/my-time-salary.deductions')}}</th>
                                                <th  scope="col" class="border-radius-right text-right">{{__('executive/my-time-salary.detail')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody class="border-bottom">
                                            <tr>
                                                <td class="hl-tb text-left">1</td>
                                                <td class="text-left">{{__('executive/my-time-salary.compulsory_insurance')}}</td>
                                                <td class="text-right">{{$salaryBasic['insurance']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">2</td>
                                                <td class="text-left">{{__('executive/my-time-salary.personal_income_tax')}}</td>
                                                <td class="text-right">{{$salaryBasic['personal_income_tax'] + $salaryPosition['provisional_tax']}}</td>   
                                            </tr>
                                            <tr>
                                                <td class="hl-tb text-left">3</td>
                                                <td class="text-left">{{__('executive/my-time-salary.other2')}}</td>
                                                <td class="text-right">{{$salaryBasic['arrears'] + $salaryPosition['arrears']}}</td>   
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">{{__('executive/my-time-salary.total')}}</td>
                                                <td class="text-right font-bold">{{$deductions}}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <table class="table table-general text-2 total-amount-table2 ">
                                        <tr>
                                            <th scope="col" class="text-left border-radius-left">{{__('executive/my-time-salary.total_amount_received')}}</th>
                                            <th scope="col" class="text-right font-bold border-radius-right ">{{$incomes - $deductions}}</th> 
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- End Timekeeping table -->
                        </div>
                    </div>
                </div>
            </div>
        </div>