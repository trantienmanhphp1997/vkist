<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use App\Enums\ETypeUpload;
use App\Models\File;
use App\Models\Topic;
use Livewire\Component;

class TabPresentation extends Component
{

    protected $listeners = [
        'validate-data-event' => 'validateData',
        'changed-root-topic-event' => 'setRootTopic',
    ];

    public $model = Topic::class;
    public $filePresentation;

    public ?Topic $topic;

    public function setRootTopic($topicId)
    {
        $rootTopic = Topic::query()->find($topicId);
        $this->topic->present_note = $rootTopic->present_note ?? '';
    }

    protected function rules()
    {
        return [
            'topic.present_note' => 'nullable|max:1000'
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'topic.present_note' => __('data_field_name.topic.present_infomation')
        ];
    }

    public function mount(Topic $topic = null)
    {
        $this->topic = ($topic != null) ? $topic : new Topic();
        $this->filePresentation = ETypeUpload::TOPIC_PRESENTATION;
    }

    public function validateData() {
        try {
            $this->validate();
            $this->emitUp('validated-data-event', 'tab-presentation', $this->topic->getDirty());
        } catch (\Exception $exception) {
            $this->emitUp('invalid-data-event', 'tab-presentation', __('data_field_name.topic.error_tab_presentation'));
            throw $exception;
        }
    }

    public function render()
    {
        return view('livewire.admin.research.topic.tab-presentation');
    }
}
