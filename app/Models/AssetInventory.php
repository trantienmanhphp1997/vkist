<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetInventory extends BaseModel
{
    use HasFactory;
    use SoftDeletes;
    protected $table='asset_inventory';
    protected $fillable=['name','department_id','inventory_date','status','asset_category_id', 'admin_id','unsign_text', 'period_id'];
    protected $guarded=['*'];
    private static $searchable = [
        'name',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
    public function user(){
        return $this->belongsToMany(UserInf::class,'asset_inventory_has_user_info','asset_inventory_id','user_info_id');
    }

    public function assets(){
        return $this->belongsToMany(Asset::class,'inventory_details','inventory_id','asset_id')->withPivot(['actual_quantity', 'actual_situation']);
    }

    public function category(){
        return $this->belongsToMany(AssetCategory::class,'inventory_category','inventory_id','category_id');
    }
    public function department(){
        return $this->belongsTo(Department::class,'department_id');
    }
}
