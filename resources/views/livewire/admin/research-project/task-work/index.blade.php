<div class="col-md-10 col-xl-11 box-detail box-user">
    <style>
        .select-status-project {
            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }
        .group-task{
            border: 1px solid #FFEFEB !important;
            color: #FF5630 !important;
        }
        .personal-task{
            border: 1px solid #F5FCF9 !important;
            color: #38CB89 !important;
        }
        .title-approve{
            margin: 0 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a
                    href="{{ route('admin.research-project.information.index') }}">{{__('research/project.name')}}</a> \
                <span>{{__('research/project.information.breadcrumbs')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-6">
            <h3 class="title">{{__('research/taskwork.index')}}</h3>
        </div>
        @if($checkCreatePermission)
            <div class="col-md-6 ">
                <div class="form-group float-right">
                    <a href="{{ route('admin.research-project.task-work.create',$project_code_id ?? ' ') }}"
                       class="btn btn-viewmore-news mr0 ">
                        <img src="/images/plus2.svg" alt="plus">{{__('research/taskwork.create')}}
                    </a>
                </div>
            </div>
        @endif
        <div class="col-md-12">
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row  box-filter">
                        <div class="col-md-10 px-8 d-flex filter-left">
                            <div class="form-group search-expertise pr-8">
                                <div class="search-expertise inline-block">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control"
                                           placeholder="{{__('research/project.information.search')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                            <div class="w-160 px-8">
                                    <div class="form-group ">
                                        <select  class="form-control select2 size13"
                                               wire:model="status_id">
                                            <option value="">{{__('research/taskwork.status')}}</option>
                                            @foreach($statusDataList as $status_id => $status)
                                                <option value="{{$status_id}}" style="height: 40px;">{{$status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="px-8 w-160">
                                    <div class="form-group ">
                                        <select  class="form-control select2 size13" wire:model="level_id">
                                            <option value="">{{__('research/taskwork.level')}}</option>
                                            @foreach($levelDataList as $level_id => $level)
                                                <option value="{{$level_id}}" style="height: 40px;">{{$level}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="w-160 px-8">
                                    <div class="form-group ">
                                        <select class="form-control select2 size13" wire:model.lazy="field">
                                            <option value="">{{__('research/productivity.table.category')}}</option>
                                            @foreach($researchCategory as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="w-160 px-8">
                                    <div class="form-group">
                                        <select class="form-control select2 size13" wire:model.lazy="leader_id">
                                            <option value="">{{__('research/productivity.table.chairperson')}}</option>
                                            @foreach($leaderList as $item)
                                                <option value="{{$item->id}}">{{$item->fullname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-2 px-8">
                            <div class="row">
                                <div class="col-md-12 px-8">
                                    <div class="form-group float-right" >
                                        <label for="is-get-personal-task"
                                               class="btn completed  personal-task d-inline"
                                               style="margin-right: 10px ;padding: 16px 20px;font-size: 13px ; border-radius: 14px ; @if($isGetPersonalTask)     background: aliceblue; @endif">
                                            {{__('research/taskwork.personal-task')}}
                                        </label>
                                        <input type="checkbox" id="is-get-personal-task"  style="visibility: hidden;position: absolute;" wire:model="isGetPersonalTask">
                                    </div>

{{--                                    <div class="form-group float-right" >--}}
{{--                                        <label for="is-get-team-task"--}}
{{--                                           class="btn cancel group-task d-inline"--}}
{{--                                           style="margin-right: 10px ;padding: 16px 20px ;font-size: 13px ; border-radius: 14px ; @if($isGetTeamTask)     background: aliceblue; @endif">--}}
{{--                                            {{__('research/taskwork.group-task')}}--}}
{{--                                        </label>--}}
{{--                                        <input type="checkbox" id="is-get-team-task" wire:click="getTeamTask" style="visibility: hidden;position: absolute;" wire:model="isGetTeamTask">--}}
{{--                                    </div>--}}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div wire:loading class="loader" style="z-index: 10"></div>
                    <div class="table-responsive">
                        <table class="table table-general working-plan">
                            <thead>
                            <tr class="border-radius text-center">
                                <th scope="col">
                                    {{__('research/taskwork.table.project-code')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('research/taskwork.table.work-type')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('research/taskwork.table.task-code')}}
                                </th>
                                <th scope="col">
                                    {{__('research/taskwork.table.priority-level')}}
                                </th>
                                <th scope="col">
                                    {{__('research/taskwork.table.name')}}
                                </th>
                                <th scope="col">
                                    <div class="ool-md-12">
                                        {{__('research/taskwork.table.plan')}}
                                        <div class="row">
                                            <div class="col-md-6 w-140"
                                                style="font-size: 9px ;font-weight: 500 ; border-right: 1px solid ">
                                                {{__('research/taskwork.table.start')}}
                                            </div>
                                            <div class="col-md-6 w-140" style="font-size: 9px ;font-weight: 500 ">
                                                {{__('research/taskwork.table.end')}}
                                            </div>
                                        </div>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="ool-md-12">
                                        {{__('research/taskwork.table.reality')}}
                                        <div class="row">
                                            <div class="col-md-6 w-140"
                                                style="font-size: 9px ;font-weight: 500 ; border-right: 1px solid ">
                                                {{__('research/taskwork.table.start')}}
                                            </div>
                                            <div class="col-md-6 w-140 " style="font-size: 9px ;font-weight: 500 ">
                                                {{__('research/taskwork.table.end')}}
                                            </div>
                                        </div>
                                    </div>

                                </th>
                                <th scope="col">
                                    {{__('research/taskwork.table.status')}}
                                </th>
                                <th scope="col">
                                    {{__('research/taskwork.table.assign')}}
                                </th>
                                <th scope="col">
                                    {{__('research/taskwork.table.preview')}}
                                </th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>

                            @foreach ($data as $row)
                                <tr>
                                    <td class="text-center">
                                       @if($row->topic->projectResearch->id ?? '')
                                            <a
                                                href="{{ route('admin.research-project.information.edit', $row->topic->projectResearch->id ?? '') }}">
                                                {!! boldTextSearch($row->topic->projectResearch->project_code ?? __('research/taskwork.not-khown-project'), $searchTerm) !!}
                                            </a>
                                        @else
                                            <a
                                                href="#">
                                                {!! boldTextSearch($row->topic->projectResearch->project_code ?? __('research/taskwork.not-khown-project'), $searchTerm) !!}
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{$row->work_type == 0 ? __('research/taskwork.select-work-type.main')
                                        : ($row->work_type == 1 ? __('research/taskwork.select-work-type.sub')
                                        : __('research/taskwork.select-work-type.detail'))}}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.research-project.task-work.edit', $row->id) }}">
                                            {!! boldTextSearch($row->code, $searchTerm) !!}
                                        </a>
                                    </td>
                                    <td class="text-center"
                                        style=" color:{{$row->priority_level == 0 ? '#377DFF': ($row->priority_level == 1 ? '#38CB89': ($row->priority_level == 2 ? '#FFAB00' : '#FF5630'))}}">
                                        {{$row->priority_level == 0 ? __('research/taskwork.select-level.low')
                                        : ($row->priority_level == 1 ? __('research/taskwork.select-level.medium')
                                        : ($row->priority_level == 2 ? __('research/taskwork.select-level.high')
                                        : __('research/taskwork.select-level.emergency')))}}
                                    </td>
                                    <td class="text-center">{!! boldTextSearch(( $row->name ?? 'Chưa xác định'), $searchTerm) !!}</td>
                                    <td class="text-center">
                                        <div class="ool-md-12">
                                            <div class="row">
                                                <div class="col-md-6" style="border-right: 1px solid ">
                                                    {{reFormatDate($row->start_date ?? '')}}
                                                </div>
                                                <div class="col-md-6">
                                                    {{reFormatDate($row->end_date ?? '')}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="ool-md-12">
                                            <div class="row">
                                                <div class="col-md-6" style="border-right: 1px solid ">
                                                    {{reFormatDate($row->real_start_date ?? '')}}
                                                </div>
                                                <div class="col-md-6">
                                                    {{reFormatDate($row->real_end_date?? '')}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        {{
                                            Form::select('status',
                                            array(
                                                0=>  __('research/taskwork.select-status.undo'),
                                                1=> __('research/taskwork.select-status.on-working'),
                                                3=> __('research/taskwork.select-status.done')
                                                ),  $row->status,
                                                ['class'=> ($row->status == 0 ? 'wait-approval' :
                                                ($row->status == 1 ? 'in-process' :
                                                ($row->status == 3 ? 'completed' : '')).'').' select-status-project text-center',
                                                'disabled'
                                                ]
                                            )}}
                                    </td>
                                    <td class="text-center">
                                        {{$row->assigner->name ?? ''}}
                                    </td>
                                    <td class="text-center">
                                        {{$row->reviewer->fullname ?? ''}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
