<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserResearchLicense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_research_license', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('linh vuc , de tai, loai hinh nghien cuu');
            $table->string('name', 500)->comment('ten bang sang che');
            $table->string('license', 255)->nullable()->comment('So bang');
            $table->string('number_license', 255)->nullable()->comment('So don');
            $table->string('author_name', 255)->nullable()->comment('Ten chu van bang');
            $table->integer('date_created')->comment('Nam cong bo');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('map với user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
