<?php

namespace App\Exports;

use App\Models\AssetCategory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class AssetReportSumaryDepartment implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $data;

    public function __construct($data)
    {
        $this->data[0]['index']=1 ;
        $this->data[0]['title']=__('research/report.report4-page.table.in-use') ;
        $this->data[0]['count']=$data['report_lost']['count'];
        $this->data[0]['department_name']=$data['report_lost']['department_name'];
        $this->data[1]['index']=2 ;
        $this->data[1]['title']=__('research/report.report4-page.table.report-cancel');
        $this->data[1]['count']=$data['report_cancel']['count'];
        $this->data[1]['department_name']=$data['report_cancel']['department_name'];
        $this->data[2]['index']=3 ;
        $this->data[2]['title']=__('research/report.report4-page.table.liquidation');
        $this->data[2]['count']=$data['report_liquidation']['count'];
        $this->data[2]['department_name']=$data['report_liquidation']['department_name'];
        $this->data[3]['index']=4 ;
        $this->data[3]['title']=__('research/report.report4-page.table.liquidation');
        $this->data[3]['count']=$data['report_liquidation']['count'];
        $this->data[3]['department_name']=$data['report_liquidation']['department_name'];
        //
        $this->data[4]['index']=5 ;
        $this->data[4]['title']=__('research/report.report4-page.table.report-lost');
        $this->data[4]['count']=$data['report_asset_has_lost']['count'];
        $this->data[4]['department_name']=$data['report_asset_has_lost']['department_name'];
        //
        $this->data[5]['index']=6 ;
        $this->data[5]['title']=__('research/report.report4-page.table.report-cancel');
        $this->data[5]['count']=$data['report_asset_has_cancel']['count'];
        $this->data[5]['department_name']=$data['report_asset_has_cancel']['department_name'];
        //
        $this->data[6]['index']=7 ;
        $this->data[6]['title']=__('research/report.report4-page.table.report-liquidation');
        $this->data[6]['count']=$data['report_asset_has_liquidation']['count'];
        $this->data[6]['department_name']=$data['report_asset_has_liquidation']['department_name'];
        //
        $this->data[7]['index']=8 ;
        $this->data[7]['title']=__('research/report.report4-page.table.wait-repaired');
        $this->data[7]['count']=$data['report_asset_wait_repaired']['count'];
        $this->data[7]['department_name']=$data['report_asset_wait_repaired']['department_name'];
        $this->data[8]['index']=9 ;
        $this->data[8]['title']=__('research/report.report4-page.table.wait-maintenance');
        $this->data[8]['count']=$data['report_asset_wait_maintenance']['count'];
        $this->data[8]['department_name']=$data['report_asset_wait_maintenance']['department_name'];
        $this->data[9]['index']=10 ;
        $this->data[9]['title']=__('research/report.report4-page.table.in-repaired');
        $this->data[9]['count']=$data['report_asset_has_repaired']['count'];
        $this->data[9]['department_name']=$data['report_asset_has_repaired']['department_name'];
        //
        $this->data[10]['index']=11 ;
        $this->data[10]['title']=__('research/report.report4-page.table.in-maintenance');
        $this->data[10]['count']=$data['report_asset_has_maintenance']['count'];
        $this->data[10]['department_name']=$data['report_asset_has_maintenance']['department_name'];

        $this->data[11]['index']=12 ;
        $this->data[11]['title']=__('research/report.report4-page.table.not-use');
        $this->data[11]['count']=$data['report_asset_not_use']['count'];
        $this->data[11]['department_name']=$data['report_asset_not_use']['department_name'];

        $this->data[12]['index']='' ;
        $this->data[12]['title']=__('research/report.report2-page.total');
        $this->data[12]['count']=$data['countAssetAmount'];
        $this->data[12]['department_name']=$data['count_department'];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            __('research/report.table.stt'),
            __('research/report.report4-page.table.status'),
            __('research/report.report4-page.table.department'),
            __('research/report.report4-page.total'),
        ];
    }

    public function map($data): array
    {
        return [
            $data['index'] ?? 0,
            $data['title'],
            $data['department_name'],
            $data['count']
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);


            $arrayAlphabet = [
                'A', 'B', 'C', 'D'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
                $event->sheet->getColumnDimension($alphabet)->setWidth(50);
            }
            $event->sheet->mergeCells('A1:K1');
            $event->sheet->setCellValue('A1', 'BÁO CÁO TỔNG HỢP TÀI SẢN');
            $event->sheet->getStyle('A1:H1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:D3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);
        },];

    }

    public function title(): string
    {
        return __('research/report.report4-page.title');
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
