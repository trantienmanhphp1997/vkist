@extends('layouts.master')
@section('title')
    <title>       {{__('user/group_user.title.view')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    <livewire:admin.user.group-user.detail :id="$id"/>
@endsection
@section('js')
@endsection
