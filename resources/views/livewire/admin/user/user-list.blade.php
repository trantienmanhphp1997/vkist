<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="breadcrumbs"><span>{{ __('menu_management.menu_name.user_info_list') }}</span></div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">
                {{ __('menu_management.menu_name.user_info_list') }}<br>
                <span>
                    {{ __('common.message.showing_entries', [
    'from' => ($data->currentPage() - 1) * $data->perPage() + 1,
    'to' => ($data->currentPage() - 1) * $data->perPage() + $data->perPage() <= $data->total() ? ($data->currentPage() - 1) * $data->perPage() + $data->perPage() : $data->total(),
    'all' => $data->total(),
]) }}
                </span>
            </h3>
            <div class="information">
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="search-expertise">
                            <input type="text" placeholder="{{ __('data_field_name.common_field.search') }}"
                                   name="search" class="form-control size13" wire:model.debounce.1000ms="searchTerm"
                                   id='input_vn_name' autocomplete="off" maxlength="48">
                            <span><img src="/images/Search.svg" alt="search"/></span>
                        </div>

                    </div>
                    <div class="col-md-6 text-right">
                        <button type="button" id='btnOpenExportModal' title="{{__('common.button.export_file')}}"
                                style="border-radius: 11px; border:none;" data-toggle="modal"
                                data-target="#exampleModal">
                            <img src="/images/filterdown.svg" alt="filterdown">
                        </button>
                        <button type="button" title="{{__('common.button.export_file')}}"
                                style="border-radius: 11px; border:none;">
                            <img src="/images/filter3gach.png" alt="filter3gach" id="search_more_btn">
                        </button>
                        @if($checkCreatePermission)
                            <button type="button" title="{{__('common.button.create')}}"
                                    style="border-radius: 11px; border:none;"
                                    onclick="window.location='{{ route('admin.user.create') }}'">
                                <img src="/images/filteradd.png" alt="filteradd">
                            </button>
                        @endif
                    </div>
                </div>

                <div class="row mb-3 bg-light py-3" style="border-radius: 12px;">
                    <div class="col-md-3">
                        <input class="form-control size13" type="text" onfocus="(this.type='date')"
                               placeholder="{{ __('data_field_name.common_field.time_join') }}"
                               wire:model.debounce.1000ms="searchDate" autocomplete="off">
                    </div>
                    <div class="col-md-3">
                        <select class="form-control size13" wire:model.lazy="searchDepartment">
                            <option value=""> {{ __('data_field_name.common_field.department') }} </option>
                            @foreach ($departmentList as $department)
                                <option
                                    value="{{ $department['id'] }}">{!! $department['padLeft'] !!} {{ $department['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control size13" wire:model.lazy="searchPosition">
                            <option value=""> {{ __('data_field_name.common_field.position') }} </option>
                            @foreach ($positionList as $position)
                                <option value="{{ $position->id }}">{{ $position->v_value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 size13">
                        <select class="form-control size13" wire:model.lazy="searchProject">
                            <option value=""> {{ __('data_field_name.common_field.project_join') }} </option>
                            @foreach ($projectList as $project)
                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="table-responsive">
                    <table class="table table-general" id='dataTable'>
                        <thead>
                        <tr class="border-radius">

                            <th class="text-center" scope="col">{{ __('data_field_name.common_field.stt') }}</th>
                            <th class="text-center" scope="col">{{ __('data_field_name.common_field.name') }}</th>
                            <th class="text-center" scope="col">{{ __('data_field_name.common_field.department') }}</th>
                            <th class="text-center" scope="col">{{ __('data_field_name.common_field.position') }}</th>
                            <th class="text-center"
                                scope="col">{{ __('data_field_name.common_field.type_contract') }}</th>
                            <th class="text-center" scope="col">{{ __('data_field_name.common_field.status') }}</th>
                            <th class="text-center" scope="col">{{ __('data_field_name.common_field.situation') }}</th>
                            <th class="text-center" scope="col" class="border-radius-right">
                                {{ __('data_field_name.common_field.action') }}</th>

                        </tr>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>
                        @forelse($data as $row)

                            <tr>
                                <td class="text-center">{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                <td class="text-center"><a
                                        href="{{ route('admin.user.show', ['id' => $row->id]) }}">{{ $row->fullname }}</a>
                                </td>
                                <td class="text-center">{{ $row->department->name ?? null }}</td>
                                <td class="text-center">{{ $row->position->v_value ?? null }}</td>
                                <td class="text-center">{{ \App\Enums\EContractType::valueToName($row->contract->contract_type??'')}}</td>
                                <td class="text-center"><span
                                        class="rounded bg-light text-primary p-2">{{ __('status.member.active') }}</span>
                                </td>
                                <td class="text-center">
                                    @if(count($row->leave)>0)
                                        @foreach($row->leave as $item)
                                                {{$item->content}}
                                        @endforeach
                                    @else
                                        {{ __('data_field_name.common_field.working')}}
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($checkEditPermission)
                                        <a title="{{__('common.button.edit')}}"
                                           href="{{ route('admin.user.edit', $row->id) }}">
                                            <img src="/images/edit.png" alt="edit">
                                        </a>
                                    @endif &nbsp &nbsp &nbsp
                                    @if($checkDestroyPermission)
                                        <button title="{{__('common.button.delete')}}" type="button"
                                                style="background-color: white; border:none;"
                                                wire:click="deleteIdUser({{ $row->id }})"
                                                data-target="#exampleDeleteUser" data-toggle="modal">
                                            <img src="/images/trash.svg" alt="trash">
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr class="text-center text-danger">
                                <td colspan="7">{{ __('common.message.no_data') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}
                @include('livewire.common._modalExportFile')
                <div wire:ignore.self class="modal fade" id="exampleDeleteUser" tabindex="-1"
                     aria-labelledby="exampleModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body box-user">
                                <h4 class="modal-title">{{ __('common.confirm_message.confirm_title') }}</h4>
                                <p>{{ __('common.confirm_message.are_you_sure_delete') }}</p>
                            </div>
                            <div class="group-btn2 text-center  pt-24">
                                <button type="button" class="btn btn-cancel"
                                        data-dismiss="modal">{{ __('common.button.no') }}</button>
                                <button type="button" wire:click.prevent="delete()" class="btn btn-save"
                                        data-dismiss="modal">{{ __('common.button.yes') }}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('livewire.admin.user._modalDeleteSelected')


    <div class="modal fade" id="exportSelected" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h5>
                </div>
                <div class="modal-body">
                    Bạn có chắc chắn muốn xuất file các bản ghi đã chọn không?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id='btnExportSelected' download>
                        Đồng ý
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#selecctall").change(function () {
            $(".checkbox1").prop('checked', $(this).prop("checked"));
        });
    });
    $("#deleteBtn").on('click', function (e) {
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('#checkAll');
        var count = $checkedBoxes.length;
        if (count) {
            $('#deleteBtn').attr("data-target", "#deleteSelectModal");
            var checkbox = document.getElementsByName('row_id');
            var checked = [];
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked === true) {
                    checked.push(checkbox[i].value);
                }
            }
            window.livewire.emit('getIdSelected', checked);
        } else {
            $('#deleteBtn').attr("data-target", "");
            toastr.warning('You haven&#039;t selected anything');
        }
    });


    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('#checkAll');
    });


    $(".checkboxRow").change(function () {
        var $uncheckedBoxes = $('#dataTable input[type=checkbox]:not(:checked)').not('#checkAll');
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('#checkAll');
        if ($uncheckedBoxes.length != 0) {
            $("#checkAll").prop('checked', false);
        } else {
            $("#checkAll").prop('checked', true);

        }
    });

    $("#btnOpenExportModal").on('click', function (e) {
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('#checkAll');
        var count = $checkedBoxes.length;

        var checkbox = document.getElementsByName('row_id');
        var checked = [];
        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked === true) {
                checked.push(checkbox[i].value);
            }
        }
        window.livewire.emit('getIdSelected', checked);
    });

    $('#deleteSelectModal').on('hidden.bs.modal', function () {
        window.livewire.emit('resetSelected');
    });
</script>
