<?php
return [
    'index' => 'STT',
    'salary-infomation' => 'Thông tin lương',
    'salary-sheets' => 'Phiếu lương tháng :month',
    'incomes' => 'CÁC KHOẢN THU NHẬP',
    'detail' => 'THÔNG TIN CHI TIẾT',
    'basic-salary' => 'Lương cơ bản',
    'position-salary' => 'Lương chức danh',
    'allowance' => 'Phụ cấp',
    'position_allowance' => 'Phụ cấp chức vụ',
    'responsibility_allowance' => 'Phụ cấp trách nhiệm',
    'work_allowance' => 'Phụ cấp công tác',
    'incentive_allowance' => 'Phụ cấp khuyến khích',
    'lunch_allowance' => 'Phụ cấp ăn trưa',
    'other' => 'Khác (truy lĩnh)',
    'total' => 'Tổng cộng',
    'total_amount_received' => 'Tổng số tiền thực lĩnh',
    'deductions' => 'CÁC KHOẢN TRỪ VÀO LƯƠNG',
    'compulsory_insurance' => 'Bảo hiểm bắt buộc (10.5%)',
    'personal_income_tax' => 'Thuế TNCN (tạm thu)',
    'other2' => 'khác (truy thu)'
];