<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ContractDuration extends BaseModel
{
    use HasFactory;
    protected $table = "contract_duration";
    protected $guarded=['*'];
    protected $fillable = [];

}
