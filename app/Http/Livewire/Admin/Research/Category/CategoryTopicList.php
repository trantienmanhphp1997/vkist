<?php

namespace App\Http\Livewire\Admin\Research\Category;

use App\Exports\UsersExport;
use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchCategory;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use App\Enums\EResearchCategoryType;

class CategoryTopicList extends BaseLive
{
	public $searchTerm;
    public $deleteId='';
    public $updateMode = false;

    //data for update modal
    public $categoryTopicId;
    public $code;
    public $name;
    public $description;
    public $note;
    public $type = EResearchCategoryType::TOPIC;

    public function render()
    {
    	$query = ResearchCategory::query();
        $searchTerm = $this->searchTerm;

        $query->where('type', EResearchCategoryType::TOPIC);

    	if (!empty($searchTerm)) {
            $query->where('research_category.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')->get();
        }

        $data=$query->orderBy('id','DESC')->paginate($this->perPage);
        return view('livewire.admin.research.category.category-topic-list',['data'=>$data]);

    }

    public function getCategoryInfoForUpdateModal($id){
        $this->resetValidation();
        $this->updateMode = true;
        $categoryTopic = ResearchCategory::findOrFail($id);
        $this->categoryTopicId = $id;
        $this->code = $categoryTopic->code;
        $this->name = $categoryTopic->name;
        $this->description = $categoryTopic->description;
        $this->note = $categoryTopic->note;
    }
    public function resetCategoryInfo() {
        $this->reset(['updateMode', 'categoryTopicId','name','description','note', 'code']);
    }

    public function delete(){
        parent::delete();
        $categoryTopic = ResearchCategory::findOrFail($this->deleteId);
        $categoryTopic->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }
    public function store() {
        if($this->updateMode) {
            parent::edit($this->categoryTopicId);
        } else {
            parent::store();
        }
        $code_rules = ['required','max:7', 'regex: /^[A-Z0-9]*$/','unique:research_category,code'];
        if($this->categoryTopicId) {
            $code_rules = ['required','max:7', 'regex: /^[A-Z0-9]*$/','unique:research_category,code,'.$this->categoryTopicId];
        }
        $this->validate([
            'code' => $code_rules,
            'name' => 'required|max:100',
            'note' => ['nullable','max:255','regex:/^[a-zA-Z0-9'.config('common.special_character.alphabet').'\.\,\-\(\)\/ ]+$/'],
            'description' => ['nullable','max:255','regex:/^[a-zA-Z0-9'.config('common.special_character.alphabet').'\.\,\-\(\)\/ ]+$/'],
        ],[],[
            'code'=> __('research/category-topic.validate.code'),
            'name'=> __('research/category-topic.validate.name'),
            'note'=> __('research/category-topic.validate.note'),
            'description'=> __('research/category-topic.validate.description'),
        ]);
        if($this->categoryTopicId) {
            $categoryTopic = ResearchCategory::findOrFail($this->categoryTopicId);
            $isUpdate = true;
        } else {
            $categoryTopic = new ResearchCategory;
            $isUpdate = false;
        }
        $categoryTopic->code = $this->code;
        $categoryTopic->name = $this->name;
        $categoryTopic->note = $this->note;
        $categoryTopic->description = $this->description;
        $categoryTopic->type = $this->type;

        $categoryTopic->save();

        $this->resetCategoryInfo();
        $this->emit('close-modal-create-or-edit-category-topic');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $isUpdate ? __('research/category-topic.notification.edit-success') : __('research/category-topic.notification.create-success')] );

    }

}
