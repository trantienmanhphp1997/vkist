<?php
return [
    'breadcrumbs' => [
        'decision-reward-management' => 'Reward and Discipline management',
        'decision-reward-list' => 'List of Decision on Reward',
    ],
    'form' => [
        'general_info' => 'General info',
        'document' => 'Document',
        'receiver_list' => 'List of receiver',
        'attach_file' => 'Attach file',
    ],
    'table_column' => [
        'name' => 'Decision on Reward ',
        'number_decision' => 'Number of decision',
        'type_of_decision' => 'Type of decision',
        'decision_date' => 'Decision date',
        'type_of_prize' => 'Type of prize',
        'type_reward' => 'Type of Reward',
        'receiver' => 'Receiver',
        'action' => 'Action'
    ],
    'receiver_table_column' => [
        'name' => 'Name',
        'code' => 'Code',
        'department' => 'Department',
        'position' => 'Position',
        'prize_value' => 'Prize value',
        'reason' => 'Reason',
        'action' => 'Action',
    ],
    'decision-reward-prize-type' => [
        'money' => 'Money',
        'glorify' => 'Glorify',
        'product' => 'Product'
    ],
    'decision-reward-receiver-type' => [
        'organization' => 'Organization',
        'individual' => 'Individual',
    ],
    'decision-reward-decision-type' => [
        'reward' => 'Reward',
        'discipline' => 'Discipline',
    ],

    'label' => [
        'name' => "Decision on Reward",
        'reward_date' => "Reward date",
        'number_decision' => "Number of decision",
        'user_decision' => 'Decision maker',
        'type_of_receiver' => 'Type of receiver',
        'reason' => 'Reason',
        'type_reward' => 'Type of Reward',
        'type_of_decision' => 'Type of decision',
        'decision_date' => 'Decision date',
        'type_of_prize' => 'Type of prize',
        'user_decision_position' => 'Position of decision maker',
        'total_value' => 'Total value',
        'base' => 'Base'
    ],
    'placeholder' => [
        'search' => 'Search',
    ],

    'title' => [
        'create' => 'Create Decision On The Reward Scheme',
        'edit' => 'Edit Decision Reward',
        'create-receiver' => 'Create receiver',
        'no-result' => 'No result!',
        'part-1' => 'Dispaly',
        'part-2' => 'decision in',
    ],
    'empty-record' => 'No result',
    
    'modal' => [
        'title' => [
            'delete' => 'Xác nhận xoá',
            'create' => 'Thêm loại đề tài',
            'edit' => 'Sửa loại đề tài',
        ],
        'body' => [
            'delete-warning' => 'Bạn có xóa không? Thao tác này không thể phục hồi!',
            'name' => 'Tên loại đề tài',
            'description' => 'Mô tả',
            'note' => 'Ghi chú',
        ],
        'footer' => [
            'back' => 'Quay lại',
            'delete' => 'Xoá',
            'close' => 'Đóng',
            'save' => 'Lưu',
        ]
    ],
    'validate' => [
        'name' => 'Tên loại đề tài',
        'note' => 'Ghi chú loại đề tài',
        'description' => 'Mô tả loại đề tài',
        'total_value_max_length' => ':attribute should not be more than 10 digits'
    ],
    'notification' => [
        'edit-success' => 'Edit successful',
        'create-success' => 'Data is added successfully',
        'add-new-receiver-success' => 'New successfully receiver added',
        'add-new-receiver-fail' => 'Fail, receiver existed',
    ]
];
