<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ResearchCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_category', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('linh vuc , de tai, loai hinh nghien cuu');
            $table->string('name', 500)->comment('Ten linh vuc');
            $table->string('description', 1000)->nullable()->comment('Tên hội đồng');
            $table->string('note', 1000)->nullable()->comment('Ghi chu');
            $table->tinyInteger('type')->comment('1 : loai de tai, 2: loai hinh nghien cuu 3: linh vuc nghin cuu');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_category');
    }
}
