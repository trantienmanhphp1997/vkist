<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryPositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_position', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('nhân viên');
            $table->foreignId('salary_sheets_file_id')->nullable()->constrained('salary_sheets_file')->comment('bảng lương');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->json('base_salary_coefficient')->nullable()->comment('{coefficients_salary: (E4), money: thanh tien (f5)}');
            $table->bigInteger('actual_workday')->nullable()->comment('ngay cong thuc te (G6)');
            $table->json('collect_by_position')->nullable()->comment('{coefficient: (H7), money: thanh tien (I8), pursuit: truy linh (J9)}');
            $table->json('pay')->nullable()->comment('{r&d: (K10), oda: (L11)}');
            $table->json('special_allowance')->nullable()->comment('{money: (M12), pursuit: (N13)}');
            $table->bigInteger('arrears')->nullable()->comment('truy thu (O14)');
            $table->bigInteger('pursuing_tax_finalization')->nullable()->comment('truy linh thue quyet toan (P15)');
            $table->bigInteger('provisional_tax')->nullable()->comment('thue tam tinh (Q16)');
            $table->bigInteger('actually_received')->nullable()->comment('thuc linh (R17)');
            $table->string('note', 1000)->nullable()->comment('ghi chu (S18)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_position');
    }
}
