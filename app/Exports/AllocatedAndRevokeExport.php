<?php

namespace App\Exports;

use App\Models\UserInf;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
class AllocatedAndRevokeExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($searchTerm) {
        $this->searchTerm = $searchTerm;
    }

    public function collection()
    {
        $query = UserInf::query()
            ->select('user_info.unsign_text', 'user_info.id', 'user_info.code', 'user_info.fullname', 'user_info.phone', 'user_info.email', 'user_info.gender', DB::raw('SUM(amount_used) as total'))
            ->join('asset', 'asset.user_info_id', 'user_info.id')
            ->groupBy('user_info.unsign_text', 'user_info.id', 'user_info.code', 'user_info.fullname', 'user_info.phone', 'user_info.email', 'user_info.gender', 'user_info_id');
        if (strlen($this->searchTerm)) {
            $query->where('user_info.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        $data = $query->orderBy('user_info.id','DESC')->get();
        $data = $data->map(function ($item) {
            $user = UserInf::where('id', $item->id)->first();
            $position = !empty($user->position) ? $user->position->v_value : null;
            $department = !empty($user->department) ? $user->department->name : null;
            return [
                'code' => $item->code,
                'fullname' => $item->fullname,
                'gender' => $item->gender == 0 ? 'Nam' : 'Nữ',
                'phone' => $item->phone,
                'email' => $item->email,
                'position' => $position,
                'department' => $department,
                'total' => $item->total,
            ];
        });
        return $data;
    }
    public function headings():array
    {
        return
        [
            'Mã nhân viên',
            'Tên nhân viên',
            'Giới tính',
            'Số điện thoại',
            'Email',
            'Vị trí công tác',
            'Đơn vị',
            'Tài sản sử dụng'
        ];
    }

    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('D1:G1');
            $event->sheet->setCellValue('D1','DANH SÁCH CẤP PHÁT THU HỒI');
            $event->sheet->getStyle('D1:G1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:U3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        return 'DANH SÁCH CẤP PHÁT THU HỒI';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
