<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class TimeSheetsDaily extends Eloquent
{
    use HasFactory;
    protected $table = 'timesheets_daily';
    protected $fillable = [];
}
