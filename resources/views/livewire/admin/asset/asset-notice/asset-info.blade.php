<div class="info-item">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="d-flex item-tỉtle">
                    <p>{{__('data_field_name.asset.asset_info_')}}</p>
                    <span class="ml-auto"><img src="/images/Angle-down.svg" alt="angle-down"></span>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-6">
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.asset_code')}}</p>
                            <p class="content font13">: {{$assetDetail->code ?? ''}}</p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.asset_name')}}</p>
                            <p class="content font13">: {{$assetDetail->name ?? ''}}</p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.asset_cate')}}</p>
                            <p class="content font13">: {{$assetDetail->category->name ?? ''}}</p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.used_quantity')}}</p>
                            <p class="content font13">: {{$assetDetail->amount_used ?? '0'}}</p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.asset_quantity')}}</p>
                            <p class="content font13">: {{$assetDetail->quantity ?? '0'}}</p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.unit_type')}}</p>
                            <p class="content font13">: {{$assetDetail->unit_type ?? ''}}</p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.buy_date')}}</p>
                            <p class="content font13">:
                                {{reFormatDate($assetDetail->buy_date, 'd/m/Y')??''}}
                            </p>
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.warranty_expiry_date')}}</p>
                            <p class="content font13">:
                                {{reFormatDate($assetDetail->expiry_warranty_date, 'd/m/Y')?? ''}}
                            </p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.asset_value')}}</p>
                            <p class="content">: {{number_format($assetDetail->asset_value) ?? ''}}</p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.situation')}}</p>
                            <p class="content font13">:
                                @if($assetDetail)
                                    @if($assetDetail->situation == \App\Enums\EAssetSituation::SITUATION_USING|$assetDetail->situation == \App\Enums\EAssetSituation::SITUATION_NOT_USE)
                                        {{$assetSituation[$assetDetail->situation] ?? ''}}
                                    @else
                                        {{$assetStatus[$assetDetail->situation] ?? ''}}
                                    @endif
                                @endif
                            </p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.used_date')}}</p>
                            <p class="content font13">:
                                 {{$assetDetail->allocate?reFormatDate($assetDetail->allocate->implementation_date, 'd/m/Y') : ''}}
                            </p>
                        </div>
                        <div class="item-field">
                            <p class="name font13">{{__('data_field_name.asset.user_')}}</p>
                            <p class="content font13">: {{$assetDetail->user->fullname ?? ''}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

