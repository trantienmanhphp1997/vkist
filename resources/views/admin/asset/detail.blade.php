@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.asset.detail')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    @livewire('admin.asset.asset-detail.tab-detail', ['assetDetail'=>$assetDetail])
@endsection
