<div>
    <table class="table table-bordered">
        <tr>
            <th scope="col">{{ __('data_field_name.topic.code') }}</th>
            <td>{{ $data->code ?? '' }}</td>
        </tr>
        <tr>
            <th scope="col" style="width:30%">{{ __('data_field_name.topic.status') }}</th>
            <td>{{ $topicStatus[$data->status] ?? '' }}</td>
        </tr>

        <tr>
            <th scope="col">{{ __('data_field_name.topic.date_hand') }}</th>
            <td>{{ reFormatDate($data->submit_date) }}</td>
        </tr>
        <tr>
            <th scope="col">{{ __('data_field_name.topic.code_profile') }}</th>
            <td>{{ $data->approval->first()->code ?? '' }}</td>
        </tr>
    </table>
    <div class="history">
        <h4>{{ __('data_field_name.topic.tab_history') }}</h4>
        @foreach ($history as $item)

            <?php
                $status = $item->new_values['status'] ?? 0;
                $actorName = $item->user->name ?? '';
                if(
                    $status == \App\Enums\ETopicStatus::STATUS_WAIT_APPROVAL
                    || $status == \App\Enums\ETopicStatus::STATUS_REJECT
                    || $status == \App\Enums\ETopicStatus::STATUS_APPROVAL
                ) {
                    $actorName = 'Groupware';
                }
            ?>

            <div class="bg-light rounded mb-3 p-3 row">
                <div class="col-md-6 text-primary">{{ date_format($item->created_at, "H:i d/m/Y") }}</div>
                <div class="col-md-6">
                    <span class="badge badge-success float-right">{{ \App\Enums\ETopicStatus::valueToName($status) }}</span>
                </div>
                <div class="col-md-12 text-muted"><strong>{{ $actorName }}</strong> {{ __('data_field_name.topic.updated_topic_status') }} </div>
            </div>
        @endforeach
    </div>
</div>
