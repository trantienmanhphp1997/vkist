<?php

namespace App\Http\Livewire\Admin\Research\Achievement;

use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use App\Models\ResearchAchievement;
use App\Models\ResearchCategory;
use App\Enums\EResearchCategoryType;
use App\Enums\EIdeaStatus;




class AchievementList extends BaseLive
{
    public $type;
    public $from_date;
    public $to_date;
    public $listCategoryField;
    public $filter_category_field_id;
    public $listFile = [];

    protected $listeners = [
        'set-from_date' => 'setFromDate',
        'set-to_date' => 'setToDate',
    ];

    public function setFromDate($data) {
        $this->from_date = $data['from_date'] ? date('Y-m-d', strtotime($data['from_date'])) :null;
    }
    public function setToDate($data) {
        $this->to_date = $data['to_date'] ? date('Y-m-d', strtotime($data['to_date'])) : null;
    }

    public function render()
    {

        $query = ResearchAchievement::query();
        $query->where('research_achievements.type', $this->type);
        $searchTerm = $this->searchTerm;
        if (!empty($searchTerm)) {
            $query->leftJoin('topic','research_achievements.topic_id','=','topic.id')
            ->where(function($query) use($searchTerm) {
                $query->where('research_achievements.unsign_text', 'like', '%' . strtolower(removeStringUtf8($searchTerm)) . '%')
                    ->orWhere('topic.unsign_text', 'like', '%' . strtolower(removeStringUtf8($searchTerm)) . '%');
            })->select('research_achievements.*');
        }
        if(!is_null($this->from_date)) {
            $query->where('publish_date', '>=', $this->from_date);
        }
        if(!is_null($this->to_date)) {
            $query->where('publish_date', '<=', $this->to_date);
        }
        if(!empty($this->filter_category_field_id)) {
            $query->leftJoin('topic', 'topic.id', '=', 'research_achievements.topic_id')->where('topic.research_category_id',$this->filter_category_field_id);
        }
        $data = $query->with(['topic.leader', 'topic.researchCategory'])->orderBy('research_achievements.id', 'desc')->paginate($this->perPage);
        return view('livewire.admin.research.achievement.achievement-list', ['data' => $data]);
    }

    public function mount() {
        $this->listCategoryField = ResearchCategory::query()->where('type', EResearchCategoryType::FIELD)
        ->whereExists(function ($query) {
            $query->from('ideal')
                  ->whereColumn('ideal.research_field_id', 'research_category.id')
                  ->where('ideal.status', EIdeaStatus::APPROVED);
        })->get();
    }

    public function delete()
    {
        parent::delete();
        $ressearch_achievement = ResearchAchievement::findOrFail($this->deleteId);
        $ressearch_achievement->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }
    public function getListFile($id) {
        $research_achievement = ResearchAchievement::where('id', $id)->with('files')->first();
        if(!is_null($research_achievement)) {
            $this->listFile = $research_achievement->files;
        }
        $this->dispatchBrowserEvent('openDownloadFileModal');
    }
    public function download($url, $name) {
        return response()->download('storage/'.$url, $name);
    }
}
