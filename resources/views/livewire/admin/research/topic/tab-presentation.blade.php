<div class="inner-tab">
    <div class="form-group">
        <label>{{ __('data_field_name.topic.present_infomation') }}</label>
        <textarea class="form-control" rows="4" wire:model.defer="topic.present_note"></textarea>
        @error('topic.present_note') <div class="text-danger">{{ $message }}</div> @enderror
    </div>
    <div class="form-group">
        @livewire('component.files', ['model_name' => $model, 'model_id' => $topic->id, 'type' => $filePresentation, 'folder' => 'topics'])
    </div>
</div>
