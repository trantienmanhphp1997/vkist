<?php
return [
    [
        'name' => 'Phòng quản lý tài sản',
        'code' => 'PQLTS',
        'note' => 'Phòng quản lý tài sản',
        'status' => 1,
    ],
    [
        'name' => 'Phòng kế toán',
        'code' => 'PKT',
        'note' => 'Note 1',
        'status' => 1,
    ],
    [
        'name' => 'Phòng hành chính',
        'code' => 'PHC',
        'note' => 'Note 2',
        'status' => 1,
    ],
    [
        'name' => 'Phòng kiểm toán',
        'code' => 'PKIT',
        'note' => 'Note 3',
        'status' => 1,
    ],
    [
        'name' => 'Phòng chăm sóc khách hàng',
        'code' => 'PCSKH',
        'note' => 'Note 4',
        'status' => 1,
    ],
    [
        'name' => 'Phòng nhân sự',
        'code' => 'PNS',
        'note' => 'Note 5',
        'status' => 1,
    ],
    [
        'name' => 'Phòng Công nghệ thông tin',
        'code' => 'PCNTT',
        'note' => 'Note 6',
        'status' => 1,
    ],
    [
        'name' => 'Phòng quan hệ quốc tế',
        'code' => 'PQHQT',
        'note' => 'Note 7',
        'status' => 1,
    ],
    [
        'name' => 'Phòng Marketing',
        'code' => 'PMKT',
        'note' => 'Note 8',
        'status' => 1,
    ],
    [
        'name' => 'Phòng nghiên cứu và phát triển sản phẩm',
        'code' => 'PRND',
        'note' => 'Note 9',
        'status' => 1,
    ],
    [
        'name' => 'Phòng kinh doanh',
        'code' => 'PKT',
        'note' => 'Note 10',
        'status' => 1,
    ],
    [
        'name' => 'Phòng thu mua',
        'code' => 'PTM',
        'note' => 'Note 11',
        'status' => 1,
    ],
];
