<?php

namespace App\Exports;

use App\Models\AssetCategory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class AssetReportSumary implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            __('research/report.table.stt'),
            __('research/report.report2-page.table.code'),
            __('research/report.report2-page.table.name'),
            __('research/report.report2-page.table.not-use'),
            __('research/report.report2-page.table.in-use'),
            __('research/report.report2-page.table.in-repaired'),
            __('research/report.report2-page.table.in-maintenance'),
            __('research/report.report2-page.table.report-lost'),
            __('research/report.report2-page.table.losted'),
            __('research/report.report2-page.table.cancel'),
            __('research/report.report2-page.table.report-liquidation'),
            __('research/report.report2-page.table.liquidation'),
            __('research/report.report2-page.table.transfer'),
            __('research/report.report2-page.table.recall'),
            __('research/report.report2-page.table.total')
        ];
    }

    public function map($asset): array
    {
        return [
            $asset->index,
            $asset->code,
            $asset->name,
            $asset->notUse,
            $asset->amount_used,
            $asset->inRepaired,
            $asset->inMaintenance,
            $asset->report_lost,
            $asset->losted,
            $asset->cancel,
            $asset->report_liquidation,
            $asset->liquidation,
            $asset->transfer,
            $asset->recall,
            $asset->amountStatus
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);


            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
                $event->sheet->getColumnDimension($alphabet)->setWidth(50);
            }
            $event->sheet->mergeCells('A1:K1');
            $event->sheet->setCellValue('A1', 'BÁO CÁO TỔNG HỢP TÀI SẢN');
            $event->sheet->getStyle('A1:H1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:O3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);
        },];

    }

    public function title(): string
    {
        return __('research/report.report2-page.title');
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
