<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepartmentIdColumnToResearchProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_project', function (Blueprint $table) {
            $table->foreignId('department_id')->nullable()->comment('Đơn vị thực hiện')->constrained('department')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_project', function (Blueprint $table) {
            $table->dropColumn('department_id');
        });
    }
}
