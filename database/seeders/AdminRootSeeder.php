<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;



class AdminRootSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $pass = 'vkist2021^';
        $user = User::where('username', 'admin_root')->first();
        if(!$user) {
            $user = User::updateOrCreate([
                'username' => 'admin_root',
                'name' => 'admin_root',
                'email' => 'admin_root@gmail.com',
                'password' => Hash::make($pass), // password
                "status" => 1,
                'gw_user_id' => '0000000',
                'gw_pass' => $pass,
            ]);
        }

        $user->password = Hash::make($pass);
        $user->change_password_at = now();
        $role = Role::updateOrCreate(['name' => 'administrator', 'note'=>'Quyền quản trị', 'status' => 1 ]);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);
        $user->save();

        $user->assignRole([$role->id]);
    }
}
