<?php

namespace App\Http\Livewire\Admin\Budget;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Budget;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use DB;
use Excel;
use App\Models\BudgetHasExpensePlan;

class ReportBudget extends BaseLive
{
    public $searchBudget;
    public $list_budget_code;
    public $data_arr;
    public $checkFirst = false;
    public function mount(){
        if(!$this->list_budget_code){
            $this->list_budget_code = Budget::where('status',\App\Enums\EBudgetStatus::DONE)->orderBy('id', 'desc')->pluck('code','id')->toArray();
        }

    }
    public function render()
    {
        if(!$this->searchBudget){
           $this->checkFirst = true;
           $this->searchBudget =  array_key_last($this->list_budget_code);
        }
        $data_array = array();
        if ($this->searchBudget) {
            $budget = Budget::findOrFail($this->searchBudget);
            if($budget->allocated == 1){
                //phan bo theo thang
                $data_month = BudgetHasExpensePlan::where('budget_id',$this->searchBudget)->select('money_plan', 'money_real', 'month_budget')->get();
                foreach ($data_month as $rs){
                    $rs->percent = ($rs->money_plan && $rs->money_real)? number_format((($rs->money_real - $rs->money_plan)*100/$rs->money_plan),2) : '';
                    $rs->color = "red";
                    $money_plan = (int)$rs->money_plan;
                    $money_real = (int)$rs->money_real;

                    $data_array[0][$rs->month_budget-1]['month'] = (int)$rs->month_budget;
                    $data_array[1][$rs->month_budget-1]['month'] = (int) $rs->month_budget;
                    $data_array[0][$rs->month_budget-1]['x'] = (int)$rs->month_budget;
                    $data_array[1][$rs->month_budget-1]['x'] = (int)$rs->month_budget;
                    $data_array[0][$rs->month_budget-1]['y'] = $money_plan;
                    $data_array[1][$rs->month_budget-1]['y'] = $money_real;
                    $data_array[0][$rs->month_budget-1]['percent'] =  '';
                    $data_array[1][$rs->month_budget-1]['percent'] =  abs($rs->percent)? abs($rs->percent) . '%' : '' ;
                    if($rs->money_real == $rs->money_plan && $rs->money_real > 0){
                        $data_array[1][$rs->month_budget-1]['percent'] = '0%';
                    }
                    $color = "#CCCC00";
                    if($money_real > $money_plan){
                        $color = "red";
                    } else if($money_real < $money_plan){
                        $color = "blue";
                    }
                    $data_array[1][$rs->month_budget-1]['dataLabels']['color'] = $color;
                }
            } else {
                $money_plan = (int)$budget->total_budget;
                $money_real = (int)$budget->total_real_budget;
                $percent = ($money_plan && $money_real)? number_format(($money_real - $money_plan)*100/$money_plan,2) : '';
                $color = "#CCCC00";
                if($money_real > $money_plan){
                    $color = "red";
                } else if($money_real < $money_plan){
                    $color = "blue";
                }

                $data_array[0][0]['y'] = $money_plan;
                $data_array[1][0]['y'] = $money_real;
                $data_array[1][0]['percent'] =  '' ;
                $data_array[1][0]['percent'] =  abs($percent)? abs($percent) . '%' : '' ;
                if($money_real == $money_plan && $money_real > 0){
                    $data_array[1][0]['percent'] = '0%';
                }
                $data_array[1][0]['dataLabels']['color'] = $color;
            }
            $lang = array();
            $lang['title']  = __('data_field_name.budget.highchart_title');
            $lang['money_plan']  = __("data_field_name.budget.highchart_money_plan");
            $lang['money_real']  = __("data_field_name.budget.highchart_money_real");

            if($this->checkFirst){
               $this->emit("showBudgetData",$data_array, $lang );
            }
        }
        $this->data_arr = $data_array;
        return view('livewire.admin.budget.report-budget');
    }
}
