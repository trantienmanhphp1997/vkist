<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="">{{__('executive/contract.breadcrumbs.activity-management')}} </a> \ <span>{{__('menu_management.menu_name.dashboard_executive')}}</span></div> 
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title">{{__('menu_management.menu_name.dashboard_executive')}}</h3>
                </div>
            </div>
            <div class="dashboard-asset">
                <div class="row chart-asset">
                        <div class="col-md-6 pd8">
                            <div class="card">
                                <div class="widget-chart-content card_title">
                                    <div class="subheading">
                                        <div class="subheading text-uppercase">
		                                    {{__('data_field_name.dashboard_executive.statistics_by_age')}}                                         
		                                </div>
		                                <p class="text-2 size13 mt-8">{{__('data_field_name.dashboard_executive.updated')}} :<span> {{now()->timeZone(date_default_timezone_get())->format('H:i d/m/Y')}}</span></p>
                                    </div>
                                </div>
                                <figure class="highcharts-figure container-fluid">
                                    <div id="pieChart"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-6 pd8">
                            <div class="card">
                                <div class="widget-chart-content card_title">
                                    <div class="subheading text-uppercase">
	                                    {{__('data_field_name.dashboard_executive.human_resources_change_statistics')}}           
	                                </div>
                                </div>
                                <figure class="highcharts-figure container-fluid pt-5">
                                    <div id="chartColumn"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="dashboard-asset">
              <div class="row chart-asset">
                  <div class="col-md-12 pd8">
                      <div class="card h-auto">
                          <div class="widget-chart-content">
                              <div class="subheading text-uppercase text-1 size24">
                                {{__('data_field_name.dashboard_executive.work_plan_for', [
                                	'year' => now()->year
                                ])}}
                              </div>
                              <p class="mt-12">{{__('data_field_name.dashboard_executive.quarter')}} {{$quarter}}</p>
                          </div>
                          <div class="inner-tab dashboard-manager pt-14">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                      <tr class="border-radius ">
                                        <th scope="col" class="border-radius-left size11 text-left">{{__('data_field_name.dashboard_executive.index')}}</th>
                                        <th scope="col" class="size11 text-center">{{__('data_field_name.dashboard_executive.code')}}</th>
                                        <th scope="col" class="text-center size11">{{__('data_field_name.dashboard_executive.date')}}</th>
                                        <th scope="col" class="text-center size11">{{__('data_field_name.dashboard_executive.participants')}}</th>
                                        <th scope="col" class="border-radius-right size11 text-center">{{__('data_field_name.dashboard_executive.content')}}</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach($workPlan as $item)
	                                      <tr>
	                                        <td class="code">{{$item['index']}}</td>
                                            <td class="text-center"><a href="{{route('admin.general.work-plan.show', ['id' => $item['id']])}}">{{$item['code']}}</a></td>
	                                        <td class="text-center">{{$item['start_date']}}</td>
	                                        <td class="text-left">
	                                        	@foreach($item['workPlanHasUserInfo'] as $index => $value)
	                                        		@if($index == 0)
	                                        			{{$value->fullname}}
	                                        		@else
	                                        			{{', ' . $value->fullname}}
	                                        		@endif
	                                        	@endforeach
	                                        </td>
	                                        <td class="text-left"> {{$item['content']}} </td>
	                                      </tr>
	                                    @endforeach
                                    </tbody>
                                  </table>
                                    @if(count($workPlan) == 0)
                                        <div class="title-approve text-left ml-0">
                                            <span>{{__('common.message.empty_search')}}</span>
                                        </div>
                                    @endif
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>

<script>
	let categories = [];
	let userLeave = [];
	let userWorking = [];
	categories.push(<?php
		foreach ($year as $item) {
			echo $item . ',';
		}
	?>);
	userLeave.push(<?php
		foreach ($userLeave as $item) {
			echo $item . ',';
		}
	?>);
	userWorking.push(<?php
		foreach ($userWorking as $item) {
			echo $item . ',';
		}
	?>);
	let name1 = <?php echo __('data_field_name.dashboard_executive.personnel_is_working'); ?>;
	let name2 = <?php echo __('data_field_name.dashboard_executive.personnel_quit'); ?>;
    Highcharts.chart('chartColumn', {
        chart: {
            type: 'column'
        },
        plotOptions: {
            column: {
                colorByPoint: true
            }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: categories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span>{point.key}</span><table style="width:160px">',
            pointFormat: '<tr><td style="color:{series.color};padding:0;font-size:12px">{series.name}: </td>' +
                '<td style="padding:0;font-size:12px"><b>{point.y:.f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
    column: {
      grouping: false,
      shadow: false,
      borderWidth: 0
    }
  },
        series: [{
            name: name1,
            data: userWorking,
            color: '#6AD8A7',

        }, {
            name: name2,
            data: userLeave,
            color: '#ff5630',

        }]
    });

    // Build the chart
    let pieChartData = [];
    pieChartData.push(<?php
		foreach ($pieChartData as $item) {
			echo $item . ',';
		}
	?>);
	name1 = <?php echo __('data_field_name.dashboard_executive.under-30-years-old') ?>;
	name2 = <?php echo __('data_field_name.dashboard_executive.from-30-to-45-years-old') ?>;
	let name3 = <?php echo __('data_field_name.dashboard_executive.from-45-to-60-years-old') ?>;
	let name4 = <?php echo __('data_field_name.dashboard_executive.over-60-years-old') ?>;
    Highcharts.chart('pieChart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        plotOptions: {
            column: {
                colorByPoint: true
            }
        },
        colors: [
            '#377DFF',
            '#6AD8A7',
            '#FFAB00',
            '#FF5630',
        ],
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: <?php echo __('data_field_name.dashboard_executive.percent') ?>,
            colorByPoint: true,
            data: [{
                name: name1,
                y: pieChartData[0],
                sliced: true,
                selected: true
            }, {
                name: name2,
                y: pieChartData[1]
            }, {
                name: name3,
                y: pieChartData[2]
            }
            , {
                name: name4,
                y: pieChartData[3]
            }]
        }]
    });
</script>
