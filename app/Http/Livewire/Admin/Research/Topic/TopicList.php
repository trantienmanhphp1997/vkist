<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use App\Enums\EResearchCategoryType;
use App\Enums\ETopicStatus;
use App\Enums\ETopicType;
use App\Exports\TopicExport;
use App\Models\GroupUserHasTopic;
use App\Service\Community;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use App\Models\Topic;
use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchCategory;
use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Facades\Excel;

class TopicList extends BaseLive
{
    public $searchTerm;
    public $searchStatus;
    public $searchField;
    public $searchStartDate;
    public $searchEndDate;

    public $topicStatus;
    public $topicTypes;
    public $topicFields;
    public $topicEditableConditions;

    public $checkDownloadPermission;

    protected $listeners = [
        'set-from_date' => 'setStartDate',
        'set-to_date' => 'setEndDate'
    ];

    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
        $this->checkDownloadPermission = checkRoutePermission('download');

        $this->topicStatus = ETopicStatus::getList();

        $this->topicTypes = ETopicType::getList();

        $this->topicFields = ResearchCategory::query()->where('type', EResearchCategoryType::FIELD)->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        })->toArray();
        $this->topicEditableConditions = [
            ETopicStatus::STATUS_NOT_SUBMIT
        ];
    }

    public function render()
    {
        $query = Topic::query()->with(['researchCategory', 'level']);

        $this->searchTerm = trim($this->searchTerm);
        if (strlen($this->searchTerm) > 0) {
            $query->where(function($query) {
                return $query->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')
                            ->orWhereRaw("LOWER(code) LIKE LOWER('%{$this->searchTerm}%')");
            });
        }
        if (strlen($this->searchStatus) > 0) {
            $query->where('status', '=', $this->searchStatus);
        }

        if (!empty($this->searchField)) {
            $query->where('research_category_id', $this->searchField);
        }
        //check permission topic
        if (!empty(Community::listTopicIdAllowed())) {
            $query->whereIn('id', Community::listTopicIdAllowed());
        }

        if (!empty($this->searchStartDate)) {
            $query->where('start_date', '>=', $this->searchStartDate);
        }

        if (!empty($this->searchEndDate)) {
            $query->where('end_date', '<=', $this->searchEndDate);
        }

        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);

        return view('livewire.admin.research.topic.topic-list', compact('data'));
    }

    public function delete()
    {
        $topic = Topic::query()->where('status', '=', ETopicStatus::STATUS_NOT_SUBMIT)->findOrFail($this->deleteId);
        $topic->members()->delete();
        $topic->delete();
        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.delete')]);
    }

    public function setStartDate($value)
    {
         $this->searchStartDate = empty($value['from_date']) ? '' : date('Y-m-d', strtotime($value['from_date']));
    }

    public function setEndDate($value)
    {
        $this->searchEndDate = empty($value['to_date']) ? '' : date('Y-m-d', strtotime($value['to_date']));
    }

    public function export()
    {
        $today = date("d_m_Y");
        return Excel::download(new TopicExport($this->searchTerm, $this->searchStatus, $this->searchField, $this->searchStartDate, $this->searchEndDate), 'list-topc-' . $today . '.xlsx');
    }
}
