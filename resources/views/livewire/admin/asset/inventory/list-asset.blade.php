<div class="col-md-10 col-xl-11 box-detail box-user">
    <style>
        .divider-th-r::after {
            content: '';
            position: absolute;
            top: 5px;
            right: 0;
            width: 1px;
            height: 20px;
            border-right: 1px solid #C1C7D0;
            background: #F6F6F7 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="">{{__('common.listNameFunction.asset')}} </a> \ <span>{{__('data_field_name.asset_inventory.title')}}</span>\
            <span>{{__('data_field_name.asset_inventory.list_asset')}}</span>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.asset_inventory.list_asset')}}</h3>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row list-active">
                        <div class="col-md-4">
                            <div  class="form-group search-expertise search-plan ">
                                <div class="search-expertise inline-block">
                                    <input type="text" class="form-control size13" placeholder="{{__('data_field_name.asset_inventory.search_name')}}" wire:model.debounce.1000ms="searchTerm">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group search-expertise search-plan ">
                                <select wire:model.debounce.1000ms="searchDepartment" style="min-width:90%;" class="form-control">
                                    <option hidden>{{__('data_field_name.asset_inventory.department')}}</option>
                                    <option value="0">{{__('common.select-box.choose_all')}}</option>
                                    @foreach($department as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class=" float-right">
                                <button type="button" class="btn par6  " title="print" data-toggle="modal" data-target="#export-modal"><img src="/images/filterdown.svg" alt="filterdown"></button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <table wire:ignore.self class="table table-general table-sort" id="inventory_detail">
                            <thead>
                            <tr class="border-radius text-uppercase">
                                <th scope="col" rowspan="2"
                                    class="border-radius-left text-left text-uppercase">{{__('data_field_name.asset.asset_code_')}}
                                </th>
                                <th scope="col" rowspan="2" class="text-uppercase">{{__('data_field_name.asset.asset_name')}}</th>

                                <th scope="col" rowspan="2" class="text-uppercase">{{__('data_field_name.asset_inventory.department_used')}}</th>
                                <th scope="col" colspan="2"
                                    class="text-center text-uppercase">{{__('data_field_name.asset_inventory.books')}}</th>
                                <th scope="col" colspan="5"
                                    class="text-center text-uppercase">{{__('data_field_name.asset_inventory.actual')}}</th>
                                <th scope="col" rowspan="2"
                                    class=" min-width text-center text-uppercase">{{__('data_field_name.asset_inventory.difference')}}</th>
                                <th scope="col" rowspan="2" class="view"  style="border-top-right-radius: 7px;border-bottom-right-radius: 7px"
                                    class="border-radius-right min-width text-center text-uppercase">{{__('data_field_name.asset_inventory.action')}}</th>
{{--                               --}}
                            </tr>
                            <tr>
                                <th scope="col" class="text-center sub-th divider-th-r w-10">{{__('data_field_name.asset_inventory.quantity')}}</th>
                                <th scope="col" class="text-center sub-th w-10">{{__('data_field_name.asset_inventory.status')}}</th>
                                <th scope="col" class="text-center sub-th divider-th-r w-10">{{__('data_field_name.asset_inventory.quantity')}}</th>
                                <th scope="col" class="text-center sub-th divider-th-r w-10">{{__('data_field_name.asset_inventory.cancel')}}</th>
                                <th scope="col" class="text-center sub-th divider-th-r w-10">{{__('data_field_name.asset.lost_report')}}</th>
                                <th scope="col" class="text-center sub-th divider-th-r w-10">{{__('data_field_name.asset.liquidation')}}</th>
                                <th scope="col" class="text-center sub-th divider-th-r w-10">{{__('data_field_name.asset.maintain').' - '.__('data_field_name.asset.repair')}}</th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                            @forelse($inventory->category as $category)
                                <tr>
                                    <td colspan="13" class="sub-td text-uppercase">
                                        {{$category->name}}
                                    </td>
                                </tr>
                                @php $hasData = false; @endphp

                                @forelse($data as  $asset)
                                    @if($asset->category_id==$category->id)
                                    <tr>
                                        @php $hasData = true; @endphp
                                        <td style="    padding-left: 40px;">{{$asset->code}}</td>
                                        <td>{{$asset->name}}</td>
                                        <td>{{$inventory->department?$inventory->department->name:''}}</td>
                                        <td class="text-center">
                                            {{ $asset->allocate }}
                                        </td>
                                        <td class="text-center">{{$status->valueToName(\App\Enums\EAssetStatus::USING)}}</td>
                                        <td class="text-center">{{ $asset->actual_sum ??0}}</td>
                                        <td class="text-center">{{$asset->actual_situation == \App\Enums\EAssetStatus::CANCELLED? $asset->actual_quantity :0 }}</td>
                                        <td class="text-center">{{$asset->actual_situation == \App\Enums\EAssetStatus::LOSTED? $asset->actual_quantity :0}} </td>
                                        <td class="text-center">{{$asset->actual_situation == \App\Enums\EAssetStatus::LIQUIDATION? $asset->actual_quantity :0}}</td>
                                        <td class="text-center">{{$asset->maintenance +$asset->repaired}}</td>
                                        <td class="text-center">{{$asset->actual_quantity?abs($asset->actual_sum-$asset->allocate):0}}</td>
                                        <td class="text-center view">
                                            @if($checkEditPermission)
                                            <button type="button" data-toggle="modal" id="show_modal_edit" data-target="#edit-modal" title="Sửa" wire:click="edit({{ $asset->id }})" class="btn-sm border-0 bg-transparent mr-1 show_modal_edit"><img src="/images/edit.png" alt="edit"></button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                @empty
                                @endforelse
                                @if(!$hasData)
                                <tr>
                                    <td class="text-danger p-3" colspan="3">
                                        {{__('common.message.no_data')}}
                                    </td>
                                </tr>
                                @endif
                            @empty
                            @endforelse
                            @if(!isset($hasData))
                                <tr>
                                    <td class="text-danger p-3" colspan="3">
                                        {{__('common.message.no_data')}}
                                    </td>
                                </tr>
                            @endif

                            @if($data->isNotEmpty())
                            <tr class="text-center">
                                <td colspan="8">
                                    @if($checkEditPermission)
                                    <button class="btn btn-viewmore-news mr0 mrr16" wire:click='updateStatus'> {{__('common.button.complete')}}</button>
                                    @endif
                                </td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="edit-modal" tabindex="-1" aria-labelledby="createTopicFee" aria-hidden="true">
            <div class="modal-dialog  modal-appraisal">
                <div class="modal-content personnel">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{__('data_field_name.research_cost.create_cost')}}</h4>

                        <div class="form-group">
                            <label>{{__('data_field_name.asset.asset_code')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.lazy="code">
                            @error('code') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.asset.asset_name')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control form-input" wire:model.lazy="name">
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group info-request">
                            <label>{{__('data_field_name.asset_inventory.department_execute')}}<span class="text-danger ">(*)</span></label>
                            <select wire:model.lazy="department_id">
                                <option hidden>{{__('common.select-box.choose')}}</option>
                                <option value="all">{{__('common.select-box.choose_all')}}</option>
                                @foreach($department as $key => $val)
                                    <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                            @error('department_id') <span class="text-danger" style="font-size: 14px">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group info-request">
                            <label>{{__('data_field_name.asset_inventory.choose_member')}}
                            </label>
                            <select wire:model.lazy="user_info_id">
                                <option hidden>{{__('common.select-box.choose')}}</option>
                                <option value="all">{{__('common.select-box.choose_all')}}</option>
                                @foreach($user_info as $key => $val)
                                    <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.asset_inventory.quantity')}}</label>
                            <input type="text" class="form-control form-input" wire:model.lazy="quantity" disabled>
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.asset_inventory.situation')}}</label>
                            <select wire:model.lazy="situation" class="form-control" disabled="">
                                <option value="">--{{__('data_field_name.asset_inventory.situation')}}--</option>
                                @foreach($status->getList() as $key => $val)
                                <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.asset_inventory.actual_quantity')}}<span class="text-danger">(*)</span></label>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control form-input" wire:model.lazy="actual_quantity">
                            @error('actual_quantity') <span class="text-danger" style="font-size: 14px">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{__('data_field_name.asset_inventory.actual_situation')}}<span class="text-danger">(*)</span></label>
                            <select wire:model.lazy="actual_situation" class="form-control">
                                <option value="">--{{__('data_field_name.asset_inventory.actual_situation')}}--</option>
                                @foreach($status->getList() as $key => $val)
                                <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                            @error('actual_situation') <span class="text-danger" style="font-size: 14px">{{ $message }}</span>@enderror
                        </div>
                        <div class="attached-center">
                            <button type="button" id="close-modal-edit" class="btn btn-secondary close-btn" data-dismiss="modal">{{__('common.button.close')}}</button>
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="update()" class="btn btn-primary close-modal">{{__('common.button.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @include('livewire.common.modal._modalConfirmExport')
</div>
<script>
    $("document").ready(function() {

        window.livewire.on('close-modal-edit', () => {
          document.getElementById('close-modal-edit').click()
      });

        $('#edit-modal').on('hidden.bs.modal', function () {
            window.livewire.emit('resetInput');
        });

    });
</script>



