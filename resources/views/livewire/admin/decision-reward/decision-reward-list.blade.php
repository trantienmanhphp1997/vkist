<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div  class="breadcrumbs">
                    <a href="">{{__('decision-reward/decision-reward.breadcrumbs.decision-reward-management')}}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">
                {{__('decision-reward/decision-reward.breadcrumbs.decision-reward-list')}}
            </h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group search-expertise">
                                <div class="search-expertise">
                                    <input type="text" placeholder="{{__('decision-reward/decision-reward.placeholder.search')}}" name="search" class="form-control size13"
                                           wire:model.debounce.1000ms="searchTerm"
                                           id='input_vn_name' autocomplete="off">
                                    <span><img src="/images/Search.svg" alt="search"/></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group float-right">
                                @if($checkCreatePermission)
                                <a class="btn btn-viewmore-news mr0" href="{{route('admin.decision-reward.create')}}" style="color:white">
                                    <img src="/images/plus2.svg" alt="plus"> {{__('common.button.add')}}
                                </a>
                                @endif
                            </div>
                        </div>
                       <div class="table-responsive">
                        <table class="table table-general">
                                <thead>
                                <tr class="border-radius">
                                    <th scope="col" class="border-radius-left text-center " style="width:20%">{{__('decision-reward/decision-reward.table_column.name')}}</th>
                                    <th scope="col" class="text-center" style="width:10%">{{__('decision-reward/decision-reward.table_column.type_of_decision')}}</th>
                                    <th scope="col" class="text-center" style="width:10%">{{__('decision-reward/decision-reward.table_column.number_decision')}}</th>
                                    <th scope="col" class="text-center" style="width:10%">{{__('decision-reward/decision-reward.table_column.decision_date')}}</th>
                                    <th scope="col"  class="text-center" style="width:10%">{{__('decision-reward/decision-reward.table_column.type_of_prize')}}</th>
                                    <th scope="col" class="text-center" style="width:10%">{{__('decision-reward/decision-reward.table_column.type_reward')}}</th>
                                    <th scope="col"  class="text-center" style="width:20%">{{__('decision-reward/decision-reward.table_column.receiver')}}</th>
                                    <th scope="col"  class="text-center" class="border-radius-right min-width text-center" style="width:10%">{{__('decision-reward/decision-reward.table_column.action')}}</th>
                                </tr>
                                </thead>
                                <div wire:loading class="loader"></div>
                                <tbody>
                                @forelse($data as $row)
                                    <tr>
                                        <td class="text-center">{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                        <td  class="text-center" >{{App\Enums\EDecisionRewardDecisionType::valueToName($row->type_of_decision)}}</td>
                                        <td class="text-center" >{{$row->number_decision }}</td>
                                        <td class="text-center" >{{ reFormatDate($row->decision_date) }}</td>
                                        <td class="text-center">{{App\Enums\EDecisionRewardPrizeType::valueToName($row->type_of_prize)}}</td>
                                        <td class="text-center">{{$row->type_reward}}</td>
                                        <td class="text-center">
                                            @foreach($row->receivers as $key => $receiver)
                                            @if($key + 1 == count($row->receivers))
                                            <span>{{$receiver->info->fullname ?? ''}}</span>
                                            @else
                                            <span>{{$receiver->info->fullname ?? ''}},</span>
                                            @endif
                                            @endforeach
                                        </td>
                                        <td class="text-center">
                                            @if($checkEditPermission)
                                                <button type="button" title="{{__('common.button.edit')}}" class="btn-sm border-0 bg-transparent mr-1">
                                                    <a href="{{route('admin.decision-reward.edit', ['id' => $row->id])}}">
                                                        <img src="/images/edit.png" alt="edit">
                                                    </a>
                                                </button>
                                            @endif
                                            @if($checkDestroyPermission)
                                                @include('livewire.common.buttons._delete')
                                            @endif

                                        </td>
                                    </tr>
                                    @empty
                                        <tr class="text-center text-danger"><td colspan="8">{{__('decision-reward/decision-reward.empty-record')}}</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                       </div>
                        {{$data->links()}}

                        @include('livewire.common.modal._modalDelete')

                        {{-- <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body box-user">
                                        <h4 class="modal-title" id="exampleModalLabel">{{__('decision-reward/decision-reward.modal.title.delete')}}</h4>
                                        {{__('decision-reward/decision-reward.modal.body.delete-warning')}}
                                    </div>
                                    <div class="group-btn2 text-center  pt-24">
                                        <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.back')}}</button>
                                        <button type="button" class="btn btn-save" data-dismiss="modal" wire:click="delete()">{{__('common.button.delete')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


