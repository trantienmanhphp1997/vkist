<?php

namespace App\Http\Livewire\Admin\Research\Category;

use App\Exports\UsersExport;
use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchCategory;
use App\Enums\EResearchCategoryType;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;

class CategoryFieldList extends BaseLive
{
    public $searchTerm;
    public $deleteId = '';
    public $updateMode = false;
    public $editMode = false;

    //data for update modal
    public $categoryId;
    public $name;
    public $description;
    public $note;
    public $type = EResearchCategoryType::FIELD;

    public function render()
    {
        $query = ResearchCategory::query();
        $searchTerm = $this->searchTerm;

        $query->where('type', EResearchCategoryType::FIELD);

        if (!empty($searchTerm)) {
            $query->where('research_category.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')->get();
        }

        $data = $query->orderBy('id', 'DESC')->paginate($this->perPage);
        return view('livewire.admin.research.category.category-field-list', ['data' => $data]);
    }

    public function getCategoryInfoForUpdateModal($id)
    {
        $this->resetValidation();
        $this->updateMode = true;
        $this->editMode = false;
        $categoryField = ResearchCategory::findOrFail($id);
        $this->categoryId = $id;
        $this->name = $categoryField->name;
        $this->description = $categoryField->description;
        $this->note = $categoryField->note;
    }
    public function showId($id)
    {
        $this->resetValidation();
        $this->updateMode = false;
        $this->editMode = true;
        $categoryField = ResearchCategory::findOrFail($id);
        $this->categoryId = $id;
        $this->name = $categoryField->name;
        $this->description = $categoryField->description;
        $this->note = $categoryField->note;
    }
    // showId
    public function resetCategoryInfo()
    {
        $this->reset(['updateMode', 'categoryId', 'name', 'description', 'note','editMode']);
    }

    public function delete()
    {
        parent::delete();
        $categoryField = ResearchCategory::find($this->deleteId);
        if(count($categoryField->ideals) != 0) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('research/category-field.notification.delete-fail')]);
            return;
        }
        $categoryField->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }
    public function store()
    {
        if($this->updateMode) {
            parent::edit($this->categoryId);
        } else {
            parent::store();
        }
        $this->validate([
            'name' => ['required','max:48','regex:/^[a-zA-Z0-9'.config('common.special_character.alphabet').'\.\-\_ ]+$/'],
            'note' => 'max:1000',
            'description' => 'max:1000',
        ], [], [

            'name' => __('research/category-field.validate.name'),
            'note' => __('research/category-field.validate.note'),
            'description' => __('research/category-field.validate.description'),

        ]);
        if ($this->categoryId) {
            $categoryField = ResearchCategory::findOrFail($this->categoryId);
            $isUpdate = true;
        } else {
            $categoryField = new ResearchCategory;
            $isUpdate = false;
        }
        $categoryField->name = $this->name;
        $categoryField->note = $this->note;
        $categoryField->description = $this->description;
        $categoryField->type = $this->type;

        $categoryField->save();

        $this->resetCategoryInfo();
        $this->emit('close-modal-create-or-edit-category-field');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $isUpdate ? __('research/category-field.notification.edit-success') : __('research/category-field.notification.create-success')]);
    }
}
