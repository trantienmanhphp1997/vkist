@php $checkShowMode = checkShowMode(); @endphp
<div class="row inner-tab">
    <div class="col-md-3">
        <div class="m-auto">
            <img src="{{ !empty($data->avatar) ? $data->avatar : asset('images/default-avatar.png') }}" alt="default-avatar" class="w-75 border" style="aspect-ratio: 3/4; cursor: pointer;" id="preview-avatar" onclick="document.querySelector('#select-avatar').click()"/>
            @if($checkShowMode)
            @else
            <div class="upload-avatar d-flex justify-content-center align-items-center">
                <input type="file" name="avatar" id="select-avatar" class="d-none">
            </div>
            @endif
        </div>
        <div class="text-danger text-center" id="avatar-error">
            @error('avatar')
                {{$message}}
            @enderror
        </div>
    </div>
    <div class="col-md-9">
        <div class="row">

                @if($isEdit == false)
                <div class="col-md-12">
                    <h3 class="title-personal">{{__('data_field_name.user.personal_information')}}</h3>
                </div>
                @endif
                @if($isEdit==true)
                <div class="col-md-6">
                    <h3 class="title-personal">{{__('data_field_name.user.personal_information')}}</h3>
                </div>
                <div class="col-md-6" id="additional">

                        <button type="button" class="btn btn-sm btn-viewmore-news mr-5 col-7" data-toggle="modal" data-target="#editModal">
                        {{__('data_field_name.user.appoint_dismissed')}}
                        </button>

                </div>
                @endif


            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.full_name')}} <span class="text-danger">*</span></label>
                            {!! Form::text('fullname', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
                            @error('fullname')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.code')}} <span class="text-danger">*</span></label>
                            {!! Form::text('code', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
                            @error('code')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.common_field.department')}} <span class="text-danger">*</span></label>
                            <select name="department_id"  id="exampleFormControlInput4" class="form-control"{{$isEdit?'disabled':''}} {{$checkShowMode?'disabled':''}} wire:model.lazy="parent_id">
                                <option value="">-- --</option>
                                @foreach ($data_department as $value)
                                <option value="{{$value['id']}}" {{($value['id'] == $data->department_id || $value['id'] == old('department_id')) ? 'selected' : ''}}>
                                    {!!$value['padLeft'].$value['name']!!}
                                </option>
                                @endforeach
                            </select>
                            @error('department_id')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.common_field.position')}} <span class="text-danger">*</span></label>
                            {!! Form::select('position_id', $master_data['positions'], null, ['class' => 'form-control', 'disabled' => $checkShowMode,'disabled'=>$isEdit]) !!}
                            @error('position_id')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.common_field.address')}} <span class="text-danger">*</span></label>
                            {!! Form::text('address', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
                            @error('address')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.phone')}} <span class="text-danger">*</span></label>
                            {!! Form::tel('phone', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode, 'maxlength' => 11)) !!}
                            @error('phone')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.birth_day')}} <span class="text-danger">*</span></label>
                            @include('layouts.partials.input._inputDate', ['input_id' => 'birthday', 'place_holder'=>'','name'=>'birthday', 'default_date' => is_null($data->birthday) ? date('Y-m-d') : date('Y-m-d',strtotime($data->birthday)) , 'disabled' => $checkShowMode ])
                            @error('birthday')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.sex')}}</label>
                            {!! Form::select('gender', [0 => __('data_field_name.common_field.female'), 1 => __('data_field_name.common_field.male')], $data->gender, ['class' => 'form-control form-control-lg', 'disabled' => $checkShowMode]) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.marriage')}}</label>
                            {!! Form::select('married', [0 => __('data_field_name.common_field.single'), 1 => __('data_field_name.common_field.married')], $data->married, ['class' => 'form-control form-control-lg', 'disabled' => $checkShowMode]) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.common_field.email')}} <span class="text-danger">*</span></label>
                            {!! Form::text('email', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
                            @error('email')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h3 class="title-personal">{{__('data_field_name.user.another_infomation')}}</h3>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.nation')}}</label>
                            {!! Form::select('ethnic_id', $master_data['ethnics'], $data->ethnic_id, ['class' => 'form-control form-control-lg', 'disabled' => $checkShowMode]) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.religion')}}</label>
                            {!! Form::select('religion_id', $master_data['religions'], $data->religion_id, ['class' => 'form-control form-control-lg', 'disabled' => $checkShowMode]) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.country')}} <span class="text-danger">*</span></label>
                            {!! Form::select('nationality_id', $master_data['nationalities'], $data->nationality_id, ['class' => 'form-control form-control-lg', 'disabled' => $checkShowMode]) !!}
                            @error('nationality_id')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.cmnd')}} <span class="text-danger">*</span></label>
                            {!! Form::text('id_card', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
                            @error('id_card')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    {{-- kendo validate date-picker--}}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{__('data_field_name.user.create_at')}} <span class="text-danger">*</span></label>
                            @include('layouts.partials.input._inputDate', ['input_id' => 'issued_date', 'place_holder'=>'','name'=>'issued_date', 'default_date' => is_null($data->issued_date) ? date('Y-m-d') : date('Y-m-d',strtotime($data->issued_date)), 'disabled' => $checkShowMode ])
                            @error('issued_date') <span class="text-danger">{{ $message }}</span> @enderror
                            {{-- {!! Form::date('issued_date', $data->issued_date, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}--}}
                            {{-- @error('issued_date')--}}
                            {{-- <div class="text-danger">{{$message}}
                        </div>--}}
                        {{-- @enderror--}}
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.user.area')}}<span class="text-danger">*</span></label>
                        {!! Form::text('issued_place', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
                        @error('issued_place')
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.user.passport')}}</label>
                        {!! Form::text('passport_number', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
                        @error('passport_number')
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                {{-- kendo validate date-picker--}}
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__('data_field_name.user.create_at')}}</label>
                        {{-- {!! Form::date('passport_date', $data->passport_date, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}--}}
                        {{-- @error('passport_date')--}}
                        {{-- <div class="text-danger">{{$message}}
                    </div>--}}
                    {{-- @enderror--}}
                    @include('layouts.partials.input._inputDate', ['input_id' => 'passport_date', 'place_holder'=>'','name'=>'passport_date', 'default_date' => is_null($data->passport_date) ? date('Y-m-d') : date('Y-m-d',strtotime($data->passport_date)) , 'disabled' => $checkShowMode ])
                    @error('passport_date') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>{{__('data_field_name.user.area')}}</label>
                    {!! Form::text('passport_place', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
                    @error('passport_place')
                    <div class="text-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <h3 class="title-personal">{{__('data_field_name.common_field.note')}}</h3>
    <div class="col-md-12">
        {!! Form::textarea('note', null, array('placeholder' => '','class' => 'form-control', 'disabled' => $checkShowMode)) !!}
        @error('note')
            <div class="text-danger">{{$message}}</div>
        @enderror
    </div>
</div>
@if ($checkShowMode)
<div class="row">
    <div class="col-md-12">
        <div class="group-btn text-center">
            <a href="{{route('admin.user.index')}}" class="btn btn-cancel">{{ __('common.button.back') }}</a>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-12">
        <div class="group-btn text-center">
            <a href="{{route('admin.user.index')}}" class="btn btn-cancel">{{ __('common.button.cancel') }}</a>
            <button type="submit" class="btn btn-save">{{__('common.button.save')}}</button>
        </div>
    </div>
</div>
@endif
</div>

<script>
    let $previewAvatar = document.getElementById('preview-avatar');
    let $selectAvatar = document.getElementById('select-avatar');
    let $avatarError = document.getElementById('avatar-error');

    let imgExtensions = ['png', 'jpg', 'jpeg', 'bmp'];
    $selectAvatar.onchange = (event) => {
        let file = $selectAvatar.files[0];
        $avatarError.innerHTML = '';
        if(!file) return;

        let extension = file.name.split('.').pop();

        if(imgExtensions.includes(extension)) {
            $previewAvatar.src = URL.createObjectURL(file);
        } else {
            $selectAvatar.value = null;
            $avatarError.innerHTML = "{{ __('data_field_name.user.avatar_error') }}";
        }

    }
</script>
</div>
