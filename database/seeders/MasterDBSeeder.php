<?php

namespace Database\Seeders;

use App\Models\MasterData;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterDBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->position();
        $this->ethnic();
        $this->religion();
        $this->nation();
        $this->qualification();
        $this->rank();
        $this->topicLevel();
        $this->roleInTopic();
        $this->topicScientificName();
        $this->coefficientsSalary();
        $this->bank();
        $this->researchWay();
        $this->assetUnit();
        $this->specialized();
        $this->field();
        $this->roleInMember();
    }

    protected function uniqueInsertMany($data)
    {
        foreach ($data as $item) {
            // if(!isset($item['v_value_en']) || !isset($item['v_key']) || !isset($item['v_value'])) continue;
            // MasterData::query()->updateOrCreate(
            //     [
            //         'type' => $item['type'],
            //         'v_value' => $item['v_value'],
            //         'v_key'=>$item['v_key'],
            //         'order_number'=>$item['order_number'],
            //         'parent_id'=>$item['parent_id'] ?? null,
            //     ],
            //     ['v_value_en' => $item['v_value_en']]
            // );

            MasterData::query()->firstOrCreate(
                [
                    'type' => $item['type'],
                    'v_key' => $item['v_key'] ?? '',
                    'v_value' => $item['v_value'] ?? '',
                    'v_value_en' => $item['v_value_en'] ?? '',
                    'order_number' => $item['order_number'] ?? '',
                    'parent_id' => $item['parent_id'] ?? null,
                ]
            );
        }
    }

    public function position()
    {
        $data = require_once(database_path('raw/PositionData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Positions" . PHP_EOL;
    }

    public function ethnic()
    {
        $data = require_once(database_path('raw/EthnicData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Ethnic" . PHP_EOL;
    }

    public function religion()
    {
        $data = require_once(database_path('raw/ReligionData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Religion" . PHP_EOL;
    }

    public function nation()
    {
        $data = require_once(database_path('raw/NationData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Nations" . PHP_EOL;
    }

    public function qualification()
    {
        $data = require_once(database_path('raw/QualificationData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Qualifications" . PHP_EOL;
    }

    public function rank()
    {
        $data = require_once(database_path('raw/RankData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Ranks" . PHP_EOL;
    }

    public function bank()
    {
        $data = require_once(database_path('raw/BankData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Banks" . PHP_EOL;
    }
    public function specialized()
    {
        $data = require_once(database_path('raw/SpecializedData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Specialized" . PHP_EOL;
    }
    public function topicLevel()
    {
        $data = require_once(database_path('raw/TopicLevelData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Positions" . PHP_EOL;
    }
    public function assetUnit()
    {
        $data = require_once(database_path('raw/AssetUnitData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Units" . PHP_EOL;
    }

    public function roleInTopic()
    {
        $data = require_once(database_path('raw/RoleInTopicData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Topic Levels" . PHP_EOL;
    }

    public function topicScientificName()
    {
        for ($i = 1; $i <= 5; $i++) {
            $masterData = MasterData::query()->updateOrCreate([
                'type' => 12,
                'v_key' => 'Scientific name',
                'v_value' => "Chuyên ngành khoa học $i",
            ]);

            for ($j = 1; $j <= random_int(3, 5); $j++) {
                MasterData::query()->updateOrCreate([
                    'type' => 12,
                    'v_key' => 'Scientific name',
                    'v_value' => "Chuyên ngành khoa học $i.$j",
                    'parent_id' => $masterData->id
                ]);
            }
        }
        echo "Seeded: Scientific name" . PHP_EOL;
    }

    public function coefficientsSalary()
    {
        $data = require_once(database_path('raw/CoefficientSalaryData.php'));
        foreach ($data as $index => $item) {
            MasterData::query()->updateOrCreate([
                'v_key' => 'CoefficientsSalary',
                'v_value' => $item,
                'type' => '18',
                'order_number' => $index,
            ]);
        }
        echo "Seeded: Coefficient Salary" . PHP_EOL;
    }

    public function researchWay()
    {
        for ($i = 0; $i < 5; $i++) {
            MasterData::query()->updateOrCreate([
                'type' => 19,
                'v_key' => 'Research Way',
                'v_value' => 'Hướng nghiên cứu ' . $i,
                'order_number' => $i
            ]);
        }
        echo "Seeded: Research Ways" . PHP_EOL;
    }

    public function field()
    {
        $data = require_once(database_path('raw/FieldData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: Fields" . PHP_EOL;
    }

    public function roleInMember()
    {
        $data = require_once(database_path('raw/RoleMemberData.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: role member" . PHP_EOL;
    }
}
