<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnLeaderIdInResearchProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_project', function (Blueprint $table) {
            $table->dropColumn('leader_id');
        });
        Schema::table('research_project', function (Blueprint $table) {
            $table->foreignId('leader_id')->nullable()->constrained('user_info')->comment('leader_id map mới bảng user_info');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_project', function (Blueprint $table) {
            $table->dropColumn('leader_id');
        });
        Schema::table('research_project', function (Blueprint $table) {
            $table->foreignId('leader_id')->nullable()->constrained('topic')->comment('leader_id map mới bảng topic');
        });
    }
}
