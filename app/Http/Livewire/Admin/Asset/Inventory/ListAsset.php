<?php

namespace App\Http\Livewire\Admin\Asset\Inventory;

use App\Models\Asset;
use App\Models\AssetCategory;
use App\Models\AssetInventory;
use App\Models\Department;
use App\Models\InventoryDetail;
use App\Models\UserInf;
use App\Http\Livewire\Base\BaseLive;
use DB;
use App\Enums\EInventoryAssetStatus;
use App\Enums\EAssetStatus;
use App\Exports\InventoryDetailExport;

class ListAsset extends BaseLive
{
    public $inventory_id;
    public $searchDepartment;
    public $asset_id;
    public $name;
    public $code;
    public $department_id;
    public $user_info_id;
    public $quantity;
    public $situation;
    public $actual_quantity;
    public $actual_situation;
    protected $listeners =[
        'resetInput',
    ];
    /**
     * @var mixed
     */

    public function render()
    {
        $status = new EAssetStatus();
        $inventory = AssetInventory::findorFail($this->inventory_id);
        $query = InventoryDetail::where('inventory_id',$inventory->id)
                            ->where('a.department_id',$inventory->department_id)
                            ->leftJoin('asset', 'asset.id', '=','inventory_details.asset_id')
                            ->leftJoin('asset_allocated_revoke','asset_allocated_revoke.asset_id','=','asset.id')
                            ->leftJoin('user_info a','asset_allocated_revoke.user_info_id','=','a.id');
        $query->select('asset.id','asset.code','asset.name','asset.unsign_text','asset.category_id',DB::raw('sum(inventory_details.actual_quantity) as actual_sum'),'inventory_details.actual_quantity','inventory_details.actual_situation',
            DB::raw("SUM(CASE WHEN asset_allocated_revoke.status =".EAssetStatus::RECALL."  THEN asset_allocated_revoke.implementation_quantity ELSE 0 END) as recall"),
            DB::raw("SUM(CASE WHEN asset_allocated_revoke.status =".EAssetStatus::ALLOCATE."  THEN asset_allocated_revoke.implementation_quantity ELSE 0 END) as allocate"),
            DB::raw("SUM(CASE WHEN asset_allocated_revoke.status =".EAssetStatus::LIQUIDATION."  THEN asset_allocated_revoke.implementation_quantity ELSE 0 END) as liquidation"),
        )->groupBy('asset.id','asset.code','asset.name','asset.unsign_text','asset.category_id','inventory_details.actual_quantity','inventory_details.actual_situation');
        if ($this->searchTerm) {
            $query->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        if ($this->searchDepartment){
            $query->where('asset.department_id', $this->searchDepartment);
        }
        $data= $query->get();
        $department=Department::all()->pluck('name','id');
        $user_info=UserInf::all()->pluck('fullname','id');

        $listCategory = '';
        foreach($inventory->category as $category) {
            $listCategory.=$category->name.', ';
        }
//        dd($data);
        $listCategory = rtrim($listCategory, ', ');
        return view('livewire.admin.asset.inventory.list-asset',compact('data', 'department', 'inventory', 'user_info', 'listCategory', 'status'));
    }

    public function edit($id){
        $this->asset_id = $id;
        $asset = Asset::findOrFail($id);
//        dd($asset);
        $this->name = $asset->name;
        $this->code = $asset->code;
        $this->department_id = $asset->department_id;
        $this->user_info_id = $asset->user_info_id;
        $this->quantity = $asset->quantity;
        $this->situation = $asset->status;
        $this->actual_quantity = DB::table('inventory_details')->where('asset_id', '=', $id)->where('inventory_id', '=', $this->inventory_id)->get()->first()->actual_quantity??$asset->quantity;
        $this->actual_situation = DB::table('inventory_details')->where('asset_id', '=', $id)->where('inventory_id', '=', $this->inventory_id)->get()->first()->actual_situation??$asset->status;
//        $this->resetValidation();
    }

    public function update(){
        $this->validate([
            'code' => 'required|max:15|not_regex:/[a-z]/',
            'name' =>  ['required','regex:/^[a-zA-Z' . config('common.special_character.alphabet') .']+$/', 'max:100'],
            'actual_quantity' => 'required|max:7',
            'actual_situation' => 'required|max:7',
        ],[],[
            'code' => __('data_field_name.asset.asset_code'),
            'name' => __('data_field_name.asset.asset_name'),
            'actual_quantity' => __('data_field_name.asset_inventory.actual_quantity'),
            'actual_situation' => __('data_field_name.asset_inventory.actual_situation'),
        ]);
        $asset = Asset::findOrFail($this->asset_id);
        $asset->name=$this->name;
        $asset->code=$this->code;
        $asset->department_id = $this->department_id;
        $asset->user_info_id = $this->user_info_id;
        $asset->quantity = $this->quantity;
        $asset->situation = $this->situation;
        $asset->actual_quantity = $this->actual_quantity;
        $asset->actual_situation = $this->actual_situation;
        $asset->save();
        DB::table('inventory_details')->where('asset_id', '=', $asset->id)->where('inventory_id', '=', $this->inventory_id)->update(['actual_quantity' => $this->actual_quantity]);
        DB::table('inventory_details')->where('asset_id', '=', $asset->id)->where('inventory_id', '=', $this->inventory_id)->update(['actual_situation' => $this->actual_situation]);
        $this->emit('close-modal-edit');
        $this->resetInput();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }

    public function resetInput(){
        $this->user_info_id='';
        $this->name='';
        $this->code='';
        $this->department_id='';
        $this->quantity='';
        $this->situation='';
        $this->actual_quantity='';
        $this->actual_situation='';
    }

    public function updateStatus(){
        AssetInventory::findOrFail($this->inventory_id)->update(['status' => EInventoryAssetStatus::ACCOMPLISHED]);
        return redirect()->route('admin.asset.inventory.index');
    }

    public function export() {
        $today = date("d_m_Y");
        $export = new InventoryDetailExport($this->inventory_id);
        return \Excel::download($export, 'inventory-detail-'.$today.'.xlsx');
    }

}
