<?php

namespace App\Http\Livewire\Admin\Asset\Inventory;

use App\Enums\EInventoryAssetStatus;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use App\Models\AssetCategory;
use App\Models\AssetInventory;
use App\Models\Budget;
use App\Models\Department;
use App\Models\UserInf;
use App\Models\InventoryPeriod;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\Exports\InventoryDetailExport;
use DB;

class ListInventory extends BaseLive
{
    public $rowId;
    public $searchTerm;
    public $searchDeparkment;
    public $searchStatus;
    public $deleteId;
    public $name;
    public $department_id;
    public $category_inventory_id=[];
    public $period_id;
    public $inventory_date;
    public $user_info_id=[];
    public $filter;
    public $category_id;
    public $code;
    public $asset_id;
    public $asset_department_id;
    public $asset_user_info_id;
    public $quantity;
    public $situation;
    public $actual_quantity;
    public $actual_situation;
    protected $listeners =[
        'showRow',
        'deleteId',
        'editAsset',
        'resetInput',
        'set-perform-date' => 'setPerformDate'
    ];
    protected $rules=[
        'name'=>'required|max:255',
        'department_id'=>'required',
    ];

    public function mount(){
        $this->inventory_date = date('Y-m-d');
    }

    public function render()
    {

        if(!empty($this->validateInventoryCategory())){
            $message = __('data_field_name.asset_inventory.validate_period_start');
            foreach($this->validateInventoryCategory() as $id){
                $name_category=AssetCategory::findOrFail($id)->name;
                $message = $message.$name_category.', ';
            }
            $message=trim($message,', ');
            $message = $message.
            __('data_field_name.asset_inventory.validate_period_end');
            $this->addError('category_inventory_id', $message);
        }
        $query=AssetInventory::query();
        if (!empty($this->searchTerm)) {
            $query->where('asset_inventory.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')->get();
        }
        if (!empty($this->searchStatus) && $this->searchStatus!=0) {
            $query->where('asset_inventory.status', $this->searchStatus)->get();
        }
        if (!empty($this->searchDeparkment)) {
            $query->where('asset_inventory.department_id',$this->searchDeparkment)->get();
        }

        $query->leftJoin('department','department.id','=','asset_inventory.department_id');
        $query->select('asset_inventory.*',DB::raw('department.name as depart_name'));
        // filter

        $query->orderBy('asset_inventory.id', 'desc');
        $data=$query->paginate($this->perPage);
        $asset_category=[];
        $asset_inventory=[];
        if($this->rowId){
            $asset_inventory=AssetInventory::findorFail($this->rowId);
            $this->emit('loadListAsset',$asset_inventory->category->pluck('id'), $asset_inventory->id);
        }
        // create inventory
        $user_info=UserInf::all()->pluck('fullname','id');
        $category=AssetCategory::all()->pluck('name','id');
        $category->prepend(__('data_field_name.asset_inventory.choose_all'), '0');
        $department=Department::all()->pluck('name','id');
        $period=InventoryPeriod::all()->pluck('name','id');
        return view('livewire/admin/asset/inventory/list-inventory',
            compact('data', 'asset_inventory','asset_category','user_info','category','department', 'period'));
    }

    public function setPerformDate($value)
    {
        $this->inventory_date = date('Y-m-d', strtotime($value['perform-date']));
    }

    public function showRow($id){
        $this->rowId=$id[0];
    }
    public function delete(){
        $data_delete=[];
        if($this->deleteId){
            $data_delete=AssetInventory::where('id',$this->deleteId)->first();
        }
        if ($data_delete!=[]) {
            $data_delete->delete();
        }
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );

    }
    public function storeInventory(){
        $listCategoryInventoryId = [];
        if(in_array("0", $this->category_inventory_id)) {
            $listCategoryInventoryId = AssetCategory::all()->pluck('id')->toArray();
        } else {
            $listCategoryInventoryId = $this->category_inventory_id;
        }
        $this->validate([
            'name'=>'required|max:255|regex:/^[a-zA-Z0-9' . config('common.special_character.alphabet') . ']+$/',
            'department_id'=>'required',
            'period_id' => 'required',
        ],[],[
            'name'=>__('data_field_name.asset_inventory.minutes_name'),
            'department_id'=>__('data_field_name.asset_inventory.department_execute'),
            'period_id' => __('data_field_name.asset_inventory.period-inventory'),
        ]);
        if(!empty($this->validateInventoryCategory())){
            return;
        }
        $inventory=AssetInventory::create([
            'name'=>$this->name,
            'department_id'=>$this->department_id?$this->department_id:null,
            'inventory_date'=>$this->inventory_date,
            'period_id' => $this->period_id,
            'status'=>EInventoryAssetStatus::UNFULFILLED
        ]);

        if (!empty($this->user_info_id)) {
            $inventory->user()->attach($this->user_info_id);
        }
        if(!empty($listCategoryInventoryId)) {
            $inventory->category()->attach($listCategoryInventoryId);
            $assetId = Asset::whereIn('category_id', $listCategoryInventoryId)->pluck('id');
            $inventory->assets()->attach($assetId);
        }
        $this->emit('close-model-create');
        $this->emit('setSelect2Input');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);
        $this->resetInput();
    }


    public function resetInput(){
        $this->user_info_id=[];
        $this->name='';
        $this->code='';
        $this->department_id='';
        $this->period_id='';
        $this->inventory_date='';
        $this->category_inventory_id=[];
        $this->emit('setSelect2Input');
        $this->resetValidation();
    }

    public function validateInventoryCategory()
    {
        $listCategoryInventoryId = [];
        if($this->period_id == null || empty($this->category_inventory_id)) {
            return false;
        }
        if(in_array("0", $this->category_inventory_id)) {
            $listCategoryInventoryId = AssetCategory::all()->pluck('id')->toArray();
        } else {
            $listCategoryInventoryId = $this->category_inventory_id;
        }
        $inventory_id = AssetInventory::where('period_id', $this->period_id)->pluck('id');
        $categoryId = DB::table('inventory_category')->whereIn('inventory_id', $inventory_id)->pluck('category_id');
        return collect($listCategoryInventoryId)->intersect($categoryId)->toArray();
    }

    public function export() {
        $today = date("d_m_Y");
        $export = new InventoryDetailExport($this->deleteId);
        return \Excel::download($export, 'inventory-detail-'.$today.'.xlsx');
    }
}
