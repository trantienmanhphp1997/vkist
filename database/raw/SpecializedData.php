<?php
return [
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật sinh học',
        'v_value_en' => 'Bioengineering',
        'order_number' => '1',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật thực phẩm',
        'v_value_en' => 'Food Engineering',
        'order_number' => '2',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật hóa học',
        'v_value_en' => 'Chemical Engineering',
        'order_number' => '3',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Hóa học',
        'v_value_en' => 'Chemistry',
        'order_number' => '4',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật in',
        'v_value_en' => 'Printing Engineering',
        'order_number' => '5',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật hóa dược',
        'v_value_en' => 'Chemical Engineering',
        'order_number' => '6',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Công nghệ giáo dục',
        'v_value_en' => 'Educational Technology',
        'order_number' => '7',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Công nghệ thông tin',
        'v_value_en' => 'Information Technology',
        'order_number' => '8',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Tài chính ngân hàng',
        'v_value_en' => 'Banking and Financial Sector',
        'order_number' => '9',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật điều khiển - Tự động hóa',
        'v_value_en' => 'Control Engineering - Automation',
        'order_number' => '10',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật y sinh',
        'v_value_en' => 'Biomedical Engineering',
        'order_number' => '11',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật Môi trường',
        'order_number' => '12',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật môi trường',
        'v_value_en' => 'Enviromental Engineer',
        'order_number' => '13',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Điện tử viễn thông',
        'v_value_en' => 'Electronics and Telecommunication',
        'order_number' => '14',
    ],
    [
        'type' => '21',
        'v_key' => 'Specialized',
        'v_value' => 'Kỹ thuật nhiệt',
        'v_value_en' => 'Heat Engineering',
        'order_number' => '15',
    ],
];