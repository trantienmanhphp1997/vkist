<?php

namespace App\Http\Livewire\Admin\News\category;

use App\Enums\ERouteName;
use App\Models\CategoryNews;
use App\Models\News;
use App\Service\Community;
use Illuminate\Support\Facades\Auth;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use function Livewire\str;
use function Sodium\compare;

class Index extends BaseLive
{
    public $name;
    public $category_id;
    public $searchName;
    public $title;
    public $originName;

    public function render()
    {
        $query = CategoryNews::with(['admin'])->orderBy('id', 'DESC');
        $searchKey = $this->searchName;

        if ($this->searchName != null) {
            $query->whereRaw("LOWER(name) LIKE LOWER('%{$this->searchName}%')")
                ->orWhereHas('admin', function ($query) use ($searchKey) {
                    $query->whereRaw("LOWER(name) LIKE LOWER('%{$searchKey}%')");
                });
        }

        $data = $query->paginate($this->perPage);
        $data = $this->boldTextSearch($data, $searchKey);

        return view('livewire.admin.news.category.index', ['data' => $data]);
    }

    public function boldTextSearch($data, $searchKey)
    {
        if (!empty($searchKey)) {
            foreach ($data as $item) {
                $item->name = str_replace(strtolower($searchKey), '<b>' . $searchKey . '</b>', strtolower($item->name));
                $item->admin->name = str_replace(strtolower($searchKey), '<b>' . $searchKey . '</b>', strtolower($item->admin->name));
            }
        }

        return $data;
    }

    public function openModal()
    {
        $this->resetInputFields();
        $this->emit('show');
    }

    public function getCategoryInfo($id)
    {
        $category = CategoryNews::findOrFail($id);
        $this->category_id = $id;
        $this->name = $category->name;
        $this->originName = $category->name;
    }

    public function resetInputFields()
    {
        $this->originName = "";
        $this->title = "";
        $this->name = "";
        $this->category_id = "";
        $this->resetValidation();
    }

    protected function rules()
    {
        return [
            'name' => 'required|max:255|unique:category_news,name,NULL,id,deleted_at,NULL',
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'name' => __('news/newsManager.menu_name.create-category.empty-name'),
        ];
    }


    public function store()
    {
        $this->validate();

        $formData = [
            'name' => $this->name,
            'admin_id' => Auth::id(),
            'slug' => Community::slugify($this->name).'-'.rand(0,100)
        ];

        CategoryNews::create($formData);

        session()->flash('success',__('news/newsManager.menu_name.notification.category.create.success'));
        $this->redirect(route(ERouteName::ROUTE_NEWS_CATEGORY_INDEX));
    }


    public function setIdCategory($id)
    {
        $this->category_id = $id;
    }

    public function compareNames()
    {
        if (!strcmp(trim($this->name), trim($this->originName))) {
            return '';
        }else{
            return '|max:255|unique:category_news,name,NULL,id,deleted_at,NULL';
        }
    }

    public function update()
    {
        $this->validate([
            'name' => 'required|max:255' . $this->compareNames()
        ], [], [
            'name' => __('news/newsManager.menu_name.create-category.empty-name')
        ]);
        $category = CategoryNews::findOrFail($this->category_id);
        $category->update([
            'title' => $this->name,
            'name' => $this->name,
            'slug' => Community::slugify($this->name).rand(0,100)
        ]);
        session()->flash('success',__('news/newsManager.menu_name.notification.category.update.success'));
        $this->redirect(route(ERouteName::ROUTE_NEWS_CATEGORY_INDEX));
    }

    public function delete()
    {
        CategoryNews::destroy($this->category_id);
        session()->flash('success',__('news/newsManager.menu_name.notification.category.delete'));
        $this->redirect(route(ERouteName::ROUTE_NEWS_CATEGORY_INDEX));
    }
}
