<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Topic;
use App\Models\TopicHasUserInfo as Member;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Config;

class TabMember extends BaseLive {

    # basic info for list
    public ?Topic $topic;

    public array $readyMembers;

    protected $listeners = [
        'add-member-event' => 'addMember',
        'validate-data-event' => 'validateData',
        'changed-root-topic-event' => 'setRootTopic',
    ];

    public function setRootTopic($topicId)
    {
        $rootTopic = Topic::query()->with(['members' => function($query) {
            return $query->with('userInfo.academicLevel', 'role', 'research');
        }])->find($topicId);

        $this->readyMembers = (empty($rootTopic)) ? [] : $rootTopic->members->mapWithKeys(function($member) {
            return [
                $member['user_info_id'] => $member
            ];
        })->toArray();
    }

    protected function rules() {
        return [
            'readyMembers' => (($this->topic->exists && $this->topic->members()->get()->count() > 0) ? 'nullable' : 'required')
        ];
    }

    public function mount(Topic $topic) {
        $this->topic = $topic;
        $this->readyMembers = [];
    }

    public function validateData() {
        try {
            $this->validate();
            $this->emitUp('validated-data-event', 'tab-member', $this->readyMembers);
        } catch (\Exception $exception) {
            $this->emitUp('invalid-data-event', 'tab-member', __('data_field_name.topic.error_tab_member'));
            throw $exception;
        }
    }

    public function render() {
        $members = $this->topic->members()->with(['userInfo', 'userInfo.academicLevel', 'role', 'research'])->paginate(Config::get('app.per_page') ?? 25);
        return view('livewire.admin.research.topic.tab-member')
            ->with('members', $members);
    }

    // function này cần tối ưu hoá
    public function addMember($data) {
        $check1 = $check2 = null;
        $check3 = $check4 = null;

        try {
            // kiểm tra nếu đã có chủ nhiệm đề tài
            if($data['role']['order_number'] == 0) {
                $check1 = array_filter($this->readyMembers, fn ($item) => $item['role']['order_number'] == 0);
                $check2 = $this->topic->leader_id;
                if(!empty($check1) || !empty($check2)) {
                    throw new Exception(__('data_field_name.topic.error_exist_leader'));
                }
            }

            // kiểm tra nếu trùng thành viên
            $check3 = $this->readyMembers[$data['user_info_id']] ?? null;
            $check4 = $this->topic->members()->where('user_info_id', $data['user_info_id'])->first();

            if(!empty($check3) || !empty($check4)) {
                throw new Exception(__('data_field_name.topic.error_exist_member'));
            }
            $this->readyMembers[$data['user_info_id']] = $data;
            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.add')]);

        } catch(\Exception $exception) {
            $this->dispatchBrowserEvent('show-toast', ['type' => 'error', 'message' => $exception->getMessage()]);
        }

    }

    public function deleteAvailableMember($userInfoId) {
        if($userInfoId == $this->topic->leader_id) {
            Topic::query()->where('id', $this->topic->id)->update([
                'leader_id' => null
            ]);
        }
        Member::where('user_info_id', $userInfoId)->where('topic_id', $this->topic->id)->delete();
        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('data_field_name.topic.success_delete_member')]);
    }

    public function deleteReadyMember($userInfoId) {
        unset($this->readyMembers[$userInfoId]);
        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('data_field_name.topic.success_delete_member')]);
    }
}
