<?php

namespace App\Http\Livewire\Admin\ResearchProject\Information;

use App\Mail\CreateUserSuccessfully;
use App\Models\Role;
use App\Models\User;
use App\Models\UserInf;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class FormData extends Component
{
    public $isCreate = true;
    public $researchID ;
    public $disableBtn = true ;

    public $currentTab = 'research';

    protected $listeners = [
      'disable-btn'  => 'setDisableBtn'
    ];

    public function setDisableBtn($isDisable){
        $this->disableBtn = $isDisable;
    }

    public function mount($id = null){
        if ($id){
            $this->isCreate = false ;
            $this->researchID = $id;
        }
    }

    public function render()
    {
        return view('livewire.admin.research-project.information.form-data');
    }




    public function setCurrentTab($tab)
    {
        $this->currentTab = $tab;
    }

    public function update(){
        $this->emit('edit-research-project',$this->researchID);
    }

    public function store(){
        $this->emit('save-research-data-information');
    }

    public function toggleShoudEditCode() {
        $this->shouldEditCode = !$this->shouldEditCode;
    }

}
