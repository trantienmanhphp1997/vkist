<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <a href="">{{__('executive/user-info-leave.breadcrumbs.executive-management')}} \ 
                        </a><span>{{__('executive/user-info-leave.breadcrumbs.user-info-leave-management')}}</span>
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">
                {{__('executive/user-info-leave.breadcrumbs.user-info-leave-list')}}
            </h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group search-expertise">
                                <div class="search-expertise">
                                    <input type="text" placeholder="{{__('executive/user-info-leave.placeholder.search')}}" name="search" class="form-control size13"
                                           wire:model.debounce.1000ms="searchTerm"
                                           id='input_vn_name' autocomplete="off">
                                    <span><img src="/images/Search.svg" alt="search"/></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group float-right">
                                @if($checkCreatePermission)
                                    <a class="btn btn-viewmore-news mr0" href="{{route('admin.executive.user-info-leave.create')}}" style="color:white">
                                        <img src="/images/plus2.svg" alt="plus"> {{__('executive/user-info-leave.button.add')}}
                                    </a>
                                @endif
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-general"> 
                                <thead>
                                <tr class="border-radius">
                                    <th scope="col" style="width:10%">{{__('executive/user-info-leave.table_column.code')}}</th>
                                    <th scope="col" style="width:12%">{{__('executive/user-info-leave.table_column.name')}}</th>
                                    <th scope="col" style="width:12%">{{__('executive/user-info-leave.table_column.department')}}</th>
                                    <th scope="col" style="width:12%">{{__('executive/user-info-leave.table_column.position')}}</th>
                                    <th scope="col" style="width:20%">{{__('executive/user-info-leave.table_column.reason')}}</th>
                                    <th scope="col" style="width:12%">{{__('executive/user-info-leave.table_column.leave-date')}}</th>
                                    <th scope="col" style="width:12%">{{__('executive/user-info-leave.table_column.status')}}</th>
                                    <th scope="col" class="text-center" style="width:10%" class="border-radius-right">{{__('executive/user-info-leave.table_column.action')}}</th>
                                </tr>
                                </thead>
                                <div wire:loading class="loader"></div>  
                                <tbody>
                                @forelse($data as $row)
                                    <tr>
                                        <td>{!! boldTextSearch($row->code, $searchTerm) !!}</td>
                                        <td>{!! boldTextSearch($row->fullname,$searchTerm) !!}</td>
                                        <td>{{$row->department->name ?? '' }}</td>
                                        <td>{{$row->position->v_value ?? '' }}</td>
                                        <td>{{$row->reason}}</td>
                                        <td>{{reFormatDate($row->leave_date)}}</td>
                                        <td>{{App\Enums\EUserInfoLeaveStatus::valueToName($row->status)}}</td>
                                        <td class="text-center">
                                            @if($checkDestroyPermission)
                                            <button type="button" style="background-color: white; border:none;" class="btn-sm" title="{{__('common.button.delete')}}"
                                                    data-toggle="modal" wire:click="deleteId({{$row->id}})" data-target="#deleteModal">
                                                <img src="/images/trash.svg" alt="trash">
                                            </button>
                                            @endif
                                            @if($checkEditPermission)
                                            <button type="button" title="{{__('common.button.edit')}}" class="btn-sm border-0 bg-transparent mr-1">
                                                    <a href="{{route('admin.executive.user-info-leave.edit', ['id' => $row->id])}}">
                                                            <img src="/images/edit.png" alt="edit">
                                                    </a>
                                            </button>
                                            @endif
                                        </td>
                                    </tr>
                                    @empty 
                                        <tr class="text-center text-danger"><td colspan="12">{{__('common.message.no_data')}}</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        {{$data->links()}}
                        @include('livewire.common._modalExportFile')
                        @include('livewire.common.modal._modalDelete')

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


