<?php

namespace App\Http\Controllers\Admin\AppraisalBoard;

use App\Http\Controllers\Controller;

class AppraisalBoardController extends Controller
{
    public function index()
    {
        return view('admin.appraisalboard.index');
    }
}
