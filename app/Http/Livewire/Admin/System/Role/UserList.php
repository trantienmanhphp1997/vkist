<?php

namespace App\Http\Livewire\Admin\System\Role;


use App\Enums\ERole;
use App\Http\Livewire\Base\BaseLive;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserList extends BaseLive
{

    public $searchTerm;
    public $filterStatus;
    public $roleId;
    public $userId;
    public $listUserCheck;

    public function mount($roleId)
    {
        $this->roleId = $roleId;
    }

    public function render()
    {
        $query = User::query();
        $term = $this->searchTerm;

        if (!empty($term)) {
            $query->whereRaw("name LIKE '%$term%'");
        }

        $data = $query->with(['info' => function ($query) {
            return $query->with('position');
        }])->orderBy('id', 'DESC')->paginate($this->perPage);

        if (!empty($term)) {
            foreach ($data as $item) {
                $item->name = str_replace($term, '<b>' . $term . '</b>', $item->name);
            }
        }


        $roleUser = DB::table('model_has_roles')->where('role_id', $this->roleId)->get();

        $listIdUser = [];
        if ($roleUser->isNotEmpty()) {
            foreach ($roleUser as $user) {
                $listIdUser[] = $user->model_id;
                $this->userId[$user->model_id] = ERole::STATUS_ON;
            }
        }

        if (!empty($this->listUserCheck)) {
            foreach ($this->listUserCheck as $key => $val) {
                if (empty($val)) {
                    unset($this->userId[$key]);
                }
            }
        }

        return view('livewire.admin.system.role.user-list', ['data' => $data, 'listIdUser' => $listIdUser]);
    }

    public function userClicked()
    {
        $arrUser = $this->userId;
        if (!empty($arrUser)) {
            foreach ($arrUser as $key => $val) {
                $this->listUserCheck[$key] = $val;
            }
        }
    }
}
