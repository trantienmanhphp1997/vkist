<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs">
            @if(checkPermission('admin.asset.category.index'))
                <a href="{{route('admin.asset.category.index')}}">{{ __('menu_management.menu_name.asset-management') }}</a>
            @endif
                 \ <span>{{ __('data_field_name.instrument.asset_instrument_list') }}</span>
            </div> 
        </div>
    </div>

    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{ __('data_field_name.instrument.asset_instrument_list') }}</h3>
                </div>
                <div class="col-md-6">
                    <div class=" float-right">
                        <button class="btn btn-viewmore-news mr0 mrr16" data-toggle="modal" data-target="#exampleModal"><img src="/images/plus2.svg">{{ __('data_field_name.instrument.add') }}</button>
                        <button type="button" class="btn par6 mrr16 btn-header" title="edit"><img src="/images/pent2.svg"></button>
                        <button type="button" class="btn par6  btn-header" title="delete"><img src="/images/trash.svg"></button>
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block">
                                <input type="text" class="form-control size13"
                                           placeholder="{{__('data_field_name.instrument.search')}}"
                                           wire:model.debounce.1000ms="searchTerm" autocomplete="off">
                                    <span>
                                        <img src="/images/Search.svg"/>
                                    </span>  
                                </div>                                      
                            </div>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group float-right">
                                <button class="btn btn-viewmore-news mr0 mrr16 bg-green"><img src="/images/User.svg"> {{__('data_field_name.instrument.my_work')}} </button>
                                <button class="btn btn-viewmore-news mr0 bg-oranges"><img src="/images/work-group.svg"> {{__('data_field_name.instrument.group_work')}} </button>
                                  <div class="dropdown navbar manipulation">
                                    <a class="btn btn-gray dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{__('data_field_name.instrument.cmd.instrument_cmd')}}
                                    </a>
                                  
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                      <a class="dropdown-item" href="#"><img src="/images/Minus.svg">{{__('data_field_name.instrument.cmd.report_broken')}}</a>
                                      <a class="dropdown-item" href="#"><img src="/images/Warning.svg">{{__('data_field_name.instrument.cmd.report_lost')}}</a>
                                      <a class="dropdown-item" href="#"><img src="/images/Export.svg">{{__('data_field_name.instrument.cmd.allocate')}}</a>
                                      <a class="dropdown-item" href="#"><img src="/images/Import.svg">{{__('data_field_name.instrument.cmd.recall')}}</a>
                                      <a class="dropdown-item" href="#"> <img src="/images/Broom.svg">{{__('data_field_name.instrument.cmd.maintain')}}</a>
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                            <tr class="border-radius">
                                <th  scope="col" class="border-radius-left text-left w50">
                                    <label class="box-checkbox checkbox-analysis ">
                                        <input checked="checked" type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </th>
                                
                                <th  scope="col" >{{__('data_field_name.instrument.code')}}</th>
                                <th  scope="col" >{{__('data_field_name.instrument.name')}}</th>
                                <th  scope="col" >{{__('data_field_name.instrument.instrument_id')}}</th>
                                <th  scope="col" >{{__('data_field_name.instrument.department_id')}}</th>
                                <th  scope="col" >{{__('data_field_name.instrument.unit')}}</th>
                                <th  scope="col" >{{__('data_field_name.instrument.quantity')}}</th>
                                <th  scope="col" >{{__('data_field_name.instrument.price')}}</th>
                                <th  scope="col" class="border-radius-right min-width">{{__('data_field_name.instrument.total_price')}}</th>
                            </tr>
                            </thead>
                            <tbody> 
                            @forelse ($data as $val)
                            <tr >
                                <td class="w50">
                                    <label class="box-checkbox checkbox-analysis ">
                                        <input checked="checked" type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </td>
                                <td >{{ $val->code }}</td>
                                <td >{{ $val->name }}</td> 
                                <td >{{ $val->instrument_id }}</td>
                                <td >{{ $val->department_id }}</td>
                                <td >{{ $val->unit }}</td>
                                <td >{{ $val->quantity }}</td>
                                <td >{{ $val->price }}</td>
                                <td >{{ $val->quatity * $val->price }}</td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="6">{{__('common.message.empty_search')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                            
                        </table>
                    </div>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="text-center">{{__('data_field_name.instrument.create_new')}}</h4>
                    <div class="bd-attached add-property">
                        <div class="attached-center">
                            <input type="file" class="custom-file-input">
                            <span class="file">{{__('data_field_name.instrument.upload_data')}}<img src="/images/file2.svg" height="24"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{__('data_field_name.instrument.note')}}</label>
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="group-btn2 text-center ">
                    <button type="submit" class="btn btn-cancel">{{__('common.button.cancel')}}</button>
                    <button type="submit" class="btn btn-save">{{__('common.button.save')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
   