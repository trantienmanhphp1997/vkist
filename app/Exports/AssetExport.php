<?php

namespace App\Exports;

use App\Models\AssetAllocatedRevoke;
use App\Models\UserInf;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
class AssetExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $getIdSelected;

    function __construct($checked) {
        $this->getIdSelected = $checked;
    }

    public function collection()
    {
        $query = AssetAllocatedRevoke::query();
        if(empty($this->getIdSelected)){
            $list = $query->with('asset');
        }else{
            $list = $query->where('status',\App\Enums\EAssetStatus::REPORT_LOST)->with('asset');
        }
        $key=1;
        foreach ($list as $value){
            $value->id=$key++;
        }
        return $list;
    }
    public function headings():array
    {
        return
            [
                'STT',
                'MÃ TÀI SẢN',
                'SỐ BIÊN BẢN',
                'TÊN TÀI SẢN',
//                'LOẠI TÀI SẢN',
//                'ĐƠN VỊ TÍNH',
//                'TÌNH TRẠNG',
//                'SỐ SERIAL',
//                'NGÀY BÁO MẤT'
            ];
    }

    public function map($list): array
    {
        return [
            $list->key,
//            $list->asset->code,
            $list->number_report,
            $list->asset->name,
//            $list->asset->category->name,
//            $list->asset->unit->v_value,
//            $list->status == 4 ? 'Đã mất' : '',
//            $list->asset->series_number,
            $list->implementaion_date,

        ];
    }

    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('D1:G1');
            $event->sheet->setCellValue('D1','DANH SÁCH TÀI SẢN BỊ MẤT');
            $event->sheet->getStyle('D1:G1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:U3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        return 'DANH SÁCH TÀI SẢN MẤT';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
