@extends('layouts.master')
<title>{{__('executive/my-day-off.title_edit')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.executive.my-day-off.apply-for-leave-edit', ['Request_leave_id'=>$id])
@endsection
@section('js')
@endsection