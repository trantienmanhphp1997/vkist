<?php

namespace App\Http\Livewire\Admin\Research\Ideal;

use App\Models\Ideal;
use App\Models\UserInf;
use App\Component\Recursive;
use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchCategory;
use App\Enums\EResearchCategoryType;

class IdealList extends BaseLive {

    public $searchTerm;
    public $filterStatus;
    public $filterProposer;
    public $deleteId='';
    public $research_field;
    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render() {
        $query = Ideal::query();
        $status = $this->filterStatus;
        $proposer = $this->filterProposer;

        if ($status != null) {
            $query->where('status', $status);
        }

        if (strlen($proposer) && $proposer != null) {
            $query->where('proposer', $proposer);
        }

        if (strlen($this->searchTerm)) {
            $query->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }

        if (!empty($this->research_field)) {
            $query->where('research_field_id', $this->research_field);
        }

        $query->with('researchField');

        $data = $query->orderBy('id','DESC')->paginate($this->perPage);

        $userList = UserInf::get();
        foreach ($data as $item) {
            if (is_numeric($item->proposer)) {
                $item->proposer = !empty(UserInf::findOrFail($item->proposer)) ? UserInf::findOrFail($item->proposer)->fullname : null;
            }
        }
        $researchFieldList = ResearchCategory::where('type', '=', EResearchCategoryType::FIELD)->get();
        return view('livewire.admin.research.ideal.ideal-list',['data' => $data, 'userList' => $userList, 'researchField' => $researchFieldList]);

    }
    //delete
    public function deleteId($id) {
        $this->deleteId=$id;
    }
    public function delete() {
        Ideal::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }

}
