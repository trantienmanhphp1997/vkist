<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_fee', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('danh sach loai chi phi');
            $table->string('code', 255)->nullable()->comment('ma chi phi');
            $table->string('name', 500)->nullable()->comment('Ten chi phi');
            $table->tinyInteger('type', 48)->nullable()->comment('Loai hinh theo doi');            
            $table->string('note', 1000)->nullable()->comment('Ten chi phi');
            $table->tinyInteger('status')->nullable()->comment('cong tac phi');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->string('accounting_code', 48)->nullable()->comment('Ma ke toan');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_fee');
    }
}
