<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 500)->comment('Tieu de chuyen muc');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });

        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 500)->comment('Tieu de bai viet');
            $table->string('image_path');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->tinyInteger('status')->default(0)->comment('0: tin nhap; 1: tin da dang ; 2: tin can phe duyet ; 3:tin duoc phe duyet');
            $table->tinyInteger('published')->default(0)->comment('0: dang khong hien thi ; 1: dang hien thi');
            $table->text('description')->comment('noi dung bai viet');
            $table->text('content')->nullable()->comment('Noi dung tom tat');
            $table->string('slug',500)->unique();
            $table->unsignedBigInteger('category_news_id')->nullable()->comment('Map voi category_news (chua dung)');
            $table->foreign('category_news_id')->references('id')->on('category_news');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_news');
        Schema::dropIfExists('news');
    }
}
