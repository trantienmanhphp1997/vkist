<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;
use App\Models\UserInf;
use App\Models\Contract;
use App\Models\UserWorkProcess;
use Illuminate\Support\Carbon;
use App\Models\UserInfoLeave;
use App\Models\SystemConfig;
use App\Enums\ESystemConfigType;
class UserWorkingSeniority extends Component
{
    public $user_info_id;

    public function render()
    {   
        $systemConfig = SystemConfig::where('type', ESystemConfigType::TYPE_IMPORT_SALARY)->first();
        $contract = Contract::where('user_info_id', $this->user_info_id)->first();
        $user = UserInf::where('id', $this->user_info_id)->first();
        $leaves = $user->resquestLeaves;
        $dayLeave = 0;
        
        if (!empty($systemConfig) && isset($systemConfig->content['seniority_does_not_include_vacation_day']) && !$systemConfig->content['seniority_does_not_include_vacation_day']['value']) {
            foreach($leaves as $item)  {
                $dayLeave += Carbon::parse($item->end_date)
                ->diffInDays(Carbon::parse($item->start_date));
            }
        }
        $userInfoLeave = UserInfoLeave::where('status', 1)->where('user_info_id', $this->user_info_id)->first();
        $month = null;
        $day = null;
        $year = null;
        if (!empty($userInfoLeave)) {
            if (!empty($contract && $userInfoLeave->leave_date)) {
               $year = (Carbon::parse($contract->start_date)->diffInDays(Carbon::parse($userInfoLeave->leave_date))-$dayLeave)/365;
               if ($year > 1) {
                    $floatYear = $year - (int)$year;
                    $year2 = $floatYear * 360;
                    $month = $year2 / 30;
                    $floatYear2 = $year2 - (int)$year2;
                    $day1 = $floatYear2 * 30;
                    if ($day1 > 15) {
                        $month ++;
                    }
                    
                    $year = (int)$year . ' ' . __('common._year') . ' ' . (int)$month . ' ' . __('common._month');
                } else {
                    $year = substr($year, 0, 3) . ' ' . __('common._year');
                }
               $month = round((Carbon::parse($userInfoLeave->leave_date)->diffInDays(Carbon::parse($contract->start_date))-$dayLeave)/30, 1) . ' ' . __('common._month');
               $day = round((Carbon::parse($userInfoLeave->leave_date)->diffInDays(Carbon::parse($contract->start_date))-$dayLeave)/30) . ' ' . __('common.time.day');
            } else {
                $year = __('common.empty');
                $month = __('common.empty');
                $day = __('common.empty');
            }
        } else {
            if (!empty($contract)) {
                $year = (Carbon::parse($contract->start_date)->diffInDays(Carbon::parse($contract->end_date))-$dayLeave)/365;
                if ($year > 1) {
                    $floatYear = $year - (int)$year;
                    $year2 = $floatYear * 360;
                    $month = $year2 / 30;
                    $floatYear2 = $year2 - (int)$year2;
                    $day1 = $floatYear2 * 30;
                    if ($day1 > 15) {
                        $month ++;
                    }
                    
                    $year = (int)$year . ' ' . __('common._year') . ' ' . (int)$month . ' ' . __('common._month');
                } else {
                    $year = round($year, 1) . ' ' . __('common._year');
                }
                $month = round((Carbon::parse($contract->end_date)->diffInDays(Carbon::parse($contract->start_date))-$dayLeave)/30, 1) . ' ' . __('common._month');
                $day = round(Carbon::parse($contract->end_date)->diffInDays(Carbon::parse($contract->start_date))-$dayLeave) . ' ' . __('common.time.day');
            } else {
                $year = __('common.empty');
                $month = __('common.empty');
                $day = __('common.empty');
            }
        }
        return view('livewire.admin.user.user-working-seniority',[
            'contract' => $contract,
            'user' => $user,
            'userInfoLeave' => $userInfoLeave,
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'systemConfig' => $systemConfig
        ]);
    }
}
