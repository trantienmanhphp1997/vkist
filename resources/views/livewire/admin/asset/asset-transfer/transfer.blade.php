<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="{{route('admin.asset.allocated-revoke.index')}}">{{__('menu_management.menu_name.asset-management')}}</a> \ <span>{{__('menu_management.menu_name.transfer')}}</span></div>
        </div>
    </div>
    <div class="row bd-border inner-tab">
        <div class="col-md-12">
            <div class="detail-task information box-idea">
                <h4>{{__('data_field_name.allocated-revoke.transfer_form_title')}}</h4>
                <h3 class="title-asset pt-24">{{__('data_field_name.allocated-revoke.general_information')}}</h3>
                <div class="box-calendar col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__('data_field_name.allocated-revoke.number_report')}} <span class="text-danger">*</span></label>
                                <input type="text" wire:model.lazy="number_report" class="form-control" placeholder="">
                                @error('number_report')
                                  <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__('data_field_name.allocated-revoke.transfer_date')}}</label>
                                @include('layouts.partials.input._inputDate', ['input_id' => 'transferDay', 'place_holder'=>'', 'default_date' => is_null($day) ? date('Y-m-d') : $day ])
                                @error('day')
                                  <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.allocated-revoke.reason_transfer')}}</label>
                                <textarea wire:model.lazy="reason" class="form-control" placeholder=""></textarea>
                                @error('reason')
                                  <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{ __('data_field_name.asset.transfer_type') }} <span class="text-danger">*</span></label>
                                <select class="form-control" wire:model.lazy="transferType" wire:ignore>
                                    <option value="">{{__('common.select-box.choose')}}</option>
                                    @foreach (\App\Enums\EAssetTransferType::getList() as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                                @error('transferType')
                                    <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                @if (!empty($transferType))
                    <h3 class="title-asset pt-24" style="text-transform: uppercase;">{{__('data_field_name.allocated-revoke.transfer')}}</h3>
                    <div class="box-calendar col-md-12">
                        @if ($transferType == \App\Enums\EAssetTransferType::BY_INDIVIDUAL)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.allocated-revoke.from_user')}} <span class="text-danger">*</span></label>
                                        <div class="form-asset">
                                            <select id="from-user-id"  class="form-control form-control-lg" wire:model.lazy="fromUserId">
                                                <option value="">{{__('common.select-box.choose')}}</option>
                                                @foreach($modalUserList as $item)
                                                    <option value="{{$item->id}}">{{$item->fullname}}</option>
                                                @endforeach
                                            </select>
                                            <span><img src="/images/cham.svg" alt=""></span>
                                        </div>
                                        @error('fromUserId')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.allocated-revoke.to_user')}} <span class="text-danger">*</span></label>
                                        <div class="form-asset">
                                            <select id="to-user-id" class="form-control form-control-lg" wire:model.lazy="toUserId">
                                                <option value="">{{__('common.select-box.choose')}}</option>
                                                @foreach($modalUserList as $item)
                                                <option value="{{$item->id}}">{{$item->fullname}}</option>
                                                @endforeach
                                            </select>
                                            <span><img src="/images/cham.svg" alt=""> </span>
                                        </div>
                                        @error('toUserId')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        @elseif ($transferType == \App\Enums\EAssetTransferType::BY_DEPARTMENT)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.allocated-revoke.from_department')}} <span class="text-danger">*</span></label>
                                        <div class="form-asset">
                                            <select id="from-department-id" class="form-control form-control-lg" wire:model.lazy="fromDepartmentId">
                                                <option value="">{{__('common.select-box.choose')}}</option>
                                                @foreach($modalDepartmentList as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            <span><img src="/images/cham.svg" alt=""></span>
                                        </div>
                                        @error('fromDepartmentId')
                                            <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('data_field_name.common_field.manager')}}</label>
                                        <input class="form-control" disabled value="{{ $department1Leader }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.allocated-revoke.to_department')}} <span class="text-danger">*</span></label>
                                        <div class="form-asset">
                                            <select id="to-department-id" class="form-control form-control-lg" wire:model.lazy="toDepartmentId">
                                                <option value="">{{__('common.select-box.choose')}}</option>
                                                @foreach($modalDepartmentList as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            <span><img src="/images/cham.svg" alt=""> </span>
                                        </div>
                                        @error('toDepartmentId')
                                            <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('data_field_name.common_field.manager')}}</label>
                                        <input class="form-control" disabled value="{{ $department2Leader }}">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                @endif
                <h3 class="title-asset pt-24" style="text-transform: uppercase;">{{__('data_field_name.allocated-revoke.transfered_assets')}}<span class="text-8"> ({{count($arrAssetChoosed)}})</span> <span class="text-danger">*</span></h3>
                <div class="form-group">
                    <button class="btn btn-asset" wire:click="openModalAsset" style="position: unset;"><img src="/images/Plus3.svg" alt="plus"> {{__('data_field_name.allocated-revoke.quick_pick')}}  </button>
                    @error('arrAssetChoosed')
                      <div class="text-danger mt-1">{{$message}}</div>
                    @enderror
                </div>
                @if(count($arrAssetChoosed) > 0)
                    <table class="table">
                        <thead>
                            <tr class="border-radius">
                                <th scope="col" class="text-center">
                                    {{__('data_field_name.allocated-revoke.asset_code')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('data_field_name.allocated-revoke.asset_name')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($modalAssetList as $index => $row)
                                @if(in_array($row['id'], $arrAssetChoosed))
                                    <tr>
                                        <td class="text-center">{!! $row['code'] !!}</td>
                                        <td class="text-center">{!! $row['name'] !!}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                @endif
                <h3 class="title-asset pt-24" style="text-transform: uppercase;">{{__('data_field_name.allocated-revoke.attachments')}}</h3>
                <div class="form-group upload-file">
                      <div class="bd-attached">
                          <div class="attached-center">
                              <input type="file" onchange="uploadFile(this, @this)" class="custom-file-input">
                              <span class="file">{{__('data_field_name.allocated-revoke.invoice_attached')}}<img src="/images/file2.svg" alt="file" height="24"></span>
                          </div>
                      </div>
                      @if(!empty($errorFile))
                          <div class="text-danger mt-1">{{$errorFile}}</div>
                      @endif
                      @error('file')
                        <div class="text-danger mt-1">{{$message}}</div>
                      @enderror
                      <div class="text-danger upload-error"></div>
                </div>
                <div class="row">
                  @if(!empty($file))
                      @foreach ($file as $index => $item)
                        <div class="form-group col-md-4">
                            <div class="attached-files">
                                <img src="/images/File.svg" alt="file">
                                <div class="content-file">
                                    <p class="kb" style="word-break: break-all;">{{ $item->getClientOriginalName() }}</p>
                                    <p class="kb">{{$item->getSize() / 1024}} KB</p>
                                </div>
                                <span>
                                <button wire:click.prevent="deleteFileUpload({{$index}})" style="border: none;background: white" type="button"><img
                                        src="/images/close.svg" alt="close"/>
                                </button>
                            </span>
                            </div>
                        </div>
                      @endforeach
                    @endif
              </div>
                <div class="group-btn2 text-center group-btn ">
                    <a href="{{route('admin.asset.transfer.index')}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                    @if($disable)
                        <button type="button" disabled="" class="btn btn-cancel btn-save bg-primary2" wire:click="saveData(false)">{{__('data_field_name.allocated-revoke.btn_transfer')}}</button>
                        @if(checkRoutePermission('download'))
                            <button type="button" disabled="" wire:click="saveData(true)" class="btn btn-save">{{__('data_field_name.allocated-revoke.btn_transfer_and_print')}}</button>
                        @endif
                    @else
                        <button type="button" class="btn btn-cancel btn-save bg-primary2" wire:click="saveData(false)">{{__('data_field_name.allocated-revoke.btn_transfer')}}</button>
                        @if(checkRoutePermission('download'))
                            <button type="button" wire:click="saveData(true)" class="btn btn-save">{{__('data_field_name.allocated-revoke.btn_transfer_and_print')}}</button>
                        @endif
                    @endif
                  </div>
                </div>
            </div>
            @include('livewire.admin.asset.asset-transfer.modal-asset')
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(() => {
        window.livewire.on('showModalAsset', () => {
          $('#taisan').modal('show');
        });
        window.livewire.on('doExportTransfer', () => {
          window.livewire.emit('exportTransfer');
        });
        window.livewire.on('doResetDataTransfer', () => {
          window.livewire.emit('resetDataTransfer');
        });
    });
    function uploadFile(input, proxy = null) {
        let file = input.files[0];
        if (!file || !proxy) return;
        let $error = $(input).parents('.box-user').find('.upload-error');
        if(file.size/(1024*1024) >= 255) {
            $error.html("{{ __('notification.upload.maximum_size', ['value' => 255]) }}");
            return;
        }

        let mimes = {!! json_encode(['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z']) !!};
        let extension = file.name.split('.').pop();
        if(!mimes.includes(extension)) {
            $error.html("{{ __('notification.upload.mime_type', ['values' => implode(', ', ['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z'])]) }}");
            return;
        }

        proxy.upload('file', file, (uploadedFilename) => {

        }, () => {
            console.log(error);
        }, (event) => {

        });
    }
</script>
