@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.research_cost.list_cost') }}</title>
@endsection
@section('homeLeft')
@include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    @livewire('admin.research.cost.topic-fee-detail-list', ['topicFeeId' => $data->id])
@endsection
