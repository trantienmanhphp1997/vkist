<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Facades\Config;


class UserInf extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    protected $table = "user_info";
    protected $guarded=['*'];
    protected $fillable = [
        'fullname',
        'code',
        'address',
        'phone',
        'birthday',
        'gender',
        'married',
        'email',
        'ethnic_id',
        'religion_id',
        'nationality_id',
        'id_card',
        'issued_date',
        'issued_place',
        'passport_number',
        'passport_date',
        'passport_place',
        'user_info_id',
        'note',
        'department_id',
        'position_id',
        'telephone',
        'ext',
        'avatar',
        'leave_day_last_year',
        'leave_day_remaining',
        'leave_day_current_year',
        'seniority',
    ];

    protected $casts = [
        'birthday' => 'datetime:Y-m-d',
        'passport_date' => 'datetime:Y-m-d',
        'issued_date' => 'datetime:Y-m-d',
        'start_working_date' => 'datetime:Y-m-d',
        'contract_sign_date' => 'datetime:Y-m-d',
        'retire_date' => 'datetime:Y-m-d'
    ];

    public function workProcesses() {
        return $this->hasMany(UserWorkProcess::class, 'user_info_id');
    }

    public function department() {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function position() {
        return $this->belongsTo(MasterData::class, 'position_id');
    }

    public function account() {
        return $this->hasOne(User::class, 'user_info_id');
    }

    public function academicLevel() {
        return $this->belongsTo(MasterData::class, 'academic_level_id');
    }
    private static $searchable = [
        'fullname',
        'code',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
    public function academics(){
        return $this->hasMany(UserAcademic::class,'academic_level_id');
    }

    public function topics() {
        return $this->belongsToMany(Topic::class, 'topic_has_user_info', 'user_info_id', 'topic_id');
    }
    public function files() {
        return $this->hasMany(File::class,'model_id')->where('files.type','=',Config::get('common.type_upload.UserWorkingProcess'));
    }
    public function resquestLeaves(){
        return $this->hasMany(RequestLeave::class,'user_info_id');
    }

    public function topicHasUserInfos() {
        return $this->hasMany(TopicHasUserInfo::class, 'user_info_id');
    }
    public function contract(){
        return $this->hasOne(Contract::class,'user_info_id');
    }
    public function leave()
    {
        return $this->hasMany(RequestLeave::class, 'user_info_id')
                    ->where('request_leave.status',\App\Enums\ERequestLeave::STATUS_APPROVAL)
                    ->whereDate('request_leave.start_date','<=', Carbon::now()->toDateTimeString())
                    ->whereDate('request_leave.end_date','>=', Carbon::now()->toDateTimeString())
                    ->orderBy('id', 'DESC')->limit(1)
            ;
    }
}
