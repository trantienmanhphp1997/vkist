<?php

namespace Database\Factories;

use App\Models\ShoppingDevice;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShoppingDeviceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ShoppingDevice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'shopping_id' => null,
            'name' => $this->faker->colorName,
            'quantity' => $this->faker->numberBetween(1, 100),
        ];
    }
}
