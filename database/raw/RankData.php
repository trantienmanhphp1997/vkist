<?php
return [
    [
        'type' => '6',
        'v_key' => 'rank',
        'v_value' => 'Xuất sắc',
        'order_number' => '1',
    ],
    [
        'type' => '6',
        'v_key' => 'education_background',
        'v_value' => 'Giỏi',
        'order_number' => '2',
    ],
    [
        'type' => '6',
        'v_key' => 'education_background',
        'v_value' => 'Khá',
        'order_number' => '3',
    ],
    [
        'type' => '6',
        'v_key' => 'education_background',
        'v_value' => 'Trung Bình Khá',
        'order_number' => '4',
    ],
    [
        'type' => '6',
        'v_key' => 'education_background',
        'v_value' => 'Trung Bình',
        'order_number' => '5',
    ],
];
