<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCompensationToAssetAllocatedRevokeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->bigInteger('compensation_value')->nullable()->comment('Giá trị đền bù');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->dropColumn('compensation_value');
        });
    }
}
