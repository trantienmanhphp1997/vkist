<div class="col-md-10 col-xl-11 box-detail box-user">
    <style>
        .report-main-text {
            padding: 70px 40px 10px;
        }
        .download-report-btn{
            margin-bottom: 60px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('common.listNameFunction.asset')}} </a> \
                <span>{{__('research/report.title')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('research/report.title')}}</h3>
        </div>
        <div class="col-md-12">
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-3 px-8">

                        </div>
                        <div class="col-md-12 bd-border">
                            <div class="row p-2">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{__('research/report.select.type')}}</label>
                                        <select class="form-control size13" wire:model.lazy="reportNumber">
                                            {{--                                            <option value="">--{{__('research/report.select.chose')}}--</option>--}}
                                            <option value="1">{{__('research/report.report1')}}</option>
                                            <option value="3">{{__('research/report.report2')}}</option>
                                            <option value="2">{{__('research/report.report3')}}</option>
                                            <option value="4">{{__('research/report.report4')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class=" @if ($reportNumber == 1 || $reportNumber == 2) col-md-2 @else col-md-4 @endif">
                                    <div class="form-group">
                                        <label>{{__('research/report.select.department')}}</label>
                                        <select class="form-control size13" wire:model.lazy="department_id">
                                            <option value="">--vkist--</option>
                                            @foreach($departments as $department)
                                                <option value="{{$department->id}}">{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class=" @if ($reportNumber == 1 || $reportNumber == 2) col-md-2 @else d-none @endif">
                                    <div class="form-group">

                                        <span
                                            class="@if ($reportNumber == 1 || $reportNumber == 2) d-block @else d-none @endif">
                                             <label>{{__('research/report.select.start')}}</label>
                                            @include('layouts.partials.input._inputDate', ['input_id' => 'start_date','name'=>'start_date','place_holder' => __('data_field_name.research_plan.start_time') ,'default_date' => now()? date('Y-m-d') : ''])
                                        </span>
                                    </div>
                                </div>
                                <div class=" @if ($reportNumber == 1 || $reportNumber == 2) col-md-2 @else d-none @endif">
                                    <span
                                        class="@if ($reportNumber == 1 || $reportNumber == 2) d-block @else d-none @endif"><label>{{__('research/report.select.end')}}</label>
                                    @include('layouts.partials.input._inputDate', ['input_id' => 'end_date','wire_model'=>'end_date','name'=>'end_date','place_holder' => __('data_field_name.research_plan.start_time') ,'disable_input'=>true])
                                                                        </span>
                                </div>
                                <script>
                                    $('input[name="start_date"]').on('change', (e) => {
                                        Livewire.emit('set-start-time',
                                            document.getElementById('start_date').value,
                                        );
                                    });
                                    $('input[name="end_date"]').on('change', (e) => {
                                        Livewire.emit('set-end-time',
                                            document.getElementById('end_date').value,
                                        );
                                    });
                                </script>
                                @if($checkCreatePermission)
                                    <div class="col-md-2" style="padding-top: 2rem;">
                                        <div class="form-group">
                                            <a href="#"
                                               class="btn btn-viewmore-news mr0 ">
                                                <img src="/images/plus2.svg" alt="plus">{{__('research/report.select.process')}}
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div wire:loading class="loader" style="z-index: 10"></div>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade {{$reportNumber == 1 ? 'active show' : ''}}" id="report1"
                             role="tabpanel" aria-labelledby="nav-home-tab">
                            @include('admin.research.report.assetIncreateDecreaseReport')
                        </div>
                        <div class="tab-pane fade {{$reportNumber == 2 ? 'active show' : ''}}" id="report2"
                             role="tabpanel" aria-labelledby="nav-profile-tab">
                            @include('admin.research.report.assetMovementReport')

                        </div>
                        <div class="tab-pane fade {{$reportNumber == 3 ? 'active show' : ''}}" id="report3"
                             role="tabpanel" aria-labelledby="nav-home-tab">
                            @include('admin.research.report.assetSumaryReport')
                        </div>
                        <div class="tab-pane fade {{$reportNumber == 4 ? 'active show' : ''}}" id="report4"
                             role="tabpanel" aria-labelledby="nav-profile-tab">
                            @include('admin.research.report.assetSumaryResearchReport')
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="exportModal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{__('notification.member.warning.warning')}}</h4>
                    {{__('notification.member.warning.Do you want to export excel file?')}}
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                    <button type="button" wire:click="exportData" class="btn btn-save"
                            data-dismiss="modal">{{__('common.button.export_file')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
