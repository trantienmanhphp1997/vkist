<?php

namespace App\Http\Controllers\admin\research\plan;

use App\Http\Controllers\Controller;
use App\Models\ResearchPlan;
use App\Models\Topic;
use App\Models\Task;
use Illuminate\Http\Request;

class ResearchPlanController extends Controller
{
    public function index(){
       return view('admin.research.plan.index');
    }
    public function edit($id){
        $researchPlan = ResearchPlan::findOrFail($id)->load('topic');
        return view('admin.research.plan.edit', compact('researchPlan'));
    }

}
