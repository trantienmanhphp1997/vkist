<div class="col-md-10 col-xl-11 box-detail box-user">
	<div class="row">
		<div class="col-md-12">
				<div  class="breadcrumbs"><a href="{{route('admin.research.ideal.index')}}">{{__('research/ideal.breadcrumbs.research-ideal-management')}}</a> \ <span>{{__('menu_management.menu_name.survey')}}</span></div>
		</div>
	</div>
	<div class="row bd-border">
		<div class="col-md-12">
			<h3 class="title">{{ __('data_field_name.request-survey-list.title') }}</h3>
			<div class="information">
				<div class="inner-tab pt0">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-3 px-8">
									<div class="search-expertise inline-block mb-24 w-100 ">
										<input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control w-100" placeholder="{{__('research/ideal.filter.input.placeholder.search')}}">
										<span>
											<img src="/images/Search.svg" alt="search"/>
										</span>
									</div>
								</div>
								<div class="col-md-2  px-8">
									<div wire:ignore class="w-100 inline-block position-relative">
										<select  name="proposer" class="size13 form-control" wire:model.lazy="field">
											<option value=''>{{ __('data_field_name.request-survey-list.field') }}</option>
											@foreach($field_data as $item)
												<option value="{{$item->id}}">{{$item->v_value}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-2  px-8">
									<div wire:ignore class="w-100 inline-block mb-24 w-xs-100">
										<select wire:model.lazy="status" class="size13 form-control form-control-lg ">
											<option value=''>
												{{__('research/ideal.filter.select-box.option.status')}}
											</option>
											<option value="{{\App\Enums\ESurveyRequestStatus::NOT_STARTED}}">
												{{__('data_field_name.request-survey-list.not_started')}}
											</option>
											<option value="{{\App\Enums\ESurveyRequestStatus::IN_PROGRESS}}">
												{{__('data_field_name.request-survey-list.in_progress')}}
											</option>
											<option value="{{\App\Enums\ESurveyRequestStatus::DONE}}">
												{{__('data_field_name.request-survey-list.done')}}
											</option>
										</select>
									</div>
								</div>
								<div class="col-md-2  px-8">
									<div wire:ignore class="w-100 inline-block mb-24 w-xs-100">
										<select wire:model.lazy="result" class="size13 form-control form-control-lg ">
											<option value=''>
												{{ __('data_field_name.request-survey-list.result') }}
											</option>
											<option value="{{\App\Enums\ESurveyRequestResult::DEFAULT}}">
												{{ __('data_field_name.request-survey-list.default') }}
											</option>
											<option value="{{\App\Enums\ESurveyRequestResult::STANDARD}}">
												{{ __('data_field_name.request-survey-list.standard') }}
											</option>
											<option value="{{\App\Enums\ESurveyRequestResult::SUBSTANDARD}}">
												{{ __('data_field_name.request-survey-list.substandard') }}
											</option>
										</select>
									</div>
								</div>
								<div class="col-md-3 px-0">
									@if($checkCreatePermission)
										<div class="">
											<a href="{{route('admin.research.survey.create')}}" class="btn btn-viewmore-news mr0 float-right mb-24">
												<img src="/images/plus2.svg" alt="plus">{{ __('data_field_name.request-survey-list.create') }}
											</a>
										</div>
									@endif
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-general">
							<thead>
								<tr class="border-radius">
									<th scope="col" class="border-radius-left">
										{{ __('data_field_name.request-survey-list.name') }}
									</th>
									<th scope="col">
										{{ __('data_field_name.request-survey-list.description') }}
									</th>
									<th scope="col" class="text-center">
										{{ __('data_field_name.request-survey-list.field') }}
									</th>
									<th scope="col" class="text-center">
										{{ __('data_field_name.request-survey-list.perfomer') }}
									</th>
									<th scope="col" class="text-center">
										{{ __('data_field_name.request-survey-list.status') }}
									</th>
									<th scope="col" class="text-center">
										{{ __('data_field_name.request-survey-list.result') }}
									</th>
									<th scope="col" class="border-radius-right">
										{{__('research/ideal.table_column.action')}}
									</th>
								</tr>
							</thead>
							<div wire:loading class="loader"></div>
							<tbody class="approve">
								@foreach($data as $row)
									<tr>
										<td lass="text-center">
											{!! boldTextSearch($row['name'], $searchTerm) !!}
										</td>
										<td>{!! boldTextSearch($row['content'], $searchTerm) !!}</td>
										<td class="text-center">{!! $row['field'] !!}</td>
										<td class="text-center" data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $row['perfomer'] }}">
                                            {!! strLimit($row['perfomer'], 30) !!}
                                        </td>
										<td class="text-center">
											@if($row['status'] == App\Enums\ESurveyRequestStatus::NOT_STARTED)
												<div class="btn yellow">
													{!! $row['statusStr'] !!}
												</div>
											@elseif($row['status'] == App\Enums\ESurveyRequestStatus::IN_PROGRESS)
												<div class="in-process select-status-project" style="border: unset; border-radius: 4px;">
													{!! $row['statusStr'] !!}
												</div>
											@else
												<div class="btn blue">
													{!! $row['statusStr'] !!}
												</div>
											@endif
										</td>
										<td class="text-center">{!! $row['result'] !!}</td>
										<td>
											@php $id = $row['id']; @endphp
											@include('layouts.partials.button._edit')
											@if($checkDestroyPermission)
												<button type="button" class="btn par6" title="{{__('common.button.delete')}}" wire:click="$set('deleteId', {{$row['id']}})" data-target="#exampleDelete" data-toggle="modal">
													<img src="/images/trash.svg" alt="trash">
												</button>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@if(count($data) > 0)
						{{$data->links()}}
					@else
						<div class="title-approve text-center">
							<span>{{__('common.message.empty_search')}}</span>
						</div>
					@endif
					@include('livewire.common._modalDelete')
				</div>
			</div>
		</div>
	</div>
</div>
