<?php

namespace App\Notifications;

use App\Enums\ENotificationType;
use Illuminate\Database\Eloquent\Model;

class WorkResultNotification extends BaseNotification
{

    public Model $model;
    public string $action;

    public function __construct(Model $model, string $action)
    {
        $this->type = ENotificationType::WORK_RESULT;
        $this->model = $model;
        $this->action = $action;
    }

    protected function message($notifiable)
    {
        return [
            'template_name' => 'notification.user.work_result',
            'model' => [
                'id' => $this->model->id,
                'type' => get_class($this->model),
                'name' => $this->model->name,
                'code' => $this->model->code
            ],
            'action' => $this->action
        ];
    }
}
