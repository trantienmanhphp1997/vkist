<?php

namespace App\Http\Controllers\Admin\Research\Cost;

use App\Enums\ETopicSource;
use App\Enums\ETopicStatus;
use App\Enums\ETopicCheckSendFile;
use App\Enums\ETopicType;
use App\Enums\ETypeUpload;
use App\Http\Controllers\Controller;
use App\Models\Audit;
use App\Models\File;
use App\Models\MasterData;
use App\Models\ResearchCategory;
use App\Models\Topic;
use App\Models\TopicFee;
use App\Models\TopicFeeDetail;
use App\Models\TopicHasUserInfo;
use App\Models\TopicLaborParticipation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class TopicFeeController extends Controller
{

    public function index()
    {
        return view('admin.research.cost.index');
    }

    public function edit($id)
    {
        $data = TopicFee::findOrFail($id);
        return view('admin.research.cost.edit', ['data' => $data]);
    }

    public function laborParticipation($id)
    {
        $data = TopicFeeDetail::with('topicFee')->findOrFail($id);
        return view('admin.research.cost.labor-participation', ['data' => $data]);
    }

    public function laborParticipationDetail($id)
    {
        $data = TopicFeeDetail::with('topicFee')->findOrFail($id);
        return view('admin.research.cost.labor-participation-detail', ['data' => $data]);
    }

    public function materialCost($id)
    {
        $data = TopicFeeDetail::with('topicFee')->findOrFail($id);
        return view('admin.research.cost.material-cost', ['data' => $data]);
    }

    public function equipmentCost($id)
    {
        $data = TopicFeeDetail::with('topicFee')->findOrFail($id);
        return view('admin.research.cost.equipment-cost', ['data' => $data]);
    }

    public function assetCost($id)
    {
        $data = TopicFeeDetail::with('topicFee')->findOrFail($id);
        return view('admin.research.cost.asset-cost', ['data' => $data]);
    }

    public function otherCost($id)
    {
        $data = TopicFeeDetail::with('topicFee')->findOrFail($id);
        return view('admin.research.cost.other-cost', ['data' => $data]);
    }

    public function expertCost($id)
    {
        $data = TopicFeeDetail::with('topicFee')->findOrFail($id);
        return view('admin.research.cost.expert-cost', ['data' => $data]);
    }

    public function normalCost($id)
    {
        $data = TopicFeeDetail::with('topicFee')->findOrFail($id);
        return view('admin.research.cost.normal-cost', ['data' => $data]);
    }
}
