<?php

namespace App\Http\Livewire\Admin\Research\Cost;

use App\Enums\ETopicFee;
use App\Enums\ETypeUpload;
use App\Models\TopicFee;
use App\Models\TopicFeeDetail;
use App\Models\EquipmentCost;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Topic;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class EquipmentCostList extends BaseLive
{
    public $topicFeeDetailId;
    public $name;
    public $unit;
    public $quantity;
    public $price;
    public $state_capital;
    public $other_capital;
    public $equipmentCostID;
    public $model;
    public $type_upload;
    public $updateMode = false;
    public $topicFeeId;
    public $topicFeeStatus;
    public $displayAttachFile = true;


    public function mount($topicFeeDetailId, $topicFeeId, $topicFeeStatus) {
        $this->updateMode = false;
        $this->type_upload = Config::get('common.type_upload.EquipmentCost');
        $this->topicFeeDetailId = $topicFeeDetailId;
        $this->topicFeeId = $topicFeeId;
        $this->topicFeeStatus = $topicFeeStatus;
    }

    public function render()
    {

        $this->model = EquipmentCost::class;
        $this->updateTopicFeeDetail();
        $query = EquipmentCost::query();
        $query->where('topic_fee_detail_id', '=', $this->topicFeeDetailId);
        $dataTotal = [
            'total' => $query->sum('total'),
            'state_capital' => $query->sum('state_capital'),
            'other_capital' => $query->sum('other_capital'),
        ];
        $data = $query->with('files')->orderBy('id', 'desc')->paginate($this->perPage);
        return view('livewire.admin.research.cost.equipment-cost-list', ['dataTopicFee' => $this->topicFeeDetailId, 'data' => $data, 'model' => $this->model, 'type_upload' => $this->type_upload, 'model_id'=>$this->equipmentCostID, 'updateMode'=>$this->updateMode, 'dataTotal'=>$dataTotal]);
    }

    public function validateData()
    {
        $this->validate([
            'name' => 'required|max:100',
            'unit' => 'required|max:10',
            'quantity' => 'required|regex:/^[0-9,]+$/|max:7',
            'price' => 'required|regex:/^[0-9,]+$/|max:14',
            'state_capital' => 'nullable|regex:/^[0-9,]+$/|max:18',
            'other_capital' => 'nullable|regex:/^[0-9,]+$/|max:18',
            ],[
                'quantity.max' => __('client_validation.form.research_cost.max.quantity'),
                'price.max' => __('client_validation.form.research_cost.max.price'),
                'state_capital.max' => __('client_validation.form.research_cost.max.state_capital'),
                'other_capital.max' => __('client_validation.form.research_cost.max.other_capital'),
            ],[
            'name' => __('data_field_name.research_cost.name'),
            'unit' => __('data_field_name.research_cost.unit'),
            'quantity' => __('data_field_name.research_cost.quantity'),
            'price' => __('data_field_name.research_cost.price'),
            'state_capital' => __('data_field_name.research_cost.state_capital'),
            'other_capital' => __('data_field_name.research_cost.other_capital'),

        ]);
    }
    public function store()
    {
        $this->validateData();
        $equipmentCost = new EquipmentCost;
        $equipmentCost->name = $this->name;
        $equipmentCost->unit = $this->unit;
        $equipmentCost->quantity = removeFormatNumber($this->quantity);
        $equipmentCost->price = removeFormatNumber($this->price);
        $equipmentCost->state_capital = removeFormatNumber($this->state_capital);
        $equipmentCost->other_capital = removeFormatNumber($this->other_capital);
        $equipmentCost->total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
        $equipmentCost->topic_fee_detail_id = $this->topicFeeDetailId;
        $equipmentCost->admin_id = Auth::user()->id;
        $equipmentCost->save();

        \App\Models\File::where('model_name', EquipmentCost::class)
                ->where('type', $this->type_upload)->where('admin_id', auth()->id())->where('model_id', null)->update([
                    'model_id' => $equipmentCost->id,
        ]);
        $this->displayAttachFile = false;
        $this->emit('close-modal-create-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function resetInputFields(){
        $this->displayAttachFile = true;
        $this->name= null;
        $this->unit= null;
        $this->quantity=null;
        $this->price=null;
        $this->state_capital=null;
        $this->other_capital=null;
        $this->resetValidation();
    }

    public function edit($id){
        $this->updateMode = true;
        $this->equipmentCostID = $id;
        $equipmentCost = EquipmentCost::findOrFail($id);
        $this->name = $equipmentCost->name;
        $this->unit = $equipmentCost->unit;
        $this->quantity = $equipmentCost->quantity;
        $this->price = $equipmentCost->price;
        $this->state_capital = $equipmentCost->state_capital;
        $this->other_capital = $equipmentCost->other_capital;
        $this->total = $equipmentCost->total;
        $this->resetValidation();
    }

    public function update(){
        $equipmentCost = EquipmentCost::findOrFail($this->equipmentCostID);
        $this->validateData();
        $equipmentCost->name = $this->name;
        $equipmentCost->unit = $this->unit;
        $equipmentCost->quantity = removeFormatNumber($this->quantity);
        $equipmentCost->price = removeFormatNumber($this->price);
        $equipmentCost->state_capital = removeFormatNumber($this->state_capital);
        $equipmentCost->other_capital = removeFormatNumber($this->other_capital);
        $equipmentCost->total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
        $equipmentCost->save();
        \App\Models\File::where('model_name', EquipmentCost::class)
                ->where('type', $this->type_upload)->where('admin_id', auth()->id())->where('model_id', null)->update([
                    'model_id' => $equipmentCost->id,
        ]);
        $this->emit('close-modal-edit-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }

    public function deleteId($id){
        $this->equipmentCostID = $id;
    }

    public function delete(){
        $equipmentCost = EquipmentCost::findOrFail($this->equipmentCostID);

        $equipmentCost->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }

    public function updateTopicFeeDetail(){
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->topicFeeDetailId);
        $topicFeeDetail->total_capital = EquipmentCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('total');
        $topicFeeDetail->state_capital = EquipmentCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('state_capital');
        $topicFeeDetail->other_capital = EquipmentCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('other_capital');
        $topicFeeDetail->save();
    }

    public function resetMode(){
        $this->updateMode = false;
    }

    public function download($id){
        $file = \App\Models\File::findOrFail($id);
        return Storage::download($file->url, $file->file_name);
    }
}
