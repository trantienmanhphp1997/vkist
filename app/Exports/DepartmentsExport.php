<?php

namespace App\Exports;

use App\Enums\EDepartment;
use App\Models\Department;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class DepartmentsExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $searchName;
    public function __construct($searchName = '')
    {
        $this->searchName = $searchName;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $index = 1;
        $query = Department::with('parentDepartment')->withCount('users');
        if (!empty($this->searchName)) {
            $query->where('department.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchName)) . '%')
                ->orWhereRaw("LOWER(name) LIKE LOWER('%{$this->searchName}%')");
        }

        $departments = $query->orderBy('id', 'desc')->get();
        foreach ($departments as $department) {
            $department->index = $index++;
        }

        return $departments;
    }

    public function headings(): array
    {
        return [
            'STT',
            __('data_field_name.department.code'),
            __('data_field_name.department.name'),
            __('data_field_name.department.type_class'),
            __('data_field_name.department.type_group'),
            __('data_field_name.department.type_unit'),
            __('data_field_name.department.Number of employees'),
            __('data_field_name.department.note'),
            __('data_field_name.department.status'),
        ];
    }

    public function map($department): array
    {

        $department->class = '';
        $department->group = '';
        $department->unit = '';

        if ($department->type == EDepartment::TYPE_CLASS) {
            $department->class = $department->name;
        } elseif ($department->type == EDepartment::TYPE_GROUP) {
            $department->class = $department->parentDepartment->name ?? '';
            $department->group = $department->name;
        } elseif ($department->type == EDepartment::TYPE_UNIT) {
            $department->class = $department->parentDepartment->parentDepartment->name ?? '';
            $department->group = $department->parentDepartment->name ?? '';
            $department->unit = $department->name;
        }

        return [
            $department->index,
            $department->code ,
            $department->name,
            $department->class,
            $department->group,
            $department->unit,
            $department->users_count,
            $department->note,
            $department->status == 0 ?__('data_field_name.system.role.active')  : __('data_field_name.system.role.inactive'),
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = range('A', 'I');
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:E1');
            $event->sheet->setCellValue('A1', 'DANH SÁCH ĐƠN VỊ');
            $event->sheet->getStyle('A1:E1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:I3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        }, ];

    }
    public function title(): string
    {
        return 'DANH SÁCH ĐƠN VỊ';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
