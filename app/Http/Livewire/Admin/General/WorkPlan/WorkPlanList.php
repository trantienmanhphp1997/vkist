<?php

namespace App\Http\Livewire\Admin\General\WorkPlan;

use App\Enums\EWorkPlan;
use App\Enums\EWorkPlanPriorityLevel;
use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Models\WorkPlan;
use App\Models\MasterData;
use App\Models\UserInf;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use App\Exports\WorkPlanListExport;

class WorkPlanList extends BaseLive
{
    public $searchTerm;
    public $leader_id;
    public $searchPriority;
    public $searchStatus;
    public $value_priority;
    public $priority_list;

    public function mount() {
        $this->priority_list=[
            EWorkPlanPriorityLevel::MEDIUM => __('data_field_name.work_plan.index.medium'),
            EWorkPlanPriorityLevel::HIGH => __('data_field_name.work_plan.index.high'),
            EWorkPlanPriorityLevel::COGENCY => __('data_field_name.work_plan.index.cogency'),
        ];
    }
    public function render()
    {
        $query= WorkPlan::leftjoin('user_info', function($join){
            $join->on('work_plan.leader_id','=','user_info.id');
        });
        $query->leftjoin('department', function($join){
            $join->on('department.id','=','user_info.department_id');
        });
        $query->leftjoin('master_data', function($join){
            $join->on('master_data.id','=','work_plan.priority_id');
        });
        $query->leftjoin(DB::raw('master_data b'), function($join){
            $join->on('b.id','=','work_plan.work_type_id');
        });
        $query->select('work_plan.*',DB::raw('user_info.fullname as leader_name'),DB::raw('department.name as leader_department_name'),DB::raw('master_data.v_value as value_priority'),DB::raw('b.v_value as work_type'),DB::raw('master_data.order_number as order_number'))->get();

        $search_priority = $this->searchPriority;
        $search_status = $this->searchStatus;


        if(strlen($this->searchTerm)){
            $query->where('work_plan.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        if ($this->searchPriority) {
            $query->where('work_plan.priority_level','=',$search_priority);
        }
        if ($this->searchStatus) {
            $query->where('work_plan.status','=',$search_status);
        }

        $listStatus = EWorkPlan::getListStatus();
        $data = $query->orderBy('work_plan.id','DESC')->paginate($this->perPage);
        return view('livewire.admin.general.work-plan.work-plan-list', ['data' => $data, 'listStatus' => $listStatus]);
    }

    public function export() {
        return Excel::download(new WorkPlanListExport($this->searchTerm, $this->searchPriority, $this->searchStatus), 'work-plan-' . now() . '-' . '.xlsx');
    }
}
