<div class="col-md-10 col-xl-11 box-detail box-user list-topic-fee">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="{{ route('admin.research.topic-fee.index') }}">{{ __('data_field_name.research_cost.list_cost') }}</a> \ <span>{{ __('data_field_name.research_cost.work_detail_content') }}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{ __('data_field_name.research_cost.work_detail_content') }}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="col-md-12" wire:ignore>
                        @if ($topicFeeStatus == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                            <div class="form-group float-right" >
                                <button type="button"class="btn btn-viewmore-news mr0 createLabor" wire:click="create">{{__('data_field_name.common_field.add')}}</button>
                            </div>
                        @endif
                    </div>
                    <table class="table">
                        <thead>
                            <tr class="border-radius text-center">
                                <th scope="col" class="border-radius-left">
                                    {{ __('data_field_name.common_field.stt') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.work_content') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.expected_result') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.executor') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.common_field.position') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.wage_coefficient') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.number_workday') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.base_salary') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.total_wages') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.state_capital') }}
                                </th>
                                <th scope="col">
                                    {{ __('data_field_name.research_cost.other_capital') }}
                                </th>
                                <th scope="col" class="border-radius-right">
                                    {{ __('data_field_name.common_field.action') }}
                                </th>
                            </tr>
                            <tr class="border-radius text-center">
                                <th scope="col" class="text-center">(1)</th>
                                <th scope="col" class="text-center">(2)</th>
                                <th scope="col" class="text-center">(3)</th>
                                <th scope="col" class="text-center">(4)</th>
                                <th scope="col" class="text-center">(5)</th>
                                <th scope="col" class="text-center">(6)</th>
                                <th scope="col" class="text-center">(7)</th>
                                <th scope="col" class="text-center">(8)</th>
                                <th scope="col" class="text-center">(9)=(6)*(7)*(8)</th>
                                <th scope="col" class="text-center">(10)</th>
                                <th scope="col" class="text-center">(11)</th>
                                <th scope="col" class="text-center">(12)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $row)
                                <tr>
                                    <td class="text-center">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                    <td class="text-center">{{ $row->task->name ?? '' }}</td>
                                    <td class="text-center">{{ $row->expected_result ?? '' }}</td>
                                    <td class="text-center">{{ $row->topicHasUserInfo->userInfo->fullname ?? '' }}</td>
                                    <td class="text-center">{{ $row->topicHasUserInfo->role->v_value }}</td>
                                    <td class="text-center">{{ $row->wage_coefficient ?? '' }}</td>
                                    <td class="text-center">{{ $row->number_workday ?? '' }}</td>
                                    {{-- <td class="text-center">{!! isset($row->task_id) ? numberFormat($dataList[$row->task_id]['base_salary']) : '' !!}</td> --}}
                                    <td class="text-center">{{numberFormat($row->base_salary) ?? ''}}</td>
                                    <td class="text-center">{{ numberFormat($row->total) ?? '' }}</td>
                                    <td class="text-center">{{ numberFormat($row->state_capital) ?? '' }}</td>
                                    <td class="text-center">{{ numberFormat($row->other_capital) ?? '' }}</td>
                                    <td class="text-center">
                                        @if ($topicFeeStatus == \App\Enums\ETopicFee::STATUS_NOT_SUBMIT)
                                            <button class="btn-edit-film btn buttonEdit" wire:click="edit({!! $row->id !!})">
                                                <img src="/images/pent2.svg" alt="">
                                            </button>
                                            @include('livewire.common.buttons._delete')
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td class="text-center">{{ __('data_field_name.research_cost.sum') }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-center">{{ numberFormat($dataTotal['total']) }}</td>
                                <td class="text-center">{{ numberFormat($dataTotal['state_capital']) }}</td>
                                <td class="text-center">{{ numberFormat($dataTotal['other_capital']) }}</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')

                    <div class="col-md-12 text-center mt-5">
                        <a href="{{route('admin.research.topic-fee.labor', $topicFeeDetailId)}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-create" id="detail-work-modal" tabindex="-1" aria-labelledby="detailWorkModal" aria-hidden="true">
            <div class="modal-dialog modal-appraisal">
                <div class="modal-content personnel">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{ __('data_field_name.research_cost.work_detail_content') }}</h4>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.work_content') }} <span class="text-danger">*</span></label>
                            <select class="form-control form-control-lg form-input form-select" wire:model.lazy="topicDetailWork.task_id">
                                <option value="">{{__('data_field_name.common_field.select_default')}}</option>
                                @foreach ($tasks as $task)
                                    <option value="{{ $task->id }}">{{ $task->name }}</option>
                                @endforeach
                            </select>
                            @error('topicDetailWork.task_id') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.expected_result') }} <span class="text-danger">*</span></label>
                            <textarea class="form-control form-input" wire:model.defer="topicDetailWork.expected_result"></textarea>
                            @error('topicDetailWork.expected_result') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.fullname_perform') }} <span class="text-danger">*</span></label>
                            <select class="form-control form-control-lg form-input form-select" wire:model.lazy="selectedUserInfoId">
                                <option value="">{{__('data_field_name.common_field.select_default')}}</option>
                                @foreach (($tasks->firstWhere('id', $topicDetailWork->task_id)->userInfos ?? []) as $userInfo)
                                    <option value="{{ $userInfo->id }}">{{ $userInfo->fullname }}</option>
                                @endforeach
                            </select>
                            @error('topicDetailWork.topic_has_user_info_id') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.acdymic_topic') }}</label>
                            <input type="text" class="form-control form-input" readonly value="{{ $selectedUserInfoRole }}">
                            @error('topicDetailWork.acdymic_topic') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.wage_coefficient') }} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-input" wire:model.lazy="topicDetailWork.wage_coefficient">
                            @error('topicDetailWork.wage_coefficient') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.number_workday') }} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-input" wire:model.lazy="topicDetailWork.number_workday">
                            @error('topicDetailWork.number_workday') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.base_salary') }} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.lazy="topicDetailWork.base_salary">
                            @error('topicDetailWork.base_salary') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.total_wages') }} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-input" value="{{ $topicDetailWork->total }}" readonly>
                            @error('topicDetailWork.total') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.state_capital') }} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="topicDetailWork.state_capital">
                            @error('topicDetailWork.state_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.research_cost.other_capital') }} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-input format_number" wire:model.defer="topicDetailWork.other_capital">
                            @error('topicDetailWork.other_capital') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>

                        <div class="attached-center">
                            <button type="button" id="close-modal-edit-department" class="btn btn-cancel mt-0" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="submit" class="btn btn-save close-modal">{{__('common.button.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script>
        $("document").ready(function() {
            $('.buttonCreate, .buttonEdit').click(function() {
                $('.error').remove();
                $('.form-input').val('');
            });

            $('#createDetailWork, #editDetailWork').on('hidden.bs.modal', function () {
                $('.error').remove();
                window.livewire.emit('resetData');
            });
        });

        window.addEventListener('open-detail-work-modal-event', function() {
            $("#detail-work-modal").modal('show');
        });
    </script>
</div>
