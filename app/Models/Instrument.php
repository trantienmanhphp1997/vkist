<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{
    use HasFactory;
    protected $table='instrument';
    protected $fillable=['code', 'name','instrument_id','unit','quantity','price','note','status', 'admin_id', 'unsign_text'];
    
    private static $searchable = [
        'name',
        'note',
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
}
