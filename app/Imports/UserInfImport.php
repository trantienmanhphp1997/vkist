<?php

namespace App\Imports;

use App\Models\UserInf;
use App\Models\MasterData;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use DB;
use DateTime;

class UserInfImport implements ToModel,WithStartRow,SkipsEmptyRows
{
    use SkipsErrors ,Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new UserInf([
            'code' => @$row[0],
            'fullname' =>  @$row[3],
            'position_id' => @$this->getPositionID($row[8]),
            'birthday' => @$this->formatDateImport($row[12]),
            'email' =>  @$row[13],
            'phone' =>  @$row[14],
        ]);

    }

    public function startRow(): int
    {
        return 3;
    }

    public function getPositionID($positionName) {
        $position = MasterData::firstOrCreate([
            'type' => '1',
            'v_value' => $positionName,
            'v_key' => 'position',
            ],
            [
        ]);
        return $position->id;
    }

    public function formatDateImport($row){
        $date = DateTime::createFromFormat('d/m/Y', $row);
        if($date && $date->format('d/m/Y') === $row){
            $row = $date->format('Y-m-d');
        }
        else {
            $row= is_numeric($row)?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row))->format('Y-m-d'):null;
        }
        return $row;
    }
}
