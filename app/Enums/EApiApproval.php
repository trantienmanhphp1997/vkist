<?php


namespace App\Enums;


class EApiApproval {
    const TYPE_IDEAL = 1;
    const TYPE_TOPIC = 2;
    const TYPE_COST = 3;
    const TYPE_RESEARCH_PLAN = 4;
    const TYPE_WORK_PLAN = 5;
    const TYPE_BUDGET = 6;
    const TYPE_REQUEST_LEAVE = 7;

    const STATUS_DRAFT = 'draft';
    const STATUS_COMPLETE = 'complete';
    const STATUS_REJECT = 'reject';
}
