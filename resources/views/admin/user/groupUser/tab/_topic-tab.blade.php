<div class="information">
    <style>
        .title-approve {
            margin: 0 !important;
        }

    </style>
    <div class="inner-tab pt0">

        @if (count($readyTopics) > 0)
            <table class="table mt-3">
                <thead>
                    <tr class="border-radius">
                        <th scope="col" class="text-center" class="border-radius-left">{{ __('news/newsManager.menu_name.news_title_table.stt') }}</th>
                        <th scope="col" class="text-center">{{ __('user/group_user.table.topic-code') }}</th>
                        <th scope="col" class="text-center">{{ __('user/group_user.table.topic-name') }}</th>
                        <th scope="col" class="text-center">{{ __('user/group_user.table.option') }}</th>
                        <th scope="col" class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($readyTopics as $topicId => $value)
                        <tr>
                            <td class="text-center">*</td>
                            <td class="text-center">
                                <a href="{{ route('admin.research.topic.detail', $topicId) }}">
                                    {{ $value['code'] }}
                                </a>
                            </td>
                            <td class="text-center text-danger">{{ $value['name'] }}</td>
                            <td class="text-center text-danger">
                                <a class="btn par6" data-toggle="modal" data-target="#delete-ready-topic-modal" onclick="$('#delete-ready-topic-btn').attr('data-id', {{ $topicId }})"> <img src="/images/trash.svg" height="30px" alt="trash"></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <hr>
        @endif

        <div class="row">
            <div class="col-md-10">
                <div class="form-group search-expertise">
                    <div class="search-expertise">
                        <input type="text" placeholder="{{ __('common.place_holder.search') }}" name="search" class="form-control" wire:model.debounce.1000ms="searchTerm" id='input_vn_name' autocomplete="off">
                        <span><img src="/images/Search.svg" alt="search" /></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-right">
                <button type="button" class="btn btn-viewmore-news w-xs-100" data-toggle="modal" data-target="#TopicPickerModal">{{ __('user/group_user.add-topic') }}</button>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr class="border-radius">
                    <th scope="col" class="text-center" class="border-radius-left">{{ __('news/newsManager.menu_name.news_title_table.stt') }}</th>
                    <th scope="col" class="text-center">{{ __('user/group_user.table.topic-code') }}</th>
                    <th scope="col" class="text-center">{{ __('user/group_user.table.topic-name') }}</th>
                    <th scope="col" class="text-center">{{ __('user/group_user.table.option') }}</th>
                    <th scope="col" class="text-center"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($topics as $key => $topic)
                    <tr>
                        <td class="text-center">
                            {{ ($topics->currentPage() - 1) * $topics->perPage() + $loop->iteration }}
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.research.topic.detail', $topic->id) }}">
                                {!! boldTextSearch($topic->code, $searchTerm) !!}
                            </a>
                        </td>
                        <td class="text-center">
                            {!! boldTextSearch($topic->name, $searchTerm) !!}
                        </td>
                        <td class="text-center">
                            <a class="btn par6" data-toggle="modal" data-target="#delete-topic-modal" onclick="$('#delete-topic-btn').attr('data-id', {{ $topic->id }})"> <img src="/images/trash.svg" height="30px" alt="trash"></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @if (count($topics) > 0)
            {{ $topics->links() }}
        @else
            <div class="title-approve text-center">
                <span>{{ __('common.message.empty_search') }}</span>
            </div>
        @endif
    </div>
    <div wire:ignore.self class="modal fade" id="TopicPickerModal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>{{ __('user/group_user.title.pick-topic') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group" style="width: 100% ; margin-top: 10px">
                        <label>{{ __('user/group_user.title.pick-research-category') }}</label>
                        @livewire('component.advanced-select', [
                            'name' => 'research-category',
                            'modelName' => \App\Models\ResearchCategory::class,
                            'searchBy' => ['name', 'code'],
                            'columns' => ['name', 'code'],
                            'placeholder' => __('common.select-box.choose')
                        ], key($selectionKey))
                    </div>
                    <div class="form-group d-flex align-items-center">
                        <input type="checkbox" wire:model.lazy="allTopicsOfField" {{ empty($selectedResearchCategory) ? 'disabled' : '' }}>
                        <span>&nbsp;&nbsp;{{ __('user/group_user.title.pick-all-topic') }}</span>
                    </div>

                    @if (!$allTopicsOfField && !empty($selectedResearchCategory))
                        <div class="form-group">
                            <label>{{ __('user/group_user.title.pick-topic') }}</label>
                            @livewire('component.advanced-select', [
                                'name' => 'topic',
                                'modelName' => \App\Models\Topic::class,
                                'searchBy' => ['name', 'code'],
                                'columns' => ['name', 'code'],
                                'multiple' => true,
                                'placeholder' => __('common.select-box.choose'),
                                'conditions' => [
                                    ['research_category_id', '=', $selectedResearchCategory['id']]
                                ]
                            ], key($selectedResearchCategory['id'] ?? 0))
                        </div>
                    @endif
                </div>
                <div class="text-center">
                    <span>
                        <button class="btn btn-main bg-4 text-3 size16 px-48 py-14" wire:click="resetSelection" data-dismiss="modal">{{ __('common.button.cancel') }}</button>
                        <button class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn" wire:click="saveSelectedTopics" data-dismiss="modal">{{ __('common.button.save') }}</button>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="delete-topic-modal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{ __('common.confirm_message.confirm_title') }}</h4>
                    <button class="btn float-right" style="position: absolute; top: 0px; right: 0px;" data-dismiss="modal"><em class="fa fa-times"></em></button>
                    {{ __('common.confirm_message.are_you_sure_delete') }}
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.cancel') }}</button>
                    <button type="button" class="btn btn-save" data-dismiss="modal" id="delete-topic-btn" wire:click="deleteTopic($event.target.getAttribute('data-id'))">{{ __('common.button.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="delete-ready-topic-modal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{ __('common.confirm_message.confirm_title') }}</h4>
                    <button class="btn float-right" style="position: absolute; top: 0px; right: 0px;" data-dismiss="modal"><em class="fa fa-times"></em></button>
                    {{ __('common.confirm_message.are_you_sure_delete') }}
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.cancel') }}</button>
                    <button type="button" class="btn btn-save" data-dismiss="modal" id="delete-ready-topic-btn" wire:click="deleteReadyTopic($event.target.getAttribute('data-id'))">{{ __('common.button.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

</div>
