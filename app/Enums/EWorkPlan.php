<?php


namespace App\Enums;


class EWorkPlan {
    const STATUS_NEW = 1;
    const STATUS_WAIT_APPROVAL = 2;
    const STATUS_APPROVAL = 3;
    const STATUS_REJECT = 4;

    public static function getListStatus() {
        return [
            self::STATUS_NEW => __('data_field_name.work_plan.index.status_new'),
            self::STATUS_WAIT_APPROVAL => __('data_field_name.work_plan.index.wait'),
            self::STATUS_APPROVAL => __('data_field_name.work_plan.index.done'),
            self::STATUS_REJECT => __('data_field_name.work_plan.index.deny'),
        ];
    }
}
