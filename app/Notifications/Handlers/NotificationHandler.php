<?php

namespace App\Notifications\Handlers;

use App\Models\User;
use App\Notifications\BaseNotification;
use Illuminate\Support\Facades\Notification;

class NotificationHandler
{

    public $users;
    public BaseNotification $notification;

    public function __construct($users, BaseNotification $notification)
    {
        $this->users = $users;
        $this->notification = $notification;
    }

    public function send()
    {
        if (empty($this->users)) {
            return;
        }

        if ($this->users instanceof User) {
            $this->users = [$this->users];
        }

        foreach ($this->users as $user) {
            if (!$user instanceof User) {
                continue;
            }

            $configs = $user->notificationConfig()->content ?? null;
            if ($configs == null) {
                continue;
            }

            $tmpNotification = clone $this->notification;
            $config = $configs[$tmpNotification->type] ?? [];
            $methods = $config['via'] ?? ['broadcast'];
            $tmpNotification->withMethods($methods);

            Notification::send($user, $tmpNotification);
        }
    }

    public static function sendNotification($users, BaseNotification $notification)
    {
        $handler = new static($users, $notification);
        $handler->send();
    }
}
