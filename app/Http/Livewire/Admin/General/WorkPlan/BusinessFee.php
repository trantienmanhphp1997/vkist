<?php

namespace App\Http\Livewire\Admin\General\WorkPlan;


use App\Component\SizeFile;
use App\Http\Livewire\Base\BaseLive;
use App\Models\WorkPlanHasBusinessFee;
use App\Models\WorkPlanHasUserInfo;
use App\Service\Community;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class BusinessFee extends BaseLive
{
    use WithFileUploads;

    public $work_plan_id;
    public $fee;
    public $date;
    public $user_info_id;
    public $fee_name;
    public $business_fee_id;

    public $key;

    protected $listeners = [
        'set-work-date' => 'setWorkDate',
    ];

    protected function rules() {
        return [
            'fee' => 'required',
            'date' => 'required',
            'user_info_id' => 'required',
            'business_fee_id' => 'required',
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'fee' => __('data_field_name.work_plan.edit.fee'),
            'date' => __('data_field_name.work_plan.edit.date'),
            'user_info_id' => __('data_field_name.work_plan.edit.user_name'),
            'business_fee_id' => __('data_field_name.work_plan.edit.fee_name'),
        ];
    }

    public function mount() {
        $this->key = Str::random(5);
    }

    public function setWorkDate($data)
    {
        $this->date = date('Y-m-d', strtotime($data['work-date']));
    }


    public function render()
    {

        $user_info = WorkPlanHasUserInfo::join('user_info', 'user_info.id', '=', 'work_plan_has_user_info.user_info_id')
            ->where('work_plan_has_user_info.work_plan_id', $this->work_plan_id)->where('work_plan_has_user_info.admin_id', auth()->id())
            ->pluck('user_info.fullname', 'work_plan_has_user_info.user_info_id');
        $business_fee = WorkPlanHasBusinessFee::where('work_plan_id', $this->work_plan_id)->get();
        //anhta add
        foreach ($business_fee as $business) {
            $business->money = numberFormat($business->money) ?? 0;
        }
        //end of anhta


        $business_fee_list = \App\Models\BusinessFee::all();
        $user_info_list = WorkPlanHasUserInfo::where('work_plan_id', $this->work_plan_id)
            ->get();

        $type_fee = \App\Models\BusinessFee::where('name', '!=', 'null')->pluck('name', 'id');

        return view('livewire.admin.general.work-plan.business-fee', [
            'business_fee_list' => $business_fee_list,
            'user_info' => $user_info,
            'business_fee' => $business_fee, 'type_fee' => $type_fee,
            // 'files' => $files,
            // 'fileFee' => $fileFee,
            'user_info_list' => $user_info_list,
        ]);
    }


    public function storeBusinessFee()
    {

        $this->validate();

        $businees_fee = WorkPlanHasBusinessFee::create([
            'working_day' => $this->date,
            'work_plan_id' => $this->work_plan_id,
            'business_fee_id' => $this->business_fee_id,
            'user_info_id' => $this->user_info_id,
            //anhta add
            'money' => Community::getAmount($this->fee),
            //end of anhta
            'status' => 0,
        ]);

        \App\Models\File::query()
            ->where('model_name', WorkPlanHasBusinessFee::class)
            ->whereNull('model_id')
            ->where('type', Config::get('common.type_upload.WorkPlanHasBusinessFee'))
            ->update([
                'status' => -1,
                'model_id' => $businees_fee->id,
            ]);
        $this->resetInput();
        $this->dispatchBrowserEvent('stored-business-fee-event');
    }

    public function resetInput()
    {
        $this->fee = '';
        $this->date = null;
        $this->user_info_id = '';
        $this->fee_name = '';
        $this->business_fee_id = '';
        $this->key = Str::random(5);
    }

}
