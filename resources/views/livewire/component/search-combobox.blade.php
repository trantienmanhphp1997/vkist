<div class="position-relative">
        <input
            type="text"
            class="form-input w-100"
            placeholder="Search ..."
            wire:model.debounce.1000ms="query"
            wire:keydown.escape="resetInput"
            wire:keydown.tab="resetInput"
            wire:keydown.enter="resetInput"
            id="search-input-combobox{{is_null($classify) ? '': -$classify}}" 
            style="height:48px"
            @if($disable) disabled @endif
        />
    {{--    <div wire:loading class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg">--}}
    {{--        <div class="list-item">Searching...</div>--}}
    {{--    </div>--}}
    
            <div class="position-absolute z-10 list-group bg-white w-100 rounded-t-none shadow-lg search_bar_div @if(!$displayOption)d-none @endif" 
            id="option-list{{is_null($classify) ? '': -$classify}}" 
            style="max-height: 250px; overflow-y: auto;overflow-x: hidden; z-index:10">
                @if(!empty($data))
                    @foreach($data as $i => $item)
                        <a wire:click="setInput({{ $item['id'] }},'{{ $item[$value_render_on_combobox] }}')"
                            class="search_bar"
                        >{{ $item[$value_render_on_combobox] }}</a>
                    @endforeach
                @else
                    <div class="search_bar">{{__('common.message.empty_search')}}</div>
                @endif
            </div>
       
    </div>
    <script>
        $("document").ready(function() {
            // optionListElement = $("#option-list{{is_null($classify) ? '': -$classify}}");
            // console.log('op-l',optionListElement);
            $("#option-list{{is_null($classify) ? '': -$classify}}").scroll( e => {
                console.log('croll',$("#option-list{{is_null($classify) ? '': -$classify}}")[0].scrollTop,$("#option-list{{is_null($classify) ? '': -$classify}}")[0].clientHeight,$("#option-list{{is_null($classify) ? '': -$classify}}")[0].scrollHeight)
                if(Math.abs($("#option-list{{is_null($classify) ? '': -$classify}}")[0].scrollTop + $("#option-list{{is_null($classify) ? '': -$classify}}")[0].clientHeight - $("#option-list{{is_null($classify) ? '': -$classify}}")[0].scrollHeight) < 1) {
                    Livewire.emit('loadMore{{is_null($classify) ? '':$classify}}');
                }
            })
            $('#search-input-combobox{{is_null($classify) ? '': -$classify}}').focus(() => {
                Livewire.emit('displayOption{{is_null($classify) ? '':$classify}}');
            })
            window.addEventListener('reset-height-scroll', () => {
                $("#option-list{{is_null($classify) ? '': -$classify}}")[0].scrollTop = 0;
            })
            $('#search-input-combobox{{is_null($classify) ? '': -$classify}}').blur(() => {
                Livewire.emit('hideOption{{is_null($classify) ? '':$classify}}');
            })
        })
    </script>
    