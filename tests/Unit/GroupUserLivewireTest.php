<?php

namespace Tests\Unit;
use App\Models\User;
use Livewire\Livewire;
use Tests\TestCase;

class GroupUserLivewireTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    /** @test */
    public function testIndexGroupUser()
    {
        $user = $this->user;
        $this->actingAs($user)->get(route('admin.system.group-user.index'))->assertOk();
    }
    public function testCreateGroupUserSuccess(){
        $user = User::first();
        if(empty($user)){
            $user = User::factory()->create();

        }
        Livewire::actingAs($user)->test('admin.user.group-user.form-data',[
            'name'=>'Admin',
            'code'=>'123',
            'description'=>'test'
        ])
        ->call('save')->assertHasNoErrors(['name','code','description']);
    }
    public function testCreateGroupUserFail(){
        $user = User::first();
        if(empty($user)){
            $user = User::factory()->create();

        }
        Livewire::actingAs($user)->test('admin.user.group-user.form-data',[
            'name'=>'Admin',
            'code'=>'',
            'description'=>''
        ])
        ->call('save')->assertHasErrors(['description','code']);
    }
}
