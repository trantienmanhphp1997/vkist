<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetAllocatedRevokeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_allocated_revoke', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('asset_id')->nullable()->constrained('asset')->comment('map vs bnagr asset');
            $table->tinyInteger('status')->nullable()->comment('1:cấp phát, 2:thu hồi, 3:điều chuyển');
            $table->date('implementation_date')->nullable()->comment('ngày thực hiện');
            $table->tinyInteger('used_type')->nullable()->comment('đối tượng sử dụng, enums, 1:nhân viên, 2:văn phòng');
            $table->string('number_report')->nullable()->comment('số biên bản');
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('nhân viên, map vs user_ifo');
            $table->string('implementation_reason')->nullable()->comment('lý do thực hiện');
            $table->integer('implementation_quantity')->nullable()->comment('số lượng thực hiện');

            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_allocated_revoke');
    }
}
