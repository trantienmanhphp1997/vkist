<?php

namespace App\Http\Livewire\Admin\General\WorkPlan;


use App\Enums\EMasterDataType;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Department;
use App\Models\MasterData;
use App\Models\UserInf;
use App\Models\WorkPlanHasUserInfo;
use Livewire\Component;

class ListMember extends BaseLive
{
    public $user_info_id;
    public $position_id;
    public $department_id;
    public $userIdSelects = [];

    public function resetInput()
    {
        $this->user_info_id = '';
        $this->position_id = '';
        $this->department_id = '';
    }

    public function render()
    {
        $userQuery = UserInf::query();
        $list_members = WorkPlanHasUserInfo::join('user_info', 'user_info.id', '=', 'work_plan_has_user_info.user_info_id')
            ->where('work_plan_has_user_info.status', 0)
            ->where('work_plan_id',null)
            ->select('user_info.id', 'user_info.department_id', 'work_plan_has_user_info.position_id')->get();
        foreach ($list_members as $item) {
            $userQuery->where('id', '!=', $item->id);
        }
        $user = $userQuery->pluck('fullname', 'id');
        $departments = Department::all()->pluck('name', 'id');
        $departmentUser = [];
        if ($this->user_info_id) {
            $departmentUser = UserInf::findOrFail($this->user_info_id)->department()->pluck('department.name', 'department.id');
            foreach ($departmentUser as $key => $val) {
                $this->department_id = $key;
            }
        }
        $userAll = UserInf::all()->pluck('fullname', 'id');

        $position = MasterData::getDataByType(EMasterDataType::TITLE);

        $output = [
            'list_members' => $list_members,
            'users' => $user,
            'userAll' => $userAll,
            'departmentUser' => $departmentUser,
            'position' => $position,
            'departmentAll' => $departments,
        ];
        return view('livewire.admin.general.work-plan.list-member', $output);
    }

    public function storeMember()
    {

        if ($this->user_info_id) {
            WorkPlanHasUserInfo::create([
                'user_info_id' => $this->user_info_id,
                'position_id' => $this->position_id,
                'status' => 0,
                'admin_id' => auth()->id()
            ]);
            $this->resetInput();
        }
    }

}
