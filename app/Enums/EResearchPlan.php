<?php


namespace App\Enums;

class EResearchPlan
{
    const STATUS_NOT_SUBMIT = 1;
    const STATUS_SUBMIT = 2;
    const STATUS_COMPLETE_PROFILE = 3;
    const STATUS_INVALID_PROFILE = 4;
    const STATUS_WAIT_ASSESSMENT = 5;
    const STATUS_WAIT_APPROVAL = 6;
    const STATUS_REJECT = 7;
    const STATUS_APPROVAL = 8;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::STATUS_NOT_SUBMIT:
                $result = __('data_field_name.research_plan.not_submit');
                break;
            case self::STATUS_SUBMIT:
                $result = __('data_field_name.research_plan.submit');
                break;
            case self::STATUS_COMPLETE_PROFILE:
                $result = __('data_field_name.topic.status_completed_file');
                break;
            case self::STATUS_INVALID_PROFILE:
                $result = __('data_field_name.topic.status_invalid_file');
                break;
            case self::STATUS_WAIT_ASSESSMENT:
                $result = __('data_field_name.research_plan.waiting_expertise');
                break;
            case self::STATUS_WAIT_APPROVAL:
                $result = __('data_field_name.research_plan.wait_approve');
                break;
            case self::STATUS_REJECT:
                $result = __('data_field_name.topic.status_rejected');
                break;
            case self::STATUS_APPROVAL:
                $result = __('data_field_name.topic.status_approved');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
