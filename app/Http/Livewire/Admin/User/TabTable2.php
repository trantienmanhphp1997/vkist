<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;
use App\Models\UserResearchPaper;
use App\Component\Recursive;
use Illuminate\Support\Facades\Config;
use Route;

class TabTable2 extends Component

{
    public $user_info_id;
    public $product_name;
    public $author;
    public $publishing_company;
    public $issn;
    public $publishing_year;
    public $editReTable2='';
    public $deleteId2 ='';
    public $canEdit = true;

    protected $listeners = [
        'set-publishing-year' => 'setPublishingYear'
    ];

    protected $rule =[
        'product_name' => 'required|max:100',
        'author' => 'max:100',
        'publishing_company' => 'required|max:48',
        'issn' => 'max:9',
        'publishing_year' => 'required',
    ];
    public function render()
    {
        $data2 =  UserResearchPaper::where('user_info_id',$this->user_info_id)
                    ->orderBy('user_research_paper.id','desc')->get();
        $model_name = UserResearchPaper::class;
        $type = Config::get('common.type_upload.UserResearchPaper');
        $folder = app($model_name)->getTable();
        $model_id = $this->user_info_id;
        if(checkShowMode()) {
            $this->canEdit = false;
        }
        $this->dispatchBrowserEvent('render-research-result-event');
        return view('livewire.admin.user.tab-table2',['data2'=>$data2,'model_name'=>$model_name, 'type'=>$type, 'model_id'=>$model_id, 'folder'=>$folder]);
    }

     public function store(){
        $this->validate([
            'product_name' => ['required', 'regex:/[^.][^0-9<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:100'],
            'author' => ['regex:/[^.][^0-9<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:100'],
            'publishing_company' => ['required', 'regex:/[^.][^0-9<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:48'],
            'issn' => 'max:9',
            'publishing_year' => 'required',
        ],[],[

            'product_name'=>__('data_field_name.working_process.product_name'),
            'author'=>__('data_field_name.working_process.author'),
            'publishing_company'=>__('data_field_name.working_process.newspaprer_name'),
            'issn'=>__('data_field_name.working_process.issn'),
            'publishing_year'=>__('data_field_name.working_process.announced_year'),
        ]);

        if($this->editReTable2){
            UserResearchPaper::findOrFail($this->editReTable2)->update([
                'user_info_id'=>$this->user_info_id,
                'product_name'=>$this->product_name,
                'author'=>$this->author,
                'publishing_company'=>$this->publishing_company,
                'issn'=>$this->issn,
                'publishing_year'=>$this->publishing_year,
            ]);
            $this->resetInputFields2();
            $this->emit('userStore');
            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.update')]);
        }else{
            UserResearchPaper::create([
                'user_info_id'=>$this->user_info_id,
                'product_name'=>$this->product_name,
                'author'=>$this->author,
                'publishing_company'=>$this->publishing_company,
                'issn'=>$this->issn,
                'publishing_year'=>$this->publishing_year,
            ]);
            $this->resetInputFields2();
            $this->emit('userStore');
            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.add')]);
        }

    }


    public function editReTable2($id){
        $this->editReTable2 = $id;
        $data=UserResearchPaper::findOrFail($this->editReTable2);
        $this->product_name=$data->product_name;
        $this->author=$data->author;
        $this->publishing_company=$data->publishing_company;
        $this->issn=$data->issn;
        $this->publishing_year = $data->publishing_year?date('Y-m-d',strtotime($data->publishing_year)):'';

    }

    public function getIdDeleteTable2($id){
        $this->deleteId2 = $id;
    }
    public function deleteReTable2(){
        $data2 = UserResearchPaper::findOrFail($this->deleteId2);
        $data2->delete();
        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.delete')]);
    }
    public function resetInputFields2(){
        $this->product_name='';
        $this->author='';
        $this->publishing_company='';
        $this->issn='';
        $this->publishing_year='';
        $this->editReTable2 = null;
    }

    public function setPublishingYear($value) {
        $this->publishing_year = date('Y-m-d', strtotime($value['publishing-year']));
    }
}
