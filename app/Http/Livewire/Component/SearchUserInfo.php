<?php

namespace App\Http\Livewire\Component;

use App\Enums\EResearchCategoryType;
use App\Http\Livewire\Base\BaseLive;
use App\Models\UserInf;

class SearchUserInfo extends BaseLive
{
    public $query='';
    public $data;
    public $successful=false;
    public $name;
    public $key;
    public $pageNum = 1;
    public $perPage = 10;
    public $displayOption = false;
    public $userInfoId ;
    public $classify;

    protected $listeners = [];

    protected function getListeners()
    {
        return [
                'loadMore'.$this->classify => 'loadMore',
                'displayOption'.$this->classify =>'displayOption',
                'hideOption'.$this->classify => 'hideOption',
            ];
    }

    public function loadMore() {
        $this->pageNum = $this->pageNum+1;
        $this->getData();
    }
    public function displayOption() {
        $this->displayOption = true;
    }

    public function hideOption() {
        if($this->userInfoId) {
            $this->query = UserInf::findOrFail($this->userInfoId)->fullname;
        }
        $this->displayOption = false;
    }

    public function resetInput()
    {
        $this->query = '';
        $this->data = [];
    }

    public function updatedQuery()
    {
        $this->pageNum = 1;
        $this->getData();
        $this->dispatchBrowserEvent('reset-height-scroll');
    }
    public function getData() {
        $this->data = UserInf::where('fullname', 'like', '%' . $this->query . '%')
        ->limit($this->pageNum*$this->perPage)
        ->get()
        ->toArray();
    }
    public function mount() {
        if($this->userInfoId) {
            $this->query = UserInf::findOrFail($this->userInfoId)->fullname;
        }
        $this->getData();
    }
    public function render()
    {
        if(!$this->query) {
            $this->successful=false;
        }

        return view('livewire.component.search-user-info');
    }
    public function setInput($id,$name){
        $this->userInfoId = $id;
        $this->query=$name;
        $this->displayOption=false;
        $this->emit('setUserInfo'.$this->classify, $id);
    }
}
