<?php
$topic_level = 'Topic level';
return [
    [
        'type' => 11,
        'v_key' => $topic_level,
        'v_value' => 'Cấp bộ',
        'order_number' => 1
    ],
    [
        'type' => 11,
        'v_key' => $topic_level,
        'v_value' => 'Cấp cơ sở',
        'order_number' => 0
    ],
    [
        'type' => 11,
        'v_key' => $topic_level,
        'v_value' => 'Cấp trung ương',
        'order_number' => 2
    ],
];
