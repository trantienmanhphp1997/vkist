<?php

namespace App\Enums;

class EIdeaStatus {
    const WAIT_APPROVE = 0;
    CONST APPROVED = 1;
    CONST REJECTED = 2;
    CONST JUST_CREATED = 3;
}
