<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs">
                <a href="">{{ __('data_field_name.topic.topic_research_control') }} </a> \
                <span>
                    @if ($topic->exists)
                        {{ __('data_field_name.topic.edit') }}
                    @else

                        {{ __('data_field_name.topic.create') }}
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="bd-border" wire:ignore>
        <div class="detail-task information box-idea">
            <h4>
                @if ($topic->exists)
                    {{ __('data_field_name.topic.edit') }}
                @else
                    {{ __('data_field_name.topic.create') }}
                @endif
            </h4>
            <div class="group-tabs">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" data-toggle="tab" href="#tab-general" role="tab" aria-controls="tab-general" aria-selected="false">
                            <span>{{ __('data_field_name.topic.tab_general') }}</span>
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#tab-presentation" role="tab" aria-controls="tab-presentation" aria-selected="false">
                            <span>{{ __('data_field_name.topic.tab_presentation') }}</span>
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#tab-member" role="tab" aria-controls="tab-member" aria-selected="false">
                            <span>{{ __('data_field_name.topic.tab_member') }}</span>
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#tab-expected-result" role="tab" aria-controls="tab-expected-result" aria-selected="false">
                            <span>{{ __('data_field_name.topic.tab_expected_result') }}</span>
                        </a>

                    </div>
                </nav>

                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade active show" id="tab-general" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="alert alert-danger d-none mt-3 tab-error tab-general-error"></div>
                        @livewire('admin.research.topic.tab-general', ['topic' => $topic])
                    </div>

                    <div class="tab-pane fade" id="tab-presentation" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="alert alert-danger d-none mt-3 tab-error tab-presentation-error"></div>
                        @livewire('admin.research.topic.tab-presentation', ['topic' => $topic])
                    </div>

                    <div class="tab-pane fade" id="tab-member" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="alert alert-danger d-none mt-3 tab-error tab-member-error"></div>
                        @livewire('admin.research.topic.tab-member', ['topic' => $topic])
                    </div>

                    <div class="tab-pane fade" id="tab-expected-result" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="alert alert-danger d-none mt-3 tab-error tab-expected-result-error"></div>
                        @livewire('admin.research.topic.tab-expected-result', ['topic' => $topic])
                    </div>

                </div>
            </div>

            <div class="text-center">
                <button class="btn btn-main bg-4 text-3 size16 px-48 py-14" wire:click="cancel" wire:loading.attr="disabled">{{ __('common.button.cancel') }}</button>
                <button class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn" wire:click="submit" onclick="this.disabled = true;">{{ __('common.button.save') }}</button>
            </div>

        </div>
    </div>

    <script>
        window.addEventListener('first-failed-tab-event', function(event) {
            $('.tab-error').addClass('d-none');

            let failedTabs = event.detail;

            for(let tab of failedTabs) {
                $('.' + tab.name + '-error').removeClass('d-none');
                $('.' + tab.name + '-error').html(tab.message);
            }

            let firstTab = failedTabs[0];
            document.querySelector("[href='#" + firstTab.name + "']").click();
            $(window).scrollTop($('.form-group').has('div.text-danger').first().offset().top);
            $('.submit-btn').attr('disabled', false);
        });
    </script>
</div>
