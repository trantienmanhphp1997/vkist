<div class="col-md-8 col-xl-9 box-detail box-user" style="background: white;    margin-top: 15px;     padding: 10px 50px 0px;">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="/">{{ __('news/newsManager.menu_name.home') }} \</a>
                <a href="{{ route('news.index', ' ') }}">{{ __('news/newsManager.menu_name.list-new') }} \</a>
                <span>{{ __('news/newsManager.menu_name.detail-new') }}</span>
            </div>
        </div>
    </div>
    <div class="container-fluid bd-border">
        <div class="row form-component" style="padding: 10px 50px 0;max-width: calc(100% - 250px)!important; background: white; margin: 0 auto;">
            <div class="col-md-12">
                <h3 class="title-detail">
                    {!! $new->name !!}
                </h3>
                <div class="date date-detail">
                    <img src="{{ asset('images/date.svg') }}" alt="">
                    {{ formatDateLocale($new->updated_at, '%a, %e/%m/%Y') }}
                </div>
            </div>
            <div class="col-md-12">
                <img src="{!! trim($new->image_path) != '' ? asset('storage/' . $new->image_path) : asset('images/img.jpg') !!}" style="margin-bottom: 12px" class="card-img-top" alt="">
            </div>
            <div class="col-md-12 detail-description-detail" style=" text-align: justify;  ; font-size: 16px">
                {!! $new->description !!}
            </div>
            <div class="col-md-12" style="margin-bottom: 40px">
                <a class="date date-detail" style="margin-right: 10px">
                    <img style="    margin-bottom: 6px;" src="{{ asset('images/User.svg') }}" alt="user">
                    {{ __('news/newsManager.menu_name.article') }} :
                    {!! $new->admin->name !!}
                </a>
                <a class="date date-detail" style="margin-right: 10px">
                    <img style="    margin-bottom: 6px;" src="{{ asset('images/eye.svg') }}" alt="eye">
                    {{ __('news/newsManager.menu_name.count_read') }}:
                    {{ $new->count_read }}
                </a>
                @if ($amountFile > 0)
                    <a class="viewmore" style="cursor:pointer;margin-bottom: 12px" data-toggle="modal" data-target="#downloadFile">File
                        đính
                        kèm ({!! $amountFile !!})</a>
                @endif
            </div>
            <div class="col-md-12 card-date" style=" color: black;font-size: 14px ;border-bottom: 1px solid black">
            </div>
            <div class="col-md-12" style="padding: 0; margin-bottom: 50px">
                <div class="menu-news" style="width: 100% ; padding: 12px 0">
                    {{ __('news/newsManager.menu_name.news_child.post') }}
                </div>
                <div style="padding: 0 8px">
                    @foreach ($news as $new)
                        <div class="row box-new" wire:click.prevent="getDetail('{!! $new->slug !!}','{{ $new->category->slug ?? 'unkhown' }}')" style="padding: 10px 0">
                            <div class="col-md-3" style="padding: 10px 14px;">
                                <img style="    width: 100%;border-radius: 8px;    height: 90px;" src="{{ trim($new->image_path) ? asset('storage/' . $new->image_path) : asset('images/img.jpg') }}" alt="">
                            </div>
                            <div class="col-md-9" style="padding: 20px 0 20px 20px;    text-align: justify;">
                                <h6 class="title-new-detail">
                                    {{ $new->name }}
                                </h6>
                                <div class="date date-detail">
                                    <img src="{{ asset('images/date.svg') }}" alt="date">
                                    {!! $new->updated_at !!}
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    </div>
    {{-- Knowing others is intelligence; knowing yourself is true wisdom. --}}

    <div wire:ignore.self class="modal fade" id="downloadFile" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                @foreach ($files as $key => $file)
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="attached-files" wire:click="download('{{ $file->url }}')" style="cursor: pointer">
                                <img src="/images/File.svg" alt="file">
                                <div class="content-file">
                                    <p>{{ $file->file_name }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
