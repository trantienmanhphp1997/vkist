@extends('layouts.master')
<title>{{__('decision-reward/decision-reward.title.edit')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.asset.provider.asset-provider-create-and-edit')
@endsection
