<!--start sidebar-left-->
<div class="col-md-2 col-xl-3 sidebar-left">
    <ul class="nav flex-column memu-main-left">

        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{ __('data_field_name.shopping.shopping_management') }}
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="dropdown-menu-left">
                @if (checkPermission('admin.executive.shopping.index'))
                    <li>
                        <a class="nav-link active" href="{{ route('admin.executive.shopping.index') }}">
                            <span class="circle"></span> {{ __('data_field_name.shopping.list') }}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
    </ul>
</div>
<!--end sidebar-left-->
