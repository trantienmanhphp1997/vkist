<?php


namespace App\Enums;


class EMasterDataType {
    const POSITION = 1;
    const ETHNIC = 2;
    const RELIGION = 3;

    const NATION = 4;
    const EDUCATION_BACKGROUND = 5;
    const RANK = 6;
    const TITLE = 7;


    const TOPIC_LEVEL_BASE = 349 ;
    const TOPIC_LEVEL_CADRES = 348 ;
    const TOPIC_LEVEL_STATE = 350 ;

    const TOPIC_LEVEL = 11;
    const SCIENTIFIC_NAME = 12;

    const ROLE_IN_TOPIC = 13;

    const BANK_NAME = 17;
    const COEFICIENTS_SALARY = 18;

    const RESEARCH_WAY = 19;
    const UNIT = 20;

    const SPECIALIZED=21;
    const FIELD = 22;

}
