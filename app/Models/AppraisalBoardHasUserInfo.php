<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppraisalBoardHasUserInfo extends Participant
{
    protected $table = 'appraisal_board_has_user_info';

    protected $fillable = [
        'role_id', 'status', 'appraisal_board_id', 'user_info_id'
    ];

    protected $mapRelations = [
        'staff' => 'userInfo',
        'work' => 'appraisalBoard'
    ];

    public function userInfo()
    {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }

    public function appraisalBoard()
    {
        return $this->belongsTo(AppraisalBoard::class, 'appraisal_board_id');
    }
}
