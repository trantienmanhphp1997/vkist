<?php

namespace App\Exports;

use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Illuminate\Support\Carbon;
use App\Enums\EAssetStatus;
use Illuminate\Support\Collection;
class ErrorFileExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($error, $type) {
        $this->error = $error;
        $this->type = $type;
    }

    public function collection()
    {
        return collect($this->error);
    }
    public function headings():array
    {
        return
        [
            'Thông tin lỗi',
        ];
    }

    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('D1:k1');
            if ($this->type == 'salary') {
                $event->sheet->setCellValue('D1','DANH SÁCH LỖI IMPORT LƯƠNG');
            } else {
                $event->sheet->setCellValue('D1','DANH SÁCH LỖI IMPORT CHẤM CÔNG');
            }
            
            $event->sheet->getStyle('D1:k1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:U3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        if ($this->type == 'salary') {
            $title = 'DANH SÁCH LỖI IMPORT LƯƠNG';
        } else {
            $title = 'DANH SÁCH LỖI IMPORT CHẤM CÔNG';
        }
        return $title;
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
