<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\MasterData;
use App\Models\UserInf;
use Illuminate\Database\Seeder;

class UserInfSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = Department::all();
        $qualifications = MasterData::where('type', 5)->get();
        $positions = MasterData::where('type', 1)->get();

        UserInf::factory()->times(30)
            ->state(function () use ($departments, $qualifications, $positions) {
                return [
                    'department_id' => $departments->random()->id,
                    'academic_level_id' => $qualifications->random()->id,
                    'position_id' => $positions->random()->id
                ];
            })->create();
    }
}
