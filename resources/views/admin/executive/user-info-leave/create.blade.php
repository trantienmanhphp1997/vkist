@extends('layouts.master')
<title>{{__('executive/user-info-leave.create-update-form.title')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.executive.user-info-leave.user-info-leave-create-and-update')
@endsection