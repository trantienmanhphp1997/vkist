<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Factory;
use DB;

class BusinessFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_fee')->insert([
            [
                'code' => 'a',
                'name' => 'a',
                'accounting_code' => 'a', 
                'type' => 'a', 
                'status' => '1', 
                'admin_id'=>'1',
                'created_at' => date("d-m-Y"),
                'updated_at' =>date("d-m-Y"),
                'note'=>'aaaa',

            ],
            [
                'code' => 'b',
                'name' => 'b',
                'accounting_code' => 'b', 
                'type' => 'b', 
                'status' => '2', 
                'admin_id'=>'8',
                'created_at' => date("d-m-Y"),
                'updated_at' =>date("d-m-Y"),
                'note'=>'aaaa',
            ],
            [
                'code' => 'c',
                'name' => 'c',
                'accounting_code' => 'c', 
                'type' => 'c', 
                'status' => '3', 
                'admin_id'=>'2',
                'created_at' => date("d-m-Y"),
                'updated_at' =>date("d-m-Y"),
                'note'=>'aaaa',
            ],
            [
                'code' => 'd',
                'name' => 'd',
                'accounting_code' => 'd', 
                'type' => 'd', 
                'status' => '4', 
                'admin_id'=>'4',
                'created_at' => date("d-m-Y"),
                'updated_at' =>date("d-m-Y"),
                'note'=>'aaaa',
            ]
        ]);

    }
}
