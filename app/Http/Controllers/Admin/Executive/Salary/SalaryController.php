<?php

namespace App\Http\Controllers\Admin\Executive\Salary;

use App\Http\Controllers\Controller;
use DB;

class SalaryController extends Controller {
    public function index() {
        return view('admin.executive.salary.index');
    }

    public function show($id) {
        return view('admin.executive.salary.show', ['id' => $id]);
    }
}
