<div class="inner-tab row">
    <div class="col-sm-12 form-group">
        @php $check = $data->achievement_paper_check==1 @endphp
        <input type="checkbox" name="achievement_paper_check" id="a-1" value="1"
               @if($check) disabled checked @else disabled @endif>
        <label for="a-1">{{ __('data_field_name.topic.achievement_paper_check') }}</label>
    </div>

    <div class="col-sm-12 form-group">
        <label for="">{{ __('data_field_name.topic.expected_result_content') }}</label>
        <div class="form-control" name="achievement_note" style="height: auto;">
            {{ trim($data->achievement_note) }}
        </div>
    </div>

    <div class="col-sm-6 form-group">
        @php $check1 = $data->achievement_review_check==1 @endphp
        <input type="checkbox" name="achievement_review_check" id="a-3" value="1"
               @if($check1) disabled checked @else disabled @endif>
        <label for="a-3">{{ __('data_field_name.topic.achievement_review_check') }}</label>
    </div>

    <div class="col-sm-6 form-group">
        @php $check2 = $data->achievement_announcement_check==1 @endphp
        <input type="checkbox" disable name="achievement_announcement_check" id="a-4" value="1"
               @if($check2) disabled checked @else disabled @endif>
        <label for="a-4">{{ __('data_field_name.topic.achievement_announcement_check') }}</label>
    </div>

    <div class="col-sm-6 form-group">
        @php $check3 = $data->achievement_conference_check==1 @endphp
        <input type="checkbox" name="achievement_conference_check" id="a-5" value="1" disable
               @if($check3) disabled checked @else disabled @endif>
        <label for="a-5">{{ __('data_field_name.topic.achievement_conference_check') }}</label>
    </div>

    <div class="col-sm-6 form-group">
        @php $check4 = $data->achievement_work_check==1 @endphp
        <input type="checkbox" name="achievement_work_check" id="a-6" value="1"
               @if($check4) disabled checked @else disabled @endif>
        <label for="a-6">{{ __('data_field_name.topic.achievement_work_check') }}</label>
    </div>

    <div class="col-sm-6 form-group">
        @php $check5 = $data->achievement_textbook_check== 1 @endphp
        <input type="checkbox" name="achievement_textbook_check" id="a-7" value="1"
               @if($check5) disabled checked @else disabled @endif>
        <label for="a-7">{{ __('data_field_name.topic.achievement_textbook_check') }}</label>
    </div>
</div>
