@extends('layouts.master')
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('title')
    <title>{{ __('menu_management.menu_name.user_info_list') }}</title>
@endsection
@section('content')
@livewire('admin.user.user-list')
@endsection
@section('js')
    <script>
        $('#search_more_btn').click(function (){
            var moreId=document.getElementById('searchMore');
            if (moreId.style.display=== 'none'){
                moreId.style.display='';
                document.getElementById('search_more_btn').src="/images/more_active.png"
            } else{
                moreId.style.display='none';
                document.getElementById('search_more_btn').src="/images/filter3gach.png"
            }
        });
    </script>
@endsection
