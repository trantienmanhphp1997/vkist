<?php


namespace App\Enums;

class EPayForm
{
    const EXPENSES = 1;
    const NO_EXPENSES = 0;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::EXPENSES:
                $result = __('data_field_name.reality_expense.expenses');
                break;
            case self::NO_EXPENSES:
                $result = __('data_field_name.reality_expense.no_expenses');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
