@extends('layouts.master')
<title>{{__('menu_management.menu_name.asset-provider')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
@livewire('admin.asset.provider.asset-provider-list')
@endsection
