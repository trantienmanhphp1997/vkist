<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicActualFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic_actual_fee', function (Blueprint $table) {
            $table->id()->comment('Danh sach khoan chi');
            $table->tinyInteger('source_capital')->nullable()->comment('Nguon kinh phi');
            $table->bigInteger('approval_costs')->nullable()->comment('Duyet chi');
            $table->string('payment', 255)->nullable()->comment('Hinh thuc chi');
            $table->tinyInteger('type_cost')->nullable()->comment('Loai chi phi');
            $table->bigInteger('total_cost')->nullable()->comment('Tong so tien da chi');
            $table->bigInteger('money_remain')->nullable()->comment('So tien con lai');
            $table->bigInteger('user_request_id')->nullable()->comment('Nguoi lap yeu cau');
            $table->string('note', 1000)->nullable()->comment('Noi dung chi tiet');
            $table->bigInteger('money_cost')->nullable()->comment('So tien can chi');
            $table->date('disbursement_date')->nullable()->comment('Thoi han giai ngan');
            $table->integer('remain_day')->nullable()->comment('Thoi gian con');
            $table->tinyInteger('status')->comment('Trang thai giai ngan');
            $table->foreignId('task_id')->nullable()->constrained('task');
            $table->foreignId('topic_fee_detail_id')->nullable()->constrained('topic_fee_detail');
            $table->unsignedBigInteger('topic_id')->nullable()->comment('Map voi topic');
            $table->foreign('topic_id')->references('id')->on('topic')->comment('Map voi topic');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topic_actual_fee');
    }
}
