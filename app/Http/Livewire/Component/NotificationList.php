<?php

namespace App\Http\Livewire\Component;

use App\Enums\ENotificationType;
use App\Enums\ESystemConfigType;
use App\Models\Notification;
use App\Models\SystemConfig;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class NotificationList extends Component
{

    protected $shouldReadAll = false;

    public $config;
    public $configContent;

    public function mount()
    {
        $this->config = SystemConfig::query()->where('admin_id', auth()->id())->where('type', ESystemConfigType::NOTIFICATION)->firstOrNew();
        $this->configContent = $this->config->content ?? [];
    }

    public function render()
    {
        $userId = auth()->id();
        $query = Notification::query()->where('admin_id', $userId)->orderBy('created_at', 'desc');
        $count = DB::table('notification')->where('admin_id', '=', $userId)->whereNull('read_at')->count('id');
        $this->dispatchBrowserEvent('count-unread-notification-event', [
            'count' => $count
        ]);

        $notifications = [];
        if ($this->shouldReadAll) {
            $notifications = $query->get();
        } else {
            $notifications = $query->limit(10)->get();
        }

        $notifications->each(function($notification) {
            $notiConfig = Arr::first($this->configContent, fn($item) => ($item['name'] ?? '') == $notification->name);
            if(!empty($notiConfig) && in_array('broadcast', $notiConfig['via'])) {
                $notification->enabled = true;
            } else {
                $notification->enabled = false;
            }
        });

        return view('livewire.component.notification-list', [
            'notifications' => $notifications,
            'count' => $count
        ]);
    }

    public function readNotification($notificationId)
    {
        Notification::where('id', $notificationId)->update([
            'read_at' => Carbon::now()
        ]);
    }

    public function readAllNotification()
    {
        $this->shouldReadAll = true;
    }

    public function clearAllNotification()
    {
        Notification::query()->where('admin_id', auth()->id())->delete();
        $this->dispatchBrowserEvent('show-toast', [
            'message' => __('notification.common.success.delete'),
            'type' => 'success'
        ]);
    }

    public function clearNotification($id)
    {
        Notification::query()->where('id', $id)->delete();
        $this->dispatchBrowserEvent('show-toast', [
            'message' => __('notification.common.success.delete'),
            'type' => 'success'
        ]);
    }

    public function toggleNotification($name, $action)
    {

        if (!empty($name) && !empty($this->configContent)) {
            foreach($this->configContent as &$content) {
                if($content['name'] == $name) {
                    $content['via'] = ($action == 'enable') ? ['broadcast'] : [];
                    break;
                }
            }

            $this->config->content = $this->configContent;
            $this->config->save();

            $arr = explode('|', __('data_field_name.notification.type_' . strtolower($name)));
            $notificationName = strtolower(array_shift($arr));

            $message = ($action == 'enable') ? __('notification.common.success.turned_on_notification', ['name' => $notificationName]) : __('notification.common.success.turned_off_notification', ['name' => $notificationName]);

            $this->dispatchBrowserEvent('show-toast', [
                'message' => $message,
                'type' => 'success'
            ]);
        }
    }

    public function getListeners()
    {
        return [
            "new-notification-event" => '$refresh',
        ];
    }
}
