

                  <div class="detail-task information box-idea">
                        <h4>{{__('data_field_name.businesfee.create_business')}}</h4>
                        <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>{{__('data_field_name.businesfee.code')}} <span
                                          class="text-danger">*</span></label>
                                  <input type="text" name="code" class="form-control" placeholder="" value="{{old('code')}}">
                                  @error('code') <span class="text-danger error">{{ $message }}</span>@enderror
                              </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.businesfee.fee_name')}} <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="name" class="form-control" placeholder="" value="{{old('name')}}">
                                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.businesfee.accountant_code')}} <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="accounting_code" class="form-control" placeholder="" value="{{old('accounting_code')}}">
                                @error('accounting_code') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.businesfee.type_of_tracking')}} <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="tracking_type" class="form-control" placeholder="" value="{{old('tracking_type')}}">
                                @error('tracking_type') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.businesfee.note')}}</label>
                                <textarea name="note" class="form-control" rows="4" >
                                    {{ trim(old('note')) }}
                                </textarea>
                                @error('note') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                        </div>
                          <div class="col-md-12">
                              <div class="group-btn2 text-center group-btn ">
{{--                                <button type="submit" class="btn kb">{{__('common.button.review')}}<img src="/images/eye.svg" alt="eye"></button>--}}
                                <button type="submit" class="btn btn-save">{{__('common.button.save')}}</button>
                                <a href="{{route('admin.general.businessfee.index')}}" class="btn btn-cancel">{{ __('common.button.cancel') }}</a>
                              </div>
                          </div>
                      </div>
                          </div>



