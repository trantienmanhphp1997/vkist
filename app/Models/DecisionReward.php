<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;



class DecisionReward extends BaseModel 
{
    use HasFactory;
    protected $table = "decision_reward";
    protected $guarded=['*'];


    public function receivers() {
        return $this->hasMany(DecisionRewardReceiver::class, 'decision_reward_id');
    }
}

