<?php

namespace App\Http\Livewire\Admin\ResearchProject\TaskWork;

use Livewire\Component;
use App\Models\TaskWorkHasResultPlan;
use App\Models\TaskWork;
use App\Enums\ETypeUpload;
use App\Http\Livewire\Base\BaseLive;
use App\Models\File;
use Illuminate\Support\Facades\Config;
use Auth;

class TaskWorkHasResult extends BaseLive
{
    public $name ;
    public $isEdit = false;
    public $task_work_has_result_id;
    public $task_work_detail_id;
    public $listFile = [];
    public $admin_id;
    protected $rules = [
        'name' => ['required', 'max:100', 'regex:/[^<>!@#$%^&*(){}\[\]\'\"\/\|]+$/'],
    ];

    public function render()
    {
        $data = TaskWorkHasResultPlan::query()->whereNull('task_work_detail_id')->get();
        $model_name = TaskWorkHasResultPlan::class;
        $type = Config::get('common.type_upload.TaskWorkHasResult');
        $folder = app($model_name)->getTable();
        return view('livewire.admin.research-project.task-work.task-work-has-result', 
        [
        'data' => $data, 
        'model_name' => $model_name,
        'type' => $type,
        'folder' => $folder]);
    }
    public function store()
    {
        $this->isEdit = false;
        $taskWorkHasResult = new TaskWorkHasResultPlan;
        $this->validate([
            'name' => ['required', 'max:100', 'regex:/[^<>!@#$%^&*(){}\[\]\'\"\/\|]+$/'],
        ], [], [
            'name' => __('data_field_name.task_work_has_result.name')
        ]);
        $taskWorkHasResult->name = $this->name;
        $taskWorkHasResult->admin_id = Auth::user()->id;
        $taskWorkHasResult->save();
        $this->task_work_has_result_id = $taskWorkHasResult->id;
        File::query()
            ->whereNull('model_id')
            ->where('admin_id', auth()->id())
            ->where('type', Config::get('common.type_upload.TaskWorkHasResult'))
            ->update([
                'model_id' => $this->task_work_has_result_id
            ]);

        $this->emit('close-modal-create-task-work-has-result');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);
        $this->resetInputFields();
    }
    public function edit($id){
        $taskWorkHasResult = TaskWorkHasResultPlan ::findOrFail($id);
        $this->isEdit = true;
        $this->task_work_has_result_id = $id;
        $this->admin_id = $taskWorkHasResult->admin_id;
        $this->name = $taskWorkHasResult->name;

    }
    public function update(){
        $this->validate([
            'name' => ['required', 'max:100', 'regex:/[^<>!@#$%^&*(){}\[\]\'\"\/\|]+$/'],
        ], [], [
            'name' => 'Tên sản phẩm'
        ]);
        $taskWorkHasResult = TaskWorkHasResultPlan ::findOrFail($this->task_work_has_result_id);
        $taskWorkHasResult ->name = $this->name;
        $taskWorkHasResult->save();
        $this->emit('close-modal-create-task-work-has-result');
    }
    public function delete(){
        $taskWorkHasResult = TaskWorkHasResultPlan::findOrFail($this->deleteId);
        $taskWorkHasResult->delete();
    }
    public function resetInputFields(){
        $this->name = null;
        $this->task_work_has_result_id = null ;
        $this->isEdit = false;
    }
    public function getListFile($id) {
        $taskWorkHasResult = TaskWorkHasResultPlan::where('id', $id)->with('files')->first();
        if(!is_null($taskWorkHasResult)) {
            $this->listFile = $taskWorkHasResult->files;
        }
        $this->dispatchBrowserEvent('openDownloadFileModal');
    }
    public function download($url, $name) {
        return response()->download('storage/'.$url, $name);

    }

}
