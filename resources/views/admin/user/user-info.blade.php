<form action="{{URL::to('admin/user/import')}}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}

    <input id="user-file" type="file" name="user_file" class="hidden" accept=".xlsx, .xls, .csv, .ods">
    <button type="submit">Import</button>
</form>
