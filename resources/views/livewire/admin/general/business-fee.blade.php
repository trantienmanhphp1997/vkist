<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a
                    href="http://192.168.2.100:8182/admin/research/ideal">{{__('data_field_name.work_plan.index.general')}}</a>
                \ <span>{{__('data_field_name.businesfee.business_fee_control')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.businesfee.business_fee_list')}}</h3>
                </div>
                <div class="col-md-6">
                    @if($checkCreatePermission)
                        <div class="form-group float-right">
                            <a class="btn btn-viewmore-news mr0 " href="{{route('admin.general.businessfee.create')}}"
                               style="color:white;">
                                <img src="/images/plus2.svg" alt="plus">
                                {{__('data_field_name.businesfee.create_business')}}
                            </a>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-12 approve">
                <div class="row rowUser">
                    <div id="search" class="w-100 mb-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="d-inline-block search-expertise">
                                    <div class="search-expertise">
                                        <input type="text" class="form-control size13" id="searchTerm"
                                               placeholder="{{__('data_field_name.businesfee.input')}}"
                                               name="searchTerm" autocomplete="off"
                                               wire:model.debounce.1000ms="searchTerm">
                                        <span>
                      <img src="/images/Search.svg" alt="search">
                    </span>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general" id="coUser" id="businessFee">
                            <thead>
                            <tr class="title-approve">
                                <th scope="col colUser">{{__('data_field_name.common_field.stt')}}</th>
                                <th scope="col colUser">{{__('data_field_name.businesfee.code')}}</th>
                                <th scope="col colUser">{{__('data_field_name.businesfee.fee_name')}}</th>
                                <th scope="col colUser">{{__('data_field_name.businesfee.accountant_code')}}</th>
                                <th scope="col colUser">{{__('data_field_name.businesfee.type_of_tracking')}}</th>
                                <th scope="col colUser">{{__('data_field_name.businesfee.create_at')}}</th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                            @forelse($data as $row)
                                <tr>
                                    <td>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                    <td>{!! boldTextSearch($row->code,$searchTerm) !!}</td>
                                    <td>{!! boldTextSearch($row->name,$searchTerm) !!}</td>
                                    <td>{!! boldTextSearch($row->accounting_code,$searchTerm) !!}</td>
                                    <td>{{$row->tracking_type}}</td>
                                    <td>{{reFormatDate($row->created_at)}}</td>
                                </tr>
                            @empty
                                <tr class="text-center text-danger">
                                    <td colspan="12">{{__('common.message.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    {{$data->links()}}
                    <div id="pageUser" class="title-approve "></div>
                </div>
            </div>
        </div>
    </div>
</div>

