@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.work_plan.detail')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.work_plan.create.general')}} </a> \ <span>{{__('data_field_name.work_plan.create.planWork')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <form class="">
                @csrf
                <div class="col-md-12">
                    <div class="detail-task information box-idea">
                        <h4>{{__('data_field_name.work_plan.detail')}}</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.content')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="" name="content" value="{{$data->content ?? ''}}" readonly>
                                    @error('content') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.mission_id')}}<span class="text-danger">*</span></label>
                                    <input class="form-control form-control-lg" value="{{$data->missions ?? ''}}" name="mission" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.MISSIONS_CODE')}}</label>
                                    <input type="text" class="form-control mission-code" value="{{$data->missions_code ?? ''}}" placeholder="" name="missions_code" readonly>
                                    @error('missions_code') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.start_time')}}<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control" placeholder="" name="start_date" readonly value="{{$data->start_date? date('Y-m-d',strtotime($data->start_date)):''}}">
                                    @error('start_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.end_time')}}<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control" placeholder="" name="end_date" readonly  value="{{$data->end_date? date('Y-m-d',strtotime($data->end_date)):''}}">
                                    @error('end_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.fee_type')}}</label>
                                    <input class="form-control form-control-lg" value="{{$data->fee->name ?? ''}}" name="budget_id" readonly>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{__('data_field_name.work_plan.create.fee')}}</label>
                                            <input type="text" class="form-control format_number" placeholder="" value="{{numberFormat($data->estimated_cost) ?? ''}}" name="estimated_cost" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{__('data_field_name.work_plan.index.priority1')}}</label>
                                            <input class="form-control form-control-lg" value="{{$data->priority_level?\App\Enums\EWorkPlanPriorityLevel::valueToName($data->priority_level):''}}" name="budget_id" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

{{--                            @livewire('admin.general.work-plan.list-member')--}}
{{--                            @livewire('admin.general.work-plan.file')--}}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="title-plan text-uppercase">{{__('data_field_name.work_plan.edit.title_item1')}}</h4>
                                    </div>
                                </div>
                            </div>
                            @foreach($data->memberList as $item)
                                <div class="box-calendar col-md-12">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.work_plan.create.name')}}</label>
                                                <input type="text" class="form-control mission-code" value="{{$item->userInfo->fullname ?? ''}}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.work_plan.create.department')}}</label>
                                                <input type="text" class="form-control mission-code" value="{{$item->userInfo->department->name ?? ''}}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{__('data_field_name.work_plan.create.mission')}}</label>
                                                <input type="text" class="form-control mission-code" value="{{$item->position->v_value ?? ''}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="title-plan text-uppercase">{{__('common.file')}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group  form-inline">
                                    @foreach($files as $item)
                                        <div class="attached-files col-md-4 mb-2 mr-3">
                                            <img src="/images/File.svg" alt="file"/>
                                            <div class="content-file">
                                                <p>{{$item->file_name}}</p>
                                                <p class="kb">{{$item->size_file}}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @if ($data->status == \App\Enums\EWorkPlan::STATUS_APPROVAL)
                                <div class="mb-5 col-md-12 list-file-budget">
                                    @livewire('common.list-attach-file', ['modelId' => $data->id, 'type' => \App\Enums\EApiApproval::TYPE_WORK_PLAN])
                                </div>
                            @endif
                            <div class="col-md-12 border-top">
                                <div class="group-btn2 text-center">
                                    <a href="{{route('admin.general.work-plan.index')}}">
                                        <button type="button" class="btn btn-cancel" name="cancel">
                                            {{__('data_field_name.work_plan.create.btn_cancel')}}
                                        </button>
                                    </a>
                                    @if(checkRoutePermission('approve'))
                                    <button type="button" class="btn btn-save buttonSendApproval" name="save">
                                        {{__('data_field_name.work_plan.create.btn_send')}}
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @livewire('common.api-approval', ['modelId' => $data->id, 'type' => \App\Enums\EApiApproval::TYPE_WORK_PLAN, 'requestId' => $requestId])

@endsection

