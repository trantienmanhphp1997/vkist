<?php

 
namespace App\Resolvers;
use Illuminate\Support\Facades\Auth;

class UserResolver implements \OwenIt\Auditing\Contracts\UserResolver

{

    /**
     * {@inheritdoc}
     */
    public static function resolve()
    {
        return auth()->check() ? Auth::user()  : null;
    }

}

 