<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAcademic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_academic', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('standard')->nullable()->comment('trinh do hoc van');
            $table->string('major')->nullable()->comment('nganh hoc');
            $table->string('school')->nullable();
            $table->dateTime('graduated_year')->nullable();
            $table->integer('degree')->nullable();
            $table->tinyInteger('type')->nullable()->comment('1:van bang, 2:chung chi');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('map với user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_academic');
    }
}
