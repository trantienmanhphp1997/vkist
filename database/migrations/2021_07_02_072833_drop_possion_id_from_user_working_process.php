<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPossionIdFromUserWorkingProcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_working_process', function (Blueprint $table) {
            $table->dropColumn('possion_id');
             $table->bigInteger('position_id')->nullable()->comment('master_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_working_process', function (Blueprint $table) {
            $table->bigInteger('possion_id')->nullable()->comment('master_data');
            $table->dropColumn('position_id');

        });
    }
}
