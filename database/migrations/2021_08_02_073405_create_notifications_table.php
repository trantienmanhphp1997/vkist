<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('type')->nullable()->comment('Loại thông báo');
            $table->foreignId('admin_id')->nullable()->comment('Người dùng được thông báo')->constrained('users');
            $table->string('data', 1500)->nullable()->comment('Nội dung thông báo');
            $table->timestamp('read_at')->nullable()->comment('Thời điểm đọc thông báo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification');
    }
}
