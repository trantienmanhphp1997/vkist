<?php

namespace App\Http\Livewire\Admin\System;

use App\Enums\ESystemConfigType;
use App\Models\SalaryBasic;
use App\Models\SystemConfig;
use Livewire\Component;

class GeneralConfig extends Component
{

    public SystemConfig $config;
    public array $content;

    public $dateImportBasicSalary;
    public $dateImportPositionSalary;
    public $dateCreateBudgetPlan;
    public $passwordDefault;
    public $minLengthOfPassword;
    public $passwordSpecialRequirement;
    public $seniorityDoesNotIncludeVacationDay;

    public function mount()
    {
        $this->config = SystemConfig::query()
            ->firstOrCreate([
                'name' => 'Import salary config',
                'model_name' => SalaryBasic::class,
                'type' => ESystemConfigType::TYPE_IMPORT_SALARY,
            ], [
                'content' => [
                    "date_import_basic_salary" => [
                        "value" => null,
                    ],
                    "date_import_position_salary" => [
                        "value" => null,
                    ],
                    "date_create_budget_plan" => [
                        "value" => null,
                    ],
                    "password_default" => [
                        "value" => null,
                    ],
                    "min_length_of_password" => [
                        "value" => 8
                    ],
                    "password_special_requirement" => [
                        "value" => false
                    ],
                    "seniority_does_not_include_vacation_day" => [
                        "value" => false
                    ],
                ]
            ]);
        $this->content = $this->config->content;

        $this->dateImportBasicSalary = $this->content['date_import_basic_salary'] ?? ["value" => null];
        $this->dateImportPositionSalary = $this->content['date_import_position_salary'] ?? ["value" => null];
        $this->dateCreateBudgetPlan = $this->content['date_create_budget_plan'] ?? ["value" => null];
        $this->passwordDefault = $this->content['password_default'] ?? ["value" => null];
        $this->minLengthOfPassword = $this->content['min_length_of_password'] ?? ["value" => null];
        $this->passwordSpecialRequirement = $this->content['password_special_requirement'] ?? ["value" => false];
        $this->seniorityDoesNotIncludeVacationDay = $this->content['seniority_does_not_include_vacation_day'] ?? ["value" => false];
    }

    public function render()
    {
        return view('livewire.admin.system.general-config');
    }

    public function save()
    {
        $this->validation();
        $this->content['date_import_basic_salary'] = $this->dateImportBasicSalary;
        $this->content['date_import_position_salary'] = $this->dateImportPositionSalary;
        $this->content['date_create_budget_plan'] = $this->dateCreateBudgetPlan;
        $this->content['password_default'] = $this->passwordDefault;
        $this->content['min_length_of_password'] = $this->minLengthOfPassword;
        $this->content['password_special_requirement'] = $this->passwordSpecialRequirement;
        $this->content['seniority_does_not_include_vacation_day'] = $this->seniorityDoesNotIncludeVacationDay;
        $this->config->content = $this->content;
        $this->config->save(); 
        $this->dispatchBrowserEvent('show-toast', ['message' => __('data_field_name.general_config.save_successfully'), 'type' => 'success']);
    }

    public function validation()
    {
        $this->validate([
            'dateImportBasicSalary.value' => ['nullable','max:48', 'regex:/^([0-9]{0,2}[,]{0,1})+$/'],
            'dateImportPositionSalary.value' => [ 'nullable', 'max:48', 'regex:/^([0-9]{0,2}[,]{0,1})+$/'],
            'dateCreateBudgetPlan.value' => ['nullable', 'max:48', 'regex:/^([0-9]{0,2}[,]{0,1})+$/'],
            'passwordDefault.value' => 'required',
            'minLengthOfPassword.value' => 'required | numeric',
        ], [], [
            'dateImportBasicSalary.value' => __('executive/my-time-salary.basic-salary'),
            'dateImportPositionSalary.value' => __('executive/my-time-salary.position-salary'),
            'dateCreateBudgetPlan.value' => __('data_field_name.general_config.date_create_budget_plan'),
            'passwordDefault.value' => __('data_field_name.general_config.password_default'),
            'minLengthOfPassword.value' => __('data_field_name.general_config.min_length_of_password'),
        ]);
    }
}
