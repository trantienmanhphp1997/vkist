<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname', 255)->nullable()->comment('Ho va ten');
            $table->string('code', 255)->unique('code')->comment('ma nhan vien');
            $table->string('address', 255)->nullable()->comment('Địa chỉ');
            $table->string('phone', 12)->nullable();
            $table->string('birthday')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->tinyInteger('married')->nullable();
            $table->string('email')->nullable();
            $table->integer('ethnic_id')->nullable()->comment('Ton giao');
            $table->integer('religion_id')->nullable()->comment('Quoc tich map với master_data');
            $table->integer('nationality_id')->nullable()->comment('Quoc tich map với master_data');
            $table->string('id_card')->nullable()->comment('cmnd');
            $table->date('issued_date')->nullable();
            $table->string('issued_place')->nullable();
            $table->string('passport_number')->nullable();
            $table->date('passport_date')->nullable();
            $table->string('passport_place')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->string('note',1000)->nullable();
            $table->bigInteger('position_id')->comment('map voi master_ data');
            $table->string('avatar', 255)->nullable();
            $table->string('research', 255)->nullable();
            $table->bigInteger('research_id')->nullable()->comment('map voi master_ data type = 18');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->unsignedBigInteger('department_id')->nullable()->comment('user_info_id');
            $table->foreign('department_id')->references('id')->on('department');
            $table->bigInteger('academic_level_id')->nullable()->comment(' trinh do học van map master data type = 5');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info');
    }
}
