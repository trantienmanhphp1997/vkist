<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="">{{__('common.listNameFunction.asset')}} </a> \ <span>
                {{__('data_field_name.inventory_period.title')}}
            </span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.inventory_period.title')}}</h3>
                </div>
                <div class="col-md-6">
                    <div class=" float-right">
                        <button class="btn btn-viewmore-news mr0 mrr16" data-toggle="modal" data-target="#createPeriod"><img src="/images/plus2.svg" alt="plus"> {{__('common.button.create')}} </button>
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div wire:loading class="loader" style="z-index: 3"></div>
                    <div class="table-responsive">
                        <table class="table table-general list-active">
                            <thead>
                                <tr class="border-radius text-uppercase">
                                    <th  scope="col" class="name_w_lg" >{{__('data_field_name.inventory_period.name')}}</th>
                                </tr>
                            </thead>

                            <tbody id="dataTable">
                                @forelse($data as $val)
                                <tr >
                                    <td>{{$val->name}}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-danger p-3" colspan="5">
                                        @if($searchTerm!='')
                                        {{__('common.message.empty_search')}}
                                        @else
                                        {{__('common.message.no_data')}}
                                        @endif
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="list-active">
                        {{$data->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="createPeriod" role="dialog" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: -90px">
            <div class="modal-header" style="border-bottom:0px ">
                <h4 class="modal-title" id="exampleModalLabel">{{__('data_field_name.inventory_period.create')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        wire:click.prevent="resetInput()">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 pd-col">
                            <div class="form-group info-request">
                                <label>{{__('data_field_name.inventory_period.name')}}<span
                                        class="text-danger">(*)</span></label>
                                <input type="text" wire:model.lazy="name">
                                @error('name')
                                    <span class="text-danger" style="font-size: 14px">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="group-btn2 text-center ">
                <button type="submit" class="btn btn-cancel" id="close-modal-create"
                        data-dismiss="modal">{{__('common.button.cancel')}}</button>
                <button type="submit" class="btn btn-save"
                        wire:click.prevent="store()">{{__('common.button.save')}}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.livewire.on('close-modal-create', () => {
        document.getElementById('close-modal-create').click();
    });

    $('#createPeriod').on('hidden.bs.modal', function () {
        window.livewire.emit('resetInput');
    });
</script>
