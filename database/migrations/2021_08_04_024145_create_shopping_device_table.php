<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_device', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shopping_id')->nullable()->constrained('shopping')->comment('yeu cau mua sam');
            $table->string('name', 500)->nullable()->comment('Tên thiết bị');
            $table->integer('quantity')->nullable()->comment('Số lượng');
            $table->tinyInteger('status')->nullable()->comment('Trạng thái thiết bị');
            $table->foreignId('admin_id')->nullable()->constrained('users')->comment("nguoi tao");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_device');
    }
}
