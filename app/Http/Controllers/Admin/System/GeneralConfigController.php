<?php

namespace App\Http\Controllers\Admin\System;

use App\Enums\ESystemConfigType;
use App\Http\Controllers\Controller;
use App\Models\SystemConfig;
use App\Models\UserInfoLeave;
use Illuminate\Http\Request;

class GeneralConfigController extends Controller
{
    public function index()
    {
        return view('admin.system.general-config.index');
    }
}
