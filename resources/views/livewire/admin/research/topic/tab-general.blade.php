<div class="inner-tab position-relative">

    <div class="row">
        <div class="col-sm-6 form-group">
            <label>{{ __('data_field_name.topic.level') }} <span class="text-danger">(*)</span></label>
            {!! Form::select('level_id', ['' => __('common.select-box.choose')] + $topicLevels, null, ['class' => 'form-control', 'wire:model.defer' => 'topic.level_id', 'disabled' => !$editable]) !!}
            @error('topic.level_id') <div class="text-danger">{{ $message }}</div> @enderror
        </div>

        <div class="col-sm-6 form-group">
            <label class="mb-0">{{ __('data_field_name.topic.type') }}: </label>
            <div class="d-flex h-50 align-items-center">
                <div class="form-check">
                    <input wire:model.lazy="topic.type" type="radio" name="type" class="form-check-input topic-type" value="0" id="type-1">
                    <label class="form-check-label" for="type-1">{{ __('data_field_name.topic.new_topic') }}</label>
                </div>
                <div class="form-check mx-auto">
                    <input wire:model.lazy="topic.type" type="radio" name="type" class="form-check-input topic-type" value="1" id="type-2">
                    <label class="form-check-label" for="type-2">{{ __('data_field_name.topic.continuous_topic') }}</label>
                </div>
            </div>
            @error('topic.type') <div class="text-danger">{{ $message }}</div> @enderror
        </div>
    </div>

    <div class="form-group">
        <label>{{ __('data_field_name.topic.source') }} <span class="text-danger">(*)</span></label>
        {{ Form::select('source', ['' => __('common.select-box.choose')] + $topicSources, null, ['class' => 'form-control topic-source', 'wire:model.lazy' => 'topic.source', 'disabled' => !$topicSourceSelectable]) }}
        @error('topic.source') <div class="text-danger">{{ $message }}</div> @enderror
    </div>

    <div id="root-topic-box" class="{{ $rootTopicSelectable ? '' : 'd-none' }}">
        <div class="form-group">
            <label>{{ __('data_field_name.topic.root_topic') }} <span class="text-danger">(*)</span></label>
            @livewire('component.advanced-select', [
                'name' => 'root-topic',
                'modelName' => \App\Models\Topic::class,
                'searchBy' => ['name', 'code'],
                'columns' => ['name', 'code', 'research_category_id', 'major_id', 'necessary', 'overview', 'target', 'space_scope', 'start_date', 'end_date', 'expected_fee'],
                'relations' => ['researchCategory'],
                'conditions' => [
                    ['id', '!=', $topic->id],
                    ['status', '=', \App\Enums\ETopicStatus::STATUS_APPROVAL]
                ],
                'selectedIds' => [$topic->parent_id],
                'placeholder' => __('common.select-box.choose')
            ], key('root-topic-' . $rootTopicSelectable))
            @error('topic.parent_id') <div class="text-danger">{{ $message }}</div> @enderror
        </div>

        <div class="form-group">
            <label>{{ __('data_field_name.topic.field') }}</label>
            <input type="text" class="form-control" disabled value="{{ $selectedRootTopic['research_category']['name'] ?? $topic['researchCategory']['name'] ?? '' }}">
        </div>
    </div>

    <div id="field-box" class="{{ $topicSourceSelectable ? '' : 'd-none' }} form-group">
        <label>{{ __('data_field_name.topic.field') }} <span class="text-danger">(*)</span></label>
        @livewire('component.advanced-select', [
            'name' => 'research-field',
            'modelName' => \App\Models\ResearchCategory::class,
            'searchBy' => ['name'],
            'columns' => ['name'],
            'conditions' => [
                (empty($availableResearchPlanIds) ? null : ['id', 'in', $availableResearchPlanIds])
            ],
            'selectedIds' => [$topic->research_category_id],
            'placeholder' => __('common.select-box.choose')
        ], key('field-' . time()))
        @error('topic.research_category_id') <div class="text-danger">{{ $message }}</div> @enderror
    </div>

    <div class="{{ $ideaSelectable ? '' : 'd-none' }}">
        <div class="form-group">
            <label>{{ __('data_field_name.topic.select_idea') }} <span class="text-danger">(*)</span></label>
            @livewire('component.advanced-select', [
                'name' => 'idea',
                'modelName' => \App\Models\Ideal::class,
                'searchBy' => ['name'],
                'columns' => ['name', 'necessity', 'target', 'code'],
                'conditions' => [
                    ['research_field_id', '=', $topic->research_category_id],
                ],
                'selectedIds' => [$topic->ideal_id],
                'disabled' => empty($topic->research_category_id),
                'placeholder' => __('common.select-box.choose')
            ], key('idea-' . ($topic->research_category_id ?? 0)))
            @error('topic.ideal_id') <div class="text-danger">{{ $message }}</div> @enderror
        </div>

        <div class="form-group">
            <label>{{ __('data_field_name.topic.idea_code') }}</label>
            <input type="text" class="form-control text-danger" disabled value="{{ $selectedIdea['code'] ?? $topic['idea']['code'] ?? '' }}">
        </div>
    </div>

    <div class="form-group">
        <label>{{ __('data_field_name.topic.scientific_name') }} <span class="text-danger">(*)</span></label>
        <select name="topic_code" class="form-control select2" wire:model.defer="topic.major_id" id="select-topic-major">
            <option value="">{{ __('common.select-box.choose') }}</option>
            @foreach ($availableCodes as $parentCode)
                <optgroup label="{{ $parentCode['v_value'] }}">
                    @foreach ($parentCode['children'] as $childCode)
                        <option value="{{ $childCode['id'] }}" {{ $childCode['id'] == $topic->major_id ? 'selected' : '' }}>{{ $childCode['v_value'] }}</option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        @error('topic.major_id') <div class="text-danger">{{ $message }}</div> @enderror
    </div>

    <div class="form-group">
        <label>{{ __('data_field_name.topic.name') }} <span class="text-danger">(*)</span></label>
        <div class="d-flex">
            <input type="text" class="form-control" placeholder="" wire:model.defer="topic.name">
            <button class="btn">
                <img src="/images/pent2.svg" alt=""/>
            </button>
        </div>
        @error('topic.name') <div class="text-danger">{{ $message }}</div> @enderror
    </div>

    <div class="form-group">
        <label>{{ __('data_field_name.topic.necessary') }} <span class="text-danger">(*)</span></label>
        <textarea class="form-control" rows="4" wire:model.defer="topic.necessary"></textarea>
        @error('topic.necessary') <div class="text-danger">{{ $message }}</div> @enderror
    </div>

    <div class="form-group">
        <label>{{ __('data_field_name.topic.overview') }} <span class="text-danger">(*)</span></label>
        <textarea class="form-control" rows="4" wire:model.defer="topic.overview"></textarea>
        @error('topic.overview') <div class="text-danger">{{ $message }}</div> @enderror
    </div>

    @livewire('component.files', ['name' => __('data_field_name.topic.topic_attachment'), 'model_name' => $model, 'model_id' => $topic->id, 'type' => $fileOverview, 'folder' => 'topics'])

    <div class="form-group">
        <label>{{ __('data_field_name.topic.target') }} <span class="text-danger">(*)</span></label>
        <textarea class="form-control" rows="4" wire:model.defer="topic.target"></textarea>
        @error('topic.target') <div class="text-danger">{{ $message }}</div> @enderror
    </div>

    <div class="form-group">
        <label>{{ __('data_field_name.topic.space_scope') }} <span class="text-danger">(*)</span></label>
        <textarea class="form-control" rows="4" wire:model.defer="topic.space_scope"></textarea>
        @error('topic.space_scope') <div class="text-danger">{{ $message }}</div> @enderror
    </div>

    <div class="row">
        <div class="col-sm form-group">
            <label>{{ __('data_field_name.topic.start_date') }} <span class="text-danger">(*)</span></label>
            @livewire('component.date-picker', ['inputId' => 'start-date', 'value' => $topic->start_date], key($topic->start_date ?? 'start-date'))
            @error('topic.start_date') <div class="text-danger">{{ $message }}</div> @enderror
        </div>

        <div class="col-sm form-group">
            <label>{{ __('data_field_name.topic.end_date') }} <span class="text-danger">(*)</span></label>
            @livewire('component.date-picker', ['inputId' => 'end-date', 'value' => $topic->end_date], key($topic->end_date ?? 'end-date'))
            @error('topic.end_date') <div class="text-danger">{{ $message }}</div> @enderror
        </div>
    </div>


    <div class="form-group">
        <label>{{ __('data_field_name.topic.expected_fee') }} (VND) <span class="text-danger">(*)</span></label>
        <span wire:ignore>
            <input type="text" min="0" class="form-control format_number" placeholder="" wire:model.defer="topic.expected_fee" maxlength="18">
        </span>
        @error('topic.expected_fee') <div class="text-danger">{{ $message }}</div> @enderror
    </div>
    @livewire('component.files', ['name' => __('data_field_name.topic.research_documentation_attachment'), 'model_name' => $model, 'model_id' => $topic->id, 'type' => $fileAttachment, 'folder' => 'topics'])

    <script>
        function showBox() {
            let $topicSource = document.querySelector('.topic-source');
            let $topicType = document.querySelector('.topic-type:checked');

        }
    </script>
</div>
