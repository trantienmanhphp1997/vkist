<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function getToken(Request $request)
    {
        $this->validateLogin($request);

        $success = false;
        if (Auth::once($this->credentials($request))) {
            if(!is_null(Auth::user()->status) && Auth::user()->status == 0){
                $this->incrementLoginAttempts($request);
                $data['message'] = trans('auth.locked');
            } else {
                $success = true;
                $data['token'] = auth()->user()->createToken('authToken')->accessToken;
            }
        } else {
            $data['message'] = trans('auth.failed');
        }

        return response()->json(['success' => $success, 'data' => $data]);

    }

    public function username()
    {
        return 'username';
    }
}
