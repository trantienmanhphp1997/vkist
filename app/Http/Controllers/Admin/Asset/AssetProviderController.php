<?php

namespace App\Http\Controllers\Admin\Asset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AssetProvider;


class AssetProviderController extends Controller
{
    public function index(){
        return view('admin.asset.provider.index');
    }
    public function create(){
        return view('admin.asset.provider.create');
    }
    public function edit($id){
        $data = AssetProvider::findOrFail($id);
        return view('admin.asset.provider.edit',compact('data'));
    }
}
