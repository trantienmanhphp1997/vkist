<?php

namespace App\Http\Controllers\Admin\ResearchProject;

use App\Http\Controllers\Controller;
use App\Models\ResearchProject;
use Illuminate\Http\Request;

class ProjectInformationController extends Controller
{
    public function index()
    {
        return view('admin.researchProject.information.index');
    }
    public function getDraftList(){
        return view('admin.researchProject.information.draft');
    }
    public function create()
    {
        return view('admin.researchProject.information.create');
    }

    public function edit($id)
    {
        return view('admin.researchProject.information.edit', ['id' => $id]);
    }

}
