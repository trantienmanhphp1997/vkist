<div class="col-md-10 col-xl-11 box-detail box-user">
	<div class="row">
		<div class="col-md-12">
			<div  class="breadcrumbs">
					<a href="">{{__('research/category-field.breadcrumbs.category-field-management-1')}} \ 
                    </a> <span>{{__('research/expert.list')}}</span>
			</div> 
		</div>
	</div>
	<div class="row bd-border">
		<div class="col-md-12">
			<h3 class="title">
				{{__('research/expert.list-new')}}
                @if($data->total() === 0) <br> <span>{{__('research/category-field.title.no-result')}}</span>  
                @else
                <br>
                <span>
                    {{__('research/expert.title.part-1')}}  {{count($data)}} {{__('research/expert.title.part-2')}} {{$data->total()}}
                </span>
                @endif
			</h3>
			<div class="information">
				<div class="inner-tab pt0">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group search-expertise">
								<div class="search-expertise inline-block">
									<input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control" placeholder="{{__('research/ideal.filter.input.placeholder.search')}}">
									<span>
										<img src="/images/Search.svg" alt="search"/>
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 ">
							@if($checkCreatePermission)
								<div class="form-group float-right inline-block">
									<a href="#" id='aCreateExpert' class="btn btn-viewmore-news mr0 ">
										<img src="/images/plus2.svg" alt="plus">{{__('common.button.create')}}
									</a>
									<button id='btn_create_expert' style="display: none;" 	data-toggle="modal" data-target="#createExpert" wire:click='create'></button>
								</div>
							@endif
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-general">
							<thead>
								<tr class="border-radius">
									<th scope="col" class="border-radius-left">
										{{__('research/expert.table_column.name_expert')}}
									</th>
									<th scope="col" class="border-radius-left">
										{{__('research/expert.table_column.unit')}}
									</th>
									<th scope="col">
										{{__('research/expert.table_column.academic_level')}}
									</th>
									<th scope="col">
										{{__('research/expert.table_column.specialized')}}
									</th>
									<th scope="col" style='max-height: 200px;'>
										{{__('research/expert.table_column.work_unit')}}
									</th>
									<th scope="col">
										{{__('research/expert.table_column.telephone_number')}}
									</th>
									<th scope="col" style='max-height: 200px;'>
										{{__('research/expert.table_column.address')}}
									</th>
									<th scope="col">
										{{__('research/expert.table_column.email')}}
									</th>
									<th scope="col" class="border-radius-right">
										{{__('research/expert.table_column.action')}}
									</th>
								</tr>
							</thead>
							<div wire:loading class="loader"></div>
							<tbody>
								@foreach($data as $row)
									<tr>
										<td>{!! boldTextSearchV2($row->fullname, $searchTerm) !!}</td>
										<td>{{\App\Enums\EUnitExpert::valueToName($row->unit)}}</td>
										<td>
											{{$row->academic->v_value ?? ''}}
										</td>
										<td>
											{{$row->specialize->v_value ?? ''}}
										</td>
										<td>{{$row->department}}</td>
										<td>{!! boldTextSearch($row->phone, $searchTerm) !!}</td>
										<td>{{$row->address}}</td>
										<td>{!! boldTextSearchV2($row->email, $searchTerm) !!}</td>
										<td>
											@php $id = $row->id; @endphp
											@if($checkEditPermission)
											<button type="button" data-toggle="modal" data-target="#createExpert" title="{{__('research/expert.edit')}}" 
													wire:click="editId({{$row->id}})" class="btn-sm border-0 bg-transparent mr-1">
												<img src="/images/edit.png" alt="edit">
											</button>
											@endif
											@include('livewire.common.buttons._delete')
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@if(count($data) > 0)
						{{$data->links()}}
					@else
						<div class="title-approve text-center">
							<span>{{__('common.message.no_data')}}</span>
						</div>
					@endif
					{{-- model delete --}}
				    <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				        <div class="modal-dialog">
				          <div class="modal-content">
				            <div class="modal-body box-user">
								<h4 class="modal-title" id="exampleModalLabel">{{__('notification.member.warning.warning')}}</h4>
				              {{__('notification.member.warning.warning-delete-expert')}}
				            </div>
				            <div class="group-btn2 text-center  pt-12">
				              <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.back')}}</button>
				              <button type="button" class="btn btn-save" data-dismiss="modal" wire:click="delete()">{{__('common.button.delete')}}</button>
				            </div>
				          </div>
				        </div>
				    </div>
                    @include('livewire.admin.research.category.create')
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    $("document").ready(function() {
    	$('#aCreateExpert').click(function(e){
    		e.preventDefault()
    		$('#btn_create_expert').click()
    	})
	    window.livewire.on('close_modal_create', () => {
	        document.getElementById('close-add-member-form-btn').click()
	    });
  	})
</script>
