<?php


namespace App\Enums;

class ECapital
{
    const ODA = 1;
    const STATE_CAPITAL = 2;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::ODA:
                $result =  __('data_field_name.reality_expense.ODA');
                break;
            case self::STATE_CAPITAL:
                $result = __('data_field_name.reality_expense.state_capital');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
