<?php

namespace App\Notifications;

use App\Enums\ENotificationType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class IdeaDeadlineNotification extends BaseNotification
{
    public function __construct()
    {
        $this->type = ENotificationType::IDEA_DEADLINE;
    }

    protected function message($notifiable)
    {
        return [
            'title' => 'Idea Deadline',
            'body' => 'You have 15 days left to submit your idea'
        ];
    }
}
