<?php

namespace App\Http\Livewire\Admin\System\Account;

use App\Models\User;
use Exception;
use Livewire\Component;

class TabGeneral extends Component
{

    public User $user;
    public $username;
    public $userInfoId;
    public $status;
    public $groupId;

    public $selectedUserInfo;

    public $shouldEditCode = true;

    protected $listeners = [
        'validate-data' => 'validateData',
        'changed-user-info-event' => 'selectUserInfo',
        'changed-user-group-event' => 'selectUserGroup',
    ];

    protected function rules()
    {
        return [
            'username' => 'required|max:48|regex:/^[a-zA-Z0-9]+$/|unique:users,username' . (($this->user->exists) ? ",{$this->user->id}" : ''),
            'userInfoId' => 'required|unique:users,user_info_id' . (($this->user->exists) ? ",{$this->user->id}" : ''),
            'status' => 'nullable|boolean',
            'groupId' => 'nullable'
        ];
    }

    protected function validationAttributes()
    {
        return [
            'username' => __('data_field_name.system.account.user_name'),
            'userInfoId' => __('data_field_name.system.account.user_code'),
        ];
    }

    public function mount(User $user)
    {
        $this->user = $user;

        $this->username = $this->user->username;
        $this->userInfoId = $this->user->user_info_id;
        $this->status = $this->user->exists ? $this->user->status == 1 : true;
        $this->groupId = $this->user->group_id;
        $this->selectedUserInfo = $this->user->info;

        $this->shouldEditCode = !$this->user->exists;
    }

    public function render()
    {
        return view('livewire.admin.system.account.tab-general');
    }

    public function validateData()
    {
        try {
            $data = $this->validate();
            $data['userInfo'] = $this->selectedUserInfo;
            $data['status'] = ($data['status']) ? 1 : 0;
            $this->emitUp('valid-data-event', 'tab-general', $data);
        } catch (Exception $exception) {
            $this->emitUp('invalid-data-event', 'tab-general', __('notification.common.fail.invalid_data'));
            throw $exception;
        }
    }

    public function selectUserInfo($userInfo)
    {
        $this->userInfoId = $userInfo['id'] ?? null;
        $this->selectedUserInfo = $userInfo;
    }

    public function selectUserGroup($group) {
        $this->groupId = $group['id'] ?? null;
    }
}
