<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use App\Component\MajorRecursive;
use App\Enums\EIdeaStatus;
use App\Enums\ETypeUpload;
use App\Enums\ETopicSource;
use App\Enums\ETopicStatus;
use App\Enums\ETopicType;
use App\Models\Ideal;
use App\Models\MasterData;
use App\Models\Topic;
use App\Service\Community;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class TabGeneral extends Component
{

    public $model = Topic::class;
    public $editable = true;
    public $topicLevels;
    public $topicSources;

    public $availableCodes;  // ma de tai
    public $availableResearchPlanIds;

    public $selectedIdea;
    public $selectedRootTopic;

    public $topicSourceSelectable = true;
    public $ideaSelectable = true;
    public $rootTopicSelectable = false;

    public $fileOverview;
    public $fileAttachment;

    public Topic $topic;

    public $message;

    protected $listeners = [
        'validate-data-event' => 'validateData',
        'changed-root-topic-event' => 'setRootTopic',
        'changed-idea-event' => 'setIdea',
        'changed-research-field-event' => 'setResearchField',
        'set-start-date' => 'setStartDate',
        'set-end-date' => 'setEndDate',
    ];

    protected function rules()
    {
        return [
            'topic.source' => 'required',
            'topic.type' => 'required',
            'topic.research_category_id' => 'required',
            'topic.major_id' => 'required',
            'topic.name' => 'required|max:100',
            'topic.necessary' => 'required|max:1000',
            'topic.overview' => 'required|max:1000',
            'topic.target' => 'required|max:1000',
            'topic.level_id' => 'required',
            'topic.space_scope' => 'required|max:1000',
            'topic.start_date' => 'required|date',
            'topic.end_date' => 'required|date|after_or_equal:topic.start_date',
            'topic.expected_fee' => 'required|max:14',
            'topic.parent_id' => (($this->rootTopicSelectable) ? [
                'required',
                Rule::unique('topic', 'parent_id')->whereNot('status', ETopicStatus::STATUS_REJECT)->whereNull('deleted_at')->ignore($this->topic->id)
            ] : 'nullable'),
            'topic.ideal_id' => ($this->ideaSelectable ? [
                'required',
                Rule::unique('topic', 'ideal_id')->whereNot('status',  ETopicStatus::STATUS_REJECT)->whereNull('deleted_at')->ignore($this->topic->id)
            ] : 'nullable')
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'topic.source' => __('data_field_name.topic.source'),
            'topic.type' => __('data_field_name.topic.type'),
            'topic.research_category_id' => __('data_field_name.topic.field'),
            'topic.major_id' => __('data_field_name.topic.scientific_name'),
            'topic.name' => __('data_field_name.topic.name'),
            'topic.necessary' => __('data_field_name.topic.necessary'),
            'topic.overview' => __('data_field_name.topic.overview'),
            'topic.target' => __('data_field_name.topic.target'),
            'topic.level_id' => __('data_field_name.topic.level'),
            'topic.space_scope' => __('data_field_name.topic.space_scope'),
            'topic.start_date' => __('data_field_name.topic.start_date'),
            'topic.end_date' => __('data_field_name.topic.end_date'),
            'topic.expected_fee' => __('data_field_name.topic.expected_fee'),
            'topic.parent_id' => __('data_field_name.topic.root_topic'),
            'topic.ideal_id' => __('data_field_name.topic.select_idea')
        ];
    }

    public function mount(Topic $topic = null)
    {
        // khởi tạo loại file
        $this->fileOverview = ETypeUpload::TOPIC_OVERVIEW;
        $this->fileAttachment = ETypeUpload::TOPIC_ATTACHMENT;

        // load dữ liệu cơ bản
        $this->topicLevels = MasterData::where('type', 11)->get()->mapWithKeys(fn ($item) => [$item['id'] => $item['v_value']])->all();

        // load topic
        $this->topic = ($topic != null) ? $topic : new Topic();
        if (!$this->topic->exists) {
            $this->topic->type = ETopicType::NEW_TOPIC;
            $this->topic->source = null;
        } else {
            $this->topic->load(['idea', 'researchCategory']);
            $this->topic->expected_fee = numberFormat($this->topic->expected_fee);
        }

        // lấy mã đề tài theo chuyên ngành khoa học
        $this->availableCodes = $this->getAvailableCodes();
    }

    public function render()
    {

        $this->topicSourceSelectable = $this->topic->type != ETopicType::CONTINUOUS_TOPIC; // chọn được nguồn đề tài <=> loại khác đề tài tiếp tục
        $this->rootTopicSelectable = $this->topic->type == ETopicType::CONTINUOUS_TOPIC; // chọn được đề tài gốc <=> loại đề tài = đề tài tiếp tục
        $this->ideaSelectable = $this->topic->source != ETopicSource::FROM_DIRECTION && $this->topic->type != ETopicType::CONTINUOUS_TOPIC; // chọn được ý tưởng <=> nguồn khác chỉ đạo trực tiếp & đề tài != đề tài tiếp tục

        // load các trường select
        $this->topicSources = $this->getTopicSources();

        $this->dispatchBrowserEvent('component-onload-event');
        return view('livewire.admin.research.topic.tab-general');
    }

    public function validateData()
    {
        $tmpFee = $this->topic->expected_fee;
        $this->topic->expected_fee = Community::getAmount($this->topic->expected_fee ?? '');
        try {
            $this->validate();

            // thiết lập topic code

            // loại đề tài = tạo mới && nguồn đề tài từ ý tưởng hoặc đề xuất => mã đề tài = DT+mã ý tưởng
            if (!$this->topic->exists) {
                if ($this->topic->type == ETopicType::NEW_TOPIC && ($this->topic->source == ETopicSource::FROM_IDEA || $this->topic->source == ETopicSource::FROM_OFFER)) {
                    $this->topic->code = !empty($this->selectedIdea) ? 'DT' . $this->selectedIdea['code'] : 'DT' . Str::random(4);
                } else {
                    // mã đề tài = DT + số tự tăng
                    $lastIncreaseCode = DB::table('topic')->whereNotNull('increase_code')->orderBy('created_at', 'desc')->first('increase_code')->increase_code ?? 0;
                    $this->topic->increase_code = ++$lastIncreaseCode;
                    $this->topic->code = 'DT' . ($lastIncreaseCode);
                }
            }

            $this->emitUp('validated-data-event', 'tab-general', $this->topic->getDirty());
        } catch (\Exception $exception) {
            $this->topic->expected_fee = $tmpFee;
            $this->emitUp('invalid-data-event', 'tab-general', __('data_field_name.topic.error_tab_general'));
            throw $exception;
        }
    }

    public function getAvailableCodes($keyword = null)
    {
        $recursive = new MajorRecursive(MasterData::query()->where('type', 12)->get());
        return $recursive->data;
    }

    public function getTopicSources()
    {
        if ($this->topic->type == ETopicType::CONTINUOUS_TOPIC) {
            $this->topic->source = ETopicSource::FROM_DIRECTION;
            return [
                ETopicSource::FROM_DIRECTION => __('data_field_name.topic.topic_from_direction')
            ];
        } else {
            return [
                ETopicSource::FROM_IDEA => __('data_field_name.topic.topic_from_idea'),
                ETopicSource::FROM_OFFER => __('data_field_name.topic.topic_from_offer'),
                ETopicSource::FROM_DIRECTION => __('data_field_name.topic.topic_from_direction')
            ];
        }
    }

    public function setStartDate($value)
    {
        $this->topic->start_date = date('Y-m-d', strtotime($value['start-date']));
    }

    public function setEndDate($value)
    {
        $this->topic->end_date = date('Y-m-d', strtotime($value['end-date']));
    }

    public function setRootTopic($value)
    {
        $this->selectedRootTopic = $value;
        $this->topic->parent_id = $value['id'] ?? null;
        $this->topic->name = $value['name'] ?? null;
        $this->topic->research_category_id = $value['research_category']['id'] ?? null;
        $this->topic->major_id = $value['major_id'] ?? null;
        $this->topic->necessary = $value['necessary'] ?? null;
        $this->topic->overview = $value['overview'] ?? null;
        $this->topic->target = $value['target'] ?? null;
        $this->topic->space_scope = $value['space_scope'] ?? null;
        $this->setStartDate(['start-date' => $value['start_date'] ?? date('Y-m-d')]);
        $this->setEndDate(['end-date' => $value['end_date'] ?? date('Y-m-d')]);
        $this->topic->expected_fee = numberFormat($value['expected_fee'] ?? 0);

        $this->emitTo(TabPresentation::class, 'changed-root-topic-event', $value['id'] ?? null);
        $this->emitTo(TabMember::class, 'changed-root-topic-event',  $value['id'] ?? null);
        $this->emitTo(TabExpectedResult::class, 'changed-root-topic-event',  $value['id'] ?? null);
    }

    public function setIdea($value)
    {
        $this->selectedIdea = $value;
        $this->topic->ideal_id = $value['id'] ?? null;
        $this->topic->name = $value['name'] ?? '';
        $this->topic->necessary = $value['necessity'] ?? '';
        $this->topic->target = $value['target'] ?? '';
    }

    public function setResearchField($value)
    {
        $this->topic->research_category_id = $value['id'];
    }

    public function updatedTopicType()
    {
        if ($this->topic->type == ETopicType::NEW_TOPIC) {
            $this->topic->research_category_id = null;
            $this->topic->parent_id = null;
            $this->setRootTopic(null);
        } elseif ($this->topic->type == ETopicType::CONTINUOUS_TOPIC) {
            $this->topic->ideal_id = null;
        }
    }

    public function updatedTopicSource() {
        if($this->topic->source == ETopicSource::FROM_DIRECTION) {
            $this->availableResearchPlanIds = [];
        } else {
            $this->availableResearchPlanIds = Ideal::query()->where('status', EIdeaStatus::APPROVED)->groupBy('research_field_id')->get('research_field_id')->pluck('research_field_id')->all();
        }
    }
}
