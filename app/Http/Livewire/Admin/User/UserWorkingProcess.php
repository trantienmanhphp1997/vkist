<?php

namespace App\Http\Livewire\Admin\User;

use App\Models\UserWorkProcess;
use App\Models\UserInf;
use Livewire\Component;
use Illuminate\Support\Facades\Storage;
use App\Exports\UserWorkingProcessExport;
use Excel;
use Illuminate\Support\Facades\Config;
class UserWorkingProcess extends Component
{
    public $user_info_id;
    public $listFile = [];

    public function render()
    {
        $data = UserWorkProcess::where('user_info_id', $this->user_info_id)
            ->with(['position', 'department'])
            ->orderBy('user_working_process.id', 'desc')
            ->get();
        return view('livewire.admin.user.user-working-process', ['data' => $data]);
    }
    public function getListFile($id) {
        $userWorkAccess = UserWorkProcess::where('id', $id)->with('userInfo.files')->first();
        if(!is_null($userWorkAccess)) {
            $this->listFile = $userWorkAccess->userInfo->files;
        }
        $this->dispatchBrowserEvent('openDownloadFileModal');
    }
    public function download($url, $name) {
        return response()->download('storage/'.$url, $name);

    }

    public function export() {
        $user = UserInf::where('id', $this->user_info_id)->first();
        if (!empty($user)) {
            $nameFile = $user->fullname . '-working-access-' . now()->format(Config::get('common.year-month-day')) . '-' . '.xlsx';
        } else {
            $nameFile = 'working-access-' . now()->format(Config::get('common.year-month-day')) . '-' . '.xlsx';
        }
        return Excel::download(new UserWorkingProcessExport($this->user_info_id), $nameFile);
    }
}
