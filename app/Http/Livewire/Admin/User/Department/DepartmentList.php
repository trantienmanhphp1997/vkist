<?php

namespace App\Http\Livewire\Admin\User\Department;

use App\Component\DepartmentRecursive;
use App\Models\Department;
use App\Component\Recursive;
use App\Enums\EDepartment;
use App\Exports\DepartmentsExport;
use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class DepartmentList extends BaseLive
{
    public $deleteId;
    public $searchTerm;

    public $action;
    public $departmentId;

    public function render()
    {
        $query = Department::query();
        if (strlen($this->searchTerm)) {
            $query->where('department.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')
                ->orWhereRaw("LOWER(name) LIKE LOWER('%{$this->searchTerm}%')");
        }
        $data = $query->withCount('users')->orderBy('id', 'desc')->paginate($this->perPage);

        foreach ($data as $item) {
            $item->class = '';
            $item->group = '';
            $item->unit = '';

            if ($item->type == EDepartment::TYPE_CLASS) {
                $item->class = boldTextSearch($item->name, $this->searchTerm);
            } elseif ($item->type == EDepartment::TYPE_GROUP) {
                $item->class = $item->parentDepartment->name ?? '';
                $item->group = boldTextSearch($item->name, $this->searchTerm);
            } elseif ($item->type == EDepartment::TYPE_UNIT) {
                $item->class = $item->parentDepartment->parentDepartment->name ?? '';
                $item->group = $item->parentDepartment->name ?? '';
                $item->unit = boldTextSearch($item->name, $this->searchTerm);
            }
        }

        return view('livewire.admin.user.department.department-list', [
            'data' => $data,
            'hierarchyDeparment' => $this->getHierarchyDepartment()
        ]);
    }

    public function getHierarchyDepartment()
    {
        return DepartmentRecursive::hierarch(Department::all());
    }


    public function getIdDelete($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        if ($this->checkDestroyPermission) {
            if (empty($this->getIdSelected)) {
                $department = Department::where('id', $this->deleteId)->get();
            } else {
                $department = Department::whereIn('id', $this->getIdSelected)->get();
            }

            $departmentHasChildren = Department::has('childrenDepartment')->get();
            $departmentHasUser = Department::has('users')->get();

            if (($departmentHasChildren->intersect($department)->isNotEmpty())) {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.delete_department_has_children')]);
            } elseif ($departmentHasUser->intersect($department)->isNotEmpty()) {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.delete_department_has_user')]);
            } else {
                foreach ($department as $department) {
                    $department->delete();
                }
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete_department_success')]);
            }
        } else {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.no_delete_permission')]);
        }
    }

    public function export()
    {
        $today = date("d_m_Y");
        return Excel::download(new DepartmentsExport($this->searchTerm), 'list-department-' . $today . '.xlsx');
    }

    public function action($action, $departmentId = 0)
    {
        $this->action = $action;
        $this->departmentId = $departmentId;
    }
}
