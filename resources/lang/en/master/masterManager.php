<?php

return [
    'menu_name' => [
        'type' => 'Type',
        'master'=>'Manager master data',
        'master_title' => 'Master Data List',
        'master_title_table'=>[
            'ID' => 'Code',
            'vkey'=>'Data type name',
            'vvalue'=> 'Data of type (Vi)',
            'ordernumber'=>'Ordernumber',
            'type' => 'Value type',
            'vvalueen' => 'Data of type (En)',
            'parentid' => 'Parent code',
            'action' => 'Action',
        ],
        'category_page'=>'category master-data',
    ],
    'form_data'=>[
        'create'=>'Create Master Data',
        'edit'  => 'Edit Master Data',
    ],
];
