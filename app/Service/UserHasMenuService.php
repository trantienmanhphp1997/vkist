<?php

namespace App\Service;

use App\Models\UserHasMenu;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\This;

class UserHasMenuService
{
    public static function getMenu()
    {
        return UserHasMenu::where(['admin_id' => Auth::id()])->with(['menu'])->get();
    }

    public static function removeMenuByAdminID()
    {
        $menuChecked = UserHasMenu::where(['admin_id'=>Auth::id()])->orderBy('id', 'DESC')->get();
        foreach ($menuChecked as $page){
            UserHasMenu::findOrFail($page->id)->delete();
        }
    }

    public static function updateMenu($menuChecked)
    {
        static::removeMenuByAdminID();
        if (!empty($menuChecked)){
            foreach ($menuChecked as $page) {
                $formData = [
                    'admin_id' => Auth::id(),
                    'menu_id' => $page
                ];
                UserHasMenu::create($formData);
            }
        }
    }
}
