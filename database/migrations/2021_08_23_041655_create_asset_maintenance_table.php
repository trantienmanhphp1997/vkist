<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetMaintenanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_maintenance', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('phiếu sửa chữa bảo dưỡng');
            $table->foreignId('asset_id')->nullable()->constrained('asset')->comment('map vs bnagr asset');
            $table->string('number_report')->unique()->comment('số biên bản');
            $table->tinyInteger('status')->nullable()->comment('1:sửa chữa, 2: bảo dưỡng');
            $table->date('implementation_date')->nullable()->comment('ngay thực hiện');
            $table->date('estimated_completion_date')->nullable()->comment('ngay dự kiến hoàn thành');
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('người thực hiện, map user_info');
            $table->foreignId('asset_provider_id')->nullable()->constrained('asset_provider')->comment('đơn vị thực hiện, map asset_provider');
            $table->tinyInteger('finished ')->nullable()->comment('đã hoàn thành ');
            $table->bigInteger('estimated_cost')->nullable()->comment('chi phí dự kiến');
            $table->foreignId('content_maintenance_id')->nullable()->constrained('asset_rule_maintenance')->comment('nội dung bảo dưỡng');
            $table->string('content_repair')->nullable()->comment('nội dung sửa chữa');
            $table->string('detailed_description')->nullable()->comment('mô tả chi tiết');
            $table->tinyInteger('place')->nullable()->comment('địa điểm sửa chữa/bảo dưỡng,1:tại đơn vị, 2: tại nhà cung cấp');

            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_maintenance');
    }
}
