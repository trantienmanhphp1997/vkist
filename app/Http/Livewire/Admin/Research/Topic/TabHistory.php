<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use Livewire\Component;

class TabHistory extends Component
{
    public function render()
    {
        return view('livewire.admin.research.topic.tab-history');
    }
}
