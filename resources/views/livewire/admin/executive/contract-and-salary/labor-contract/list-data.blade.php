<div class="col-md-10 col-xl-11 box-detail box-user">
	<div class="row">
		<div class="col-md-12">
				<div  class="breadcrumbs"><a href="{{route('admin.research.ideal.index')}}">{{__('executive/contract.breadcrumbs.activity-management')}}</a> \ <span>{{__('executive/contract.list')}}</span></div> 
		</div>
	</div>
	<div class="row bd-border">
		<div class="col-md-12">
			<h3 class="title">{{__('executive/contract.list')}}</h3>
			<div class="information">
				<div class="inner-tab pt0">
					<div class="row">
						<div class="col-md-2">
							<div class="form-group search-expertise">
								<div class="search-expertise inline-block">
									<input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control w-100" placeholder="{{__('research/ideal.filter.input.placeholder.search')}}">
									<span>
										<img src="/images/Search.svg" alt="search"/>
									</span>
								</div>
								
							</div>
						</div>
						<div class="col-md-2">
							<div class="inline-block">
								<select wire:model.debounce.1000ms="filterStatus" class="form-control form-control-lg size13">
									<option value=''>
										{{__('executive/contract.filter.select-box.option.status')}}
									</option>
									<option value='1'>
										{{__('executive/contract.status.active')}}
									</option>
									<option value='-1'>
										{{__('executive/contract.status.off')}}
									</option>
								</select>
							</div>
						</div>
						<div class="col-md-2 px-8">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'from_date','place_holder' => __('research/achievement.label.from'), 'set_null_when_enter_invalid_date' => true ])
                        </div>
                        <div class="col-md-2 px-8">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'to_date' ,'place_holder' => __('research/achievement.label.to'), 'set_null_when_enter_invalid_date' => true ])
                        </div>
						<div class="col-md-3">
							@if($checkCreatePermission)
								<div class="form-group float-right">
									<button class="btn btn-viewmore-news mr0" wire:click="openModalCreate">
										<img src="/images/plus2.svg" alt="plus">{{__('common.button.create')}}
									</button>
								</div>
							@endif
						</div>
						@if(checkRoutePermission('download'))
							<div class="col-md-1">
								<div class="form-group float-right">
		                            <button type="button" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#exampleModal">
		                                <img src="/images/filterdown.svg" alt="filterdown">
		                            </button>
		                        </div>
							</div>
						@endif
					</div>
					<div class="table-responsive">
						<table class="table table-general">
							<thead>
								<tr class="border-radius">
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.user_name')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.code')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.position')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.contract_type')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.duration')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.start_date')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.end_date')}}
									</th>
									<th scope="col" class="text-center">
										{{__('executive/contract.table_column.status')}}
									</th>
									<th scope="col" class="border-radius-right">
										{{__('executive/contract.table_column.action')}}
									</th>
								</tr>
							</thead>
							<div wire:loading class="loader"></div>
							<tbody>
								@foreach($data as $row)
									<tr>
										<td class="text-center">{!! boldTextSearch($row->user_name, $searchTerm) !!}</td>
										<td class="text-center">{!! boldTextSearch($row->contract_code, $searchTerm) !!}</td>
										<td class="text-center">{!! $row->position_value !!}</td>
										<td class="text-center">{{App\Enums\EContractType::valueToName($row->contract_type)}}</td>
										<td class="text-center">{!! $row->duration !!}</td>
										<td class="text-center">{!! Illuminate\Support\Carbon::parse($row->start_date)->format(Config::get('common.formatDate')) !!}</td>
										<td class="text-center">{!! !empty($row->end_date) ? Illuminate\Support\Carbon::parse($row->end_date)->format(Config::get('common.formatDate')) : null !!}</td>
										<td class="text-center">
											@if($row->status == App\Enums\EContractStatus::ACTIVE)
												<div class="in-process select-status-project">
													{{App\Enums\EContractStatus::valueToName($row->status)}}
												</div>
											@else
												<div class="cancel group-task select-status-project">
													{{App\Enums\EContractStatus::valueToName($row->status)}}
												</div>
											@endif
										</td>
										<td>
											@if($checkEditPermission)
												<button type="button" class="btn par6" title="{{__('common.button.edit')}}" wire:click="openModalCreate({{$row->id}})">
													<img src="/images/pent2.svg" alt="">
												</button>
											@endif
											@if($checkDestroyPermission)
												<button type="button" class="btn par6" title="{{__('common.button.delete')}}" wire:click="$set('deleteId', {{$row->id}})" data-target="#exampleDelete" data-toggle="modal">
													<img src="/images/trash.svg" alt="trash">
												</button>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@if(count($data) > 0)
						{{$data->links()}}
					@else
						<div class="title-approve text-center ml-0">
							<span>{{__('common.message.empty_search')}}</span>
						</div>
					@endif
					@include('livewire.common._modalDelete')
					@include('livewire.admin.executive.contract-and-salary.labor-contract.modal-save')
					@include('livewire.common._modalExportFile')
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    $("document").ready(() => {
        window.livewire.on('showModalCreate', (start_date, end_date) => {
        	$('#startDateDisable').val(start_date);
        	$('#endDateDisable').val(end_date);
        	$('#themoihdld').modal('show');
        });
        window.livewire.on('showModalSetting', () => {
        	$('#themoihdld').modal('hide');
        	$('#themgiatri').modal('show');
        });
        $('#themgiatri').on('hidden.bs.modal', () => {
		 	$('#themoihdld').modal('show');
		});
		window.livewire.on('closeModalCreate', () => {
        	$('#themoihdld').modal('hide');
        });
        window.livewire.on('closeModelSetting', () => {
        	$('#themgiatri').modal('hide');
        });
    });
</script>
