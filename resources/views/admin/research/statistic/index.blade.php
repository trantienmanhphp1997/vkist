@extends('layouts.master')
<title>{{__('menu_management.menu_name.research_reporting')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
@livewire('admin.research.statistic.statistic')
@endsection
