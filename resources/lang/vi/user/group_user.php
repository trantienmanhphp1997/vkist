<?php
return [
    'add-topic' => 'Thêm Đề tài',
    'title' => [
        'main-title' => 'Quản lý và Ủy quyền Người dùng',
        'create' => 'Tạo mới nhóm người dùng',
        'update' => 'Cập nhật nhóm người dùng',
        'list' => 'Danh sách nhóm người dùng',
        'delete' => 'Bạn có chắc muốn xóa nhóm người dùng này ??',
        'view' => 'Chi tiết nhóm người dùng',
        'pick-topic' => 'Chọn đề tài',
        'pick-research-category' => 'Chọn lĩnh vực nghiên cứu',
        'pick-all-topic' => 'Chọn tất cả đề tài của lĩnh vực'
    ],
    'tab1' => 'Thông tin chung',
    'tab2' => 'Danh sách đề tài',
    'form-data' => [
        'name' => 'Tên nhóm',
        'code' => 'Mã nhóm',
        'description' => 'Mô tả'
    ],
    'table' => [
        'topic-code' => 'Mã đề tài',
        'topic-name' => 'Tên đề tài',
        'option' => 'Lựa chọn',
    ],
    'msg' => [
        'success-save' => 'Tạo mới nhóm thành công',
        'success-update' => 'Cập nhật nhóm thành công',
        'delete-success' => 'Xóa nhóm người dùng thành công',
        'exist-topic' => 'Đề tài này đã tồn tại trong danh sách',
        'delete-topic-success' => 'Xóa đề tài thành công',
        'add-topic-success' => 'Thêm đề tài vào danh sách thành công'
    ]
];
