<?php

namespace App\Http\Controllers\Admin\News;

use App\Enums\ERouteName;
use App\Http\Controllers\Controller;
use App\Models\CategoryNews;
use App\Models\News;
use App\Models\UserInf;
use App\Service\UserHasMenuService;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    const UPDATE = 1;

    public function create()
    {

        return view('admin.news.create');
    }

    public function edit($id)
    {
        return view('admin.news.edit', ['id' => $id]);
    }

    public function getListPosted()
    {
        return view(ERouteName::ROUTE_NEWS_INDEX, ['status' => News::NEWS_POSTED]);
    }

    public function getListDraft()
    {
        return view(ERouteName::ROUTE_NEWS_INDEX, ['status' => News::DRAFT_NEWS]);
    }

    public function getListNeedApproval()
    {
        return view(ERouteName::ROUTE_NEWS_INDEX, ['status' => News::NEWS_NEED_APPROVAL]);
    }

    public function getListApproved()
    {
        return view(ERouteName::ROUTE_NEWS_INDEX, ['status' => News::APPROVED_NEWS]);
    }

    public function getDetail($categorySlug, $slug)
    {
        $new = News::query()->with(['category', 'admin'])
            ->where(['slug' => $slug])->first();
        $newsPerDayId = session('newsPerDayId');

        if (array_search($new->id, $newsPerDayId) !== false){
            unset($newsPerDayId[array_search($new->id, $newsPerDayId)]);
            session(['newsPerDayId' => $newsPerDayId]);
        }

        $new->increment('count_read');
        $categorys = CategoryNews::all();
        return view('admin.news.detail', ['new' => $new, 'categorySlug' => $categorySlug, 'categorys' => $categorys]);
    }

    public function index($categorySlug = null)
    {
        $categorys = CategoryNews::all();
        $newsLeft = \App\Models\News::query()
            ->with(['admin', 'category'])
            ->where(['status' => \App\Models\News::APPROVED_NEWS])
            ->where(['published' => 1])
            ->where(['is_trending' => 1])
            ->orderBy('id', 'DESC')
            ->take(5)->get();
        return view('admin.news.list', ['news' => $newsLeft, 'categorys' => $categorys, 'categorySlug' => $categorySlug]);
    }
}
