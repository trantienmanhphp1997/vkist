<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

use DB;

class AssetCategory extends BaseModel
{
    use HasFactory;
    protected $table='asset_category';
    protected $fillable=['code', 'name','group_id','parent_id','lost_rate','user_year','note','status', 'admin_id','unsign_text'];

    private static $searchable = [
        'name',
    ];

    public static function getListSearchAble()
    {
        return self::$searchable;
    }

    public function asset() {
        return $this->hasMany(Asset::class,'category_id','id')
            ->leftJoin('user_info','user_info.id','=','asset.category_id')
            ->leftJoin('department','department.id','=','user_info.department_id')
            ->select('asset.*', 'fullname',DB::raw('department.name as depart_name'));
    }

    public function assetValue(){
        return $this->hasMany(Asset::class, 'category_id');
    }

}
