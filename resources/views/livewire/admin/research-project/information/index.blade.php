<div class="col-md-10 col-xl-11 box-detail box-user">
    <style>
        .select-status-project {
            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }
        .title-approve{
            margin: 0 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a
                    href="{{ route('admin.research-project.information.index') }}">{{__('research/project.name')}}</a> \
                <span>{{__('research/project.information.breadcrumbs')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('research/project.information.table-title')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group search-expertise">
                                <div class="search-expertise inline-block">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control"
                                           placeholder="{{__('research/project.information.search')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @if($checkCreatePermission)
                        <div class="col-md-2 ">
                            <div class="form-group float-right">
                                <a href="{{ route('admin.research-project.information.create') }}"
                                   class="btn btn-viewmore-news mr0 ">
                                    <img src="/images/plus2.svg" alt="plus">{{__('research/project.information.create')}}
                                </a>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div wire:loading class="loader" style="z-index: 10"></div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                            <tr class="border-radius">
                                <th scope="col" class="border-radius-left">
                                    {{ __('data_field_name.topic.stt') }}
                                </th>
                                <th scope="col">
                                    {{__('research/project.information.table-column.code-project')}}
                                </th>
                                <th scope="col"  class="w-25">
                                    {{__('research/project.information.table-column.name')}}
                                </th>
                                <th scope="col" >
                                    {{__('research/project.information.table-column.research-category')}}
                                </th>
                                <th scope="col" >
                                    {{__('research/project.information.tab-project.department')}}
                                </th>
                                <th scope="col">
                                    {{__('research/project.information.table-column.leader')}}
                                </th>
                                <th scope="col">
                                    {{__('research/project.information.table-column.status')}}
                                </th>
                                <th scope="col" style="width: 250px">
                                    {{__('research/project.information.tab-project.note')}}
                                </th>
                                <th scope="col">
                                </th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>

                            @foreach ($data as $row)
                                <tr>
                                    <td >{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                    <td >
                                        <a href="{{ route('admin.research-project.information.edit', $row->id) }}">
                                            {!! boldTextSearch($row->project_code, $searchTerm) !!}
                                        </a>
                                    </td>
                                    <td >
                                        {!! boldTextSearch($row->name, $searchTerm) !!}
                                    </td>
                                    <td >{!! boldTextSearch(($row->researchCategory->name ?? 'Chưa xác định'), $searchTerm) !!}</td>
                                    <td>{!! boldTextSearch(($row->department->name ?? 'Chưa xác định'), $searchTerm) !!}</td>
                                    <td >{!! boldTextSearch(( $row->topic->leader->fullname ?? 'Chưa xác định'), $searchTerm) !!}</td>
                                    <td >
                                        {{
                                            Form::select('status',
                                            array(
                                                0=> __('research/project.status.wait_approval') ,
                                                1=>__('research/project.status.in_process'),
                                                2=>__('research/project.status.completed'),
                                                3=>__('research/project.status.cancel')
                                                ),  $row->status,
                                                ['class'=> ($row->status == 0 ? 'wait-approval' :
                                                ($row->status == 1 ? 'in-process' :
                                                ($row->status == 2 ? 'completed' : (
                                                    $row->status == 3 ?  'cancel' : ''
                                                ))).'').' select-status-project',
                                                'disabled'
                                                ]
                                            )}}
                                    </td>
                                    <td  class="over_flow">
                                        {{$row->note}}
                                    </td>
                                    @if(checkButtonCanView('admin.research-project.information.edit') || $row->topic->leader->account->id == \Illuminate\Support\Facades\Auth::id())
                                        <td>
                                            <a class="btn par6" data-toggle="modal" data-target="#transferToDraft"
                                            wire:click="setProjectID({{$row->id}})">
                                                <img width="40px" src="/images/fileIcon.png" alt="file">
                                            </a>
                                        </td>
                                    @endif

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if (count($data) > 0)
                        {{ $data->links() }}
                    @else
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    @endif
                    <div wire:ignore.self class="modal fade" id="transferToDraft" tabindex="-1"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header" style="padding-left: 0">
                                    <strong>
                                        <h5 class="modal-title"
                                            id="exampleModalLabel">{{__('notification.member.warning.warning')}}</h5>
                                    </strong>
                                </div>
                                <div class="modal-body" style="font-size: 16px">
                                    {{__('research/project.error.warning-saving-draft')}}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-cancel" style="    padding: 8px 20px;"
                                            data-dismiss="modal">{{__('common.button.back')}}</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"
                                            wire:click="transferToStorage">{{__('common.button.save')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
