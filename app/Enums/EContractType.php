<?php


namespace App\Enums;


class EContractType
{
    // ban dau062 có 6 loại, sau này update lại thành 2
    const WORK = 1;
    const LABOR = 2;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::WORK:
                $result = __('data_field_name.labor-contract.work');
                break;
            case self::LABOR:
                $result = __('data_field_name.labor-contract.labor');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
