<?php


namespace App\Enums;

class ESalarySheetsFileType
{
    const BASIC_SALARY = 1;
    const POSITION_SALARY = 2;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::BASIC_SALARY:
                $result = __('data_field_name.import-salary.basic');
                break;
            case self::POSITION_SALARY:
                $result = __('data_field_name.import-salary.position');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
