<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="breadcrumbs"><a href="{{ route('admin.approval.index') }}">{{ __('data_field_name.approval_topic.text_manager_approval') }} </a> \ <span>{{ __('data_field_name.approval_topic.text_edit_approval') }}</span></div>
    <div class="creat-box bg-content border-20">
        <div class="detail-task pt-44">
            <form wire:submit.prevent="submit" autocomplete="off">
                <h4>{{ __('data_field_name.approval_topic.text_edit_approval') }}</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{ __('data_field_name.approval_topic.name_profile') }}</label>
                            <input type="text" class="form-control" wire:model.defer="name" @if (!$readonly) readonly @endif>
                            @error('name') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('data_field_name.common_field.status') }}</label>
                            <select class="form-control form-control-lg" wire:model.lazy="status">
                                @foreach ($statusAppraisal as $key => $textStatus)
                                    <option value='{!! $key !!}'>{!! $textStatus !!}</option>
                                @endforeach
                            </select>
                            @error('status') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('data_field_name.approval_topic.profile_type') }}</label>
                            <select class="form-control form-control-lg profile-type" wire:model.lazy="type" wire:click="changeType()">
                                @foreach ($typeAppraisal as $key => $val)
                                    @if ($key == $approval->type)
                                        <option value='{!! $key !!}'>{!! $val !!}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('type') <span class="error text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div wire:loading class="loader"></div>
                    @if ($type == \App\Enums\EApproval::TYPE_TOPIC || $type == \App\Enums\EApproval::TYPE_PLAN || $type == \App\Enums\EApproval::TYPE_COST)
                        <div class="row code-file px-16">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.approval_topic.profile_code') }}</label>
                                    <input type="text" class="form-control form-input" wire:model.defer="code" @if ($status != \App\Enums\EApproval::STATUS_COMPLETE_PROFILE) readonly @endif placeholder="{{ __('data_field_name.approval_topic.text_code') }}">
                                    @error('code') <span class="error text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.contract.code') }}</label>
                                    <input type="text" class="form-control form-input" wire:model.defer="contract_code" @if ($approval->status != \App\Enums\EApproval::STATUS_APPROVAL) readonly @endif placeholder="{{ __('data_field_name.approval_topic.text_contract_code') }}">
                                    @error('contract_code') <span class="error text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    @if ($type == \App\Enums\EApproval::TYPE_TOPIC)
                                        <label>{{ __('data_field_name.topic.code') }}</label>
                                        <select class="form-control form-control-lg" wire:model.lazy="topic_id">
                                            @if (!empty($listTopic))
                                                @if ($status == \App\Enums\EApproval::STATUS_NEW)
                                                    <option value="">{{ __('data_field_name.common_field.select_default') }}</option>
                                                    @foreach ($listTopic as $key => $topic)
                                                        <option value='{!! $key !!}'>{!! $topic['code'] !!}</option>
                                                    @endforeach
                                                @else
                                                    @foreach ($listTopic as $key => $topic)
                                                        @if ($topic_id == $key)
                                                            <option value='{!! $key !!}'>{!! $topic['code'] !!}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                        @error('topic_id') <span class="error text-danger">{{ $message }}</span>@enderror
                                    @endif
                                    @if ($type == \App\Enums\EApproval::TYPE_COST)
                                        <label>{{ __('data_field_name.approval_topic.type_cost_estimate') }}</label>
                                        <select class="form-control form-control-lg" wire:model.lazy="topic_fee_id">
                                            @if (!empty($listTopicFee))
                                                @if ($status == \App\Enums\EApproval::STATUS_NEW)
                                                    <option value="">{{ __('data_field_name.common_field.select_default') }}</option>
                                                    @foreach ($listTopicFee as $key => $topicFee)
                                                        <option value='{!! $key !!}'>{!! $topicFee['name'] !!}</option>
                                                    @endforeach
                                                @else
                                                    @foreach ($listTopicFee as $key => $topicFee)
                                                        @if ($topic_fee_id == $key)
                                                            <option value='{!! $key !!}'>{!! $topicFee['name'] !!}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                        @error('topic_fee_id') <span class="error text-danger">{{ $message }}</span>@enderror
                                    @endif
                                    @if ($type == \App\Enums\EApproval::TYPE_PLAN)
                                        <label>{{ __('data_field_name.approval_topic.type_research_plan') }}</label>
                                        <select class="form-control form-control-lg" wire:model.lazy="research_plan_id">
                                            @if (!empty($listPlan))
                                                @if ($status == \App\Enums\EApproval::STATUS_NEW)
                                                    <option value="">{{ __('data_field_name.common_field.select_default') }}</option>
                                                    @foreach ($listPlan as $key => $plan)
                                                        <option value='{!! $key !!}'>{!! $plan['name'] !!}</option>
                                                    @endforeach
                                                @else
                                                    @foreach ($listPlan as $key => $plan)
                                                        @if ($research_plan_id == $key)
                                                            <option value='{!! $key !!}'>{!! $plan['name'] !!}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                        @error('research_plan_id') <span class="error text-danger">{{ $message }}</span>@enderror
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    @if ($type == \App\Enums\EApproval::TYPE_TOPIC)
                                        <label>{{ __('data_field_name.topic.name') }}</label>
                                        <div class="content-link form-control form-input">
                                            @if ($topic_id != null)
                                                <a href="{{ route('admin.research.topic.edit', $topic_id) }}">{{ $listTopic[$topic_id]['name'] }}</a>
                                            @endif
                                        </div>
                                    @endif
                                    @if ($type == \App\Enums\EApproval::TYPE_COST)
                                        <label>{{ __('data_field_name.research_cost.name_topic_fee') }}</label>
                                        <div class="content-link form-control form-input">
                                            @if ($topic_fee_id != null)
                                                <a href="{{ route('admin.research.topic-fee.edit', $topic_fee_id) }}">{{ $listTopicFee[$topic_fee_id]['name'] }}</a>
                                            @endif
                                        </div>
                                    @endif
                                    @if ($type == \App\Enums\EApproval::TYPE_PLAN)
                                        <label>{{ __('data_field_name.approval_topic.type_research_plan') }}</label>
                                        <div class="content-link form-control form-input">
                                            @if ($research_plan_id != null)
                                                <a href="{{ route('admin.research.plan.research_plan.edit', $research_plan_id) }}">{{ $listPlan[$research_plan_id]['name'] }}</a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.approval_topic.appraisal_board') }}</label>
                                    <select class="form-control form-control-lg" wire:model.lazy="appraisal_board_id">
                                        @if ($appraisal_board_id == null)
                                            <option value="">{{ __('data_field_name.approval_topic.not_create_appraisal') }}</option>
                                        @else
                                            <option value="{{ $appraisal_board_id }}">{{ $appraisalName }}</option>
                                        @endif
                                    </select>
                                    @error('appraisal_id') <span class="error text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    @if ($approval->status == \App\Enums\EApproval::STATUS_COMPLETE_PROFILE)
                                        @if ($appraisal_board_id == null)
                                            <button type="button" class="btn btn-main bg-7 mt-32 text-5 size14 py-12 px-15 w-100 buttonCreate" data-toggle="modal" data-target="#appraisal-modal">
                                                <img src="/images/Plus3.svg" alt="plus">{{ __('data_field_name.approval_topic.text_create_appraisal') }}
                                            </button>
                                            <p><span class="error text-danger">*{{ __('data_field_name.approval_topic.not_create_appraisal') }}</span></p>
                                        @else
                                            <button type="button" class="btn btn-main bg-7 mt-32 text-5 size14 py-12 px-15 w-100 buttonEdit" data-toggle="modal" data-target="#appraisal-modal">
                                                <img src="/images/pent2.svg" alt="plus">{{ __('data_field_name.approval_topic.text_edit_appraisal') }}
                                            </button>
                                        @endif
                                    @endif

                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($type == \App\Enums\EApproval::TYPE_IDEAL)
                        <div class="col-md-12 list-idea px-16">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('data_field_name.approval_topic.profile_code') }}</label>
                                        <input type="text" class="form-control form-input" wire:model.defer="code" placeholder="{{ __('data_field_name.approval_topic.text_code') }}" @error('code') <span class="error text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <h3 class="title px-16">{{ __('research/ideal.list') }}</h3>
                                <div class="table-responsive px-16">
                                    <table class="table table-general ">
                                        <thead>
                                            <tr class="border-radius">
                                                @if ($status == \App\Enums\EApproval::STATUS_NEW)
                                                    <th class="border-radius-left text-center">{{ __('data_field_name.common_field.select_default') }}</th>
                                                    <th class="text-center" scope="col">{{ __('data_field_name.common_field.code') }}</th>
                                                @else
                                                    <th class="border-radius-left text-center">{{ __('data_field_name.common_field.code') }}</th>
                                                @endif
                                                <th class="text-center" scope="col">{{ __('data_field_name.approval_topic.name_ideal') }}</th>
                                                <th class="text-center" scope="col">{{ __('data_field_name.topic.field') }}</th>
                                                <th class="text-center" scope="col">{{ __('data_field_name.topic.expected_fee') }}</th>
                                                <th class="text-center" scope="col">{{ __('data_field_name.common_field.start_time') }}</th>
                                                <th class="text-center" scope="col" class="border-radius-right min-width text-center">{{ __('data_field_name.approval_topic.proposal_sender') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (!empty($listIdeal))
                                                @foreach ($listIdeal as $ideal)
                                                    @if ($status == \App\Enums\EApproval::STATUS_NEW)

                                                        <tr>
                                                            <td class="text-center">
                                                                <label class="box-checkbox checkbox-analysis">
                                                                    <input type="checkbox" value="1" wire:model.defer="ideal_id.{{ $ideal['id'] }}" />
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </td>
                                                            <td class="text-center"><a href="{{ route('admin.research.ideal.edit', $ideal['id']) }}">{!! $ideal['code'] !!}</a></td>
                                                            <td class="text-center">{!! $ideal['name'] !!}</td>
                                                            <td class="text-center">{!! $ideal['research_name'] !!}</td>
                                                            <td class="text-center">{!! $ideal['fee'] !!}</td>
                                                            <td class="text-center">{!! $ideal['created_at'] !!}</td>
                                                            <td class="text-center">{!! $ideal['fullname'] !!}</td>
                                                        </tr>
                                                    @else
                                                        @if (in_array($ideal['id'], $listIdealApproval))
                                                            <tr>
                                                                <td class="text-center"><a href="{{ route('admin.research.ideal.edit', $ideal['id']) }}">{!! $ideal['code'] !!}</a></td>
                                                                <td class="text-center">{!! $ideal['name'] !!}</td>
                                                                <td class="text-center">{!! $ideal['research_name'] !!}</td>
                                                                <td class="text-center">{!! $ideal['fee'] !!}</td>
                                                                <td class="text-center">{!! $ideal['created_at'] !!}</td>
                                                                <td class="text-center">{!! $ideal['fullname'] !!}</td>
                                                            </tr>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                    @if ($text_ideal_error != null) <span class="error text-danger">{{ $text_ideal_error }}</span>@endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class=" col-md-12 text-center mt-42">
                    <a href="{{ route('admin.approval.index') }}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{ __('common.button.cancel') }}</a>

                    @if ($approval->status == \App\Enums\EApproval::STATUS_NEW || $approval->status == \App\Enums\EApproval::STATUS_COMPLETE_PROFILE || $approval->status == \App\Enums\EApproval::STATUS_INVALID_PROFILE || $approval->status == \App\Enums\EApproval::STATUS_APPROVAL)
                        @if ($type != \App\Enums\EApproval::TYPE_IDEAL)
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="update()" class="btn btn-main bg-primary text-9 size16 px-48 py-14 close-modal">{{ __('common.button.save') }}</button>
                        @endif
                    @endif
                    @if ($type == \App\Enums\EApproval::TYPE_IDEAL)
                        @if ($approval->status == \App\Enums\EApproval::STATUS_NEW)
                            <button id="buttonCreateBoard" type="button" wire:click.prevent="update()" class="btn btn-main bg-primary text-9 size16 px-48 py-14 close-modal">{{ __('common.button.save') }}</button>
                        @endif
                    @endif
                </div>
            </form>

            <div class="modal fade show modal-fix" id="appraisal-modal" tabindex="-1" aria-modal="true" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        @if ($appraisal_board_id == null)
                            @livewire('admin.appraisal.appraisal-editor', ['approvalId' => $approval->id], key(date('Y-m-d H:i:s')))
                        @else
                            @livewire('admin.appraisal.appraisal-editor', ['appraisalBoardId' => $appraisal_board_id, 'approvalId' => $approval->id], key(date('Y-m-d H:i:s')))
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.addEventListener('close-appraisal-modal-event', function() {
        $('.modal').modal('hide');
        @this.loadDataAppraisal();
    });
</script>

</div>
