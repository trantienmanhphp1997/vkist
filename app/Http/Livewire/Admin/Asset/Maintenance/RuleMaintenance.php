<?php

namespace App\Http\Livewire\Admin\Asset\Maintenance;

use App\Component\AssetCategoryRecursive;
use App\Enums\EAssetFrequency;
use App\Enums\EAssetTimeMaintenance;
use App\Enums\EAssetTimeRepeat;
use App\Http\Livewire\Base\BaseLive;
use App\Models\AssetAllocatedRevoke;
use App\Models\AssetCategory;
use App\Models\AssetRuleMaintenance;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class RuleMaintenance extends BaseLive
{
    public $assetFrequency;
    public $assetMaintenanceTime;
    public $assetTimeRepeat;

    public $asset_category_id;
    public $content_maintenance;
    public $frequency;
    public $repeat;
    public $time_choose ;
    public $number_date ;

    public function mount()
    {
        $this->assetFrequency = [
            EAssetFrequency::ONE => __('data_field_name.asset.one'),
            EAssetFrequency::PERIODIC => __('data_field_name.asset.periodic'),
        ];
        $this->assetMaintenanceTime = [
            EAssetTimeMaintenance::AFETR_USE => __('data_field_name.asset.after_use'),
            EAssetTimeMaintenance::AFTER_BUY => __('data_field_name.asset.after_buy'),
            EAssetTimeMaintenance::OPTION => __('data_field_name.asset.option'),
        ];
        $this->assetTimeRepeat = [
            EAssetTimeRepeat::MONTH => __('data_field_name.asset.month'),
            EAssetTimeRepeat::QUARTER => __('data_field_name.asset.quarter'),
            EAssetTimeRepeat::SIX_MONTH => __('data_field_name.asset.six_month'),
            EAssetTimeRepeat::YEAR => __('data_field_name.asset.year'),
        ];
    }
    public function render()
    {
        $category = $this->getHierarchyAssetCategory();
        return view('livewire.admin.asset.maintenance.rule-maintenance',['category' => $category]);
    }

    //validation rule
    protected function rules(){
        return [
            'asset_category_id'=>'required',
            'content_maintenance'=>'required|max:255',
            'frequency'=>'required',
            'time_choose'=>'required',
            'number_date'=>'required|max:4',
            ];
    }
    protected function getValidationAttributes()
    {
        return [
            'asset_category_id'=>__('data_field_name.regulation.asset-type'),
            'content_maintenance'=>__('data_field_name.regulation.content'),
            'frequency'=>__('data_field_name.regulation.frequency'),
            'time_choose'=>__('data_field_name.regulation.time'),
            'number_date'=>__('data_field_name.regulation.day'),
        ];
    }

    public function save(){
        $this->validate();
        $maintenance_time_json = '';
        if ($this->time_choose&&$this->number_date){
            $data_json = ['time_choose'=>$this->time_choose,'number_date'=>$this->number_date];
            $maintenance_time_json=json_encode($data_json);
        }
        $this->validate([
            'frequency' => 'required',
            'content_maintenance' => 'max:255',
            'number_date' => 'required|max:4',],[],[
             'frequency' =>__('data_field_name.asset.frequency'),
             'content_maintenance'=> __('data_field_name.asset.content_maintenance'),
             'number_date' =>__('data_field_name.asset.number_date'),
        ]);
        $data = AssetRuleMaintenance::create([
            'asset_category_id'=>$this->asset_category_id,
            'content_maintenance'=>$this->content_maintenance,
            'frequency'=>$this->frequency,
            'repeat'=>$this->repeat,
            'maintenance_time_json'=>$maintenance_time_json,
        ]);
        if ($data){
            $this->emit('closeRule');
            $this->resetInput();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
                __('notification.common.success.add')] );
        }

    }
    public function resetInput()
    {
        $this->asset_category_id = '';
        $this->content_maintenance = '';
        $this->frequency = '';
        $this->repeat = '';
        $this->time_choose = '';
        $this->number_date = '';
    }
    public function getHierarchyAssetCategory()
    {
        return AssetCategoryRecursive::hierarch(AssetCategory::all());
    }
}
