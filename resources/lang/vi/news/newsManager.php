<?php

return [
    'menu_name' => [
        'download-file-fail'=>'Tải file không thành công',
        'download-file-success'=>'Tải file thành công',
        'button_publish' => 'Lưu & đăng',
        'button_wait_approval' => 'Lưu & Gửi phê duyệt',
        'button_save_draft'=>'Lưu nháp',
        'back'=> 'Quay về danh sách ',
        'news'=>'Quản lý tin tức',
        'news_page' => 'Danh sách thông tin bài viết',
        'news_title_table'=>[
            'stt' => 'STT',
            'name'=>'Tiêu đề bài viết',
            'author'=> 'Tác giả',
            'category'=>'Chuyên mục',
            'publication_time' => 'Thời gian xuất bản',
            'status' => 'TRẠNG THÁI',
            'action' => 'HÀNH ĐỘNG'
        ],
        'news_child'=>[
            'post'=> 'Tin đã đăng',
            'draft'=> 'Tin nháp',
            'need_approval'=> 'Tin cần phê duyệt',
            'approve'=> 'Tin được phê duyệt',
        ],
        'category_page'=>'Quản lý chuyên mục',
        'category_title_table'=>[
            'name'=>'TÊN CHUYÊN MỤC',
            'author'=> 'TÁC GIẢ',
            'publication_time' => 'THỜI GIAN TẠO',
            'action' => 'HÀNH ĐỘNG'
        ],

        'status' => [
            'on'=>'Đang hiển thị',
            'off'=>'Không hiển thị'
        ],
        'form-data'=>[
            'chose'=>'Chọn',
            'description'=>'Nội dung công việc',
            'update'=>'Cập nhật tin mới',
            'create'=>'Tạo mới tin tức',
            'name'=>'Tiêu đề',
            'image'=>'Ảnh đại diện',
            'content'=>'Thông tin tóm tắt',
            'category'=>'Chuyên mục',
            'file'=>'Tải ảnh',
            'trending'=>'Tin nổi bật'
        ],
        'create-category'=>[
            'title'=>'Tạo mới chuyên mục',
            'name'=>'Tên chuyên mục',
            'empty-name'=>'Tên chuyên mục'
        ],
        'update-category'=>[
            'title'=>'Cập nhật chuyên mục'
        ],
        'notification'=>[
           'success'=>'Lưu bản tin thành công',
           'fail'=>'Lưu bản tin không thành công',
            'category'=>[
                'create'=>[
                    'success'=>'Tạo mới chuyên mục thành công',
                    'fail'=>'Tạo mới chuyên mục thất bại'
                ],
                'update'=>[
                    'success'=>'Cập nhật chuyên mục thành công',
                    'fail'=>'Cập nhật chuyên mục thất bại'
                ],
                'delete'=>'Xóa chuyên mục thành công'
            ],
        ],
        'delete-category'=>[
            'warning'=>'!!! Lưu ý việc xóa chuyên mục sẽ làm ảnh hưởng tới tin tức chứa chuyên mục này !!!'
        ],
        'warning-over-file-msg'=> 'File đính kèm tối đa là 5 , hãy nén file zip rồi tải lại',
        'success-approved-success-msg' => 'Duyệt bản tin thành công',
        'amount_file_can_upload'=>'Tối đa 5 file',
        'approved'=>'Duyệt bài viết',
        'save-draft'=>'Lưu nháp',
        'overFileWarningMsg'=>'Chỉ tối đa cho phép 5 file được tải lên !!!',
        'see-more' => 'Xem thêm',
        'permission-fail-msg'=>'Bạn đang không được cấp phép gửi phê duyệt',
        'success-to-delete-msg'=>'Xóa bản tin thành công',
        'list-new'=>'Tin tức',
        'imageSize'=>'(Chiều dài: 500px | Chiều rộng: 700px hoặc chiều dài: 1200px | chiều rộng: 630px)',
        'popular-news'=>'Tin nổi bật',
        'home' => 'Trang chủ',
        'detail-new'=>'Trang chi tiết',
        'transfer-to-trending-on'=>'Bản tin này đã được đưa lên trang nổi bật',
        'transfer-to-trending-off'=>'Bản tin này đã được gỡ khỏi tin nổi bật',
        'all'=>'Tất cả bản tin mới nhất',
        'article'=>'Tên tác giả',
        'count_read'=>'Số lượng người xem',
        'msg'=>[
            'upload-attached-file'=> [
                'is-over-size'=>'File tải lên quá',
                'success'=>'Tải file thành công',
                'wrong-file-format'=>'File tải lên không đúng định dạng file',
            ],
            'upload-avatar'=>[
                'mimes'=>'Ảnh tải lên không đúng định dạng',
                'max'=>'Ảnh tải lên quá 200kb yêu cầu tải ảnh khác',
                'empty'=>'Ảnh bản tin không được phép bỏ trống',
                'success'=>'Tải ảnh lên thành công',
                'fail'=>'Tải ảnh lên thất bại',
                'over-dimension'=>'Ảnh tải lên sai kích cỡ cho phép'
            ],
            'name'=>[
                'empty'=>'Tiêu đề bản tin không được bỏ trống'
            ],
            'description'=> [
                'empty'=>'Nội dung không được bỏ trống'
            ]
        ]
    ]
];
