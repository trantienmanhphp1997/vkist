<?php

namespace App\Http\Livewire\Admin\DecisionReward;

use App\Http\Livewire\Base\BaseLive;
use App\Enums\EDecisionRewardReceiverType;
use App\Enums\EDecisionRewardPrizeType;
use App\Component\SizeFile;
use Livewire\WithFileUploads;
use App\Component\FileUpload;
use App\Models\UserInfoLeave;
use App\Models\UserInf;
use App\Models\DecisionReward;
use App\Models\DecisionRewardReceiver;
use Illuminate\Support\Facades\Config;


use function Complex\theta;

class DecisionRewardList extends BaseLive
{

    //checked
    public $allChecked = false;
    public $checked = []; //mảng associate với key là id của provider, value là trạng thái checked trong checkbox
    public $needToResetChecked = true;

    public function render()
    {
        $query = DecisionReward::query();
        $searchTerm = $this->searchTerm;
        if (!empty($searchTerm)) {
            $query->where(function ($q) use ($searchTerm) {
                $q->where('name', 'like', '%' . $searchTerm . '%');
            });
        }
        $data = $query->with('receivers.info')->orderBy('id', 'desc')->paginate($this->perPage);
        if($this->needToResetChecked){
            foreach ($data as $item) {
                $this->checked[$item->id] = false;
            }
        }
        $this->needToResetChecked = true;
        return view('livewire.admin.decision-reward.decision-reward-list',  ['data' => $data]);
    }

    public function updatedChecked() {
        $this->needToResetChecked = false;
    }

    public function updatedAllChecked() {
        $this->checked = array_fill_keys(array_keys($this->checked), $this->allChecked);
        $this->needToResetChecked = false;
    }

    public function delete()
    {
        parent::delete();
        $decisionReward = DecisionReward::findOrFail($this->deleteId);
        $decisionReward->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }
}
