<div class="col-md-10 col-xl-11 box-detail box-user">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.asset.list_title')}} </a>
                    \ <span>{{__('data_field_name.asset.detail_asset')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-5">
                        <h3 class="title">{{__('data_field_name.asset.detail_asset')}}</h3>
                    </div>
                    <div class="col-md-7">
                        <div class=" float-right">
                            @if($checkCreatePermission)
                                <div class="dropdown navbar manipulation bg_blue">
                                    <a class="btn  dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{__('data_field_name.asset.group_btn_asset')}}
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a href="#" data-toggle="modal" data-target="#notice_lost"
                                           @if($assetDetail->situation==\App\Enums\EAssetSituation::SITUATION_USING)
                                                class="dropdown-item"
                                           @else
                                                class="dropdown-item disabled blur_link"
                                           @endif
                                        >
                                            {{__('data_field_name.asset.lost_report')}}
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#notice_cancel"
                                           @if($assetDetail->situation==\App\Enums\EAssetSituation::SITUATION_USING)
                                           class="dropdown-item"
                                           @else class="dropdown-item disabled blur_link"
                                            @endif>
                                            {{__('data_field_name.asset.broken_report')}}
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#notice_liquidation"
                                           @if($assetDetail->situation==\App\Enums\EAssetSituation::SITUATION_NOT_USE)
                                           class="dropdown-item"
                                           @else
                                           class="dropdown-item disabled blur_link"
                                            @endif
                                        >
                                            {{__('data_field_name.asset.liquidation')}}
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#notice_repair" 
                                           @if($assetDetail->situation==\App\Enums\EAssetSituation::SITUATION_USING)
                                           class="dropdown-item"
                                           @else
                                           class="dropdown-item disabled blur_link"
                                            @endif
                                        >
                                            {{__('data_field_name.asset.repair')}}
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#notice_maintenance"
                                           @if($assetDetail->situation==\App\Enums\EAssetSituation::SITUATION_USING)
                                           class="dropdown-item"
                                           @else
                                           class="dropdown-item disabled blur_link"
                                            @endif
                                        >
                                            {{__('data_field_name.asset.maintain')}}
                                        </a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="information information-asset">
                    <div class="inner-tab pt0">
                        <form wire:submit.prevent="submit">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class=" menu-asset nav" role="tablist">
                                        <li role="presentation" wire:ignore>
                                            <a href="#profile" class="active" aria-controls="profile" role="tab"
                                               data-toggle="tab">
                                                {{__('data_field_name.asset.info_general')}}
                                            </a>
                                        </li>
                                        <li role="presentation" wire:ignore>
                                            <a href="#insurance" aria-controls="insurance" role="tab" data-toggle="tab">
                                                {{__('data_field_name.asset.info_insurance')}}
                                            </a>
                                        </li>
                                        <li role="presentation" wire:ignore>
                                            <a href="#distribute" aria-controls="distribute" role="tab"
                                               data-toggle="tab">
                                                {{__('data_field_name.asset.info_distribute')}}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9 tab-content">
                                    @livewire('admin.asset.asset-detail.tab-detail-info',['assetDetail'=>$assetDetail])
                                    @livewire('admin.asset.asset-detail.tab-detail-warranty',['assetDetail'=>$assetDetail])
                                    @livewire('admin.asset.asset-detail.tab-detail-distribute',['assetDetail'=>$assetDetail])
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="group-btn2 text-center group-btn ">
                        <a href="{{route('admin.asset.index')}}">
                            <button type="" class="btn btn-cancel"
                                    name="cancel">{{__('data_field_name.budget.back')}}</button>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {{--    modal--}}
    @livewire('admin.asset.asset-notice.notice-lost',['assetDetail'=>$assetDetail])
    @livewire('admin.asset.asset-notice.notice-cancel',['assetDetail'=>$assetDetail])
    @livewire('admin.asset.asset-notice.notice-liquidation',['assetDetail'=>$assetDetail])
    @livewire('admin.asset.asset-notice.notice-repair',['assetDetail'=>$assetDetail])
    @livewire('admin.asset.asset-notice.notice-maintenance',['assetDetail'=>$assetDetail])

</div>
<script>
    $("document").ready(function () {
        window.livewire.on('closeNotice', () => {
            document.getElementById('close-notice-lost').click();
            document.getElementById('close-notice-cancel').click();
            document.getElementById('close-notice-liquidation').click();
            document.getElementById('close-notice-repair').click();
            document.getElementById('close-notice-maintenance').click();
        });
        $(".num").keypress(function() {
            if ($(this).val().length >= $(this).attr("maxlength")) {
                return false;
            }
        });
    });
    $('.disabled').click(function (e) {
        e.preventDefault();
    })
</script>

