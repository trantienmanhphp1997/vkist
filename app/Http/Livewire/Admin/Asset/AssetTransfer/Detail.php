<?php

namespace App\Http\Livewire\Admin\Asset\AssetTransfer;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use App\Models\AssetAllocatedRevoke;
use App\Models\Asset;
use App\Models\UserInf;
use App\Models\ClaimForm;
use App\Models\AssetClaimForm;
use DB;
use Illuminate\Support\Carbon;
use App\Models\File;
use App\Component\FileUpload;
use App\Enums\EAssetTransferType;
use App\Models\Department;
use Livewire\WithFileUploads;

class Detail extends BaseLive
{
    use WithFileUploads;

    public $allocatedId;

    public $fileUpload = [];
    public $day;
    public $reason;
    public $number_report;
    public $from;
    public $to;
    public $transferType;

    public $department1Leader;
    public $department2Leader;

    public function render() {
        $type = Config::get('common.type_upload.Transfer');
        $model_name = ClaimForm::class;
        app($model_name)->getTable();

        $allocated = AssetAllocatedRevoke::where('id', $this->allocatedId)->where('status', 3)->first();
        $this->transferType = $allocated->transfer_type;

        if($allocated->transfer_type == EAssetTransferType::BY_INDIVIDUAL) {
            $this->from = UserInf::query()->find($allocated->transfered_user_id)->fullname ?? '';
            $this->to = UserInf::query()->find($allocated->user_info_id)->fullname ?? '';
        } elseif ($allocated->transfer_type == EAssetTransferType::BY_DEPARTMENT) {
            $department1 = Department::query()->find($allocated->from_department_id);
            $department2 = Department::query()->find($allocated->to_department_id);

            $this->from = $department1->name ?? '';
            $this->department1Leader = $department1->leader->fullname ?? '';

            $this->to = $department2->name ?? '';
            $this->department2Leader = $department2->leader->fullname ?? '';
        }

        $this->reason = $allocated->implementation_reason;
        $this->number_report = $allocated->number_report;
        $this->day = Carbon::parse($allocated->implementation_date)->format(Config::get('common.year-month-day'));
        $asset = Asset::findOrFail($allocated->asset_id);

        $fileList = File::where('model_name', $model_name)
            ->where('model_id', $allocated->claim_form_id)
            ->where('type', $type)
            ->get();
        return view('livewire.admin.asset.asset-transfer.detail', ['asset' => $asset, 'fileList' => $fileList]);
    }
}
