<?php

return [
    'menu_name' => [
        'download-file-fail' => 'Download file failed',
        'download-file-success' => 'Download file success',
        'button_publish' => 'Save & publish',
        'button_wait_approval' => 'Save & Submit Approval',
        'button_save_draft' => 'Save Draft',
        'back' => 'Back to list',
        'news' => 'News Management',
        'news_page' => 'List of Articles',
        'news_title_table' => [
            'stt' => 'STT',
            'name' => 'Article title',
            'author' => 'Author',
            'category' => 'Category',
            'publication_time' => 'Publication time',
            'status' => 'STATUS',
            'action' => 'Action',
        ],
        'news_child' => [
            'post' => 'Posted',
            'draft' => 'Draft',
            'need_approval' => 'News that requires approval',
            'approve' => 'Approved news',
        ],
        'category_page' => 'Category',
        'category_title_table' => [
            'name' => 'CATEGORY NAME',
            'author' => 'AUTHOR',
            'publication_time' => 'CREATION TIME',
            'action' => 'Action'
        ],

        'status' => [
            'on' => 'Showing',
            'off' => 'Not visible'
        ],
        'form-data' => [
            'chose'=>'Select a category',
            'description'=>'Work Content',
            'create' => 'Create News',
            'update'=>'Update News',
            'name' => 'Title',
            'image' => 'Available image',
            'content' => 'Summary information',
            'category' => 'Category',
            'file' => 'Load image',
            'trending' => 'Popular new'
        ],
        'create-category' => [
            'title' => 'Create new category',
            'name' => 'Category name',
            'empty-name'=>'Name category'
        ],
        'update-category' => [
            'title' => 'Update Category'
        ],
        'notification' => [
            'success' => 'Successful message saved',
            'fail' => 'Save message failed',
            'category' => [
                'create' => [
                    'success' => 'Create category success',
                    'fail' => 'Create category fail'
                ],
                'update' => [
                    'success' => 'Update category success',
                    'fail' => 'Update category fails'
                ],
                'delete' => 'Delete category success'
            ],
        ],
        'delete-category' => [
            'warning' => '!!! Note that deleting a category will affect the news containing this category!!!'
        ],
        'warning-over-file-msg' => 'Maximum attachment is 5 , please upload a zip file instead',
        'success-approved-success-msg' => 'Successful approved new',
        'amount_file_can_upload' => 'Maximum only 5 file',
        'approved' => 'Approved new',
        'save-draft' => 'Save to draft',
        'overFileWarningMsg' => 'Maximum only 5 file to be uploaded !!!',
        'see-more' => 'See more',
        'permission-fail-msg' => 'you are not permission to send an approval',
        'success-to-delete-msg' => 'deleted new success',
        'list-new' => 'News',
        'imageSize' => '(Height: 500px | Width: 700px or Height: 630px | Width: 1200px)',
        'popular-news' => 'Trending news',
        'home' => 'Home page',
        'detail-new' => 'Detail page',
        'transfer-to-trending-on' => 'This post has been on to trending news',
        'tranfer-to-trending-off' => 'This post has been taking off from trending news',
        'all' => 'All new posted',
        'article' => 'Article',
        'count_read' => 'Viewer',
        'msg' => [
            'upload-attached-file' => [
                'is-over-size' => 'Please upload a file less than',
                'success' => 'Upload file success',
                'wrong-file-format' => 'File upload wrong file format',
            ],
            'upload-avatar' => [
                'mimes'=>'The image is not valid',
                'max'=>'The image upload is over 200kb',
                'empty'=>'New Image can not be empty',
                'success' => 'Upload file success',
                'fail' => 'Upload file fail',
                'over-dimension' => 'Image not valid with the width and the height permission'
            ],
            'name'=>[
                'empty'=>'Title of the new can not be empty'
            ],
            'description'=> [
                'empty'=>'The description can not be empty'
            ]
        ]
    ]
];
