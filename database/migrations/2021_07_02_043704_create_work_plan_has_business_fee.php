<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkPlanHasBusinessFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_plan_has_business_fee', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('du an');
            $table->dateTime('working_day')->nullable();
            $table->bigInteger('money')->comment('So tien thuc te');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('map với user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info');
            $table->unsignedBigInteger('work_plan_id')->nullable()->comment('map với user_info');
            $table->foreign('work_plan_id')->references('id')->on('work_plan');
            $table->unsignedBigInteger('business_fee_id')->nullable()->comment('map với user_info');
            $table->foreign('business_fee_id')->references('id')->on('business_fee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_plan_has_business_fee');
    }
}
