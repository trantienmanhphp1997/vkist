<?php

namespace App\Http\Livewire\Admin\Appraisal;

use App\Enums\EAppraisalBoard;
use App\Enums\EAppraisalBoardHasUserInfo;
use App\Enums\EApproval;
use App\Enums\ETopicStatus;
use App\Http\Livewire\Base\BaseLive;
use App\Models\AppraisalBoard;
use App\Models\AppraisalBoardHasUserInfo;
use App\Models\Approval;
use App\Models\Topic;
use App\Models\UserInf;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class AppraisalBoardList extends BaseLive
{
    public $searchStatus;
    public $searchType;
    public $searchAppraisalDate;

    public $statusSendFile;

    public $currentAppraisalBoardId;

    public bool $showOnly = false;

    protected $listeners = [
        'emitDeleteFileInvitation',
        'emitDeleteFile',
        'set-appraisalDate' => 'setAppraisalDate',
        'set-appraisalDateEdit' => 'setAppraisalDateEdit',
    ];

    public function mount() {
    }

    // ========================================================

    public function updatedCurrentAppraisalBoardId() {
        $this->dispatchBrowserEvent('open-appraisal-modal-event');
    }

    // ========================================================

    public function render()
    {
        $query = AppraisalBoard::query();
        $searchTerm = $this->searchTerm;
        $status = $this->searchStatus;
        $type = $this->searchType;
        $appraisalDate = $this->searchAppraisalDate;

        if (!empty($searchTerm)) {
            $query->whereRaw("LOWER(name) LIKE LOWER('%$searchTerm%')");
            $query->with('topic')->orwhereHas('topic', function (Builder $query) use ($searchTerm) {
                return $query->whereRaw("LOWER(name) LIKE LOWER('%$searchTerm%')");
            });
        } else {
            $query->with('topic');
        }

        if (!empty($appraisalDate) && Carbon::canBeCreatedFromFormat($appraisalDate, 'd/m/Y')) {
            $appraisalDate = Carbon::createFromFormat('d/m/Y', $appraisalDate);
            $query->whereDate('appraisal_date', $appraisalDate);
        }

        if ($status != null) {
            $query->where('status', $status);
        }

        if (!empty($type)) {
            $query->where('type', $type);
        }

        $data = $query->orderBy('id','DESC')->paginate($this->perPage);

        $appraisalType = EAppraisalBoard::getListType();
        $statusAppraisal = EAppraisalBoard::statusAppraisal();
        $appraisalRole = EAppraisalBoardHasUserInfo::appraisalRole();

        $listTopic =  $this->getTopic();
        $this->statusSendFile = [EAppraisalBoardHasUserInfo::STATUS_UNSENT, EAppraisalBoardHasUserInfo::STATUS_REJECT];

        $dataUserInfo = UserInf::with('academicLevel')->get();
        $listUserInfo = [];
        foreach ($dataUserInfo as $userInfo) {
            $listUserInfo[$userInfo->id] = [
                'id' => $userInfo->id,
                'fullname' => $userInfo->fullname,
                'academicLevel' => $userInfo->academicLevel->v_value ?? '',
            ];
        }

        return view('livewire.admin.appraisal.appraisal-board-list',
            [
                'listUserInfo' => $listUserInfo,
                'appraisalRole' => $appraisalRole,
                'data' => $data, 'appraisalType' => $appraisalType,
                'statusAppraisal' => $statusAppraisal,
                'listTopic' => $listTopic,
            ]);
    }

    public function delete() {
        $appraisal = AppraisalBoard::findOrFail($this->deleteId);
        if (!empty($appraisal)) {
            $approval = Approval::where('appraisal_board_id', $this->deleteId)->first();
            if (!empty($approval)) {

                if ($approval->status > EApproval::STATUS_INVALID_PROFILE) {
                    $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.appraisal.error_delete_appraisal')]);
                    return true;
                }

                $approval->appraisal_board_id = null;
                $approval->update();
            }
            $appraisal->delete();

            AppraisalBoardHasUserInfo::where('appraisal_board_id', $this->deleteId)->delete();

            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
            return true;
        }

        $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.delete')]);
        return false;
    }

    protected function getTopic()
    {
        $dataTopic = Topic::where('status', '=', ETopicStatus::STATUS_NOT_SUBMIT)->get();
        $listTopic = [];
        foreach ($dataTopic as $topic) {
            $listTopic[$topic->id] = $topic->name;
        }

        return $listTopic;
    }

    protected function getListUserBoard($idAppraisal)
    {
        $boardUser = AppraisalBoardHasUserInfo::where('appraisal_board_id', $idAppraisal)->get();
        $listUserBoard = [];
        if ($boardUser->isNotEmpty()) {
            foreach ($boardUser as $user) {
                $listUserBoard[$user->user_info_id] = [
                    'user_info_id' => $user->user_info_id,
                    'role_id' => $user->role_id,
                    'status' => $user->status,
                    'id' => $user->id,
                ];
            }
        }

        return $listUserBoard;
    }
}
