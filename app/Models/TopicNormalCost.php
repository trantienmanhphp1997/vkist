<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopicNormalCost extends Model
{
    use HasFactory;

    protected $table = "topic_normal_cost";
    protected $fillable = [
        'name',
        'unit',
        'quantity',
        'price',
        'total',
        'state_capital',
        'other_capital',
        'topic_fee_detail_id',
    ];

    public function topicFeeDetail() {
        return $this->belongsTo(TopicFeeDetail::class, 'topic_fee_detail_id');
    }
}
