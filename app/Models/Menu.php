<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Menu extends Eloquent
{
    use HasFactory;
    protected $table = "menu";
    public $autoincrement = true;
    protected $guarded=['*'];
    protected $fillable = [
        'name',
        'code',
        'permission_name',
        'alias',
        'note',
        'admin_id',
    ];
}
