<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWageCoefficientToTopicDetailWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_detail_work', function (Blueprint $table) {
            $table->string('wage_coefficient', 10)->nullable()->comment('He so tien cong');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_detail_work', function (Blueprint $table) {
            $table->dropColumn('wage_coefficient');
        });
    }
}
