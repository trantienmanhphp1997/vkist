<div class="col-md-12">
    <style>
        .box-news .card-text {
            background: white;
        }
    </style>
    <div class="row">
        @forelse($news as $key => $new)
            <div class="col-md-12 col-lg-4" style="cursor:pointer;" wire:click="getDetail('{{$new->slug}}','{{$new->category->slug ?? "unkhown"}}')">
                <div class="card">
                    <img src="{!! trim($new->image_path) != '' ? 'storage/uploads'.$new->image_path : "images/img.jpg"
                       !!}"
                         height="200px" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-date"><img src="images/date.svg" alt="date">{{formatDateLocale($new->created_at,"%a , %e/%m/%Y")}}</h5>
                        <p class="card-text">
                            {{ strLimit($new->name,40) }}
                        </p>
                        <a class="viewmore" style="cursor:pointer;" >{!! __("news/newsManager.menu_name.see-more") !!}</a>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-md-12">
                <div class="card">
                    <h5>{{__('status.news.empty')}}</h5>
                </div>
            </div>
        @endforelse
    </div>
</div>
