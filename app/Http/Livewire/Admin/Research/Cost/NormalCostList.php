<?php

namespace App\Http\Livewire\Admin\Research\Cost;

use App\Models\TopicNormalCost;
use App\Models\TopicFeeDetail;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Auth;

class NormalCostList extends BaseLive
{
    public $topicFeeDetailId;
    public $name;
    public $unit;
    public $quantity;
    public $price;
    public $state_capital;
    public $other_capital;
    public $otherCostID;
    public $topicFeeId;
    public $total;
    public $topicFeeStatus;

    public function mount($topicFeeDetailId, $topicFeeId, $topicFeeStatus) {
        $this->topicFeeDetailId = $topicFeeDetailId;
        $this->topicFeeId = $topicFeeId;
        $this->topicFeeStatus = $topicFeeStatus;
    }

    public function render()
    {
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->topicFeeDetailId);
        $this->updateTopicFeeDetail();
        $query = TopicNormalCost::query();
        $query->where('topic_fee_detail_id', '=', $this->topicFeeDetailId);
        $dataTotal = [
            'total' => $query->sum('total'),
            'state_capital' => $query->sum('state_capital'),
            'other_capital' => $query->sum('other_capital'),
        ];
        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);

        if ($this->quantity != null && $this->price != null) {
            $total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
            $this->total = numberFormat($total);
        }

        return view('livewire.admin.research.cost.normal-cost-list',
            ['dataTopicFee' => $this->topicFeeDetailId, 'data' => $data, 'dataTotal' => $dataTotal, 'topicFeeDetail' => $topicFeeDetail]);
    }

    public function validateData()
    {
        $this->validate([
            'name' => 'required|max:100',
            'unit' => 'required|max:10',
            'quantity' => 'required|regex:/^[0-9,]+$/|max:7',
            'price' => 'required|regex:/^[0-9,]+$/|max:14',
            'state_capital' => 'nullable|required|regex:/^[0-9,]+$/|max:18',
            'other_capital' => 'nullable|required|regex:/^[0-9,]+$/|max:18',
        ],[
            'quantity.max' => __('client_validation.form.research_cost.max.quantity'),
            'price.max' => __('client_validation.form.research_cost.max.price'),
            'state_capital.max' => __('client_validation.form.research_cost.max.state_capital'),
            'other_capital.max' => __('client_validation.form.research_cost.max.other_capital'),
        ],[

            'name' => __('data_field_name.research_cost.name'),
            'unit' => __('data_field_name.research_cost.unit'),
            'quantity' => __('data_field_name.research_cost.quantity'),
            'price' => __('data_field_name.research_cost.price'),
            'state_capital' => __('data_field_name.research_cost.state_capital'),
            'other_capital' => __('data_field_name.research_cost.other_capital'),

        ]);
    }
    public function store()
    {
        $this->validateData();
        $normalCost = new TopicNormalCost;
        $normalCost->name = $this->name;
        $normalCost->unit = $this->unit;
        $normalCost->quantity = removeFormatNumber($this->quantity);
        $normalCost->price = removeFormatNumber($this->price);
        $normalCost->state_capital = removeFormatNumber($this->state_capital);
        $normalCost->other_capital = removeFormatNumber($this->other_capital);
        $normalCost->total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
        $normalCost->topic_fee_detail_id = $this->topicFeeDetailId;
        $normalCost->admin_id = Auth::user()->id;
        $normalCost ->save();
        $this->emit('close-modal-create-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function resetInputFields(){
        $this->name = null;
        $this->unit = null;
        $this->quantity = null;
        $this->price = null;
        $this->total = null;
        $this->state_capital = null;
        $this->other_capital = null;
        $this->resetValidation();
    }

    public function edit($id){
        $this->otherCostID = $id;
        $otherCost = TopicNormalCost::findOrFail($id);
        $this->name = $otherCost->name;
        $this->unit = $otherCost->unit;
        $this->quantity = numberFormat($otherCost->quantity);
        $this->price = numberFormat($otherCost->price);
        $this->state_capital = numberFormat($otherCost->state_capital);
        $this->other_capital = numberFormat($otherCost->other_capital);
        $this->total = numberFormat($otherCost->total);
        $this->resetValidation();
    }

    public function update(){
        $normalCost = TopicNormalCost::findOrFail($this->otherCostID);
        $this->validateData();
        $normalCost->name = $this->name;
        $normalCost->unit = $this->unit;
        $normalCost->quantity = removeFormatNumber($this->quantity);
        $normalCost->price = removeFormatNumber($this->price);
        $normalCost->state_capital = removeFormatNumber($this->state_capital);
        $normalCost->other_capital = removeFormatNumber($this->other_capital);
        $normalCost->total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
        $normalCost->save();

        $this->emit('close-modal-edit-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }

    public function deleteId($id){
        $this->otherCostID = $id;
    }

    public function delete(){
        $otherCost = TopicNormalCost::findOrFail($this->otherCostID);

        $otherCost->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }

    public function updateTopicFeeDetail(){
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->topicFeeDetailId);
        $topicFeeDetail->total_capital = TopicNormalCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('total');
        $topicFeeDetail->state_capital = TopicNormalCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('state_capital');
        $topicFeeDetail->other_capital = TopicNormalCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('other_capital');
        $topicFeeDetail->save();
    }
}
