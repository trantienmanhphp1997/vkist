<?php

namespace App\Http\Livewire\Admin\Approval;

use App\Enums\EApproval;
use App\Exports\ApprovalIdeaExport;
use App\Exports\ApprovalResearchPlanExport;
use App\Exports\ApprovalTopicExport;
use App\Exports\ApprovalTopicFeeExport;
use App\Models\Approval;
use App\Http\Livewire\Base\BaseLive;
use App\Service\Community;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Facades\Excel;

class ApprovalList extends BaseLive
{

    public $searchTerm;
    public $searchStatus;
    public $searchType;
    public $approvalId;

    public function render()
    {
        $query = Approval::query();
        $status = $this->searchStatus;
        $type = $this->searchType;

        $query->with('appraisal', 'topic', 'topicFee');
        if ($this->searchTerm != null) {
            $term = strtolower(removeStringUtf8($this->searchTerm));
            $query->where('unsign_text', 'like', '%' . $term . '%')
                ->where(function ($q) use ($term) {
                    return $q->where('unsign_text', 'like', '%' . $term . '%')
                        ->with('admin')
                        ->orWhereHas('admin.info', function (Builder $query) use ($term) {
                            return $query->where('unsign_text', 'like', '%' . $term . '%');
                        });
                });
        }
        $query->with('admin', 'admin.info');

        if ($status != null) {
            $query->where('status', $status);
        }

        if ($type != null) {
            $query->where('type', $type);
        }

        if (!empty(Community::listTopicIdAllowed())) {
            $query->whereIn('topic_id', Community::listTopicIdAllowed())->orWhereNull('topic_id');
        }

        //anh ta add
        $data = $query->orderBy('id', 'DESC')->paginate($this->perPage);

        $typeAppraisal = EApproval::getListType();
        $statusAppraisal = EApproval::getListStatus();

        return view('livewire.admin.approval.approval-list', ['data' => $data, 'typeAppraisal' => $typeAppraisal, 'statusAppraisal' => $statusAppraisal]);
    }

    public function deleteId($id)
    {
        $this->approvalId = $id;
    }

    public function delete()
    {
        $approval = Approval::findOrFail($this->approvalId);
        $approval->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }

    public function export()
    {
        $exporter = null;
        if ($this->searchType == EApproval::TYPE_IDEAL) {
            $exporter = new ApprovalIdeaExport($this->searchTerm, $this->searchStatus);
        } elseif ($this->searchType == EApproval::TYPE_TOPIC) {
            $exporter = new ApprovalTopicExport($this->searchTerm, $this->searchStatus);
        } elseif ($this->searchType == EApproval::TYPE_PLAN) {
            $exporter = new ApprovalResearchPlanExport($this->searchTerm, $this->searchStatus);
        } elseif ($this->searchType == EApproval::TYPE_COST) {
            $exporter = new ApprovalTopicFeeExport($this->searchTerm, $this->searchStatus);
        }

        return Excel::download($exporter, 'list-approval-' . date('Y-m-d') . '.xlsx');
    }
}
