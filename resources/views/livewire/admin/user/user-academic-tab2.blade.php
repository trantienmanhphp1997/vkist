<div class="mt-5">
    <div class="row">
        <div class="col-md-6">
            <h3 class="title-personal">{{ __('data_field_name.user.certificate') }}</h3>
        </div>
        <div class="col-md-6 ">
            @if ($canEdit)
                <button class="btn btn-adduser float-right " type="button" data-toggle="modal" data-target="#createModal2"><img src="/images/plususer.svg" alt="plus"></button>
            @endif

        </div>
    </div>
    <table class="table border-bottom">
        <thead>
            <tr class="border-radius">
                <th scope="col" class="border-radius-left text-uppercase">{{ __('data_field_name.user.name_certificate') }}</th>
                <th scope="col" class="text-uppercase">{{ __('data_field_name.user.area') }}</th>
                <th scope="col" class="text-uppercase">{{ __('data_field_name.user.create_at') }}</th>
                <th scope="col" class="text-uppercase">{{ __('data_field_name.user.point') }}</th>
                <th scope="col" class="text-uppercase">{{ __('data_field_name.user.classification') }}</th>
                <th scope="col" class="border-radius-right">{{ __('data_field_name.common_field.action') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse($dataType2 as $val)
                <tr>
                <tr>
                    <td>{{ $val->name }}</td>
                    <td>{{ $val->school }}</td>
                    <td>{{ $val->graduated_year ? date('d-m-Y', strtotime($val->graduated_year)) : '' }}</td>
                    <td>{{ $val->score }}</td>
                    <td><a href="" class="btn-classification">{{ $val->rank_name }}</a></td>
                    <td>
                        <button type="button" class="btn par6" title="edit" wire:click="editId2({{ $val->id }})" data-toggle="modal" data-target="#updateModal2"><img src="/images/pent2.svg" alt=""></button>
                        <button type="button" class="btn par6" title="delete" data-toggle="modal" data-target="#exampleDelete2" wire:click="deleteId2({{ $val->id }})"><img src="/images/trash.svg" alt="trash"></button>
                    </td>
                </tr>
                </tr>
            @empty
                <tr>
                    <td colspan="6">{{ __('data_field_name.user.empty_certificate') }}</td>
                </tr>

            @endforelse
        </tbody>
    </table>
    {{ $dataType2->links() }}
    <div wire:init="openModal" wire:ignore.self class="modal fade" id="createModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('data_field_name.user.create_certificate') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.name_certificate') }}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control" wire:model.lazy="name" placeholder="{{ __('data_field_name.user.input_certificate') }}">
                            @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.area') }} <span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control" wire:model.lazy="school" placeholder="{{ __('data_field_name.user.input_area') }}">
                            @error('school') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.create_at') }}<span class="text-danger">(*)</span></label>
                            <input type="text" class="input-date-kendo form-control mb-0 graduated-year-2" id="graduated-year-2" placeholder="dd/mm/yyyy" value="{{ $graduated_year }}">
                            @error('graduated_year') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.point') }}</label>
                            <input type="number" class="form-control" wire:model.lazy="score" placeholder="{{ __('data_field_name.user.input_point') }}">
                            @error('score') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.classification') }}</label>
                            <select class="form-control" wire:model.lazy="rank_id">
                                <option value="">--{{ __('data_field_name.user.classification') }}--</option>
                                @foreach ($ranks as $key => $rank)
                                    <option value="{{ $key }}">{{ $rank }}</option>
                                @endforeach
                            </select>
                            @error('rank_id') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </form>
                </div>
                <div class="text-center">
                    <button type="button" id="close-academic-create2" wire:click.prevent="resetInputFields()" class="btn btn-main bg-4 text-3 size16 px-48 py-14" data-dismiss="modal">{{ __('common.button.delete') }}</button>
                    <button type="button" class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn" wire:click.prevent="storeType2()">{{ __('common.button.save') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div wire:init="openModal" wire:ignore.self class="modal fade" id="updateModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('data_field_name.user.edit_certificate') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.name_certificate') }}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control" wire:model.lazy="name" placeholder="{{ __('data_field_name.user.input_certificate') }}">
                            @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.area') }}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control" wire:model.lazy="school" placeholder="{{ __('data_field_name.user.input_area') }}">
                            @error('school') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.create_at') }} <span class="text-danger">(*)</span></label>
                            <input type="text" class="input-date-kendo form-control mb-0 graduated-year-2" id="graduated-year-2" placeholder="dd/mm/yyyy" value="{{ $graduated_year }}">
                            @error('graduated_year') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.point') }}</label>
                            <input type="number" class="form-control" wire:model.lazy="score" placeholder="{{ __('data_field_name.user.input_point') }}">
                            @error('score') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label>{{ __('data_field_name.user.classification') }}</label>
                            <select class="form-control" wire:model.lazy="rank_id">
                                <option value="">--{{ __('data_field_name.user.classification') }}--</option>
                                @foreach ($ranks as $key => $rank)
                                    <option value="{{ $key }}">{{ $rank }}</option>
                                @endforeach
                            </select>
                            @error('rank_id') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </form>
                </div>
                <div class="text-center">
                    <button type="button" id="close-academic-update2" wire:click.prevent="resetInputFields()" class="btn btn-main bg-4 text-3 size16 px-48 py-14" data-dismiss="modal">{{ __('common.button.delete') }}</button>
                    <button type="button" class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn" wire:click.prevent="storeType2()">{{ __('common.button.save') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="exampleDelete2" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{ __('data_field_name.common_field.confirm_delete') }}</h4>
                    {{ __('notification.member.warning.warning-1') }}
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.back') }}</button>
                    <button type="button" wire:click.prevent="delete2()" class="btn btn-save" data-dismiss="modal">{{ __('common.button.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
    @livewire('component.files', ['model_name'=>$model_name, 'model_id'=>$model_id, 'type'=>$type, 'folder'=>$folder])

    <script>
        window.addEventListener('render-user-academic-tab2-event', function() {
            initDatePicker('.graduated-year-2');
        });
    </script>
</div>
