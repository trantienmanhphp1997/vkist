
<div>
    <!-- table2 -->
    <div class="row">
        <div class="col-md-6">
            <h3 class="title-personal">{{__('data_field_name.working_process.compose_research_result')}}</h3>
        </div>
        <div class="col-md-6 d-flex flex-row-reverse bd-highlight justify-content-en ">
            @if ($canEdit)
            <div class="p-2 bd-highlight">
                <button type="button" data-toggle="modal" data-target="#exampleModal3" class="btn btn-adduser float-right "><img src="/images/plususer.svg" alt="plus"></button>
            </div>
            @endif
        </div>
    </div>
    <table class="table border-bottom">
        <thead>
            <tr class="border-radius">
                <th scope="col" > {{__('data_field_name.working_process.solution_name')}}</th>
                <th scope="col">{{__('data_field_name.working_process.degree_number')}}</th>
                <th scope="col">{{__('data_field_name.working_process.number_order')}}</th>
                <th scope="col">{{__('data_field_name.working_process.diploma_name')}}</th>
                <th scope="col">{{__('data_field_name.working_process.announced_year')}}</th>
                <th scope="col" class="border-radius-right">{{__('data_field_name.common_field.action')}}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data3 as $key => $val)
            <tr>
              <td>{{ $val-> name }}</td>
              <td>{{ $val-> license }}</td>
              <td>{{ $val-> number_license }}</td>
              <td>{{ $val-> author_name }}</td>
              <td>{{ $val-> date_created }}</td>
              <td>
                  <button type="button" class="btn par6" title="detail"><img src="/images/playlist2.svg" alt="playlist"></button>
                  <button type="button" data-toggle="modal" data-target="#exampleModalEdit3" wire:click="editReTable3({{ $val->id }})" class="btn"  title="edit"><img src="/images/pent2.svg" alt=""></button>
                  <button type="button" wire:click="getIdDeleteTable3({{$val->id}})" class="btn" data-toggle="modal" data-target="#deleteReTable3"><img src="/images/trash.svg" alt="trash"></button>
              </td>
          </tr>
          @empty
            <tr><td colspan="6">{{__('data_field_name.working_process.empty')}}</td></tr>
          @endforelse
      </tbody>
  </table>
  <!-- Modal Create -->
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('data_field_name.working_process.create_research_result')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">{{__('data_field_name.working_process.solution_name')}}<span class="text-danger">(*)</span></label>
                        <input type="text" class="form-control" placeholder="{{__('data_field_name.working_process.input_solution_name')}}" wire:model.lazy="name">
                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">{{__('data_field_name.working_process.degree_number')}}<span class="text-danger">(*)</span></label>
                        <input type="text" class="form-control" wire:model.lazy="license" placeholder="{{__('data_field_name.working_process.input_degree_number')}}">
                        @error('license') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">{{__('data_field_name.working_process.number_order')}}<span class="text-danger">(*)</span></label>
                        <input type="text" class="form-control" wire:model.lazy="number_license" placeholder="{{__('data_field_name.working_process.input_number_order')}}">
                        @error('number_license') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">{{__('data_field_name.working_process.diploma_name')}}</label>
                        <input type="text" class="form-control" wire:model.lazy="author_name" placeholder="{{__('data_field_name.working_process.input_diploma_name')}}">
                        @error('author_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">{{__('data_field_name.working_process.announced_year')}}<span class="text-danger">(*)</span></label>
                        <input type="number" class="form-control" wire:model.lazy="date_created" placeholder="{{__('data_field_name.working_process.input_announced_year')}}">
                        @error('date_created') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    </form>
                    </div>
                    <div class="modal-footer">
                    <button type="button" id="close-modal-create-researchTable3" class="btn btn-secondary close-btn" data-dismiss="modal" wire:click.prevent="resetInputFields3()">{{__('common.button.close')}}</button>
                    <button type="button" wire:click.prevent="store()" id="btn-store-1" class="btn btn-primary close-modal">{{__('common.button.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
<!-- end modal create -->
<!-- modal edit -->
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="exampleModalEdit3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('data_field_name.working_process.edit_research_result')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                 </button>
             </div>
             <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">{{__('data_field_name.working_process.solution_name')}}<span class="text-danger">(*)</span></label>
                        <input type="text" class="form-control" placeholder="{{__('data_field_name.working_process.input_solution_name')}}" wire:model.lazy="name">
                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">{{__('data_field_name.working_process.degree_number')}}<span class="text-danger">(*)</span></label>
                        <input type="text" class="form-control" wire:model.lazy="license" placeholder="{{__('data_field_name.working_process.input_degree_number')}}">
                        @error('license') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">{{__('data_field_name.working_process.number_order')}}<span class="text-danger">(*)</span></label>
                        <input type="text" class="form-control" wire:model.lazy="number_license" placeholder="{{__('data_field_name.working_process.input_number_order')}}">
                        @error('number_license') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">{{__('data_field_name.working_process.diploma_name')}}</label>
                        <input type="text" class="form-control" wire:model.lazy="author_name" placeholder="{{__('data_field_name.working_process.input_diploma_name')}}">
                        @error('author_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">{{__('data_field_name.working_process.announced_year')}}<span class="text-danger">(*)</span></label>
                        <input type="number" class="form-control" wire:model.lazy="date_created" placeholder="{{__('data_field_name.working_process.input_announced_year')}}">
                        @error('date_created') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="close-modal-edit-researchTable3" class="btn btn-secondary close-btn" data-dismiss="modal"  wire:click.prevent="resetInputFields3()">{{__('common.button.close')}}</button>
                <button type="button" wire:click.prevent="store()" id="btn-store-1"  class="btn btn-primary close-modal">{{__('common.button.save')}}</button>
            </div>
        </div>
        </div>
        </div>
    </form>
<!-- end modal edit -->
<!-- Modal Delete -->
    <div wire:ignore.self class="modal fade" id="deleteReTable3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <strong><h5 class="modal-title" id="exampleModalLabel">{{__('notification.member.warning.warning')}}</h5></strong>
                    </div>
                    <div class="modal-body">
                    {{__('notification.member.warning.warning-1')}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('common.button.back')}}</button>
                        <button type="button" class="btn btn-danger" data-dismiss = "modal" wire:click="deleteReTable3()">{{__('common.button.delete')}}</button>
                    </div>
                </div>
        </div>
    </div>
</div>
@livewire('component.files', ['model_name'=>$model_name, 'model_id'=>$model_id, 'type'=>$type, 'folder'=>$folder])
<!-- End Modal Delete -->

