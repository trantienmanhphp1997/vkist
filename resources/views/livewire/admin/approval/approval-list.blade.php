<div class="col-md-10 col-xl-11 box-detail box-user list-role">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="{{route('admin.approval.index')}}">{{__('data_field_name.approval_topic.text_manager_approval')}} </a> \ <span>{{__('data_field_name.approval_topic.list_review')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('menu_management.menu_name.list_of_approvals')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group search-expertise">
                                <div class="search-expertise inline-block">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control" placeholder="{{__('data_field_name.approval_topic.search_profile_sender')}}">
                                    <span><img src="../images/Search.svg" alt="search"></span>
                                </div>
                                <div class="w-170 inline-block">
                                    <select class="size13 form-control form-control-lg" wire:model.lazy="searchType">
                                        <option value="">{{__('data_field_name.approval_topic.profile_type')}}</option>
                                        @foreach($typeAppraisal as $key => $type)
                                            <option value="{{ $key }}"">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="w-170 inline-block">
                                    <select class="size13 form-control form-control-lg" wire:model.lazy="searchStatus">
                                        <option value="">{{__('data_field_name.common_field.status')}}</option>
                                        @foreach($statusAppraisal as $key => $status)
                                            <option value='{!! $key !!}'>{!! $status !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-right">
                            @if (!empty($searchType))
                                <button type="button" class="mr-2" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#export-modal"> <img src="/images/filterdown.svg" alt="filterdown"> </button>
                            @endif

                            @if($checkCreatePermission)
                                <span wire:ignore>
                                    <a class="btn btn-viewmore-news mr0" href="{{ route('admin.approval.create') }}"><img src="/images/plus2.svg" alt="plus">
                                        {{__('data_field_name.approval_topic.text_create_profile')}}
                                    </a>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                                <tr class="border-radius text-center">
                                    <th scope="col" class="border-radius-left">{{__('data_field_name.approval_topic.profile_code')}}</th>
                                    <th scope="col">{{__('data_field_name.approval_topic.name_profile')}}</th>
                                    <th scope="col">{{__('data_field_name.approval_topic.text_sender')}}</th>
                                    <th scope="col">{{__('data_field_name.approval_topic.profile_type')}}</th>
                                    <th scope="col">{{__('data_field_name.common_field.status')}}</th>
                                    <th scope="col">{{__('data_field_name.approval_topic.appraisal_board')}}</th>
                                    <th scope="col" class="border-radius-right">{{ __('data_field_name.common_field.action') }}</th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                                @foreach($data as $row)
                                    <tr>
                                        <td class="text-center">
                                            @if($checkEditPermission)
                                                <a href="{{route('admin.approval.detail', $row->id)}}">{!! $row->code ?? __('data_field_name.approval_topic.text_code_not') !!}</a>
                                            @else
                                                {!! __('data_field_name.approval_topic.text_code_not') !!}
                                            @endif
                                        </td>
                                        <td class="text-center">{!! boldTextSearchV2($row->name, $searchTerm) !!}</td>
                                        <td class="text-center">{!! isset($row->admin->info->fullname) ? boldTextSearchV2($row->admin->info->fullname, $searchTerm) : '' !!}</td>
                                        <td class="text-center">{!! isset($row->type) ? $typeAppraisal[$row->type] : '' !!}</td>
                                        <td class="text-center">{!! $statusAppraisal[$row->status] !!}</td>
                                        <td class="text-center">{!! $row->appraisal->name ?? __('data_field_name.approval_topic.text_not_yet') !!}</td>
                                        <td class="text-center">
                                            @if($checkEditPermission)
                                                <a title="{{__('common.button.edit')}}" href="{{ route('admin.approval.edit', $row->id) }}">
                                                    <img src="/images/pent2.svg" alt="pent">
                                                </a>
                                            @endif
                                            @if ($row->status == \App\Enums\EApproval::STATUS_NEW)
                                                @include('livewire.common.buttons._delete')
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center ml-0">
                            <span >{{__('common.message.no_data')}}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')
                    @include('livewire.common.modal._modalConfirmExport')
                </div>
            </div>
        </div>
    </div>
</div>
