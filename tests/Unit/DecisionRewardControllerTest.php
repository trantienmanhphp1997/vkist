<?php

namespace Tests\Unit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Livewire;
use App\Models\DecisionReward;
use App\Http\Livewire\Admin\DecisionReward\DecisionRewardCreateAndUpdate;
use App\Enums\EDecisionRewardDecisionType;
use App\Enums\EDecisionRewardPrizeType;



class DecisionRewardControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */

     /** @test */
    function can_create_decision_reward()
    {
        $user =  $this->user;
        $this->actingAs($user);
        Livewire::test(DecisionRewardCreateAndUpdate::class)
            ->set([
                'name' => 'Khen thưởng quý 1',
                'reward_date' => date('Y-m-d'),
                'number_decision' => 'KT001',
                'type_of_decision' => EDecisionRewardDecisionType::REWARD,
                'reason' => 'Thưởng năng suất quý 1',
                'decision_date' => date('Y-m-d'),
                'type_of_prize' => EDecisionRewardPrizeType::MONEY, 
                'total_value' => '100000000',
                'base' => 'Chính sách khen thưởng quý 1',
                ])
            ->call('store');

        $this->assertTrue(DecisionReward::whereName('Khen thưởng quý 1')->exists());
    }

    /** @test */
    public function can_index_decision_reward()
    {
        $user = $this->user;
        $this->actingAs($user)->get(route('admin.decision-reward.index'))->assertOk();
    }
}
