<div wire:ignore.self class="modal fade modal-fix" id="nhanvien" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.allocated-revoke.add_employee')}}</h6>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pd-col">
                                <div class="form-group info-request searchbar">
                                    <label>{{__('common.button.search')}}</label>
                                    <div class="position-relative">
                                        <input type="search" placeholder="{{__('common.button.search')}}" wire:model.debounce.900ms="name">
                                        <img  class="search-icon" src="/images/Search.svg" alt="search">
                                    </div>
                                </div>
                            </div>
                            @foreach($modalUserList as $index => $item)
                                <div class="col-12 pd-col">
                                    <div class="form-check info-request">
                                        @if($item['checked'] == 1)
                                            <input type="checkbox" checked="" wire:click="chooseUser({{$item['id']}})">
                                        @else
                                            <input type="checkbox" wire:click="chooseUser({{$item['id']}})">
                                        @endif
                                        <label class="m-0" for="{{$index}}">{{$item['fullname']}}</label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.close')}}</button>
            </div>
        </div>
    </div>
</div>
