<?php

namespace App\Notifications\Drivers;

use App\Notifications\BaseNotification;

class CustomDatabaseDriver
{

    public function send($notifiable, BaseNotification $notification)
    {
        $message = $notification->toCustomDatabase($notifiable);

        // Send notification to the $notifiable instance...
        \App\Models\Notification::query()->create([
            'type' => $notification->type,
            'admin_id' => $notifiable->id,
            'data' => $message,
            'read_at' => null
        ]);
    }
}
