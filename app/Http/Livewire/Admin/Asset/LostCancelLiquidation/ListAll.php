<?php

namespace App\Http\Livewire\Admin\Asset\LostCancelLiquidation;

use Livewire\Component;

class ListAll extends Component
{
    public function render()
    {
        return view('livewire.admin.asset.lost-cancel-liquidation.list-all');
    }
}
