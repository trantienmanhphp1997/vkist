<div class=" tab-pane" id="distribute" wire:ignore.self>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.info_dis')}}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.value_asset_distribute')}}</label>
                    <input type="text" class="form-control format_number" placeholder="" value="{{numberFormat($assetDetail->asset_value)??'0'}}" readonly>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.number_distribute_year')}}</label>
                    <input type="number" class="form-control" placeholder=""
                           value="{{$assetDetail->distribute_value??'0'}}"  maxlength="2" readonly>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.number_distribute_month')}}</label>
                    <input type="text" class="form-control" placeholder="" value="{{($assetDetail->distribute_value)*12}}" readonly>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.distribute_start')}}</label>
                    <input type="text" class="form-control" placeholder=""
                           value="{{reFormatDate($assetDetail->distribute_start_date)? date('d/m/Y',strtotime($assetDetail->distribute_start_date)):''}}" readonly>
                </div>

                <div class="form-group">
                    <label>{{__('data_field_name.asset.depreciation_year')}}</label>
                    <input type="text" class="form-control" placeholder="" value="{{$assetDetail->distribute_value?numberFormat(round($assetDetail->asset_value/$assetDetail->distribute_value, 1, PHP_ROUND_HALF_UP)):'0'}}" readonly>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.depreciation_month')}}</label>
                    <input type="text" class="form-control" placeholder="" value="{{$assetDetail->distribute_value?numberFormat(round($assetDetail->asset_value/($assetDetail->distribute_value*12), 1, PHP_ROUND_HALF_UP)):'0'}}" readonly>
                </div>
            </div>
        </div>
    </div>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.depreciation_value')}}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.depreciation_value_previous')}}</label>
                    <input type="text" class="form-control format_number" placeholder="" value="{{numberFormat($assetDetail->depreciation_value_previous) ?? '0'}}" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.depreciation_value_left')}}</label>
                    <input type="text" class="form-control" placeholder=""  value="{{$assetDetail->depreciation_value_previous?numberFormat($assetDetail->asset_value-$assetDetail->depreciation_value_previous):'0'}}" readonly>
                </div>
            </div>
        </div>
    </div>


</div>
