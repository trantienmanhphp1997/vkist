<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PhpParser\Node\Expr\FuncCall;
use Tests\TestCase;

class BusinessFeeControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @Test */
    public function testRenderBusinessFee(){
        $this->actingAs($this->user)->get(route('admin.general.businessfee.index'))->assertOk();
    }
    public function testStoreBusinessFee(){
        $this->actingAs($this->user)->get(route('admin.general.businessfee.create'))->assertOk();
        $this->actingAs($this->user)->post(route('admin.general.businessfee.store'),[
            'code' => 'aaaaaa',
            'name' => 'aaa',
            'accounting_code' => 'a',
            'type' => 'a',
            'status' => 1,
            'admin_id'=>1,
            'created_at' => date("d-m-Y"),
            'updated_at' =>date("d-m-Y"),
            'note'=>'aaaa',
        ])->assertRedirect(route('admin.general.businessfee.create'));
    }
      /** @Test */
      public function testCreateBusinessFee(){
        $this->actingAs($this->user)->get(route('admin.general.businessfee.index'))->assertOk();
    }


}
