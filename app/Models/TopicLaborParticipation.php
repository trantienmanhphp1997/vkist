<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopicLaborParticipation extends BaseModel
{
    use HasFactory;

    protected $table = 'topic_labor_participation';
    protected $fillable = [
    	'id', 'wage_coefficient', 'number_workday', 'total', 'topic_has_user_info_id', 'state_capital', 'other_capital', 'topic_fee_detail_id', 'admin_id'
    ];

    public function topicFeeDetail() {
        return $this->belongsTo(TopicFeeDetail::class, 'topic_fee_detail_id');
    }

    public function topicHasUserInfo(){
        return $this->belongsTo(TopicHasUserInfo::class, 'topic_has_user_info_id');
    }
}
