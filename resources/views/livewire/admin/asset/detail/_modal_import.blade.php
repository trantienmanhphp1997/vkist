<div wire:ignore.self class="modal fade modal-fix" id="import" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 444px !important">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.asset.title_upload')}}</h6>
            </div>
            <div class="modal-body" style="font-size: unset;">
                <form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <form wire:submit.prevent="submit" class='pull-left'>
                                    <div class="form-group  info-request2 mb-0">
                                        <input type="file"
                                               class="custom-file-input"
                                               wire:model.lazy="fileExcel" id="input-file" accept=".xlsx, .xls">
                                        <input id = 'input_file' type='text' value="{{!empty($fileExcel)?$fileExcel->getClientOriginalName():''}}"
                                            placeholder = "{{__('data_field_name.modal_result.placeholderInput')}}" style='cursor: pointer;'readonly>
                                        <label for="input-file" class="attachfile-field m-auto" style='cursor: pointer;'>
                                            <span><img src="/images/Clip-blue.svg" alt="clip-blue"></span>
                                        </label>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <!-- @if(!empty($fileExcel))
                                        <div class="form-group col-md-12">
                                            <div class="attached-files mt-2">
                                                <img src="/images/File.svg">
                                                <div class="content-file">
                                                    <p class="kb" style="word-break: break-all;">{{ $fileExcel->getClientOriginalName() }}</p>
                                                </div>
                                                <span>
                                                <button wire:click.prevent="deleteFile"
                                                        style="border: none;background: white" type="button"><img
                                                        src="/images/close.svg"/>
                                                </button>
                                            </span>
                                            </div>
                                        </div>
                                    @endif -->
                                    @error('fileExcel')
                                        <div class="text-danger mt-1" style="margin-left: 13px">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="row">
                                    <div class="text-danger mt-1" style="margin-left: 13px" wire:loading wire:target="fileExcel">
                                        {{__('data_field_name.asset.wait_upload')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12" >
                    <div class="form-group" style='margin:0px;'>
                        <a href="#" id='getExampleFile' wire:click='getExampleFile'>{{__('data_field_name.modal_result.sample_file')}}</a>
                    </div>
                </div>
                <button id='btn-close-upload'type="button" class="btn btn-cancel" data-dismiss="modal">{{__('data_field_name.modal_result.cancel')}}</button>
                <button id='btn-save' type="button" class="btn btn-primary" wire:loading.attr="disabled">{{__('data_field_name.modal_result.save')}}</button>
                <button id='btn-save1' type="button" style='display:none;' class="btn btn-primary" wire:click.prevent='saveData'>{{__('data_field_name.modal_result.save')}}</button>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function(){
        $('#btn-save').click(function(){
            $('#btn-save1').click()
        })
        $('#btn_download_error').click(function(){
            var data = $('#input_error').val()
            window.livewire.emit('getError', data);
        })
        $('#getExampleFile').click(function(e){
            e.preventDefault()
        })
        $('#input_file').click(function(){
            $('#input-file').click()
        })
        Livewire.on('closeModalImport', postId => {
            $('#btn-close-upload').click()
        })
        Livewire.on('openModalResult', postId => {
            $('#result').modal('show')
        })

    })
</script>
<div wire:ignore.self class="modal fade modal-fix" id="result" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 444px !important">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.modal_result.DownloadresultError')}}</h6>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pd-col">
                                <input id='input_error' type='text' value="{{$error}}" style='display:none;'>
                                <!-- @if($fileSuccessMessage)
                                <div class="text-success">
                                    {{$fileSuccessMessage}}
                                </div>
                                <div class="text-danger">
                                    {{ __('server_validation.import-time-sheets.msg.error', ['value' => $numItemData - $numItemSaved])}}
                                </div>
                                <div class="font-weight-bold">
                                    {{__('server_validation.import-time-sheets.reason')}}
                                </div>
                                @endif -->
                                <!-- @if(!empty($fileErrorMessage) && count($fileErrorMessage) > 0)
                                    @foreach($fileErrorMessage as $item)
                                        <div class="text-danger mt-1" style="margin-left: 13px">{{$item}}</div>
                                    @endforeach
                                @endif -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('data_field_name.modal_result.close')}}</button>
                <button id='btn_download_error' type="button" class="btn btn-primary" data-dismiss="modal">{{__('data_field_name.modal_result.Download')}}</button>
            </div>
        </div>
    </div>
</div>
