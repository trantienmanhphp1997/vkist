<?php

namespace App\Http\Livewire\Admin\Executive\Shopping;

use App\Enums\EShoppingStatus;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Shopping;
use Illuminate\Database\Eloquent\Builder;
use App\Exports\ShoppingListExport;
use Excel;

class ShoppingList extends BaseLive
{

    public array $shoppingStatus;
    public array $shoppingHTMLClassList;

    public $searchStatus;
    public $currentWork = false;
    public $checkApprovePermission = false;
    public $checkEditPermission = false;
    public $checkDownloadPermission = false;

    public function mount()
    {
        $this->searchStatus = '';
        $this->checkApprovePermission = checkRoutePermission('approve');
        $this->checkEditPermission = checkRoutePermission('edit');
        $this->checkDownloadPermission = checkRoutePermission('download');
    }

    public function render()
    {

        $this->shoppingStatus = $this->getShoppingStatus();
        $this->shoppingHTMLClassList = EShoppingStatus::getHTMLClassList();

        $query = Shopping::query();

        if ($this->currentWork) {
            $query->where('executor_id', auth()->id())->whereIn('status', [EShoppingStatus::PENDING, EShoppingStatus::DELIVERING]);
        } else if (!$this->currentWork && strlen($this->searchStatus) > 0) {
            $query->where('status', '=', $this->searchStatus);
        }

        // quản lý mua sắm được xem các đề xuất trừ chưa phê duyệt hoặc bị từ chối
        if (!$this->checkApprovePermission) {
            $query->whereNotIn('status', [EShoppingStatus::NOT_APPROVED, EShoppingStatus::REJECTED]);
        }

        if (strlen($this->searchTerm) > 0) {
            $text = $this->searchTerm;
            $unsignText = strtolower(removeStringUtf8($this->searchTerm));
            $query->whereRaw("unsign_text LIKE '%$unsignText%'")
                ->orWhereHas('budget', function (Builder $query) use ($text, $unsignText) {
                    $query->whereRaw("LOWER(name) LIKE LOWER('%$text%')")
                        ->orWhereRaw("unsign_text LIKE '%$unsignText%'");
                });
        }

        $query->with(['executor:id,name', 'budget:id,name', 'proposer:id,name,user_info_id', 'proposer.info.department:id,name']);

        $data = $query->orderBy('created_at', 'desc')->paginate($this->perPage);
        return view('livewire.admin.executive.shopping.shopping-list', [
            'data' => $data
        ]);
    }

    public function getShoppingStatus()
    {
        $shoppingStatus = EShoppingStatus::getList();
        // nếu là quản lý mua sắm => không hiển thị trạng thái chưa phê duyệt || bị từ chối
        if (!$this->checkApprovePermission) {
            unset($shoppingStatus[EShoppingStatus::NOT_APPROVED]);
            unset($shoppingStatus[EShoppingStatus::REJECTED]);
        }

        return $shoppingStatus;
    }

    public function export() {
        return Excel::download(new ShoppingListExport($this->searchTerm, $this->currentWork, $this->searchStatus, $this->checkApprovePermission), 'shopping-' . now() . '-' . '.xlsx');
    }
}
