<div wire:ignore.self class="modal fade modal-fix" id="import" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 444px !important">
        <div class="modal-content">
            <div class="modal-header">
                <h6>
                    @if(!empty($idShow))
                        {{__('data_field_name.import-salary.reload')}}
                    @else
                        {{__('data_field_name.import-salary.title-add')}}
                    @endif
                </h6>
            </div>
            <div class="modal-body" style="font-size: unset;">
                <form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.import-salary.choose-time')}}<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <select wire:model.lazy="month" class="form-control" @if($disableSave || $disableInput) disabled @endif>
                                        <option value=''>{{__('data_field_name.import-salary.month_placeholder')}}</option>
                                        @if ($checkBasicSalary || $checkPositionSalary)
                                            @for($i = 1; $i < 13; $i++)
                                                <option value="{{$i}}">{{App\Enums\EMonth::valueToName($i)}}</option>
                                            @endfor
                                        @endif
                                    </select>
                                    @error('month')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request mb-0">
                                    @if(empty($idShow))
                                        <select wire:model.lazy="year" class="form-control" @if($disableSave || $disableInput) disabled @endif>
                                            <option value=''>{{__('data_field_name.import-salary.year_placeholder')}}</option>
                                            @if ($checkBasicSalary || $checkPositionSalary)
                                                @for($i = 1990; $i < now()->year + 100; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            @endif
                                        </select>
                                    @else
                                    @endif
                                </div>
                                @error('year')
                                    <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('executive/import-salary.table_column.type')}}<span class="text-danger">*</span></label>
                                    @if(empty($idShow))
                                        <select wire:model.lazy="typeSalary" class="form-control" @if($disableSave) disabled @endif>
                                            <option value=''>{{__('common.select-box.choose')}}</option>
                                            @if ($checkBasicSalary)
                                            <option value="{{App\Enums\ESalarySheetsFileType::BASIC_SALARY}}">{{App\Enums\ESalarySheetsFileType::valueToName(App\Enums\ESalarySheetsFileType::BASIC_SALARY)}}</option>
                                            @endif
                                            @if ($checkPositionSalary)
                                            <option value="{{App\Enums\ESalarySheetsFileType::POSITION_SALARY}}">{{App\Enums\ESalarySheetsFileType::valueToName(App\Enums\ESalarySheetsFileType::POSITION_SALARY)}}</option>
                                            @endif
                                        </select>
                                    @else
                                        <select wire:model.lazy="typeSalary" disabled="" class="form-control" @if($disableSave) disabled @endif>
                                            <option value=''>{{__('common.select-box.choose')}}</option>
                                            @if ($checkBasicSalary && $typeSalary == App\Enums\ESalarySheetsFileType::BASIC_SALARY)
                                            <option value="{{App\Enums\ESalarySheetsFileType::BASIC_SALARY}}">{{App\Enums\ESalarySheetsFileType::valueToName(App\Enums\ESalarySheetsFileType::BASIC_SALARY)}}</option>
                                            @endif
                                            @if ($checkPositionSalary && $typeSalary == App\Enums\ESalarySheetsFileType::POSITION_SALARY)
                                            <option value="{{App\Enums\ESalarySheetsFileType::POSITION_SALARY}}">{{App\Enums\ESalarySheetsFileType::valueToName(App\Enums\ESalarySheetsFileType::POSITION_SALARY)}}</option>
                                            @endif
                                        </select>
                                    @endif
                                    @error('typeSalary')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.import-salary.name')}}<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" @if($disableSave) disabled @endif placeholder="{{__('data_field_name.import-salary.name')}}" wire:model.defer="name" class="form-control" style="border-radius: 12px">
                                        <div class="input-group-append">
                                            @if(!$disableSave)
                                            <form wire:submit.prevent="submit">
                                                <div class="form-group info-request mb-0">
                                                    <input type="file"
                                                           class="custom-file-input"
                                                           wire:model="file" id="input-file">
                                                    <label for="input-file" class="attachfile-field m-auto"><span><img src="/images/Clip-blue.svg" alt=""></span></label>
                                                </div>
                                            </form>
                                            @endif
                                        </div>
                                    </div>
                                    @error('name')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            @if(empty($idShow))
                                <div class="col-12 pd-col">
                                    <div class="ml-5">
                                        <a href="/template/salary_basic.xlsx" download="">{{__('executive/import-salary.template-salary-basic')}}</a>
                                    </div>
                                    <div class="ml-5">
                                        <a href="/template/salary_position.xlsx" download="">{{__('executive/import-salary.template-salary-position')}}</a>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-12">
                                <div class="row">
                                    @if(!empty($file))
                                        <div class="form-group col-md-12">
                                            <div class="attached-files mt-2">
                                                <img src="/images/File.svg" alt="file">
                                                <div class="content-file">
                                                    <p class="kb" style="word-break: break-all;">{{ $file->getClientOriginalName() }}</p>
                                                </div>
                                                <span>
                                                <button wire:click.prevent="deleteFile"
                                                        style="border: none;background: white" type="button"><img
                                                        src="/images/close.svg" alt="close/>
                                                </button>
                                            </span>
                                            </div>
                                        </div>
                                    @endif
                                    @error('file')
                                        <div class="text-danger mt-1" style="margin-left: 13px">{{$message}}</div>
                                    @enderror
                                    @if(!empty($fileErrorMessage) && count($fileErrorMessage) > 0)
                                        @foreach($fileErrorMessage as $item)
                                            <div class="text-danger mt-1" style="margin-left: 13px">{{$item['error']}}</div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                @if($disableSave)
                    <button id='btn-save-disable' type="button" class="btn btn-primary" disabled="">{{__('common.button.save')}}</button>
                    <div class="text-danger mt-1">{{ __('data_field_name.general_config.text_warning_import') }}</div>
                @else
                    <button id='btn-save' type="button" class="btn btn-primary" wire:click.prevent="saveData">{{__('common.button.save')}}</button>
                @endif
            </div>
        </div>
    </div>
</div>

@if($showResult)
    <div wire:ignore.self class="modal fade modal-fix" id="result" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 444px !important">
            <div class="modal-content">
                <div class="modal-header">
                    <h6>{{__('executive/import-salary.result')}}</h6>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 pd-col">
                                    @if($fileSuccessMessage)
                                    <div class="text-success">
                                        {{$fileSuccessMessage}}
                                    </div>
                                    <div class="text-danger">
                                        {{ __('server_validation.import-time-sheets.msg.error', ['value' => $numItemData - $numItemSaved])}}
                                    </div>
                                    @endif
                                    @if(!empty($fileErrorExport) && count($fileErrorExport) > 0)
                                        <a href="#" class="mt-1" wire:click="downloadErrorFile">{{__('executive/import-time-sheets.download_error_list_file')}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
@endif
