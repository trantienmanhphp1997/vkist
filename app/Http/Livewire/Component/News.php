<?php

namespace App\Http\Livewire\Component;

use App\Http\Livewire\Base\BaseLive;


class News extends BaseLive
{
    public $image_path ;
    public $name ;
    public $date ;
    public $admin ;
    public $category ;
    public $description ;


    public function render()
    {
        $news = \App\Models\News::query()->with(['admin','category'])->where(['status'=>\App\Models\News::APPROVED_NEWS])
            ->where(['published'=>1])
            ->take(3)->orderBy('id','DESC')
            ->get();
        foreach($news as $new){
            $new->image_path = str_replace("uploads","",$new->image_path);
        }
        return view('livewire.component.news',['news'=>$news]);
    }

    public function getDetail($slug,$categorySlug){
        if (!$categorySlug){
            $categorySlug = 'not-khown';
        }
        return redirect(route('news.detail',['slug'=>$slug,'categorySlug'=>$categorySlug]));
    }

}
