<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_project', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_code', 255)->unique()->comment('Mã dự án');
            $table->string('name', 500)->unique()->comment('Tên dự án');
            $table->string('contract_code', 255)->unique()->comment('Mã hợp đồng');
            $table->foreignId('research_category_id')->nullable()->constrained('research_category')->comment('research_category_id map mới bảng research category');
            $table->tinyInteger('status')->default(0)->comment('0: chờ phê duyệt , 1: đang thực hiện, 2:hoàn thành , 3:Hủy bỏ');
            $table->date('start_date')->nullable()->comment('từ ngày');
            $table->date('end_date')->nullable()->comment('đến ngày');
            $table->string('description',1000)->nullable()->comment('Mô tả');
            $table->integer('project_scale')->comment('Man month');
            $table->foreignId('leader_id')->nullable()->constrained('topic')->comment('leader_id map mới bảng topic');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');

            $table->softDeletes();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_project');
    }
}
