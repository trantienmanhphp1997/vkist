@extends('layouts.master')
@section('title')
    <title>    {{__('research/taskwork.update')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    <livewire:admin.research-project.task-work.form-data :id="$id"/>
@endsection
