<?php

namespace App\Http\Livewire\Admin\Asset\AssetNotice;

use App\Enums\EAssetSituation;
use App\Enums\EAssetStatus;
use App\Models\UserInf;
use Livewire\Component;

class AssetInfo extends Component
{
    public $assetDetail;
    public $assetSituation;
    public $assetStatus;
    public function mount()
    {
        $this->assetSituation = [
            EAssetSituation::SITUATION_NOT_USE =>__('data_field_name.asset.no_use'),
            EAssetSituation::SITUATION_USING => __('data_field_name.asset.using'),
        ];
        $this->assetStatus = EAssetStatus::getList();
    }
    public function render()
    {
        return view('livewire.admin.asset.asset-notice.asset-info');
    }
}
