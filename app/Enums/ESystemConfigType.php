<?php

namespace App\Enums;

class ESystemConfigType {
    const NOTIFICATION = 1;
    const USER_INFO_LEAVE = 2;
    const TYPE_IMPORT_SALARY = 3;
}
