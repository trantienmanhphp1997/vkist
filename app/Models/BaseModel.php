<?php

namespace App\Models;

use App\Notifications\Handlers\NotificationHandler;
use App\Notifications\ModuleDataChangeNotification;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

//diepth2
// apply for table with admin_id; start_time; end_time
abstract class BaseModel extends Eloquent
{
    use SoftDeletes;
    protected static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->admin_id = auth()->check() ? auth()->id() : $model->admin_id;
            $arrSearchAble  = self::getSearchTextFiels();
            if ($arrSearchAble) {
                $unsign_text = '';
                $separate = '';
                foreach ($arrSearchAble as $keyColumn) {
                    $unsign_text =  $unsign_text . $separate . removeStringUtf8($model->$keyColumn);
                }
                $model->unsign_text = strtolower($unsign_text);
            }
        });
        self::updating(function ($model) {
            $arrSearchAble  = self::getSearchTextFiels();
            if ($arrSearchAble) {
                $unsign_text = '';
                $separate = '';
                foreach ($arrSearchAble as $keyColumn) {
                    $unsign_text =  $unsign_text . $separate . removeStringUtf8($model->$keyColumn);
                }
                $model->unsign_text = strtolower($unsign_text);
            }
        });

        self::created(function (BaseModel $model) {
            $model->notifyToDepartmentLeaders('create');
        });

        self::updated(function (BaseModel $model) {
            $model->notifyToDepartmentLeaders('update');
        });

        self::deleted(function (BaseModel $model) {
            $model->notifyToDepartmentLeaders('delete');
        });
    }
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
    public static function getSearchTextFiels()
    {
        if (method_exists(static::class, 'getListSearchAble')) {
            return static::getListSearchAble();
        }
        return false;
    }

    protected function notifyToDepartmentLeaders(string $action)
    {
        if (in_array(static::class, ModuleDataChangeNotification::NOTIFIABLE_MODELS)) {
            /**
             * @var \App\Models\User $user
             */
            $user = auth()->user();
            if($user){
              $leaders = $user->getDepartmentLeaders();
              NotificationHandler::sendNotification($leaders, new ModuleDataChangeNotification($user, $this, $action));
            }

        }
    }
}
