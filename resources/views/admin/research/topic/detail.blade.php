@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.topic.detail_topic') }}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection

@section('content')
    <!--start news -->
    <div class="col-md-10 col-xl-11 box-detail box-user">

        <div class="breadcrumbs">
            <a href="">{{ __('data_field_name.topic.topic_control') }}</a> \
            <span>{{ __('data_field_name.topic.detail') }}</span>
        </div>

        <div class="bd-border">
            <div class="detail-task information box-idea">
                <h4>{{ __('data_field_name.topic.detail_topic') }}</h4>
                <div class="group-tabs">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                <span>{{ __('data_field_name.topic.tab_general') }}</span>
                            </a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                                <span>{{ __('data_field_name.topic.tab_presentation') }}</span>
                            </a>
                            <a class="nav-item nav-link" id="user-tab" data-toggle="tab" href="#user" role="tab" aria-controls="nav-profile" aria-selected="false">
                                <span>{{ __('data_field_name.topic.tab_member') }}</span>
                            </a>
                            <a class="nav-item nav-link" id="user-tab" data-toggle="tab" href="#achievement" role="tab" aria-controls="nav-profile" aria-selected="false">
                                <span>{{ __('data_field_name.topic.tab_expected_result') }}</span>
                            </a>
                            <a class="nav-item nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="nav-profile" aria-selected="false">
                                <span>{{ __('data_field_name.topic.tab_history') }}</span>
                            </a>
                        </div>
                    </nav>
                    <div class="tab-content mt-3" id="nav-tabContent">
                        <div class="tab-pane fade active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width:30%" scope="col">{{ __('data_field_name.topic.status')}} </th>
                                    <td>{{ $topicStatus[$data->status] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.level') }}</th>
                                    <td>{{ $data->level->v_value ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.source') }}</th>
                                    <td>{{ $topicSource[$data->source] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.field') }}</th>
                                    <td>{{ $data->researchCategory->name ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.select_idea') }}</th>
                                    <td>{{ $data->ideal->name ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.idea_code') }}</th>
                                    <td>{{ $data->ideal->code ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.scientific_name') }}</th>
                                    <td>{{ $data->major_id ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.scientific') }}</th>
                                    <td>{{ $data->major->v_value }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.name') }}</th>
                                    <td>{{ $data->name }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.necessary') }}</th>
                                    <td>{{ $data->necessary }}</td>
                                </tr>

                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.overview') }}</th>
                                    <td>
                                        <p>{{ $data->overview }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        @livewire('component.files', ['name' => __('data_field_name.topic.topic_attachment'), 'model_name' => $model, 'model_id' => $data->id, 'type' => $fileOverview, 'folder' => 'topics', 'canUpload' => false, 'canDownload' => true])
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.target') }}</th>
                                    <td>{{ $data->target }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.space_scope') }}</th>
                                    <td>{{ $data->space_scope }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.start_time') }}</th>
                                    <td>{{ reformatDate($data->start_date) }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.end_time') }}</th>
                                    <td>{{ reformatDate($data->end_date) }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">{{ __('data_field_name.topic.expected_fee') }}</th>
                                    <td>
                                        <p>{{ numberFormat($data->expected_fee) }} VND</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        @livewire('component.files', ['name' => __('data_field_name.topic.research_documentation_attachment'), 'model_name' => $model, 'model_id' => $data->id, 'type' => $fileAttachment, 'folder' => 'topics', 'canUpload' => false, 'canDownload' => true])
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="inner-tab">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.topic.present_infomation') }}</label>
                                    <div>{{ $data->present_note }}</div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        @livewire('component.files', ['model_name' => $model, 'model_id' => $data->id, 'type' => $filePresentation, 'folder' => 'topics', 'canUpload' => false, 'canDownload' => true])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade " id="user" role="tabpanel" aria-labelledby="nav-profile-tab">
                            @livewire('admin.research.topic.tab-detail-user', ['user_detail_id' => $data->id])
                        </div>
                        <div class="tab-pane fade " id="achievement" role="tabpanel" aria-labelledby="nav-profile-tab">
                            @include('admin.research.topic._tabDetailAchievement',['data' => $data])
                        </div>
                        <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="nav-profile-tab">
                            @include('admin.research.topic._tabDetailHistory',['data' => $data,'history' => $history,'topicCheckSendFile' => $topicCheckSendFile])
                        </div>
                    </div>
                </div>

                @if ($data->status == \App\Enums\ETopicStatus::STATUS_APPROVAL)
                    <div class="row mb-5">
                        @livewire('common.list-attach-file', ['modelId' => $data->id, 'type' => \App\Enums\EApiApproval::TYPE_TOPIC])
                    </div>
                @endif

                <form class="text-center" action="{{ route('admin.research.topic.update') }}" method="POST">
                    <a href="{{ route('admin.research.topic.index') }}" class="btn btn-main bg-4 text-3 size16 px-48 py-14">{{ __('common.button.close') }}</a>
                    @if ($data->status == \App\Enums\ETopicStatus::STATUS_NOT_SUBMIT)
                        @csrf
                        <input type="hidden" name="topic_id" value="{{ $data->id }}">
                        <button class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn">{{ __('common.button.submit_topic') }}</button>
                    @endif
                </form>

            </div>
        </div>
    </div>
    <!--end news -->

@endsection
