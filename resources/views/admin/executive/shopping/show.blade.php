@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.shopping.list') }}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user bg-light rounded">
        <div class="row">
            <div class="breadcrumbs">
                <a class="text-primary" href="#">{{ __('data_field_name.shopping.shopping_management') }}</a> \ <span class="text-muted">{{ __('data_field_name.shopping.shopping_proposal') }}</span>
            </div>
        </div>

        <div class="mt-3 p-5 bg-white rounded">
            <h3>{{ __('data_field_name.shopping.general') }}</h3>

            <div class="form-group">
                <label>{{ __('data_field_name.shopping.name') }}</label>
                <input type="text" class="form-control" value="{{ $shopping->name }}" disabled>
            </div>

            <div class="form-group">
                <label>{{ __('data_field_name.shopping.budget') }}</label>
                <input type="text" class="form-control" value="{{ $shopping->budget->name ?? '' }}" disabled>
            </div>

            <div class="row">
                <div class="col-sm-6 form-group">
                    <label>{{ __('data_field_name.shopping.estimate_money') }}</label>
                    <input type="text" class="form-control format_number" value="{{ numberFormat($shopping->estimate_money) ?? '' }}" disabled>
                </div>

                <div class="col-sm-6 form-group">
                    <label>{{ __('data_field_name.shopping.needed_date') }}</label>
                    <input type="text" class="form-control" value="{{ reFormatDate($shopping->needed_date) }}" disabled>
                </div>

                @if (!$checkEditPermission || !$canEdit)
                    <div class="col-sm-6 form-group">
                        <label>{{ __('data_field_name.shopping.estimated_delivery_date') }}</label>
                        <input type="text" class="form-control" value="{{ reFormatDate($shopping->estimated_delivery_date) }}" disabled>
                    </div>
                @endif

            </div>

            <h3 class="title-asset pt-24">{{ __('data_field_name.shopping.device_list') }}</h3>
            <div id="device-list">
                @foreach ($shopping->devices as $device)
                    <div class="box-calendar col-md-12 device-info">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.shopping.device_name') }}</label>
                                    <input type="text" class="form-control" value="{{ $device->name }}" disabled>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.shopping.device_quantity') }}</label>
                                    <input type="text" class="form-control" value="{{ $device->quantity }}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="form-group mt-3">
                <label>{{ __('data_field_name.shopping.note') }}</label>
                <textarea class="form-control" rows="5" disabled>{{ $shopping->note }}</textarea>
            </div>

            <div class="mb-3 form-group">
                <label>{{ __('data_field_name.shopping.proposer') }}</label>
                <input type="text" class="form-control" value="{{ $shopping->proposer->name ?? '' }}" disabled>
            </div>

            @livewire('component.files', [
                'model_name' => $modelName,
                'model_id' => $shopping->id,
                'type' => $fileType,
                'folder' => 'shopping',
                'uploadOnShow' => 1,
                'acceptMimeTypes' => config('common.mime_type.general'),
                'canUpload' => ($checkEditPermission && $canEdit)
            ])

            @if ($checkApprovePermission)

                <form method="POST" action="{{ route('admin.executive.shopping.update', ['id' => $shopping->id]) }}" class="mb-3">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="action" value="validation">
                    <div class="form-group">
                        <label>{{ __('data_field_name.shopping.approved_note') }}</label>
                        <textarea name="approved_note" class="form-control" rows="10">{{ old('approved_note', $shopping->approved_note) }}</textarea>
                        @error('approved_note')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="text-center mt-42">
                        <button name="validation_type" value="reject" class="btn btn-main bg-primary-4 mr-20 text-9 size16 px-48 py-14">{{ __('data_field_name.shopping.reject') }}</button>
                        <button name="validation_type" value="approve" class="btn btn-main bg-primary text-9 size16 px-48 py-14">{{ __('data_field_name.shopping.approve') }}</button>
                        <a href="{{ route('admin.executive.shopping.index') }}" class="btn text-4 size16 px-48 py-14 float-right">{{ __('common.button.close') }}</a>
                    </div>
                </form>
            @else
                <div class="form-group mt-3">
                    <label>{{ __('data_field_name.shopping.approved_note') }}</label>
                    <textarea class="form-control" rows="5" disabled>{{ $shopping->approved_note }}</textarea>
                </div>
            @endif

            @if ($checkEditPermission && $canEdit)
                <form method="POST" action="{{ route('admin.executive.shopping.update', ['id' => $shopping->id]) }}" class="mb-3">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="action" value="edit">
                    <div class="form-group">
                        <label>{{ __('data_field_name.shopping.estimated_delivery_date') }}</label>
                        {{-- @dump($shopping->estimated_delivery_date) --}}
                        <input type="text" name="estimated_delivery_date" class="form-control input-date-kendo" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime($shopping->estimated_delivery_date)) }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('data_field_name.shopping.executor') }}</label>
                        {{ Form::select('executor_id', $availableUsers, $shopping->executor_id ?? auth()->id(), ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        <label>{{ __('data_field_name.shopping.status') }}</label>
                        {{ Form::select('status', $shoppingStatus, $shopping->status, ['class' => 'form-control']) }}
                    </div>
                    <div class="text-center">
                        <a href="{{ route('admin.executive.shopping.index') }}" class="btn btn-main bg-light text-4 size16 px-48 py-14 mr-3">{{ __('common.button.cancel') }}</a>
                        <button class="btn btn-main bg-primary text-9 size16 px-48 py-14">{{ __('common.button.save') }}</button>
                    </div>
                </form>
            @elseif ($checkEditPermission && !$canEdit)
                <div class="alert alert-danger">{{ __('data_field_name.shopping.alert_not_approved_or_rejected') }}</div>
            @endif
        </div>
    </div>
@endsection
