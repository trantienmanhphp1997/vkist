<?php
return [
    'breadcrumbs' => [
        'category-field-management-1' => 'Quản lý danh mục',
        'category-field-management-2' => 'Quản lý lĩnh vực nghiên cứu',
        'category-field-list' => 'Danh sách lĩnh vực nghiên cứu',
    ],
    'table_column' => [
        'code' => 'MÃ',
        'name' => 'TÊN LĨNH VỰC',
        'description' => 'MÔ TẢ',
        'note' => 'GHI CHÚ',
        'action' => 'HÀNH ĐỘNG',
    ],
    'title' => [
        'create' => 'Tạo mới',
        'edit' => 'Chỉnh sửa',
        'no-result' => 'Không có kết quả!',
        'part-1' => 'Hiển thị',
        'part-2' => 'lĩnh vực nghiên cứu trong',
    ],
    'modal' => [
        'title' => [
            'delete' => 'Xác nhận xoá',
            'create' => 'Thêm lĩnh vực nghiên cứu',
            'edit' => 'Sửa lĩnh vực nghiên cứu',
            'show' => 'Xem chi tiết lĩnh vực nghiên cứu',
        ],
        'body' => [
            'delete-warning' => 'Bạn có xóa không? Thao tác này không thể phục hồi!',
            'name' => 'Tên lĩnh vực nghiên cứu',
            'description' => 'Mô tả',
            'note' => 'Ghi chú',
        ],
        'footer' => [
            'back' => 'Quay lại',
            'delete' => 'Xoá',
            'close' => 'Đóng',
            'save' => 'Lưu',
        ]
    ],
    'empty-record' => 'Không có bản ghi nào',
    'placeholder' => [
        'search' => 'Tìm kiếm',
        'name' => "Nhập tên lĩnh vực nghiên cứu",
        'description' => "Nhập mô tả lĩnh vực nghiên cứu",
        'note' => "Nhập ghi chú lĩnh vực nghiên cứu",
    ],
    'validate' => [
        'name' => 'Tên lĩnh vực nghiên cứu',
        'note' => 'Ghi chú lĩnh vực nghiên cứu',
        'description' => 'Mô tả lĩnh vực nghiên cứu',
    ],
    'notification' => [
        'edit-success' => 'Chỉnh sửa thành công',
        'create-success' => 'Thêm mới thành công',
        'delete-fail' => 'Đã có ý tưởng liên quan đến lĩnh vực này. Không được xoá lĩnh vực',
    ]
];
