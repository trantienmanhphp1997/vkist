@extends('layouts.master')
<title>{{__('research/ideal.title.detail')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')

    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div  class="breadcrumbs"><a href="{{route('admin.research.ideal.index')}}">{{__('research/ideal.breadcrumbs.research-ideal-management')}}</a> \ <span>{{__('research/ideal.title.detail')}}</span></div> 
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
             
              <div class="detail-task information box-idea">
                    <h4>{{__('research/ideal.title.detail')}} {{$data->name}}</h4>
                    @livewire('admin.research.ideal.form-data', ['ideal' => $data, 'show_detail' => true])
              	</div>
            </div>
        </div>
    </div>

@endsection
