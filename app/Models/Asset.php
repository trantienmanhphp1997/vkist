<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Asset extends BaseModel implements Auditable
{
    use HasFactory;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $table = 'asset';
    protected $fillable = ['code', 'name', 'series_number', 'quantity', 'original_price', 'gifting_date',
        'follow_year', 'unit_type', 'buy_date', 'wasting_start_date'
        , 'status', 'situation', 'admin_id', 'category_id', 'user_info_id', 'guarantee_date', 'receiver_id',
        'department_id','manager_id','asset_value','origin','contract_number','asset_provider_id',
        'note','warranty_date_json','condition_warranty','expiry_warranty_date',
        'maintenance_date_json','other_information_json','maintenance_start_date',
        'distribute_value','distribute_left_times_json','distribute_number_times_json',
        'distribute_value_wait','distribute_start_date','fixed_assets','distribute_value_done','unit_id','amount_used',
        'increase_code','depreciation_value_previous','group_asset'
        ];

    public function userInfo()
    {
        return $this->hasOne(UserInf::class, 'id');
    }

    public function assetCate()
    {
        return $this->hasOne(AssetCategory::class, 'id');
    }

    protected $guarded = ['*'];

    public function unit() {
        return $this->belongsTo(MasterData::class, 'unit_id');
    }
    public function manager() {
        return $this->belongsTo(UserInf::class, 'manager_id');
    }
    public function provider()
    {
        return $this->belongsTo(AssetProvider::class, 'asset_provider_id');
    }
    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }
    public function category()
    {
        return $this->belongsTo(AssetCategory::class, 'category_id');
    }
    public function allocate()
    {
        return $this->hasOne(AssetAllocatedRevoke::class, 'asset_id');
    }
    public function allocatedRevoke(){
        return $this->hasMany(AssetAllocatedRevoke::class, 'asset_id');
    }
    public function maintenance()
    {
        return $this->hasMany(AssetMaintenance::class, 'asset_id');
    }
    public function user() {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }
    public function originAsset() {
        return $this->belongsTo(AssetOrigin::class, 'origin');
    }

    private static $searchable = [
        'name',
        'code'
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }

    public function inventory(){
        return $this->belongsToMany(AssetInventory::class,'inventory_details','asset_id','inventory_id');
    }
}
