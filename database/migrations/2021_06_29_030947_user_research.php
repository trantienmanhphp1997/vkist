<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserResearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_research', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('topic_name')->nullable();
            $table->string('role')->nullable();
            $table->string('foundation')->nullable()->comment('to chuc tai tro');
            $table->integer('budget')->nullable()->comment('kinh phi');
            $table->string('achievement')->nullable()->comment('thanh qua');
            $table->tinyInteger('status')->nullable()->comment('trang thai');
            $table->integer('created_year')->nullable()->comment(' nam nghien cuu');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('map với user_info');
            $table->foreign('user_info_id')->references('id')->on('user_info');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_research');

    }
}
