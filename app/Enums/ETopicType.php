<?php

namespace App\Enums;

class ETopicType {
    const NEW_TOPIC = 0;
    const CONTINUOUS_TOPIC = 1;

    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            ETopicType::NEW_TOPIC => __('data_field_name.topic.new_topic'),
            ETopicType::CONTINUOUS_TOPIC => __('data_field_name.topic.continuous_topic')
        ];
    }
}
