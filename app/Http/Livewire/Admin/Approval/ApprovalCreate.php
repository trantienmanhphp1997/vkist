<?php

namespace App\Http\Livewire\Admin\Approval;

use App\Enums\EApproval;
use App\Enums\EIdeaStatus;
use App\Enums\EResearchPlan;
use App\Enums\ETopicFee;
use App\Enums\ETopicStatus;
use App\Models\Approval;
use App\Http\Livewire\Base\BaseLive;
use App\Models\ApprovalIdealDetail;
use App\Models\Ideal;
use App\Models\ResearchPlan;
use App\Models\Topic;
use App\Models\TopicFee;
use Illuminate\Database\Eloquent\Builder;
use function Matrix\trace;

class ApprovalCreate extends BaseLive {

    public $searchTerm;
    public $searchStatus;
    public $searchType;
    public $type;
    public $status;
    public $name;
    public $code;
    public $topic_id;
    public $listTopic;
    public $listTopicFee;
    public $listPlan;
    public $listIdeal;
    public $ideal_id;
    public $text_ideal_error;
    public $approvalIdealId;
    public $topic_fee_id;
    public $research_plan_id;

    public function render()
    {
        $typeAppraisal = EApproval::getListType();
        $statusAppraisal = EApproval::getListStatus();

        if ($this->type == EApproval::TYPE_IDEAL) {
            $this->getDataIdeal();
        }
        if ($this->type == EApproval::TYPE_TOPIC) {
            $this->getDataTopic();
        }
        if ($this->type == EApproval::TYPE_COST) {
            $this->getDataTopicFee();
        }
        if ($this->type == EApproval::TYPE_PLAN) {
            $this->getDataResearchPlan();
        }

        return view('livewire.admin.approval.approval-create', ['typeAppraisal' => $typeAppraisal, 'statusAppraisal' => $statusAppraisal]);
    }

    public function store()
    {
        $this->validation();

        if ($this->type == EApproval::TYPE_IDEAL && empty($this->ideal_id)) {
            $this->text_ideal_error = __('notification.approval.select_ideal');
            return false;
        }

        $dataInsert = [
            'name' => $this->name,
            'code' => $this->code,
            'type' => $this->type,
            'status' => EApproval::STATUS_NEW,
            'admin_id' => auth()->id(),
        ];

        // Insert data topic
        if ($this->type == EApproval::TYPE_TOPIC) {
            $dataInsert['topic_id'] = $this->topic_id;
        } elseif ($this->type == EApproval::TYPE_COST) {
            $dataInsert['topic_fee_id'] = $this->topic_fee_id;
        } elseif ($this->type == EApproval::TYPE_PLAN) {
            $dataInsert['research_plan_id'] = $this->research_plan_id;
        }

        $approval = Approval::create($dataInsert);
        // Insert data ideal
        if ($this->type == EApproval::TYPE_IDEAL) {
            foreach ($this->ideal_id as $ideaId => $checked) {
                if ($checked) {
                    ApprovalIdealDetail::create([
                        'ideal_id' => $ideaId,
                        'approval_id' => $approval->id,
                    ]);
                }
            }
        }


        session()->flash('success', __("notification.common.success.add"));
        return redirect()->route('admin.approval.index');
    }

    public function getDataTopic()
    {
        $topics = Topic::where('status', '=', ETopicStatus::STATUS_SUBMIT)->get();
        if ($topics->isNotEmpty()) {
            foreach ($topics as $topic) {
                $this->listTopic[$topic->id] = [
                    'code' => $topic->code,
                    'name' => $topic->name,
                ];
            }
        }
    }

    public function getDataIdeal() {
        $ideals = Ideal::with('researchField')->where('status', '=', EIdeaStatus::JUST_CREATED)->get();
        if ($ideals->isNotEmpty()) {
            foreach ($ideals as $ideal) {
                $this->listIdeal[$ideal->id] = [
                    'id' => $ideal->id,
                    'code' => $ideal->code,
                    'name' => $ideal->name,
                    'fee' => $ideal->fee,
                    'research_name' => $ideal->researchField->name ?? '',
                    'fullname' => $ideal->admin->info->fullname ?? '',
                    'created_at' => reFormatDate($ideal->created_at,'d/m/Y') ?? '',
                ];
            }
        }
    }

    public function getDataTopicFee()
    {
        $topicFees = TopicFee::where('status', '=', ETopicFee::STATUS_SUBMIT)->get();
        if ($topicFees->isNotEmpty()) {
            foreach ($topicFees as $topicFee) {
                $this->listTopicFee[$topicFee->id] = [
                    'name' => $topicFee->name,
                ];
            }
        }
    }

    public function getDataResearchPlan()
    {
        $plans = ResearchPlan::where('status', '=', EResearchPlan::STATUS_SUBMIT)->get();
        if ($plans->isNotEmpty()) {
            foreach ($plans as $plan) {
                $this->listPlan[$plan->id] = [
                    'name' => $plan->name,
                ];
            }
        }
    }

    public function validation()
    {
        $typeAppraisal = [EApproval::TYPE_IDEAL, EApproval::TYPE_TOPIC, EApproval::TYPE_COST, EApproval::TYPE_PLAN];

        $rules = [
            'name' => ['required', 'regex:/^[a-zA-Z0-9'.config('common.special_character.VN').'\.\,\-\(\)\/ ]+$/', 'max:100'],
            'type' => 'required|numeric|in:' . implode(',', $typeAppraisal),
        ];
        if ($this->type == EApproval::TYPE_TOPIC) {
            $rules['topic_id'] = 'required|exists:topic,id';
        }

        if ($this->type == EApproval::TYPE_COST) {
            $rules['topic_fee_id'] = 'required|exists:topic_fee,id';
        }

        if ($this->type == EApproval::TYPE_PLAN) {
            $rules['research_plan_id'] = 'required|exists:research_plan,id';
        }

        if ($this->type == EApproval::TYPE_IDEAL) {
            $rules['code'] = ['required', 'regex:/^[a-zA-Z0-9'.config('common.special_character.VN').'\.\,\-\(\)\/ ]+$/', 'max:20'];
        }
        $attribute = [
            'name' => __('data_field_name.approval_topic.name_profile'),
            'status' => __('data_field_name.common_field.status'),
            'type' => __('data_field_name.approval_topic.profile_type'),
            'code' => __('data_field_name.approval_topic.profile_code'),
            'topic_id' => __('data_field_name.topic.code'),
            'topic_fee_id' => __('data_field_name.approval_topic.type_cost_estimate'),
            'research_plan_id' => __('data_field_name.approval_topic.type_research_plan'),
            'ideal.*' => __('data_field_name.approval_topic.type_ideal'),
        ];

        $this->validate($rules, [], $attribute);
    }

    public function changeType()
    {
        $this->resetValidation();
        $this->text_ideal_error = null;
        $this->ideal_id = null;
    }
}
