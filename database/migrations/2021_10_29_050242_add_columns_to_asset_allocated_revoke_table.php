<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAssetAllocatedRevokeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->tinyInteger('transfer_type')->nullable()->comment('Loại điều chuyển');
            $table->foreignId('from_department_id')->nullable()->comment('Từ đơn vị')->constrained('department')->nullOnDelete();
            $table->foreignId('to_department_id')->nullable()->comment('Đến đơn vị')->constrained('department')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->dropColumn('transfer_type', 'from_department_id', 'to_department_id');
        });
    }
}
