<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToClaimFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_form', function (Blueprint $table) {
            $table->bigInteger('estimated_cost')->nullable()->comment('chi phi dự kiến');
            $table->foreignId('department_id')->nullable()->constrained('department')
                ->comment('phòng tiếp nhận phối hợp map vs department');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_form', function (Blueprint $table) {
            $table->dropColumn('estimated_cost');
            $table->dropColumn('department_id');
        });
    }
}
