<div class="row inner-tab">
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.user_code') }}</label>
            <div class="d-flex">
                @if ($shouldEditCode)
                    <div class="w-100">
                        @livewire('component.advanced-select', [
                        'name' => 'user-info',
                        'modelName' => App\Models\UserInf::class,
                        'searchBy' => ['code'],
                        'perPage'=>10 ,
                        'conditions'=>[['user_id','=',null]],
                        'columns' => ['code', 'fullname', 'email', 'phone'],
                        'itemTemplate' => ':code<br><small>:fullname</small>',
                        'selectedIds' => [$user->user_info_id],
                        'disabled' => !$shouldEditCode,
                        'placeholder' => __('data_field_name.system.account.search_user_info')
                        ])
                    </div>
{{--                    ['user_id', 'in',['']]--}}
                @else
                    <input type="text" class="form-control" readonly value="{{ $user->info->code ?? '' }}">
                @endif

                @if ($user->exists)
                    <button class="btn" wire:click="$set('shouldEditCode', {{ !$shouldEditCode }})"><em class="fa fa-pencil"></em></button>
                @endif
            </div>
            @error('userInfoId') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.user_name') }} *</label>
            <input type="text" class="form-control" placeholder="" wire:model.defer="username">
            @error('username') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.phone') }}</label>
            <input type="text" class="form-control" placeholder="" value="{{ $selectedUserInfo['phone'] ?? '' }}" readonly>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.name') }}</label>
            <input type="text" class="form-control" placeholder="" value="{{ $selectedUserInfo['fullname'] ?? ($user->name ?? '') }}" readonly>
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.email') }} *</label>
            <input type="email" class="form-control" placeholder="" value="{{ $selectedUserInfo['email'] ?? $user->email ?? '' }}" readonly>
        </div>
        <div class="form-group">
            <label>{{ __('data_field_name.system.account.user_group') }}</label>
            @livewire('component.advanced-select', [
                'name' => 'user-group',
                'placeholder' => __('data_field_name.system.account.group_unit'),
                'modelName' => App\Models\GroupUser::class,
                'searchBy' => ['name'],
                'columns' => ['name'],
                'selectedIds' => [$user->group_id]
            ])
        </div>
    </div>
    <div class="col-md-12">
        <label>{{ __('data_field_name.system.account.status') }}</label>
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="customSwitch1" value="1" wire:model.defer="status">
            <label class="custom-control-label" for="customSwitch1">
                {{ __('data_field_name.system.account.active') }}
            </label>
        </div>
    </div>
</div>
