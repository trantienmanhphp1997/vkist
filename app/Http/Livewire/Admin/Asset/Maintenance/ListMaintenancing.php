<?php

namespace App\Http\Livewire\Admin\Asset\Maintenance;

use App\Enums\EAssetSituation;
use App\Enums\EAssetStatus;
use App\Exports\AssetMaintainingExport;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use App\Models\AssetMaintenance;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class ListMaintenancing extends BaseLive
{
    public $searchTerm;
    protected $listeners = [
        'loadConfirm' => 'render'
    ];
    public $idConfirm;
    public $assetSituation;
    public $assetStatus;
    public $finished;
    protected $rule = [
        'finished' => 'required',
    ];

    public function mount()
    {
        $this->assetSituation = [
            EAssetSituation::SITUATION_NOT_USE => __('data_field_name.asset.no_use'),
            EAssetSituation::SITUATION_USING => __('data_field_name.asset.using'),
        ];
        $this->assetStatus = EAssetStatus::getList();
    }

    public function render()
    {
        $listMaintain = AssetMaintenance::where('status', \App\Enums\EAssetStatus::REPAIRED)
            ->orWhere('status', \App\Enums\EAssetStatus::MAINTENANCE)
            ->with('asset');

        if ($this->searchTerm) {
            $listMaintain->whereHas('asset', function (Builder $q) {
                $q->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%');

            });
        }
        $detailNotice = [];
        if ($this->idConfirm) {
            $this->emit('loadAssetInfo', $this->idConfirm);
            $detailNotice = AssetMaintenance::findOrFail($this->idConfirm);
        }
        $data = $listMaintain->orderBy('asset_maintenance.id', 'DESC')->paginate($this->perPage);
        return view('livewire.admin.asset.maintenance.list-maintenancing', ['data' => $data,
            'detailNotice' => $detailNotice]);
    }

    public function confirmNotice($idConfirm)
    {
        $this->idConfirm = $idConfirm;
    }

    public function confirm()
    {
        if ($this->idConfirm) {
            $data = AssetMaintenance::findOrFail($this->idConfirm);

            if ($data->status == \App\Enums\EAssetStatus::REPAIRED || $data->status == \App\Enums\EAssetStatus::MAINTENANCE) {
                $this->validate([
                    'finished' => 'required',
                ], [], [
                    'finished' => __('data_field_name.asset.check_finish'),
                ]);
                if ($this->finished == 1) {

                    if ($data->status == \App\Enums\EAssetStatus::REPAIRED || $data->status == \App\Enums\EAssetStatus::MAINTENANCE) {
                        AssetMaintenance::where('id', $this->idConfirm)->update([
                            'status' => \App\Enums\EAssetSituation::SITUATION_USING,
                        ]);
                        Asset::where('id', $data->asset_id)->update([
                            'situation' => \App\Enums\EAssetSituation::SITUATION_USING,
                        ]);
                    }

                }
                $this->emit('closeRule');

                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
                    __('notification.common.success.confirmed')]);
            } else {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" =>
                    __('notification.common.fail.no_confirmed')]);
            }
        }


    }

    public function export()
    {
        $today = date("d_m_Y");
        return \Maatwebsite\Excel\Facades\Excel::download(new AssetMaintainingExport($this->searchTerm), 'list-asset-need-maintenance-' . $today . '.xlsx');
    }
}
