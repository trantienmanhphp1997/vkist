<?php

namespace App\Http\Livewire\Admin\Asset\InventoryPeriod;


use App\Http\Livewire\Base\BaseLive;
use Carbon\Carbon;
use App\Models\InventoryPeriod;
use Illuminate\Support\Facades\Config;
use DB;

class ListPeriod extends BaseLive
{
    public $name;
    protected $listeners =[
        'resetInput',
    ];

    public function render()
    {
        $data = InventoryPeriod::query()->paginate($this->perPage);
        return view('livewire/admin/asset/inventory-period/list-period',compact('data'));
    }

    public function store(){
        $this->validate([
            'name' => 'required|unique:inventory_periods,name',
        ],[],[
            'name' => __('data_field_name.inventory_period.name'),
        ]);
        InventoryPeriod::create([
            'name' => $this->name,
        ]);
        $this->emit('close-modal-create');
    }

    public function resetInput(){
        $this->name='';
        $this->resetValidation();
    }
}
