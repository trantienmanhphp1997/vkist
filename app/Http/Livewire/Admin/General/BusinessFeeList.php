<?php

namespace App\Http\Livewire\Admin\General;

use Livewire\Component;
use App\Models\BusinessFee;
use App\Http\Livewire\Base\BaseLive;
use League\CommonMark\Inline\Element\Strong;
use Illuminate\Http\Request;

class BusinessFeeList extends BaseLive
{
    public $searchTerm;
    public function render()
    {
        $data = BusinessFee::all();
        $searchTerm =$this->searchTerm;
        $query = BusinessFee::query();
        if(!empty($this->searchTerm)){
            $query->where('name','like','%'.$searchTerm.'%');
            $query->orWhere('code','like','%'.$searchTerm.'%');
            $query->orWhere('accounting_code','like','%'.$searchTerm.'%');
        }
        $data = $query->orderBy('id','desc')->paginate(10);
        return view('livewire.admin.general.business-fee',compact('data'));
    }
}



