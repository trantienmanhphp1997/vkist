<div class="col-md-10 col-xl-11 box-detail box-user">
	<div class="row">
		<div class="col-md-12">
				<div  class="breadcrumbs"><a href="{{route('admin.research.ideal.index')}}">{{__('research/ideal.breadcrumbs.research-ideal-management')}}</a> \ <span>{{__('research/ideal.list')}}</span></div> 
		</div>
	</div>
	<div class="row bd-border">
		<div class="col-md-12">
			<h3 class="title">{{__('research/ideal.list')}}</h3>
			<div class="information">
				<div class="inner-tab pt0">
					<div class="row">
						<div class="col-md-12">
							<div class=" search-expertise d-inline-block ">
								<div class="search-expertise inline-block mb-24 w-xs-100 ">
									<input wire:model.debounce.1000ms="searchTerm" type="text" class="size13 form-control w-xs-100" placeholder="{{__('research/ideal.filter.input.placeholder.search')}}">
									<span>
										<img src="/images/Search.svg" alt="search"/>
									</span>
								</div>
								<div wire:ignore class="w-170 inline-block position-relative">
									<select name="proposer" class="size13 form-control" wire:model.lazy="filterProposer">
					                	<option value=''>{{__('research/ideal.filter.input.placeholder.proposer')}}</option>
					                	@foreach($userList as $item)
					                		<option value="{{$item->id}}">{{$item->fullname}}</option>
					                	@endforeach
					                </select>
								</div>
								<div wire:ignore class="w-170 inline-block mb-24 w-xs-100">
									<select wire:model.lazy="filterStatus" class="size13 form-control form-control-lg ">
										<option value=''>
											{{__('research/ideal.filter.select-box.option.status')}}
										</option>
										<option value="0">
											{{__('research/ideal.status.waiting_for_appraisal')}}
										</option>
										<option value="1">
											{{__('research/ideal.status.approved')}}
										</option>
										<option value="2">
											{{__('research/ideal.status.refuse')}}
										</option>
										<option value="3">
											{{__('research/ideal.status.just_created')}}
										</option>
									</select>
								</div>
								<div wire:ignore class="inline-block mb-24 w-xs-100">
									<select wire:model.lazy="research_field" class="size13 form-control form-control-lg ">
										<option value=''>
											{{__('research/ideal.table_column.research_field')}}
										</option>
										@foreach($researchField as $item)
											<option value="{{$item->id}}">
												{{$item->name}}
											</option>
										@endforeach
									</select>
								</div>
							</div>
					
							@if($checkCreatePermission)
								<div class=" float-right inline-block">
									<a href="{{route('admin.research.ideal.create')}}" class="btn btn-viewmore-news mr0 w-xs-100 mb-24">
										<img src="/images/plus2.svg" alt="plus">{{__('common.button.create')}}
									</a>
								</div>
							@endif
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-general">
							<thead>
								<tr class="border-radius">
									<th scope="col" class="border-radius-left"  style="width:10%">
										{{__('research/ideal.table_column.code')}}
									</th>
									<th scope="col" class="text-center">
										{{__('research/ideal.table_column.name')}}
									</th>
									<th scope="col" class="text-center">
										{{__('research/ideal.table_column.proposer')}}
									</th>
									<th scope="col" class="text-center">
										{{__('research/ideal.table_column.status')}}
									</th>
									<th scope="col" class="text-center">
										{{__('research/ideal.table_column.research_field')}}
									</th>
									<th scope="col" class="text-center uppercase">
										{{__('research/ideal.table_column.created_at')}}
									</th>
									<th scope="col" class="text-center uppercaser">
										{{__('research/ideal.table_column.updated_at')}}
									</th>
									<th scope="col" class="border-radius-right w-140">
										{{__('research/ideal.table_column.action')}}
									</th>
								</tr>
							</thead>
							<div wire:loading class="loader"></div>
							<tbody>
								@foreach($data as $row)
									<tr>
										<td class="code">
											<a href="{{route('admin.research.ideal.detail', ['id' => $row->id])}}" class="text-decoration-none">
												{!! boldTextSearch($row->code, $searchTerm) !!}
											</a>
										</td>
										<td>{!! boldTextSearch($row->name, $searchTerm) !!}</td>
										<td class="text-center">{!! boldTextSearch($row->proposer, $filterProposer) !!}</td>
										<td class="text-center">
											<span class="status">{{$row->status == 0 ? __('research/ideal.status.waiting_for_appraisal') : ($row->status == 1 ? __('research/ideal.status.approved') : ($row->status == 2?__('research/ideal.status.refuse'):__('research/ideal.status.just_created')))}}
											</span>
										</td>
										<td class="text-center">
												{!! $row->researchField->name ?? '' !!}
										</td>
										<td class="text-center">
												{!! reFormatDate($row->created_at) !!}
										</td>
										<td class="text-center">
												{!! reFormatDate($row->updated_at) !!}
										</td>
										<td>
											@if($row->status == 3)
												@php $id = $row->id; @endphp
												@include('layouts.partials.button._edit')
												@include('livewire.common.buttons._delete')
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@if(count($data) > 0)
						{{$data->links()}}
					@else
						<div class="title-approve text-center">
							<span>{{__('common.message.empty_search')}}</span>
						</div>
					@endif
					@include('livewire.common.modal._modalDelete')
				</div>
			</div>
		</div>
	</div>
</div>
