@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.system.audit.title') }}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    @livewire('admin.system.audit.list-data')
@endsection
