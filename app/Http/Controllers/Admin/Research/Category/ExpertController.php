<?php

namespace App\Http\Controllers\Admin\Research\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Expert;
use DB;

class ExpertController extends Controller{

    public function index(){
        return view('admin.research.category.expert.index');
    }

}
