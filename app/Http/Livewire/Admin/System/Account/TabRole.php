<?php

namespace App\Http\Livewire\Admin\System\Account;

use App\Http\Livewire\Base\BaseLive;
use App\Models\User;
use Livewire\Component;

class TabRole extends BaseLive
{

    public User $user;

    public array $readyRoleList = [];

    protected $listeners = [
        'changed-role-event' => 'addReadyRole',
        'validate-data' => 'validateData'
    ];

    public function mount(User $user)
    {
        $this->user = $user;
    }

    public function render()
    {
        $roleList = $this->user->roles()->paginate($this->perPage);
        $existRoleIds = [];
        array_push($existRoleIds, ...array_keys($this->readyRoleList), ...$roleList->pluck('id')->all());

        return view('livewire.admin.system.account.tab-role', [
            'roleList' =>  $roleList,
            'existRoleIds' => $existRoleIds
        ]);
    }

    public function addReadyRole($data)
    {
        $this->readyRoleList[$data['id']] = $data;
    }

    public function removeReadyRole($roleId)
    {
        if (isset($this->readyRoleList[$roleId])) {
            unset($this->readyRoleList[$roleId]);
        }
    }

    public function validateData()
    {
        $this->emitUp('valid-data-event', 'tab-role', $this->readyRoleList);
    }
}
