<?php

namespace Database\Factories;

use App\Models\MasterData;
use App\Models\ResearchCategory;
use App\Models\Topic;
use App\Models\UserInf;
use Illuminate\Database\Eloquent\Factories\Factory;

class TopicFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Topic::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'status' => 1,
            'source' => 0,
            'code' => $this->faker->numberBetween(1000, 99999),
            'major_id' => MasterData::where('type', 12)->get()->random()->id,
            'name' => 'DT' . $this->faker->hexColor,
            'necessary' => $this->faker->sentence(30),
            'overview' => $this->faker->sentence(30),
            'target' => $this->faker->sentence(30),
            // 'level_id' => MasterData::where('type', 11)->get()->random()->id,
            'research_category_id' => ResearchCategory::all()->random()->id,
            'leader_id' => UserInf::all()->random()->id,
            'admin_id' => 1,
            'type' => 0,
            'space_scope' => $this->faker->address,
            'expected_fee' => $this->faker->numberBetween(1000, 2000),
            'start_date' => $this->faker->dateTimeBetween('2020/01/01', '2020/12/30'),
            'end_date' => $this->faker->date('2021/01/01', '2021/12/30'),
            'submit_date' => $this->faker->date('2020/01/01', '2020/12/30')
        ];
    }
}
