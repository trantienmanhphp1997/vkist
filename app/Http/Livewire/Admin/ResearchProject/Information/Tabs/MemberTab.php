<?php

namespace App\Http\Livewire\Admin\ResearchProject\Information\Tabs;

use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchProject;
use App\Models\Topic;
use App\Models\TopicHasUserInfo;
use Illuminate\Support\Facades\Config;


class MemberTab extends BaseLive
{
    public $topicId ;
    protected $listeners = [
        'set-topic-id' => 'setUpTopicId',
    ];

    public function setUpTopicId($id){
        $this->topicId = $id ;
    }
    public function mount($id = 0){
        if ($id){
            $project = ResearchProject::findOrFail($id);
            $contract_code = $project->contract_code;
            $topic = Topic::query()->where(['contract_code'=>$contract_code])->first();
            $this->topicId = $topic->id;
        }
    }
    public function render()
    {
        $members = [];
        if ($this->topicId){
            $query = TopicHasUserInfo::query()->with(['role']);
            $query->where(['topic_id'=>$this->topicId]);
            $members = $query->paginate($this->perPage);
        }

        return view('livewire.admin.research-project.information.tabs.member-tab')->with('members', $members);
    }
}
