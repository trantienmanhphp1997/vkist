<div class="inner-tab row">
    <div class="col-sm-12 form-group">
        <input type="checkbox" id="a-1" value="1" wire:model.defer="topic.achievement_paper_check">
        <label for="a-1">{{ __('data_field_name.topic.achievement_paper_check') }}</label>
    </div>

    <div class="col-sm-12 form-group">
        <label for="">{{ __('data_field_name.topic.expected_result_content') }} <span class="text-danger">(*)</span></label>
        <textarea class="form-control" wire:model.defer="topic.achievement_note"></textarea>
        @error('topic.achievement_note')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="col-sm-6 form-group">
        <input type="checkbox" id="a-3" value="1" wire:model.defer="topic.achievement_review_check">
        <label for="a-3">{{ __('data_field_name.topic.achievement_review_check') }}</label>
    </div>

    <div class="col-sm-6 form-group">
        <input type="checkbox" id="a-4" value="1" wire:model.defer="topic.achievement_announcement_check">
        <label for="a-4">{{ __('data_field_name.topic.achievement_announcement_check') }}</label>
    </div>

    <div class="col-sm-6 form-group">
        <input type="checkbox" id="a-5" value="1" wire:model.defer="topic.achievement_conference_check">
        <label for="a-5">{{ __('data_field_name.topic.achievement_conference_check') }}</label>
    </div>

    <div class="col-sm-6 form-group">
        <input type="checkbox" id="a-6" value="1" wire:model.defer="topic.achievement_work_check">
        <label for="a-6">{{ __('data_field_name.topic.achievement_work_check') }}</label>
    </div>

    <div class="col-sm-6 form-group">
        <input type="checkbox" id="a-7" value="1" wire:model.defer="topic.achievement_textbook_check">
        <label for="a-7">{{ __('data_field_name.topic.achievement_textbook_check') }}</label>
    </div>
</div>
