<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('data_field_name.budget.list')}} </a></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.budget.list')}}</h3>
                </div>
                <div class="col-md-6">
                    @if($checkCreatePermission)
                        @if ($value_date)
                            @if ((in_array($now, $value_date))||$date_create == '')
                                <div class="form-group float-right">
                                    <a href="#" data-toggle="modal" data-target="#category_budget">
                                        <button class="btn btn-viewmore-news mr0 "><img src="/images/plus2.svg" alt="plus">
                                            {{__('data_field_name.budget.add_')}}
                                        </button>
                                    </a>
                                </div>
                            @else
                                <div class="form-group float-right margin-bot">
                                    <button class="btn btn-viewmore-news mr0 " disabled><img src="/images/plus2.svg" alt="plus">
                                        {{__('data_field_name.budget.add_')}}
                                    </button>
                                    <p class="red-12">{{__('data_field_name.budget.not_come')}}</p>
                                </div>
                            @endif
                        @else
                            @if ($date_create == '')
                                <div class="form-group float-right">
                                    <a href="#" data-toggle="modal" data-target="#category_budget">
                                        <button class="btn btn-viewmore-news mr0 "><img src="/images/plus2.svg" alt="plus">
                                            {{__('data_field_name.budget.add_')}}
                                        </button>
                                    </a>
                                </div>
                            @else
                                <div class="form-group float-right margin-bot">
                                    <button class="btn btn-viewmore-news mr0 " disabled><img src="/images/plus2.svg" alt="plus">
                                        {{__('data_field_name.budget.add_')}}
                                    </button>
                                    <p class="red-12">{{__('data_field_name.budget.not_come')}}</p>
                                </div>
                            @endif
                        @endif
                    @endif
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block">
                                    <input type="text" class="form-control size13"
                                           placeholder="{{__('data_field_name.budget.search')}}"
                                           wire:model.debounce.1000ms="searchTerm" autocomplete="off">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                                <div class="w-170 inline-block">
                                    <select class="form-control form-control-lg size13" wire:model.lazy="searchStatus">
                                        <option value="">{{__('data_field_name.budget.status')}}</option>
                                        @foreach($budgetStatus as $key => $val)
                                            <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="w-170 inline-block">
                                    <select class="form-control form-control-lg size13" wire:model.lazy="searchProject">
                                        <option value="">{{__('data_field_name.budget.research_project')}}</option>
                                        @foreach($researchProject as $rs)
                                            <option value="{{$rs->code}}">{{$rs->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table working-plan" id="dataTable">
                            <thead>
                            <tr class="border-radius">
                                <th scope="col">{{__('data_field_name.budget.code')}}</th>
                                <th scope="col">{{__('data_field_name.budget.name')}}</th>
                                <th scope="col">{{__('data_field_name.budget.total')}}</th>
                                <th scope="col">{{__('data_field_name.budget.content')}}</th>
                                <th scope="col">{{__('data_field_name.budget.year')}}</th>
                                <th scope="col">{{__('data_field_name.budget.status')}}</th>
                                <th scope="col">{{__('data_field_name.budget.plan_budget')}}</th>
                                <th scope="col">{{__('data_field_name.budget.department')}}</th>
                                <th scope="col">{{__('data_field_name.budget.research_project')}}</th>
                                <th scope="col">{{__('data_field_name.budget.number_of_updates')}}</th>
                                <th scope="col">{{__('data_field_name.common_field.action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <div wire:loading class="loader"></div>
                            @forelse ($data as $key => $val)
                                <tr>
                                        @php $check = $val->status==\App\Enums\EBudgetStatus::DONE||$val->status==\App\Enums\EBudgetStatus::WAIT @endphp
                                    <td class="text-center">

                                        <a href="{{route('admin.budget.show',$val->id)}}">
                                            {!! boldTextSearch($val->code,$searchTerm) !!}
                                        </a>

                                    </td>
                                    <td class="text-center">{!! boldTextSearch($val->name,$searchTerm) !!}</td>
                                    <td class="text-center"> {{numberFormat($val->total_budget)}}</td>
                                    <td class="text-center">{{$val->content}}</td>
                                    <td class="text-center">{{$val->year_created}}</td>
                                    <td class="text-center">
                                        @if ($val->status==\App\Enums\EBudgetStatus::WAIT)
                                            <button
                                                class="btn  mr0 btn-plan bg-blue high"> {{__('data_field_name.budget.status_wait')}}</button>
                                        @elseif ($val->status==\App\Enums\EBudgetStatus::DONE)
                                            <button
                                                class="btn  mr0 btn-plan bg-blue medium "> {{__('data_field_name.budget.status_done')}}</button>
                                        @elseif($val->status==\App\Enums\EBudgetStatus::DENY)
                                            <button
                                                class="btn mr0 btn-plan emergency"> {{__('data_field_name.budget.status_deny')}}</button>
                                        @elseif($val->status==\App\Enums\EBudgetStatus::NEW)
                                            <button
                                                class="btn mr0 btn-plan bg-blue "> {{__('data_field_name.budget.status_new')}}</button>
                                        @elseif($val->status==\App\Enums\EBudgetStatus::WAIT_CHANGE)
                                            <button
                                                class="btn mr0 btn-plan bg-blue high "> {{__('data_field_name.budget.status_wait_change')}}</button>
                                        @else
                                            <button class="btn  mr0 btn-plan emergency"></button>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <button type="button" data-toggle="modal" data-target="#fileBudget"
                                                wire:click="listFile({{ $val->id }})"
                                                class="btn btn-primary btn-sm par6">
                                            <img src="/images/file3.svg" alt="file"></button>
                                    </td>
                                    <td class="text-center">{{$val->department->name??''}}</td>
                                    <td class="text-center">{{$val->researchProject->name??''}}</td>
                                    <td class="text-center">
                                        <a href="{{route('admin.budget.history-budget.index')}}">
                                            {{$val->version}}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        @if ($val->status == \App\Enums\EBudgetStatus::NEW || $val->status == \App\Enums\EBudgetStatus::DENY
                                                )
                                            @if ($checkEditPermission)
                                                <a title="{{__('common.button.edit')}}"
                                                   href="{{route('admin.budget.edit-budget', $val->id)}}">
                                                    <img src="/images/edit.png" alt="edit">
                                                </a>
                                            @endif
                                            @if ($checkDestroyPermission)
                                                <button title="{{__('common.button.delete')}}" type="button"
                                                        data-toggle="modal" data-target="#deleteModal"
                                                        title="{{__('common.button.delete')}}"
                                                        wire:click="deleteId({{$val->id}})"
                                                        class="btn-sm border-0 bg-transparent"><img
                                                        src="/images/trash.svg" alt="trash"></button>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12">{{__('common.message.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    {{$data->links()}}
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="DeleteBudget" tabindex="-1"
         aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('common.confirm_message.confirm_title')}}</h5>
                </div>
                <div class="modal-body">
                    {{__('common.confirm_message.are_you_sure_delete')}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                            data-dismiss="modal">{{__('common.button.back')}}</button>
                    <button type="button" class="btn btn-danger"
                            data-dismiss="modal" wire:click.prevent="delete()">{{__('common.button.delete')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="fileBudget" tabindex="-1"
         aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('data_field_name.budget.list_file')}}</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @forelse ($file as $key => $val)
                            <div class="form-group col-md-12" wire:click="download({{$val->id}})">
                                {{--                                <input type="radio" class=""  value="{{$val->id}}" wire:model.lazy="getIdFile" id="selectFile">--}}
                                <div class="attached-files">
                                    <img src="/images/File.svg" alt="file">
                                    <div class="content-file">
                                        <p>{{ $val-> file_name }}</p>
                                        <p class="kb">{{ $val-> size_file }}</p>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>{{__('common.message.empty_search')}}</p>
                        @endforelse
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"
                            data-dismiss="modal">{{__('common.button.back')}}</button>
                    {{--                    <button type="button" wire:click.prevent="download()" class="btn btn-danger"--}}
                    {{--                             title="Download"  id="button_down" >Tải xuống</button>--}}
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common.modal._modalDelete')
    @livewire('admin.budget.add.category-budget')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".selectall").change(function () {
                $(".checkbox1").prop('checked', $(this).prop("checked"));
            });

            $("#deleteBtn").on('click', (e) => {
                var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.selectall');
                var count = $checkedBoxes.length;
                // alert(count)
                if (count >= 1) {
                    var checkbox = document.getElementsByName('check');
                    var checked = [];
                    for (var i = 0; i < checkbox.length; i++) {
                        if (checkbox[i].checked === true) {
                            checked.push(checkbox[i].value);
                        }
                    }
                    window.livewire.emit('deleteBudget', checked);
                    // var $updateStateConfirmModal = $('#DeleteBudget');
                    // $updateStateConfirmModal.modal('show');
                    // jQuery.noConflict();
                    $('#DeleteBudget').modal('show');
                } else {
                    toastr.warning('{{ __('data_field_name.budget.no_choose') }}');
                }
            });
        });

    </script>
</div>

