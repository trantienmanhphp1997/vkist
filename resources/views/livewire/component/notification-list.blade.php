<div>
    <div class="bell"><img src="{{ asset('/images/Notifications.svg') }}" alt="notifications"></div>
    <div class="d-block">
        <h3 class="text-1 size24 font-weight-bold d-inline-block">
            {{ __('data_field_name.notification.notification_list') }}
        </h3>
        <a href="{{ route('admin.config.notification.public') }}" class="float-right" data-toggle="tooltip" data-placement="right" title="{{ __('menu_management.menu_name.notification_configuration') }}" data-original-title="{{ __('menu_management.menu_name.notification_configuration') }}">
            <img src="/images/settings-switch.svg" alt="settings-switch"/></a>
    </div>
    <hr>
    <div class="overflow-scroll-y h-400">
        @foreach ($notifications as $notification)
            <div class="media py-3 border-bottom {{ $notification->read_at ? '' : 'bg-light' }}">
                <img src="{{ asset('images/avatar.jpg') }}" alt="avatar" style="width: 50px; height: 50px; border-radius: 50%;">
                <div class="media-body pl-16">
                    <a href="#" wire:click="readNotification({{ $notification->id }})" class="d-block mb-0 text-1">
                        {!! $notification->body() !!}
                    </a>
                    <span class="size12 text-3">{{ $notification->created_at->diffForHumans() }}</span>
                </div>
                <div>
                    <button class="btn text-muted" wire:click="clearNotification({{ $notification->id }})"><em class="fa fa-times"></em></button>
                    @if ($notification->enabled)
                        <button class="btn text-muted" wire:click="toggleNotification('{{ $notification->name ?? '' }}', 'disable')"><em class="fa fa-bell-slash"></em></button>
                    @else
                        <button class="btn text-muted" wire:click="toggleNotification('{{ $notification->name ?? '' }}', 'enable')"><em class="fa fa-bell"></em></button>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <hr class="mt-0">
    <div>
        <button class="btn border-8 font-weight-bold bg-5 mr-4 text-9 size12 py-8 px-16" wire:click="readAllNotification">{{ __('common.button.view_all') }}</button>
        <button class="btn border-8 font-weight-bold bg-6 text-9 size12 py-8 px-16" wire:click="clearAllNotification">{{ __('common.button.clear_all') }}</button>

        <button class="btn border-8 font-weight-bold bg-warning text-9 size12 py-8 px-16 float-right" data-toggle="modal" data-target="#deadline-reminder-modal">{{ __('data_field_name.notification.file_deadline') }}</button>
    </div>

    <script>
        document.addEventListener('livewire:load', function() {

            window.dispatchEvent(new CustomEvent('count-unread-notification-event', {
                detail: {
                    count: '{{ $count }}'
                }
            }));

            Echo.private('App.Models.User.{{ auth()->id() }}')
                .notification((notification) => {
                    toastr.success(notification.body);
                    Livewire.emit('new-notification-event');
                });
        });
    </script>
</div>
