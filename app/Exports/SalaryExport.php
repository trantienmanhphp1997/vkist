<?php

namespace App\Exports;

use App\Models\Department;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class SalaryExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    private $data;
    public $month;
    public $name;
    public $year ;
    public function __construct($data,$name , $month , $year)
    {
        $this->name = $name ;
        $this->month = $month;
        $this->year = $year ;
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            __('data_field_name.import-salary.user-code'),
            __('data_field_name.import-salary.user-name'),
            __('data_field_name.import-salary.coefficients'),
            __('data_field_name.import-salary.total_salary'),
            __('data_field_name.import-salary.bank_account'),
            __('data_field_name.import-salary.bank_name'),
        ];
    }

    public function map($salary): array
    {
        return [
            $salary['userCode'],
            $salary['fullname'],
            $salary['coefficients'],
            $salary['totalSalary'],
            $salary['bankAccount'],
            $salary['bankName'],
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color' => ['argb' => '#FFFFFF'],
                    'background' => [
                        'color' => '#5B9BD5',
                    ]],
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' => true, 'horizontal' => 'center', 'vertical' => 'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F'
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('A1:E1');
            $event->sheet->setCellValue('A1', __('data_field_name.import-salary.detail1', [
                'name' => $this->name,
                'month' => $this->month,
                'year' => $this->year,
            ]));
            $event->sheet->getStyle('A1:E1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:F3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical' => 'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }

    public function title(): string
    {
        return __('data_field_name.import-salary.detail1', [
            'name' => $this->name,
            'month' => $this->month,
            'year' => $this->year,
        ]);
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
