<?php


namespace App\Enums;


class EAssetFrequency {
    const ONE = 1;
    const PERIODIC = 2;
}

