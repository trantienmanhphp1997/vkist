<?php

namespace App\Http\Livewire\Admin\Finance;

use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;

class FinanceIndex extends BaseLive
{

    public function render()
    {
        return view('livewire.admin.finance.finance-index');
    }

    public function openMisa()
    {
        $pathToFile = 'C:\MISA JSC\MISA Mimosa.NET 2020\Bin\MISA Mimosa.NET 2020.exe';

        exec($pathToFile);
    }
}
