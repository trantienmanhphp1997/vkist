<?php

namespace App\Http\Controllers\Admin\Asset;

use App\Component\DepartmentRecursive;
use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\AssetAllocatedRevoke;
use App\Models\AssetCategory;
use App\Models\AssetMaintenance;
use App\Models\AssetRevenue;
use App\Models\Department;
use Illuminate\Http\Request;


class AssetController extends Controller
{
    public function index()
    {
        return view('admin.asset.detail.index');
    }

    public function create()
    {

        return view('admin.asset.create');
    }

    public function dashboard()
    {
        // anh ta add
        // lấy dữ liệu tất cả loại tài sản
        $assetCategorys = AssetCategory::query()->with(['asset'])->get();
        $countTotalAsset = 0 ;
        foreach ($assetCategorys as $assetCategory){
            $countTotalAsset += count($assetCategory->asset);
        }
        foreach ($assetCategorys as $assetCategory){
            if (count($assetCategory->asset) > 0){
                $assetCategory->countPercent =   round((100 / $countTotalAsset)*count($assetCategory->asset));
            }else{
                $assetCategory->countPercent = 0;
            }
        }


        //anh ta end
        // anh ta add 21/9/2021
        // lấy dữ liệu từ bảng asset revenue theo từng năm
        $assetRevenue = AssetRevenue::query()->where(['report_year'=>date('Y')])->get();

        $dataAssetRevenue = [];
        foreach ($assetRevenue as $asset) {
            $dataAssetRevenue[] = (int)$asset->quantity;
        }

        //anh ta end
        $data = Asset::query()->get();
        $maintain = AssetMaintenance::where('status', \App\Enums\EAssetStatus::MAINTENANCE)
            ->orWhere('status', \App\Enums\EAssetStatus::REPAIRED)
            ->get();

        $lost = AssetAllocatedRevoke::where('status', \App\Enums\EAssetStatus::LOSTED)
            ->orWhere('status', \App\Enums\EAssetStatus::LIQUIDATION)
            ->orWhere('status', \App\Enums\EAssetStatus::CANCELLED)
            ->get();

        $report_maintain = AssetMaintenance::where('status', \App\Enums\EAssetStatus::REPORT_REPAIR)
            ->orWhere('status', \App\Enums\EAssetStatus::REPORT_MAINTENANCE)
            ->get();

        $report_lost = AssetAllocatedRevoke::where('status', \App\Enums\EAssetStatus::REPORT_LOST)
            ->orWhere('status', \App\Enums\EAssetStatus::REPORT_LIQUIDATION)
            ->orWhere('status', \App\Enums\EAssetStatus::REPORT_CANCEL)
            ->get();
        $total = 0;
        $used_value = 0;
        $mount_used = 0;
        $quantity = 0;
        $mount_maintain = 0;
        $value_maintain = 0;
        $value_lost = 0;
        $mount_lost = 0;
        $mount_report_maintain = 0;
        $value_report_maintain = 0;
        $mount_report_lost = 0;
        $value_report_lost = 0;
        foreach ($data as $val) {
            $total += $val->quantity * $val->asset_value;
            $mount_used += $val->amount_used;
            $quantity += $val->quantity;
            $used_value += $val->amount_used * $val->asset_value;
        }
        foreach ($maintain as $value) {
            $mount_maintain += $value->implementation_quantity;
            $value_maintain += $value->implementation_quantity * $value->asset->asset_value;
        }

        foreach ($lost as $item) {
            $mount_lost += $item->implementation_quantity;
            $value_lost += $item->implementation_quantity * $item->asset->asset_value;
        }

        foreach ($report_maintain as $value) {
            $mount_report_maintain += $value->implementation_quantity;
            $value_report_maintain += $value->implementation_quantity * $value->asset->asset_value;
        }

        foreach ($report_lost as $item) {
            $mount_report_lost += $item->implementation_quantity;
            $value_report_lost += $item->implementation_quantity * $item->asset->asset_value;
        }

        $pieChartData = AssetCategory::query()->with('asset');

        //Các loại tài sản thuộc tài sản cố định
        $asset_cate_fixed=AssetCategory::where('type_manage',1)->with('asset')->get();
        $countTotal=0;
        $depre_previous=0;
        $left=0;
        $countTotalValue=[];
        $nameCate=[];
        $depreciation=[];
        $leftValue=[];
        foreach ($asset_cate_fixed as $value){
            foreach ($value->assetValue as $item){
                $countTotal += ($item->asset_value);
                $depre_previous += $item->depreciation_value_previous;
                $left += $item->asset_value-$item->depreciation_value_previous;
            }
            $countTotalValue[]=$countTotal;
            $nameCate[]=$value->name;
            $depreciation[]=$depre_previous;
            $leftValue[]=$left;
        }
//        dd($depreciation);
        //        hong end

        return view('admin.asset.dashboard', ['data' => $data,
            'assetCategorys'=> $assetCategorys,
            'quantity' => $quantity,
            'total' => $total,
            'used_value' => $used_value,
            'mount_used' => $mount_used,
            'mount_maintain' => $mount_maintain,
            'value_maintain' => $value_maintain,
            'mount_lost' => $mount_lost,
            'value_lost' => $value_lost,
            'mount_report_maintain' => $mount_report_maintain,
            'value_report_maintain' => $value_report_maintain,
            'mount_report_lost' => $mount_report_lost,
            'value_report_lost' => $value_report_lost,
            'pieChartData' => $pieChartData,
            'assetRevenue'=>$assetRevenue,
            'dataAssetRevenue' => $dataAssetRevenue,
            'countTotalValue'=>$countTotalValue,
            'nameCate'=>$nameCate,
            'depreciation'=>$depreciation,
            'leftValue'=>$leftValue
        ]);
    }

    public function show($idAsset)
    {
        $assetDetail=Asset::findOrFail($idAsset);
        return view('admin.asset.detail', ['assetDetail'=>$assetDetail]);
    }

    public function lostCancel()
    {
        return view('admin.asset.lost-cancel-liquidation.list');
    }

    public function maintenance()
    {
        return view('admin.asset.maintenance');
    }
}
