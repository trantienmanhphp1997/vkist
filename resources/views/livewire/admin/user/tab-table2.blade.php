<div>
    <!-- table2 -->
    <div class="row">
        <div class="col-md-6">
            <h3 class="title-personal">{{__('data_field_name.working_process.research_result')}}</h3>
        </div>
        <div class="col-md-6 d-flex flex-row-reverse bd-highlight justify-content-en ">
            @if ($canEdit)
            <div class="p-2 bd-highlight">
                <button type="button" data-toggle="modal" data-target="#exampleModal2"
                        class="btn btn-adduser float-right "><img src="/images/plususer.svg" alt="plus"></button>
            </div>
            @endif
        </div>
    </div>
    <table class="table border-bottom">
        <thead>
        <tr class="border-radius">
            <th scope="col"> {{__('data_field_name.working_process.product_name')}}</th>
            <th scope="col">{{__('data_field_name.working_process.author')}}</th>
            <th scope="col">{{__('data_field_name.working_process.newspaprer_name')}}</th>
            <th scope="col">{{__('data_field_name.working_process.issn')}}</th>
            <th scope="col">{{__('data_field_name.working_process.announced_year')}}</th>
            <th scope="col" class="border-radius-right">{{__('data_field_name.common_field.action')}}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($data2 as $key => $val)
            <tr>
                <td>{{ $val-> product_name }}</td>
                <td>{{ $val-> author }}</td>
                <td>{{ $val-> publishing_company }}</td>
                <td>{{ $val-> issn }}</td>
                <td>{{ $val-> publishing_year }}</td>
                <td>
                    <button type="button" class="btn par6" title="detail"><img src="/images/playlist2.svg" alt="playlist"></button>
                    <button type="button" data-toggle="modal" data-target="#exampleModalEdit2"
                            wire:click="editReTable2({{ $val->id }})" class="btn" title="edit">
                        <img src="/images/pent2.svg" alt=""></button>
                    <button type="button" wire:click="getIdDeleteTable2({{ $val->id }})"
                            class="btn" data-toggle="modal" data-target="#deleteReTable2"><img
                            src="/images/trash.svg" alt="trash"></button>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6">{{__('data_field_name.working_process.empty')}}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <!-- Modal Create -->
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="exampleModal2" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLabel">{{__('data_field_name.working_process.create_research_result')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput1">{{__('data_field_name.working_process.product_name')}}
                                    <span class="text-danger">(*)</span></label>
                                <input type="text" class="form-control"
                                       placeholder="{{__('data_field_name.working_process.input_product_name')}}"
                                       wire:model.lazy="product_name">
                                @error('product_name') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput2">{{__('data_field_name.working_process.author')}}</label>
                                <input type="text" class="form-control" wire:model.lazy="author"
                                       placeholder="{{__('data_field_name.working_process.input_author_name')}}">
                                @error('author') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput2">{{__('data_field_name.working_process.newspaprer_name')}}
                                    <span class="text-danger">(*)</span></label>
                                <input type="text" class="form-control" wire:model.lazy="publishing_company"
                                       placeholder="{{__('data_field_name.working_process.input_newspaprer_name')}}">
                                @error('publishing_company') <span
                                    class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput2">{{__('data_field_name.working_process.issn')}}</label>
                                <input type="text" class="form-control" wire:model.lazy="issn"
                                       placeholder="{{__('data_field_name.working_process.input_issn')}}">
                                @error('issn') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput2">{{__('data_field_name.working_process.announced_year')}}
                                    <span class="text-danger">(*)</span></label>
                                    <input type="text" class="input-date-kendo form-control mb-0 publishing-year" id="publishing-year" placeholder="dd/mm/yyyy" value="{{ $publishing_year }}">
                                @error('publishing_year') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>

                        </form>
                    </div>
                    <div class="text-center">
                        <button type="button" id="close-modal-create-researchTable2" class="btn btn-main bg-4 text-3 size16 px-48 py-14" data-dismiss="modal" wire:click.prevent="resetInputFields2()">{{__('common.button.close')}}</button>
                        <button type="button" wire:click.prevent="store()" id="btn-store-2p" class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn">{{__('common.button.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- end create  -->
    <!-- modal edit -->
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="exampleModalEdit2" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLabel">{{__('data_field_name.working_process.edit_research_result')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput1">{{__('data_field_name.working_process.product_name')}}
                                    <span class="text-danger">(*)</span></label>
                                <input type="text" class="form-control"
                                       placeholder="{{__('data_field_name.working_process.input_product_name')}}"
                                       wire:model.lazy="product_name">
                                @error('product_name') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput2">{{__('data_field_name.working_process.author')}}</label>
                                <input type="text" class="form-control" wire:model.lazy="author"
                                       placeholder="{{__('data_field_name.working_process.input_author_name')}}">
                                @error('author') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput2">{{__('data_field_name.working_process.newspaprer_name')}}
                                    <span class="text-danger">(*)</span></label>
                                <input type="text" class="form-control" wire:model.lazy="publishing_company"
                                       placeholder="{{__('data_field_name.working_process.input_newspaprer_name')}}">
                                @error('publishing_company') <span
                                    class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput2">{{__('data_field_name.working_process.issn')}}</label>
                                <input type="text" class="form-control" wire:model.lazy="issn"
                                       placeholder="{{__('data_field_name.working_process.input_issn')}}">
                                @error('issn') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label
                                    for="exampleFormControlInput2">{{__('data_field_name.working_process.announced_year')}}
                                    <span class="text-danger">(*)</span></label>
                                    <input type="text" class="input-date-kendo form-control mb-0 publishing-year" id="publishing-year" placeholder="dd/mm/yyyy" value="{{ $publishing_year }}">
                                @error('publishing_year') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>

                        </form>
                    </div>
                    <div class="text-center">
                        <button type="button" id="close-modal-edit-researchTable2" class="btn btn-main bg-4 text-3 size16 px-48 py-14" data-dismiss="modal" wire:click.prevent="resetInputFields2()">{{__('common.button.close')}}</button>
                        <button type="button" wire:click.prevent="store()" id="btn-store-2p" class="btn btn-main bg-primary text-9 size16 px-48 py-14 ml-3 submit-btn">{{__('common.button.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- end modal edit -->
    <!-- Modal Delete -->
    <div wire:ignore.self class="modal fade" id="deleteReTable2" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong><h5 class="modal-title"
                                id="exampleModalLabel">{{__('notification.member.warning.warning')}}</h5></strong>
                </div>
                <div class="modal-body">
                    {{__('notification.member.warning.warning-1')}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"
                            data-dismiss="modal">{{__('common.button.back')}}</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"
                            wire:click="deleteReTable2()">{{__('common.button.delete')}}</button>
                </div>
            </div>
        </div>
    </div>
    @livewire('component.files', ['model_name'=>$model_name, 'model_id'=>$model_id, 'type'=>$type, 'folder'=>$folder])

    <script>
        window.addEventListener('render-research-result-event', function() {
            initDatePicker('.publishing-year');
        });
    </script>
</div>



