<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AssetRevenueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('asset_revenue')->insert([
            [
                'report_year'=> 2021,
                'report_month'=> 1,
                'quantity'=> 120,
                'total_money'=> 1234,
            ],
            [
                'report_year'=> 2021,
                'report_month'=> 2,
                'quantity'=> 130,
                'total_money'=> 1234,
            ],[
                'report_year'=> 2021,
                'report_month'=> 3,
                'quantity'=> 160,
                'total_money'=> 1234,
            ],[
                'report_year'=> 2021,
                'report_month'=> 4,
                'quantity'=> 130,
                'total_money'=> 1234,
            ],[
                'report_year'=> 2021,
                'report_month'=> 5,
                'quantity'=> 190,
                'total_money'=> 1234,
            ],[
                'report_year'=> 2021,
                'report_month'=> 6,
                'quantity'=> 110,
                'total_money'=> 1234,
            ],[
                'report_year'=> 2021,
                'report_month'=> 7,
                'quantity'=> 60,
                'total_money'=> 1234,
            ],[
                'report_year'=> 2021,
                'report_month'=> 8,
                'quantity'=> 120,
                'total_money'=> 1234,
            ],
        ]);
    }
}
