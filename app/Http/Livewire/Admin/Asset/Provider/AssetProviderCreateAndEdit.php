<?php

namespace App\Http\Livewire\Admin\Asset\Provider;

use Livewire\Component;
use App\Models\AssetProvider;
use App\Http\Livewire\Base\BaseLive;
use App\Models\MasterData;
use App\Enums\EMasterDataType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;


class AssetProviderCreateAndEdit extends BaseLive
{

    public $isEdit = false;
    //asset_provider 
    public $asset_provider;

    public $list_bank_info;

    public $name;
    public $code;
    public $address;
    public $tax_code;
    public $phone_number;
    public $website;
    public $bank_account_number;
    public $bank_name_id;
    public $email;
    public $note;

    public function mount() {
        if($this->asset_provider) {
            $this->isEdit = true;

            $this->name = $this->asset_provider->name;
            $this->code = $this->asset_provider->code;
            $this->address = $this->asset_provider->address;
            $this->tax_code = $this->asset_provider->tax_code;
            $this->phone_number = $this->asset_provider->phone_number;
            $this->website = $this->asset_provider->website;
            $this->bank_account_number = $this->asset_provider->bank_account_number;
            $this->bank_name_id = $this->asset_provider->bank_name_id;
            $this->email = $this->asset_provider->email;
            $this->note = $this->asset_provider->note;
        }
        $this->list_bank_info = MasterData::where('type', EMasterDataType::BANK_NAME)->get();
    }
    
    public function render()
    {
        return view('livewire.admin.asset.provider.create-and-edit');
    }

    public function store() {
        $this->bank_name_id = is_null($this->bank_name_id) ? intval($this->list_bank_info[0]->id) : intval($this->bank_name_id);
        $this->validate([
            'name' => 'required|max:255',
            'code' => 'required',
            'address' => 'required',
            'tax_code' => 'required',
            'phone_number' => 'required',
            'website' => 'required',
            'bank_account_number' => 'required',
            'bank_name_id' => 'required',
            'email' => 'required|email',
        ], [], [
            'name' => __('asset/provider.label.name'),
            'code' => __('asset/provider.label.code'),
            'address' => __('asset/provider.label.address'),
            'tax_code' => __('asset/provider.label.tax_code'),
            'phone_number' => __('asset/provider.label.phone_number'),
            'website' => __('asset/provider.label.website'),
            'bank_account_number' => __('asset/provider.label.bank_account_number'),
            'bank_name_id' => __('asset/provider.label.bank_name_id'),
            'email' => __('asset/provider.label.email'),
        ]);
        if(is_null($this->asset_provider)) {
            $this->asset_provider = new AssetProvider();
        }
        $this->asset_provider->name= $this->name;
        $this->asset_provider->address= $this->address;
        $this->asset_provider->tax_code= $this->tax_code;
        $this->asset_provider->code= $this->code;
        $this->asset_provider->phone_number= $this->phone_number;
        $this->asset_provider->website= $this->website;
        $this->asset_provider->bank_account_number= $this->bank_account_number;
        $this->asset_provider->bank_name_id= $this->bank_name_id;
        $this->asset_provider->email= $this->email;
        $this->asset_provider->note= $this->note;
        $this->asset_provider->save();
        if($this->isEdit) {
            session()->flash('success', __('asset/provider.notification.edit-success'));
        } else {
            session()->flash('success', __('asset/provider.notification.create-success'));
        }

        return redirect()->route('admin.asset.provider.index');
    }
}
