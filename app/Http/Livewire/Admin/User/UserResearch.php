<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use App\Models\UserInf;
use App\Models\UserResearchPaper;
use App\Models\UserResearchs;
use App\Models\MasterData;

class UserResearch extends Component

{
    public $user_info_id;
    public $research;
    public $model_name;


    public function render()
    {
        return view('livewire.admin.user.user-research');
    }
}
