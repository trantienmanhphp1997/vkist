<?php

namespace App\Http\Livewire\Admin\Asset\AssetNotice;

use App\Models\Asset;
use App\Models\AssetAllocatedRevoke;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class NoticeCancel extends Component
{
    public $assetDetail;
    public $implementation_date;
    public $implementation_reason;
    public $number_report;
    public $implementation_quantity;
    public $errorQuantity;
    public $implementationDate;
    protected $listeners = [
        'set-implementation_date' => 'setImplementationDate',
    ];
    protected $rule = [
        'implementation_date' => 'required',
        'number_report' => 'required|max:10',
    ];

    public function render()
    {
        $model_name = AssetAllocatedRevoke::class;
        $type = Config::get('common.type_upload.AssetAllocatedRevoke');
        $folder = app($model_name)->getTable();
        $status = -1;

        $type_manage = '';

        if ($this->assetDetail) {
            $asset_Detail = $this->assetDetail;
            $type_manage = $asset_Detail->category->type_manage;

            if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                $this->implementation_quantity = 1;
            }
        }
        return view('livewire.admin.asset.asset-notice.notice-cancel', ['model_name' => $model_name, 'type' => $type,
            'folder' => $folder, 'status' => $status, 'type_manage' => $type_manage]);
    }

    public function setImplementationDate($data)
    {
        $this->implementationDate = date('Y-m-d', strtotime($data['implementation_date']));
    }

    public function save()
    {
        $asset = $this->assetDetail;
        $type_manage = $asset->category->type_manage;
        $quantity = $asset->amount_used;
        //để lấy default leaveDate là ngày hiện tại
        if (is_null($this->implementationDate)) {
            $this->implementationDate = date('Y-m-d');
        }

        if (!empty($quantity) && $quantity < $this->implementation_quantity) {
            $this->errorQuantity = __('validation.lt.numeric', [
                'attribute' => __('data_field_name.asset.asset_quantity'),
                'value' => __('data_field_name.asset.asset_used_quantity')
            ]);
        }
        $this->validate([
            'number_report' => ['required', 'regex:/^[A-Z0-9' . config('common.special_character.alphabet') . ']+$/', 'max:10'],
            'implementation_quantity' => ['required', 'regex:/^[1-9\.\,]+$/', 'max:3'],
            'implementation_reason' => 'max:255',
        ], [], [
            'number_report' => __('data_field_name.asset.number_report'),
            'implementation_quantity' => __('data_field_name.asset.asset_quantity'),
            'implementation_reason' => __('data_field_name.asset.reason_liquidation'),
        ]);
        if ($quantity >= $this->implementation_quantity) {
            $data = AssetAllocatedRevoke::create([
                'asset_id' => $asset->id,
                'implementation_date' => $this->implementationDate,
                'implementation_reason' => $this->implementation_reason,
                'number_report' => $this->number_report,
                'status' => \App\Enums\EAssetStatus::REPORT_CANCEL,
                'implementation_quantity' => $this->implementation_quantity,
            ]);

            if ($data) {
                \App\Models\File::where('model_name', AssetAllocatedRevoke::class)
                    ->where('type', Config::get('common.type_upload.AssetAllocatedRevoke'))
                    ->where('admin_id', auth()->id())
                    ->where('model_id', null)
                    ->update([
                        'model_id' => $data->id,
                        'status' => 1
                    ]);

                if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                    Asset::where('id', $data->asset_id)->update([
                        'situation' => \App\Enums\EAssetStatus::REPORT_CANCEL
                    ]);
                } else {
                    $asset_quantity = Asset::findOrFail($data->asset_id);
                    if ($asset_quantity->quantity == $this->implementation_quantity) {
                        Asset::where('id', $data->asset_id)->update([
                            'situation' => \App\Enums\EAssetStatus::REPORT_CANCEL
                        ]);
                    }
                }
                $this->emit('loadStatus');
            }
            $this->emit('closeNotice');
            $this->resetInput();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>
                __('notification.asset.success.cancle_success')]);
        }
    }

    public function cancel()
    {
        \App\Models\File::where('model_name', AssetAllocatedRevoke::class)
            ->where('type', Config::get('common.type_upload.AssetAllocatedRevoke'))
            ->where('admin_id', auth()->id())
            ->where('status', -1)
            ->where('model_id', null)
            ->delete();
        $this->resetInput();
    }

    public function resetInput()
    {
        $this->implementation_date = '';
        $this->implementation_reason = '';
        $this->number_report = '';
        $this->implementation_quantity = '';
    }
}
