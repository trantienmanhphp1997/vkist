<?php

namespace App\Http\Controllers\admin\research\Topic;

use App\Enums\ETopicSource;
use App\Enums\ETopicStatus;
use App\Enums\ETopicCheckSendFile;
use App\Enums\ETopicType;
use App\Enums\ETypeUpload;
use App\Http\Controllers\Controller;
use App\Models\Audit;
use App\Models\File;
use App\Models\GroupUserHasTopic;
use App\Models\MasterData;
use App\Models\ResearchCategory;
use App\Models\Topic;
use App\Models\TopicHasUserInfo;
use App\Service\Community;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class TopicController extends Controller
{
    public function index()
    {
        return view('admin.research.topic.index');
    }

    public function create()
    {
        return view('admin.research.topic.create');
    }

    public function edit($id)
    {
        return view('admin.research.topic.edit', ['topicId' => $id]);
    }


    public function detail($id)
    {
        //check permission topic
        $listTopicIdAllowed = Community::listTopicIdAllowed();
        if (!Auth::user()->hasRole('administrator') && !in_array($id,$listTopicIdAllowed)){
                abort(404);
        }

        $data = Topic::query()->findOrFail($id)->load(['level', 'approval']);

        $topicStatus = ETopicStatus::getList();
        $topicSource = ETopicSource::getList();
        $topicCheckSendFile = ETopicCheckSendFile::getList();

        $history = Audit::query()->with('user')->where('auditable_type', Topic::class)
            ->where('auditable_id', $id)
            ->orderBy('id', 'desc')
            ->get()
            ->filter(function($item) {
                return (isset($item->old_values['status']) || isset($item->new_values['status'])) && $item->event == 'updated';
            });


        return view('admin.research.topic.detail', [
            'data' => $data,
            'history' => $history,
            'fileOverview' => ETypeUpload::TOPIC_OVERVIEW,
            'fileAttachment' => ETypeUpload::TOPIC_ATTACHMENT,
            'filePresentation' => ETypeUpload::TOPIC_PRESENTATION,
            'model' => Topic::class,
            'topicStatus' => $topicStatus,
            'topicSource' => $topicSource,
            'topicCheckSendFile' => $topicCheckSendFile
        ]);
    }

    public function update(Request $request) {
        $request->validate([
            'topic_id' => 'required|exists:topic,id'
        ]);

        $topicId = $request->input('topic_id');

        Topic::query()->findOrFail($topicId)->update([
            'status' => ETopicStatus::STATUS_SUBMIT,
            'submit_date' => Carbon::now()
        ]);
        session()->flash('success', __('notification.common.success.submit_topic'));
        return redirect()->route('admin.research.topic.detail', ['id' => $topicId]);
    }
}
