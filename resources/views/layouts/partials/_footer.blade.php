<footer style="margin-top: 30px">
  <div class="container-fluid">
     <div class="row">
        <div class="col-md-6 col-lg-4">
            <p>
              <a href=""><img style="width:180px" src="/images/Logo_VKIST_Transparent_background-36.png" alt="logo"/></a>
            </p>
        </div>
        <div class="col-md-4">
            <p><h3> {{__('footer.info')}}</h3></p>
            <p>
              <img src="/images/Active-call.svg" alt="active-call"/>
              <span>
                {{__('footer.phone')}}
              </span>
            </p>
            <p>
              <img src="/images/Home.svg" alt="home"/>
              <span>Email: vienvkist@most.gov.vn</span>
            </p>
            <p>
              <img src="/images/Home.svg" alt="home"/>
              <span>{{__('footer.address')}}</span>
            </p>
        </div>
        <div class="col-md-2">
           <p><h3>{{__('footer.new')}}</h3></p>
           <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="#">
                    {{__('footer.new-1')}}
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                    {{__('footer.new-2')}}
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                    {{__('footer.new-3')}}
                </a>
              </li>
            </ul>
          </div>
     </div>
  </div>
</footer>
