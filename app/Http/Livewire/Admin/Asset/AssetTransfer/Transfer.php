<?php

namespace App\Http\Livewire\Admin\Asset\AssetTransfer;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use App\Models\AssetAllocatedRevoke;
use App\Models\Asset;
use App\Models\UserInf;
use App\Models\ClaimForm;
use App\Models\AssetClaimForm;
use DB;
use Illuminate\Support\Carbon;
use App\Models\File;
use App\Component\FileUpload;
use App\Enums\EAssetTransferType;
use App\Models\Department;
use Livewire\WithFileUploads;

class Transfer extends BaseLive
{
    use WithFileUploads;

    public $data;
    public $user;
    public $modalDepartmentList = [];
    public $modalUserList = [];
    public $modalAssetList = [];
    public $idAssetSelected = [];
    public $asset;
    public $fileList;
    public $unCheckAsset = [];
    public $disable = false;

    public $name;
    public $assetName;

    public $file = [];
    public $day;
    public $reason;
    public $number_report;

    public $transferType;
    public $fromDepartmentId;
    public $toDepartmentId;
    public $fromUserId;
    public $toUserId;

    public $department1Leader;
    public $department2Leader;

    public $arrAssetChoosed = [];
    public $chooseAll;

    public $type;
    public $model_name;
    public $folder;

    public $errorFile;
    public $errorUser;
    public $errorAsset;

    protected $listeners = ['exportTransfer' => 'export', 'resetDataTransfer' => 'resetForm', 'set-transferDay' => 'setTransferDay'];

    public function mount() {
        $this->day = now()->format(Config::get('common.year-month-day'));
        $this->getUserlist();
        $this->getDepartmentList();
    }

    public function render() {
        if (!empty($this->fromUserId) || !empty($this->fromDepartmentId)) {
             $this->getAssetlist();
        }

        $this->type = Config::get('common.type_upload.Transfer');
        $this->model_name = ClaimForm::class;
        $this->folder = app($this->model_name)->getTable();
        if (!empty($this->file) && count($this->file) > 5) {
            $this->errorFile = __('validation.number_of_file.max', [
                'attribute' => __('data_field_name.labor-contract.file'),
                'max' => 5
            ]);
            array_splice($this->file, 5, 1);
        } elseif (!empty($this->file)) {
            $this->validateFile();
        }

        return view('livewire.admin.asset.asset-transfer.transfer', ['dataList' => $this->user]);
    }

    public function validateFile() {
        $file_upload = new FileUpload();
        $arr = ['jpeg', 'gif', 'png', 'jpg', 'tiff', 'bmp', 'xlsx', 'xls', 'xlms', 'doc', 'docx', 'pdf', 'zip', 'rar', '7z'];
        foreach ($this->file as $index => $item) {
            $dataUpload = $file_upload->uploadFile($item, $this->folder, $this->model_name);
            if (!$item->getSize() > 255 * 1024 * 1024) {
                $this->errorFile = __('common.file_size', [
                    'value' => '255MB'
                ]);
            }
            $tmp = explode('.', $dataUpload['file_name']);
            if (count($tmp) > 0 && !in_array(end($tmp), $arr)) {
                $this->errorFile = __('data_field_name.allocated-revoke.invalid_file');
                array_splice($this->file, $index, 1);
                break;
            }
        }

    }

    public function getAssetlist() {
        $query = Asset::query()->select('asset.id', 'asset.code', 'asset.name', 'unit_type', 'quantity');
        if($this->transferType == EAssetTransferType::BY_INDIVIDUAL) {
            $query->where('user_info_id', $this->fromUserId);
        } else {
            $query->where('department_id', $this->fromDepartmentId);
        }

        if (strlen($this->assetName)) {
            $query->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->assetName)) . '%');
        }
        $modal_asset_list = $query->limit(10)->get();
        $modal_asset_list = $modal_asset_list->map(function ($item) {
            $checked = 0;
            if (count($this->arrAssetChoosed) > 0) {
                foreach ($this->arrAssetChoosed as $id) {
                    if ($item->id == $id) {
                        $checked = 1;
                        break;
                    }
                }
            }

            return [
                'id' => $item->id,
                'code' => $item->code,
                'checked' => $checked,
                'name' => $item->name,
                'quantity' => $item->quantity,
                'unit_type' => $item->unit_type,
            ];
        });
        $this->modalAssetList = collect($modal_asset_list)->all();
    }

    public function getUserlist() {
        $this->modalUserList = UserInf::all();
    }

    public function getDepartmentList()
    {
        $this->modalDepartmentList = Department::all();
    }

    public function openModalAsset() {
        $this->emit('showModalAsset');
    }

    public function cancelAllAsset() {
        $this->idAssetSelected = [];
    }

    public function saveAsset() {
        $this->idAssetSelected = array_map(fn($item) => intval($item), $this->idAssetSelected);
        $this->arrAssetChoosed = [...$this->idAssetSelected];
    }

    public function deleteFileUpload($index) {
        array_splice($this->file, $index, 1);
    }

    public function validateData() {
        $this->validate([
            'day' => ['required'],
            'reason' => ['max:255'],
            'number_report' => ['required', 'max:10', 'regex:/^[A-Z0-9]+$/u'],
            'transferType' => 'required',
            'fromUserId' => ($this->transferType == EAssetTransferType::BY_INDIVIDUAL) ? 'required' : 'nullable',
            'toUserId' => ($this->transferType == EAssetTransferType::BY_INDIVIDUAL) ? 'required' : 'nullable',
            'fromDepartmentId' => ($this->transferType == EAssetTransferType::BY_DEPARTMENT) ? 'required' : 'nullable',
            'toDepartmentId' => ($this->transferType == EAssetTransferType::BY_DEPARTMENT) ? 'required' : 'nullable',
            'arrAssetChoosed' => ['required'],
        ],[],[
            'day'=> __('data_field_name.allocated-revoke.allocated_date'),
            'reason'=> __('data_field_name.allocated-revoke.reason_transfer'),
            'number_report'=> __('data_field_name.allocated-revoke.number_report'),
            'transferType' => __('data_field_name.asset.transfer_type'),
            'fromUserId' => __('data_field_name.allocated-revoke.employee'),
            'toUserId' => __('data_field_name.allocated-revoke.employee'),
            'fromDepartmentId' => __('data_field_name.allocated-revoke.from_department'),
            'toDepartmentId' => __('data_field_name.allocated-revoke.to_department'),
            'file' => 'File',
            'arrAssetChoosed' => __('data_field_name.allocated-revoke.asset')
        ]);
    }

    public function saveData($export) {
        $this->errorFile = null;
        $this->validateData();
        $this->disable = true;
        // dd('hello');
        return DB::transaction(function() use ($export) {
            $day_tranfer = Carbon::parse($this->day);
            $claimForm = new ClaimForm();
            $claimForm->claim_date = $day_tranfer;
            $claimForm->reason = $this->reason;
            $claimForm->type = 7;
            $claimForm->save();
            foreach ($this->arrAssetChoosed as $id) {
                $asset_tranfer = Asset::findOrFail($id);
                if($this->transferType == EAssetTransferType::BY_INDIVIDUAL) {
                    $asset_tranfer->user_info_id = $this->toUserId;
                } elseif ($this->transferType == EAssetTransferType::BY_DEPARTMENT) {
                    $asset_tranfer->department_id = $this->toDepartmentId;
                }
                $asset_tranfer->save();

                DB::table('asset_claim_form')->insert(
                    [
                        'form_id' => $claimForm->id,
                        'asset_id' => $id,
                        'user_info_id' => $this->toUserId,
                        'department_id' => $this->toDepartmentId,
                        'quantity' => $asset_tranfer->amount_used ?? 1,
                    ]
                );

                $allocated = new AssetAllocatedRevoke();
                $allocated->asset_id = $id;
                $allocated->status = 3;
                $allocated->implementation_date = $day_tranfer;
                $allocated->used_type = 1;
                $allocated->implementation_quantity = $asset_tranfer->amount_used;
                $allocated->number_report = $this->number_report;
                $allocated->implementation_reason = $this->reason;
                $allocated->claim_form_id = $claimForm->id;

                $allocated->transfer_type = $this->transferType;
                if($this->transferType == EAssetTransferType::BY_INDIVIDUAL) {
                    $allocated->transfered_user_id = $this->fromUserId;
                    $allocated->user_info_id = $this->toUserId;
                } elseif ($this->transferType == EAssetTransferType::BY_DEPARTMENT) {
                    $allocated->from_department_id = $this->fromDepartmentId;
                    $allocated->to_department_id = $this->toDepartmentId;
                }

                $allocated->save();
            }
            $this->storeFile($claimForm->id);
            if ($export) {
                $this->emit('doExportTransfer');
            } else {
                $this->resetForm();
            }


            session()->flash('success', __('data_field_name.allocated-revoke.success.transfer'));
            return redirect()->route('admin.asset.transfer.index');
        });

    }

    public function storeFile($id_model) {
        foreach ($this->file as $item) {
            $file_store = new FileUpload();
            $dataUpload= $file_store->uploadFile($item, $this->folder, $this->model_name);
            $file_upload = new File();
            $file_upload->url = $dataUpload['url'];
            $file_upload->size_file = $dataUpload['size_file'];
            $file_upload->file_name = $dataUpload['file_name'];
            $file_upload->model_name = $this->model_name;
            $file_upload->model_id = $id_model;

            $file_upload->type = $this->type;
            $file_upload->save();
        }
    }

    public function changeDay() {
        $this->getAssetlist();
    }

    public function resetForm() {
        $this->file = [];
        $this->reason = null;
        $this->number_report = null;
        $this->fromUserId = null;
        $this->toUserId = null;
        $this->arrAssetChoosed = [];
        $this->disable = false;
        $this->fromDepartmentId = null;
        $this->toDepartmentId = null;
    }

    public function export() {

        $fromDepartment = $toDepartment = null;
        $fromUser = $toUser = null;

        if ($this->transferType == EAssetTransferType::BY_DEPARTMENT) {
            $fromDepartment = Department::query()->findOrNew($this->fromDepartmentId);
            $fromUser = $fromDepartment->leader;

            $toDepartment = Department::query()->findOrNew($this->toDepartmentId);
            $toUser = $toDepartment->leader;
        } elseif ($this->transferType == EAssetTransferType::BY_INDIVIDUAL) {
            $fromUser = UserInf::query()->findOrNew($this->fromUserId);
            $fromDepartment = $fromUser->department;

            $toUser = UserInf::query()->findOrNew($this->toUserId);
            $toDepartment = $fromUser->department;
        }

        $numberReport = $this->number_report;
        $reasonTranfer = $this->reason;
        $this->emit('doResetDataTransfer');

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template/template_transfer.docx');
        $templateProcessor->setValue('numberReport', $numberReport);
        $templateProcessor->setValue('day', now()->format(Config::get('common.formatDate')));
        $templateProcessor->setValue('fromUser', $fromUser->fullname ?? '');
        $templateProcessor->setValue('position1', $fromUser->position->v_value ?? '');
        $templateProcessor->setValue('department1', $fromDepartment->name ?? '');
        $templateProcessor->setValue('toUser', $toUser->fullname ?? '');
        $templateProcessor->setValue('position2', $toUser->position->v_value ?? '');
        $templateProcessor->setValue('department2', $toDepartment->name ?? '');
        $templateProcessor->setValue('userCode2', $toUser->code ?? '');
        $templateProcessor->setValue('reason', $reasonTranfer);
        $total = 0;
        $value = [];
        foreach ($this->arrAssetChoosed as $index => $id) {
            $asset_export = Asset::findOrFail($id);
            $total += $asset_export->amount_used;
            $data_export = [
                'index' => $index + 1,
                'assetName' => $asset_export->name,
                'assetCode' => $asset_export->code,
                'assetSerial' => $asset_export->series_number,
                'assetQuantity' => $asset_export->amount_used,
                'assetUnitType' => $asset_export->unit_type,
                'assetNote' => $asset_export->note
            ];
            array_push($value, $data_export);
        }
        $templateProcessor->cloneRowAndSetValues('index', $value);
        $templateProcessor->setValue('total', $total);
        $today = date("d_m_Y");
        $docxFile = 'transfer_' . $today . '.docx';
        $path  = storage_path("app/uploads/" . $docxFile);
        $templateProcessor->saveAs($path);
        return response()->download($path);
    }

    public function setTransferDay($data) {
        $this->day= date('Y-m-d', strtotime($data['transferDay']));
    }

    public function updatedTransferType()
    {
        $this->fromDepartmentId = '';
        $this->toDepartmentId = '';
        $this->fromUserId = '';
        $this->toUserId = '';
        $this->department1Leader = '';
        $this->department2Leader = '';
    }

    public function updatedFromDepartmentId($departmentId)
    {
        $department = Department::query()->find($departmentId);
        $this->department1Leader = $department->leader->fullname ?? null;
    }

    public function updatedToDepartmentId($departmentId)
    {
        $department = Department::query()->find($departmentId);
        $this->department2Leader = $department->leader->fullname ?? null;
    }
}
