<?php

namespace App\Http\Controllers\Admin\General\WorkPlan;

use App\Enums\EWorkPlan;
use App\Enums\ERouteName;
use App\Enums\EValidation;
use App\Enums\ETypeUpload;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Task;
use App\Models\WorkPlan;
use App\Models\WorkPlanHasBusinessFee;
use App\Models\WorkPlanHasSchedule;
use App\Models\WorkPlanHasUserInfo;
use App\Service\Community;
use Illuminate\Http\Request;
use App\Enums\EWorkPlanPriorityLevel;
use App\Models\Budget;

use App\Models\MasterData;
use DB;
use Illuminate\Support\Facades\Config;

class WorkPlanController extends Controller
{
    public $content;
    public $mission_id;
    public $missions_code;
    public $start_date;
    public $end_date;
    public $user_info_id;
    public $priority_list;
    const MAX_MISSIONS = 'max:255';

    public function index()
    {

        return view(ERouteName::ROUTE_WORK_PLAN_INDEX);
    }


    public function create()
    {
        $this->priority_list = [
            EWorkPlanPriorityLevel::MEDIUM => __('data_field_name.work_plan.index.medium'),
            EWorkPlanPriorityLevel::HIGH => __('data_field_name.work_plan.index.high'),
            EWorkPlanPriorityLevel::COGENCY => __('data_field_name.work_plan.index.cogency'),
        ];
        $model_name = WorkPlan::class;
        $type = ETypeUpload::WORK_PLAN;
        $folder = app($model_name)->getTable();
        $status = -1;

        $source_cost = Budget::all();
        return view('admin.general.work-plan.create', ['source_cost' => $source_cost, 'priority_list' => $this->priority_list,
            'model_name' => $model_name, 'type' => $type, 'folder' => $folder, 'status' => $status]);
    }

    public function store(Request $request)
    {
        if ($request->has('cancel')) {
            File::where('model_name', WorkPlan::class)
                ->where('type', ETypeUpload::WORK_PLAN)
                ->where('status', -1)
                ->where('model_id', null)
                ->delete();
            WorkPlanHasUserInfo::where('admin_id', auth()->id())->where('status', 0)->where('work_plan_id', null)->delete();
            return redirect()->route(ERouteName::ROUTE_WORK_PLAN_INDEX);
        } else if ($request->has('save')) {
            $WorkPlan = new WorkPlan;
            $this->validate($request, [
                'missions' => 'required|'.self::MAX_MISSIONS,
                'content' => 'required',
                'start_date' => 'required',
                'end_date' => 'required|after_or_equal:start_date',
                'estimated_cost' => ['regex:/^[0-9' . EValidation::ALPHABET . '\.\, ]+$/', 'max:11','nullable'],
                'missions_code' => 'alpha_dash|max:48|nullable',
            ], [], [
                'missions' => __('data_field_name.work_plan.under_missions'),
                'content' => __('data_field_name.work_plan.content'),
                'start_date' => __('data_field_name.work_plan.create.start_time'),
                'end_date' => __('data_field_name.work_plan.create.end_time'),
                'estimated_cost' => __('data_field_name.work_plan.create.fee'),
                'missions_code' => __('data_field_name.work_plan.create.MISSIONS_CODE'),
            ]);
            $WorkPlan->content = $request->content;
            $WorkPlan->missions = $request->missions;
            $WorkPlan->budget_id = $request->budget_id;
            $WorkPlan->missions_code = $request->missions_code;
            $WorkPlan->start_date = $request->start_date;
            $WorkPlan->end_date = $request->end_date;
            $WorkPlan->estimated_cost = Community::getAmount($request->estimated_cost);
            $WorkPlan->priority_level = $request->priority_level;
            $WorkPlan->status = EWorkPlan::STATUS_NEW;
            $WorkPlan->admin_id = auth()->id();

            $WorkPlan->save();
            $user_info = WorkPlanHasUserInfo::where('status', 0)->where('admin_id', auth()->id())->get();
            \App\Models\File::where('model_name', WorkPlan::class)
                ->where('status', -1)
                ->where('model_id', null)->update([
                    'status' => 1,
                    'model_id' => $WorkPlan->id,
                ]);
            if ($user_info) {
                foreach($user_info as $item) {
                    $item->update([
                        'status' => 1,
                        'work_plan_id' => $WorkPlan->id,
                    ]);
                }
            }

            $leaderID = WorkPlanHasUserInfo::leftJoin('master_data', 'work_plan_has_user_info.position_id', '=', 'master_data.id')
                ->where('work_plan_id', $WorkPlan->id)
                ->where('order_number', '<=', DB::raw('(select min("ORDER_NUMBER") from work_plan_has_user_info join "MASTER_DATA" a on work_plan_has_user_info.position_id = a.id  where a."TYPE"=7 and work_plan_id = ' . $WorkPlan->id . ' GROUP BY a."TYPE")'))
                ->select('work_plan_has_user_info.user_info_id')->get();
            foreach ($leaderID as $value) {
                WorkPlan::where('id', $WorkPlan->id)->update([
                    'leader_id' => $value->user_info_id,
                ]);
            }
            return redirect()->route(ERouteName::ROUTE_WORK_PLAN_INDEX)->with('success', __('notification.common.success.add'));
        }
    }

    public function resetInput()
    {
        $this->content = '';
        $this->mission_id = '';
        $this->missions_code = '';
        $this->start_date = '';
        $this->end_date = '';
        $this->missions = '';
    }

    public function edit($id)
    {
        $this->priority_list = [
            EWorkPlanPriorityLevel::MEDIUM => __('data_field_name.work_plan.index.medium'),
            EWorkPlanPriorityLevel::HIGH => __('data_field_name.work_plan.index.high'),
            EWorkPlanPriorityLevel::COGENCY => __('data_field_name.work_plan.index.cogency'),
        ];
        $model_name = WorkPlan::class;
        $type = ETypeUpload::WORK_PLAN;
        $folder = app($model_name)->getTable();
        $status = -2;

        $source_cost = Budget::all();
        $data = WorkPlan::findOrFail($id);
        $missions = Task::all();
        return view('admin.general.work-plan.edit', ['source_cost' => $source_cost, 'data' => $data, 'missions' => $missions,
            'priority_list' => $this->priority_list,
            'model_name' => $model_name, 'type' => $type, 'folder' => $folder, 'status' => $status]);
    }

    public function formatDate($date) {
        $result = null;
        if(!is_null($date)) {
            $date_array = explode('/', $date);
            $result = $date_array[1] . '/' . $date_array[0] . '/' . $date_array[2];
            $result = date('Y-m-d', strtotime($result));
        }
        return $result;
    }

    public function update($id, Request $request)
    {
        //format date data
        $start_date_format = WorkPlanController::formatDate($request->start_date);
        $end_date_format = WorkPlanController::formatDate($request->end_date);

        $request->request->set('start_date', $start_date_format);
        $request->request->set('end_date', $end_date_format);

        if ($request->has('cancel')) {
            File::where('model_name', WorkPlan::class)
                ->where('type', ETypeUpload::WORK_PLAN)
                ->where('status', -2)
                ->delete();
            File::where('model_name', WorkPlanHasBusinessFee::class)
                ->where('type', Config::get('common.type_upload.WorkPlanHasBusinessFee'))
                ->where('status', -1)
                ->orWhere('status', -2)
                ->delete();

            WorkPlanHasBusinessFee::where('work_plan_id', $id)->where('status', 0)->delete();
            WorkPlanHasSchedule::where('work_plan_id', $id)->where('status', 0)->delete();
            return redirect()->route(ERouteName::ROUTE_WORK_PLAN_INDEX, $id);
        } else if ($request->has('save')) {
            $this->validate($request, [
                'missions' => 'required|'.self::MAX_MISSIONS,
                'contents' => 'required|max:255',
                'start_date' => 'required',
                'end_date' => $request->start_date ? 'required | after_or_equal:'.$request->start_date : 'required',
                'estimated_cost' => ['regex:/^[0-9' . EValidation::ALPHABET . '\.\, ]+$/', 'max:11','nullable'],
                'missions_code' => ['regex:/^[a-zA-Z0-9' . EValidation::ALPHABET . '\.\, ]+$/', 'max:48'],
                'result' => 'max:1000',


            ], [
                'end_date.after_or_equal' => __('data_field_name.work_plan.create.end_date_after_error_message'),
            ], [
                'missions' => __('data_field_name.work_plan.under_missions'),
                'contents' => __('data_field_name.work_plan.edit.content'),
                'start_date' => __('data_field_name.work_plan.create.start_time'),
                'end_date' => __('data_field_name.work_plan.create.end_time'),
                'estimated_cost' => __('data_field_name.work_plan.create.fee'),
                'missions_code' => __('data_field_name.work_plan.create.MISSIONS_CODE'),
                'result' => __('data_field_name.work_plan.edit.result'),

            ]);
            $workPlan = WorkPlan::findOrFail($id)->update([
                'content' => $request->contents,
                'missions' => $request->missions,
                'budget_id' => $request->budget_id,
                'missions_code' => $request->missions_code,
                'real_start_date' => $request->start_date,
                'real_end_date' => $request->end_date,
                'estimated_cost' => Community::getAmount($request->estimated_cost),
                'result' => $request->result,
                'priority_level' => $request->priority_level,
            ]);


            if ($workPlan) {
                WorkPlanHasBusinessFee::where('work_plan_id', $id)->where('status', 0)
                    ->update([
                        'status' => 1,
                    ]);
            }

                WorkPlanHasSchedule::where('work_plan_id', $id)->where('status', 0)
                    ->update([
                        'status' => 1,
                    ]);


            File::where('model_name', WorkPlan::class)
                ->where('type', ETypeUpload::WORK_PLAN)
                ->where('status', -2)
                ->update([
                    'status' => 1,
                ]);
            File::where('model_name', WorkPlanHasBusinessFee::class)
                ->where('type', Config::get('common.type_upload.WorkPlanHasBusinessFee'))
                ->where('status', -1)
                ->where('model_id', null)
                ->update([
                    'status' => 1,
                ]);
            return redirect()->route(ERouteName::ROUTE_WORK_PLAN_INDEX)->with('success', __('notification.common.success.update'));
        }
    }

    public function show($id)
    {
        $this->priority_list = [
            EWorkPlanPriorityLevel::MEDIUM => __('data_field_name.work_plan.index.medium'),
            EWorkPlanPriorityLevel::HIGH => __('data_field_name.work_plan.index.high'),
            EWorkPlanPriorityLevel::COGENCY => __('data_field_name.work_plan.index.cogency'),
        ];
        $data = WorkPlan::findOrFail($id);
        $missions = Task::all();
        $source_cost = Budget::all();
        $requestId = time() . random_int(0,99);

        $files = File::where('model_name', WorkPlan::class)
            ->where('type', ETypeUpload::WORK_PLAN)
            ->where('status', 1)
            ->where('model_id', $id)
            ->get();

        return view('admin.general.work-plan.detail',
            [
                'source_cost' => $source_cost,
                'missions' => $missions, 'data' => $data,
                'priority_list' => $this->priority_list,
                'requestId' => $requestId,
                'files'=>$files
            ]);
    }
}
