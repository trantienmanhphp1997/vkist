<div class="inner-tab">
    @livewire('admin.user.user-academic-tab1',['user_info_id'=>$user_info_id])
    @livewire('admin.user.user-academic-tab2',['user_info_id'=>$user_info_id])


</div>


<script>
    $("document").ready(function() {
        window.livewire.on('userStore', () => {
            document.getElementById('close-academic-create1').click();
            document.getElementById('close-academic-update1').click();
            document.getElementById('close-academic-create2').click();
            document.getElementById('close-academic-update2').click();
        });
    });
</script>
