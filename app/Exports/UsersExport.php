<?php

namespace App\Exports;

use App\Models\UserInf;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
class UsersExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $getIdSelected;
    protected $searchKey ;
    protected $searchDepartment ;
    protected $searchPosition ;
    protected $searchDate ;
    protected $searchProject ;
    protected $searchStaffIds;
    function __construct($checked,$searchKey,$searchDepartment , $searchPosition , $searchDate , $searchProject,$searchStaffIds) {
        $this->searchKey = $searchKey;
        $this->getIdSelected = $checked;
        $this->searchDepartment = $searchDepartment ;
        $this->searchPosition = $searchPosition;
        $this->searchDate = $searchDate;
        $this->searchProject = $searchProject ;
        $this->searchStaffIds = $searchStaffIds;
    }

    public function collection()
    {
        if(empty($this->getIdSelected))
        {
            $query=UserInf::leftJoin('department','department.id','=', 'user_info.department_id' )
                ->leftJoin('master_data','master_data.id','=', 'user_info.position_id' )->with(['department:id,name', 'position:id,v_value']);
            if (strlen($this->searchKey)) {
                $query->where('user_info.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchKey)) . '%');
            }
            if (!empty($this->searchDepartment)) {
                $query->where('department_id', $this->searchDepartment);
            }

            if (!empty($this->searchPosition)) {
                $query->where('position_id', $this->searchPosition);
            }

            if (!empty($this->searchDate)) {
                $query->whereDate('user_info.created_at', '=', $this->searchDate);
            }

            if (!empty($this->searchProject) && !empty($this->searchStaffIds)) {
                $query->whereIn('user_info.id', $this->searchStaffIds);
            }

            $user = $query->select('user_info.id','user_info.code','fullname',DB::raw('department.name as department_name'),
                DB::raw('master_data.v_value as master_data_name')
                ,'gender','birthday','address','phone','email','married','ethnic_id','religion_id','nationality_id','id_card','issued_date','issued_place', DB::raw('user_info.note as user_note'))->orderBy('id', 'DESC')->get();
        }
        else
        {
            $query=UserInf::whereIn('user_info.id', $this->getIdSelected)->leftJoin('department','department.id','=', 'user_info.department_id' )
                ->leftJoin('master_data','master_data.id','=', 'user_info.position_id' )->with(['department:id,name', 'position:id,v_value']);
            if (strlen($this->searchKey)) {
                $query->where('user_info.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchKey)) . '%');
            }
            if (!empty($this->searchDepartment)) {
                $query->where('department_id', $this->searchDepartment);
            }

            if (!empty($this->searchPosition)) {
                $query->where('position_id', $this->searchPosition);
            }

            if (!empty($this->searchDate)) {
                $query->whereDate('user_info.created_at', '=', $this->searchDate);
            }

            if (!empty($this->searchProject) && !empty($this->searchStaffIds)) {
                $query->whereIn('user_info.id', $this->searchStaffIds);
            }

            $user = $query->select('user_info.id','user_info.code','fullname',DB::raw('department.name as department_name'),
                DB::raw('master_data.v_value as master_data_name')
                ,'gender','birthday','address','phone','email','married','ethnic_id','religion_id','nationality_id','id_card','issued_date','issued_place', DB::raw('user_info.note as user_note'))->orderBy('id', 'DESC')->get();

        }
        $key=1;
        foreach ($user as $value){
            $value->id=$key++;
            if ($value->married==1){
                $value->married = __('data_field_name.common_field.married');
            } else {
                $value->married = __('data_field_name.common_field.single');
            }
        }

        return $user;
    }
    public function headings():array
    {
        return
        [
            'STT',
            'Mã nhân viên',
            'Họ và tên',
            'Đơn vị trực thuộc',
            'Chức danh',
            'Giới tính',
            'Ngày sinh',
            'Địa chỉ',
            'Số điện thoại',
            'Email',
            'Tình trạng hôn nhân',
            'Dân tộc',
            'Tôn giáo',
            'Quốc tịch',
            'Số CMND',
            'Ngày cấp',
            'Nơi cấp',
            'Số hộ chiếu',
            'Ngày cấp',
            'Nơi cấp',
            'Ghi chú'
        ];
    }

    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'S', 'T','U',
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('D1:G1');
            $event->sheet->setCellValue('D1','DANH SÁCH CÁN BỘ');
            $event->sheet->getStyle('D1:G1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:U3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        return 'DANH SÁCH CÁN BỘ';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
