<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 500)->comment('Tên đơn vị');
            $table->string('note',1000)->nullable()->comment('Mo tả phong ban');
            $table->tinyInteger('status')->nullable()->comment('trang thai');
            $table->bigInteger('parent_id')->nullable()->comment('phong ban cha map voi department');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->bigInteger('leader_id')->nullable()->comment('truong phong, nguoi dung dau');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department');
    }
}
