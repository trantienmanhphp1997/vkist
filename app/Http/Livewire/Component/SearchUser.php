<?php

namespace App\Http\Livewire\Component;

use App\Http\Livewire\Base\BaseLive;
use App\Models\UserInf;

class SearchUser extends BaseLive
{
    public $query='';
    public $data;
    public $successful=false;
    public $name;
    public $key;
    public function resetInput()
    {
        $this->query = '';
        $this->data = [];
    }

    public function updatedQuery()
    {
        $this->data = UserInf::where('fullname', 'like', '%' . $this->query . '%')
            ->get()
            ->toArray();
    }
    public function render()
    {
        if(!$this->query) {
            $this->successful=false;
        }
        if($this->query==''){
            $this->emit('updateAsset');
        }

        return view('livewire.component.search-user');
    }
    public function setInput($id,$name){
        $this->query=$name;
        $this->successful=true;
        $this->emit('setDepartment11', [$this->name=>$id]);
    }
}
