@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.approval_topic.detail_review_profile')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div  class="breadcrumbs"><a href="{{route('admin.approval.index')}}">{{__('data_field_name.approval_topic.text_manager_approval')}} </a> \ <span>{{__('data_field_name.approval_topic.detail_review_profile')}}</span></div>
        <div class="creat-box bg-content border-20">
            <div class="detail-task pt-44">
                <h4>{{__('data_field_name.approval_topic.detail_review_profile')}}</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('data_field_name.approval_topic.name_profile')}}</label>
                            <input type="text" class="form-control form-input" value="{{$approval->name}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('data_field_name.common_field.status')}}</label>
                            <select class="form-control form-control-lg">
                                @foreach($statusAppraisal as $key => $textStatus)
                                    @if ($approval->status == $key)
                                        <option value='{!! $key !!}'>{!! $textStatus !!}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('data_field_name.approval_topic.profile_type')}}</label>
                            <select class="form-control form-control-lg profile-type">
                                @foreach($typeAppraisal as $key => $val)
                                    @if ($key == $approval->type)
                                        <option value='{!! $key !!}'>{!! $val !!}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if ($approval->type != \App\Enums\EApproval::TYPE_IDEAL)
                        <div class="row code-file px-16">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__('data_field_name.approval_topic.profile_code')}}</label>
                                    <input type="text" class="form-control form-input" value="{{$approval->code}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__('data_field_name.contract.code')}}</label>
                                    <input type="text" class="form-control form-input" value="{{$approval->contract_code}}" readonly placeholder="{{__('data_field_name.approval_topic.text_contract_code')}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    @if ($approval->type == \App\Enums\EApproval::TYPE_TOPIC)
                                        <label>{{__('data_field_name.topic.code')}}</label>
                                        <select class="form-control form-control-lg">
                                            <option value="">{!! $topic->code !!}</option>
                                        </select>
                                    @endif
                                    @if ($approval->type == \App\Enums\EApproval::TYPE_COST)
                                        <label>{{__('data_field_name.approval_topic.type_cost_estimate')}}</label>
                                        <select class="form-control form-control-lg">
                                            <option value="">{!! $topic_fee->name !!}</option>
                                        </select>
                                    @endif
                                    @if ($approval->type == \App\Enums\EApproval::TYPE_PLAN)
                                        <label>{{__('data_field_name.approval_topic.type_research_plan')}}</label>
                                        <select class="form-control form-control-lg">
                                            <option value="">{!! $research_plan->name !!}</option>
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    @if ($approval->type == \App\Enums\EApproval::TYPE_TOPIC)
                                        <label>{{__('data_field_name.topic.name')}}</label>
                                        <div class="content-link form-control form-input">
                                            @if (!empty($topic))
                                                <a href="{{ route('admin.research.topic.edit', $topic->id) }}">{{ $topic->name }}</a>
                                            @endif
                                        </div>
                                    @endif
                                    @if ($approval->type == \App\Enums\EApproval::TYPE_COST)
                                        <label>{{__('data_field_name.research_cost.name_topic_fee')}}</label>
                                        <div class="content-link form-control form-input">
                                            @if (!empty($topic_fee))
                                                <a href="{{ route('admin.research.topic-fee.edit', $topic_fee->id) }}">{{ $topic_fee->name }}</a>
                                            @endif
                                        </div>
                                    @endif
                                    @if ($approval->type == \App\Enums\EApproval::TYPE_PLAN)
                                        <label>{{__('data_field_name.approval_topic.type_research_plan')}}</label>
                                        <div class="content-link form-control form-input">
                                            @if (!empty($research_plan))
                                                <a href="{{ route('admin.research.plan.research_plan.edit', $research_plan->id) }}">{{ $research_plan->name }}</a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>{{__('data_field_name.approval_topic.appraisal_board')}}</label>
                                    <select class="form-control form-control-lg">
                                        @if ($appraisal_board_id == null)
                                            <option value="">{{__('data_field_name.approval_topic.not_create_appraisal')}}</option>
                                        @else
                                            <option value="{{$appraisal_board_id}}">{{$appraisalName}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 d-none">
                                <div class="form-group">
                                    @if ($approval->status == \App\Enums\EApproval::STATUS_WAIT_ASSESSMENT)
                                        @if ($appraisal_board_id == null)
                                            <button type="button" class="btn btn-main bg-7 mt-32 text-5 size14 py-12 px-15 w-100 buttonCreate" data-toggle="modal" data-target="#createAppraisal">
                                                <img src="/images/Plus3.svg" alt="plus">{{__('data_field_name.approval_topic.text_create_appraisal')}}
                                            </button>
                                            <p><span class="error text-danger">*{{__('data_field_name.approval_topic.not_create_appraisal')}}</span></p>
                                        @else
                                            <button type="button" class="btn btn-main bg-7 mt-32 text-5 size14 py-12 px-15 w-100 buttonEdit" data-toggle="modal" data-target="#editAppraisal">
                                                <img src="/images/pent2.svg" alt="pent">{{__('data_field_name.approval_topic.text_edit_appraisal')}}
                                            </button>
                                        @endif
                                    @endif

                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($approval->type == \App\Enums\EApproval::TYPE_IDEAL)
                        <div class="col-md-12 list-idea px-16">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('data_field_name.approval_topic.profile_code')}}</label>
                                        <input type="text" class="form-control form-input" value="{{$approval->code}}" readonly>
                                    </div>
                                </div>
                                <h3 class="title px-16">{{__('research/ideal.list')}}</h3>
                                <div class="table-responsive px-16">
                                    <table class="table table-general ">
                                        <thead>
                                        <tr class="border-radius">
                                            <th class="border-radius-left text-center" scope="col">{{__('data_field_name.common_field.code')}}</th>
                                            <th class="text-center" scope="col">{{__('data_field_name.approval_topic.name_ideal')}}</th>
                                            <th class="text-center" scope="col">{{__('data_field_name.topic.field')}}</th>
                                            <th class="text-center" scope="col">{{__('data_field_name.topic.expected_fee')}}</th>
                                            <th class="text-center" scope="col">{{__('data_field_name.common_field.start_time')}}</th>
                                            <th class="text-center" scope="col" class="border-radius-right min-width text-center">{{__('data_field_name.approval_topic.proposal_sender')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (!empty($listIdeal))
                                            @foreach($listIdeal as $ideal)
                                                <tr>
                                                    <td class="text-center"><a href="{{route('admin.research.ideal.edit', $ideal['id'])}}">{!! $ideal['code'] !!}</a></td>
                                                    <td class="text-center">{!! $ideal['name'] ?? '' !!}</td>
                                                    <td class="text-center">{!! $ideal['research_name'] ?? '' !!}</td>
                                                    <td class="text-center">{!! numberFormat($ideal['fee']) ?? '' !!}</td>
                                                    <td class="text-center">{!! $ideal['created_at'] ?? '' !!}</td>
                                                    <td class="text-center">{!! $ideal['fullname'] ?? '' !!}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($approval->status == \App\Enums\EApproval::STATUS_APPROVAL && $approval->type != \App\Enums\EApproval::TYPE_IDEAL)
                        @livewire('common.list-attach-file', ['modelId' => $modelId, 'type' => $approval->type])
                    @endif
                </div>
                <div class=" col-md-12 text-center mt-42">
                    <a href="{{ route('admin.approval.index') }}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{__('common.button.cancel')}}</a>

                    @if ($approval->type == \App\Enums\EApproval::TYPE_IDEAL)
                        @if ($approval->status == \App\Enums\EApproval::STATUS_NEW)
                            <button type="button" class="btn btn-main bg-primary text-9 size16 px-48 py-14 buttonSendApproval">{{__('data_field_name.approval_topic.send_approval')}}</button>
                        @endif
                    @else
                        @if ($approval->status == \App\Enums\EApproval::STATUS_WAIT_ASSESSMENT && $appraisalName != null)
                            <button type="button" class="btn btn-main bg-primary text-9 size16 px-48 py-14 buttonSendApproval">{{__('data_field_name.approval_topic.send_approval')}}</button>
                        @endif
                    @endif
                </div>

                <br>

                @livewire('common.api-approval', ['modelId' => $modelId, 'type' => $approval->type, 'requestId' => $requestId, 'approvalId' => $idApproval])
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
