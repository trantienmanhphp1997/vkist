<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class UserResearchPaper extends Eloquent
{
    use HasFactory;
    protected $table = "user_research_paper";
    protected $guarded=['*'];
    protected $fillable = [
        'user_info_id',
        'product_name',
        'author',
        'publishing_company',
        'issn',
        'publishing_year',
    ];
}
