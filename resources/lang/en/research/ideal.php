<?php
return [
    'breadcrumbs' => [
        'research-ideal-management' => 'Research idea management',
    ],
    'list' => 'List of ideas',
    'filter' => [
        'input' => [
            'placeholder' => [
                'search' => 'Search',
                'proposer' => 'Proposer',
            ],
        ],
        'select-box' => [
            'option' => [
                'status' => 'Status',
            ],
        ],
    ],
    'status' => [
        'waiting_for_appraisal' => 'Waiting for approval',
        'approved' => 'Approved',
        'refuse' => 'Reject',
        'just_created' => 'Unsent',
    ],
    'table_column' => [
        'code' => 'CONCEPT CODE',
        'name' => 'NAME OF IDEA',
        'proposer' => 'PROPOSER',
        'status' => 'STATUS',
        'action' => 'Action',
        'created_at' => 'Propose date',
        'updated_at' => 'Update date',
        'research_field' => 'Research field'
    ],
    'title' => [
        'create' => 'Create new ideas',
        'edit' => 'Edit',
        'detail' => 'Detailed idea'
    ],
];