<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumn3ToUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->dropColumn('leave_day_last_year');
            $table->dropColumn('leave_day_remaining');
        });
        Schema::table('user_info', function (Blueprint $table) {
            $table->float('leave_day_last_year',10,1)->nullable()->comment('So ngay nghi con han nam truoc');
            $table->float('leave_day_remaining',10,1)->nullable()->comment('So ngay nghi phep con lai');
            $table->float('leave_day_current_year',10,1)->nullable()->comment('So ngay phep da su dung nam nay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->integer('leave_day_last_year')->nullable()->comment('So ngay nghi con han nam truoc');
            $table->dropColumn('leave_day_remaining');
        });
        Schema::table('user_info', function (Blueprint $table) {
            $table->dropColumn('leave_day_last_year');
            $table->dropColumn('leave_day_remaining');
            $table->dropColumn('leave_day_current_year');
        });
    }
}
