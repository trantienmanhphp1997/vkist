<?php


namespace App\Enums;


class EResearchCategoryType {
    const TOPIC = 1;
    const TYPE = 2;
    const FIELD = 3;
}
