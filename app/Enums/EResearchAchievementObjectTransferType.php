<?php


namespace App\Enums;


class EResearchAchievementObjectTransferType
{
    const INDUSTRIAL_PROPERTY_OBJECTS = 1; //Các đối tượng sở hữu công nghiệp;
    const TECHNICAL_KNOWLEDGE_ABOUT_TECHNOLOGY = 2; //Các bí quyết kỹ thuật, kiến thức kỹ thuật về công nghệ;
    const SOLUTIONS_TO_RECOGNIZE_PRODUCTION = 3; //Các giải pháp hợp lý hóa sản xuất, đổi mới công nghệ.

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::INDUSTRIAL_PROPERTY_OBJECTS:
                $result = __('research/achievement.object_transfer_type.industrial_property_object');
                break;
            case self::TECHNICAL_KNOWLEDGE_ABOUT_TECHNOLOGY:
                $result = __('research/achievement.object_transfer_type.technical_knowledge_about_technology');
                break;
            case self::SOLUTIONS_TO_RECOGNIZE_PRODUCTION:
                $result = __('research/achievement.object_transfer_type.solutions_to_recognize_production');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
