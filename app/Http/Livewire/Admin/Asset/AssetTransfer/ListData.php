<?php

namespace App\Http\Livewire\Admin\Asset\AssetTransfer;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use App\Models\AssetAllocatedRevoke;
use App\Models\UserInf;
use DB;
use App\Exports\AssetTransferExport;
use Excel;
use Illuminate\Support\Carbon;
use App\Enums\EAssetStatus;
use App\Enums\EAssetTransferType;
use App\Models\Department;

class ListData extends BaseLive
{
    public $searchTerm;
    public $isChecked;
    public $selectedIndex;
    public $selectedValue;
    public $arrData;
    public $checkCreatePermission;
    public $total_asset = 0;
    public $checkDownloadPermission;

    public function mount() {
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkDownloadPermission = checkRoutePermission('download');
    	$this->isChecked = 0;
    	$this->arrData = [];
    }

    public function render() {
    	$data = $this->searchData();
        return view('livewire.admin.asset.asset-transfer.list-data', ['data' => $data]);
    }

    public function searchData() {
        $this->total_asset = 0;
        $query = AssetAllocatedRevoke::query()
        	->where('status', EAssetStatus::TRANSFER);
        if (strlen($this->searchTerm)) {
        	$query->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        $data = $query->orderBy('id','DESC')->paginate($this->perPage);
        $tmp = $data->map(function ($item) {
        	$perfomer = UserInf::query()->find($item->admin_id);

            $from = $to = '';
            if ($item->transfer_type == EAssetTransferType::BY_INDIVIDUAL) {
                $from = UserInf::query()->find($item->transfered_user_id)->fullname ?? '';
                $to = UserInf::query()->find($item->user_info_id)->fullname ?? '';
            } elseif ($item->transfer_type == EAssetTransferType::BY_DEPARTMENT) {
                $from = Department::query()->find($item->from_department_id)->name ?? '';
                $to = Department::query()->find($item->to_department_id)->name ?? '';
            }

            $this->total_asset += $item->implementation_quantity;
            return [
                'id' => $item->id,
                'number_report' => $item->number_report,
                'implementation_date' => Carbon::parse($item->implementation_date)->format(Config::get('common.formatDate')),
                'from' => $from,
                'to' => $to,
                'implementation_quantity' => $item->implementation_quantity,
                'reason' => $item->implementation_reason,
                'perfomer' => !empty($perfomer) ? $perfomer->fullname : null,
                'transfer_type' => EAssetTransferType::valueToName($item->transfer_type),
            ];
        });
        $data->setCollection($tmp);
        return $data;
    }
    public function selectedItem($index, $value) {
    	$this->selectedIndex = $index;
    	$this->selectedValue = $value;
    }

    public function checked($value) {
    	$this->isChecked = $value;
    	$this->selectedIndex = null;
    }

    public function export() {
        return Excel::download(new AssetTransferExport($this->searchTerm), 'transfer-' . now()->format(Config::get('common.year-month-day')) . '-' . '.xlsx');
    }
}
