<?php

namespace App\Http\Livewire\Admin\Research\Cost;

use App\Enums\ETopicFee;
use App\Models\TopicFee;
use App\Models\TopicFeeDetail;
use App\Models\ExpertCost;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Topic;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;

class ExpertCostList extends BaseLive
{
    public $topicFeeDetailId;
    public $expert_name;
    public $nationality;
    public $organization;
    public $content;
    public $working_time;
    public $salary;
    public $state_capital;
    public $other_capital;
    public $expertCostID;
    public $topicFeeId;
    public $topicFeeStatus;

    public function mount($topicFeeDetailId, $topicFeeId, $topicFeeStatus) {
        $this->topicFeeDetailId = $topicFeeDetailId;
        $this->topicFeeId = $topicFeeId;
        $this->topicFeeStatus = $topicFeeStatus;
    }

    public function render()
    {
        $this->updateTopicFeeDetail();
        $query = ExpertCost::query();
        $query->where('topic_fee_detail_id', '=', $this->topicFeeDetailId);
        $dataTotal = [
            'total' => $query->sum('total'),
            'state_capital' => $query->sum('state_capital'),
            'other_capital' => $query->sum('other_capital'),
        ];
        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);

        return view('livewire.admin.research.cost.expert-cost-list', ['dataTopicFee' => $this->topicFeeDetailId, 'data' => $data, 'dataTotal' => $dataTotal]);
    }

    public function validateData()
    {
        $this->validate([
            'expert_name' => 'required|max:100',
            'nationality' => 'required|max:100',
            'organization' => 'required|max:100',
            'content' => 'required|max:100',
            'working_time' => 'required|digits_between:1,2|numeric',
            'salary' => 'required|regex:/^[0-9,]+$/|max:15',
            'state_capital' => 'nullable|regex:/^[0-9,]+$/|max:18',
            'other_capital' => 'nullable|regex:/^[0-9,]+$/|max:18',
            ],[
                'salary.max' => __('client_validation.form.research_cost.max.salary'),
                'state_capital.max' => __('client_validation.form.research_cost.max.state_capital'),
                'other_capital.max' => __('client_validation.form.research_cost.max.other_capital'),
            ],[
            'expert_name' => __('data_field_name.research_cost.expert_name'),
            'nationality' => __('data_field_name.research_cost.nationality'),
            'organization' => __('data_field_name.research_cost.organization'),
            'content' => __('data_field_name.research_cost.content'),
            'working_time' => __('data_field_name.research_cost.working_time'),
            'salary' => __('data_field_name.research_cost.salary'),
            'state_capital' => __('data_field_name.research_cost.state_capital'),
            'other_capital' => __('data_field_name.research_cost.other_capital'),

        ]);
    }
    public function store()
    {
        $this->validateData();
        $expertCost = new expertCost;
        $expertCost->expert_name = $this->expert_name;
        $expertCost->nationality = $this->nationality;
        $expertCost->organization = $this->organization;
        $expertCost->content = $this->content;
        $expertCost->working_time = $this->working_time;
        $expertCost->salary = removeFormatNumber($this->salary);
        $expertCost->state_capital = removeFormatNumber($this->state_capital);
        $expertCost->other_capital = removeFormatNumber($this->other_capital);
        $expertCost->total = removeFormatNumber($this->salary)*removeFormatNumber($this->working_time);
        $expertCost->topic_fee_detail_id = $this->topicFeeDetailId;
        $expertCost->admin_id = Auth::user()->id;
        $expertCost ->save();
        $this->emit('close-modal-create-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function resetInputFields(){
        $this->expert_name= null;
        $this->nationality= null;
        $this->organization=null;
        $this->content=null;
        $this->working_time=null;
        $this->salary=null;
        $this->state_capital=null;
        $this->other_capital=null;
        $this->resetValidation();
    }

    public function edit($id){
        $this->expertCostID = $id;
        $expertCost = ExpertCost::findOrFail($id);
        $this->expert_name = $expertCost->expert_name;
        $this->nationality = $expertCost->nationality;
        $this->organization = $expertCost->organization;
        $this->content = $expertCost->content;
        $this->working_time = $expertCost->working_time;
        $this->salary = $expertCost->salary;
        $this->state_capital = $expertCost->state_capital;
        $this->other_capital = $expertCost->other_capital;
        $this->total = $expertCost->total;
        $this->resetValidation();
    }

    public function update(){
        $expertCost = ExpertCost::findOrFail($this->expertCostID);
        $this->validateData();
        $expertCost->expert_name = $this->expert_name;
        $expertCost->nationality = $this->nationality;
        $expertCost->organization = $this->organization;
        $expertCost->content = $this->content;
        $expertCost->working_time = $this->working_time;
        $expertCost->salary = removeFormatNumber($this->salary);
        $expertCost->state_capital = removeFormatNumber($this->state_capital);
        $expertCost->other_capital = removeFormatNumber($this->other_capital);
        $expertCost->total = removeFormatNumber($this->salary)*removeFormatNumber($this->working_time);
        $expertCost->save();

        $this->emit('close-modal-edit-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }

    public function deleteId($id){
        $this->expertCostID = $id;
    }

    public function delete(){
        $expertCost = ExpertCost::findOrFail($this->expertCostID);

        $expertCost->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }

    public function updateTopicFeeDetail(){
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->topicFeeDetailId);
        $topicFeeDetail->total_capital = ExpertCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('total');
        $topicFeeDetail->state_capital = ExpertCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('state_capital');
        $topicFeeDetail->other_capital = ExpertCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('other_capital');
        $topicFeeDetail->save();
    }
}
