<?php

namespace App\Http\Controllers\Admin\ResearchProject;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductivityController extends Controller
{
    public function index(){
        return view('admin.researchProject.productivity.index');
    }
}
