<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class CategoryNews extends BaseModel
{
    use HasFactory;

    protected $fillable = ['name','admin_id','slug','title'];

    public function admin(){
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function news(){
        return $this->hasMany(News::class, 'category_id', 'id');
    }

    private static $searchable = [
        'name'
    ];
    public static function getListSearchAble(){
        return self::$searchable;
    }
}
