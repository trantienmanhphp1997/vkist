<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('year_created')->nullable()->comment('nam');
            $table->string('code', 255)->nullable()->comment('ma ngan sach');
            $table->string('name', 500)->nullable()->comment('Ten ngan sach');
            $table->string('content', 1000)->nullable()->comment('noi dung');
            $table->string('estimated_code', 255)->nullable()->comment('Ma du toan');
            $table->tinyInteger('status')->nullable()->comment('1: cho phe duyet; 2: da phe duyet; 3: tu choi, 4:moi tao');
            $table->bigInteger('total_budget')->nullable()->comment('Tong ngan sach');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget');
    }
}
