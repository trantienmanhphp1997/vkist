<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute phải được chấp nhận.',
    'active_url' => ':attribute không phải là một URL hợp lệ.',
    'after' => ':attribute phải sau :date.',
    'after_or_equal' => ':attribute phải là một ngày sau hoặc bằng :date.',
    'alpha' => ':attribute phải là chữ cái.',
    'alpha_dash' => ':attribute chỉ có thể chứa các chữ cái, số, dấu gạch ngang và dấu gạch dưới.',
    'alpha_num' => ':attribute chỉ có thể chứa các chữ cái và số.',
    'array' => ':attribute phải là mảng.',
    'before' => ':attribute phải là một ngày trước :date.',
    'before_or_equal' => ':attribute phải là một ngày trước hoặc bằng :date.',
    'between' => [
        'numeric' => ':attribute phải ở giữa :min và :max.',
        'file' => ':attribute phải ở giữa :min và :max kilobytes.',
        'string' => ':attribute phải ở giữa :min and :max kí tự.',
        'array' => ':attribute phải ở giữa :min và :max phần tử.',
    ],
    'boolean' => ':attribute trường phải đúng hoặc sai.',
    'confirmed' => ':attribute xác nhận không đúng.',
    'date' => ':attribute không phải là ngày hợp lệ.',
    'date_equals' => ':attribute phải là một ngày bằng :date.',
    'date_format' => ':attribute không đúng định dạng :format.',
    'different' => ':attribute và :other phải khác nhau.',
    'digits' => ':attribute cần phải :digits chữ số.',
    'digits_between' => ':attribute phải ở giữa :min và :max chữ số.',
    'dimensions' => ':attribute có kích thước hình ảnh không hợp lệ.',
    'distinct' => ':attribute trường có giá trị trùng lặp.',
    'email' => ':attribute không đúng định dạng',
    'ends_with' => ':attribute phải kết thúc bằng một trong những điều sau: :values.',
    'exists' => ':attribute không tồn tại.',
    'file' => ':attribute phải là một tập tin.',
    'filled' => ':attribute trường phải có một giá trị.',
    'gt' => [
        'numeric' => ':attribute phải lớn hơn :value.',
        'file' => ':attribute phải lớn hơn :value kilobytes.',
        'string' => ':attribute phải lớn hơn :value characters.',
        'array' => ':attribute phải có nhiều hơn :value phần tử.',
    ],
    'gte' => [
        'numeric' => ':attribute phải lớn hơn or equal :value.',
        'file' => ':attribute phải lớn hơn or equal :value kilobytes.',
        'string' => ':attribute phải lớn hơn or equal :value characters.',
        'array' => ':attribute must have :value items or more.',
    ],
    'image' => ':attribute phải là một hình ảnh.',
    'in' => ':attribute Không hợp lệ.',
    'in_array' => ':attribute không tồn tại trong :other.',
    'integer' => ':attribute phải là số nguyên.',
    'ip' => ':attribute phải là một địa chỉ IP hợp lệ.',
    'ipv4' => ':attribute phải là địa chỉ IPv4 hợp lệ.',
    'ipv6' => ':attribute phải là địa chỉ IPv6 hợp lệ.',
    'json' => ':attribute phải là một chuỗi JSON hợp lệ.',
    'lt' => [
        'numeric' => ':attribute phải nhỏ hơn :value.',
        'file' => ':attribute phải nhỏ hơn :value kilobytes.',
        'string' => ':attribute phải nhỏ hơn :value kí tự.',
        'array' => ':attribute phải có ít hơn :value phần tử.',
    ],
    'lte' => [
        'numeric' => ':attribute phải nhỏ hơn hoặc bằng :value.',
        'file' => ':attribute phải nhỏ hơn hoặc bằng :value kilobytes.',
        'string' => ':attribute phải nhỏ hơn hoặc bằng :value characters.',
        'array' => ':attribute không được có nhiều hơn :value items.',
    ],
    'max' => [
        'numeric' => ':attribute nhỏ hơn :max.',
        'file' => ':attribute nhỏ hơn :max kilobytes.',
        'string' => ':attribute không được nhiều hơn :max kí tự (kể cả dấu phẩy).',
        'array' => ':attribute không được nhiều hơn :max phần tử (kể cả dấu phẩy).',
    ],
    'mimes' => ':attribute phải là tệp: :values.',
    'mimetypes' => ':attribute phải là tệp: :values.',
    'min' => [
        'numeric' => ':attribute ít nhất phải là :min.',
        'file' => ':attribute ít nhất phải là :min kilobytes.',
        'string' => ':attribute phải có ít nhất :min ký tự.',
        'array' => ':attribute phải có ít nhất :min phần tử.',
    ],
    'not_in' => ':attribute đã chọn không hợp lệ.',
    'not_regex' => ':attribute không hợp lệ.',
    'numeric' => ':attribute phải là một số.',
    'password' => 'Mật khẩu không đúng.',
    'present' => ':attribute lĩnh vực phải có mặt.',
    'regex' => ':attribute không hợp lệ.',
    'required' => ':attribute không được bỏ trống',
    'required_if' => ':attribute trường được yêu cầu khi :other là :value.',
    'required_unless' => ':attribute trường là bắt buộc trừ khi :other trong :values.',
    'required_with' => ':attribute bắt buộc khi có giá trị :values.',
    'required_with_all' => ':attribute trường được yêu cầu khi :values có mặt.',
    'required_without' => ':attribute trường được yêu cầu khi :values không có mặt.',
    'required_without_all' => ':attribute trường là bắt buộc khi không có :values có mặt.',
    'same' => ':attribute và :other phải giống nhau.',
    'size' => [
        'numeric' => ':attribute phải có kích thước :size.',
        'file' => ':attribute phải có kích thước :size kilobytes.',
        'string' => ':attribute phải có :size kí tự.',
        'array' => ':attribute phải chứa :size phần tử.',
    ],
    'starts_with' => ':attribute phải bắt đầu bằng một trong những điều sau: :values.',
    'string' => ':attribute phải là một chuỗi.',
    'timezone' => ':attribute phải là một khu vực hợp lệ.',
    'unique' => ':attribute đã tồn tại',
    'uploaded' => ':attribute không tải lên được.',
    'url' => ':attribute định dạng không hợp lệ.',
    'uuid' => ':attribute phải là một UUID hợp lệ.',
    'number_of_file' => [
        'min' => ':attribute có ít nhất :min',
        'max' => 'Nhiều nhất là :max hoặc bạn có thể nén file trước khi upload',
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'name' => [
//            'required' => 'Tên là thông tin bắt buộc',
            'maxlength' => 'Tên quá dài',
            'unique' => 'Tên đã tồn tại'
        ],
        'display_code' => [
            'required' => 'Mã danh mục thiết bị là thông tin bắt buộc',
            'maxlength' => 'Mã danh mục thiết bị quá dài',
            'unique' => 'Mã danh mục đã tồn tại'
        ],
        'ideaDeadline' => [
            'after' => 'Hạn nộp ý tưởng phải sau ngày hiện tại'
        ],
        'topicDeadline' => [
            'after' => 'Hạn nộp đề tài phải sau ngày hiện tại'
        ],
        'deadline' => [
            'after' => 'Hạn nộp phải sau ngày hiện tại'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [

    ],

];
