@extends('layouts.master')
<title>{{__('menu_management.menu_name.topic_type_management')}}</title>
@section('homeLeft')
    @include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
@livewire('admin.research.category.category-topic-list')
@endsection