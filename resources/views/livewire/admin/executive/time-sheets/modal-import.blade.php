<div wire:ignore.self class="modal fade modal-fix" id="import" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 444px !important">
        <div class="modal-content">
            <div class="modal-header">
                <h6>
                    @if(!empty($idShow))
                        {{__('data_field_name.import-time-sheets.reload')}}
                    @else
                        {{__('data_field_name.import-time-sheets.title-add')}}
                    @endif
                </h6>
            </div>
            <div class="modal-body" style="font-size: unset;">
                <form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.import-time-sheets.choose-time')}}<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <select wire:model.lazy="month" class="form-control" @if($disableSave || $disableInput) disabled @endif>
                                        <option value=''>{{__('data_field_name.import-time-sheets.month_placeholder')}}</option>
                                        @for($i = 1; $i < 13; $i++)
                                            <option value="{{$i}}">{{App\Enums\EMonth::valueToName($i)}}</option>
                                        @endfor
                                    </select>
                                    @error('month')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request mb-0">
                                    <select wire:model.lazy="year" class="form-control" @if($disableSave || $disableInput) disabled @endif>
                                        <option value=''>{{__('data_field_name.import-time-sheets.year_placeholder')}}</option>
                                        @for($i = 1990; $i < now()->year + 100; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                @error('year')
                                    <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.import-time-sheets.name')}}<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" placeholder="{{__('data_field_name.import-time-sheets.name')}}" wire:model.defer="name" class="form-control" style="border-radius: 12px">
                                        <div class="input-group-append">
                                            <form wire:submit.prevent="submit">
                                                <div class="form-group info-request mb-0">
                                                    <input type="file"
                                                           class="custom-file-input"
                                                           wire:model.lazy="file" id="input-file">
                                                    <label for="input-file" class="attachfile-field m-auto"><span><img src="/images/Clip-blue.svg"></span></label>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    @error('name')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div class="ml-5">
                                    <a href="#" wire:click="downloadFileExport">{{__('executive/import-time-sheets.template-time-sheet')}}</a>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    @if(!empty($file))
                                        <div class="form-group col-md-12">
                                            <div class="attached-files mt-2">
                                                <img src="/images/File.svg">
                                                <div class="content-file">
                                                    <p class="kb" style="word-break: break-all;">{{ $file->getClientOriginalName() }}</p>
                                                </div>
                                                <span>
                                                <button wire:click.prevent="deleteFile"
                                                        style="border: none;background: white" type="button"><img
                                                        src="/images/close.svg" alt="close"/>
                                                </button>
                                            </span>
                                            </div>
                                        </div>
                                    @endif
                                    @error('file')
                                        <div class="text-danger mt-1" style="margin-left: 13px">{{$message}}</div>
                                    @enderror
                                    @if(!empty($fileErrorMessage) && count($fileErrorMessage) > 0)
                                        @foreach($fileErrorMessage as $item)
                                            <div class="text-danger mt-1" style="margin-left: 13px">{{$item['error']}}</div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                @if($disableSave)
                    <button id='btn-save-disable' type="button" class="btn btn-primary" disabled="">{{__('common.button.save')}}</button>
                @else
                    <button id='btn-save' type="button" class="btn btn-primary" wire:click.prevent="saveData">{{__('common.button.save')}}</button>
                @endif
            </div>
        </div>
    </div>
</div>

@if($showResult)
    <div wire:ignore.self class="modal fade modal-fix" id="result" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 444px !important">
            <div class="modal-content">
                <div class="modal-header">
                    <h6>{{__('executive/import-salary.result')}}</h6>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 pd-col">
                                    @if($fileSuccessMessage)
                                    <div class="text-success">
                                        {{$fileSuccessMessage}}
                                    </div>
                                    <div class="text-danger">
                                        {{ __('server_validation.import-time-sheets.msg.error', ['value' => $numItemData - $numItemSaved])}}
                                    </div>
                                    @endif
                                    @if(!empty($fileErrorMessage) && count($fileErrorMessage) > 0)
                                        <a href="#" class="mt-1" wire:click="downloadErrorFile">{{__('executive/import-time-sheets.download_error_list_file')}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
@endif
