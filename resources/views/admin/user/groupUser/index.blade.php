@extends('layouts.master')
@section('title')
    <title>{{__('user/group_user.title.list')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    <livewire:admin.user.group-user.index />
@endsection
@section('js')
@endsection
