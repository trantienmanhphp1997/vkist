<div class=" tab-pane" id="insurance" wire:ignore.self>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.warranty_info')}}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.warranty_time')}}</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" class="form-control asset-type-date" placeholder=""
                                   value="{{old('warranty_date')}}"
                                   wire:model.lazy="warranty_date">
                            @error('warranty_date') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-md-6">
                            <select class="form-control form-control-lg" wire:model.lazy="warranty_type_date">
                                @foreach($assetTypeDate as $key => $val)
                                    <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.warranty_expiry_date')}}</label>
                    @include('layouts.partials.input._inputDate', ['input_id' => 'expiry_warranty_date', 'place_holder'=>'dd/mm/yyyy'])

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.warranty_condition')}}</label>
                    <textarea class="form-control" rows="4" wire:model.lazy="condition_warranty"
                              value="{{old('condition_warranty')}}">
                    </textarea>
                    @error('condition_warranty') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
        </div>
    </div>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.maintenance_info')}}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.maintenance_date')}}</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" class="form-control asset-type-date" placeholder=""
                                   value="{{old('maintenance_date')}}"
                                   wire:model.lazy="maintenance_date">
                            @error('maintenance_date') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-md-6">
                            <select class="form-control form-control-lg" wire:model.lazy="maintenance_type_date">
                                @foreach($assetTypeDate as $key => $val)
                                    <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.maintenance_start_date')}}</label>
                    @include('layouts.partials.input._inputDate', ['input_id' => 'maintenance_start_date', 'place_holder'=>'dd/mm/yyyy'])
                </div>
            </div>
        </div>
    </div>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.implement')}}</h3>
        <div class="row">
            <div class="col-sm-12 form-group">
                <input type="checkbox" name="achievement_paper_check" id="a-1" wire:model.lazy="status" value="1">
                <label  for="a-1">{{__('data_field_name.asset.implement')}}</label>
            </div>
            <div class="col-md-6">
                <div class="form-group relative-div" wire:key="{{ $status }}">  {{-- wire:key=> bắt buộc phải chạy render,check giá trị thay đổi trong wire:key để render --}}
                    <label>{{__('data_field_name.asset.implement_date')}}</label>
                    @include('layouts.partials.input._inputDate', ['input_id' => 'implementation_date',
                     'place_holder'=>'dd/mm/yyyy','default_date' => is_null($implementation_date) ? date('Y-m-d') : $implementation_date
                     ,'disable_input' => $status != 1])
                </div>

                <div class="form-group">
                    <label>{{__('data_field_name.asset.users')}}</label>
                    <select class="form-control form-control-lg" wire:model.lazy="user_info_id">
                        <option>{{__('data_field_name.asset.choose_users')}}</option>
                        @foreach($user as $key => $val)
                            <option value="{{$val->id}}">{{$val->fullname}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group info-request">
                    <label>{{__('data_field_name.asset.asset_quantity')}}
                        <span class="text-danger">*</span></label>
                    <input type="number"  class="num" maxlength="3"
                           @if ($type_manage==\App\Enums\EAssetCategoryTypeManage::CODE)
                           disabled
                            @endif
                           wire:model.lazy="implementation_quantity"
                    >
                    @error('implementation_quantity') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.department_id')}}</label>
                    <input type="text" class="form-control"
                           value="{{$data_department?$data_department->department->name:''}}" disabled>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.position_id')}}</label>
                    <input type="text" class="form-control"
                           value="{{$data_department?$data_department->position->v_value:''}}" disabled>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.number_report')}}<span class="text-danger">*</span></label>
                    <input type="text" class="form-control"
                           wire:model.lazy="number_report">
                    @error('number_report') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
        </div>
    </div>

    <script>
        window.addEventListener('render-event', function() {
            initDatePicker("#implementation_date");
        })
    </script>
</div>

