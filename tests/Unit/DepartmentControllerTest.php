<?php

namespace Tests\Unit;

use App\Http\Livewire\Admin\User\Department\DepartmentList;
use App\Models\User;
use App\Models\Department;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use PhpParser\Node\Expr\FuncCall;
use Tests\TestCase;

class DepartmentControllerTest extends TestCase
{

    use WithFaker;

    public $routeIndex = 'admin.department.index';

    /** test */
    public function testDepartmentIndex()
    {
        $this->actingAsSuperAdmin()->get(route($this->routeIndex))->assertOk();
    }

    /** test */
    public function testDepartmentCreateSuccess()
    {
        $this->actingAsSuperAdmin()->get(route($this->routeIndex))->assertOk();

        $input = [
            'name' => $this->faker->colorName,
            'note' => 'This is department note',
            'status' => 0,
            'parent_id' => null,
        ];

        Livewire::test(DepartmentList::class)
            ->fill($input)
            ->call('store')
            ->assertHasNoErrors();
    }

    public function testDepartmentCreateFail()
    {
        $this->actingAsSuperAdmin()->get(route($this->routeIndex))->assertOk();

        $input = [
            'name' => '',
            'note' => '',
            'status' => 0,
            'parent_id' => null,
        ];

        Livewire::test(DepartmentList::class)
            ->fill($input)
            ->call('store')
            ->assertHasErrors();
    }
}
