<?php

namespace App\Http\Livewire\Admin\Budget;

use App\Enums\EBudgetStatus;
use App\Enums\ESystemConfigType;
use App\Models\SystemConfig;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use App\Models\Budget;
use App\Models\ResearchProject;
use App\Models\TopicFee;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Log;
use DB;
use Excel;
use Illuminate\http\Feedback;

class BudgetList extends BaseLive
{
    public $searchStatus;
    public $deleteBudget;
    public $getId = [];
    public $ids = [];
    public $getIdFile;
    public $budgetStatus;
    public $researchProject;
    public $searchProject;
    public $value_date;
    public $now;
    public $date_create;


    protected $listeners=['deleteBudget'];

    public function mount() {
        $this->budgetStatus = EBudgetStatus::getList();

        $config = SystemConfig::query()->where('name','Import salary config')
            ->where('type', ESystemConfigType::TYPE_IMPORT_SALARY)->first();

        $this->date_create = $config->content['date_create_budget_plan']['value'];

        if ($this->date_create != null){
            $this->value_date = explode(",", $this->date_create);
        }

        $this->now =Carbon::now()->day;

    }

    public function render()
    {
        $this->researchProject = ResearchProject::query()->join('topic', 'topic.contract_code', '=', 'research_project.contract_code')->join('topic_fee', 'topic.id', '=', 'topic_fee.topic_id')->join('budget', 'budget.estimated_code', '=', 'topic_fee.code')->whereNull('budget.deleted_at')->whereNull('topic.deleted_at')->whereNull('topic_fee.deleted_at')->select('research_project.name as name', 'topic_fee.code as code')->get();
        $query = Budget::query();

        $file = \App\Models\File::whereIn('id', $this->ids)->get();

        if(strlen($this->searchTerm)){
            $query->where('budget.unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');

        }
        if ($this->searchStatus) {
            $query->where('budget.status', '=', $this->searchStatus);

        }

        if ($this->searchProject) {
            $query->where('budget.estimated_code', '=', $this->searchProject);

        }


        $data = $query->orderBy('budget.id', 'DESC')->paginate(25);
        Log::info("render" . count($data));
        return view('livewire.admin.budget.budget-list', ['data' => $data, 'file' => $file]);
    }

    public  function  deleteId($id){
        $this->deleteBudget = $id;
    }

    public function delete()
    {

        Budget::where('id', $this->deleteBudget)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );

    }

    public function listFile($id)
    {
        $files = \App\Models\File::where('model_id', $id)->where('model_name', Budget::class)->where('type', Config::get('common.type_upload.Budget'))->get();
        $id = [];
        foreach ($files as $file) {
            $id[] = $file->id;
        }
        $this->ids = $id;
    }

    public function download($id)
    {

            $file = \App\Models\File::where('id', $id)->get();

        foreach ($file as $files) {
            $link_file = storage_path('app/' . $files->url);

        }
        return response()->download($link_file);

    }
}
