<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIntoBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget', function (Blueprint $table) {
            $table->foreignId('department_id')->nullable()->comment('map voi phong ban')->constrained('department');
            $table->foreignId('research_project_id')->nullable()->comment('map voi du an nghien cuu')->constrained('research_project');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget', function (Blueprint $table) {
            $table->dropColumn('department_id');
            $table->dropColumn('research_project_id');
        });
    }
}
