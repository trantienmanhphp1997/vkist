<?php

namespace App\Http\Livewire\Admin\Research\Plan;

use App\Service\Community;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use App\Models\ResearchPlan;
use App\Models\Topic;
use App\Models\Task;
use App\Models\TopicHasUserInfo;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use App\Enums\EResearchPlan;
use App\Enums\ETopicStatus;
use App\Models\TaskHasUserInfo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PlanInfoList extends BaseLive
{
    public ?ResearchPlan $researchPlan;
    public ?Topic $selectedTopic;
    public $name;
    public $start_date;
    public $end_date;
    public $start_date_2;
    public $end_date_2;
    public $user_info_id;
    public $parent_id;
    public $selectedTopicId;
    public $research_plan_id;
    public $currentParentTask;
    public $idMainMission;
    public $isEdit = false;
    public $isEditChildren = false;
    public $idMainMissionEditing;
    public $idChildrenMissionEditing;

    public $userInfoIds = [];
    public $advancedSelectKey;
    public $members = [];

    protected $listeners = [
        'set-start_date' => 'setStartDate',
        'set-end_date' => 'setEndDate',
        'set-start_date_2' => 'setStartDate2',
        'set-end_date_2' => 'setEndDate2',
        'changed-user-infos-event' => 'setUserInfoIds'
    ];

    public function mount($researchPlan)
    {
        $this->researchPlan = $researchPlan;
        $this->advancedSelectKey = Str::random(5);
    }

    public function setUserInfoIds($data)
    {
        $this->userInfoIds = array_keys($data);
    }

    public function setStartDate($data)
    {
        $this->start_date = date('Y-m-d', strtotime($data['start_date']));
    }

    public function setEndDate($data)
    {
        $this->end_date = date('Y-m-d', strtotime($data['end_date']));
    }

    public function setStartDate2($data)
    {
        $this->start_date_2 = date('Y-m-d', strtotime($data['start_date_2']));
    }

    public function setEndDate2($data)
    {
        $this->end_date_2 = date('Y-m-d', strtotime($data['end_date_2']));
    }

    public function render()
    {
        $parentTasks = Task::where('research_plan_id', $this->researchPlan->id)
            ->whereNull('parent_id')
            ->with(['userInfos', 'childrenTask.userInfos'])
            ->get();
        $this->selectedTopicId = $this->researchPlan->topic_id;

        $this->selectedTopic = Topic::findOrFail($this->selectedTopicId);
        $this->members = $this->selectedTopic->members()->get();
        $this->dispatchBrowserEvent('render-plan-info-event');
        return view('livewire.admin.research.plan.plan-info-list', ['parentTasks' => $parentTasks]);
    }

    public function store_research_mission()
    {

        if ($this->isEdit) {
            $taskmission = Task::findorFail($this->idMainMissionEditing);
        } else {
            $taskmission = new Task;
        }
        $this->validate([
            'name' => 'required|max:50',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'userInfoIds' => 'required|array',
        ], [], [

            'name' => __('data_field_name.research_plan.mission_name'),
            'start_date' => __('data_field_name.research_plan.start_time'),
            'end_date' => __('data_field_name.research_plan.end_time'),
            'userInfoIds' => __('data_field_name.research_plan.created_at')
        ]);
        $taskmission->name = $this->name;
        $taskmission->start_date = $this->start_date;
        $taskmission->end_date = $this->end_date;
        // $taskmission->user_info_id = $this->user_info_id;
        $taskmission->research_plan_id = $this->researchPlan->id;
        $taskmission->topic_id = $this->selectedTopicId;
        $taskmission->save();

        $this->updateTaskHasUserInfo($taskmission, $this->userInfoIds);

        $this->isEdit = false;
        $this->idMainMission = $taskmission->id;
        $this->emit('close-modal-create-parent_mission');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')]);
    }

    public function editMainMission($id)
    {
        $this->resetInputFields();

        $mainMission = Task::findorFail($id);
        $this->name = $mainMission->name;
        $this->start_date = date('Y-m-d', strtotime($mainMission->start_date));
        $this->end_date = date('Y-m-d', strtotime($mainMission->end_date));
        $this->dispatchBrowserEvent('setDateForDatePicker', ["id" => "start_date", "value" => $this->start_date]);
        $this->dispatchBrowserEvent('setDateForDatePicker', ["id" => "end_date", "value" => $this->end_date]);
        $this->selectedTopicId = $mainMission->topic_id;
        // $this->user_info_id = $mainMission->user_info_id;
        $this->userInfoIds = $mainMission->userInfos->pluck('id')->all();
        $this->selectedTopic = Topic::findOrFail($this->selectedTopicId);
        // $this->members =  $this->selectedTopic->members()->with('userInfo')->get();
        $this->isEdit = true;
        $this->idMainMissionEditing = $mainMission->id;
    }

    public function editChildrenMission($id)
    {
        $this->resetInputFields();

        $this->isEditChildren = true;
        $childrenMission = Task::findorFail($id);
        $this->name = $childrenMission->name;
        $this->start_date_2 = date('Y-m-d', strtotime($childrenMission->start_date));
        $this->end_date_2 = date('Y-m-d', strtotime($childrenMission->end_date));
        $this->dispatchBrowserEvent('setDateForDatePicker', ["id" => "start_date_2", "value" => $this->start_date_2]);
        $this->dispatchBrowserEvent('setDateForDatePicker', ["id" => "end_date_2", "value" => $this->end_date_2]);
        $this->selectedTopicId = $childrenMission->topic_id;
        // $this->user_info_id = $childrenMission->user_info_id;
        $this->userInfoIds = $childrenMission->userInfos->pluck('id')->all();
        $this->isEditChildren = true;
        $this->idChildrenMissionEditing = $childrenMission->id;
        //Get created_at
        $this->selectedTopic = Topic::findOrFail($this->selectedTopicId);
        // $this->members = $this->selectedTopic->members()->with('userInfo')->get();
        // get info parent task
        $this->currentParentTask = $childrenMission->parentTask;
        $this->parent_id = $this->currentParentTask->id;
    }

    public function resetInputFields()
    {
        $this->selectedTopicId = "";
        $this->name = "";
        $this->start_date = "";
        $this->end_date = "";
        $this->user_info_id = "";
        $this->isEdit = false;
        $this->userInfoIds = [];
        $this->advancedSelectKey = Str::random(5);
        $this->resetValidation();
    }

    public function edit($id)
    {
        $this->isEditChildren = false;
        $this->currentParentTask = Task::findorFail($id);
        // $this->user_info_id = $this->currentParentTask->user_info_id;
        $this->userInfoIds = $this->currentParentTask->userInfos->pluck('id')->all();
        $this->name = $this->currentParentTask->name;
        $this->start_date = $this->currentParentTask->start_date;
        $this->end_date = $this->currentParentTask->end_date;
        $this->parent_id = $this->currentParentTask->id;
        $this->resetInputFields();
    }

    public function store_research_children_mission()
    {

        if ($this->isEditChildren) {
            $childrenMission = Task::findorFail($this->idChildrenMissionEditing);
        } else {
            $childrenMission = new Task;
        }
        $this->validate([
            'name' => 'required',
            'start_date_2' => 'required|date|after_or_equal:' . date('d-m-Y', strtotime($this->currentParentTask->start_date)) . '|before_or_equal:' . date('d-m-Y', strtotime($this->currentParentTask->end_date)),
            'end_date_2' => 'required|date|before_or_equal:' . date('d-m-Y', strtotime($this->currentParentTask->end_date)) . '|after_or_equal:' . date('d-m-Y', strtotime($this->currentParentTask->start_date)) . '|after:start_date_2',
            'userInfoIds' => 'required|array',
        ], [], [

            'name' => __('data_field_name.research_plan.mission_name'),
            'start_date_2' => __('data_field_name.research_plan.start_time'),
            'end_date_2' => __('data_field_name.research_plan.end_time'),
            'userInfoIds' => __('data_field_name.research_plan.created_at')
        ]);
        $childrenMission->name = $this->name;
        $childrenMission->start_date = $this->start_date_2;
        $childrenMission->end_date = $this->end_date_2;
        // $childrenMission->user_info_id = $this->user_info_id;
        $childrenMission->parent_id = $this->parent_id;
        $childrenMission->topic_id = $this->selectedTopicId;
        $this->isEditChildren = false;
        $childrenMission->save();

        $this->updateTaskHasUserInfo($childrenMission, $this->userInfoIds);

        $this->emit('close-modal-create-children_mission');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);
    }

    public function delete()
    {
        $childenMission = Task::findorFail($this->deleteId);
        $count = $childenMission->childrenTask()->count();
        if ($count > 0) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.delete_mission')]);
        } else {
            $childenMission->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
        }
    }

    public function export()
    {
        $today = date("d_m_Y");
        $this->parentTasks = Task::where('research_plan_id', $this->researchPlan->id)->whereNull('parent_id')->with([
            'childrenTask' => function ($query) {
                return $query->with('userInfo', 'userInfo.department');
            }
        ])->get();
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $section->addText($this->researchPlan->name);
        $section->addText($this->researchPlan->topic->name);
        $section->addText($this->researchPlan->topic->target);
        $section->addText($this->researchPlan->topic->overview);
        foreach ($this->parentTasks as $row) {
            $section->addText($row->name);
            $section->addText($this->researchPlan->topic->start_date);
            $section->addText($this->researchPlan->topic->end_date);
            $section->addText($row->user_info_id);
            foreach ($row->childrenTask as $row2) {
                $section->addText($row2->name);
                $section->addText($row2->start_date);
                $section->addText($row2->end_date);
                $section->addText($row2->user_info_id);
            }
        }
        $section->addText($this->researchPlan->topic->achievement_textbook_check);
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $filename = $this->researchPlan->name . $today . '.docx';
        $path = storage_path("app/uploads/" . $filename);
        $objWriter->save($path);
        return response()->download($path);
    }

    public function submitted()
    {

        $this->researchPlan->status = EResearchPlan::STATUS_SUBMIT;
        $this->researchPlan->save();
        session()->flash('success', __("notification.common.success.submit_plan"));
        return redirect()->route('admin.research.plan.research_plan.index');
    }

    public function approved()
    {
        $this->researchPlan->status = EResearchPlan::STATUS_WAIT_ASSESSMENT;
        $this->researchPlan->save();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.aprove')]);
    }

    public function waiting_appraisal()
    {
        $this->researchPlan->status = EResearchPlan::STATUS_WAIT_APPROVAL;
        $listTopicIdAllowed = Community::listTopicIdAllowed();
        if (!Auth::user()->hasRole('administrator') && !in_array($this->researchPlan->topic_id, $listTopicIdAllowed)) {
            abort(404);
        }
        $this->researchPlan->save();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.submit_approve')]);
    }

    public function searchStartTime($times)
    {
        $this->start_date = date('Y-m-d', strtotime($times));
    }

    public function searchEndTime($times)
    {
        $this->end_date = date('Y-m-d', strtotime($times));
    }

    public function searchStartTime2($times)
    {
        $this->start_date_2 = date('Y-m-d', strtotime($times));
    }

    public function searchEndTime2($times)
    {
        $this->end_date_2 = date('Y-m-d', strtotime($times));
    }

    protected function updateTaskHasUserInfo($task, $userInfoIds)
    {
        TaskHasUserInfo::query()->where('task_id', $task->id)->delete();
        $task->userInfos()->attach($userInfoIds);
    }
}
