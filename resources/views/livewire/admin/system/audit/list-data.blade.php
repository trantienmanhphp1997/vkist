<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
                <div  class="breadcrumbs"><a href="{{route('admin.system.audit.list')}}">{{__('data_field_name.system.audit.title')}}</a></div> 
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('data_field_name.system.audit.title')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block search-staff">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="form-control size13" placeholder="{{__('common.place_holder.search')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div>
                                <select wire:model.debounce.1000ms="userId" class="form-control form-control-lg size13">
                                    <option value=''>
                                        {{__('data_field_name.system.audit.performer')}}
                                    </option>
                                    @foreach($userList as $item)
                                        <option value="{{$item['id']}}">
                                            {{$item['name']}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 px-8">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'from_date','place_holder' => __('research/achievement.label.from'), 'set_null_when_enter_invalid_date' => true ])
                        </div>
                        <div class="col-md-2 px-8">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'to_date' ,'place_holder' => __('research/achievement.label.to'), 'set_null_when_enter_invalid_date' => true ])
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                                <tr class="border-radius">
                                    <th scope="col" class="text-center border-radius-left">
                                        {{__('data_field_name.system.audit.performer')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.event')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.audit_table_type')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.audit_table_id')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.old_values')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.new_values')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.created_at')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.url')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.ip_address')}}
                                    </th>
                                    <th scope="col" class="text-center">
                                        {{__('data_field_name.system.audit.user_agent')}}
                                    </th>
                                    <th scope="col" class="text-center border-radius-right">
                                        {{__('data_field_name.system.audit.note')}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $index => $row)
                                    <tr>
                                        <td class="text-left" title="{{$row['perfomer']}}">{!! boldTextSearch($row['perfomer'], $searchTerm) !!}</td>
                                        <td class="text-left" title="{{$row['event']}}">{!! boldTextSearch($row['event'], $searchTerm) !!}</td>
                                        <td class="text-left" title="{{$row['audittable_type']}}">{!! boldTextSearch($row['audittable_type'], $searchTerm) !!}</td>
                                        <td class="text-left" title="{{$row['audittable_id']}}">{!! boldTextSearch($row['audittable_id'], $searchTerm) !!}</td>
                                        <td class="text-left">
                                            @if(!empty($row['old_values']) && count($row['old_values']) > 0)
                                                @foreach($row['old_values'] as $index => $item)
                                                    <p class="mb-1 content-audit-data" title='{!!$index.": ".json_encode($item)!!}'><span class="font-weight-bold">{{$index}}</span>: {!! boldTextSearch(json_encode($item), $searchTerm) !!}</p>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td class="text-left">
                                            @if(!empty($row['new_values']) && count($row['new_values']) > 0)
                                                @foreach($row['new_values'] as $index => $item)
                                                    <p class="mb-1 content-audit-data" title='{{$index.": ".json_encode($item)}}'><span class="font-weight-bold">{{$index}}</span>: {!! boldTextSearch(json_encode($item), $searchTerm) !!}</p>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td class="text-left" title="{{$row['created_at']}}">{!! $row['created_at'] !!}</td>
                                        <td class="text-left" title="{{$row['url']}}">
                                            <p class="mb-1 content-audit-data">{!! boldTextSearch($row['url'], $searchTerm) !!}</p>
                                        </td>
                                        <td class="text-left" title="{{$row['ip_address']}}">{!! boldTextSearch($row['ip_address'], $searchTerm) !!}</td>
                                        <td class="text-left" title="{{$row['user_agent']}}">
                                            <p class="mb-1 content-audit-data">{!! boldTextSearch($row['user_agent'], $searchTerm) !!}</p>
                                        </td>
                                        <td class="text-left" title="{{$row['note']}}">{!! boldTextSearch($row['note'], $searchTerm) !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center">
                            <span>{{__('common.message.empty_search')}}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>