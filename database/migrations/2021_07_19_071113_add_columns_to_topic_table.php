<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic', function (Blueprint $table) {
            $table->tinyInteger('source')->unsigned()->nullable()->comment('Nguồn đề tài: 0=Từ ý tưởng, 1=Từ đề xuất, 2=Từ chỉ đạo');
            $table->string('necessary', 1500)->nullable()->comment('Tính cấp thiết');
            $table->string('space_scope', 1500)->nullable()->comment('Phạm vi không gian');
            $table->bigInteger('expected_fee')->unsigned()->nullable()->comment('Kinh phí dự kiến');
            $table->date('submit_date')->nullable()->comment('Ngày nộp');
            $table->foreignId('parent_id')->nullable()->comment('Đề tài gốc')->constrained('topic');

            $table->dropColumn('source_id');
            $table->dropColumn('urgency_note');
            $table->dropColumn('range_use');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic', function (Blueprint $table) {
            $table->tinyInteger('source_id')->nullable();
            $table->string('urgency_note', 1500)->nullable()->comment('Tinh cap thiet');
            $table->string('range_use', 255)->nullable()->comment('Pham vi khong tian');

            $table->dropColumn('source');
            $table->dropColumn('necessary');
            $table->dropColumn('space_scope');
            $table->dropColumn('expected_fee');
            $table->dropColumn('submit_date');
            $table->dropColumn('parent_id');
        });
    }
}
