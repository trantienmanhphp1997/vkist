<?php

namespace App\Http\Livewire\Admin\User;

use App\Http\Livewire\Base\BaseLive;
use DB;
class UserAcademic extends BaseLive
{
    public $user_info_id;
    public function render()
    {
        return view('livewire.admin.user.user-academic');
    }
    public function openModal(){
        $this->emit('show');
    }
}
