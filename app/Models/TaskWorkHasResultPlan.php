<?php

namespace App\Models;

use App\Http\Livewire\Admin\General\WorkPlan\UserInfo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Facades\Config;

class TaskWorkHasResultPlan extends Eloquent
{
    use HasFactory;
    // use \OwenIt\Auditing\Auditable;
    protected $table = "task_work_has_result_plan";
    protected $guarded=['*'];
    protected $fillable = [
        'name','task_work_detail_id','admin_id'
    ];
    public function files() {
        return $this->hasMany(File::class,'model_id')->where('files.type','=',Config::get('common.type_upload.TaskWorkHasResult'));
    }

    
}
