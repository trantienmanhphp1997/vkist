<?php
return [
	'template-salary-basic' => 'File mẫu import lương cơ bản',
	'template-salary-position' => 'File mẫu import lương chức danh',
    'title' => 'Dữ liệu lương',
    'btn-add' => 'Thêm dữ liệu',
    'result' => 'Kết quả',
    'table_column' => [
        'name' => 'Tên bảng lương',
        'duration' => 'Thời gian',
        'type' => 'Loại bảng lương'
    ],
];