<?php

namespace App\Http\Livewire\Admin\Research\Report;

use App\Enums\EAssetSituation;
use App\Enums\EAssetStatus;
use App\Exports\AssetCategoryExport;
use App\Exports\AssetReportIncreaseDecreaseExport;
use App\Exports\AssetReportMovement;
use App\Exports\AssetReportSumary;
use App\Exports\AssetReportSumaryDepartment;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use App\Models\AssetAllocatedRevoke;
use App\Models\AssetCategory;
use App\Models\AssetMaintenance;
use App\Models\Department;
use Carbon\Carbon;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class Index extends BaseLive
{
    public $reportNumber = 1;
    public $department_id;
    public $start_date;
    public $end_date;

    public function mount()
    {
        $this->start_date = date('d/m/Y');
        $this->end_date = date('d/m/Y');
    }

    protected $listeners = [
        'set-start-time' => 'setStartTime',
        'set-end-time' => 'setEndTime'
    ];

    public function render()
    {
        $departments = Department::all();
        if (Carbon::createFromFormat(config('common.formatDate'), $this->start_date)->format('Y-m-d') > Carbon::createFromFormat(config('common.formatDate'), $this->end_date)->format('Y-m-d')) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('research/report.error-search-date')]);
        }
        $assetSumaryReport = $this->getAssetSumary($this->department_id);
        $assetSumaryStatus = $this->getAssetSumaryStatus($this->department_id, $this->start_date, $this->end_date);
        $assetMovement = $this->getAssetMovement($this->department_id, $this->start_date, $this->end_date);

        return view('livewire.admin.research.report.index', compact('assetSumaryReport', 'assetSumaryStatus', 'assetMovement', 'departments'));
    }

    public function getAssetMovement($department_id, $start_date, $end_date)
    {
        $query = AssetCategory::query()->with(['asset'])
            ->whereBetween('created_at', [Carbon::createFromFormat(config('common.formatDate'), $start_date)->format('Y-m-d'), Carbon::createFromFormat(config('common.formatDate'), date('d/m/Y'))->format('Y-m-d')]);
        if ($this->searchTerm != null) {
            $query->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%');
            $query->orWhereHas('asset', function ($query) {
                $query->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%');
            });
        }
        if ($department_id != null) {
            $query->orWhereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }

        $assetCategory = $query->orderBy('id', 'desc')->paginate($this->perPage);

        //tong cong tat ca cac trang thai
        $totalCategoryAssetAdded = 0;
        $totalCategoryAssetLost = 0;
        $totalCategoryAssetCancel = 0;
        $totalCategoryAssetLiquidation = 0;
        $totalCategoryAssetInUse = 0;
        $totalCategoryAssetRepaired = 0;
        $totalCategoryAssetMaintenance = 0;
        $totalCategoryAssetTransfer = 0;
        $totalCategoryAssetBeginQuantity = 0;
        $totalCategoryAssetEndQuanity = 0;

        foreach ($assetCategory as $category) {
            $countCategoryAssetAdded = 0;
            $countCategoryAssetLost = 0;
            $countCategoryAssetCancel = 0;
            $countCategoryAssetLiquidation = 0;
            $countCategoryAssetInUse = 0;
            $countCategoryAssetRepaired = 0;
            $countCategoryAssetMaintenanced = 0;
            $countCategoryAssetTransfer = 0;
            $countCategoryAssetBeginQuanity = 0;
            $countCategoryAssetEndQuanity = 0;

            foreach ($category->asset as $asset) {
                // tai san them moi , da mat ,da huy , da thanh ly
                $asset->added = $asset->quantity ?? 0;
                $countCategoryAssetAdded += $asset->added;
                $asset->losted = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::LOSTED])->count() ?? 0;
                $countCategoryAssetLost += $asset->losted;
                $asset->cancel = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::CANCELLED])->count() ?? 0;
                $countCategoryAssetCancel += $asset->cancel;
                $asset->liquidation = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::LIQUIDATION])->count() ?? 0;
                $countCategoryAssetLiquidation += $asset->liquidation;
                // da su dung , da sua chua , da bao duong
                $asset->amount_used = $asset->amount_used ?? 0;
                $countCategoryAssetInUse += $asset->amount_use ?? 0;
                $asset->inRepaired = AssetMaintenance::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::REPAIRED])->count() ?? 0;
                $countCategoryAssetRepaired += $asset->inRepaired ?? 0;
                $asset->inMaintenance = AssetMaintenance::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::MAINTENANCE])->count() ?? 0;
                $countCategoryAssetMaintenanced += $asset->inMaintenance ?? 0;
                $asset->transfer = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::TRANSFER])->count() ?? 0;
                $countCategoryAssetTransfer += $asset->transfer;
                $asset->endPeriodQuanity = $asset->quantity ?? 0;
                $countCategoryAssetEndQuanity += $asset->endPeriodQuanity;
                $asset->beginPeriodQuanity = $asset->added + $asset->losted + $asset->liquidation + $asset->cancel;
                $countCategoryAssetBeginQuanity += $asset->beginPeriodQuanity;
            }

            // tong so luong cac trang thai cua cac tai san thuoc the loai
            $category->countAssetAdded = $countCategoryAssetAdded;
            $totalCategoryAssetAdded += $countCategoryAssetAdded;
            $category->countAssetLosted = $countCategoryAssetLost;
            $totalCategoryAssetLost += $countCategoryAssetLost;
            $category->countAssetCancel = $countCategoryAssetCancel;
            $totalCategoryAssetCancel += $countCategoryAssetCancel;
            $category->countAssetLidation = $countCategoryAssetLiquidation;
            $totalCategoryAssetLiquidation += $countCategoryAssetLiquidation;
            $category->countAssetInUse = $countCategoryAssetInUse;
            $totalCategoryAssetInUse += $countCategoryAssetInUse;
            $category->countAssetRepaired = $countCategoryAssetRepaired;
            $totalCategoryAssetRepaired += $countCategoryAssetRepaired;
            $category->countAssetMaintenance = $countCategoryAssetMaintenanced;
            $totalCategoryAssetMaintenance += $countCategoryAssetMaintenanced;
            $category->countassetTransfer = $countCategoryAssetTransfer;
            $totalCategoryAssetTransfer += $countCategoryAssetTransfer;
            $category->countAssetBeginQuanity = $countCategoryAssetBeginQuanity;
            $totalCategoryAssetBeginQuantity += $countCategoryAssetBeginQuanity;
            $category->countAssetEndQuanity = $countCategoryAssetEndQuanity;
            $totalCategoryAssetEndQuanity += $countCategoryAssetEndQuanity;
        }

        //tong so luong cua toan bo tai san voi moi trang thai
        $assetCategory->totalAssetAdded = $totalCategoryAssetAdded;
        $assetCategory->totalAssetLost = $totalCategoryAssetLost;
        $assetCategory->totalAssetCancel = $totalCategoryAssetCancel;
        $assetCategory->totalAssetLiquidation = $totalCategoryAssetLiquidation;
        $assetCategory->totalAssetInUse = $totalCategoryAssetInUse;
        $assetCategory->totalAssetRepaired = $totalCategoryAssetRepaired;
        $assetCategory->totalAssetMaintenance = $totalCategoryAssetMaintenance;
        $assetCategory->totalAssetTransfer = $totalCategoryAssetTransfer;
        $assetCategory->totalAssetBeginQuanity = $totalCategoryAssetBeginQuantity;
        $assetCategory->totalAssetEndQuanity = $totalCategoryAssetEndQuanity;

        return $assetCategory;
    }

    public function getAssetSumaryStatus($department_id, $start_date, $end_date)
    {
        $query = Asset::query()->with(['maintenance', 'allocate']);
        if ($department_id != null) {
            $query->orWhereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }

        $assets = $query->paginate($this->perPage);

        $countAssetInRepaired = 0;
        $countAssetInMaintenance = 0;
        $countAssetReportLost = 0;
        $countAssetLost = 0;
        $countAssetCancel = 0;
        $countAssetReportLiquidation = 0;
        $countAssetLiquidation = 0;
        $countAssetRecall = 0;
        $countAssetTransfer = 0;
        $countAssetNotUse = 0;
        $countAssetUsed = 0;
        $countAmountOfAssets = 0;

        // lay so lieu cho moi trang thai cua tai san
        foreach ($assets as $asset) {
            // tong so luong cua toan bo trang thai
            $countAmountOfAsset = 0;
            $asset->inRepaired = AssetMaintenance::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::REPAIRED])->count() ?? 0;
            if ($asset->inRepaired != null) {
                $countAssetInRepaired += $asset->inRepaired;
            }
            $countAmountOfAsset += $asset->inRepaired ?? 0;
            $asset->inMaintenance = AssetMaintenance::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::MAINTENANCE])->count() ?? 0;
            if ($asset->inMaintenance != null) {
                $countAssetInMaintenance += $asset->inMaintenance;
            }
            $countAmountOfAsset += $asset->inMaintenance ?? 0;
            $asset->report_lost = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::REPORT_LOST])->count() ?? 0;
            if ($asset->report_lost != null) {
                $countAssetReportLost += $asset->report_lost;
            }
            $countAmountOfAsset += $asset->report_lost ?? 0;
            $asset->losted = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::LOSTED])->count() ?? 0;
            if ($asset->losted != null) {
                $countAssetLost += $asset->losted;
            }
            $countAmountOfAsset += $asset->losted ?? 0;
            $asset->cancel = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::CANCELLED])->count() ?? 0;
            if ($asset->cancel != null) {
                $countAssetCancel += $asset->cancel;
            }
            $countAmountOfAsset += $asset->cancel ?? 0;
            $asset->report_liquidation = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::REPORT_LIQUIDATION])->count() ?? 0;
            if ($asset->report_liquidation != null) {
                $countAssetReportLiquidation += $asset->report_liquidation;
            }
            $countAmountOfAsset += $asset->report_liquidation ?? 0;
            $asset->liquidation = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::LIQUIDATION])->count() ?? 0;
            if ($asset->liquidation != null) {
                $countAssetLiquidation += $asset->liquidation;
            }
            $countAmountOfAsset += $asset->liquidation ?? 0;
            $asset->recall = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::RECALL])->count() ?? 0;
            if ($asset->recall != null) {
                $countAssetRecall += $asset->recall;
            }
            $countAmountOfAsset += $asset->recall ?? 0;
            $asset->transfer = AssetAllocatedRevoke::query()->where(['asset_id' => $asset->id])->where(['status' => EAssetStatus::TRANSFER])->count() ?? 0;
            if ($asset->transfer != null) {
                $countAssetTransfer += $asset->transfer;
            }
            $countAmountOfAsset += $asset->transfer ?? 0;
            $asset->amount_used = $asset->amount_used != null ? $asset->amount_used : 0;
            $asset->notUse = ($asset->quantity - $asset->amount_used) ?? 0;
            if ($asset->amount_used != null) {
                $countAssetUsed += $asset->amount_used;
            }
            $countAmountOfAsset += $asset->amount_used ?? 0;
            $countAmountOfAsset += $asset->notUse ?? 0;
            if ($asset->notUse != null) {
                if ($asset->notUse < 0){
                    $asset->notUse = 0 ;
                    $countAssetNotUse += 0;
                }else{
                    $countAssetNotUse += $asset->notUse;
                }
            }
            $asset->amountStatus = $countAmountOfAsset ?? 0;

            $countAmountOfAssets += $countAmountOfAsset ?? 0;
        }


        //dem so luong cua moi trang thai
        $assets->countAssetInRepaired = $countAssetInRepaired;
        $assets->countAssetInMaintenance = $countAssetInMaintenance;
        $assets->countAssetReportLost = $countAssetReportLost;
        $assets->countAssetLost = $countAssetLost;
        $assets->countAssetCancel = $countAssetCancel;
        $assets->countAssetReportLiquidation = $countAssetReportLiquidation;
        $assets->countAssetLiquidation = $countAssetLiquidation;
        $assets->countAssetRecall = $countAssetRecall;
        $assets->countAssetTransfer = $countAssetTransfer;
        $assets->countAssetNotUse = $countAssetNotUse;
        $assets->countAssetUsed = $countAssetUsed;
        $assets->amountStatus = $countAmountOfAssets;

        return $assets;
    }

    public function getAssetSumary($department_id)
    {
        $data = [];
        $data['count_department'] = 0;
        // tai san : bao mat , bao huy , de nghi thanh ly , da huy , da mat , da thanh ly

        // tổng cộng số tài sản bị mất
        $query = Asset::query()->where(['status' => EAssetStatus::REPORT_LOST]);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportLost = $query->get()->count() ?? 0;

        //tên đơn vị có tài sản bị mất
        $query = Asset::query()->where(['status' => EAssetStatus::REPORT_LOST])->with(['department']);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetLostDepartmentName = $query->first()->department->name ?? '';
        $data['report_lost']['count'] = $countAssetReportLost;
        $data['report_lost']['department_name'] = $assetLostDepartmentName;

        // tổng cộng tài sản bị báo hủy
        $query = Asset::query()->where(['status' => EAssetStatus::REPORT_CANCEL]);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportCancel = $query->get()->count() ?? 0;

        //tên đơn vị có tài sản bị báo hủy
        $query = Asset::query()->where(['status' => EAssetStatus::REPORT_CANCEL])->with(['department']);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetCancelDepartmentName = $query->first()->department->name ?? '';
        $data['report_cancel']['count'] = $countAssetReportCancel;
        $data['report_cancel']['department_name'] = $assetCancelDepartmentName;

        //tổng cộng tài sản có báo cáo thanh lý
        $query = Asset::query()->where(['status' => EAssetStatus::REPORT_LIQUIDATION]);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportLiquidation = $query->get()->count() ?? 0;
        //tên đơn vị có tài sản bị báo thanh lý
        $query = Asset::query()->where(['status' => EAssetStatus::REPORT_LIQUIDATION])->with(['department']);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetLiquidationDepartmentName = $query->first()->department->name ?? '';
        $data['report_liquidation']['count'] = $countAssetReportLiquidation;
        $data['report_liquidation']['department_name'] = $assetLiquidationDepartmentName;

        //tổng cộng số tài sản bị mất
        $query = Asset::query()->where(['status' => EAssetStatus::LOSTED]);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportHasLost = $query->get()->count() ?? 0;
        //tên đơn vị có tài sản bị mất
        $query = Asset::query()->where(['status' => EAssetStatus::LOSTED])->with(['department']);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetHasLostDepartmentName = $query->first()->department->name ?? '';
        $data['report_asset_has_lost']['count'] = $countAssetReportHasLost;
        $data['report_asset_has_lost']['department_name'] = $assetHasLostDepartmentName;

        //tổng cộng tài sản bị hủy bỏ
        $query = Asset::query()->where(['status' => EAssetStatus::CANCELLED]);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportHasCancel = $query->get()->count() ?? 0;
        //tên đơn vị có tài sản bị hủy bỏ
        $query = Asset::query()->where(['status' => EAssetStatus::CANCELLED])->with(['department']);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetHasCancelDepartmentName = $query->first()->department->name ?? '';
        $data['report_asset_has_cancel']['count'] = $countAssetReportHasCancel;
        $data['report_asset_has_cancel']['department_name'] = $assetHasCancelDepartmentName;

        //tổng cộng tài sản bị thanh lý
        $query = Asset::query()->where(['status' => EAssetStatus::LIQUIDATION]);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportHasLiquidation = $query->get()->count() ?? 0;
        //tên đơn vị có tài sản bị thanh lý
        $query = Asset::query()->where(['status' => EAssetStatus::LIQUIDATION])->with(['department']);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetHasLiquidationDepartmentName = $query->first()->department->name ?? '';
        $data['report_asset_has_liquidation']['count'] = $countAssetReportHasLiquidation;
        $data['report_asset_has_liquidation']['department_name'] = $assetHasLiquidationDepartmentName;

        // tai san cho sua chua , cho bao duong , dang sua chua , dang bao duong
        //tổng công tài sản được báo cáo là sửa chữa
        $query = AssetMaintenance::query()->where(['status' => EAssetStatus::REPORT_REPAIR]);
        if ($department_id != null) {
            $query->whereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportWaitRepaired = $query->get()->count() ?? 0;
        //tên đơn vị có tài sản được báo cáo sửa chữa
        $query = AssetMaintenance::query()->where(['status' => EAssetStatus::REPORT_REPAIR])->with(['asset.department']);
        if ($department_id != null) {
            $query->whereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetWaitRepairedDepartmentName = $query->first()->asset->department->name ?? '';
        $data['report_asset_wait_repaired']['count'] = $countAssetReportWaitRepaired;
        $data['report_asset_wait_repaired']['department_name'] = $assetWaitRepairedDepartmentName;

        //tổng tài sản được báo cáo bảo trì
        $query = AssetMaintenance::query()->where(['status' => EAssetStatus::REPORT_MAINTENANCE]);
        if ($department_id != null) {
            $query->whereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportWaitMaintenance = $query->get()->count() ?? 0;
        //tên đơn vị có tài sản được báo cáo bảo trì
        $query = AssetMaintenance::query()->where(['status' => EAssetStatus::REPORT_MAINTENANCE])->with(['asset.department']);
        if ($department_id != null) {
            $query->whereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetWaitMaintenanceDepartmentName = $query->first()->asset->department->name ?? '';
        $data['report_asset_wait_maintenance']['count'] = $countAssetReportWaitMaintenance;
        $data['report_asset_wait_maintenance']['department_name'] = $assetWaitMaintenanceDepartmentName;

        //tổng cộng tài sản được sửa chữa
        $query = AssetMaintenance::query()->where(['status' => EAssetStatus::REPAIRED]);
        if ($department_id != null) {
            $query->whereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportHasRepaired = $query->get()->count() ?? 0;
        //tên đơn vị có tài sản được sửa chữa
        $query = AssetMaintenance::query()->where(['status' => EAssetStatus::REPAIRED])->with(['asset.department']);
        if ($department_id != null) {
            $query->whereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetHasRepairedDepartmentName = $query->first()->asset->department->name ?? '';
        $data['report_asset_has_repaired']['count'] = $countAssetReportHasRepaired;
        $data['report_asset_has_repaired']['department_name'] = $assetHasRepairedDepartmentName;

        //tổng cộng tài sản được bảo trì
        $query = AssetMaintenance::query()->where(['status' => EAssetStatus::MAINTENANCE]);
        if ($department_id != null) {
            $query->whereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportHasMaintenance = $query->get()->count() ?? 0;

        //tên đơn vị có tài sản được bảo trì
        $query = AssetMaintenance::query()->where(['status' => EAssetStatus::MAINTENANCE])->with(['asset.department']);
        if ($department_id != null) {
            $query->whereHas('asset.department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetHasMaintenanceDepartmentName = $query->first()->asset->department->name ?? '';
        $data['report_asset_has_maintenance']['count'] = $countAssetReportHasMaintenance;
        $data['report_asset_has_maintenance']['department_name'] = $assetHasMaintenanceDepartmentName;

        // tai san dang su dung hoac dang khong su dung
//        tổng cộng tài sản được sử dụng
        $query = Asset::query()->where(['situation' => EAssetSituation::SITUATION_USING]);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportInUsing = $query->get()->count() ?? 0;
        //tên tài sản đang được sử dụng
        $query = Asset::query()->where(['situation' => EAssetSituation::SITUATION_USING])->with(['department']);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetInUsingDepartmentName = $query->first()->department->name ?? '';
        $data['report_asset_in_using']['count'] = $countAssetReportInUsing;
        $data['report_asset_in_using']['department_name'] = $assetInUsingDepartmentName;
        //tổng cộng tài sản đang không được sử dụng
        $query = Asset::query()->where(['situation' => EAssetSituation::SITUATION_NOT_USE]);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $countAssetReportNotUse = $query->get()->count() ?? 0;
        //tên đơn vị có tài sản đang không được sử dụng
        $query = Asset::query()->where(['situation' => EAssetSituation::SITUATION_NOT_USE])->with(['department']);
        if ($department_id != null) {
            $query->whereHas('department', function ($query) use ($department_id) {
                $query->where(['id' => $department_id]);
            });
        }
        $assetNotUseDepartmentName = $query->first()->department->name ?? '';
        $data['report_asset_not_use']['count'] = $countAssetReportNotUse;
        $data['report_asset_not_use']['department_name'] = $assetNotUseDepartmentName;

        if ($data['report_asset_not_use']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_lost']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_cancel']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_liquidation']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_asset_has_lost']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_asset_has_cancel']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_asset_has_liquidation']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_asset_wait_repaired']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_asset_wait_maintenance']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_asset_has_repaired']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_asset_has_maintenance']['department_name']) {

            $data['count_department'] += 1;
        }
        if ($data['report_asset_in_using']['department_name']) {

            $data['count_department'] += 1;
        }
        // tong so luong
        $data['countAssetAmount'] = $data['report_lost']['count'] + $data['report_cancel']['count'] + $data['report_liquidation']['count'] +
            $data['report_asset_has_lost']['count'] + $data['report_asset_has_cancel']['count'] + $data['report_asset_has_liquidation']['count'] +
            $data['report_asset_wait_repaired']['count'] + $data['report_asset_wait_maintenance']['count'] + $data['report_asset_has_repaired']['count'] +
            $data['report_asset_has_maintenance']['count'] + $data['report_asset_in_using']['count'] + $data['report_asset_not_use']['count'];

        return $data;
    }

    public function exportData()
    {
        $today = date("d_m_Y");
        switch ($this->reportNumber) {
            case 1 :
                return Excel::download(new AssetReportIncreaseDecreaseExport($this->getAssetMovement($this->department_id, $this->start_date, $this->end_date)), 'asset-report-' . $today . '.xlsx');
            case 2 :
                return Excel::download(new AssetReportMovement($this->getAssetMovement($this->department_id, $this->start_date, $this->end_date)), 'asset-report-' . $today . '.xlsx');
            case 3 :
                return Excel::download(new AssetReportSumary($this->getAssetSumaryStatus($this->department_id, $this->start_date, $this->end_date)), 'asset-report-' . $today . '.xlsx');
            case 4 :
                return Excel::download(new AssetReportSumaryDepartment($this->getAssetSumary($this->department_id)), 'asset-report-' . $today . '.xlsx');
            default :
                return Excel::download(new AssetReportSumaryDepartment($this->getAssetSumary($this->department_id)), 'asset-report-default' . $today . '.xlsx');
        }
    }

    public function setStartTime($date)
    {
        $this->start_date = $date;
    }

    public function setEndTime($date)
    {
        $this->end_date = $date;
    }
}
