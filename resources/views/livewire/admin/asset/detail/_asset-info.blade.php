<div class="info-item">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="d-flex item-tỉtle">
                    <p class="text-uppercase">{{__('data_field_name.asset.asset_info')}}</p>
                    <span class="ml-auto"><img src="/images/Angle-down.svg" alt="angle-down"></span>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.asset_code_')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->code:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.guarantee_date')}}</p>
                    <p class="content">: {{$dataItem?reFormatDate($dataItem->guarantee_date??'','d-m-Y'):''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.asset_name')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->name:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.situation')}}</p>
                    <p class="content">:
                        @if($dataItem)
                            @if($dataItem->situation==0){{__('data_field_name.asset.no_use')}}
                            @elseif($dataItem->situation==1) {{__('data_field_name.asset.using')}}
                            @elseif($dataItem->situation==2) {{__('data_field_name.asset.recalled')}}
                            @endif
                        @endif</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.series_number')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->series_number:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.user_asset')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->fullname:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.buy_date')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->buy_date:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.asset_cate')}}</p>
                    <p class="content">: {{$dataItem&&$dataItem->category_id?$dataItem->assetCate->name:''}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="item-field">
                    <p class="name">{{__('data_field_name.asset.unit_type')}}</p>
                    <p class="content">: {{$dataItem?$dataItem->unit_type:''}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
