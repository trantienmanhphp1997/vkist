<?php

namespace App\Http\Livewire\Admin\System\Account;

use App\Enums\ESystemConfigType;
use App\Models\Account;
use Livewire\Component;

use App\Http\Livewire\Base\BaseLive;
use App\Models\SystemConfig;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;

class AccountList extends BaseLive
{
    public $searchTerm;
    public $deleteId = '';
    public $searchStatus;
    public $username;
    public $code;
    public $name;
    public $email;
    public $phone;
    public $status;

    public function mount()
    {
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render()
    {
        $searchTerm = $this->searchTerm;
        $searchStatus = $this->searchStatus;
        $query = User::with('info');
        if (!empty($this->searchTerm)) {
            $query->whereRaw("LOWER(username) LIKE LOWER('%{$searchTerm}%')");
            $query->orWhereRaw("LOWER(name) LIKE LOWER('%{$searchTerm}%')");
            $query->orWhereRaw("LOWER(email) LIKE LOWER('%{$searchTerm}%')");
            $query->orWhereHas('info', function (Builder $query) use ($searchTerm) {
                $query->whereRaw("LOWER(code) LIKE LOWER('%{$searchTerm}%')");
                $query->orWhereRaw("LOWER(phone) LIKE LOWER('%{$searchTerm}%')");
            });
        }
        if (strlen($this->searchStatus) > 0) {
            $query->where('status', '=', $searchStatus);
        }
        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);
        return view('livewire.admin.system.account.account-list', compact('data'));
    }

    public function getIdDelete($id)
    {
        $this->deleteId = $id;
    }

    public function toggleLockUser($id)
    {
        $account = User::findorFail($id);
        if (!is_null($account->status) && $account->status == 0) {
            $account->status = 1;
            $account->save();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('data_field_name.system.account.success_unlock_account')]);
        } else {
            $account->status = 0;
            $account->save();
            __('research/category-topic.notification.edit-success');
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('data_field_name.system.account.success_lock_account')]);
        }
    }


    public function delete()
    {
        $account = User::query()->findOrFail($this->deleteId)->load('info');
        if (!empty($account->info)) {
            $account->info->update([
                'user_id' => null
            ]);
        }
        $account->update([
            'user_info_id' => null
        ]);
        $account->delete();
        $this->dispatchBrowserEvent('show-toast', ['message' => __('notification.common.success.delete'), 'type' => 'success']);
    }
    public function detail($id)
    {
        $this->user = User::findorFail($id)->load('info');
        $this->id = $this->user->id;
        $this->username = $this->user->username ?? null;
        $this->code = $this->user->info->code ?? null;
        $this->name = $this->user->name ?? null;
        $this->phone = $this->user->info->phone ?? null;
        $this->email = $this->user->info->email ?? null;
        $this->status = $this->user->status ?? null;
    }

    public function recoveryPassword($accountId)
    {
        // load config
        $config = SystemConfig::query()->where('type', ESystemConfigType::TYPE_IMPORT_SALARY)->first();
        if (empty($config)) {
            $this->dispatchBrowserEvent('show-toast', ['message' => __('data_field_name.system.account.invalid_system_config'), 'type' => 'error']);
            return;
        }
        $content = $config->content ?? [];
        $defaultPassword = $content['password_default']['value'];

        // save password
        $account = User::query()->find($accountId);
        if (empty($account)) return;

        $account->password = Hash::make($defaultPassword);
        $account->save();

        $this->dispatchBrowserEvent('show-toast', ['message' => __('data_field_name.system.account.success_recovery_password'), 'type' => 'success']);
    }
}
