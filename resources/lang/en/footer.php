<?php
return [
    'licence' => 'License to set up a general website on the Internet No. 3221/GP-TTDT issued by the Department of Information and Communications of Hanoi on July 3, 2019',
    'info'=>'INFORMATION',
    'address'=>'Address: Hoa Lac Hi-tech Park, Km29, Thang Long Avenue, Hanoi',
    'phone'=>'Phone: (024) 3556 0695',
    'new'=>'NEWS - EVENTS',
    'new-1'=>'VKIST activity news',
    'new-2'=>'Recruitment',
    'new-3'=>'Email',
];
