<?php

namespace App\Http\Livewire\Admin\RealityExpense;

use Livewire\Component;
use App\Models\RealityExpense;
use App\Enums\ECapital;
use App\Enums\EPayForm;
use App\Enums\ETypePay;
use App\Enums\EDisbursedStatus;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class RealityExpenseList extends BaseLive
{
    public $capital;
    public $payform;
    public $typepay;
    public $status;
    public $searchCapital;
    public $searchPayForm;
    public $searchTypePay;
    public $searchStatus;

    public function mount(){
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->capital=[
            ECapital::ODA => __('data_field_name.reality_expense.ODA'),
            ECapital::STATE_CAPITAL => __('data_field_name.reality_expense.state_capital'),
        ];
        $this->payform=[
            EPayForm::EXPENSES => __('data_field_name.reality_expense.expenses'),
            EPayForm::NO_EXPENSES => __('data_field_name.reality_expense.no_expenses'),
        ];
        $this->typepay=[
            ETypePay::DIRECT_EXPENSES => __('data_field_name.reality_expense.direct_expenses'),
            ETypePay::INDIRECT_EXPENSES => __('data_field_name.reality_expense.indirect_expenses'),
        ];
        $this->status=[
            EDisbursedStatus::DISBURSED => __('data_field_name.reality_expense.disbursed'),
            EDisbursedStatus::NOT_DISBURSED => __('data_field_name.reality_expense.not_disbursed'),
        ];
    }
    public function render()
    {
        $query = RealityExpense::query()->with('topic.topicFeeDetails');
        if($this->searchTerm){    
            $query->whereHas('topic',function(Builder $q){
                 $q->where('topic.unsign_text','like','%'. strtolower(removeStringUtf8($this->searchTerm)).'%');

            });
        }
        if(!empty($this->searchCapital)){
            $query->where('source_capital',$this->searchCapital);
        }
        if(strlen($this->searchPayForm)>0){
            $query->where('payment',$this->searchPayForm);
        }
        if(!empty($this->searchTypePay)){
            $query->where('type_cost',$this->searchTypePay);
        }
        if(!empty($this->searchStatus)){
            $query->where('status',$this->searchStatus);
        }
        
        $data = $query->with('topicFeeDetail')->orderBy('id', 'desc')->paginate($this->perPage);
        $tmp = $data->each(function($item) { // Thêm biến sum_capital vào topic_actual_fee chứa tổng kinh phí đề xuất

            if($item->type_expense <= 6) {
                $item->expense_name = \App\Enums\ETopicFeeDetail::valueToName($item->type_expense);
            } else {
                $item->expense_name = $item->topicFeeDetail->name;
            }

            if($item->topic){
                $item->sum_capital = $item->topic->topicFeeDetails->sum('total_capital') ?? 0;
            } else {
                $item->sum_capital = 0;
            }
        });
        $data->setCollection($tmp);
        return view('livewire.admin.reality-expense.reality-expense-list',compact('data'));
    }
    public function deleteExpense(){
        $reality_expense = RealityExpense::findorFail($this->deleteId);
        $reality_expense->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }

}
