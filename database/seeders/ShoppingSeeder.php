<?php

namespace Database\Seeders;

use App\Models\Budget;
use App\Models\Shopping;
use App\Models\ShoppingDevice;
use App\Models\User;
use Illuminate\Database\Seeder;

class ShoppingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $budgets = Budget::all();
        $users = User::all();

        Shopping::factory()
            ->times(10)
            ->state(function () use ($budgets, $users) {
                return [
                    'budget_id' => $budgets->random()->id ?? null,
                    'executor_id' => $users->random()->id ?? null
                ];
            })
            ->has(ShoppingDevice::factory()->times(5), 'devices')
            ->create();
    }
}
