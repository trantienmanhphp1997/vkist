<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\SoftDeletes;

class EquipmentCost extends Model
{
    use HasFactory;

    protected $table = "equipment_cost";
    protected $fillable = [
        'name',
        'unit',
        'quantity',
        'price',
        'total',
        'state_capital',
        'other_capital',
    ];

    public function file(){
        return \App\Models\File::where('model_name', EquipmentCost::class)
                ->where('type', Config::get('common.type_upload.EquipmentCost'))->where('admin_id', auth()->id())->where('model_id', $this->id)->first();
    }

    public function files() {
        return $this->hasMany(File::class, 'model_id')->where('files.type','=',Config::get('common.type_upload.EquipmentCost'));
    }

    public function topicFeeDetail() {
        return $this->belongsTo(TopicFeeDetail::class, 'topic_fee_detail_id');
    }
}
