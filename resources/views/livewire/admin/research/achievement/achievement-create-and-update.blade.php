<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                    <div  class="breadcrumbs">
                        <a href="">{{__('research/achievement.breadcrumbs.achievement-management')}}</a>
                    </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row mx-5 p-4 bg-white">
                    <div class="col-md-12">
                            <h3 class="title">
                                    @switch($type)
                                    @case(App\Enums\EResearchAchievementType::SCIENCE_ARTICLE)
                                        @if($isEdit)
                                        {{__('research/achievement.title.edit-science-article')}}
                                        @else
                                        {{__('research/achievement.title.create-science-article')}}
                                        @endif
                                    @break
                                    @case(App\Enums\EResearchAchievementType::INTELLECTUAL_PROPERTY)
                                        @if($isEdit)
                                        {{__('research/achievement.title.edit-intellectual-property')}}
                                        @else
                                        {{__('research/achievement.title.create-intellectual-property')}}
                                        @endif
                                    @break
                                    @case(App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                                        @if($isEdit)
                                        {{__('research/achievement.title.edit-technology-transfer')}}
                                        @else
                                        {{__('research/achievement.title.create-technology-transfer')}}
                                        @endif
                                    @break
                                @endswitch
                            </h3>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('research/achievement.label.topic_code')}}</label>
                                        @livewire('component.search-combobox', ['model_name' => App\Models\Topic::class, 'filters' => ['name', 'code'], 'value_render_on_combobox' => 'code','targetId' => $topic_id ?? null])
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('research/achievement.label.topic_name')}}</label>
                                        <input type="text" class="form-control" placeholder="" name="topic_name" wire:model.defer="topic_name" readonly>
                                        @error('topic_name') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group">
                                            <label>{{__('research/achievement.label.topic_leader_name')}}</label>
                                            <input type="text" class="form-control" placeholder="" name="topic_leader_name" wire:model.defer="topic_leader_name" readonly>
                                            @error('topic_leader_name') <span class="text-danger">{{ $message }}</span> @enderror
                                        </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('research/achievement.label.topic_field_name')}}</label>
                                        <input type="text" class="form-control" placeholder="" name="topic_field_name" wire:model.defer="topic_field_name" readonly>
                                        @error('topic_field_name') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>{{__('research/achievement.label.topic_execution_time')}}</label>
                                        <input type="text" class="form-control" placeholder="" name="topic_execution_time" wire:model.defer="topic_execution_time" readonly>
                                    </div>
                                </div>
                                @switch($type)
                                    @case(App\Enums\EResearchAchievementType::SCIENCE_ARTICLE)
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label>{{__('research/achievement.label.name_of_science_article')}} <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" placeholder="" name="name_of_science_article" wire:model.defer="name">
                                                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('research/achievement.label.article_link')}}</label>
                                                <input type="text" class="form-control" placeholder="" name="article_link" wire:model.defer="article_link">
                                                @error('article_link') <span class="text-danger">{{ $message }}</span> @enderror
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('research/achievement.label.magazine_name')}} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="" name="magazine_name" wire:model.defer="magazine_name">
                                                @error('magazine_name') <span class="text-danger">{{ $message }}</span> @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('research/achievement.label.publish_date')}}</label>
                                                @include('layouts.partials.input._inputDate', ['input_id' => 'publish_date', 'place_holder'=>'','default_date' => $publish_date ])
                                            </div>
                                        </div>
                                    @break
                                    @case(App\Enums\EResearchAchievementType::INTELLECTUAL_PROPERTY)
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label>{{__('research/achievement.label.name_of_intellectual_property')}} <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" placeholder="" name="name_of_intellectual_property" wire:model.defer="name">
                                                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('research/achievement.label.publish_date_intellectual')}}</label>
                                                @include('layouts.partials.input._inputDate', ['input_id' => 'publish_date', 'place_holder'=>'', 'default_date' => $publish_date ])
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('research/achievement.label.type_of_owner')}}</label>
                                                <select class="form-control form-control-lg" name="type_of_owner" id="type_of_owner" wire:model.defer="type_of_owner">
                                                    <option>{{__('research/achievement.owner_type.default_option')}}</option>
                                                    <option value="1">{{App\Enums\EResearchAchievementOwnerType::valueToName(App\Enums\EResearchAchievementOwnerType::INVENTION)}}</option>
                                                    <option value="2">{{App\Enums\EResearchAchievementOwnerType::valueToName(App\Enums\EResearchAchievementOwnerType::INDUSTRIAL_DESIGNS)}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    @break
                                    @case(App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label>{{__('research/achievement.label.name_of_technology_transfer')}} <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" placeholder="" name="name_of_technology_transfer" wire:model.defer="name">
                                                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('research/achievement.label.receiver_department')}} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="" name="receiver_department" wire:model.defer="receiver_department">
                                                @error('receiver_department') <span class="text-danger">{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{__('research/achievement.label.type_of_object_transfer')}}</label>
                                                <select class="form-control form-control-lg" name="type_of_object_transfer" id="type_of_object_transfer" wire:model.defer="type_of_object_transfer">
                                                    <option>{{__('research/achievement.object_transfer_type.default_option')}}</option>
                                                    <option value="1">{{App\Enums\EResearchAchievementObjectTransferType::valueToName(App\Enums\EResearchAchievementObjectTransferType::INDUSTRIAL_PROPERTY_OBJECTS)}}</option>
                                                    <option value="2">{{App\Enums\EResearchAchievementObjectTransferType::valueToName(App\Enums\EResearchAchievementObjectTransferType::TECHNICAL_KNOWLEDGE_ABOUT_TECHNOLOGY)}}</option>
                                                    <option value="3">{{App\Enums\EResearchAchievementObjectTransferType::valueToName(App\Enums\EResearchAchievementObjectTransferType::SOLUTIONS_TO_RECOGNIZE_PRODUCTION)}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    @break
                                @endswitch
                            </div>
                        </div>

                        <div class="col-md-12 upload-container">
                            <h4 >
                                {{__('research/achievement.table_column.document')}}
                            </h4>
                            <form wire:submit.prevent="submit" enctype="multipart/form-data">
                                <div class="form-group upload-file">
                                    <label></label>
                                    <div class="bd-attached file_form">
                                        <div class="attached-center">
                                            <input type="file" multiple onchange="uploadFile(this, @this)" id="file_create" class="custom-file-input cur_input">
                                            <span class="file" >
                                                    {{__('research/achievement.attach-file')}}
                                                <img src="/images/Clip.svg" alt="" height="24">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="col-md-12">
                                <div class="row">
                                        @foreach ($listFileUpload as $key => $val)
                                        <div class="form-group col-md-4">
                                            <div class="attached-files">
                                                <img src="/images/File.svg" alt="file">
                                                <div class="content-file">
                                                    <p>{{ $val['file_name'] }}</p>
                                                    <p class="kb">{{ $val['size_file']}}</p>
                                                </div>
                                                <span>
                                                <button wire:click.prevent="deleteNewFileUpload({{$key}})"
                                                        style="border: none;background: white" type="button"><img
                                                        src="/images/close.svg" alt="close"/>
                                                </button>
                                            </span>
                                            </div>
                                        </div>
                                    @endforeach
                                    @foreach ($listOldFileTemp as $key => $val)
                                        <div class="form-group col-md-4">
                                            <div class="attached-files">
                                                <img src="/images/File.svg" alt="file">
                                                <div class="content-file" wire:click="download('{{$val['url']}}', '{{$val['file_name']}}')" style="cursor: pointer">
                                                    <p>{{ $val['file_name'] }}</p>
                                                    <p class="kb">{{ $val['size_file']}}</p>
                                                </div>
                                                <span>
                                                    <button wire:click.prevent="deleteFileOldTemp({{$key}})"
                                                            style="border: none;background: white" type="button"><img
                                                            src="/images/close.svg" alt="close"/>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="file-loading position-absolute bg-primary rounded text-center text-white" style="transition: width 0.3s; bottom: 0px; left: 0; height: 5px; width: 0%;">&nbsp;</div>
                                <div class="row">
                                    @error('fileUpload')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                
                                    @error('current_total_file')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror   
                                </div>
                                <div class="row">
                                    <div class="text-danger upload-error"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-5">
                            <div class="group-btn2 d-flex justify-content-center">
                                @switch($type)
                                    @case(App\Enums\EResearchAchievementType::SCIENCE_ARTICLE)
                                        <a class="btn btn-cancel" style="color:gray" href="{{route('admin.research.achievement.science-article.index')}}">
                                            {{__('common.button.cancel')}}
                                        </a>
                                        @break
                                    @case(App\Enums\EResearchAchievementType::INTELLECTUAL_PROPERTY)
                                        <a class="btn btn-cancel" style="color:gray" href="{{route('admin.research.achievement.intellectual-property.index')}}">
                                            {{__('common.button.cancel')}}
                                        </a>
                                        @break
                                    @case(App\Enums\EResearchAchievementType::TECHNOLOGY_TRANSFER)
                                        <a class="btn btn-cancel" style="color:gray" href="{{route('admin.research.achievement.technology-transfer.index')}}">
                                            {{__('common.button.cancel')}}
                                        </a>
                                        @break
                                @endswitch
                                <button type="button" id="btn-save-asset-provider" wire:click="store" class="btn btn-save mr-2">
                                    {{__('common.button.save')}}
                                </button>
                            </div>
                        </div>
            </div>
        </div>

    </div>
    <script>
        function uploadFile(input, proxy = null, index = 0) {
                let file = input.files[index];
                if (!file || !proxy) return;
                let $fileLoading = $(input).parents('.upload-container').find('.file-loading');
                let $error = $(input).parents('.upload-container').find('.upload-error');
                $fileLoading.width('0%');
                if(file.size/(1024*1024) >= {{$limit_file_size}}) {
                    console.log($error);
                    $error.html("{{ __('notification.upload.maximum_size', ['value' => $limit_file_size]) }}");
                    return;
                }
                let mimes = {!! json_encode($accept_file) !!};
                let extension = file.name.split('.').pop();
                if (!mimes.includes(extension)) {
                    $error.html("{{ __('notification.upload.mime_type', ['values' => implode(', ', $accept_file)]) }}");
                    return;
                }
                proxy.upload('fileUpload', file, (uploadedFilename) => {
                    setTimeout(() => {
                        $fileLoading.width('0%');
                        uploadFile(input, proxy, index + 1);
                    }, 500);
                }, () => {
                    console.log(error);
                }, (event) => {
                    $fileLoading.width(event.detail.progress + '%');
                });
                
            }
    </script>
</div>





