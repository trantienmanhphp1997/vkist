<?php

namespace App\Http\Livewire\Component;

use Livewire\Component;

class AdvancedSelect extends Component
{

    public string $name = 'advanced-select';

    public $modelName;
    protected $modelQuery;

    public array $conditions = [];
    public array $searchBy = [];
    public array $columns = [];
    public array $relations = [];

    public $keyword = '';
    public string $placeholder = '';
    public bool $disabled = false;
    public array $data = [];
    public bool $displayMultiSelected = true;

    public $multiple = false;

    public $currentPage = 1;
    public $perPage = 5;

    public array $selectedIds = [];
    public array $selectedItems = [];
    public string $itemTemplate;
    public string $selectedItemTemplate;

    public function mount()
    {
        if (empty($this->itemTemplate) && isset($this->columns[0])) {
            $this->itemTemplate = ":{$this->columns[0]}";
        }

        if (empty($this->selectedItemTemplate) && isset($this->columns[0])) {
            $this->selectedItemTemplate = ":{$this->columns[0]}";
        }

        $this->selectedIds = array_filter($this->selectedIds, fn ($item) => $item != null);

        $this->initModelQuery();
        $this->loadData();
        $this->loadSelectedItems();
    }

    public function hydrate()
    {
        $this->initModelQuery();
    }

    public function render()
    {
        return view('livewire.component.advanced-select');
    }

    public function updatedKeyword()
    {
        $this->keyword = trim($this->keyword);
        if (!$this->multiple && empty($this->keyword)) {
            $this->selectedItems = [];
        }
        $this->loadData();
    }

    public function selectItem($itemId)
    {
        $item = $this->data[$itemId] ?? null;
        if ($this->multiple) {
            $this->selectedItems[$itemId] = $item;
            $this->reset('keyword');
            $this->emitUp("changed-{$this->name}-event", $this->selectedItems);
        } else {
            $this->selectedItems = [$itemId => $item];
            $this->emitUp("changed-{$this->name}-event", $this->selectedItems[$itemId]);
        }
    }

    public function deselectItem($itemId)
    {
        if (isset($this->selectedItems[$itemId])) {
            unset($this->selectedItems[$itemId]);
            $this->reset('keyword');
        }
        $this->emitUp("changed-{$this->name}-event", $this->selectedItems);
    }

    public function loadMore()
    {
        $this->currentPage++;
        $this->loadData();
    }

    public function loadData()
    {
        $tmpQuery = clone $this->modelQuery;
        if (!empty($this->keyword)) {
            $tmpQuery->where(function ($q) {
                foreach ($this->searchBy as $field) {
                    $q->orWhereRaw("LOWER($field) LIKE LOWER('%{$this->keyword}%')");
                }
            });
        }
        $this->data = $tmpQuery->limit($this->currentPage * $this->perPage)->get(array_merge($this->columns, ['id']))->keyBy('id')->toArray();
    }

    public function loadSelectedItems()
    {
        if (count($this->selectedIds) > 0) {
            if (!$this->multiple) {
                $this->selectedIds = [$this->selectedIds[0]];
            }
            $tmpQuery = clone $this->modelQuery;
            $this->selectedItems = $tmpQuery->whereIn('id', $this->selectedIds)->get(array_merge($this->columns, ['id']))->keyBy('id')->toArray();
        }
    }

    public function initModelQuery()
    {
        if (empty($this->modelQuery) && empty($this->modelName)) {
            return [];
        }

        if (empty($this->modelQuery) && !empty($this->modelName)) {
            $this->modelQuery = $this->modelName::query();
        }

        if (!empty($this->relations)) {
            $this->modelQuery->with($this->relations);
        }

        foreach ($this->conditions as $condition) {
            if (empty($condition)) continue;

            if (is_string($condition)) {
                $this->modelQuery->whereRaw($condition);
            } elseif (is_array($condition)) {
                if ($condition[1] == 'in') {
                    $this->modelQuery->whereIn($condition[0], $condition[2]);
                } else if ($condition[1] == 'not in') {
                    $this->modelQuery->whereNotIn($condition[0], $condition[2]);
                } else {
                    $this->modelQuery->where(...$condition);
                }
            }
        }
    }
}
