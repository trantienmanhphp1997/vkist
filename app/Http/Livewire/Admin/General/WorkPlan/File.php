<?php

namespace App\Http\Livewire\Admin\General\WorkPlan;

use App\Http\Livewire\Base\BaseLive;
use App\Models\WorkPlan;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Component\SizeFile;
use Illuminate\Support\Str;

class File extends BaseLive
{
    use WithFileUploads;
    public $file;
    public $url;
    public $maximumFileSize = 255; // Mb
    public $file_name;
    public function render()
    {
        if ($this->file) {
            $this->store();
            $this->file = null;
        }
        $files=\App\Models\File::where('model_name','App/Models/WorkPlan')->where('type', -1)->get();
        return view('livewire.admin.general.work-plan.file', ['files'=>$files]);
    }
    public function store()
    {
        $files=\App\Models\File::where('model_name','App/Models/WorkPlan')->where('type', -1)->get();
        if(count($files)>=5) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.upload.maximum_uploads')]);
            return;
        }
        // check file size
        if ($this->file->getSize() > $this->maximumFileSize * 1024 * 1024) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.upload.maximum_size', ['value' => $this->maximumFileSize])]);
            return;
        }
        $file_workplan = $this->file;
        $size = new SizeFile();
        $size=$size->sizeFile($file_workplan->getSize());

        $file_name_work_plan = $file_workplan->getClientOriginalName();
        $file_name_hash = Str::random(20) . '.' . $file_workplan->getClientOriginalExtension();
        $url_work_plan = $this->file->storeAs('uploads/work-plan/files/' . auth()->id(), $file_name_hash, 'local');
        $file_upload = new \App\Models\File();
        $file_upload->url = $url_work_plan;
        $file_upload->file_name = $file_name_work_plan;
        $file_upload->size_file = $size;
        $file_upload->model_name = 'App/Models/WorkPlan';
        $file_upload->type = -1;
        $file_upload->save();

    }
    public function deleteFile($id){
        \App\Models\File::findOrFail($id)->delete();
    }
    public function deleteAllFile() {
        $files=\App\Models\File::where('model_name','App/Models/WorkPlan')->where('type', -1)->get();
        foreach($files as $item) {
            $this->deleteFile($item->id);
        }
    }
}
