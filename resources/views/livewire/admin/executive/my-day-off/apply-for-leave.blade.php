<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="">{{__('menu_management.menu_name.infomation-management')}}
            \ {{__('executive/my-day-off.title')}}</a>
            \ <span>{{isset($Request_leave_id)?__('executive/my-day-off.title_edit'):__('executive/my-day-off.apply_for_leave')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('executive/my-day-off.general_info')}}</h3>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="form-group field-fix d-flex flex-row  mr-12">
                        <div class="col-md-2">
                            <br>
                            <label for="" class="mb-0 mr-12">{{__('executive/my-day-off.from_date')}} <span class="text-danger">(*)</span></label>
                        </div>
                        <div class="col-md-3">
                            @if(!isset($Request_leave_id)||$requestLeave->status==$statusNew)
                                @include('layouts.partials.input._inputDate', ['input_id' => 'from-date', 'place_holder'=>'', 'default_date' => is_null($fromDate) ? date('Y-m-d') : $fromDate, ])
                                @error('fromTime')
                                <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            @else
                                <input type="text" value ="{{reFormatDate($fromDate)}}" disabled>
                            @endif
                        </div>

                        <div class="col-md-2">
                        <select wire:model.lazy='fromHour' class='form-control' {{(!isset($Request_leave_id)||$requestLeave->status==$statusNew)?'':'disabled'}}>
                            <option value="1">08:00</option>
                            <option value="2">13:30</option>
                        </select>
                            {{--<input type="text" id='fromHour' placeholder="HH:MM" wire:model.lazy='fromHour'>--}}
                        @error('fromHour')
                        <span class="error text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="form-group field-fix d-flex flex-row  mr-12">
                        <div class="col-md-2">
                            <br>
                            <label for="" class="mb-0 mr-12">{{__('executive/my-day-off.to_date')}} <span class="text-danger">(*)</span></label>
                        </div>
                        <div class="col-md-3" >
                                @if(!isset($Request_leave_id)||$requestLeave->status==$statusNew)
                                    @include('layouts.partials.input._inputDate', ['input_id' => 'to-date', 'place_holder'=>'', 'default_date' => is_null($toDate) ? date('Y-m-d') : $toDate])
                                @else
                                    <input type="text" value ="{{reFormatDate($toDate)}}" disabled>
                                @endif
                            @error('toTime')
                                <span class="error text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-md-2">
                            <select wire:model.lazy='toHour' class='form-control' {{(!isset($Request_leave_id)||$requestLeave->status==$statusNew)?'':'disabled'}}>
                                <option value="1">12:00</option>
                                <option value="2">17:30</option>
                            </select>
                            {{--<input type="text" id='toHour' placeholder="HH:MM" wire:model.lazy='toHour'>--}}
                            @error('toHour')
                            <span class="error text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group field-fix d-flex flex-row align-items-center mr-12">
                        <div class="col-md-2">
                            <label for="" class="mb-0 mr-12">{{__('executive/my-day-off.day_leave')}}</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" wire:model.lazy='leaveDay' disabled>
                        </div>
                    </div>
                    <div class="form-group field-fix d-flex flex-row align-items-center mr-12">
                        <div class="col-md-2">
                            <label for="" class="mb-0 mr-12">{{__('executive/my-day-off.type_leave')}} <span class="text-danger">(*)</span></label>
                        </div>
                        <div class="col-md-6">
                            <select wire:model.lazy="type" class="form-control" {{(!isset($Request_leave_id)||$requestLeave->status==$statusNew)?'':'disabled'}}>
                                <option>--{{__('executive/my-day-off.chose_leave_type')}}--</option>
                                <option value="1">{{__('executive/my-day-off.leave_for_the_year')}}</option>
                                <option value="2">{{__('executive/my-day-off.paid_time_off')}}</option>
                                <option value="3">{{__('executive/my-day-off.holiday')}}</option>
                                <option value="4">{{__('executive/my-day-off.meeting')}}</option>
                                <option value="5">{{__('executive/my-day-off.paid_personal_leave')}}</option>
                                <option value="6">{{__('executive/my-day-off.unpaid_personal_leave')}}</option>
                                <option value="7">{{__('executive/my-day-off.sick_leave')}}</option>
                                <option value="8">{{__('executive/my-day-off.sick_child_leave')}}</option>
                                <option value="9">{{__('executive/my-day-off.maternity_leave')}}</option>
                                <option value="10">{{__('executive/my-day-off.discipline')}}</option>
                                <option value="11">{{__('executive/my-day-off.labor_accident')}}</option>
                                <option value="12">{{__('executive/my-day-off.working')}}</option>
                                <option value="13">{{__('executive/my-day-off.training')}}</option>
                                <option value="14">{{__('executive/my-day-off.meet')}}</option>
                                <option value="15">{{__('executive/my-day-off.wedding_funeral')}}</option>
                                <option value="16">{{__('executive/my-day-off.difference')}}</option>
                            </select>
                            @error('type')
                            <span class="error text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group field-fix d-flex flex-row align-items-center mr-12">
                        <div class="col-md-2">
                            <label for="" class="mb-0 mr-12">{{__('executive/my-day-off.leave_day_remaining')}}</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" wire:model.lazy='leaveDayRemaining' disabled>
                        </div>
                    </div>
                    <div class="form-group field-fix d-flex flex-row align-items-center mr-12">
                        <div class="col-md-2">
                            <label for="" class="mb-0 mr-12">{{__('executive/my-day-off.reason_for_leave')}} <span class="text-danger">(*)</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" wire:model.lazy='content' class="form-control" {{(!isset($Request_leave_id)||$requestLeave->status==$statusNew)?'':'disabled'}}>
                            @error('content')
                            <span class="error text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                @if(!isset($Request_leave_id)||$requestLeave->status==$statusNew)
                    @if(!isset($Request_leave_id))
                        @livewire('component.files', ['model_name'=>$model_name, 'type'=>$type_file, 'folder'=>$folder,'uploadOnShow'=>1,'acceptMimeTypes' => config('common.mime_type.general')])
                    @else
                        @livewire('component.files', ['model_name'=>$model_name, 'type'=>$type_file, 'folder'=>$folder,'uploadOnShow'=>1,'model_id'=>$Request_leave_id,'acceptMimeTypes' => config('common.mime_type.general')])
                    @endif
                @else
                    @livewire('component.files', ['model_name'=>$model_name, 'type'=>$type_file, 'folder'=>$folder,'uploadOnShow'=>1,'model_id'=>$Request_leave_id,'disabled'=>true,'acceptMimeTypes' => config('common.mime_type.general')])
                @endif

                <div class="col-md-12 mt-5">
                    <div class="attached-center" style="width: 450px">
                        @if(!isset($Request_leave_id)||$requestLeave->status==$statusNew)
                            <button id='btn_submit' type="button" wire:click='sendGW' class="btn btn-save close-modal" style='display:none'>{{__('common.button.send')}}</button>
                            <button id='btn_submit_create' type="button" wire:click='saveReq' class="btn btn-save close-modal" style='display:none'>{{__('common.button.save')}}</button>
                            <button id='btn_click_create' type="button" class="btn btn-save close-modal" wire:loading.attr="disabled" {{($disabled)?"disabled":""}}>{{__('common.button.save')}}</button>
                            <button id='btn_click' type="button"  class="btn btn-save close-modal" wire:loading.attr="disabled" {{($disabled)?"disabled":""}}>{{__('common.button.send')}}</button>
                        @endif
                        <a class="btn btn-cancel close-btn" href="{{route('admin.executive.my-day-off.index')}}">{{__('common.button.close')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (!empty($request_leave))
        @livewire('common.api-approval', ['modelId' => $request_leave->id, 'type' => \App\Enums\EApiApproval::TYPE_REQUEST_LEAVE, 'requestId' => $requestId])
    @endif

</div>
<script type="text/javascript">

    $("document").ready(function() {
        $('#search-approved-by').select2({
            placeholder: "{{ __('data_field_name.system.account.search_role') }}",
            matcher: function(params, data) {
                if ($.trim(params.term) === '') {
                    return data;
                }
                if (typeof data.text === 'undefined') {
                    return null;
                }

                if (data.text.toLowerCase().indexOf(params.term) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }
                return null;
            }
        });
        document.addEventListener('livewire:load', function () {
            $('.approved-by').on('change', function (e) {
                let data = $(this).val();
            @this.set('approved_by', data);
            });

        });
        $('#fromHour').bind('keyup',function(evt){
            let last_input= String.fromCharCode(evt.keyCode);
            let str = $('#fromHour').val();
            str = str.replace(/:/, '');
            if (str.length < 4) {
                str = '0000' + str;
            }
            if (str.length > 4) {
                str = str.substring(str.length - 4, str.length);
            }
            str = str.substring(0, 2) + ':' + str.substring(2, 4);
            $("#fromHour").val(str);
        });
        $('#toHour').bind('keyup',function(evt){
            let last_input= String.fromCharCode(evt.keyCode);
            let str = $('#toHour').val();
            str = str.replace(/:/, '');
            if (str.length < 4) {
                str = '0000' + str;
            }
            if (str.length > 4) {
                str = str.substring(str.length - 4, str.length);
            }
            str = str.substring(0, 2) + ':' + str.substring(2, 4);
            $("#toHour").val(str);
        });
        $('#btn_click').click(function(){
            $('#btn_submit').click()
        })
        $('#btn_click_create').click(function(){
            $('#btn_submit_create').click()
        })
     });
</script>
