<div class="col-md-10 col-xl-11 box-detail box-user box-new-table list-role">
    <style>
        .title-approve{
            margin: 0 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="#"><span>{{__('user/group_user.title.main-title')}}</a> \
                <span>{{__('user/group_user.title.list')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('user/group_user.title.list')}}</h3>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group search-expertise">
                                <div class="search-expertise">
                                    <input type="text" placeholder="{{__('common.place_holder.search')}}" name="search" class="form-control"
                                           wire:model.debounce.1000ms="searchTerm" id='input_vn_name'
                                           autocomplete="off">
                                    <span><img src="/images/Search.svg" alt="search"/></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group float-right">
                                @if($checkCreatePermission)
                                    <button title="{{__('common.button.create')}}" type="button" style="border-radius: 11px; border:none;">
                                        <a href="{{route('admin.system.group-user.create')}}">
                                            <img src="/images/filteradd.png" alt="filteradd">
                                        </a>
                                    </button>@endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-general">
                            <thead>
                            <tr class="border-radius">
                                <th scope="col" class="text-center"
                                    class="border-radius-left">{{__('news/newsManager.menu_name.news_title_table.stt')}}</th>
                                <th scope="col" class="text-center">{{__('user/group_user.form-data.code')}}</th>
                                <th scope="col" class="text-center">{{__('user/group_user.form-data.name')}}</th>
                                <th scope="col" class="text-center">{{__('user/group_user.form-data.description')}}</th>
                                <th scope="col" class="text-center">{{__('data_field_name.common_field.action')}}</th>
                            </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                            @foreach($groupUserList as $row)
                                <tr>
                                    <td class="text-center">
                                        {{ ($groupUserList->currentPage() - 1) * $groupUserList->perPage() + $loop->iteration }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.system.group-user.detail.show', $row->id) }}">
                                            {!! boldTextSearch($row->code, $searchTerm) !!}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        {!! boldTextSearch($row->name, $searchTerm) !!}
                                    </td>
                                    <td class="text-center">
                                        {{$row->description}}
                                    </td>
                                    <td class="text-center">
                                        @if($checkEditPermission)
                                            <a title="{{__('common.button.edit')}}" class="btn par6"
                                            href="{{ route('admin.system.group-user.edit', $row->id) }}"><img
                                                    src="/images/pent2.svg" alt=""></a>
                                        @endif
                                        @if($checkDestroyPermission)
                                            <a title="{{__('common.button.delete')}}" class="btn par6" data-toggle="modal" data-target="#deleteModal"
                                            wire:click="setGroupUserId({{$row->id}})"
                                            href="{{ route('admin.system.group-user.edit', $row->id) }}"> <img
                                                    src="/images/trash.svg" alt="trash" height="30px"></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if(count($groupUserList) > 0)
                        {{$groupUserList->links()}}
                    @else
                        <div class="title-approve text-center">
                            <span>{{__('data_field_name.system.role.no_result')}}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')
                </div>
            </div>
        </div>
    </div>
</div>
