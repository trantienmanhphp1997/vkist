<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
                <div  class="breadcrumbs"><a href="{{route('admin.asset.allocated-revoke.index')}}">{{__('menu_management.menu_name.asset-management')}}</a> \ <span>{{__('data_field_name.allocated-revoke.transfer_list')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.allocated-revoke.transfer_list')}}</h3>
                </div>
                <div class="col-md-6">
                    @if($checkCreatePermission)
                        <a class="btn btn-viewmore-news mr0 float-right" href="{{route('admin.asset.transfer.create')}}">
                            <img src="/images/plus2.svg" alt="plus">{{__('data_field_name.allocated-revoke.transfer')}}
                        </a>
                    @endif
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group search-expertise search-plan">
                                <div class="search-expertise inline-block search-staff">
                                    <input wire:model.debounce.1000ms="searchTerm" type="text" class="form-control size13" placeholder="{{__('data_field_name.asset.transfer_search')}}">
                                    <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group float-right">
                                @if(checkRoutePermission('download'))
                                    <button type="button" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#exampleModal"> <img src="/images/filterdown.svg" alt="filterdown"> </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <table class="table">
                        <thead>
                            <tr class="border-radius">
                                <th scope="col" class="text-center border-radius-left">
                                    {{__('data_field_name.allocated-revoke.number_report')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('data_field_name.allocated-revoke.transfer_date')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('data_field_name.asset.transfer_type')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('data_field_name.allocated-revoke.transfer_from')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('data_field_name.allocated-revoke.transfer_to')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('data_field_name.allocated-revoke.quantity')}}
                                </th>
                                <th scope="col" class="text-center">
                                    {{__('data_field_name.allocated-revoke.reason_transfer')}}
                                </th>
                                <th scope="col" class="text-center border-radius-right">
                                    {{__('data_field_name.allocated-revoke.performer')}}
                                </th>
                            </tr>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>
                            @foreach($data as $index => $row)
                                <tr>
                                    <td class="text-center">
                                        <a href="{{route('admin.asset.transfer.detail', ['id' => $row['id']])}}">{!! boldTextSearch($row['number_report'], $searchTerm) !!}</a>
                                    </td>
                                    <td class="text-center">{!! $row['implementation_date'] !!}</td>
                                    <td class="text-center">{!! $row['transfer_type'] !!}</td>
                                    <td class="text-center">{!! $row['from'] !!}</td>
                                    <td class="text-center">{!! $row['to'] !!}</td>
                                    <td class="text-center">{!! $row['implementation_quantity'] !!}</td>
                                    <td class="text-center">{!! $row['reason'] !!}</td>
                                    <td class="text-center">{!! $row['perfomer'] !!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row mb-4 mx-2">
                        <div class="col-md-3">
                            {{__('data_field_name.allocated-revoke.total_number_of_records')}}: <span class="font-weight-bold">{{$data->total()}}</span>
                        </div>
                        <div class="col-md-3">
                            {{__('data_field_name.allocated-revoke.total_assets_in_use')}}: <span class="font-weight-bold">{{$total_asset}}</span>
                        </div>
                        <div class="col-md-3">
                            <a class="font-weight-bold" href="{{route('admin.asset.dashboard.index', [])}}">{{__('data_field_name.allocated-revoke.status_statistics')}}</a>
                        </div>
                    </div>
                    @include('livewire.common._modalExportFile')
                    @if(count($data) > 0)
                        {{$data->links()}}
                    @else
                        <div class="title-approve text-center">
                            <span>{{__('common.message.empty_search')}}</span>
                        </div>
                    @endif
                    @include('livewire.common.modal._modalDelete')
                </div>
            </div>
        </div>
    </div>
</div>
