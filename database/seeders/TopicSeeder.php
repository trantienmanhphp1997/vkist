<?php

namespace Database\Seeders;

use App\Models\MasterData;
use App\Models\ResearchCategory;
use App\Models\Topic;
use App\Models\UserInf;
use Illuminate\Database\Seeder;
use DB;
class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            // Topic::create([
            //     'code' => 12345,
            //     'major_id' => MasterData::where('type', 12)->get()->random()->id,
            //     'name' => "name",
            //     'necessary' => 'necessary',
            //     'overview' => 'overview',
            //     'target' => 'target',
            //     // 'level_id' => MasterData::where('type', 11)->get()->random()->id,
            //     'research_category_id' => ResearchCategory::all()->random()->id,
            //     'leader_id' => UserInf::all()->random()->id,
            //     'admin_id' => 1,
            //     'type' => 0,
            //     'space_scope' => "space_scope",
            // ]);
            // Topic::factory()->times(20)->create();
        $userInfos = UserInf::all();
        Topic::factory()->times(10)->create()->each(function(Topic $topic) use ($userInfos) {
            $index = 0;
            $members = $userInfos->random(5)->mapWithKeys(function($item) use (&$index){
                return [$item['id'] => [
                    'role_id' => ($index++ == 0) ? 26 : 28,
                ]];
            });
            $topic->userInfos()->syncWithoutDetaching($members);

        });

    }
}
