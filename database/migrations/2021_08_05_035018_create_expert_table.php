<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expert', function (Blueprint $table) {
            $table->id()->comment('Danh sach chuyen gia');
            $table->string('fullname', 255)->comment('Ho ten');
            $table->string('address', 255)->comment('Dia chi');
            $table->string('phone', 12)->comment('So dien thoai');
            $table->string('nationality', 255)->comment('Quoc tich');
            $table->unsignedBigInteger('department_id')->nullable()->comment('Map voi department');
            $table->foreign('department_id')->references('id')->on('department');
            $table->bigInteger('academic_level_id')->nullable()->comment(' trinh do học van map master data type = 5');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_detail_expert');
    }
}
