<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="{{ route('admin.asset.allocated-revoke.index') }}">{{ __('menu_management.menu_name.asset-management') }}</a> \ <span>{{ __('menu_management.menu_name.transfer') }}</span></div>
        </div>
    </div>
    <div class="row bd-border inner-tab">
        <div class="col-md-12">
            <div class="detail-task information box-idea">
                <h4>{{ __('data_field_name.allocated-revoke.detail') }}</h4>
                <h3 class="title-asset pt-24">{{ __('data_field_name.allocated-revoke.general_information') }}</h3>
                <div class="box-calendar col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ __('data_field_name.allocated-revoke.number_report') }}</label>
                                <input type="text" value="{{ $number_report }}" disabled class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ __('data_field_name.allocated-revoke.transfer_date') }}</label>
                                <input type="date" value="{{ $day }}" disabled class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{ __('data_field_name.allocated-revoke.reason_transfer') }}</label>
                                <textarea disabled class="form-control">{{ $reason }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{ __('data_field_name.asset.transfer_type') }}</label>
                                <input type="text" value="{{ \App\Enums\EAssetTransferType::valueToName($transferType) }}" disabled class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="title-asset pt-24" style="text-transform: uppercase;">{{ __('data_field_name.allocated-revoke.transfer') }}</h3>
                <div class="box-calendar col-md-12">
                    @if ($transferType == \App\Enums\EAssetTransferType::BY_INDIVIDUAL)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.allocated-revoke.from_user') }}</label>
                                    <input type="text" value="{{ $from }}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.allocated-revoke.to_user') }}</label>
                                    <input type="text" value="{{ $to }}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                    @elseif ($transferType == \App\Enums\EAssetTransferType::BY_DEPARTMENT)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.allocated-revoke.from_department') }}</label>
                                    <input type="text" value="{{ $from }}" disabled class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('data_field_name.common_field.manager') }}</label>
                                    <input type="text" value="{{ $department1Leader }}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('data_field_name.allocated-revoke.to_department') }}</label>
                                    <input type="text" value="{{ $to }}" disabled class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('data_field_name.common_field.manager') }}</label>
                                    <input type="text" value="{{ $department2Leader }}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <h3 class="title-asset pt-24" style="text-transform: uppercase;">{{ __('data_field_name.allocated-revoke.transfered_assets') }}</h3>
                <div class="form-group">
                    <span>{{ $asset->name }}</span>
                </div>
                <h3 class="title-asset pt-24" style="text-transform: uppercase;">{{ __('data_field_name.allocated-revoke.attachments') }}</h3>
                <div class="row">
                    @foreach ($fileList as $item)
                        <div class="form-group col-md-4">
                            <div class="attached-files">
                                <img src="/images/File.svg" alt="file">
                                <div class="content-file">
                                    <p class="kb" style="word-break: break-all;">{{ $item->file_name }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="group-btn2 text-center group-btn ">
                    <a href="{{ route('admin.asset.transfer.index') }}" class="btn btn-cancel">{{ __('common.button.back') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
