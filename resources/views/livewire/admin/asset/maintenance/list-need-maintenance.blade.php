<div>
    <div class="inner-tab pt0">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group search-expertise search-plan">
                    <div class="search-expertise inline-block">
                        <input type="text" class="form-control size13" name="searchTerm" autocomplete="off"
                               wire:model.debounce.1000ms="searchTerm"
                               placeholder="{{__('data_field_name.asset.search_place')}}">
                        <span>
                                        <img src="/images/Search.svg" alt="search"/>
                                    </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                @if(checkRoutePermission('download'))
                    <div class="form-group float-right">
                        <button type="button" id='btnOpenExportModal' style="border-radius: 11px; border:none;"
                                data-toggle="modal" data-target="#export-modal">
                            <img src="/images/filterdown.svg" alt="filterdown">
                        </button>
                    </div>
                @endif
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-general" id='dataTable'>
                <thead>
                <tr class="border-radius">
                    <th scope="col" class="text-uppercase border-radius-left">{{__('data_field_name.asset.asset_code')}}</th>
                    <th scope="col" class="text-uppercase text-center">{{__('data_field_name.asset.number_report')}}</th>
                    <th scope="col" class="text-uppercase text-center">{{__('data_field_name.asset.asset_name')}}</th>
                    <th scope="col" class="text-uppercase text-center">{{__('data_field_name.asset.asset_cate')}}</th>
                    <th scope="col" class="text-uppercase text-center">{{__('data_field_name.asset.asset_content')}}</th>
                    <th scope="col" class="text-uppercase text-center">{{__('data_field_name.asset.asset_status')}}</th>
                    <th scope="col" class="text-uppercase text-center">{{__('data_field_name.asset.asset_quantity')}}</th>
                    <th scope="col" class="text-uppercase text-center">{{__('data_field_name.asset.maintenance_user')}}</th>
                    @if ($checkDestroyPermission)
                        <th class="text-uppercase text-center border-radius-right" scope="col">
                            {{__('data_field_name.common_field.action')}}</th>
                    @endif
                </tr>
                </thead>
                <div wire:loading class="loader"></div>
                <tbody>
                @forelse($data as $key=> $val)
                    <tr>
                        <td class="text-center">{!! boldTextSearch($val->asset->code,$searchTerm) !!}</td>
                        <td class="text-center">
                            @if(checkRoutePermission('approve'))
                                <a style="cursor: pointer;" class="text-primary" href="#" data-toggle="modal"
                                   data-target="#confirm_notice_maintenance"
                                   wire:click="confirmNotice({{ $val->id }})">
                                    {{$val->number_report ?? ''}}
                                </a>
                            @else
                                {{$val->number_report ?? ''}}
                            @endif
                        </td>
                        <td class="text-center">{!! boldTextSearch($val->asset->name,$searchTerm) !!}</td>
                        <td class="text-center">{{$val->asset->category->name ?? ''}} </td>
                        <td class="text-center">
                            @if($val->status==\App\Enums\EAssetStatus::REPORT_REPAIR)
                                {{$val->content_repair ?? ''}}
                            @elseif($val->status==\App\Enums\EAssetStatus::REPORT_MAINTENANCE)
                                {{$val->rule_maintenance->content_maintenance ?? ''}}
                            @endif
                        </td>
                        <td class="text-center">{{$val->status?$assetStatus[$val->status]:''}}</td>
                        <td class="text-center">{{$val->implementation_quantity}}</td>
                        <td class="text-center">{{$val->asset->user->fullname ?? ''}}</td>
                        @if ($checkDestroyPermission)
                            <td class="text-center">
                                @if($val->status==\App\Enums\EAssetStatus::REPORT_REPAIR||$val->status==\App\Enums\EAssetStatus::REPORT_MAINTENANCE)
                                    <button type="button" style="background-color: white; border:none;"
                                            wire:click="deleteIdAsset({{$val->id}})" data-target="#DeleteNeed"
                                            data-toggle="modal">
                                        <img src="/images/trash.svg" alt="trash">
                                    </button>
                                @else
                                    <button type="button" style="background-color: white; border:none;" disabled>
                                        <img src="/images/trash.svg" alt="trash">
                                    </button>
                                @endif
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr class="text-danger">
                        <td colspan="12" class="text-center">{{__('common.message.no_data')}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        {{$data->links()}}
    </div>
    <div class="modal fade modal-fix" wire:ignore.self id="confirm_notice_maintenance" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="submit">
                    <div class="modal-header">
                        <h6 class="text_center">{{__('data_field_name.asset.maintenance_repair_approval_form')}}
                            -{{__('data_field_name.asset.maintenance_property_maintenance')}}</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-header">
                        <div class="form-group info-request">
                            <label class="text_center">{{__('data_field_name.asset.number_report')}}</label>
                            <input type="text" value="{{$detailNotice->number_report ?? ''}}" name="number_report"
                                   disabled>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 pd-col">
                                    <div class="info-asset">
                                        @livewire('admin.asset.maintenance.asset-info')
                                    </div>
                                </div>
                                <div class="col-6 pd-col-15">
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.asset_quantity')}}</label>
                                        <input type="number"
                                               value="{{$detailNotice->implementation_quantity ?? ''}}" disabled>
                                        @if(!empty($errorQuantity))
                                            <div class="text-danger mb-3">{{$errorQuantity}}</div>
                                        @endif
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.repair_date')}}
                                            / {{__('data_field_name.asset.maintenance_insurance')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice?reFormatDate($detailNotice->implementation_date, 'd/m/Y') : ''}}"
                                               disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.maintenance_Estimated_completion_date')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice?reFormatDate($detailNotice->estimated_completion_date, 'd/m/Y') : ''}}"
                                               disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.maintenance_performer')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice->user->fullname?? ''}}" disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.maintenance_implementing_agencies')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice->provider->name ?? ''}}" disabled>
                                    </div>
                                </div>
                                <div class="col-6 pd-col-15">
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.estimated_cost')}}</label>
                                        <input type="text"
                                               value="{{$detailNotice->estimated_cost ?? ''}}" disabled>
                                    </div>
                                    <div class="form-group info-request">
                                        <label>{{__('data_field_name.asset.maintenance_content_repair')}}</label>
                                        <textarea class="form-control" rows="3" disabled>
                                            @if($detailNotice)
                                                @if($detailNotice->status==\App\Enums\EAssetStatus::REPORT_REPAIR)
                                                    {{$detailNotice->content_repair ?? ''}}
                                                @elseif($detailNotice->status==\App\Enums\EAssetStatus::REPORT_MAINTENANCE)
                                                    {{$detailNotice->rule_maintenance->content_maintenance ?? ''}}
                                                @endif
                                            @endif
                                        </textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-cancel" id="close-confirm-maintenance"
                                data-dismiss="modal">{{__('common.button.cancel')}}
                        </button>
                        <button type="button" class="btn btn-primary"
                                wire:click.prevent="confirm()">{{__('common.status.status_approval')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('livewire.common.modal._modalConfirmExport')
    <div wire:ignore.self class="modal fade" id="DeleteNeed" tabindex="-1"
         aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{__('common.confirm_message.confirm_title')}}</h4>
                    <p>{{__('common.confirm_message.are_you_sure_delete')}}</p>
                </div>
                <div class="group-btn2 text-center  pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal"
                            id="close-delete-need">{{__('common.button.no')}}</button>
                    <button type="button" wire:click.prevent="delete()" class="btn btn-save"
                            data-dismiss="modal">{{__('common.button.yes')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>

