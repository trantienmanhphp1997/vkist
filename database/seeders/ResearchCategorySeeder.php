<?php

namespace Database\Seeders;

use App\Models\ResearchCategory as ModelsResearchCategory;
use Illuminate\Database\Seeder;

class ResearchCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ModelsResearchCategory::factory()->times(20)->create();
    }
}
