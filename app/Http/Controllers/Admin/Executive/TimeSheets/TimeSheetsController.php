<?php

namespace App\Http\Controllers\Admin\Executive\TimeSheets;

use App\Http\Controllers\Controller;
use DB;

class TimeSheetsController extends Controller {
    public function index() {
        return view('admin.executive.time-sheets.index');
    }

    public function MyTimeSheet() {
    	return view('admin.executive.my-time-sheets.index');
    }

    public function show($id) {
        return view('admin.executive.time-sheets.show', ['id' => $id]);
    }
}
