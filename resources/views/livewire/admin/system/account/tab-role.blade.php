<div class="inner-tab ">
    <div class="mb-3">
        <label for="">{{ __('data_field_name.system.account.add_roles') }}</label>
        @livewire('component.advanced-select', [
            'name' => 'role',
            'modelName' => App\Models\Role::class,
            'searchBy' => ['name', 'code'],
            'columns' => ['name', 'code', 'note'],
            'itemTemplate' => ':name<br><small>:code</small>',
            'conditions' => [
                ['id', 'not in', $existRoleIds]
            ],
            'placeholder' => __('data_field_name.system.account.search_role'),
        ], key(Str::random(10)))
    </div>

    <?php $i = 1; ?>
    <table class="table">
        <thead>
            <tr class="border-radius">
                <th scope="col" class="border-radius-left">{{ __('data_field_name.system.role.stt') }}</th>
                <th scope="col">{{ __('data_field_name.system.role.code') }}</th>
                <th scope="col">{{ __('data_field_name.system.role.name') }}</th>
                <th scope="col">{{ __('data_field_name.system.role.content') }}</th>
                <th scope="col" class="border-radius-right"></th>
            </tr>
        </thead>
        <tbody>

            @foreach ($readyRoleList as $id => $role)
                <tr class="text-danger">
                    <td  class="border-radius-left">{{ $i++ }}</td>
                    <td >{{ $role['code'] }}</td>
                    <td >{{ $role['name'] }}</td>
                    <td >{{ $role['note'] }}</td>
                    <td ><button wire:loading.attr="disabled" wire:click="removeReadyRole({{ $id }})" class="btn"><img src="/images/trash.svg" alt="trash"/></button></td>
                </tr>
            @endforeach
            @foreach ($roleList as $role)
                <tr>
                    <td  class="border-radius-left">{{ $i++ }}</td>
                    <td >{{ $role->code }}</td>
                    <td >{{ $role->name }}</td>
                    <td >{{ $role->note }}</td>
                    <td ></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $roleList->links() }}
</div>
