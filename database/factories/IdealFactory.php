<?php

namespace Database\Factories;

use App\Enums\EIdeaStatus;
use App\Models\Ideal;
use App\Models\ResearchCategory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class IdealFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ideal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $researchCategory = ResearchCategory::all()->random();
        return [
            'code' => 'ID' . $this->faker->numberBetween(1000, 9999),
            'name' => $this->faker->colorName,
            'proposer' => $this->faker->name,
            'agencies' => $this->faker->company,
            'necessity' => $this->faker->sentence(10),
            'target' => $this->faker->sentence(10),
            'content' => $this->faker->sentence(20),
            'result' => $this->faker->sentence(15),
            'execution_time' => $this->faker->numberBetween(10, 30) . 'days',
            'fee' => $this->faker->numberBetween(100, 999),
            'status' => EIdeaStatus::JUST_CREATED,
            'admin_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
            'research_field_id' => $researchCategory->id
        ];
    }
}
