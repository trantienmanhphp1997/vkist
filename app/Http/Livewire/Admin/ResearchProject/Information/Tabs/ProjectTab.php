<?php

namespace App\Http\Livewire\Admin\ResearchProject\Information\Tabs;

use App\Component\DepartmentRecursive;
use App\Models\Department;
use App\Models\ResearchCategory;
use App\Models\ResearchProject;
use App\Models\TaskWork;
use App\Models\Topic;
use Exception;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ProjectTab extends Component
{
    public $researchID;
    public $name;
    public $project_code;
    public $contract_code;
    public $research_category_id;
    public $start_date;
    public $end_date;
    public $topic_code ;
    public $description;
    public $project_scale = 1;
    public $leader_id;
    public $admin_id;
    public $status = 0;
    public $statusProject;
    public $topicID;
    public $note;
    public $department_id;

    public $departments = [];
    protected $listeners = [
        'save-research-data-information' => 'save',
        'edit-research-project' => 'update',
        'set-start-time' => 'setStartTime',
        'set-end-time' => 'setEndTime'
    ];


    public function mount($id = 0)
    {
        if ($id) {
            $researchInformation = ResearchProject::query()->findOrFail($id);
            $this->researchID = $id;
            $this->name = $researchInformation->name;
            $this->project_code = $researchInformation->project_code;
            $this->contract_code = $researchInformation->contract_code;
            $this->start_date = $researchInformation->start_date;
            $this->research_category_id = $researchInformation->research_category_id;
            $this->end_date = $researchInformation->end_date;
            $this->description = $researchInformation->description;
            $this->project_scale = $researchInformation->project_scale;
            $this->status = $researchInformation->status;
            $this->note = $researchInformation->note;
            $this->department_id = $researchInformation->department_id;
        }

        $this->departments = DepartmentRecursive::hierarch(Department::all());

    }

    public function render()
    {
        $researchCategorys = ResearchCategory::all();
        $statusList = [
            ['name' => __('research/project.status.wait_approval'), 'id' => ResearchProject::WAIT_APPROVAL],
            ['name' => __('research/project.status.in_process'), 'id' => ResearchProject::IN_PROCESSING],
            ['name' => __('research/project.status.unfinished'), 'id' => ResearchProject::UNFINISHED],
            ['name' => __('research/project.status.acceptance'), 'id' => ResearchProject::ACCEPTANCE],
            ['name' => __('research/project.status.completed'), 'id' => ResearchProject::COMPLETED],
            ['name' => __('research/project.status.cancel'), 'id' => ResearchProject::CANCEL],
        ];
        if ($this->contract_code) {
            $topic = Topic::where(['contract_code' => $this->contract_code])->first();
            if ($topic) {
                // get member of topic
                $this->emit('set-topic-id', $topic->id);
                $this->topic_code = $topic->code ;
                $this->project_code = $topic->code.$this->contract_code;
                $this->name = $topic->name;
                $this->topicID = $topic->id;
                $this->start_date = $topic->start_date ;
                $this->end_date = $topic->end_date ;
                $this->research_category_id = $topic->research_category_id ;
                $this->description = $topic->overview;
                $this->resetValidation('contract_code');
            }
            if (!$topic) {
                $this->addError('contract_code', __('research/project.error.no-contract-code-valid'));
            }

        } else {
            $this->project_code = '';
            $this->name = '';
        }

        if ($this->name || $this->project_code || $this->contract_code
            || $this->description || $this->project_scale || $this->research_category_id
            || $this->end_date || $this->start_date || $this->status
        ) {
            $this->emit('disable-btn', false);
        }
//,'start_date'=>$this->start_date,'end_date'=>$this->end_date
        return view('livewire.admin.research-project.information.tabs.project-tab', ['statusList' => $statusList,'researchCategorys' => $researchCategorys]);

    }

    public function checkIfFieldEmpty()
    {
        $this->emit('disable-btn', true);
    }

    protected function rules()
    {
        $taskWorkDetails = TaskWork::query()->where('topic_id', $this->topicID)->where('status', '!=', TaskWork::STATUS_DONE_TASK)->get();

        return [
            'project_code' => 'required',
            'contract_code' => 'required|max:48|regex:/^[a-zA-Z0-9)\\(\/._-]+$/u|unique:research_project,contract_code,' . $this->researchID,
            'name' => ['required', 'regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . ']+$/', 'max:255','unique:research_project,name,' . $this->researchID],
            'research_category_id' => 'required',
            'start_date' => 'required|before:end_date| after:' . date('Y/m/d'),
            'end_date' => 'required',
            'project_scale' => 'required|max:5|regex:/^[0-9]+$/u',
            'description' => ['required', 'regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/ ]+$/', 'max:1000'],
            'note' => ['required', 'regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/ ]+$/', 'max:1000'],
            'department_id' => 'required',
            'status' => 'required' . ($taskWorkDetails->count() > 0 ? '|not_in:' . ResearchProject::ACCEPTANCE . ',' . ResearchProject::COMPLETED : '')
        ];
    }
    protected function validationAttributes()
    {
        return [
            'name' => __('research/project.information.tab-project.name'),
            'contract_code' => __('research/project.information.tab-project.contract-code'),
            'status' => __('research/project.information.tab-project.status'),
            'project_code' => __('research/project.information.tab-project.project-code'),
            'research_category_id' => __('research/project.information.tab-project.research-type'),
            'start_date' => __('research/project.information.tab-project.start'),
            'end_date' => __('research/project.information.tab-project.end'),
            'project_scale' => __('research/project.information.tab-project.man-month'),
            'description' => __('research/project.information.tab-project.description'),
            'note' => __('research/project.information.tab-project.note'),
            'department_id' => __('research/project.information.tab-project.department')
        ];
    }

    protected function getMessages()
    {
        return [
            'status.not_in' => __('research/project.error.invalid-status')
        ];
    }

    public function update($id)
    {
        $formData = [
            'name' => $this->name,
            'project_code' => $this->project_code,
            'contract_code' => $this->contract_code,
            'research_category_id' => $this->research_category_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'description' => $this->description,
            'project_scale' => $this->project_scale,
            'status' => $this->status == ResearchProject::UNFINISHED ? ResearchProject::DRAFT : $this->status,
            'note'=>$this->note,
            'department_id' => $this->department_id
        ];
        $researchProjectInfo = ResearchProject::findOrFail($id)->update($formData);
        if ($researchProjectInfo) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('research/project.information.update-project-information-success')]);
        } else {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('research/project.information.update-project-information-fail')]);
        }
    }


    public function save()
    {
        $this->validate();
        $formData = [
            'name' => $this->name,
            'project_code' => $this->project_code,
            'contract_code' => $this->contract_code,
            'research_category_id' => $this->research_category_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'description' => $this->description,
            'project_scale' => $this->project_scale,
            'admin_id' => Auth::id(),
            'status' => $this->status == ResearchProject::UNFINISHED ? ResearchProject::DRAFT : $this->status,
            'note' => $this->note,
            'department_id' => $this->department_id
        ];

        $researchProjectInfo = ResearchProject::create($formData);
        if ($researchProjectInfo) {
            session()->flash('success', __('research/project.information.create-project-information-success'));
        } else {
            session()->flash('error', __('research/project.information.create-project-information-fail'));
        }

        return redirect()->route('admin.research-project.information.edit', $researchProjectInfo->id);
    }

    public function setStartTime($date)
    {
        $this->start_date = $date;
    }

    public function setEndTime($date)
    {
        $this->end_date = $date;
    }

    public function updatedStatus($value)
    {
        $this->resetValidation();
        try {
            $this->validateOnly('status');
            $this->status = $value;
        } catch(Exception $e) {
            $this->status = 0;
            throw $e;
        }
    }
}
