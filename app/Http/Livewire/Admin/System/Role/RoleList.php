<?php

namespace App\Http\Livewire\Admin\System\Role;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Http\Livewire\Base\BaseLive;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\RoleHasPermission;

class RoleList extends BaseLive {

    public $searchTerm;
    public $filterStatus;
    public $idDuplicate;
    public $name;
    public $code;

    public function render() {
        $query = Role::query();
        $searchTerm = $this->searchTerm;
        $status = $this->filterStatus;

        if ($status != null) {
            $query->where('status', $status);
        }

        if (!empty($searchTerm)) {
            $query->where('unsign_text','like','%'. strtolower(removeStringUtf8($searchTerm)).'%');
        }

        $data = $query->orderBy('id','DESC')->paginate($this->perPage);

        $userList = User::get();

        return view('livewire.admin.system.role.role-list',['data' => $data, 'userList' => $userList]);
    }

    public function deleteRole($id) {
        $this->deleteId = $id;
    }

    public function delete() {
        $role = Role::findOrFail($this->deleteId);
        if (!empty($role)) {
            $modelRole = DB::table('model_has_roles')->where('role_id', $this->deleteId)->get();
            if ($modelRole->isNotEmpty()) {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.role.text_error_delete')]);
                return false;
            }

            $rolePermissions = RoleHasPermission::where("role_has_permissions.role_id", $this->deleteId)
                ->pluck('role_has_permissions.permission_id')
                ->all();

            if (!empty($rolePermissions)) {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.role.text_error_delete_function')]);
                return false;
            }

            $role->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
            return true;
        }
    }

    public function updateStatusRole($id) {
        $role = Role::findOrFail($id);
        $role->status = $role->status == 1 ? 0 : 1;
        $role->save();

        if ($role->status != 1) {
            $dataUpdate = ['model_type' => '1'];
        } else {
            $dataUpdate = ['model_type' => 'App\Models\User'];
        }

        $data = DB::table('model_has_roles')->get();
        if ($data->isNotEmpty()) {
            DB::table('model_has_roles')->where('role_id', $id)
                ->update($dataUpdate);
        }
        session()->flash('success', __("notification.common.success.update"));
    }

    public function showPopupDuplicate($idDuplicate)
    {
        $this->idDuplicate =  $idDuplicate;
        $role = Role::findOrFail($this->idDuplicate);
        $this->name = $role->name . '_copy';
        $this->code = $role->code . '_copy';
        $this->resetValidation();
    }

    public function duplicate()
    {
        $this->validation();

        $data = Role::findOrFail($this->idDuplicate);
        $unsign_text = strtolower(removeStringUtf8($data->name) . ' ' . removeStringUtf8($data->code));

        $role = Role::create([
            'name' => $this->name,
            'code' => $this->code,
            'guard_name' => 'web',
            'status' => $data->status,
            'note' => $data->note,
            'unsign_text' => $unsign_text,
        ]);

        // Save data table role_has_permissions
        $rolePermissions = $this->getRolePermissions($data->id);
        if (!empty($rolePermissions)) {
            $role->givePermissionTo($rolePermissions);
        }

        $this->emit('close-modal-duplicate');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.duplicate')]);
    }

    public function validation()
    {
        $this->validate([
            'name' => ['required', 'unique:roles', 'max:48', 'regex:/^[a-zA-Z0-9'.config('common.special_character.VN').'\_\.\,\-\(\)\/ ]+$/'],
            'code' => ['required', 'unique:roles', 'max:48', 'regex:/^[a-zA-Z0-9_]+$/'],
        ], [], [
            'name' => __('data_field_name.system.role.name'),
            'code' => __('data_field_name.system.role.code'),
        ]);
    }

    public function getRolePermissions ($idRole) {
        $rolePermissions = RoleHasPermission::where("role_has_permissions.role_id", $idRole)
            ->pluck('role_has_permissions.permission_id')
            ->all();

        $dataPermission = [];
        foreach ($rolePermissions as $permission) {
            $dataPermission[] = $permission;
        }

        return $dataPermission;
    }
}
