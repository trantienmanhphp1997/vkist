<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class UserListController extends Controller
{
    //
   public function index(Request $request){

        return view('admin.user.index');
    }

}
