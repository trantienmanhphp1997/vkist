<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeExpenseToTopicActualFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_actual_fee', function (Blueprint $table) {
            $table->tinyInteger('type_expense')->nullable()->comment('Loai khoan chi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_actual_fee', function (Blueprint $table) {
            $table->dropColumn('type_expense');
        });
    }
}
