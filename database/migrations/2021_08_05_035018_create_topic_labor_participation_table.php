<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicLaborParticipationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic_labor_participation', function (Blueprint $table) {
            $table->id()->comment('Cong lao dong truc tiep tham gia de tai');
            $table->string('wage_coefficient', 10)->comment('He so tien cong');
            $table->integer('number_workday')->comment('So ngay lam viec');
            $table->bigInteger('total')->comment('Tong tien cong');
            $table->unsignedBigInteger('topic_has_user_info_id')->nullable()->comment('Map voi topic_has_user_info');
            $table->foreign('topic_has_user_info_id')->references('id')->on('topic_has_user_info');
            $table->bigInteger('state_capital')->nullable()->comment('Nguon von nha nuoc');
            $table->bigInteger('other_capital')->nullable()->comment('Nguon khac');
            $table->foreignId('topic_fee_detail_id')->nullable()->constrained('topic_fee_detail');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topic_labor_participation');
    }
}
