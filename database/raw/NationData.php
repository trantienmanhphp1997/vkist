<?php

return [

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'VIET NAM',
        'order_number' => '1',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'KOREA, REPUBLIC OF',
        'order_number' => '2',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'AFGHANISTAN',
        'order_number' => '3',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ÅLAND ISLANDS',
        'order_number' => '4',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ALBANIA',
        'order_number' => '5',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ALGERIA',
        'order_number' => '6',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'AMERICAN SAMOA',
        'order_number' => '7',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ANDORRA',
        'order_number' => '8',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ANGOLA',
        'order_number' => '9',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ANGUILLA',
        'order_number' => '10',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ANTARCTICA',
        'order_number' => '11',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ANTIGUA AND BARBUDA',
        'order_number' => '12',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ARGENTINA',
        'order_number' => '13',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ARMENIA',
        'order_number' => '14',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ARUBA',
        'order_number' => '15',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'AUSTRALIA',
        'order_number' => '16',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'AUSTRIA',
        'order_number' => '17',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'AZERBAIJAN',
        'order_number' => '18',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BAHAMAS',
        'order_number' => '19',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BAHRAIN',
        'order_number' => '20',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BANGLADESH',
        'order_number' => '21',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BARBADOS',
        'order_number' => '22',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BELARUS',
        'order_number' => '23',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BELGIUM',
        'order_number' => '24',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BELIZE',
        'order_number' => '25',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BENIN',
        'order_number' => '26',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BERMUDA',
        'order_number' => '27',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BHUTAN',
        'order_number' => '28',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BOLIVIA',
        'order_number' => '29',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BOSNIA AND HERZEGOVINA',
        'order_number' => '30',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BOTSWANA',
        'order_number' => '31',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BOUVET ISLAND',
        'order_number' => '32',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BRAZIL',
        'order_number' => '33',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BRITISH INDIAN OCEAN TERRITORY',
        'order_number' => '34',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BRUNEI DARUSSALAM',
        'order_number' => '35',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BULGARIA',
        'order_number' => '36',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BURKINA FASO',
        'order_number' => '37',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'BURUNDI',
        'order_number' => '38',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CAMBODIA',
        'order_number' => '39',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CAMEROON',
        'order_number' => '40',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CANADA',
        'order_number' => '41',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CAPE VERDE',
        'order_number' => '42',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CAYMAN ISLANDS',
        'order_number' => '43',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CENTRAL AFRICAN REPUBLIC',
        'order_number' => '44',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CHAD',
        'order_number' => '45',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CHILE',
        'order_number' => '46',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CHINA',
        'order_number' => '47',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CHRISTMAS ISLAND',
        'order_number' => '48',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'COCOS (KEELING] ISLANDS',
        'order_number' => '49',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'COLOMBIA',
        'order_number' => '50',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'COMOROS',
        'order_number' => '51',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CONGO',
        'order_number' => '52',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
        'order_number' => '53',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'COOK ISLANDS',
        'order_number' => '54',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'COSTA RICA',
        'order_number' => '55',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CÔTE D’IVOIRE',
        'order_number' => '56',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CROATIA',
        'order_number' => '57',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CUBA',
        'order_number' => '58',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CYPRUS',
        'order_number' => '59',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'CZECH REPUBLIC',
        'order_number' => '60',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'DENMARK',
        'order_number' => '61',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'DJIBOUTI',
        'order_number' => '62',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'DOMINICA',
        'order_number' => '63',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'DOMINICAN REPUBLIC',
        'order_number' => '64',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ECUADOR',
        'order_number' => '65',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'EGYPT',
        'order_number' => '66',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'EL SALVADOR',
        'order_number' => '67',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'EQUATORIAL GUINEA',
        'order_number' => '68',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ERITREA',
        'order_number' => '69',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ESTONIA',
        'order_number' => '70',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ETHIOPIA',
        'order_number' => '71',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'FALKLAND ISLANDS (MALVINAS]',
        'order_number' => '72',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'FAROE ISLANDS',
        'order_number' => '73',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'FIJI',
        'order_number' => '74',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'FINLAND',
        'order_number' => '75',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'FRANCE',
        'order_number' => '76',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'FRENCH GUIANA',
        'order_number' => '77',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'FRENCH POLYNESIA',
        'order_number' => '78',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'FRENCH SOUTHERN TERRITORIES',
        'order_number' => '79',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GABON',
        'order_number' => '80',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GAMBIA',
        'order_number' => '81',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GEORGIA',
        'order_number' => '82',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GERMANY',
        'order_number' => '83',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GHANA',
        'order_number' => '84',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GIBRALTAR',
        'order_number' => '85',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GREECE',
        'order_number' => '86',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GREENLAND',
        'order_number' => '87',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GRENADA',
        'order_number' => '88',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GUADELOUPE',
        'order_number' => '89',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GUAM',
        'order_number' => '90',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GUATEMALA',
        'order_number' => '91',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GUINEA',
        'order_number' => '92',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GUINEA-BISSAU',
        'order_number' => '93',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'GUYANA',
        'order_number' => '94',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'HAITI',
        'order_number' => '95',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'HEARD ISLAND AND MCDONALD ISLANDS',
        'order_number' => '96',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'HOLY SEE (VATICAN CITY STATE]',
        'order_number' => '97',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'HONDURAS',
        'order_number' => '98',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'HONG KONG',
        'order_number' => '99',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'HUNGARY',
        'order_number' => '100',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ICELAND',
        'order_number' => '101',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'INDIA',
        'order_number' => '102',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'INDONESIA',
        'order_number' => '103',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'IRAN, ISLAMIC REPUBLIC OF',
        'order_number' => '104',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'IRAQ',
        'order_number' => '105',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'IRELAND',
        'order_number' => '106',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ISRAEL',
        'order_number' => '107',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ITALY',
        'order_number' => '108',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'JAMAICA',
        'order_number' => '109',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'JAPAN',
        'order_number' => '110',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'JORDAN',
        'order_number' => '111',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'KAZAKHSTAN',
        'order_number' => '112',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'KENYA',
        'order_number' => '113',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'KIRIBATI',
        'order_number' => '114',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'KOREA, DEMOCRATIC PEOPLE’S REPUBLIC OF',
        'order_number' => '115',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'KUWAIT',
        'order_number' => '116',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'KYRGYZSTAN',
        'order_number' => '117',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LAO PEOPLE’S DEMOCRATIC REPUBLIC',
        'order_number' => '118',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LATVIA',
        'order_number' => '119',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LEBANON',
        'order_number' => '120',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LESOTHO',
        'order_number' => '121',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LIBERIA',
        'order_number' => '122',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LIBYAN ARAB JAMAHIRIYA',
        'order_number' => '123',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LIECHTENSTEIN',
        'order_number' => '124',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LITHUANIA',
        'order_number' => '125',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'LUXEMBOURG',
        'order_number' => '126',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MACAO',
        'order_number' => '127',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
        'order_number' => '128',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MADAGASCAR',
        'order_number' => '129',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MALAWI',
        'order_number' => '130',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MALAYSIA',
        'order_number' => '131',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MALDIVES',
        'order_number' => '132',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MALI',
        'order_number' => '133',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MALTA',
        'order_number' => '134',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MARSHALL ISLANDS',
        'order_number' => '135',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MARTINIQUE',
        'order_number' => '136',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MAURITANIA',
        'order_number' => '137',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MAURITIUS',
        'order_number' => '138',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MAYOTTE',
        'order_number' => '139',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MEXICO',
        'order_number' => '140',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MICRONESIA, FEDERATED STATES OF',
        'order_number' => '141',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MOLDOVA, REPUBLIC OF',
        'order_number' => '142',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MONACO',
        'order_number' => '143',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MONGOLIA',
        'order_number' => '144',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MONTSERRAT',
        'order_number' => '145',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MOROCCO',
        'order_number' => '146',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MOZAMBIQUE',
        'order_number' => '147',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'MYANMAR',
        'order_number' => '148',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NAMIBIA',
        'order_number' => '149',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NAURU',
        'order_number' => '150',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NEPAL',
        'order_number' => '151',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NETHERLANDS',
        'order_number' => '152',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NETHERLANDS ANTILLES',
        'order_number' => '153',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NEW CALEDONIA',
        'order_number' => '154',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NEW ZEALAND',
        'order_number' => '155',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NICARAGUA',
        'order_number' => '156',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NIGER',
        'order_number' => '157',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NIGERIA< /td>',
        'order_number' => '158',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NIUE',
        'order_number' => '159',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NORFOLK ISLAND',
        'order_number' => '160',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NORTHERN MARIANA ISLANDS',
        'order_number' => '161',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'NORWAY',
        'order_number' => '162',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'OMAN',
        'order_number' => '163',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PAKISTAN',
        'order_number' => '164',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PALAU',
        'order_number' => '165',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PALESTINIAN TERRITORY, OCCUPIED',
        'order_number' => '166',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PANAMA',
        'order_number' => '167',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PAPUA NEW GUINEA',
        'order_number' => '168',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PARAGUAY',
        'order_number' => '169',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PERU',
        'order_number' => '170',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PHILIPPINES',
        'order_number' => '171',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PITCAIRN',
        'order_number' => '172',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'POLAND',
        'order_number' => '173',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PORTUGAL',
        'order_number' => '174',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'PUERTO RICO',
        'order_number' => '175',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'QATAR',
        'order_number' => '176',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'RÉUNION',
        'order_number' => '177',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ROMANIA',
        'order_number' => '178',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'RUSSIAN FEDERATION',
        'order_number' => '179',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'RWANDA',
        'order_number' => '180',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAINT HELENA',
        'order_number' => '181',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAINT KITTS AND NEVIS',
        'order_number' => '182',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAINT LUCIA',
        'order_number' => '183',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAINT PIERRE AND MIQUELON',
        'order_number' => '184',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAINT VINCENT AND THE GRENADINES',
        'order_number' => '185',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAMOA',
        'order_number' => '186',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAN MARINO',
        'order_number' => '187',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAO TOME AND PRINCIPE',
        'order_number' => '188',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SAUDI ARABIA',
        'order_number' => '189',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SENEGAL',
        'order_number' => '190',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SERBIA AND MONTENEGRO',
        'order_number' => '191',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SEYCHELLES',
        'order_number' => '192',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SIERRA LEONE',
        'order_number' => '193',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SINGAPORE',
        'order_number' => '194',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SLOVAKIA',
        'order_number' => '195',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SLOVENIA',
        'order_number' => '196',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SOLOMON ISLANDS',
        'order_number' => '197',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SOMALIA',
        'order_number' => '198',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SOUTH AFRICA',
        'order_number' => '199',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
        'order_number' => '200',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SPAIN',
        'order_number' => '201',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SRI LANKA',
        'order_number' => '202',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SUDAN',
        'order_number' => '203',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SURINAME',
        'order_number' => '204',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SVALBARD AND JAN MAYEN',
        'order_number' => '205',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SWAZILAND',
        'order_number' => '206',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SWEDEN',
        'order_number' => '207',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SWITZERLAND',
        'order_number' => '208',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'SYRIAN ARAB REPUBLIC',
        'order_number' => '209',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TAIWAN, PROVINCE OF CHINA',
        'order_number' => '210',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TAJIKISTAN',
        'order_number' => '211',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TANZANIA, UNITED REPUBLIC OF',
        'order_number' => '212',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'THAILAND',
        'order_number' => '213',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TIMOR-LESTE',
        'order_number' => '214',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TOGO',
        'order_number' => '215',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TOKELAU',
        'order_number' => '216',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TONGA',
        'order_number' => '217',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TRINIDAD AND TOBAGO',
        'order_number' => '218',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TUNISIA',
        'order_number' => '219',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TURKEY',
        'order_number' => '220',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TURKMENISTAN',
        'order_number' => '221',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TURKS AND CAICOS ISLANDS',
        'order_number' => '222',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'TUVALU',
        'order_number' => '223',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'UGANDA',
        'order_number' => '224',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'UKRAINE',
        'order_number' => '225',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'UNITED ARAB EMIRATES',
        'order_number' => '226',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'UNITED KINGDOM',
        'order_number' => '227',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'UNITED STATES',
        'order_number' => '228',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'UNITED STATES MINOR OUTLYING ISLANDS',
        'order_number' => '229',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'URUGUAY',
        'order_number' => '230',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'UZBEKISTAN',
        'order_number' => '231',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'VANUATU',
        'order_number' => '232',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'Vatican City State see HOLYSEEVA',
        'order_number' => '233',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'VENEZUELA',
        'order_number' => '234',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'VIRGIN ISLANDS, BRITISH',
        'order_number' => '235',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'VIRGIN ISLANDS, U.S.',
        'order_number' => '236',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'WALLIS AND FUTUNA',
        'order_number' => '237',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'WESTERN SAHARA',
        'order_number' => '238',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'YEMEN',
        'order_number' => '239',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ZAMBIA',
        'order_number' => '240',
    ],

    [
        'type' => '4',
        'v_key' => 'Nation',
        'v_value' => 'ZIMBABWE',
        'order_number' => '241',
    ],
];
