<div class="col-md-10 col-xl-11 box-detail box-user">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.asset.list_title')}} </a>
                    \ <span>{{__('data_field_name.asset.repair_maintenance')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="title">{{__('data_field_name.asset.repair_maintenance')}}</h3>
                    </div>
                    <div class="col-md-6">
                        @if($checkCreatePermission)
                            <div class="form-group float-right">
                                <a href="#" data-toggle="modal" data-target="#createRule">
                                    <button class="btn btn-viewmore-news mr0 "><img src="/images/plus2.svg"  alt="plus">
                                        {{__('data_field_name.asset.create_rule_maintenance')}}
                                    </button>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="information information-asset">
                    <div class="inner-tab pt0">
                        <div class="group-tabs">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                       href="#list-repair" role="tab" aria-controls="list-repair" aria-selected="true">
                                        <span>{{__('data_field_name.asset.asset_need_maintenance')}}</span></a>
                                    <a class="nav-item nav-link " id="nav-profile-tab" data-toggle="tab"
                                       href="#list-maintenance" role="tab" aria-controls="list-maintenance"
                                       aria-selected="false">
                                        <span>{{__('data_field_name.asset.asset_maintenancing')}}</span></a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade active show" id="list-repair" role="tabpanel"
                                     aria-labelledby="nav-home-tab"></br>
                                    @livewire('admin.asset.maintenance.list-need-maintenance')
                                </div>
                                <div class="tab-pane fade" id="list-maintenance"></br>
                                    @livewire('admin.asset.maintenance.list-maintenancing')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @livewire('admin.asset.maintenance.rule-maintenance')
</div>
<script>
    $("document").ready(function () {
        window.livewire.on('closeRule', () => {
            document.getElementById('close-rule').click();
            document.getElementById('close-confirm-maintenance').click();
            document.getElementById('close-confirm-maintenancing').click();
            document.getElementById('close-delete-need').click();
        });
    });
</script>
