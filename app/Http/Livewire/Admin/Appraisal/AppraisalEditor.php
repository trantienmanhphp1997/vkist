<?php

namespace App\Http\Livewire\Admin\Appraisal;

use App\Enums\EAppraisalBoard;
use App\Enums\EAppraisalBoardHasUserInfo;
use App\Enums\EApproval;
use App\Enums\EResearchPlan;
use App\Enums\ETopicFee;
use App\Enums\ETopicStatus;
use App\Http\Livewire\Base\BaseTrimString;
use App\Models\AppraisalBoard;
use App\Models\AppraisalBoardHasUserInfo;
use App\Models\Approval;
use App\Models\File;
use App\Models\ResearchPlan;
use App\Models\Topic;
use App\Models\TopicFee;
use App\Models\UserInf;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class AppraisalEditor extends BaseTrimString
{

    public bool $editable = true;

    public $approvalId;
    public $approval;
    public $appraisalBoardId;
    public $appraisalBoard;

    public $validationModel;

    public array $modelTypeList = [];
    public array $modelList = [];
    public array $currentMemberList = [];
    public array $tmpMemberList = [];
    public array $roleList = [];
    public array $staffList = [];

    public array $sendInvitationStatus = [];

    // input
    public $modelType;
    public $modelId;
    public $name;
    public $appraisalDate;
    public $note;

    public $tmpStaffId;
    public $tmpRoleId;
    public $deletingMemberId;

    // text
    public $textModelName;
    public $checkDisable;

    protected $listeners = [
        'set-appraisal-date' => 'setAppraisalDate'
    ];

    protected function rules()
    {
        return [
            'name' => ['required', 'regex:/^[a-zA-Z0-9'.config('common.special_character.alphabet').'\.\,\-\(\)\/ ]+$/', 'max:100'],
            'modelType' => 'required',
            'modelId' => 'required',
            'appraisalDate' => 'required',
            'note' => 'nullable|max:255',
        ];
    }

    protected function getValidationAttributes()
    {
        $attributes = [
            'name' => __('data_field_name.approval_topic.appraisal_name'),
            'modelType' => __('data_field_name.approval_topic.profile_type'),
            'modelId' => '',
            'appraisalDate' => __('data_field_name.approval_topic.appraisal_date'),
            'note' => __('data_field_name.common_field.descriptions'),
        ];

        if ($this->modelType == EAppraisalBoard::TYPE_TOPIC) {
            $attributes['modelId'] = __('data_field_name.approval_topic.topic');
        } elseif ($this->modelType == EAppraisalBoard::TYPE_COST) {
            $attributes['modelId'] = __('data_field_name.approval_topic.type_cost_estimate');
        } elseif ($this->modelType == EAppraisalBoard::TYPE_PLAN) {
            $attributes['modelId'] = __('data_field_name.approval_topic.type_research_plan');
        }

        return $attributes;
    }

    public function mount()
    {
        $this->appraisalBoard = AppraisalBoard::query()->findOrNew($this->appraisalBoardId);
        $this->approval = Approval::query()->findOrNew($this->approvalId);
        $this->modelTypeList = $this->getModelTypeList();
        $this->roleList = $this->getRoleList();
        $this->staffList = $this->getStaffList();

        if($this->appraisalBoard->exists) {
            $this->validationModel = $this->appraisalBoard->getValidationModel();
        }

        $this->sendInvitationStatus = [EAppraisalBoardHasUserInfo::STATUS_UNSENT, EAppraisalBoardHasUserInfo::STATUS_REJECT];

        $this->name = $this->appraisalBoard->name;

        $this->modelType = (!$this->approval->exists) ? $this->appraisalBoard->type : $this->approval->type;

        if ($this->modelType == EApproval::TYPE_TOPIC) {
            $this->modelId = (!$this->approval->exists) ? $this->appraisalBoard->topic_id : $this->approval->topic_id;
        } elseif ($this->modelType == EApproval::TYPE_COST) {
            $this->modelId = (!$this->approval->exists) ? $this->appraisalBoard->topic_fee_id : $this->approval->topic_fee_id;
        } elseif ($this->modelType == EApproval::TYPE_PLAN) {
            $this->modelId = (!$this->approval->exists) ? $this->appraisalBoard->research_plan_id : $this->approval->research_plan_id;
        }

        $this->appraisalDate = !empty($this->appraisalBoard->appraisal_date) ? $this->appraisalBoard->appraisal_date : date('Y-m-d');
        $this->note = $this->appraisalBoard->note;

        $this->currentMemberList = $this->getCurrentMemberList();
        $this->modelList = $this->getModelList();

        $this->checkDisable = false;
        if (isset($this->appraisalBoard->id)) {
            $dataApproval = Approval::where('appraisal_board_id', $this->appraisalBoard->id)->first();
            if (isset($dataApproval->status) && $dataApproval->status > EApproval::STATUS_WAIT_ASSESSMENT) {
                $this->checkDisable = true;
            }
        }
    }

    public function render()
    {
        $this->dispatchBrowserEvent('render-event');
        return view('livewire.admin.appraisal.appraisal-editor');
    }

    public function getModelTypeList()
    {
        $types = EAppraisalBoard::getListType();

        if ($this->approval->exists) {
            $types = array_filter($types, fn ($key) => $key == $this->approval->type, ARRAY_FILTER_USE_KEY);
        } else {
            unset($types[EAppraisalBoard::TYPE_IDEAL]);
        }
        return $types;
    }

    public function getModelList()
    {
        $query = null;
        if ($this->modelType == EApproval::TYPE_TOPIC) {
            $this->textModelName = __('data_field_name.approval_topic.type_topic');
            $query = Topic::query()->where('status', ETopicStatus::STATUS_COMPLETE_PROFILE);
        } elseif ($this->modelType == EApproval::TYPE_COST) {
            $this->textModelName = __('data_field_name.approval_topic.type_cost_estimate');
            $query = TopicFee::query()->where('status', ETopicFee::STATUS_COMPLETE_PROFILE);
        } elseif ($this->modelType == EApproval::TYPE_PLAN) {
            $this->textModelName = __('data_field_name.approval_topic.type_research_plan');
            $query = ResearchPlan::query()->where('status', '=', EResearchPlan::STATUS_COMPLETE_PROFILE);
        } else {
            $this->textModelName = '';
            return [];
        }

        $list = $query->get()->mapWithKeys(fn ($item) => [$item['id'] => $item['name'] ?? $item['code'] ?? ''])->all();

        if ($this->approval->exists && isset($list[$this->modelId])) {
            $list = [
                $this->modelId => $list[$this->modelId]
            ];
        }

        if(!empty($this->validationModel)) {
            $this->modelId = $this->validationModel->id;
            $list = [
                $this->validationModel->id => $this->validationModel->name ?? $this->validationModel->code
            ];
        }

        return $list;
    }

    public function getCurrentMemberList()
    {
        if (empty($this->appraisalBoard)) {
            return [];
        }
        return $this->appraisalBoard
            ->members()
            ->with('userInfo', 'userInfo.academicLevel')
            ->get()
            ->mapWithKeys(function ($item) {
                return [
                    $item['id'] => [
                        'user_info_id' => $item['user_info_id'],
                        'fullname' => $item['userInfo']['fullname'] ?? '',
                        'academic' => $item['userInfo']['academicLevel']['v_value'] ?? '',
                        'role_id' => $item['role_id'],
                        'status' => $item['status']
                    ]
                ];
            })
            ->all();
    }

    public function getRoleList()
    {
        return EAppraisalBoardHasUserInfo::appraisalRole();
    }

    public function getStaffList()
    {
        return UserInf::query()
            ->with('academicLevel')
            ->get()
            ->mapWithKeys(function ($item) {
                return [
                    $item['id'] => [
                        'user_info_id' => $item['id'],
                        'fullname' => $item['fullname'],
                        'academic' => $item['academicLevel']['v_value'] ?? ''
                    ]
                ];
            })
            ->all();
    }

    public function updatedModelType()
    {
        $this->modelList = $this->getModelList();
    }

    public function addTmpMember()
    {
        // chưa nhập đủ thông tin
        if (empty($this->tmpStaffId) || empty($this->tmpRoleId)) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.appraisal.select_full')]);
            return;
        }

        // đã tồn tại thành viên
        $f1 = Arr::first($this->currentMemberList, fn ($item) => $item['user_info_id'] == $this->tmpStaffId);
        if (isset($this->tmpMemberList[$this->tmpStaffId]) || !empty($f1)) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.common.fail.add')]);
            return;
        }

        // nếu là chủ tịch thẩm định
        if ($this->tmpRoleId == EAppraisalBoardHasUserInfo::ROLE_CHAIRMAN) {
            $f1 = Arr::first($this->tmpMemberList, fn ($item) => $item['role_id'] == EAppraisalBoardHasUserInfo::ROLE_CHAIRMAN);
            $f2 = Arr::first($this->currentMemberList, fn ($item) => $item['role_id'] == EAppraisalBoardHasUserInfo::ROLE_CHAIRMAN);

            if (!empty($f1) || !empty($f2)) {
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => __('notification.appraisal.chairman_already_exists')]);
                return;
            }
        }

        $this->tmpMemberList[$this->tmpStaffId] = $this->staffList[$this->tmpStaffId] + ['role_id' => $this->tmpRoleId];

        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.add')]);

        $this->reset('tmpStaffId', 'tmpRoleId');
    }

    public function deleteTmpMember($tmpStaffId)
    {
        if (!isset($this->tmpMemberList[$tmpStaffId])) {
            return;
        }
        unset($this->tmpMemberList[$tmpStaffId]);
    }

    public function deleteMember($memberId)
    {
        if (empty($memberId)) {
            return;
        }
        AppraisalBoardHasUserInfo::query()->where('id', $memberId)->delete();
        $this->currentMemberList = $this->getCurrentMemberList();
        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.delete')]);
        $this->dispatchBrowserEvent('close-delete-modal-event');
    }

    public function save()
    {
        $message = $this->appraisalBoard->exists ? __('notification.common.success.update') : __('notification.common.success.add');
        $this->validate();

        // lưu thông tin
        $this->appraisalBoard->name = $this->name;
        $this->appraisalBoard->type = $this->modelType;
        $this->appraisalBoard->appraisal_date = $this->appraisalDate;
        $this->appraisalBoard->note = $this->note;
        $this->appraisalBoard->status = EAppraisalBoard::STATUS_INCOMPLETE;
        if ($this->modelType == EApproval::TYPE_TOPIC) {
            $this->appraisalBoard->topic_id = $this->modelId;
        } elseif ($this->modelType == EApproval::TYPE_COST) {
            $this->appraisalBoard->topic_fee_id = $this->modelId;
        } elseif ($this->modelType == EApproval::TYPE_PLAN) {
            $this->appraisalBoard->research_plan_id = $this->modelId;
        }

        $this->appraisalBoard->save();

        // thêm thành viên
        if (count($this->tmpMemberList) > 0) {
            $records = array_map(function ($item) {
                return [
                    'user_info_id' => $item['user_info_id'],
                    'role_id' => $item['role_id'],
                    'status' => EAppraisalBoardHasUserInfo::STATUS_UNSENT,
                    'admin_id' => auth()->id(),
                    'appraisal_board_id' => $this->appraisalBoard->id
                ];
            }, $this->tmpMemberList);
            $this->appraisalBoard->members()->createMany($records);
        }

        // thêm file
        File::query()
            ->where('model_name', AppraisalBoard::class)
            ->where('type', config('common.type_upload.AppraisalBoard'))
            ->where('admin_id', auth()->id())
            ->where('model_id', null)
            ->update([
                'model_id' => $this->appraisalBoard->id,
            ]);

        $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => $message]);
        $this->resetValidation();

        $this->dispatchBrowserEvent('close-appraisal-modal-event');
    }

    public function cancel()
    {
        $this->resetValidation();
    }

    public function sendInvitation($memberId)
    {
        if (empty($memberId) || !isset($this->currentMemberList[$memberId])) {
            return;
        }

        AppraisalBoardHasUserInfo::query()->where('id', $memberId)->update([
            'status' => EAppraisalBoardHasUserInfo::STATUS_SEND
        ]);

        // gửi file
        File::where('model_name', AppraisalBoardHasUserInfo::class)
            ->where('type', config('common.type_upload.AppraisalBoardHasUserInfo'))
            ->where('admin_id', auth()->id())
            ->where('model_id', null)
            ->update([
                'model_id' => $memberId,
            ]);

        $this->currentMemberList = $this->getCurrentMemberList();

        $this->dispatchBrowserEvent('close-send-invite-modal-event');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.appraisal.send_success')]);
    }

    public function setAppraisalDate($value) {
        $this->appraisalDate = date('Y-m-d', strtotime($value['appraisal-date']));
    }
}
