<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Tài khoản hoặc mật khẩu không hợp lệ.',
    'locked' => "Tài khoản của bạn đã bị khoá",
    'throttle' => 'Bạn đã thử quá nhiều lần. Vui lòng thử lại sau :seconds giây.',
    "error" => [
        "username" => [
            "required" => "Tên đăng nhập là bắt buộc",
            "max" => "Tên đăng nhập tối đa 191 ký tự"
        ],
        "password" => [
            "required" => "Mật khẩu là bắt buộc",
            "max" => "c"
        ]
    ],
    'vi' => [
        'login' => 'Đăng nhập',
        'title' => 'Chào mừng bạn quay trở lại !',
        'name'=>'Tên đăng nhập',
        'password' => 'Mật khẩu',
        'remember-me'=>'Nhớ mật khẩu',
        'forgot-password'=>'Quên mật khẩu',
        'reset-password'=>'Đặt lại mật khẩu',
        'btn-reset-password'=>'Gửi liên kết đặt lại mật khẩu',
        'email'=>'Email của bạn',
        'already-have-account'=>'Đã có tài khoản? ',
        'sign-in'=>'Đăng nhập'
    ],
    'en' => [
        'login' => 'Login',
        'title' => 'Welcome back, you’ve been missed!' ,
        'name'=>'User name',
        'password' => 'Password',
        'remember-me'=>'Remember me',
        'forgot-password'=>'Forgot password',
        'reset-password'=>'Reset Password',
        'btn-reset-password'=>'Send Password Reset Link',
        'email'=>'Your email',
        'already-have-account'=>'Already have an account? ',
        'sign-in'=>'Sign In',
    ]
];
