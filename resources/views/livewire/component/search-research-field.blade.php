<div>
    <input
        type="text"
        class="form-control w-100"
        placeholder="{{__('common.place_holder.search')}} ..."
        wire:model="query"
        wire:keydown.escape="resetInput"
        wire:keydown.tab="resetInput"
        wire:keydown.enter="resetInput"
        id="search-input-combobox"
        autocomplete="off"
        @if($disable) disabled @endif
    />
{{--    <div wire:loading class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg">--}}
{{--        <div class="list-item">Searching...</div>--}}
{{--    </div>--}}

        <div class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg search_bar_div @if(!$displayOption)d-none @endif"
        id="option-list"
        style="max-height: 250px; overflow-y: auto;overflow-x: hidden">
            @if(!empty($data))
                @foreach($data as $i => $item)
                    <a wire:click="setInput({{ $item['id'] }},'{{ $item['name'] }}')"
                        class="search_bar"
                    >{{ $item['name'] }}</a>
                @endforeach
            @else
                <div class="search_bar">{{__('common.message.empty_search')}}</div>
            @endif
        </div>

</div>
<script>
    $("document").ready(function() {
        optionListElement = $("#option-list");
        optionListElement.scroll( e => {
            // console.log(optionListElement[0].scrollTop,optionListElement[0].clientHeight)
            if(optionListElement[0].scrollTop + optionListElement[0].clientHeight === optionListElement[0].scrollHeight) {
                Livewire.emit('loadMore');
            }
        })
        $('#search-input-combobox').focus(() => {
            Livewire.emit('displayOption');
        })
        window.addEventListener('reset-height-scroll', () => {
            optionListElement[0].scrollTop = 0;
        })
        $('#search-input-combobox').blur(() => {
            Livewire.emit('hideOption');
        })
    })
</script>
