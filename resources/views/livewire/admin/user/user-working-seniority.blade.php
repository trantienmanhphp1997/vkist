<div class="inner-tab">
    <label class="text-danger float-right font-weight-bold">
        <em>
            @if(!empty($systemConfig) && isset($systemConfig->content['seniority_does_not_include_vacation_day']) && $systemConfig->content['seniority_does_not_include_vacation_day']['value'] == false)
                {{__('data_field_name.working_process.have_config_leave_day')}}
            @else
                {{__('data_field_name.working_process.no_config_leave_day')}}
            @endif
        </em>
    </label>
    <table class="table border-bottom">
        <thead>
            <tr class="border-radius">
                <th scope="col" class="border-radius-left text-center">{{__('data_field_name.working_process.start_time')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.working_process.end_time')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.working_seniority.work_time')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.working_seniority.corresponding_month')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.working_seniority.corresponding_day')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.common_field.department')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.common_field.position')}}</th>
                <th scope="col" class="text-center">{{__('data_field_name.working_process.work_unit')}}</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($contract))
                <tr>
                    <td class="text-center">{{ !empty($contract && !empty($contract->start_date)) ? date('d/m/Y',strtotime($contract->start_date)):__('common.empty') }}</td>
                    <td class="text-center">
                        @if(!empty($userInfoLeave))
                            {{ !empty(!empty($userInfoLeave->leave_date)) ? date('d/m/Y',strtotime($userInfoLeave->leave_date)):__('common.empty') }}
                        @endif
                    </td>
                    <td class="text-center">{{$year}}</td>
                    <td class="text-center">{{$month}}</td>
                    <td class="text-center">{{$day}}</td>
                    <td class="text-center">{{ $user->department != null ? $user->department->name : '' }}</td>
                    <td class="text-center">{{ $user->position != null ? $user->position->v_value : '' }}</td>
                    <td class="text-center">VKIST</td>
                </tr>
            @else
                <tr>
                    <td colspan="12" class="text-center">{{__('data_field_name.working_process.empty')}}</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
