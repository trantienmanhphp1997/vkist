<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

use Faker\Factory as Factory;

class DeparmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = require_once(database_path('raw/DepartmentData.php'));
        foreach($data as $department) {
            Department::query()->firstOrCreate([
                'code' => $department['code'] ?? null
            ], [
                'name' => $department['name'] ?? null,
                'note' => $department['note'] ?? null,
                'status' => $department['status'] ?? null,
            ]);
        }
    }
}
