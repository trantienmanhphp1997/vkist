@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.asset.repair_maintenance')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    @livewire('admin.asset.maintenance.list-all')
@endsection
