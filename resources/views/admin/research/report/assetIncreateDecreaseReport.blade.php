<div class="report-main-text text-center">
    <h3> {{__('research/report.report1-page.title')}} </h3>
    <p>{{__('research/report.report1-page.sub-title')}}</p>
</div>
<div class="col-md-12 download-report-btn">
    @if(checkPermission('admin.asset.report.download'))
        <div class=" float-right">
            {{--                                wire:click='export'--}}
            <button type="button"  class="btn par6" data-toggle="modal" data-target="#exportModal"
                    title="{{__('common.button.export_file')}}"><img src="/images/filterdown.svg" alt="filter-down"></button>
        </div>
    @endif
</div>
<div class="table-responsive" style="  overflow: auto;
  white-space: nowrap;">
    <table class="table table-general working-plan">
        <thead>
        <tr class="border-radius text-center">
            <th scope="col">
                {{__('research/report.table.stt')}}
            </th>
            <th scope="col">
                {{__('research/report.report1-page.table.code')}}
            </th>
            <th scope="col">
                {{__('research/report.report1-page.table.name')}}
            </th>
            <th scope="col">
                {{__('research/report.report1-page.table.begin-period')}}
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report1-page.table.up')}}
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid">
                        {{__('research/report.report1-page.table.added')}}
                    </div>
                    <div class="col-md-6">
                        {{__('research/report.report1-page.table.move-to')}}
                    </div>
                </div>
            </th>
            <th scope="col" class="text-center">
                {{__('research/report.report1-page.table.down')}}
                <div class="row">
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report1-page.table.move-away')}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report1-page.table.lost')}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{__('research/report.report1-page.table.cancel')}}
                    </div>
                    <div class="col-md-3">
                        {{__('research/report.report1-page.table.liquidation')}}
                    </div>
                </div>
            </th>
            <th scope="col">
                {{__('research/report.report1-page.table.end-period')}}
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($assetMovement as $category)
            <tr class="border-radius text-center">
                <td colspan="3" class="text-left" style="    background: gray;
    color: white;">
                    {{$category->code}} - {{$category->name}}
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    {{$category->countAssetBeginQuanity}}
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    <div class="row">
                        <div class="col-md-6" style="border-right: 1px solid">
                            {{$category->countAssetBeginQuanity}}
                        </div>
                        <div class="col-md-6">
                            0
                        </div>
                    </div>
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    <div class="row">
                        <div class="col-md-3" style="border-right: 1px solid">
                            0
                        </div>
                        <div class="col-md-3" style="border-right: 1px solid">
                            {{$category->countAssetLosted}}
                        </div>
                        <div class="col-md-3" style="border-right: 1px solid">
                            {{$category->countAssetCancel}}
                        </div>
                        <div class="col-md-3">
                            {{$category->countAssetLidation}}
                        </div>
                    </div>
                </td>
                <td class="text-center" style="    background: gray;
    color: white;">
                    {{$category->countAssetEndQuanity}}
                </td>
            </tr>
            @if (count($category->asset) > 0)
                @foreach($category->asset as $key => $asset)
                <tr class="border-radius text-center">
                    <td class="text-center">
                        {{$key + 1 }}
                    </td>
                    <td class="text-center">
                        {{$asset->code}}
                    </td>
                    <td class="text-center">
                        {{$asset->name}}
                    </td>
                    <td class="text-center">
                        {{$asset->beginPeriodQuanity}}
                    </td>
                    <td class="text-center">
                        <div class="row">
                            <div class="col-md-6" style="border-right: 1px solid">
                                {{$asset->beginPeriodQuanity}}
                            </div>
                            <div class="col-md-6">
                                0
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="row">
                            <div class="col-md-3" style="border-right: 1px solid">
                                0
                            </div>
                            <div class="col-md-3" style="border-right: 1px solid">
                                {{$asset->losted}}
                            </div>
                            <div class="col-md-3" style="border-right: 1px solid">
                                {{$asset->cancel}}
                            </div>
                            <div class="col-md-3">
                                {{$asset->liquidation}}
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        {{$asset->endPeriodQuanity}}
                    </td>
                </tr>
            @endforeach
            @else
                <tr>
                    <td colspan="9">
                        <div class="title-approve text-center">
                            <span>{{ __('common.message.empty_search') }}</span>
                        </div>
                    </td>
                </tr>

            @endif


        @endforeach

        <tr class="border-radius">
            <td class="text-center" colspan="3" style="background: #F6F6F7;">
                {{__('research/report.report1-page.table.sum')}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetMovement->totalAssetBeginQuanity}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetBeginQuanity}}
                    </div>
                    <div class="col-md-6">
                        0
                    </div>
                </div>
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                <div class="row">
                    <div class="col-md-3" style="border-right: 1px solid">
                        0
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetLost}}
                    </div>
                    <div class="col-md-3" style="border-right: 1px solid">
                        {{$assetMovement->totalAssetCancel}}
                    </div>
                    <div class="col-md-3">
                        {{$assetMovement->totalAssetLiquidation}}
                    </div>
                </div>
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetMovement->totalAssetEndQuanity}}
            </td>
        </tr>
        </tbody>
    </table>
                        @if (count($assetMovement) > 0)
                            {{ $assetMovement->links() }}
                        @else
                            <div class="title-approve text-center">
                                <span>{{ __('common.message.empty_search') }}</span>
                            </div>
                        @endif
</div>
