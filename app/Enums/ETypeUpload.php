<?php

namespace App\Enums;

class ETypeUpload {

    const TOPIC_OVERVIEW = 7;
    const TOPIC_ATTACHMENT = 8;
    const TOPIC_PRESENTATION = 9;
    const BUDGET = 10;
    const FORM_CLAIM = 16;
    const USER_WORKING_PROCESS = 30;
    const WORK_PLAN = 34;
    const SHOPPING_ATTACHMENT = 99; 
    const TASK_WORK_HAS_RESULT_PLAN = 100;
}
