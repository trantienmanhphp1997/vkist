<?php

namespace App\Http\Livewire\Admin\Asset\AssetAdd;

use App\Enums\EAssetTypeDate;
use App\Enums\EAssetUserType;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use App\Models\AssetCategory;
use App\Models\Department;
use App\Models\UserInf;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use App\Http\Livewire\Base\BaseTrimString;


class TabWarranty extends BaseTrimString

{
    public $assetTypeDate;

    public $expiry_warranty_date = '';
    public $warranty_date = '';
    public $warranty_type_date = '';
    public $condition_warranty = '';

    public $maintenance_date = '';
    public $maintenance_type_date = '';
    public $maintenance_start_date = '';

    public $status = '';
    public $user_info_id;
    public $implementation_date;
    public $number_report = '';
    public $category_id;
    public $quantity;
    public $implementation_quantity;

    public $allocate_quantity = '';
    public $expiryWarrantyDate;
    protected $listeners = [
        'saveTabAdd',
        'loadTypeManage',
        'quantity',
        'set-expiry_warranty_date' => 'setExpiryWarrantyDate',
        'set-maintenance_start_date' => 'setMaintenanceStart',
        'set-implementation_date' => 'setImplementationDate',
    ];

    public function mount()
    {
        $this->assetTypeDate = [
            EAssetTypeDate::DAY => __('data_field_name.asset.day'),
            EAssetTypeDate::MONTH => __('data_field_name.asset.month'),
            EAssetTypeDate::YEAR => __('data_field_name.asset.year'),
        ];
    }

    public function render()
    {
        $query = UserInf::query();
        $user = $query->get();
        $data_department = [];

        $type_manage = '';

        if ($this->user_info_id) {
            $data_department = UserInf::findOrFail($this->user_info_id);
        }

        if ($this->category_id && $this->status == 1) {
                $type = AssetCategory::findOrFail($this->category_id);
                $type_manage = $type->type_manage;

                if ($type_manage == \App\Enums\EAssetCategoryTypeManage::CODE && $type_manage != null) {
                    $this->implementation_quantity = 1;
                    $this->allocate_quantity = $this->implementation_quantity;
                } else if ($type_manage == \App\Enums\EAssetCategoryTypeManage::QUANTITY && $this->implementation_quantity) {
                        $this->allocate_quantity = $this->implementation_quantity;
                }
                if ($this->implementation_date){
                    $this->emit('loadAssetDate',$this->implementation_date);
                }
        }

        $this->getValueWarranty();
        $this->dispatchBrowserEvent('render-event');
        return view('livewire.admin.asset.asset-add.tab-warranty', ['user' => $user, 'data_department' => $data_department,
            'type_manage' => $type_manage
        ]);
    }

    public function loadTypeManage($category_id)
    {
        $this->category_id = $category_id;
    }
    public function quantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setExpiryWarrantyDate($data) {
        $this->expiry_warranty_date = date('Y-m-d', strtotime($data['expiry_warranty_date']));
    }
    public function setMaintenanceStart($data) {
        $this->maintenance_start_date = date('Y-m-d', strtotime($data['maintenance_start_date']));
    }
    public function setImplementationDate($data) {
        $this->implementation_date = date('Y-m-d', strtotime($data['implementation_date']));

    }

    public function getValueWarranty()
    {
        //để lấy default leaveDate là ngày hiện tại
//        if(is_null($this->expiryWarrantyDate)) {
//            $this->expiryWarrantyDate = date('Y-m-d');
//        }
        $this->emit('getValueWarranty', $this->warranty_date, $this->warranty_type_date, $this->expiry_warranty_date,
            $this->condition_warranty, $this->maintenance_date,
            $this->maintenance_type_date, $this->maintenance_start_date,
            $this->status, $this->user_info_id,
            $this->implementation_date, $this->number_report, $this->allocate_quantity
        );
    }

    public function saveTabAdd()
    {
        if(is_null($this->implementation_date)){
            $this->implementation_date = date('Y-m-d');
        }
        if ($this->status) {
            $quantityAsset =$this->quantity;
            $this->validate([
                'warranty_date' => 'max:5',
                'condition_warranty' => ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:255'],
                'maintenance_date' => 'max:5',
                'number_report' => 'required|max:10',
                'implementation_quantity'=>'numeric|lte:'.$quantityAsset
            ], [
            ], [
                'warranty_date' => __('data_field_name.asset.warranty_info'),
                'condition_warranty' => __('data_field_name.asset.warranty_condition'),
                'maintenance_date' => __('data_field_name.asset.maintenance_date'),
                'number_report' => __('data_field_name.asset.number_report'),
                'implementation_quantity' => __('data_field_name.asset.asset_quantity'),
            ]);
        } else {
            $this->validate([
                'warranty_date' => 'max:5',
                'condition_warranty' => ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:255'],
                'maintenance_date' => 'max:5',
            ], [], [
                'warranty_date' => __('data_field_name.asset.warranty_info'),
                'condition_warranty' => __('data_field_name.asset.warranty_condition'),
                'maintenance_date' => __('data_field_name.asset.maintenance_date'),
            ]);
        }


    }

}
