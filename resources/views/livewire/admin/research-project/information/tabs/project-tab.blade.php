<div class="row inner-tab">
    <div class="col-md-12">
        <div class="form-group">
            <label>{{__('research/project.information.tab-project.name')}} </label>
            <input type="text" class="form-control" wire:model.lazy="name"
                   readonly=""
                   placeholder="{{__('research/project.information.tab-project.name')}}">
            @error('name') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{__('research/project.information.tab-project.contract-code')}} <span
                    class="text-danger">*</span></label>
            <input type="text" class="form-control" wire:model.debounce.1000ms="contract_code"
                   @if($researchID) readonly="" @endif
                   placeholder="{{__('research/project.information.tab-project.contract-code')}}">
            @error('contract_code') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{__('research/project.information.tab-project.status')}} <span class="text-danger">*</span></label>
            <select name="topic_code" class="form-control select2" wire:model="status"
                    id="select-topic-major">
                <option value="">{{__('common.select-box.choose')}}</option>
                @foreach ($statusList as $statusOption)
                    <option
                        value="{{ $statusOption['id'] }}">{{ $statusOption['name'] }}</option>
                @endforeach
            </select>
            @error('status') <div class="text-danger">{{ $message }}</div> @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{__('research/project.information.tab-project.project-code')}}</label>
            <input type="text" class="form-control" wire:model.lazy="topic_code"
                   readonly=""
                   placeholder="{{__('research/project.information.tab-project.project-code')}}">
            @error('topic_code') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{__('research/project.information.tab-project.research-type')}}</label>
            <select name="research_category_id" class="form-control select2" style="background-color: #e9ecef;"
                    wire:model.defer="research_category_id" disabled="disabled"
                    id="select-topic-major">
                <option value="">{{__('common.select-box.choose')}}</option>
                @foreach ($researchCategorys as $researchCategory)
                    <option
                        value="{{ $researchCategory->id }}">{{ $researchCategory->name }}</option>
                @endforeach
            </select>
            @error('research_category_id')
            <div class="text-danger">{{ $message }}</div> @enderror
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>{{__('research/project.information.tab-project.start')}} </label>
            <input type="text" class="form-control"
                   readonly=""
                   placeholder="" value="{{reFormatDate($start_date,'d/m/Y')}}">
            @error('start_date') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>{{__('research/project.information.tab-project.end')}} </label>
            <input type="text" class="form-control"
                   readonly=""
                   placeholder="" value="{{reFormatDate($end_date,'d/m/Y')}}">
            @error('end_date') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>
                {{__('research/project.information.tab-project.description')}}
            </label>
            <textarea readonly="" class="form-control" rows="6" wire:model.lazy="description"
                      placeholder="  {{__('research/project.information.tab-project.description')}}"></textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>
                {{__('research/project.information.tab-project.note')}}  <span class="text-danger">*</span>
            </label>
            <textarea  class="form-control" rows="6" wire:model.lazy="note" maxlength="1000"
                      placeholder="  {{__('research/project.information.tab-project.note')}}"></textarea>
            @error('note')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label>{{__('research/project.information.tab-project.department')}} <span class="text-danger">*</span></label>
            <select wire:model.defer="department_id" class="form-control">
                <option value="">{{ __('common.select-box.choose') }}</option>
                @foreach ($departments as $department)
                    <option value="{{ $department['id'] }}">{!! $department['padLeft'] . $department['name'] !!}</option>
                @endforeach
            </select>
            @error('department_id')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <script>
        $('input[name="start_date"]').on('change', (e) => {
            Livewire.emit('set-start-time',
                document.getElementById('start_date').value,
            );
        });
        $('input[name="end_date"]').on('change', (e) => {
            Livewire.emit('set-end-time',
                document.getElementById('end_date').value,
            );
        });
    </script>
</div>


