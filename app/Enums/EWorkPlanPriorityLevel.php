<?php


namespace App\Enums;

class EWorkPlanPriorityLevel
{
    const MEDIUM = 1;
    const HIGH = 2;
    const COGENCY = 3;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::MEDIUM:
                $result = __('data_field_name.work_plan.index.medium');
                break;
            case self::HIGH:
                $result = __('data_field_name.work_plan.index.high');
                break;
            case self::COGENCY:
                $result = __('data_field_name.work_plan.index.cogency');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
