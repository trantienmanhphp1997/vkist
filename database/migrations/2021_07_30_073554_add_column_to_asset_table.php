<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset', function (Blueprint $table) {
            $table->bigInteger('unit_id')->nullable()->comment('đơn vị tính master data type =');
            $table->bigInteger('unit_price')->nullable()->comment('đơn giá');
            $table->foreignId('department_id')->nullable()->constrained('department')->comment('đơn vị quản lý map vs department');
            $table->foreignId('manager_id')->nullable()->constrained('user_info')->comment('người quản lý quản lý map vs user_info');
            $table->bigInteger('asset_value')->nullable()->comment('giá trị');
            $table->tinyInteger('origin')->nullable()->comment('Nguồn gốc,1:Nguồn ngân sách ODA, 2:nguồn ngân sách nhà nước ');
            $table->string('contract_number')->nullable()->comment('số hợp đồng');
            $table->foreignId('asset_provider_id')->nullable()->constrained('asset_provider')->comment('Nhà cung cấp, map vs asset_provider');
            $table->string('note',1000)->nullable()->comment('ghi chú, tối đa 1000 ký tự');
            $table->string('warranty_date_json')->nullable()->comment('kỳ và loại ngày/tháng/năm bảo hành-lưu json');
            $table->string('condition_warranty')->nullable()->comment('điều kiện bảo hành');
            $table->date('expiry_warranty_date')->nullable()->comment('hạn bảo hành');
            $table->string('maintenance_date_json')->nullable()->comment('kỳ và loại ngày/tháng/năm bảo dưỡng -lưu json');
            $table->string('other_information_json',1000)->nullable()->comment('Thông tin tùy chỉnh nhập sau-lưu json, tối đa 1000 ký tự');
            $table->date('maintenance_start_date')->nullable()->comment('bắt đầu bảo dưỡng từ');

            $table->bigInteger('distribute_value')->nullable()->comment('giá trị phân bổ');
            $table->string('distribute_left_times_json')->nullable()->comment('Số kỳ phân bổ còn lại -json');
            $table->string('distribute_number_times_json')->nullable()->comment('Số kỳ và loại ngày/tháng/năm phân bổ- lưu json');
            $table->bigInteger('distribute_value_wait')->nullable()->comment('Giá trị còn chờ phân bổ');

            $table->date('distribute_start_date')->nullable()->comment('ngày bắt đầu phân bổ');
            $table->tinyInteger('fixed_assets')->nullable()->comment('tài sản cố định 1:tick, null ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset', function (Blueprint $table) {
            $table->dropColumn('unit_id');
            $table->dropColumn('unit_price');
            $table->dropColumn('department_id');
            $table->dropColumn('user_info_id');
            $table->dropColumn('asset_value');
            $table->dropColumn('origin');
            $table->dropColumn('contract_number');
            $table->dropColumn('asset_provider_id');
            $table->dropColumn('note');
            $table->dropColumn('warranty_date_json');
            $table->dropColumn('condition_warranty');
            $table->dropColumn('expiry_warranty_date');
            $table->dropColumn('maintenance_date_json');
            $table->dropColumn('other_information_json');
            $table->dropColumn('maintenance_start_date');
            $table->dropColumn('allocated_date');
            $table->dropColumn('user_object_id');
            $table->dropColumn('number_report');
            $table->dropColumn('distribute_value');
            $table->dropColumn('distribute_number_left_times');
            $table->dropColumn('distribute_number_times_json');
            $table->dropColumn('distribute_value_wait');
            $table->dropColumn('distribute_value');
            $table->dropColumn('distribute_start_date');
        });
    }
}
