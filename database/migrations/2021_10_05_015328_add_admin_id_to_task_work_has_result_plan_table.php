<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminIdToTaskWorkHasResultPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_work_has_result_plan', function (Blueprint $table) {
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_work_has_result_plan', function (Blueprint $table) {
            $table->dropColumn('admin_id');
        });
    }
}
