<?php
$text_role = 'Role in postion';
return [
    [
        'type' => 7,
        'v_key' => $text_role,
        'v_value' => 'Trưởng đoàn',
        'order_number' => 0
    ],
    [
        'type' => 7,
        'v_key' => $text_role,
        'v_value' => 'Phó đoàn',
        'order_number' => 1
    ],
    [
        'type' => 7,
        'v_key' => $text_role,
        'v_value' => 'Thành viên đoàn',
        'order_number' => 2
    ]
];
