<?php

namespace App\Http\Livewire\Component;

use App\Enums\EResearchCategoryType;
use App\Http\Livewire\Base\BaseLive;
use App\Models\UserInf;

class SearchCombobox extends BaseLive
{
    public $disable = false;
    public $query='';
    public $data;
    public $successful=false;
    public $pageNum = 1;
    public $perPage = 9;
    public $displayOption = false;
    public $classify;

    public $model_name; //model cần tìm kiếm
    public $value_render_on_combobox; //giá trị hiển thị trên các option của combobox (tên column trong db của model)
    public $filters = []; // lọc theo những filter nào
    public $more_conditions= []; // điều kiện thêm
    public $relations = [];
    public $targetId;

    protected $listeners = [];

    protected function getListeners()
    {
        return [
                'loadMore'.$this->classify => 'loadMore',
                'displayOption'.$this->classify =>'displayOption',
                'hideOption'.$this->classify => 'hideOption',
                'set_disable_search_combobox'.$this->classify => 'setDisable',
                'reset_query_search_combobox'.$this->classify => 'resetQuery',
            ];
    }

    public function setDisable($value) {
        $this->disable = $value;
    }

    public function resetQuery() {
        $this->reset(['targetId', 'query']);
    }

    public function loadMore() {
        $this->pageNum = $this->pageNum+1;
        $this->getData();
    }
    public function displayOption() {
        $this->displayOption = true;
    }

    public function hideOption() {
        if($this->targetId) {
            $target = $this->model_name::where('id',$this->targetId)->first();
            if(is_null($target)) {
                $this->query = $target[$this->value_render_on_combobox];
            }
        }
        $this->displayOption = false;
    }

    public function resetInput()
    {
        $this->query = '';
        $this->data = [];
    }

    public function updatedQuery()
    {
        $this->query = trim($this->query);
        $this->pageNum = 1;
        $this->getData();
        $this->dispatchBrowserEvent('reset-height-scroll');
    }
    public function getData() {
        $query = $this->model_name::query();
        $query->select($this->model_name::getTableName(). '.*');
        $query->where(function ($q) { 
            foreach($this->filters as $filter) {
                $q->orWhere($this->model_name::getTableName(). '.' . $filter, 'like', '%' . $this->query . '%');
            }
        });
        foreach($this->more_conditions as $condition) {
            $query->where($condition[0], $condition[1], $condition[2]);
        }
        foreach($this->relations as $relation) {
            $query->join($relation[0],$this->model_name::getTableName(). '.id', '=', $relation[0]. '.'. $relation[1]);
        }
        $this->data = $query->limit($this->pageNum*$this->perPage)->get()->toArray();
    }
    public function mount() {
        if($this->targetId) { //xử lí trường hợp edit
            $target =  $this->model_name::where('id',$this->targetId)->first();
            if(!is_null($target)) {
                $this->query = $target[$this->value_render_on_combobox];
            }
        }
        $this->getData();
    }
    public function render()
    {
        return view('livewire.component.search-combobox');
    }
    public function setInput($id,$name){
        $this->targetId = $id;
        $this->query=$name;
        $this->displayOption=false;
        $this->emit('setTargetId'.$this->classify, $id);
    }
}
