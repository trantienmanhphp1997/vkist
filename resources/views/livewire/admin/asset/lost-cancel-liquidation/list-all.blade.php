<div class="col-md-10 col-xl-11 box-detail box-user">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.asset.list_title')}} </a>
                    \ <span>{{__('data_field_name.asset.list_all')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="title">{{__('data_field_name.asset.list_all')}}</h3>
                    </div>
                </div>
                <div class="information information-asset">
                    <div class="inner-tab pt0">
                        <div class="group-tabs">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                       href="#list-lost" role="tab" aria-controls="list-lost" aria-selected="true">
                                        <span>{{__('data_field_name.asset.lost_property')}}</span></a>
                                    <a class="nav-item nav-link " id="nav-profile-tab" data-toggle="tab"
                                       href="#list-cancel" role="tab" aria-controls="list-cancel" aria-selected="false">
                                        <span>{{__('data_field_name.asset.cancellation_property')}}</span></a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab"
                                       href="#list-liquidation" role="tab" aria-controls="list-liquidation"
                                       aria-selected="false">
                                        <span>{{__('data_field_name.asset.recommend_liquidation')}}</span></a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade active show" id="list-lost" role="tabpanel"
                                     aria-labelledby="nav-home-tab"></br>
                                    @livewire('admin.asset.lost-cancel-liquidation.list-lost')
                                </div>
                                <div class="tab-pane fade" id="list-cancel"></br>
                                    @livewire('admin.asset.lost-cancel-liquidation.list-cancel')
                                </div>
                                <div class="tab-pane fade" id="list-liquidation"></br>
                                    @livewire('admin.asset.lost-cancel-liquidation.list-liquidation')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function() {
        window.livewire.on('closeConfirm', () => {
            document.getElementById('close-confirm-lost').click();
            document.getElementById('close-confirm-cancel').click();
            document.getElementById('close-confirm-liquidation').click();
            document.getElementById('close-delete-lost').click();
            document.getElementById('close-delete-cancel').click();
            document.getElementById('close-delete-liquidation').click();
        });
    });
</script>
