<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('chi muc');
            $table->unsignedBigInteger('user_info_id')->nullable()->comment('Map voi user_info ');
            $table->foreign('user_info_id')->references('id')->on('user_info');
            $table->string('contract_code', 15)->comment('so hop dong');
            $table->tinyInteger('contract_type')->comment('1 thu viec, 2 hop dong xac dinh thoi han, 3 hop dong khong xac dinh thoi han, 4 hoc vien, 5 hop dong mua vu, 6 hop dong dich vu');
            $table->dateTime('start_date')->comment('ngay bat dau');
            $table->dateTime('end_date')->nullable()->comment('ngay ket thuc');
            $table->integer('basic_salary')->comment('luong co ban');
            $table->integer('social_insurance_salary')->comment('luong dong BHXH');
            $table->string('bank_account_number', 14)->comment('tai khoan ngan hang');
            $table->unsignedBigInteger('bank_name_id')->nullable()->comment('Map voi master_data, type = 17');
            $table->foreign('bank_name_id')->references('id')->on('master_data');
            $table->tinyInteger('working_type')->comment('1 toan thoi gian, 2 ban thoi gian, 3 cong tac vien');
            $table->unsignedBigInteger('coefficients_salary_id')->nullable()->comment('Map voi master_data, type = 18');
            $table->foreign('coefficients_salary_id')->references('id')->on('master_data');
            $table->string('note',1000)->comment('ghi chu');
            $table->softDeletes();
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
$table->string('search_unsign', 1000)->nullable()->comment('tim kiem tieng viet');
$table->index(['search_unsign']);
            $table->tinyInteger('status')->comment('1 dang lam viec, -1 ngung lam viec');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->unsignedBigInteger('duration_id')->nullable()->comment('Map voi contract_duration');
            $table->foreign('duration_id')->references('id')->on('contract_duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract');
    }
}
