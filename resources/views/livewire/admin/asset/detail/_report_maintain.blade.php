
<div wire:ignore.self class="modal fade modal-fix" id="report_maintain" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.asset.maintain_title')}}</h6>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pd-col">
                                <div class="info-asset">
                                    @include('livewire.admin.asset.detail._asset-info')
                                    <div class="info-item">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="d-flex item-tỉtle">
                                                        <p>{{__('data_field_name.asset.info')}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name">{{__('data_field_name.asset.asset_code_')}}</p>
                                                        <p class="content">: {{$dataItem?$dataItem->code:''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="item-field">
                                                        <p class="name">{{__('data_field_name.asset.guarantee_date')}}</p>
                                                        <p class="content">
                                                            : {{$dataItem?reFormatDate($dataItem->guarantee_date??'','d-m-Y'):''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.perform_date')}}<span class="text-danger">*</span></label>
                                    <input type="date" wire:model.lazy="claim_date">
                                    @error('claim_date')
                                    <span class="error text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.asset.claim_info')}} <span class="text-danger">*</span></label>
                                    <textarea  cols="30" rows="10" wire:model.lazy="content"></textarea>
                                    @error('content')
                                    <span class="error text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal" wire:click.prevent="cancel()">{{__('common.button.cancel')}}</button>
                <button type="button" class="btn btn-primary" id="close-modal5" wire:click.prevent="saveForm({{7}}, false)">{{__('common.button.perform')}}</button>
            </div>
        </div>
    </div>
</div>
