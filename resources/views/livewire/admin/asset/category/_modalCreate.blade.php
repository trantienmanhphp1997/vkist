<form wire:submit.prevent="submit">
    <div wire:ignore.self class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                    <h4 class="modal-title text-center">{{__('data_field_name.asset_category.create_new')}}</h4>
                    
                    <form>
                        <div class="form-group">
                            <label for="name">{{__('data_field_name.asset_category.name')}}<span class="text-danger">(*)</span></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" wire:model.lazy="name">
                            @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label for="code">{{__('data_field_name.asset_category.code')}}<span class="text-danger">(*)</span></label>
                            <input type="text" id="code" class="form-control @error('code') is-invalid @enderror" placeholder="" wire:model.lazy="code">
                            @error('code') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label for="note">{{__('data_field_name.asset_category.note')}}</label>
                            <textarea id="note" class="form-control @error('note') is-invalid @enderror" wire:model.lazy="note" rows="3"></textarea>
                            @error('note') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label for="type_manage">{{__('data_field_name.asset_category.type_manage')}}</label></br>
                            
                            <input type="radio" id="manage_by_code" name="type_manage" wire:model.lazy="type_manage" value="0">
                            <label for="manage_by_code">{{__('data_field_name.asset_category.manage_by_code')}}</label><br>
                            <input type="radio" id="manage_by_quantity" name="type_manage" wire:model.lazy="type_manage" value="1">
                            <label for="manage_by_quantity">{{__('data_field_name.asset_category.manage_by_quantity')}}</label><br>
                        </div>
                    </form>
                </div>
                
                <div class="modal-footer">
                    <button type="button" id="close-modal-create-asset-category" wire:click.prevent="resetInputFields()" class="btn btn-secondary close-btn" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                    <button type="submit" wire:click.prevent="store()" class="btn btn-primary">{{__('common.button.save')}}</button>
                </div>
            </div>
        </div>
    </div>
</form>