<div wire:ignore.self class="modal fade modal-fix" id="create-or-edit" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 444px !important">
        <div class="modal-content">
            <div class="modal-header">
                <h6>
                    @if(!empty($idShow))
                        {{__('data_field_name.template_form.edit')}}
                    @else
                        {{__('data_field_name.template_form.add')}}
                    @endif
                </h6>
            </div>
            <div class="modal-body" style="font-size: unset;">
                <form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pd-col">
                                <label>{{__('data_field_name.template_form.name')}}<span class="text-danger">*</span></label>
                                <div class="form-group info-request">
                                    <input wire:ignore type="text" placeholder="{{__('data_field_name.template_form.name')}}" wire:model.defer="name" class="form-control" style="border-radius: 12px">
                                </div>
                                @error('name')
                                    <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.template_form.type')}}<span class="text-danger">*</span></label>
                                    <select wire:ignore wire:model="formType" class="form-control">
                                        <option value=''>{{__('common.select-box.choose')}}</option>
                                        <option value="{{App\Enums\ETemplateFormType::BUDGET}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::BUDGET)}}</option>
                                        <option value="{{App\Enums\ETemplateFormType::GENERAL}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::GENERAL)}}</option>
                                        <option value="{{App\Enums\ETemplateFormType::ESTIMATE}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::ESTIMATE)}}</option>
                                        <option value="{{App\Enums\ETemplateFormType::PLAN}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::PLAN)}}</option>
                                        <option value="{{App\Enums\ETemplateFormType::OTHER}}">{{App\Enums\ETemplateFormType::valueToName(App\Enums\ETemplateFormType::OTHER)}}</option>
                                    </select>
                                    @error('formType')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.template_form.file')}}<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <form wire:submit.prevent="submit">
                                            <div class="form-group info-request mb-0">
                                                <input type="file"
                                                       class="custom-file-input"
                                                       onchange="uploadFile(this, @this)" id="input-file">
                                                <label for="input-file" class="attachfile-field m-auto"><span><img src="/images/Clip-blue.svg" alt=""></span></label>
                                            </div>
                                        </form>
                                    </div>
                                </div>                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    @if(!empty($listFile))
                                        <div class="col-md-12">
                                            <div class="border border-light border-top-0 p-3 rounded-bottom file-list">
                                                <div class="d-inline-flex mr-2 mb-2 p-2 bg-light align-items-center rounded">
                                                    <img src="/images/File.svg" alt="file" width="50px">
                                                    <div>
                                                        <a href="{{ asset('storage/' . $listFile->url) }}" download>
                                                            <span class="d-block mb-0" style="word-break: break-all;">{{ $listFile->file_name }}</span>
                                                            <small class="kb">{{$listFile->size_file}}</small>
                                                        </a>
                                                    </div>
                                                    <button wire:click.prevent="deleteFileEdit" class="btn text-muted">
                                                        <em class="fa fa-times"></em>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($fileUpload))
                                        <div class="col-md-12">
                                            <div class="border border-light border-top-0 p-3 rounded-bottom file-list">
                                                <div class="d-inline-flex mr-2 mb-2 p-2 bg-light align-items-center rounded">
                                                    <img src="/images/File.svg" alt="file" width="50px">
                                                    <div>
                                                        <a href="javascript:void(0)" download>
                                                            <span class="d-block mb-0" style="word-break: break-all;">{{ $fileUpload->getClientOriginalName() }}</span>
                                                            <small class="kb">{{$fileUpload->getSize() / 1024}} KB</small>
                                                        </a>
                                                    </div>
                                                    <button wire:click.prevent="deleteFile" class="btn text-muted">
                                                        <em class="fa fa-times"></em>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @error('fileUpload')
                                        <div class="text-danger mt-1" style="margin-left: 13px">{{$message}}</div>
                                    @enderror
                                    <div class="text-danger upload-error"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                <button id='btn-save' type="button" class="btn btn-primary" wire:click.prevent="saveData">{{__('common.button.save')}}</button>
            </div>
        </div>
    </div>
</div>
