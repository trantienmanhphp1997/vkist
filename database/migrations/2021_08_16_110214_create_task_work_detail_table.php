<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskWorkDetailTable extends Migration
{
       /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_work_detail', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('danh sach cong viec');
            $table->string('code', 255)->nullable()->comment('ma cong viec');
            $table->tinyInteger('priority_level')->nullable()->comment('1 khan cap , 2 trung binh , 3 cao , 4 thap');
            $table->tinyInteger('status')->nullable()->comment('trang thai : 1 chua lam , 2 dang lam , 3 dang phe duyet , 4 hoan thanh');
            $table->foreignId('topic_id')->nullable()->constrained('topic');
            $table->foreignId('task_id')->nullable()->constrained('task')->comment('nhiem vu con');
            $table->tinyInteger('work_type')->nullable()->comment('loai cong viec : 1 = cong viec chinh , 2 = cong viec phu , 3 = cong viec chi tiet ');
            $table->string('name', 500)->nullable()->comment('ten cong viec');
            $table->dateTime('start_date')->nullable()->comment('ngay bat dau');
            $table->dateTime('end_date')->nullable()->comment('ngay ket thuc');
            $table->string('description', 1000)->nullable()->comment('mo ta cong viec');

            //nguoi tham gia
            $table->foreignId('reviewer_id')->nullable()->constrained('user_info')->comment('nguoi duoc gan cong viec cua nhiem vu chinh');
            $table->foreignId('approve_id')->nullable()->constrained('user_info')->comment('nguoi phe duyet');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');

            $table->softDeletes();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->timestamps();
        });

        Schema::create('task_work_has_user_info', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('danh sach sach nguoi theo doi');
            $table->foreignId('task_work_detail_id')->nullable()->constrained('task_work_detail')->comment('task_work_id map voi task work detail');
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('nguoi theo doi');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_work_detail');
        Schema::dropIfExists('task_work_has_user_info');
    }
}
