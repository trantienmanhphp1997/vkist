<?php


namespace App\Enums;


class ESurveyRequestResult
{
    const DEFAULT = 0;
    const SUBSTANDARD = 1;
    const STANDARD = 2;

    public static function valueToName($value)
    {
        $result = "";
        switch ($value) {
            case self::DEFAULT:
                $result = __('data_field_name.request-survey-list.default');
                break;
            case self::SUBSTANDARD:
                $result = __('data_field_name.request-survey-list.substandard');
                break;
            case self::STANDARD:
                $result = __('data_field_name.request-survey-list.standard');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
