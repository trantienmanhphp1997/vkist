<?php

namespace App\Http\Livewire\Admin\User;

use App\Http\Livewire\Base\BaseLive;
use App\Models\MasterData;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use App\Enums\EMasterDataType;
use DB;
use Illuminate\Support\Facades\Config;
use Route;

class UserAcademicTab2 extends BaseLive
{
    public $user_info_id;
    public $name;
    public $school;
    public $graduated_year;
    public $score;
    public $rank_id;
    public $deleteId2 = '';
    public $editId2 = '';
    public $canEdit = true;

    protected $listeners = [
        'set-graduated-year-2' => 'setGraduatedYear'
    ];

    public function resetInputFields()
    {
        $this->name = '';
        $this->school = '';
        $this->score = '';
        $this->graduated_year = '';
        $this->rank_id = '';
        $this->editId2 = '';
        $this->deleteId2 = '';
    }
    public function openModal()
    {
        $this->emit('show');
    }
    public function render()
    {
        $query2 = \App\Models\UserAcademic::where('user_info_id', $this->user_info_id);
        $query2->leftJoin('master_data b', 'user_academic.rank_id', '=', 'b.id')->where('user_academic.type', 2);
        $query2->select('user_academic.*', DB::raw('b.v_value as rank_name'));

        $dataType2 = $query2->orderBy('user_academic.id', 'desc')->paginate(5);

        $ranks = MasterData::getDataByType(EMasterDataType::RANK);
        $model_name = \App\Models\UserAcademic::class;
        $type = Config::get('common.type_upload.UserAcademic_certificate');
        $model_id = $this->user_info_id;
        $folder = app($model_name)->getTable();
        if (checkShowMode()) {
            $this->canEdit = false;
        }
        $this->dispatchBrowserEvent('render-user-academic-tab2-event');

        return view('livewire.admin.user.user-academic-tab2', ['dataType2' => $dataType2, 'ranks' => $ranks, 'model_name' => $model_name, 'type' => $type, 'model_id' => $model_id, 'folder' => $folder]);
    }
    public function storeType2()
    {
        $this->validate([
            'name' => 'required',
            'school' => 'required',
            'graduated_year' => 'required',
        ], [], [
            'name' => __('data_field_name.user.name_certificate'),
            'school' => __('data_field_name.user.area'),
            'graduated_year' => __('data_field_name.user.create_at'),
        ]);
        if ($this->editId2) {
            \App\Models\UserAcademic::findOrFail($this->editId2)->update([
                'user_info_id' => $this->user_info_id,
                'name' => $this->name,
                'score' => $this->score,
                'school' => $this->school,
                'graduated_year' => $this->graduated_year,
                'rank_id' => $this->rank_id,
                'type' => 2,
            ]);
            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.update')]);
        } else {
            \App\Models\UserAcademic::create([
                'user_info_id' => $this->user_info_id,
                'name' => $this->name,
                'score' => $this->score,
                'school' => $this->school,
                'graduated_year' => $this->graduated_year,
                'rank_id' => $this->rank_id,
                'type' => 2,
            ]);

            $this->dispatchBrowserEvent('show-toast', ['type' => 'success', 'message' => __('notification.common.success.add')]);
        }
        $this->resetInputFields();
        $this->emit('userStore');
    }
    public function deleteId2($id)
    {
        $this->deleteId2 = $id;
    }
    public function delete2()
    {
        $data = \App\Models\UserAcademic::findOrFail($this->deleteId2);
        $data->delete();
    }
    public function editId2($id)
    {
        $this->editId2 = $id;
        $data = \App\Models\UserAcademic::findOrFail($this->editId2);
        $this->name = $data->name;
        $this->school = $data->school;
        $this->score = $data->score;
        $this->rank_id = $data->rank_id;
        $this->graduated_year = $data->graduated_year ? date('Y-m-d', strtotime($data->graduated_year)) : '';
    }

    public function setGraduatedYear($value) {
        $this->graduated_year = date('Y-m-d', strtotime($value['graduated-year-2']));
    }
}
