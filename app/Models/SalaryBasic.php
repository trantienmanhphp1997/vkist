<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalaryBasic extends Model
{
    use HasFactory;
    protected $table = 'salary_basic';
    protected $fillable = [];
}
