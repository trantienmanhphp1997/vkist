<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'vi' => [
        'login' => 'Đăng nhập',
        'title' => 'Chào mừng bạn quay trở lại !',
        'name'=>'Tên đăng nhập',
        'password' => 'Mật khẩu',
        'remember-me'=>'Nhớ mật khẩu',
        'forgot-password'=>'Quên mật khẩu',
        'reset-password'=>'Đặt lại mật khẩu',
        'btn-reset-password'=>'Gửi liên kết đặt lại mật khẩu',
        'email'=>'Email của bạn',
        'already-have-account'=>'Đã có tài khoản? ',
        'sign-in'=>'Đăng nhập'
    ],
    'en' => [
        'login' => 'Login',
        'title' => 'Welcome back, you’ve been missed!' ,
        'name'=>'User name',
        'password' => 'Password',
        'remember-me'=>'Remember me',
        'forgot-password'=>'Forgot password',
        'reset-password'=>'Reset Password',
        'btn-reset-password'=>'Send Password Reset Link',
        'email'=>'Your email',
        'already-have-account'=>'Already have an account? ',
        'sign-in'=>'Sign In',
    ]
];
