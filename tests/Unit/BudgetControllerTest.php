<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Livewire;

class BudgetControllerTest extends TestCase
{

    use WithFaker, RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testStoreBudget()
    {
        $this->actingAs($this->user)->post(route('admin.budget.store'), [
            'year_created' => now()->year,
            'estimated_code' => '123SD',
            'code' => 'AVSFF',
            'name' => 'budget',
            'total_budget' => 1000,
            'content' => 'test',
            'money_plan' => [1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000],
        ])->assertStatus(200);
    }

    public function testListBudgetLivewireComponent() {
        Livewire::actingAs($this->user)->test('admin.budget.budget-list')
        ->call('render');
    }
}
