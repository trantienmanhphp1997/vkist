<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use OwenIt\Auditing\Contracts\Auditable;

class Topic extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    protected $table = 'topic';
    protected $fillable = [
        'status',
        'check_send_file',
        'code',
        'name',
        'overview',
        'target',
        'present_note',
        'level_id',
        'type',
        'admin_id',
        'start_date',
        'end_date',
        'deleted_at',
        'created_at',
        'updated_at',
        'unsign_text',
        'user_info_id',
        'ideal_id',
        'major_id',
        'leader_id',
        'achievement_note',
        'achievement_paper_check',
        'achievement_review_check',
        'achievement_announcement_check',
        'achievement_conference_check',
        'achievement_work_check',
        'achievement_textbook_check',
        'research_category_id',
        'source',
        'necessary',
        'space_scope',
        'expected_fee',
        'submit_date',
        'parent_id',
        'contract_code',
        'increase_code',
        'gw_attach_file',
    ];

    // nó là
    public function userInfos()
    {
        return $this->belongsToMany(UserInf::class, 'topic_has_user_info', 'topic_id', 'user_info_id');
    }

    // nó là TopicHasUserInfo
    public function members()
    {
        return $this->hasMany(TopicHasUserInfo::class, 'topic_id');
    }

    public function roles()
    {
        return $this->belongsToMany(MasterData::class, 'topic_has_user_info', 'topic_id', 'role_id');
    }

    public function task(){
        return $this->hasMany(TaskWork::class ,'topic_id');
    }

    public function projectResearch()
    {
        return $this->hasOne(ResearchProject::class, 'contract_code', 'contract_code');
    }
    public function researchs()
    {
        return $this->belongsToMany(MasterData::class, 'topic_has_user_info', 'topic_id', 'research_id');
    }

    public function leader()
    {
        return $this->belongsTo(UserInf::class, 'leader_id');
    }

    public function appraisalBoard()
    {
        return $this->hasOne(AppraisalBoard::class);
    }


    public function researchCategory()
    {
        return $this->belongsTo(ResearchCategory::class, 'research_category_id');
    }

    public function level()
    {
        return $this->belongsTo(MasterData::class, 'level_id');
    }
    public function major()
    {
        return $this->belongsTo(MasterData::class, 'major_id');
    }
    public function groupUser()
    {
        return $this->hasOne(GroupUserHasTopic::class, 'topic_id', 'id');
    }
    public function ideal()
    {
        return $this->belongsTo(Ideal::class);
    }
    public function topicFeeDetails()
    {
        return $this->hasManyThrough(
            TopicFeeDetail::class,
            TopicFee::class,
            'topic_id',
            'topic_fee_id',
            'id',
            'id'
        );
    }
    public function topicFee()
    {
       return $this->hasMany(TopicFee::class, 'topic_id', 'id');
    }
    public function actualFee()
    {
        return $this->hasMany(RealityExpense::class, 'topic_id', 'id');
    }
    public function task_work()
    {
        return $this->hasMany(TaskWork::class, 'topic_id', 'id');
    }
    public function approval(){
        return $this->hasMany(Approval::class,'topic_id','id');
    }



    public function transformAudit(array $data): array
    {
        $dataRequest = request()->all();
        $note = '';
        $noteReject = $dataRequest['serverMemo']['data']['noteReject'] ?? '';
        if (!empty($noteReject)) {
            $note = $noteReject;
        }
        $noteReview = $dataRequest['serverMemo']['data']['noteReview'] ?? '';
        if (!empty($noteReview)) {
            $note = $noteReview;
        }

        $data['note'] =  $note;
        return $data;
    }
    private static $searchable = [
        'code', 'name'
    ];
    public static function getListSearchAble()
    {
        return self::$searchable;
    }

    public function idea()
    {
        return $this->belongsTo(Ideal::class, 'ideal_id');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function parent() {
        return $this->belongsTo(Topic::class, 'parent_id');
    }
}
