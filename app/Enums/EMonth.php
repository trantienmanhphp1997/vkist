<?php


namespace App\Enums;


class EMonth
{
    const JANUARY = 1;
    const FEBRUARY = 2;
    const MARCH = 3;
    const APRIL = 4;
    const MAY = 5;
    const JUNE = 6;
    const JULY = 7;
    const AUGUST = 8;
    const SEPTEMBER = 9;
    const OCTOBER = 10;
    const NOVEMBER = 11;
    const DECEMBER = 12;

    public static function valueToName($value)
    {
        switch ($value) {
            case self::JANUARY:
                $result = __('common.month.jan');
                break;
            case self::FEBRUARY:
                $result = __('common.month.feb');
                break;
            case self::MARCH:
                $result = __('common.month.mar');
                break;
            case self::APRIL:
                $result = __('common.month.apr');
                break;
            case self::MAY:
                $result = __('common.month.may');
                break;
            case self::JUNE:
                $result = __('common.month.jun');
                break;
            case self::JULY:
                $result = __('common.month.jul');
                break;
            case self::AUGUST:
                $result = __('common.month.aug');
                break;
            case self::SEPTEMBER:
                $result = __('common.month.sep');
                break;
            case self::OCTOBER:
                $result = __('common.month.oct');
                break;
            case self::NOVEMBER:
                $result = __('common.month.nov');
                break;
            case self::DECEMBER:
                $result = __('common.month.dec');
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
