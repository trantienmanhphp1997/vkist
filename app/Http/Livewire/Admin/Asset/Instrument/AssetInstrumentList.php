<?php

namespace App\Http\Livewire\Admin\Asset\Instrument;

use Livewire\Component;
use App\Models\Instrument;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;


class AssetInstrumentList extends BaseLive
{
    public $searchTerm;
    
    public function render()
    {
        $query = Instrument::query();
        if (!empty($this->searchTerm)) {
            $query->where('instument.unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')->get();
        }

        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 25;
        $data = $query->orderBy('id', 'asc')->paginate($perPage);
        return view('livewire.admin.asset.instrument.asset-instrument-list', compact('data'));
    }
}
