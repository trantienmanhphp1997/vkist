<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GwApproveResponse extends Model
{
    use HasFactory;
    protected $table="gw_approve_response";
    protected $fillable = [
        'name',
        'model_id',
        'model_name',
        'request_id',
        'admin_id',
        'gw_document_id',
        'gw_date_approve_time',
        'gw_status',
        'gw_userid',
        'gw_empno',
        'gw_comment',
        'gw_next_users',
        'gw_title',
    ];
}
