<?php

namespace App\Http\Livewire\Base;

use Livewire\Component;
use Livewire\WithPagination;
use App\Http\Livewire\Base\BaseTrimString;
use App\Http\Livewire\BasePermission;

abstract class BaseLive extends BaseTrimString
{
	use WithPagination;
    use BasePermission;

	public $deleteId;
    public $reset = false;
    public $searchTerm;

    public $perPage = 25;
    protected  static function paginationView()
    {
        return 'livewire.common.pagination._pagination';
    }
    public function deleteId($id){

        $this->deleteId=$id;
    }
    public function levelClicked(){

    }
    public function resetSearch(){
        $this->reset = true;

    }
    public function updatingSearchTerm() {
        $this->resetPage();
    }
    public function updatingSearchCategory() {
        $this->resetPage();
    }
    public function updatingStore(){
        dd($this->checkEditPermission);
    }
    public function updatingSetDate() {
        $this->resetPage();
    }
    public function updatingPerPage() {
        $this->resetPage();
    }
}
