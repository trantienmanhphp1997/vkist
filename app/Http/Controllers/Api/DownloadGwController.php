<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Topic;
use Illuminate\Http\Request;

class DownloadGwController extends Controller
{
    public function download(Request $request, $docId, $fileNo)
    {
        $dataRequest = getDataRequest();
        if ($dataRequest['success']) {
            return abort(404);
        }
        $token = $dataRequest['token'];
        $urlDownload = config('common.url_gw.download');
        $url = $urlDownload. 'docu_no=' . $docId . '&file_no=' . $fileNo . '&token=' . $token;

        return redirect($url);
    }
}
