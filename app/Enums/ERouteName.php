<?php

namespace App\Enums;

class ERouteName {
    const ROUTE_BUDGET_INDEX = 'admin.budget.index';
    const ROUTE_BUDGET_APPROVED_INDEX = 'admin.budget.list-budget-approved.index';
    const ROUTE_SHOPPING_INDEX = 'admin.executive.shopping.index';
    const ROUTE_WORK_PLAN_INDEX = 'admin.general.work-plan.index';
    const ROUTE_NEWS_INDEX = 'admin.news.index';
    const ROUTE_PROFILE_INDEX = 'admin.profile.index';
    const ROUTE_ROLE_INDEX = 'admin.system.role.index';
    const ROUTE_ROLE_EDIT = 'admin.system.role.edit';
    const ROUTE_NEWS_POSTS_INDEX = 'admin.news.posts.index';
    const ROUTE_NEWS_CATEGORY_INDEX = 'admin.news.category.index';
}
