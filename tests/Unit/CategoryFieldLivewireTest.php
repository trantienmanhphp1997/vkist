<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Livewire\Admin\Research\Category\CategoryFieldList;
use App\Models\ResearchCategory;
use App\Models\User;
use Livewire;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Enums\EResearchCategoryType;



class CategoryFieldLivewireTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use RefreshDatabase;

    /** @test */
    public function can_index_category_field()
    {
        $user =$this->user;
        $this->actingAs($user)->get(route('admin.research.category-field.index'))->assertOk();
    }

    /** @test  */
    function can_create_category()
    {
        $user =$this->user;

        $this->actingAs($user);

        Livewire::test(CategoryFieldList::class)
            ->set([
                'name' => 'lĩnh vực nghiên cứu test',
                'description' => 'mô tả lĩnh vực nghiên cứu test',
                'note' => 'note',
                'type' => EResearchCategoryType::FIELD,
                ])
            ->call('store');

        $this->assertTrue(ResearchCategory::whereName('lĩnh vực nghiên cứu test')->exists());
    }

    /** @test  */
    function name_is_required()
    {
        $user =$this->user;

        $this->actingAs($user);

        Livewire::test(CategoryFieldList::class)
            ->set([
                'name' => '',
                ])
            ->call('store')
            ->assertHasErrors(['name' => 'required']);
    }

    /** @test  */
    function max_length_of_name_is_100()
    {
        $user =$this->user;
        $this->actingAs($user);

        Livewire::test(CategoryFieldList::class)
            ->set([
                'name' => bin2hex(random_bytes(51)), // this function create a random string with length = 51 x 2 = 102
                ])
            ->call('store')
            ->assertHasErrors(['name' => 'max']);
    }

    /** @test  */
    function max_length_of_description_is_255()
    {
        $user =$this->user;
        $this->actingAs($user);

        Livewire::test(CategoryFieldList::class)
            ->set([
                'description' => bin2hex(random_bytes(128)), // this function create a random string with length = 128 x 2 = 256
                ])
            ->call('store')
            ->assertHasErrors(['description' => 'max']);
    }

    /** @test  */
    function max_length_of_note_is_255()
    {
        $user =$this->user;
        $this->actingAs($user);

        Livewire::test(CategoryFieldList::class)
            ->set([
                'note' => bin2hex(random_bytes(128)), // this function create a random string with length = 128 x 2 = 256
                ])
            ->call('store')
            ->assertHasErrors(['note' => 'max']);
    }
}
