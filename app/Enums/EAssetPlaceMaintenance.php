<?php


namespace App\Enums;


class EAssetPlaceMaintenance {
    const UNIT = 1;
    const PROVIDER = 2;

    public static function valueToName($value = 0) {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList() {
        return [
            EAssetPlaceMaintenance::UNIT => __('data_field_name.asset.at_unit'),
            EAssetPlaceMaintenance::PROVIDER => __('data_field_name.asset.provider'),
        ];
    }
}

