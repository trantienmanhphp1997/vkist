<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="breadcrumbs">
                    <div class="breadcrumbs"><a href="{{ route('admin.reality_expense.index') }}">{{__('data_field_name.reality_expense.research_expense_control')}}</a> \ <span>{{__('data_field_name.reality_expense.revenue_and_expenditure_list')}}</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.reality_expense.expense_list')}}</h3>
                </div>
                <div class="col-md-6">
                    <div class=" float-right">
                        @if($checkCreatePermission)
                        <a href="{{route('admin.reality_expense.create')}}" title="{{__('common.button.create')}}"class="btn btn-viewmore-news mr0  mr-8 border-12">
                            <img src="../images/plus2.svg" alt="plus">{{__('data_field_name.reality_expense.create_expense')}}</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="col-md-12">
                    <div class="row box-filter mb-24">
                        <div class="d-flex filter-left">
                            <div class="search-expertise inline-block search-staff">
                            <input type="text" placeholder="{{__('data_field_name.reality_expense.search')}}" name="search" class="form-control size13" wire:model.debounce.1000ms="searchTerm" id='input_vn_name' autocomplete="off">
                                <span>
                                    <img src="../images/Search.svg" alt="search"/>
                                </span>
                            </div>
                            <div class="px-12 d-flex">
                                {{ Form::select('source', ['' =>__('data_field_name.reality_expense.select_capital')] + $capital, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchCapital']) }}
                            </div>
                            <div class="px-12 d-flex">
                                {{ Form::select('source', ['' =>__('data_field_name.reality_expense.select_payform')] + $payform, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchPayForm']) }}
                            </div>
                            <div class="px-12 d-flex">
                                {{ Form::select('source', ['' =>__('data_field_name.reality_expense.select_typepay')] + $typepay, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchTypePay']) }}
                            </div>
                            <div class="px-12 d-flex">
                                {{ Form::select('source', ['' =>__('data_field_name.reality_expense.select_status')] + $status, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchStatus']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-general">
                        <thead>
                            <tr class="border-radius">
                                <th scope="col" class="code_topic text-center">{{__('data_field_name.reality_expense.code')}}</th>
                                <th scope="col" class="topic_name text-center">{{__('data_field_name.reality_expense.topic')}}</th>
                                <th scope="col" class="type_expense text-center">{{__('data_field_name.reality_expense.expenses_type')}}</th>
                                <th scope="col" class="estimates text-center">{{__('data_field_name.reality_expense.estimates')}}</th>
                                <th scope="col" class="capital text-center">{{__('data_field_name.reality_expense.capital')}}</th>
                                <th scope="col" class="approval_costs text-center">{{__('data_field_name.reality_expense.browse')}}</th>
                                <th scope="col" class="payment text-center">{{__('data_field_name.reality_expense.payform')}}</th>
                                <th scope="col" class="type_cost text-center">{{__('data_field_name.reality_expense.typepay')}}</th>
                                <th scope="col" class="status text-center">{{__('data_field_name.reality_expense.status_dibursed')}}</th>
                                <th scope="col" class="text-center">{{__('data_field_name.reality_expense.action')}}</th>
                            </tr>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>
                            @forelse($data as $row)
                            <tr>

                                <td class=" code_topic text-center"><a href="{{route('admin.reality_expense.show',$row->id)}}">{{$row->topic->code ??''}}</a></td> <!--  Mã khoản chi -->
                                <td class=" topic_name border-radius-left"><a href="{{route('admin.reality_expense.show',$row->id)}}">{!! boldTextSearch($row->topic->name??'',$searchTerm) !!}</a></td> <!-- Đề tài  -->
                                <td class="type_expense border-radius-left">{{$row->expense_name}}</td> <!-- Loại khoản chi -->
                                <td class="estimates text-center">{{numberFormat($row->sum_capital)}}</td> <!-- Dự toán -->
                                <td class="capital text-center">{{\App\Enums\ECapital::valueToName($row->source_capital)}}</td>
                                <td class="approval_costs text-center">{{numberFormat($row->approval_costs)}}</td> <!-- Duyệt chi -->
                                <td class="payment text-center">{{\App\Enums\EPayForm::valueToName($row->payment)}}</td> <!-- Hình thức chi -->
                                <td class="type_cost text-center">{{\App\Enums\ETypePay::valueToName($row->type_cost)}}</td> <!-- Loại chi phí -->
                                <td class="status text-center">{{\App\Enums\EDisbursedStatus::valueToName($row->status)}}</td> <!-- Tình trạng giải ngân -->
                                <td class="text-center">
                                    @if($checkEditPermission)
                                    <a title="{{__('common.button.edit')}}" href="{{route('admin.reality_expense.edit',$row->id)}}">
                                        <img src="/images/edit.png" alt="edit">
                                    </a>
                                    @endif
                                    @if($checkDestroyPermission)
                                    <button title="{{__('common.button.delete')}}" type="button" style="background-color: white; border:none;" wire:click="deleteId({{$row->id}})" data-target="#exampleDeleteUser" data-toggle="modal">
                                        <img src="/images/trash.svg" alt="trash">
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr class="text-center text-danger">
                                <td colspan="12">{{__('common.message.no_data')}}</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    @if(count($data) > 0)
                    {{$data->links()}}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Delete -->
    <div wire:ignore.self class="modal fade" id="exampleDeleteUser" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title">{{__('common.confirm_message.confirm_title')}}</h4>
                    {{__('common.confirm_message.are_you_sure_delete')}}
                </div>
                <div class="group-btn2 text-center  pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.back')}}</button>
                    <button type="button" wire:click.prevent="deleteExpense()" class="btn btn-save" data-dismiss="modal">{{__('common.button.delete')}}</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("input:checkbox").attr("checked", true).click(function() {
            var addColumn = "." + $(this).attr("name");
            $(addColumn).toggle();
        });
        // $("document").ready(function() {
        //         window.livewire.on('close-modal-config-expense', () => {
        //             document.getElementById('close-modal-config-expense').click()
        //         });
        //     }); 
    </script>
</div>