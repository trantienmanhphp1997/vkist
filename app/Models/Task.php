<?php

namespace App\Models;

use App\Http\Livewire\Admin\General\WorkPlan\UserInfo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use OwenIt\Auditing\Contracts\Auditable;

class Task extends BaseModel implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    protected $table = "task";
    protected $guarded = ['*'];
    protected $fillable = [
        'name', 'start_date', 'end_date', 'topic_id', 'parent_id', 'admin_id', 'user_info_id', 'research_plan_id', 'deleted_at', 'created_at', 'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function childrenTask()
    {
        return $this->hasMany(Task::class, 'parent_id');
    }

    public function parentTask()
    {
        return $this->belongsTo(Task::class, 'parent_id');
    }

    public function userInfo()
    {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }
    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }
    public function researchPlan()
    {
        return $this->belongsTo(ResearchPlan::class, 'research_plan_id');
    }

    public function userInfos()
    {
        return $this->belongsToMany(UserInf::class, 'task_has_user_info', 'task_id', 'user_info_id');
    }
}
