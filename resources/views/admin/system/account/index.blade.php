@extends('layouts.master')
@section('title')
    <title>{{ __('data_field_name.system.account.user_list') }}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._systemLeft')
@endsection
@section('content')
    @livewire('admin.system.account.account-list')
@endsection
