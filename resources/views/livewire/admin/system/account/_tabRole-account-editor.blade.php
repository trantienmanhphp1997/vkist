<div class="inner-tab ">
    <style>
        #search-role-list {
            display: none;
            width: calc(100% - 330px);
            z-index: 998;
            background-color: #ffffff;
            top: 0px;
            right: 0px;
            box-shadow: 0px 0px 20px #212121;
        }

        #search-role-list:hover {
            display: block;
        }

        #search-role:focus~#search-role-list {
            display: block;
        }

    </style>


    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <select class="form-control" id="search-role-list" multiple="multiple">
                    @foreach ($searchRoleList as $r)
                        <option value="{{ $r->id }}" code="{{ $r->code }}">{{ $r->name ?? '' }}</option>
                    @endforeach
                </select>
                <br>
                <button wire:loading.attr="disabled" id="add-role" class="btn btn-outline-primary mt-2">{{ __('data_field_name.system.account.add_roles') }}</button>
            </div>
        </div>
    </div>

    <?php $i = 1; ?>
    <table class="table">
        <thead>
            <tr class="border-radius">
                <th scope="col" class="border-radius-left">{{ __('data_field_name.system.role.stt') }}</th>
                <th scope="col">{{ __('data_field_name.system.role.code') }}</th>
                <th scope="col">{{ __('data_field_name.system.role.name') }}</th>
                <th scope="col">{{ __('data_field_name.system.role.content') }}</th>
                <th scope="col" class="border-radius-right"></th>
            </tr>
        </thead>
        <tbody>

            @foreach ($preparedRoleList as $role)
                <tr class="text-danger">
                    <td  class="border-radius-left">{{ $i++ }}</td>
                    <td >{{ $role->code }}</td>
                    <td >{{ $role->name }}</td>
                    <td >{{ $role->note }}</td>
                    <td ><button wire:loading.attr="disabled" wire:click="removeFromPreparedRoleList({{ $role->id }})" class="btn"><img src="/images/trash.svg" alt="trash"/></button></td>
                </tr>
            @endforeach
            @foreach ($userRoleList as $role)
                <tr>
                    <td  class="border-radius-left">{{ $i++ }}</td>
                    <td >{{ $role->code }}</td>
                    <td >{{ $role->name }}</td>
                    <td >{{ $role->note }}</td>
                    <td ></td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <script>
        window.addEventListener('render-search-role-list', (data) => {
            $('#search-role-list').select2({
                placeholder: "{{ __('data_field_name.system.account.search_role') }}",
                matcher: function(params, data) {
                    let keyword = params.term.trim().toLowerCase();
                    if (typeof data.text === 'undefined') return null;
                    let text = data.text.toLowerCase();
                    if (text.indexOf(keyword) > -1) return data;
                    return null;
                }
            });

            $('#add-role').click(function() {
                let data = $('#search-role-list').select2('data');
                Livewire.emit('add-roles', data);
            });

        });
    </script>

</div>
