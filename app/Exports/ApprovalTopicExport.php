<?php

namespace App\Exports;

use App\Enums\EApproval;
use App\Enums\ETopicSource;
use App\Enums\ETopicStatus;
use App\Models\Approval;
use App\Models\Topic;
use App\Service\Community;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class ApprovalTopicExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */


    public function __construct($searchTerm, $searchStatus)
    {
        $this->order = 1;
        $this->searchTerm = $searchTerm;
        $this->searchStatus = $searchStatus;
    }

    public function collection()
    {
        $query = Approval::query()
            ->with('admin', 'appraisal.chairman.userInfo', 'topic.leader', 'topic.researchCategory')
            ->where('type', EApproval::TYPE_TOPIC);
        $status = $this->searchStatus;

        if ($this->searchTerm != null) {
            $term = strtolower(removeStringUtf8($this->searchTerm));
            $query->where('unsign_text', 'like', '%' . $term . '%')
                ->where(function ($q) use ($term) {
                    return $q->where('unsign_text', 'like', '%' . $term . '%')
                        ->with('admin')
                        ->orWhereHas('admin.info', function ($query) use ($term) {
                            return $query->where('unsign_text', 'like', '%' . $term . '%');
                        });
                });
        }

        if ($status != null) {
            $query->where('status', $status);
        }

        if (!empty(Community::listTopicIdAllowed())) {
            $query->whereIn('topic_id', Community::listTopicIdAllowed())->orWhereNull('topic_id');
        }

        //anh ta add
        return $query->get();
    }
    public function headings(): array
    {
        return [
            __('data_field_name.common_field.stt'),
            __('data_field_name.approval_topic.profile_code'),
            __('data_field_name.approval_topic.name_profile'),
            __('data_field_name.approval_topic.text_sender'),
            __('data_field_name.approval_topic.appraisal_name'),
            __('data_field_name.approval_topic.chairman'),
            __('data_field_name.common_field.status'),

            __('data_field_name.topic.code'),
            __('data_field_name.topic.name'),
            __('data_field_name.topic.field'),
            __('data_field_name.topic.expected_fee'),
            __('data_field_name.topic.start_time'),
            __('data_field_name.topic.end_time'),
            __('data_field_name.topic.topic_chairman'),
        ];
    }
    public function map($approval): array
    {
        return [
            $this->order++,
            $approval->code,
            $approval->name,
            $approval->admin->name ?? '',
            $approval->appraisal->name ?? '',
            $approval->appraisal->chairman->userInfo->fullname ?? '',
            EApproval::valueToName($approval->status),

            $approval->topic->code ?? '',
            $approval->topic->name ?? '',
            $approval->topic->researchCategory->name ?? '',
            $approval->topic->expected_fee ?? 0,
            reformatDate($approval->topic->start_date ?? date('Y-m-d')),
            reformatDate($approval->topic->end_date ?? date('Y-m-d')),
            $approval->topic->leader->fullname ?? 'Leader'
        ];
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {

            // tạo tiêu đề
            $titleCells = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
            $event->sheet->mergeCells('A1:G1');
            $event->sheet->setCellValue('A1', mb_strtoupper(__('data_field_name.approval_topic.list_review'), 'UTF-8'));
            $event->sheet->getStyle('A1:M3')->applyFromArray([
                'font' => ['bold' => true],
                'alignment' => array(
                    'horizontal' => \PHPOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                )
            ]);
            foreach($titleCells as $cell) {
                $value = $event->sheet->getCell("{$cell}3")->getValue();
                $event->sheet->mergeCells("{$cell}2:{$cell}3");
                $event->sheet->setCellValue("{$cell}2", $value);
            }

            // tạo phần thông tin đề tài
            $event->sheet->mergeCells('H2:M2');
            $event->sheet->setCellValue('H2', mb_strtoupper(__('data_field_name.approval_topic.type_topic'), 'UTF-8'));

            // tạo đường viền
        }];
    }
    public function title(): string
    {
        return __('data_field_name.approval_topic.list_review');
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
