<?php

namespace App\Http\Livewire\Admin\DecisionReward;

use App\Http\Livewire\Base\BaseLive;
use App\Enums\EDecisionRewardReceiverType;
use App\Enums\EDecisionRewardPrizeType;
use App\Component\SizeFile;
use Livewire\WithFileUploads;
use App\Component\FileUpload;
use App\Enums\EDecisionRewardDecisionType;
use App\Models\UserInfoLeave;
use App\Models\UserInf;
use App\Models\DecisionReward;
use App\Models\DecisionRewardReceiver;
use Illuminate\Support\Facades\Config;


use function Complex\theta;

class DecisionRewardCreateAndUpdate extends BaseLive
{
    //decision_reward_instance
    public $decision_reward;

    //decision_reward info
    public $name;
    public $reward_date;
    public $number_decision;
    public $user_decide_id;
    public $user_decide_name;
    public $user_decision_position;
    public $type_of_decision;
    public $reason;
    public $type_reward;
    public $decision_date;
    public $type_of_prize;
    public $total_value;
    public $base;

    //danh sách đối tượng khen thưởng thêm mới
    public $listNewReceiver = [];
    //danh sách đối tượng khen thưởng đã có(trường hợp edit)
    public $listOldReceiver = [];
    //danh sách đối tượng khen thưởng đã có, danh sách tạm, dùng để render
    public $listOldReceiverTemp = [];
    //danh sách đối tượng khen thưởng đã có cần xoá
    public $listIdOfOldReceiverNeedDelete = [];

    //thông tin đối tượng khen thưởng
    public $receiver_user;
    public $receiver_name;
    public $receiver_code;
    public $receiver_position;
    public $receiver_department;
    public $receiver_prize_value;
    public $receiver_reason;


    //file upload
    public $fileUpload;
    public $listFileUpload = [];
    public $limit_total_file = 5;
    public $limit_file_size = 255; //MB

    public $accept_file;


    //file cũ đã có sẵn của decision_reward
    public $listOldFile = [];
    //file cũ đã có sẵn của decision_reward, đây là bản tạm dùng để hiển thị cho user xem, xoá trên list nay, khi nào lưu thì mới áp dụng thay đổi vô list chính
    public $listOldFileTemp = [];
    //danh sách các file cũ mà user muốn xoá
    public $listIdOfOldFileNeedDelete = [];

    public $current_total_file;




    public $isEdit = false;

    //listeners
    protected $listeners = [
        'store-decision-reward' => 'store',
        'setTargetId1' => 'getUserDecisionInfo',
        'setTargetId2' => 'getUserReceiverInfo',
        'set-reward_date' => 'setRewardDate',
        'set-decision_date' => 'setDecisionDate',
    ];

    use WithFileUploads;

    public function mount() {
        $this->accept_file = Config::get('common.mime_type.general');
        if($this->decision_reward) {
            $this->isEdit = true;

            //decision info
            $this->name = $this->decision_reward->name;
            $this->reward_date = $this->decision_reward->reward_date ? date('Y-m-d', strtotime($this->decision_reward->reward_date)) : null;
            $this->number_decision = $this->decision_reward->number_decision;
            $this->user_decide_id = $this->decision_reward->user_decide_id;
            $user_decide =  UserInf::with('position')->find($this->user_decide_id);
            $this->user_decision_position =  $user_decide->position->v_value ?? '';
            $this->user_decide_name =  $user_decide->fullname ?? '';
            $this->type_of_decision = $this->decision_reward->type_of_decision;
            $this->reason = $this->decision_reward->reason;
            $this->type_reward = $this->decision_reward->type_reward;
            $this->decision_date = $this->decision_reward->decision_date ? date('Y-m-d', strtotime($this->decision_reward->decision_date)) : null;
            $this->type_of_prize = $this->decision_reward->type_of_prize;
            $this->total_value = numberFormat($this->decision_reward->total_value);
            $this->base = $this->decision_reward->base;

            //receiver info
            $this->listOldReceiver = $this->decision_reward->receivers;

            //đẩy list receiver cũ qua list tạm
            foreach($this->listOldReceiver as $item) {
                $infoOldReceiver = [
                    'id' => $item->id,
                    'user_info_id' => $item->user_info_id,
                    'fullname' => $item->info->fullname ?? null,
                    'code' => $item->info->code ?? null,
                    'department' => $item->info->department->name ?? null,
                    'position' => $item->info->position->v_value ?? null,
                    'prize_value' => $item->prize_value ?? null,
                    'reason' => $item->reason,
                ];
                array_push($this->listOldReceiverTemp, $infoOldReceiver);
            }
            //file info

             //get document
            $model_name = DecisionReward::class;
            $type = Config::get('common.type_upload.DecisionReward');
            $model_id = $this->decision_reward->id;
            $this->listOldFile=\App\Models\File::where('model_name',$model_name)->where('type', $type)->where('model_id',$model_id)->get();

             //đẩy list file cũ qua list tạm
            foreach($this->listOldFile as $item) {
                $infoOldFile = [
                    'id' => $item->id,
                    'file_name' => $item->file_name,
                    'size_file' => $item->size_file,
                    'url' => $item->url,
                ];
                array_push($this->listOldFileTemp, $infoOldFile);
            }
        }
    }

    public function render()
    {
        return view('livewire.admin.decision-reward.create-and-edit');
    }

    public function updatedFileUpload() {
        $this->current_total_file = count($this->listFileUpload) + count($this->listOldFileTemp);
        $rules = [
            'fileUpload' => [
                'file',
                'mimes:' . implode(',', $this->accept_file),
                'max:' . $this->limit_file_size * 1024
            ],
            'current_total_file' => 'numeric | max:'. ($this->limit_total_file - 1)
        ];
        $error_message = [
            'fileUpload.mimes' => __('notification.upload.mime_type'),
            'fileUpload.max' => __('notification.upload.maximum_size', ['value' => $this->limit_file_size]),
            'current_total_file.max' => __('notification.upload.maximum_uploads')
        ];
        $this->validate($rules,$error_message);
        $size = new SizeFile();
        $size=$size->sizeFile($this->fileUpload->getSize());
        $filenameOrigin = $this->fileUpload->getClientOriginalName();
        $fileUploadInfo = [
            'file_name' => $filenameOrigin,
            'size_file' => $size,
            'file' => $this->fileUpload,
        ];
        array_push($this->listFileUpload, $fileUploadInfo);
        $this->fileUpload = null;
    }


    public function setRewardDate($data) {
        $this->reward_date = $data['reward_date'] ? date('Y-m-d', strtotime($data['reward_date'])) : null;
    }
    public function setDecisionDate($data) {
        $this->decision_date = $data['decision_date'] ? date('Y-m-d', strtotime($data['decision_date'])) : null;
    }

    public function deleteNewReceiver($index) {
        unset($this->listNewReceiver[$index]);
    }
    public function deleteReceiverOldTemp($index) {
        if(isset($this->listOldReceiverTemp[$index])) {
            array_push($this->listIdOfOldReceiverNeedDelete, $this->listOldReceiverTemp[$index]['id']);
            unset($this->listOldReceiverTemp[$index]);
        }
    }

    public function deleteNewFileUpload($index) {
        unset($this->listFileUpload[$index]);
    }

    public function deleteFileOldTemp($index) {
        if(isset($this->listOldFileTemp[$index])) {
            array_push($this->listIdOfOldFileNeedDelete, $this->listOldFileTemp[$index]['id']);
            unset($this->listOldFileTemp[$index]);
        }
    }

    public function download($url, $name) {
        return response()->download('storage/'.$url, $name);
    }


    public function addReceiver() {
        if(!is_null($this->receiver_user)) {
            $checkExistInListNewReceiver = array_search($this->receiver_user->id, array_column($this->listNewReceiver, 'id'));
            $checkExistInListOldReceiver = array_search($this->receiver_user->id, array_column($this->listOldReceiverTemp, 'user_info_id'));
            if($checkExistInListNewReceiver === false && $checkExistInListOldReceiver === false) {
                $receiver = [
                    'id' => $this->receiver_user->id,
                    'fullname' => $this->receiver_user->fullname,
                    'code' => $this->receiver_code,
                    'department' => $this->receiver_department,
                    'position' => $this->receiver_position,
                    'prize_value' => $this->receiver_prize_value,
                    'reason' => $this->receiver_reason,
                ];
                array_push($this->listNewReceiver, $receiver);
                $this->resetFormAddReceiver();
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" =>  __('decision-reward/decision-reward.notification.add-new-receiver-success')]);
            } else {
                $this->resetFormAddReceiver();
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" =>  __('decision-reward/decision-reward.notification.add-new-receiver-fail')]);
            }
        }
    }

    public function resetFormAddReceiver() {
        $this->reset(['receiver_user', 'receiver_name', 'receiver_code','receiver_department','receiver_position','receiver_prize_value','receiver_reason']);
        $this->emit('reset_query_search_combobox2');
    }

    public function getUserDecisionInfo($data) {
        $this->user_decide_id = $data;
        $this->user_decision_position =  UserInf::with('position')->findOrFail($data)->position->v_value ?? '';
    }

    public function getUserReceiverInfo($data) {
        $this->receiver_user = UserInf::with('position')->with('department')->findOrFail($data);
        $this->receiver_code = $this->receiver_user->code ?? null;
        $this->receiver_position = $this->receiver_user->position->v_value ?? null;
        $this->receiver_department = $this->receiver_user->department->name ?? null;
    }



    public function store() {
        if($this->isEdit) {
            parent::edit($this->decision_reward->id);
        } else {
            parent::store();
        }
        //format dữ liệu
        $this->type_of_decision = is_null($this->type_of_decision) ? EDecisionRewardDecisionType::REWARD : intval($this->type_of_decision);
        $this->type_of_prize = is_null($this->type_of_prize) ? EDecisionRewardPrizeType::MONEY : intval($this->type_of_prize);

        //validate
        $this->validate([
            'name' => 'required|max:255',
            'number_decision' => 'required|max:50',
            'reason' => 'max:255',
            'base' => 'max:255',
            'type_reward' => 'max:255',
            'total_value' => 'max:13'
        ],
        ['total_value.max' => __('decision-reward/decision-reward.validate.total_value_max_length')],
        [
            'name' => __('decision-reward/decision-reward.label.name'),
            'number_decision' => __('decision-reward/decision-reward.label.number_decision'),
            'reason' => __('decision-reward/decision-reward.label.reason'),
            'base' => __('decision-reward/decision-reward.label.base'),
            'type_reward' => __('decision-reward/decision-reward.label.type_reward'),
            'total_value' => __('decision-reward/decision-reward.label.total_value'),
        ]);

        //save info
        if(is_null($this->decision_reward)) {
            $this->decision_reward = new DecisionReward();
        }
        $this->decision_reward->name = $this->name;
        $this->decision_reward->reward_date = $this->reward_date;
        $this->decision_reward->decision_date = $this->decision_date;
        $this->decision_reward->number_decision = $this->number_decision;
        $this->decision_reward->user_decide_id = $this->user_decide_id;
        $this->decision_reward->type_of_decision = $this->type_of_decision;
        $this->decision_reward->type_of_prize = $this->type_of_prize;
        $this->decision_reward->type_reward = $this->type_reward;
        $this->decision_reward->total_value = removeFormatNumber($this->total_value);
        $this->decision_reward->reason = $this->reason;
        $this->decision_reward->base = $this->base;
        $this->decision_reward->save();

        // //save receiver
        foreach($this->listNewReceiver as $receiver) {
            $reward_receiver = new DecisionRewardReceiver();
            $reward_receiver->prize_value = removeFormatNumber($receiver['prize_value']);
            $reward_receiver->reason = $receiver['reason'];
            $reward_receiver->user_info_id = $receiver['id'];
            $reward_receiver->decision_reward_id = $this->decision_reward->id;
            $reward_receiver->save();
        }

        // //save file
        $fileUploadComponent=new FileUpload();
        $model_name = DecisionReward::class;
        $type = Config::get('common.type_upload.DecisionReward');
        $model_id = $this->decision_reward->id;
        $folder = app($model_name)->getTable();
        foreach($this->listFileUpload as $file) {
            $dataUpload= $fileUploadComponent->uploadFile($file['file'],$folder);
            $file_upload = new \App\Models\File();
            $file_upload->url = $dataUpload['url'];
            $file_upload->size_file = $dataUpload['size_file'];
            $file_upload->file_name = $dataUpload['file_name'];
            $file_upload->model_name = $model_name;
            $file_upload->model_id = $model_id;
            $file_upload->type = $type;
            $file_upload->save();
        }

        //xoá các file đính kèm cần xoá
        foreach($this->listOldFile as $item) {
            if(in_array($item->id, $this->listIdOfOldFileNeedDelete)) {
                $item->delete();
            }
        }

        //xoá các receiver cũ cần xoá
        foreach($this->listOldReceiver as $item) {
            if(in_array($item->id, $this->listIdOfOldReceiverNeedDelete)) {
                $item->delete();
            }
        }

        if($this->isEdit) {
            session()->flash('success', __('decision-reward/decision-reward.notification.edit-success'));
        } else {
            session()->flash('success', __('decision-reward/decision-reward.notification.create-success'));
        }

        return redirect()->route('admin.decision-reward.index');

    }
}
