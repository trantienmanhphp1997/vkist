<?php
return [
    'breadcrumbs' => [
        'asset-provider-management' => 'Quản lý nhà cung cấp',
        'asset-provider-list' => 'Danh sách nhà cung cấp',
    ],
    'form' => [
        'general_info' => 'Thông tin chung',
        'document' => 'Tài liệu đính kèm',
        'attach_file' => 'Đính kèm file',
    ],
    'table_column' => [
        'name' => 'TÊN NHÀ CUNG CẤP',
        'code' => 'MÃ NHÀ CUNG CẤP',
        'address' => 'ĐỊA CHỈ',
        'phone_number' => 'SỐ ĐIỆN THOẠI',
        'email' => 'EMAIL',
        'action' => 'HÀNH ĐỘNG',
    ],
    'label' => [
        'name' => 'Tên nhà cung cấp',
        'code' => 'Mã nhà cung cấp',
        'address' => 'Địa chỉ',
        'tax_code' => 'Mã số thuế',
        'phone_number' => 'Số điện thoại',
        'website' => 'Website',
        'bank_account_number' => 'TK ngân hàng',
        'bank_name_id' => 'Tên ngân hàng',
        'email' => 'Email',
        'note' => 'Ghi chú',
    ],
    'placeholder' => [
        'search' => 'Tìm kiếm',
    ],

    'title' => [
        'create' => 'Thêm mới nhà cung cấp',
        'edit' => 'Chỉnh sửa nhà cung cấp',
        'no-result' => 'Không có kết quả!',
        'part-1' => 'Hiển thị',
        'part-2' => 'loại đề tài trong',
    ],
    'empty-record' => 'Không có bản ghi nào',
    
    'modal' => [
        'title' => [
            'delete' => 'Xác nhận xoá',
            'create' => 'Thêm loại đề tài',
            'edit' => 'Sửa loại đề tài',
        ],
        'body' => [
            'delete-warning' => 'Bạn có xóa không? Thao tác này không thể phục hồi!',
            'name' => 'Tên loại đề tài',
            'description' => 'Mô tả',
            'note' => 'Ghi chú',
        ],
        'footer' => [
            'back' => 'Quay lại',
            'delete' => 'Xoá',
            'close' => 'Đóng',
            'save' => 'Lưu',
        ]
    ],
    'notification' => [
        'edit-success' => 'Chỉnh sửa thành công',
        'create-success' => 'Thêm mới thành công'
    ]
];
