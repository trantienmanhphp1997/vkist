<?php


namespace App\Enums;


class EApproval {

    const STATUS_NEW = 1;
    const STATUS_COMPLETE_PROFILE = 2;
    const STATUS_INVALID_PROFILE = 3;
    const STATUS_WAIT_ASSESSMENT = 4;
    const STATUS_WAIT_APPROVAL = 5;
    const STATUS_REJECT = 6;
    const STATUS_APPROVAL = 7;

    const TYPE_IDEAL = 1;
    const TYPE_TOPIC = 2;
    const TYPE_COST = 3;
    const TYPE_PLAN = 4;

    public static function valueToName($value = 0) {
        $arr = self::getListStatus();
        return $arr[$value] ?? $value;
    }

    public static function getListStatus() {
        return [
            self::STATUS_NEW => __('data_field_name.approval_topic.status_new'),
            self::STATUS_COMPLETE_PROFILE => __('data_field_name.approval_topic.status_complete_profile'),
            self::STATUS_INVALID_PROFILE => __('data_field_name.approval_topic.status_invalid_profile'),
            self::STATUS_WAIT_ASSESSMENT => __('data_field_name.approval_topic.status_wait_assessment'),
            self::STATUS_WAIT_APPROVAL => __('data_field_name.approval_topic.status_wait_approval'),
            self::STATUS_REJECT => __('data_field_name.approval_topic.status_reject'),
            self::STATUS_APPROVAL => __('data_field_name.approval_topic.status_approval'),
        ];
    }

    public static function getListType()
    {
        return [
            self::TYPE_IDEAL => __('data_field_name.approval_topic.type_ideal'),
            self::TYPE_TOPIC => __('data_field_name.approval_topic.type_topic'),
            self::TYPE_COST => __('data_field_name.approval_topic.type_cost_estimate'),
            self::TYPE_PLAN => __('data_field_name.approval_topic.type_research_plan'),
        ];
    }
}

