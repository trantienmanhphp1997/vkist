<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetInventoryHasUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_inventory_has_user_info', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('thành viên đi kiểm kê');
            $table->foreignId('asset_inventory_id')->nullable()->constrained('asset_inventory')->comment('map với bảng inventory');
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('map vs bảng user_info');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_inventory_has_user_info');
    }
}
