<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Livewire\Admin\Research\Category\CategoryTopicList;
use App\Models\ResearchCategory;
use App\Models\User;
use Livewire;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Enums\EResearchCategoryType;


class CategoryTopicLivewireTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use RefreshDatabase;

    /** @test */
    public function can_index_category_topic()
    {
        $user =$this->user;
        $this->actingAs($user)->get(route('admin.research.category-topic.index'))->assertOk();
    }

    /** @test  */
    function can_create_category()
    {
        $user =$this->user;

        $this->actingAs($user);

        Livewire::test(CategoryTopicList::class)
            ->set([
                'name' => 'đề tài nghiên cứu thực tiễn unit test',
                'code' => 'LDT01',
                'description' => 'đề tài nghiên cứu các sản phẩm ứng dụng vào cuộc sống',
                'note' => 'note',
                'type' => EResearchCategoryType::TOPIC,
                ])
            ->call('store');

        $this->assertTrue(ResearchCategory::whereName('đề tài nghiên cứu thực tiễn unit test')->exists());
    }

    /** @test  */
    function name_is_required()
    {
        $user =$this->user;

        $this->actingAs($user);

        Livewire::test(CategoryTopicList::class)
            ->set([
                'name' => '',
                ])
            ->call('store')
            ->assertHasErrors(['name' => 'required']);
    }

    /** @test  */
    function max_length_of_name_is_100()
    {
        $user =$this->user;
        $this->actingAs($user);

        Livewire::test(CategoryTopicList::class)
            ->set([
                'name' => bin2hex(random_bytes(51)), // this function create a random string with length = 51 x 2 = 102
                ])
            ->call('store')
            ->assertHasErrors(['name' => 'max']);
    }

    /** @test  */
    function max_length_of_description_is_255()
    {
        $user =$this->user;
        $this->actingAs($user);

        Livewire::test(CategoryTopicList::class)
            ->set([
                'description' => bin2hex(random_bytes(128)), // this function create a random string with length = 128 x 2 = 256
                ])
            ->call('store')
            ->assertHasErrors(['description' => 'max']);
    }

    /** @test  */
    function max_length_of_note_is_255()
    {
        $user =$this->user;
        $this->actingAs($user);

        Livewire::test(CategoryTopicList::class)
            ->set([
                'note' => bin2hex(random_bytes(128)), // this function create a random string with length = 128 x 2 = 256
                ])
            ->call('store')
            ->assertHasErrors(['note' => 'max']);
    }
}
