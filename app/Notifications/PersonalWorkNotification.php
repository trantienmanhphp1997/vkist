<?php

namespace App\Notifications;

use App\Enums\ENotificationType;
use Illuminate\Database\Eloquent\Model;

class PersonalWorkNotification extends BaseNotification
{
    public Model $model;

    public function __construct(Model $model)
    {
        $this->type = ENotificationType::PERSONAL_WORK;
        $this->model = $model;
    }

    protected function message($notifiable)
    {
        return [
            'template_name' => 'notification.user.personal_work',
            'model' => [
                'id' => $this->model->id,
                'type' => get_class($this->model),
                'name' => $this->model->name,
                'code' => $this->model->code
            ]
        ];
    }
}
