<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{asset('bootstrap/js/bootstrap.js')}}"></script>
<script src="{{ asset('assets/js/kendo.all.min.js') }}"></script>


<script src="{{ asset('js/vkist.js') }}"></script>
<script src="{{asset('assets/js/select2.min.js')}}"></script>




@include('layouts.partials._message')
<script>
    $(document).ready(function () {
        if($(window).height() > $("body").height()){
            $(".body-container").css("min-height", $(window).height() +"px");
        }

        $(".memu-main-left li").click(function (event) {
            event.stopPropagation()
            $(this).find("ul").first().toggleClass("active");
            $(this).toggleClass("active");
        });

        $(".parent-menu").each(function(){
        if($(this).find(".child-menu").length == 0)
            {
                $(this).hide();
            }
        });
    });
</script>
