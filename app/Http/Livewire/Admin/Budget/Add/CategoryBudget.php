<?php

namespace App\Http\Livewire\Admin\Budget\Add;

use App\Http\Livewire\Base\BaseLive;
use App\Enums\EBudgetStatus;
use App\Models\Budget;
use App\Models\TopicFee;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class CategoryBudget extends BaseLive
{
    public $searchBudget;
    public $chooseCategory;
    public $topic_fee_id;
    public $topic_fee;
    protected $listeners =[
        'resetInput',
    ];

    public function mount(){
        $this->chooseCategory = 1;
        $this->searchBudget= Budget::query()->where('status',\App\Enums\EBudgetStatus::DONE)->get()->first()->id??null;
        $this->topic_fee_id = TopicFee::query()->whereNotIn('code',Budget::query()->pluck('estimated_code')->toArray())->get()->first()->id??null;
    }

    public function render()
    {
        $query = Budget::query()->where('status',\App\Enums\EBudgetStatus::DONE);
        $budget_code =$query->pluck('code','id');
        $topic_fee_list = TopicFee::query()->whereNotIn('code',Budget::query()->pluck('estimated_code')->toArray())->pluck('code', 'id');

        return view('livewire.admin.budget.add.category-budget',['budget_code'=>$budget_code, 'topic_fee_list' =>$topic_fee_list]);
    }

    public function save(){


        if ($this->chooseCategory == 2){
            return $this->redirectRoute('admin.budget.copy',['id'=>$this->searchBudget]);
        }
        elseif ($this->chooseCategory == 3){
            return $this->redirectRoute('admin.budget.topic-budget',['id'=>$this->topic_fee_id]);
        }
        else {
            $this->validate([
            'topic_fee' => ['required', 'regex:/^[A-Z0-9' . config('common.special_character.alphabet') . ']+$/', 'max:10'],
            ], [], [
                'topic_fee' => __('data_field_name.budget.estimated_code'),
            ]);
            return $this->redirectRoute('admin.budget.create',['topic_fee'=>$this->topic_fee]);
        }
    }
    public function resetInput()
    {
        $this->chooseCategory = 1;
        $this->resetValidation();
    }
}
