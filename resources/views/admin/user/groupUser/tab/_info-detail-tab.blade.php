<div class="row inner-tab">
    <div class="col-md-3">
        <div class="form-group">
            <label>{{__('user/group_user.form-data.code')}}</label>
            <input type="text" class="form-control" wire:model.defer="code" readonly
                   placeholder="{{__('user/group_user.form-data.code')}}">
            @error('code') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group">
            <label>{{__('user/group_user.form-data.name')}}</label>
            <input type="text" class="form-control" wire:model.defer="name" readonly
                   placeholder="{{__('user/group_user.form-data.name')}}">
            @error('name') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>{{__('user/group_user.form-data.description')}}</label>
            <textarea class="form-control" rows="6" wire:model.lazy="description" readonly
                      placeholder="{{__('user/group_user.form-data.description')}}"></textarea>
            @error('description') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>

</div>


