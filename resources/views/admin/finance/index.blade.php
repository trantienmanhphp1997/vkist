@extends('layouts.master')
@section('title')
    <title>{{ __('menu_management.menu_name.finance_management') }}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    @livewire('admin.finance.finance-index')
@endsection
