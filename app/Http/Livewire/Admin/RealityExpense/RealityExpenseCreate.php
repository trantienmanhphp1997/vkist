<?php

namespace App\Http\Livewire\Admin\RealityExpense;

use Livewire\Component;
use App\Models\Topic;
use App\Models\RealityExpense;
use App\Models\User;
use App\Models\Task;
use App\Enums\ECapital;
use App\Enums\EPayForm;
use App\Enums\ETypePay;
use App\Enums\EDisbursedStatus;
use App\Enums\ETopicFeeDetail;
use App\Models\TopicFeeDetail;
use App\Models\TaskWork;
use App\Http\Livewire\Base\BaseLive;
use Carbon\Carbon;
use App\Enums\ETopicStatus;
use Illuminate\Database\Eloquent\Builder;
use SebastianBergmann\CodeUnit\FunctionUnit;
use Auth;
use Exception;

class RealityExpenseCreate extends BaseLive
{
    public $topic_name;
    public $topic_id;
    public $topic_code;
    public $topicFeeList = [];
    public $type_expense;
    public $type_cost;
    public $payment;
    public $source_capital;
    public $approval_costs;
    public $total_cost;
    public $money_remain;
    public $user_request_id;
    public $note = [];
    public $expense_task = [];
    public $task_id_save;
    public $note_id_save;
    public $money_cost;
    public $disbursement_date;
    public $reality_expense;
    public $isEdit = false;
    public $capital;
    public $payform;
    public $typepay;
    public $status;
    public $status_disbursed;
    public $type;
    public ?RealityExpense $realityExpense;
    protected $listeners = [
        'setTargetId' => 'getTopicInfoList',
    ];

    public function mount()
    {
        // Nguồn vốn

        $this->user_request_id = Auth::user()->id;
        $this->capital = [
            ECapital::ODA => __('data_field_name.reality_expense.ODA'),
            ECapital::STATE_CAPITAL => __('data_field_name.reality_expense.state_capital'),
        ];
        // Hình thức chi
        $this->payform = [
            EPayForm::EXPENSES => __('data_field_name.reality_expense.expenses'),
            EPayForm::NO_EXPENSES => __('data_field_name.reality_expense.no_expenses'),
        ];
        // Loại chi phí
        $this->typepay = [
            ETypePay::DIRECT_EXPENSES => __('data_field_name.reality_expense.direct_expenses'),
            ETypePay::INDIRECT_EXPENSES => __('data_field_name.reality_expense.indirect_expenses'),
        ];
        // Trạng thái giải ngân
        $this->status_disbursed = [
            EDisbursedStatus::DISBURSED => __('data_field_name.reality_expense.disbursed'),
            EDisbursedStatus::NOT_DISBURSED => __('data_field_name.reality_expense.not_disbursed'),
        ];
        if ($this->reality_expense) {
            $this->topic_id = $this->reality_expense->topic_id;
            $this->type_expense = $this->reality_expense->type_expense;
            $this->type_cost = $this->reality_expense->type_cost;
            $this->payment = $this->reality_expense->payment;
            $this->source_capital = $this->reality_expense->source_capital;
            $this->approval_costs = numberFormat($this->reality_expense->approval_costs);
            $this->total_cost = numberFormat($this->reality_expense->total_cost);
            $this->money_remain = numberFormat($this->reality_expense->money_remain);
            $this->user_request_id = $this->reality_expense->user_request_id;
            $this->task_id_save = $this->reality_expense->expense_task;
            $this->note_id_save = $this->reality_expense->note;
            $this->money_cost = numberFormat($this->reality_expense->money_cost);
            $this->disbursement_date = date('Y-m-d', strtotime($this->reality_expense->disbursement_date));
            $this->status = $this->reality_expense->status;
            $this->getTopicInfoList(['id' => $this->topic_id]);
            $this->isEdit = true;
            $this->updatedTypeExpense();
        }
    }

    public function render()
    {
        return view('livewire.admin.reality-expense.reality-expense-create');
    }

    protected function rules()
    {
        return [
            'topic_id' => 'required',
            'task_id_save' => 'required',
            'type_expense' => 'required',
            'note_id_save' => 'required',
            'money_cost' => 'required|regex:/^[0-9,]+$/|max:14',
            'money_remain' => 'required',
            'status' => 'required',
            'approval_costs' => 'max:14',
            'total_cost' => 'required|regex:/^[0-9,]+$/|max:14',
            'disbursement_date' => 'required|date',
        ];
    }

    protected function getValidationAttributes()
    {
        return [

            'type_expense' => __('data_field_name.reality_expense.expenses_type'),
            'task_id_save' => __('data_field_name.reality_expense.expense_task'),
            'note_id_save' => __('data_field_name.reality_expense.note'),
            'money_cost' => __('data_field_name.reality_expense.money_must_cost'),
            'status' => __('data_field_name.reality_expense.status_dibursed'),
            'disbursement_date' => __('data_field_name.reality_expense.disbursement_disduration'),
            'topic_id' => __('data_field_name.reality_expense.topic_code'),
            'approval_costs' => __('data_field_name.reality_expense.browse'),
            'total_cost' => __('data_field_name.reality_expense.total_money_cost'),

        ];
    }

    public function store()
    {
        $this->validate();
        if (empty($this->reality_expense) || !$this->isEdit) { //trường hợp rỗng thì tạo mới
            $this->reality_expense = new RealityExpense;
        }

        $this->reality_expense->topic_id = $this->topic_id;
        $this->reality_expense->type_expense = $this->type_expense;
        $this->reality_expense->type_cost = $this->type_cost;
        $this->reality_expense->payment = $this->payment;
        $this->reality_expense->approval_costs = removeFormatNumber($this->approval_costs);
        $this->reality_expense->source_capital = intval($this->source_capital);
        $this->reality_expense->total_cost = removeFormatNumber($this->total_cost);
        $this->reality_expense->money_remain = removeFormatNumber($this->money_remain);
        $this->reality_expense->user_request_id = $this->user_request_id;
        $this->reality_expense->note = $this->note_id_save ? $this->note_id_save : null;
        $this->reality_expense->expense_task = $this->task_id_save ? $this->task_id_save : null;
        $this->reality_expense->money_cost = removeFormatNumber($this->money_cost);
        $this->reality_expense->disbursement_date =  date('Y-m-d', strtotime($this->disbursement_date));
        $this->reality_expense->status = $this->status;

        $this->reality_expense->save();
        redirect()->route('admin.reality_expense.index');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('Thêm mới thành công')]);
    }

    public function getTopicInfoList($id)
    {
        $topic = Topic::with(['topicFeeDetails', 'task_work', 'approval'])->where('id', $id)->first();
        $approval = $topic->approval;
        $this->topic_code = 0;
        foreach ($approval as $value) {
            $this->topic_code = $value->code;
        }
        $this->topic_name = $topic->name;
        $this->topic_id = $id;
        $this->topicFeeList = $topic->topicFeeDetails;
        $this->expense_task = $topic->task_work;
    }

    public function updatedTypeExpense() //Update thông tin khi chọn khoản chi
    {
        $topicFeeDetail = TopicFeeDetail::with([
            'topic_detail_work.task', 'expert_cost', 'material_cost', 'equipment_cost',
            'asset_cost', 'topic_other_fee', 'topic_normal_cost'
        ])->findOrFail($this->type_expense);
        $this->approval_costs = numberFormat($topicFeeDetail->approval_cost);
        switch ($topicFeeDetail->type) {
            case ETopicFeeDetail::TYPE_EXPERT:
                $this->note = $topicFeeDetail->expert_cost;
                break;
            case ETopicFeeDetail::TYPE_MATERIAL:
                $this->note = $topicFeeDetail->material_cost;
                break;
            case ETopicFeeDetail::TYPE_EQUIPMENT:
                $this->note = $topicFeeDetail->equipment_cost;
                break;
            case ETopicFeeDetail::TYPE_SHOP_REPAIR:
                $this->note = $topicFeeDetail->asset_cost;
                break;
            case ETopicFeeDetail::TYPE_PEOPLE:
                $this->note = $topicFeeDetail->topic_detail_work;
                break;
            case ETopicFeeDetail::TYPE_OTHER:
                $this->note = $topicFeeDetail->topic_other_fee;
                break;
            case ETopicFeeDetail::TYPE_NORMAL:
                $this->note = $topicFeeDetail->topic_normal_cost;
                break;
            default:
                break;
        }
        $this->type = $topicFeeDetail->type;
    }
    public function updatedTaskIdSave()
    {
        $task_work_detail = TaskWork::findOrFail($this->task_id_save);
        $this->disbursement_date =  date('d-m-Y', strtotime($task_work_detail->start_date));
    }
    public function updatedTotalCost()
    {
        $this->money_remain = numberFormat((removeFormatNumber($this->approval_costs) - removeFormatNumber($this->total_cost)));
        if ($this->money_remain < 0) {
            $this->addError('total_cost', __('data_field_name.reality_expense.error_total_cost'));
        } else {
            $this->resetErrorBag('total_cost');
        }
    }
}
