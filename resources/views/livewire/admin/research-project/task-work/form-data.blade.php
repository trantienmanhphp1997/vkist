<div class="col-md-10 col-xl-11 box-detail box-user">
    <style>
        .auto-complete-follower-user{
            position: absolute;
            z-index: 1;
            background: white;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a
                    href="{{ route('admin.research-project.information.index') }}">{{__('research/project.name')}}</a> \
                <span>{{__('research/project.information.breadcrumbs')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="detail-task information box-idea">
                <h4>
                    @if($taskID)
                        {{__('research/taskwork.update')}}
                    @else
                        {{__('research/taskwork.create')}}
                    @endif

                </h4>
                <div wire:loading class="loader" style="z-index: 999;"></div>
                <div class="group-tabs">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="row inner-tab">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{__('research/taskwork.form-data.topic')}} <span
                                            class="text-danger">*</span></label>
                                    <select class="form-control form-control-lg" wire:model.lazy="topic_id"
                                            @if($taskID) disabled="disabled"
                                            @endif style="@if($taskID) background-color: #e9ecef; @endif">
                                        <option value="">-- --</option>
                                        @foreach($topicList as $topic)
                                            <option value="{{$topic->id}}"
                                                    style="height: 40px;">{{$topic->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('topic_id')
                                    <div class="text-danger">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>  {{__('research/taskwork.form-data.main-mission')}} <span
                                            class="text-danger">*</span></label>
                                    <select class="form-control form-control-lg" wire:model.lazy="main_task_id"
                                            @if($taskID) disabled="disabled"
                                            @endif style="@if($taskID) background-color: #e9ecef; @endif">
                                        <option value="">{{__('research/taskwork.form-data.select-main-task')}}</option>
                                        @foreach($mainTaskList as $task)
                                            <option value="{{$task->id}}" style="height: 40px;">{{$task->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('main_task_id')
                                    <div class="text-danger">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>  {{__('research/taskwork.form-data.mission')}} <span
                                            class="text-danger">*</span></label>
                                    <select class="form-control form-control-lg" wire:model.lazy="task_id"
                                            @if($taskID) disabled="disabled"
                                            @endif style="@if($taskID) background-color: #e9ecef; @endif">
                                        <option value="">{{__('research/taskwork.form-data.select-task')}}</option>
                                        @foreach($taskList as $task)
                                            <option value="{{$task->id}}" style="height: 40px;">{{$task->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('task_id')
                                    <div class="text-danger">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>  {{__('research/taskwork.form-data.level')}} <span
                                            class="text-danger">*</span></label>
                                    {!! Form::select('level_id', array_merge(['' => __('common.select-box.choose')], $levelDataList), $selected_id ?? '' ,[
     'class'=>'form-control select2' , 'wire:model.defer'=>'level_id','style'=>$taskID?'background-color: #e9ecef':'' ,$taskID?'disabled="disabled"':'',
]) !!}
                                    @error('level_id')
                                    <div class="text-danger">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>  {{__('research/taskwork.form-data.status')}} <span
                                            class="text-danger">*</span></label>
                                    <select class="form-control form-control-lg" wire:model.lazy="status_id"
                                    >
                                        <option value="">-- --</option>
                                        <option value="0">{{__('research/taskwork.select-status.undo')}}</option>
                                        <option value="1">{{__('research/taskwork.select-status.on-working')}}</option>
                                        <option value="3">{{__('research/taskwork.select-status.done')}}</option>
                                    </select>
                                    @error('status_id')
                                    <div class="text-danger">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>  {{__('research/taskwork.form-data.actual-time-topic')}} <span
                                            class="text-danger">*</span></label>
                                    <div class="form-control" style="@if($taskID) background-color: #e9ecef; @endif">
                                        @if($topicStartDate)
                                            {{reFormatDate($topicStartDate)}} to {{reFormatDate($topicEndDate)}}
                                            <input type="hidden" wire:model.lazy="topicStartDate">
                                            <input type="hidden" wire:model.lazy="topicEndDate">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>   {{__('research/taskwork.form-data.work-type')}}</label>
                                    {!! Form::select('status_id', array_merge(['' => __('common.select-box.choose')], $workTypeDataList), $selected_id ?? '' ,[
 'class'=>'form-control select2' , 'wire:model.defer'=>'work_type_id','style'=>$taskID?'background-color: #e9ecef':'' ,$taskID?'disabled="disabled"':'',
]) !!}
                                    @error('work_type_id')
                                    <div class="text-danger">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>  {{__('research/taskwork.form-data.name')}} <span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control" wire:model.defer="name"
                                           placeholder="  {{__('research/taskwork.form-data.name')}}">
                                    @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>  {{__('research/taskwork.form-data.start-date')}} <span
                                            class="text-danger">*</span></label>
                                    <span wire:ignore>
                                             <input @if($taskID) disabled="" @endif type="text" class="form-control input-date-kendo" placeholder="dd/mm/yyyy" name="start_date" id="start_date"  value="{{ $start_date ?? '' }}">
                                    </span>

                                    @error('start_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>  {{__('research/taskwork.form-data.end-date')}} <span
                                            class="text-danger">*</span></label>
                                    <span wire:ignore>
                                        <input type="text" @if($taskID) disabled="" @endif class="form-control input-date-kendo" placeholder="dd/mm/yyyy" name="end_date" id="end_date" value="{{ $end_date ?? '' }}">
                                    </span>

                                    @error('end_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            @if($taskID)
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>  {{__('research/taskwork.form-data.real-start-date')}} </label>
                                        <span wire:ignore>
                                             <input @if($taskID) disabled="" @endif type="text" class="form-control input-date-kendo" placeholder="dd/mm/yyyy" name="start_date" id="start_date"  value="{{ $real_start_date ?? '' }}">
                                    </span>

                                        @error('real_start_date') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>  {{__('research/taskwork.form-data.real-end-date')}} </label>
                                        <span wire:ignore>
                                        <input type="text" @if($taskID) disabled="" @endif class="form-control input-date-kendo" placeholder="dd/mm/yyyy" name="end_date" id="end_date" value="{{ $real_end_date ?? '' }}">
                                    </span>

                                        @error('real_end_date') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-12">
                                <div class="form-group" wire:ignore>
                                    <trix-editor  class="form-control"
                                                  id ="trix-description"
                                                  wire:model.defer="description"
                                                  x-ref="trix"
                                                  style="height: 200px !important;overflow: auto;"
                                                  placeholder="{{__('news/newsManager.menu_name.form-data.description')}}"
                                    >
                                    </trix-editor>
                                </div>
                                @error('description')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>

                            <div class="col-md-12">
                                @livewire('component.files', ['model_name'=>$model_name,
                                'folder'=>$folder, 'model_id'=>$model_id,'canUpload'=>true])
                            </div>
                            <div class = "col-md-12">
                                @livewire('admin.research-project.task-work.task-work-has-result')
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> {{__('research/taskwork.form-data.member')}}</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="checkbox-task-work-follower checkbox-config hover-menu-box"><input
                                            wire:model.lazy="isFollower"
                                            type="checkbox" id="checkbox0" value="1"> <label
                                            for="checkbox0"><span>  {{__('research/taskwork.form-data.follower')}} </span></label>
                                    </div>
                                </div>
                                <input type="text" id="user_info_name"
                                       class="form-control @if($isFollower) d-block @else d-none @endif"
                                       wire:model="user_info_name" style="margin-bottom: 16px"
                                       placeholder="{{__('research/taskwork.name-follower')}}">
                                @include('admin.researchProject.task-work.modal.modalSelectUserInfo')
                                <div class="@if($isFollower) d-block @else d-none @endif">
                                    <div class="container-fluid auto-complete-follower-user">
                                        <div class="row @if($autoCompleteSearch) d-block @else d-none @endif">
                                            @foreach($userInfos as $userInfo)
                                                <a class="user-select-hover"
                                                   wire:click="setUserInfoId({{$userInfo->userInfo->id ?? ''}})">
                                                    <div class="col-md-12"
                                                         style="padding: 20px;margin-bottom: 10px"> {{$userInfo->userInfo->fullname ?? ''}}</div>
                                                </a>
                                            @endforeach
                                            <a class="user-select-hover" wire:click="closeAutoComplete">
                                                <div class="col-md-12"
                                                     style="padding: 20px;margin-bottom: 10px"> {{ __('common.button.cancel') }} </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @foreach($topicMember as $key => $member)
                                    <div class="checkbox-task-work-follower checkbox-config hover-menu-box"
                                         style="color: orange;cursor:pointer; border: 1px solid;margin-bottom: 10px">
                                        {{$member->userInfo->fullname}} <a
                                            class="btn par6 float-right"
                                            wire:click="removeMember({{$member->userInfo->id}})">X</a>
                                    </div>

                                @endforeach
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="checkbox-task-work-assign checkbox-config hover-menu-box"><input
                                            type="checkbox" id="checkbox1" wire:model.lazy="isAssign"> <label
                                            for="checkbox1"><span>  {{__('research/taskwork.form-data.assign')}} </span></label>
                                    </div>
                                </div>
                                @if($isAssign)
                                    @livewire('component.advanced-select', [
                                    'name' => 'assigner-id',
                                    'placeholder' => __('research/taskwork.form-data.assign'),
                                    'modelName' => App\Models\User::class,
                                    'searchBy' => ['name'],
                                    'columns' => ['name'],
                                    'selectedIds' => [$admin_id]
                                    ])
                            @endif
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="checkbox-task-work-preview checkbox-config hover-menu-box"><input
                                        type="checkbox" id="checkbox2" wire:model.lazy="isPreview"> <label
                                        for="checkbox2"><span>  {{__('research/taskwork.form-data.reviewer')}} </span></label>
                                </div>
                            </div>
                            @if($isPreview)
                                <div
                                    class="checkbox-task-work-preview checkbox-config select-status-project" style="color: blue;cursor:pointer; border: 1px solid;margin-bottom: 10px">{{$previewerName ? $previewerName : __('research/taskwork.unknown')}} </div>
                            @endif
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="checkbox-task-work-approve checkbox-config hover-menu-box"><input
                                        type="checkbox" id="checkbox3" wire:model.lazy="isApproval" value="1"> <label
                                        for="checkbox3"><span>  {{__('research/taskwork.form-data.approve')}} </span></label>
                                </div>
                            </div>
                            @if($isApproval)
                                <div
                                    class="checkbox-task-work-approve checkbox-config select-status-project" style="color: red;cursor:pointer; border: 1px solid;margin-bottom: 10px">{{$leaderProjectName ? $leaderProjectName : __('research/taskwork.unknown')}} </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="group-btn2 text-center group-btn">
                        <button onclick="changeData()" type="button" wire:click="back"
                                class="btn btn-cancel">{{ __('common.button.cancel') }}</button>
                        @if($taskID)
                            <button type="button" onclick="changeData()" wire:click="update" wire:loading.attr="disabled"
                                    class="btn btn-save @if($isOverFile) disabled @endif">{{ __('common.button.save') }}</button>
                        @else
                            <button onclick="changeData()" type="button" wire:click="storeTask" wire:loading.attr="disabled"
                                    class="btn btn-save @if($isOverFile) disabled @endif">{{ __('common.button.save') }}</button>
                        @endif
                    </div>
                </div>
            </div>

            </div>
        </div>

        <script>
            function uploadTrixImage(attachment) {
            @this.upload(
                'newFiles',
                attachment.file,
                function (uploadedUrl) {
                    const eventName = 'myapp:trix-upload-complete:${btoa(uploadedUrl)}';
                    const listener = function (event) {
                        attachment.setAttributes(event.detail);
                        console.log(event.detail);
                        window.removeEventListener(eventName,listener)
                    }
                    window.addEventListener(eventName,listener)

                @this.call('completeUpload',uploadedUrl,eventName);
                },
                function () {

                },
                function (event){
                    attachment.setUploadProgress(event.detail.progress);
                }
            )
            }

            addEventListener("trix-attachment-add", function (event) {
                uploadTrixImage(event.attachment)
            })
            function changeData(){
                var description = document.getElementById("trix-description");
                @this.description = description.value;
            }

            $('input[name="start_date"]').on('change', (e) => {
                Livewire.emit('set-start-time',
                    document.getElementById('start_date').value,
                );
            });
            $('input[name="end_date"]').on('change', (e) => {
                Livewire.emit('set-end-time',
                    document.getElementById('end_date').value,
                );
            });
            $('input[name="topic_id"]').on('change', (e) => {
                Livewire.emit('get-main-task-list',
                    document.getElementById('start_date').value,
                );
            });
            $("#user_info_name").focus(function () {
                window.livewire.emit('open-auto-searchBox')
            });

            $("#user_info_name").focusout(function() {
                window.livewire.emit('close-auto-search')
            })
                .blur(function() {
                    window.livewire.emit('close-auto-search')
                });
        </script>
    </div>

