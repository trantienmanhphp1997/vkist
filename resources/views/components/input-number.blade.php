<div class="form-group">
    <label for="">{{$label}}
        @if(isset($required) && $required == "true")
            <span class="text-danger">(*)</span>
        @endif
    </label>
    <input type="{{$type ?? "text"}}" class="form-control form-control-sm" id="" name="{{$formName}}" autocomplete="off" id="{{$formName}}" {{isset($required) && $required == "true" ? "required" : "" }} value="{{(old($formName)) ? old($formName) : ($type != 'date' ? $value : ($value != '' ? date("Y-m-d", strtotime($value)) : ''))}}"
           placeholder="{{$placeholder? $placeholder : "Enter $label"}}"
        {{isset($disabled) && $disabled == true ? "disabled" : "" }}
    >
    @error($formName)
    <label class="error">
        {{ $message }}
    </label>
    @enderror

    {{ $slot }}
</div>

@php
    $typeArr = ["text","number","email"]
@endphp
@if((isset($required) && $required == "true") || isset($maxlength) || (isset($type) && in_array($type,$typeArr)))
    @push('validation-rules')
        {{$formName}}: {
        @if(isset($required) && $required == "true")
            "required" : true,
            normalizer: function( value ) {
            // Trim the value of the `field` element before
            // validating. this trims only the value passed
            // to the attached validators, not the value of
            // the element itself.
            return $.trim( value );
            },
        @endif
        @if((isset($type) && $type == "text") || isset($maxlength))
            maxlength: {{isset($maxlength) ? $maxlength : 100}},
        @endif
        @if((isset($type) && $type == "number") )
            min: {{$min}},
        @endif
        @if((isset($type) && $type == "email"))
            email: true,
        @endif
        },
    @endpush

    @push('validation-messages')
        {{$formName}}: {
        @if(isset($required) && $required == "true")
            required: "{{__('client_validation.form.required.common.required', ["attribute" => $label])}}",
        @endif
        @if((isset($type) && $type == "text") || isset($maxlength))
            maxlength: "{{__('client_validation.form.size.string', ["attribute" => $label, "size" => isset($maxlength) ? $maxlength : 100])}}",
        @endif
        @if((isset($type) && $type == "number") )
            min: "{{__('client_validation.form.size.numeric_min', ["attribute" => $label, "size" => $min])}}",
            number: "{{__('client_validation.form.number.number')}}",
        @endif
        @if((isset($type) && $type == "email"))
            email: "{{__('client_validation.form.format.email', ["attribute" => $label])}}",
        @endif
        },
    @endpush
@endif
