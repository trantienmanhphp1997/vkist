<div class="group-tabs">
	<nav>
	    <div class="nav nav-tabs" id="nav-tab" role="tablist">
	        <a class="nav-item nav-link @if($tab_basic_info) active @endif" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" wire:click="selectTab(true, false)"><span>{{__('data_field_name.ideal.basic_info')}}
	        </span></a>
	        <a class="nav-item nav-link @if($tab_detail_info) active @endif" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" wire:click="selectTab(false, true)"><span>{{__('data_field_name.ideal.detail_info')}}</span></a>
	    </div>
	</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade @if($tab_basic_info) active show @endif" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
	    <div class="row inner-tab">
	        <div class="col-md-12">
	            <div class="form-group">
	                <label>
	                    {{__('data_field_name.ideal.name')}}
	                    <span class="text-danger">*</span>
	                </label>
	                <input type="text" @if($show_detail) disabled @endif name="name" class="form-control" wire:model.defer="name" placeholder="{{__('data_field_name.ideal.name')}}">
	                @error('name')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="form-group">
	                <label>
	                    {{__('data_field_name.ideal.proposer')}}
	                    <span class="text-danger">*</span>
	                </label>
	                <select name="proposer" class="form-control" wire:model.defer="proposer" @if($show_detail) disabled @endif>
	                	<option selected="">{{__('common.select-box.choose')}}</option>
	                	@foreach($user as $item)
	                		<option value="{{$item->id}}">{{$item->fullname}}</option>
	                	@endforeach
	                </select>
	                @error('proposer')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="form-group">
	                <label>
	                    {{__('data_field_name.ideal.agencies')}}
	                    <span class="text-danger">*</span>
	                </label>
	                <input type="text" @if($show_detail) disabled @endif name="agencies" class="form-control" wire:model.defer="agencies" placeholder="{{__('data_field_name.ideal.agencies')}}">
	                @error('agencies')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
	            </div>
			</div>
			<div class="col-md-12" style="margin-bottom: 24px">
					<label>{{__('data_field_name.ideal.research_field')}}</label>
					@livewire('component.search-research-field', ['researchFieldId' => $research_field, 'disable' => $show_detail])
					@error('research_field')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
			</div>
	        <div class="col-md-12">
	            <div class="form-group">
	                <label>{{__('data_field_name.ideal.necessity')}}</label>
	                <textarea class="form-control" @if($show_detail) disabled @endif rows="6" wire:model.defer="necessity" placeholder="{{__('data_field_name.ideal.necessity')}}"></textarea>
	                @error('necessity')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
	            </div>
			</div>
	        <div class="col-md-12">
	            <div class="form-group">
	                <label>{{__('data_field_name.ideal.target')}}</label>
	                <textarea class="form-control" @if($show_detail) disabled @endif rows="6" wire:model.defer="target" placeholder="{{__('data_field_name.ideal.target')}}"></textarea>
	            </div>
			</div>
			@if($show_detail)
			<div class="col-md-6">
				<label>
					{{__('research/ideal.table_column.created_at')}}
				</label>
				<input type="text" disabled class="form-control" value="{{reFormatDate($ideal->created_at)}}">
			</div>
			<div class="col-md-6">
				<label>
					{{__('research/ideal.table_column.updated_at')}}
				</label>
				<input type="text" disabled class="form-control" value="{{reFormatDate($ideal->updated_at)}}">
			</div>
			@endif
	    </div>
	</div>
	<div class="tab-pane fade @if($tab_detail_info) active show @endif" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
	    <div class="row inner-tab">
	        <div class="col-md-12">
	            <div class="form-group">
	                <label>
	                    {{__('data_field_name.ideal.content')}}
	                    <span class="text-danger">*</span>
	                </label>
	                <textarea class="form-control" @if($show_detail) disabled @endif rows="6" wire:model.defer="content" placeholder="{{__('data_field_name.ideal.content')}}"></textarea>
	                @error('content')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
	            </div>
	        </div>
	        <div class="col-md-12">
	            <div class="form-group">
	                <label>
	                    {{__('data_field_name.ideal.result')}}
	                    <span class="text-danger">*</span>
	                </label>
	                <textarea class="form-control" @if($show_detail) disabled @endif rows="6" wire:model.defer="result" placeholder="{{__('data_field_name.ideal.result')}}"></textarea>
	                @error('result')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="form-group">
	                <label>
	                	{{__('data_field_name.ideal.execution_time')}}
	                	<span class="text-danger">*</span>
	                </label>
	                <input type="text" @if($show_detail) disabled @endif name="execution_time" class="form-control" wire:model.defer="execution_time" placeholder="{{__('data_field_name.ideal.execution_time')}}">
	                @error('execution_time')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="form-group">
	                <label>
	                    {{__('data_field_name.ideal.fee')}}
	                    <span class="text-danger">*</span>
	                </label>
	                <input type="text" @if($show_detail) disabled @endif id="fee" onkeypress="return isNumberKey(event)" name="fee" class="form-control format_number" wire:model.defer.defer="fee" placeholder="{{__('data_field_name.ideal.fee')}}">
	                @error('fee')
	                    <div class="text-danger mt-1">{{$message}}</div>
	                @enderror
	            </div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label>{{__('common.file')}}</label>
					@livewire('component.files', ['model_name' => $model, 'model_id' => $idealId ?? null, 'type' => $type_upload, 'folder' => 'ideal', 'acceptMimeTypes' => config('common.mime_type.general')])
				</div>
			</div>
	    </div>
	</div>
	@if($show_detail == false) 
	<div class="row">
		<div class="col-md-12">
            <div class="group-btn2 text-center">
                <a href="{{route('admin.research.ideal.index')}}" class="btn btn-cancel">{{__('common.button.cancel')}}</a>
                @if($disabled_save == true)
                	<button wire:click.prevent="saveIdeal()" disabled="" type="button" class="btn btn-save">{{__('common.button.save')}}</button>
                @else
                	<button wire:click.prevent="saveIdeal()" type="button" class="btn btn-save">{{__('common.button.save')}}</button>
                @endif
            </div>
        </div>
	</div>
	@endif
</div>
</div>
<script>
    function isNumberKey(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;
        return true;
    }
</script>
