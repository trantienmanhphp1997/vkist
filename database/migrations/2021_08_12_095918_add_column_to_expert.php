<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToExpert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expert', function (Blueprint $table) {
            $table->string('email',500)->nullable()->comment('email chuyên gia');
            $table->string('specialized',500)->nullable()->comment('Chuyên ngành');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expert', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('specialized');
        });
    }
}
