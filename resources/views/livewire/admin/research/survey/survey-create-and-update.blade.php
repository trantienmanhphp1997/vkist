<div class="col-md-10 col-xl-11 box-detail box-user">
    <div  class="breadcrumbs"><a href="{{route('admin.research.ideal.index')}}">{{__('research/ideal.breadcrumbs.research-ideal-management')}}</a> \ <span>{{__('menu_management.menu_name.survey')}}</span></div>
        <div class="creat-box bg-content border-20">
        <div class="detail-task pt-44">
            <h4>
                @if ($isEdit == false)  {{ __('data_field_name.request-survey-list.form_create_edit.create') }}
                @else  {{ __('data_field_name.request-survey-list.form_create_edit.edit') }}
                @endif
            </h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.form_create_edit.name') }}<span class="text-danger">&nbsp*</span></label>
                        <input type="text" class="form-control" placeholder="" wire:model.defer="name" @if($isEdit && ($status == App\Enums\ESurveyRequestStatus::IN_PROGRESS || $status == App\Enums\ESurveyRequestStatus::DONE)) readonly @endif>
                        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.form_create_edit.field') }}<span class="text-danger">&nbsp*</span></label>
                        <select  wire:model.defer="field_id" class="form-control" @if($isEdit && ($status == App\Enums\ESurveyRequestStatus::IN_PROGRESS || $status == App\Enums\ESurveyRequestStatus::DONE)) readonly disabled @endif>
                            <option value={{null}}>{{ __('data_field_name.request-survey-list.form_create_edit.field') }}</option>
                            @foreach($list_field as $item)
                                <option value="{{$item->id}}">{{$item->v_value}}</option>
                            @endforeach
                        </select>
                        @error('field_id')
                            <div class="text-danger mt-1">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.form_create_edit.start_date') }}<span class="text-danger">&nbsp*</span></label>
                        <span class="@if ($isEdit && $status == App\Enums\ESurveyRequestStatus::DONE) d-block @else d-none @endif">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'start_date_disable', 'place_holder'=>'','default_date' => $start_date, 'disable_input' => true ])
                        </span>
                        <span class="@if (!$isEdit || $status != App\Enums\ESurveyRequestStatus::DONE) d-block @else d-none @endif">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'start_date', 'place_holder'=>'','default_date' => $start_date ])
                        </span>
                        @error('start_date') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.form_create_edit.status') }}<span class="text-danger">&nbsp*</span></label>
                        <select class="form-control form-control-lg" name="status" id="status" wire:model.lazy="status">
                            <option value={{null}}>{{ __('data_field_name.request-survey-list.form_create_edit.status') }}</option>
                            <option value={{App\Enums\ESurveyRequestStatus::NOT_STARTED}}>{{App\Enums\ESurveyRequestStatus::valueToName(App\Enums\ESurveyRequestStatus::NOT_STARTED)}}</option>
                            <option value={{App\Enums\ESurveyRequestStatus::IN_PROGRESS}}>{{App\Enums\ESurveyRequestStatus::valueToName(App\Enums\ESurveyRequestStatus::IN_PROGRESS)}}</option>
                            <option value={{App\Enums\ESurveyRequestStatus::DONE}}>{{App\Enums\ESurveyRequestStatus::valueToName(App\Enums\ESurveyRequestStatus::DONE)}}</option>
                        </select>
                        @error('status') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.form_create_edit.end_date') }} <span class="text-danger">&nbsp*</span></label>
                        <span class="@if ($isEdit && $status == App\Enums\ESurveyRequestStatus::DONE) d-block @else d-none @endif">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'end_date_disable', 'place_holder'=>'','default_date' => $end_date, 'disable_input' => true ])
                        </span>
                        <span class="@if (!$isEdit || $status != App\Enums\ESurveyRequestStatus::DONE) d-block @else d-none @endif">
                            @include('layouts.partials.input._inputDate', ['input_id' => 'end_date', 'place_holder'=>'','default_date' => $end_date ])
                        </span>
                        @error('end_date') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.perfomer') }}<span class="text-danger">&nbsp*</span></label>
                        @livewire('component.advanced-select', [
                            'name' => 'user-info-ids',
                            'modelName' => App\Models\UserInf::class,
                            'searchBy' => ['code', 'fullname'],
                            'columns' => ['fullname'],
                            'multiple' => true,
                            'disabled' => $isEdit && $status == App\Enums\ESurveyRequestStatus::DONE,
                            'selectedIds' => $userInfoIds
                        ], key('hello' . $status))
                        @error('userInfoIds') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.form_create_edit.survey_target') }}<span class="text-danger">&nbsp*</span></label>
                        <textarea class="form-control" rows="3" wire:model.defer="survey_target"  @if($isEdit && ($status == App\Enums\ESurveyRequestStatus::IN_PROGRESS || $status == App\Enums\ESurveyRequestStatus::DONE)) readonly @endif></textarea>
                        @error('survey_target') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.form_create_edit.survey_content') }}<span class="text-danger">&nbsp*</span></label>
                        <textarea class="form-control" rows="3" wire:model.defer="survey_content"  @if($isEdit && ($status == App\Enums\ESurveyRequestStatus::IN_PROGRESS || $status == App\Enums\ESurveyRequestStatus::DONE)) readonly @endif></textarea>
                        @error('survey_content') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>{{ __('data_field_name.request-survey-list.form_create_edit.surveyed_unit') }}<span class="text-danger">&nbsp*</span></label>
                        <textarea class="form-control" rows="3" wire:model.defer="surveyed_unit"  @if($isEdit && ($status == App\Enums\ESurveyRequestStatus::IN_PROGRESS || $status == App\Enums\ESurveyRequestStatus::DONE)) readonly @endif></textarea>
                        @error('surveyed_unit') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="col-md-12 @if ($status == App\Enums\ESurveyRequestStatus::DONE) d-block @else d-none @endif">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{ __('data_field_name.request-survey-list.form_create_edit.result') }}<span class="text-danger">&nbsp*</span></label>
                                <select class="form-control form-control-lg" name="result" id="result" wire:model.defer="result">
                                    <option value={{null}}>{{ __('data_field_name.request-survey-list.form_create_edit.result') }}</option>
                                    <option value={{App\Enums\ESurveyRequestResult::SUBSTANDARD}}>{{App\Enums\ESurveyRequestResult::valueToName(App\Enums\ESurveyRequestResult::SUBSTANDARD)}}</option>
                                    <option value={{App\Enums\ESurveyRequestResult::STANDARD}}>{{App\Enums\ESurveyRequestResult::valueToName(App\Enums\ESurveyRequestResult::STANDARD)}}</option>
                                </select>
                                @error('result') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{ __('data_field_name.request-survey-list.form_create_edit.technology_demand') }}<span class="text-danger">&nbsp*</span></label>
                                <div class="d-flex">
                                    <input type="number" class="form-control" placeholder="" wire:model.defer="technology_demand" name="technology_demand">
                                    <span class="px-10 pt-14">%</span>
                                </div>
                                @error('technology_demand') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{ __('data_field_name.request-survey-list.form_create_edit.economic_efficiency') }}<span class="text-danger">&nbsp*</span></label>
                                <div class="d-flex">
                                    <input type="number" class="form-control percentage" placeholder="" wire:model.defer="economic_efficiency" name="economic_efficiency">
                                    <span class="px-10 pt-14">%</span>
                                </div>
                                @error('economic_efficiency') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 upload-container">
                    <h4 >
                        {{__('research/achievement.table_column.document')}}
                    </h4>
                    <form wire:submit.prevent="submit" enctype="multipart/form-data">
                        <div class="form-group upload-file">
                            <label></label>
                            <div class="bd-attached file_form">
                                <div class="attached-center">
                                    <input type="file" multiple onchange="uploadFile(this, @this)" id="file_create" class="custom-file-input cur_input">
                                    <span class="file" >
                                            {{__('research/achievement.attach-file')}}
                                        <img src="/images/Clip.svg" alt="" height="24">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12">
                        <div class="row">
                            @foreach ($listFileUpload as $key => $val)
                            <div class="form-group col-md-4">
                                <div class="attached-files">
                                    <img src="/images/File.svg" alt="file">
                                    <div class="content-file">
                                        <p>{{ $val['file_name'] }}</p>
                                        <p class="kb">{{ $val['size_file']}}</p>
                                    </div>
                                    <span>
                                        <button wire:click.prevent="deleteNewFileUpload({{$key}})"
                                                style="border: none;background: white" type="button"><img
                                                src="/images/close.svg" alt="close"/>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            @endforeach
                            @foreach ($listOldFileTemp as $key => $val)
                                <div class="form-group col-md-4">
                                    <div class="attached-files">
                                        <img src="/images/File.svg" alt="file">
                                        <div class="content-file" wire:click="download('{{$val['url']}}', '{{$val['file_name']}}')" style="cursor: pointer">
                                            <p>{{ $val['file_name'] }}</p>
                                            <p class="kb">{{ $val['size_file']}}</p>
                                        </div>
                                        <span>
                                            <button wire:click.prevent="deleteFileOldTemp({{$key}})"
                                                    style="border: none;background: white" type="button"><img
                                                    src="/images/close.svg" alt="close"/>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="file-loading position-absolute bg-primary rounded text-center text-white" style="transition: width 0.3s; bottom: 0px; left: 0; height: 5px; width: 0%;">&nbsp;</div>
                        <div class="row">
                            @error('fileUpload')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror

                            @error('current_total_file')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="text-danger upload-error"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center mt-42">
                <a href="{{route('admin.research.survey.index')}}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{__('common.button.cancel')}}</a>
                    <button type="button" wire:click="store" class="btn btn-main bg-primary text-9 size16 px-48 py-14">{{__('common.button.save')}}</button>
                </div>
            </div>
        </div>
    </div>
    <script>
            function uploadFile(input, proxy = null, index = 0) {
                let file = input.files[index];
                if (!file || !proxy) return;
                let $fileLoading = $(input).parents('.upload-container').find('.file-loading');
                let $error = $(input).parents('.upload-container').find('.upload-error');
                $fileLoading.width('0%');
                if(file.size/(1024*1024) >= {{$limit_file_size}}) {
                    console.log($error);
                    $error.html("{{ __('notification.upload.maximum_size', ['value' => $limit_file_size]) }}");
                    return;
                }
                let mimes = {!! json_encode($accept_file) !!};
                let extension = file.name.split('.').pop();
                if (!mimes.includes(extension)) {
                    $error.html("{{ __('notification.upload.mime_type', ['values' => implode(', ', $accept_file)]) }}");
                    return;
                }
                proxy.upload('fileUpload', file, (uploadedFilename) => {
                    setTimeout(() => {
                        $fileLoading.width('0%');
                        uploadFile(input, proxy, index + 1);
                    }, 500);
                }, () => {
                    console.log(error);
                }, (event) => {
                    $fileLoading.width(event.detail.progress + '%');
                });

            }
        </script>

</div>
