<?php

namespace App\Enums;

use App\Notifications\AbnormalLoginNotification;
use App\Notifications\IdeaDeadlineNotification;
use App\Notifications\ModuleDataChangeNotification;
use App\Notifications\NewUserInfoNotification;
use App\Notifications\PersonalWorkNotification;
use App\Notifications\TopicDeadlineNotification;
use App\Notifications\WorkResultNotification;
use ReflectionClass;

class ENotificationType
{
    // User Notification
    const ABNORMAL_LOGIN = 1;

    // User Info Notification
    const NEW_USER_INFO = 2;

    // Module Notification
    const MODULE_DATA_CHANGE = 3;

    // Personal Works Notification
    const PERSONAL_WORK = 4;

    // Idea Notification
    const IDEA_DEADLINE = 5;

    // Topic Notification
    const TOPIC_DEADLINE = 6;

    // Work Result Notification
    const WORK_RESULT = 7;

    // Delivering Notification
    const DELIVERING = 8;


    const MAINTENANCE = 9;

    public static function valueToName($value = 0)
    {
        $arr = self::getList();
        return $arr[$value] ?? $value;
    }

    public static function getList()
    {
        return (self::getGroup1() + self::getGroup2());
    }

    public static function getGroup1()
    {
        return [
            self::ABNORMAL_LOGIN => __('data_field_name.notification.type_abnormal_login'),
            self::NEW_USER_INFO => __('data_field_name.notification.type_new_user_info'),
            self::MODULE_DATA_CHANGE => __('data_field_name.notification.type_module_data_change'),
            self::PERSONAL_WORK => __('data_field_name.notification.type_personal_work'),
            self::WORK_RESULT => __('data_field_name.notification.type_work_result')
        ];
    }

    public static function getGroup2()
    {
        return [
            self::IDEA_DEADLINE => __('data_field_name.notification.type_idea_deadline'),
            self::TOPIC_DEADLINE => __('data_field_name.notification.type_topic_deadline'),
            self::DELIVERING => __('data_field_name.notification.type_delivering'),
            self::MAINTENANCE => __('data_field_name.notification.type_maintenance')
        ];
    }

    public static function inGroup1($notificationType = 0)
    {
        return in_array($notificationType, array_keys(self::getGroup1()));
    }

    public static function inGroup2($notificationType = 0)
    {
        return in_array($notificationType, array_keys(self::getGroup2()));
    }

    public static function getConstants()
    {
        $reflectionClass = new ReflectionClass(static::class);
        return $reflectionClass->getConstants();
    }

    public static function valueToClass($value = 0)
    {
        $arr = self::getClassList();
        return $arr[$value] ?? $value;
    }

    public static function getClassList()
    {
        return [
            self::ABNORMAL_LOGIN => AbnormalLoginNotification::class,
            self::NEW_USER_INFO => NewUserInfoNotification::class,
            self::MODULE_DATA_CHANGE => ModuleDataChangeNotification::class,
            self::PERSONAL_WORK => PersonalWorkNotification::class,
            self::WORK_RESULT => WorkResultNotification::class,
            self::IDEA_DEADLINE => IdeaDeadlineNotification::class,
            self::TOPIC_DEADLINE => TopicDeadlineNotification::class,
        ];
    }
}
