<div wire:ignore.self class="modal fade modal-fix" id="themoihdld" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!empty($this->id_edit))
                    <h6>{{__('data_field_name.labor-contract.edit')}}</h6>
                @else
                    <h6>{{__('data_field_name.labor-contract.add')}}</h6>
                @endif
            </div>
            <div class="modal-body" style="font-size: unset;">
                <form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.user_code')}} <span class="text-danger">*</span></label>
                                    @if (!empty($this->id_edit))
                                        <input list="staff-list" disabled="" type="text" wire:model.lazy="user_code">
                                    @else
                                        <input list="staff-list" type="text" wire:model.lazy="user_code">
                                    @endif
                                    <datalist id="staff-list">
                                        @foreach($user_data_list as $item)
                                            <option value="{{$item->code}}"></option>
                                        @endforeach
                                    </datalist>
                                    @error('user_code')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.user_name')}} <span class="text-danger">*</span></label>
                                    <input type="text" readonly="" wire:model.defer="user_name">
                                    @error('user_name')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.user_position')}} <span class="text-danger">*</span></label>
                                    <input type="text" readonly="" wire:model.defer="user_position">
                                    @error('user_position')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.contract_type')}} <span class="text-danger">*</span></label>
                                    @if (!empty($this->id_edit))
                                        <select wire:model="contract_type" disabled="">
                                            <option selected>{{__('common.select-box.choose')}}</option>
                                            <option value="1">{{__('data_field_name.labor-contract.work')}}</option>
                                            <option value="2">{{__('data_field_name.labor-contract.labor')}}</option>
                                        </select>
                                    @else
                                        <select wire:model="contract_type">
                                            <option selected>{{__('common.select-box.choose')}}</option>
                                            <option value="1">{{__('data_field_name.labor-contract.work')}}</option>
                                            <option value="2">{{__('data_field_name.labor-contract.labor')}}</option>
                                        </select>
                                    @endif
                                    @error('contract_type')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.contract_code')}} <span class="text-danger">*</span></label>
                                    @if (!empty($this->id_edit))
                                        <input type="text" disabled="" wire:model.defer="contract_code">
                                    @else
                                        <input type="text" wire:model.defer="contract_code">
                                    @endif
                                    @error('contract_code')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.duration_id')}} <span class="text-danger">*</span></label>
                                    <div class="have-btn">
                                        @if (!empty($this->id_edit))
                                            <select wire:model.defer="duration_id" disabled="" class=" mr-8">
                                                <option selected>{{__('common.select-box.choose')}}</option>
                                                @foreach($duration as $item)
                                                    @if($item->status == 1)
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        @else
                                            <select wire:model.defer="duration_id" class=" mr-8">
                                                <option selected>{{__('common.select-box.choose')}}</option>
                                                @foreach($duration as $item)
                                                    @if($item->status == 1)
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <button type="button" class="btn btn-add" wire:click="openModalSetting()">{{__('common.button.add2')}} <img src="/images/addblue.svg" class="p-0 ml-8" alt=""></button>
                                        @endif

                                    </div>
                                    @error('duration_id')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.status')}} <span class="text-danger">*</span></label>
                                    <select wire:model.defer="status">
                                        <option value="1">{{__('executive/contract.status.active')}}</option>
                                        <option value="-1">{{__('executive/contract.status.off')}}</option>
                                    </select>
                                    @error('status')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.working_type')}} <span class="text-danger">*</span></label>
                                    @if (!empty($this->id_edit))
                                        <select wire:model="working_type" disabled="">
                                            <option selected>{{__('common.select-box.choose')}}</option>
                                            <option value="1">{{__('data_field_name.labor-contract.all_time')}}</option>
                                            <option value="2">{{__('data_field_name.labor-contract.part_time')}}</option>
                                            <option value="3">{{__('data_field_name.labor-contract.collaborators')}}</option>
                                        </select>
                                    @else
                                        <select wire:model="working_type">
                                            <option selected>{{__('common.select-box.choose')}}</option>
                                            <option value="1">{{__('data_field_name.labor-contract.all_time')}}</option>
                                            <option value="2">{{__('data_field_name.labor-contract.part_time')}}</option>
                                            <option value="3">{{__('data_field_name.labor-contract.collaborators')}}</option>
                                        </select>
                                    @endif
                                    @error('working_type')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.start_date')}} <span class="text-danger">*</span></label>
                                    <span class="@if (!empty($this->id_edit)) d-block @else d-none @endif">
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'startDateDisable', 'place_holder'=>'', 'default_date' => is_null($start_date) ? date('Y-m-d') : $start_date, 'disable_input' => true])
                                    </span>
                                    <span class="@if (empty($this->id_edit)) d-block @else d-none @endif">
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'startDate', 'place_holder'=>'', 'default_date' => is_null($start_date) ? date('Y-m-d') : $start_date ])
                                    </span>
                                    @error('start_date')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.end_date')}} <span class="text-danger">*</span></label>
                                    <span class="@if (!empty($this->id_edit)) d-block @else d-none @endif">
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'endDateDisable', 'place_holder'=>'', 'default_date' => is_null($end_date) ? date('Y-m-d') : $end_date, 'disable_input' => true])
                                    </span>
                                    <span class="@if (empty($this->id_edit)) d-block @else d-none @endif">
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'endDate', 'place_holder'=>'', 'default_date' => is_null($end_date) ? date('Y-m-d') : $end_date ])
                                    </span>

                                    @error('end_date')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.basic_salary')}} <span class="text-danger">*</span></label>
                                    <input type="number" wire:model.defer="basic_salary">
                                    @error('basic_salary')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.social_insurance_salary')}} <span class="text-danger">*</span></label>
                                    @if($disabled_social_insurance_salary == true)
                                        <input type="number" class="bg-light" wire:model.defer="social_insurance_salary" disabled="">
                                    @else
                                        <input type="number" wire:model.defer="social_insurance_salary">
                                    @endif
                                    @error('social_insurance_salary')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-4 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.coefficients_salary')}} <span class="text-danger">*</span></label>
                                    <input list="coefficients-list" type="text" wire:model.defer="coefficients_salary" placeholder="{{__('common.select-box.choose')}}">
                                    <datalist id="coefficients-list">
                                        @foreach($coefficients_info as $item)
                                            <option value="{{$item->v_value}}"></option>
                                        @endforeach
                                    </datalist>
                                    @error('coefficients_salary')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.bank_account_number')}} <span class="text-danger">*</span></label>
                                    <input type="text" wire:model.defer="bank_account_number">
                                    @error('bank_account_number')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.bank_name')}} <span class="text-danger">*</span></label>
                                    <select  wire:model.defer="bank_name">
                                        <option selected>{{__('common.select-box.choose')}}</option>
                                        @foreach($bank_info as $item)
                                            <option value="{{$item->id}}">{{$item->v_value}}</option>
                                        @endforeach
                                    </select>
                                    @error('bank_name')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div class="form-group info-request">
                                    <label>{{__('data_field_name.labor-contract.note')}}</label>
                                    <textarea name="" id="" cols="30" rows="10" wire:model.defer="note"></textarea>
                                    @error('note')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                @livewire('component.files',['model_name'=>$model_name, 'type'=>$type,'folder'=>$folder,'status'=>-1, 'model_id' => $model_id])
                            </div>
                            @if(!empty($fileLists))
                                @foreach ($fileLists as $item)
                                    <div class="col-md-12">
                                        <div class="border border-light border-top-0 p-3 rounded-bottom file-list">
                                            <div class="d-inline-flex mr-2 mb-2 p-2 bg-light align-items-center rounded">
                                                <img src="/images/File.svg" alt="file" width="50px">
                                                <div>
                                                    <a href="{{ asset('storage/' . $item->url) }}" download>
                                                        <span class="d-block mb-0" style="word-break: break-all;">{{ $item->file_name }}</span>
                                                        <small class="kb">{{$item->size_file}}</small>
                                                    </a>
                                                </div>
                                                <button wire:click.prevent="deleteFile({{$item->id}})" class="btn text-muted">
                                                    <em class="fa fa-times"></em>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                <button id='btn-save' type="button" class="btn btn-primary" wire:click.prevent="saveData">{{__('common.button.save')}}</button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self class="modal fade modal-fix" id="themgiatri" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>{{__('data_field_name.labor-contract.set_value')}}</h6>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-12 pd-col">
                                <div class="form-group info-request searchbar">
                                    <label>{{__('common.place_holder.search')}}</label>
                                    <div class="position-relative">
                                        <input type="search" placeholder="{{__('data_field_name.labor-contract.enter_info')}}" wire:model.defer="filter_duration_name">
                                        <img  class="search-icon" src="/images/Search.svg" alt="search">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pd-col">
                                <div class="form-group info-request mb-12">
                                    <div class="have-btn">
                                        <label class="m-0">{{__('data_field_name.labor-contract.duration_id')}}</label>
                                        <button type="button" class="btn btn-add ml-auto" wire:click="$set('show_add_duration', true)"><img src="/images/addblue.svg" alt="">{{__('data_field_name.labor-contract.add_duration')}}</button>
                                    </div>
                                </div>
                            </div>
                            @foreach($duration as $index => $item)
                                <div class="col-12 pd-col">
                                    <div class="form-check info-request">
                                        @if($item->status == 1)
                                            <input type="checkbox" checked="" wire:click="toggleDisplaySetting({{$item->id}})" id="{{$index}}">
                                        @else
                                            <input type="checkbox" wire:click="toggleDisplaySetting({{$item->id}})" id="{{$index}}">
                                        @endif
                                        <label class="m-0" for="{{$index}}">{{$item->name}}</label>
                                        <a class="btn btn-remove ml-auto" href="#" wire:click="deleteSetting({{$item->id}})"><img src="/images/close.svg" alt=""></a>
                                    </div>
                                </div>
                            @endforeach
                            @if($show_add_duration)
                                <div class="col-12 pd-col">
                                    <div class="form-check info-request">
                                        <input type="checkbox" value="" wire:model.defer="duration_status">
                                        <input type="text" wire:model.defer="duration_name" class="h-40">
                                    </div>
                                    @error('duration_name')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                    @enderror
                                </div>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal" >{{__('common.button.cancel')}}</button>
                @if($show_add_duration)
                    <button id="btn-save-setting" type="button" class="btn btn-primary" wire:click.prevent="saveSetting">{{__('common.button.save')}}</button>
                @else
                    <button type="button" id="disabled-btn-save" class="btn btn-primary" disabled="">{{__('common.button.save')}}</button>
                @endif
            </div>
        </div>
    </div>
</div>
