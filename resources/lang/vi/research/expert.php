<?php
return [
    'table_column' => [
        'name_expert' => 'TÊN CHUYÊN GIA',
        'unit'=>'KHỐI ĐƠN VỊ',
        'academic_level' => 'TRÌNH ĐỘ HỌC VẤN',
        'specialized' => 'CHUYÊN NGÀNH',
        'work_unit' => 'ĐƠN VỊ CÔNG TÁC',
        'telephone_number' => 'SỐ ĐIỆN THOẠI',
        'address' => 'ĐỊA CHỈ',
        'email' => 'EMAIL',
        'action' => 'HÀNH ĐỘNG',
    ],
    'title' => [
        'part-1' => 'Hiển thị',
        'part-2' => 'chuyên gia trong',
    ],
    'list' => 'Quản lý chuyên gia',
    'list-new' => 'Danh sách chuyên gia',
    'add_expert' => 'Thêm chuyên gia',
    'edit_expert' => 'Chỉnh sửa chuyên gia',

    'name_expert' => 'Nhập tên chuyên gia',
    'academic_level' => 'Chọn trình độ học vấn',
    'specialized' => 'Chuyên ngành khoa học',
    'work_unit' => 'Nhập tên đơn vị',
    'telephone_number' => 'Nhập số điện thoại',
    'address' => 'Nhập địa chỉ',
    'email' => 'Nhập địa chỉ email',
    'close' => 'Đóng',
    'edit' => 'Chỉnh sửa',
    'delete' => 'Xóa',
    'create' => 'Tạo mới',


    'validate' => [
        'name_expert' => 'Tên chuyên gia',
        'academic_level' => 'Trình độ học vấn',
        'specialized' => 'Chuyên ngành',
        'work_unit' => 'Đơn vị công tác',
        'telephone_number' => 'Số điện thoại',
        'address' => 'Địa chỉ',
        'email' => 'Email',
        // 'telephoneNumberRegex' => 'Số điện thoại phải bắt đầu bằng 0',
        'telephoneNumberDigits' => 'Số điện thoải phải là chữ số',
    ],
];