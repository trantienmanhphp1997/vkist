<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtherCost extends Model
{
    use HasFactory;

    protected $table = "topic_other_fee";
    protected $fillable = [
        'name',
        'unit',
        'quantity',
        'price',
        'total',
        'state_capital',
        'other_capital',
    ];

    public function topicFeeDetail() {
        return $this->belongsTo(TopicFeeDetail::class, 'topic_fee_detail_id');
    }

    public function files() {
        return $this->hasMany(File::class, 'model_id')->where('files.type','=',Config::get('common.type_upload.OtherCost'));
    }
}
