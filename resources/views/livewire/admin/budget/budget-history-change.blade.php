<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('data_field_name.reality_expense.budget_control')}} </a> / <a
                    href="">{{__('data_field_name.budget.update')}} </a> /
                <span>{{__('data_field_name.budget.history_change')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('data_field_name.budget.history_change')}}</h3>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="table-responsive">
                        <table class="table working-plan" id="dataTable">
                            <thead>
                            <tr class="border-radius">
                                <th scope="col">{{__('data_field_name.budget.code')}}</th>
                                <th scope="col">{{__('data_field_name.budget.name')}}</th>
                                <th scope="col">{{__('data_field_name.budget.total')}}</th>
                                <th scope="col">{{__('data_field_name.budget.rate_difference')}}</th>
                                <th scope="col">{{__('data_field_name.budget.time_change')}}</th>
                                <th scope="col">{{__('data_field_name.budget.status')}}</th>
                                <th scope="col">{{__('data_field_name.budget.plan_budget')}}</th>
                                <th scope="col">{{__('data_field_name.instrument.department_id')}}</th>
                                <th scope="col">{{__('data_field_name.budget.project_used')}}</th>
                                <th scope="col">{{__('data_field_name.budget.number_change')}}</th>
                                <th scope="col">{{__('data_field_name.common_field.action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <div wire:loading class="loader"></div>
                            @foreach($data as $val)
                                <tr>
                                    <td class="text-center">{{$val->code}}</td>
                                    <td >{{$val->name}}</td>
                                    <td class="text-center">{{numberFormat($val->total_budget)}}</td>
                                    <td class="text-center">
                                        @foreach($val->rate as $value)
                                            @php
                                                $new_total = $val->total_budget ?? '0';
                                                $data_old= json_decode($value->note, true);
                                                $old_total = $data_old['total_budget'] ?? '0';
                                                if ($old_total != 0 ){
                                                    $rate_total = ($new_total - $old_total)/$old_total;
                                                }
                                            @endphp
                                            @if($old_total)
                                                @if($rate_total > 0)
                                                    +{{round($rate_total,1,PHP_ROUND_HALF_ODD)}}%
                                                @else
                                                    {{round($rate_total,1,PHP_ROUND_HALF_ODD)}}%
                                                @endif
                                            @else
                                                0%
                                            @endif

                                        @endforeach
                                    </td>
                                    <td class="text-center">{{reFormatDate($val->updated_at)}}</td>
                                    <td class="text-center">
                                        @if ($val->status== \App\Enums\EBudgetStatus::DONE)
                                            <button
                                                class="btn  mr0 btn-plan bg-blue medium "> {{__('data_field_name.budget.status_done')}}</button>
                                        @elseif($val->status== \App\Enums\EBudgetStatus::WAIT_CHANGE )
                                            <button
                                                class="btn mr0 btn-plan bg-blue emergency "> {{__('data_field_name.budget.status_wait_change')}}</button>
                                        @else
                                            <button class="btn  mr0 btn-plan emergency"></button>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <button type="button" data-toggle="modal" data-target="#fileBudget"
                                                                     wire:click="listFile({{ $val->id }})" class="btn btn-primary btn-sm par6">
                                            <img src="/images/file3.svg" alt="file"></button>
                                    </td>
                                    <td class="text-center">{{$val->department->name ?? ''}}</td>
                                    <td class=>{{$val->researchProject->name ?? ''}}</td>
                                    <td class="text-center">{{$val->version ?? '0'  }}</td>
                                    <td class="text-center"><a href="{{route('admin.budget.budget-history-change',['id'=>$val->id])}}">
                                            <img src="/images/eye.svg" alt="eye">
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                                        {{$data->links()}}
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="fileBudget" tabindex="-1"
         aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('data_field_name.budget.list_file')}}</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @forelse ($file as $key => $val)
                            <div class="form-group col-md-12" wire:click="download({{$val->id}})">
                                {{--                                <input type="radio" class=""  value="{{$val->id}}" wire:model.lazy="getIdFile" id="selectFile">--}}
                                <div class="attached-files">
                                    <img src="/images/File.svg" alt="file">
                                    <div class="content-file" >
                                        <p>{{ $val-> file_name }}</p>
                                        <p class="kb">{{ $val-> size_file }}</p>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>{{__('common.message.empty_search')}}</p>
                        @endforelse
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"
                            data-dismiss="modal">{{__('common.button.back')}}</button>
                    {{--                    <button type="button" wire:click.prevent="download()" class="btn btn-danger"--}}
                    {{--                             title="Download"  id="button_down" >Tải xuống</button>--}}
                </div>
            </div>
        </div>
    </div>
</div>


