<?php

namespace App\Http\Livewire\Admin\Research\Cost;


use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchPlan;
use App\Models\SalaryBasic;
use App\Models\Task;
use App\Models\Topic;
use App\Models\TopicDetailWork;
use App\Models\TopicFeeDetail;
use App\Models\TopicLaborParticipation;
use Illuminate\Support\Facades\Config;

class LaborParticipationList extends BaseLive
{
    public $topicFeeDetailId;
    public $user_info_id;
    public $number_workday;
    public $state_capital;
    public $other_capital;
    public $idEdit;
    public $topicFeeId;

    protected $listeners = ['resetData'];

    public function mount($topicFeeDetailId, $topicFeeId)
    {
        $this->topicFeeDetailId = $topicFeeDetailId;
        $this->topicFeeId = $topicFeeId;
    }

    public function render()
    {
        $query = TopicDetailWork::query();
        $query->with('topicHasUserInfo', 'topicHasUserInfo.userInfo', 'topicHasUserInfo.role');
        $query->selectRaw('sum(number_workday) as total_number_workday, sum(total) as total, topic_has_user_info_id,
                        sum(state_capital) as total_state_capital, sum(other_capital) as total_other_capital');
        $query->where('topic_fee_detail_id', '=', $this->topicFeeDetailId);

        $dataTotal = [
            'total' => $query->sum('total'),
            'state_capital' => $query->sum('state_capital'),
            'other_capital' => $query->sum('other_capital'),
        ];

        $query->groupBy('topic_has_user_info_id');

        $data = $query->paginate($this->perPage);

        $arrDataUser = $this->getDataSalaryBasic();

        return view('livewire.admin.research.cost.labor-participation-list', ['data' => $data, 'dataTotal' => $dataTotal, 'arrDataUser' => $arrDataUser]);
    }

    public function getDataSalaryBasic()
    {
        $details = TopicDetailWork::with('topicHasUserInfo')->where('topic_fee_detail_id', '=', $this->topicFeeDetailId)->get();
        $arrDataUser = [];
        if ($details->isNotEmpty()) {
            foreach ($details as $detail) {
                // $salaryBase = SalaryBasic::where('user_info_id', '=', $detail->topicHasUserInfo->user_info_id)->latest('id')->first();
                // $base_salary = 0;
                // if (!empty($salaryBase)) {
                //     $base_salary = $salaryBase->salary + $salaryBase->salary_special;
                // }
                $arrDataUser[$detail->topic_has_user_info_id] = [
                    'wage_coefficient' => $detail->wage_coefficient,
                    'base_salary' => $detail->base_salary,
                ];
            }
        }

        return $arrDataUser;
    }
}
