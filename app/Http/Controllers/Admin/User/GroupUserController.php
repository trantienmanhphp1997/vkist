<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GroupUserController extends Controller
{
    public function index(){
        return view('admin.user.groupUser.index');
    }

    public function create(){
        return view('admin.user.groupUser.create');
    }


    public function edit($id){
        return view('admin.user.groupUser.edit',compact('id'));
    }

    public function show($id){
        return view('admin.user.groupUser.detail',compact('id'));
    }
}
