<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $table = "notification";
    protected $fillable = [
        'type',
        'admin_id',
        'data',
        'read_at'
    ];

    protected $casts = [
        'data' => 'array'
    ];

    public function body()
    {
        $body = __($this->data['template_name'] ?? '', [
            'name' => $this->data['user']['name'] ?? '',
            'action' => (isset($this->data['action']) && !empty($this->data['action'])) ? __('notification.user.actions.' . $this->data['action']) : '',
            'model_type' => __('common.model.' . ($this->data['model']['type'] ?? '')),
            'model_value' => $this->data['model']['code'] ?? $this->data['model']['name'] ?? ''
        ]);
        return ($body);
    }

    public function getNameAttribute() {
        $templateName = $this->data['template_name'] ?? '';
        $arr = explode('.', $templateName);
        return strtoupper(array_pop($arr));
    }
}
