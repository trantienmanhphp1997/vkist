<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLengthStringNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_news', function (Blueprint $table) {
            $table->string('title', 1000)->nullable()->comment('Tên chuyên mục bài viết');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->string('title', 1000)->nullable('Tên tiêu đề bản tin');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('title');
        });
        Schema::table('category_news', function (Blueprint $table) {
            $table->dropColumn('title');
        });
    }
}
