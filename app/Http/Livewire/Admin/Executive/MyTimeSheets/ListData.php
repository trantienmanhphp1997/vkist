<?php

namespace App\Http\Livewire\Admin\Executive\MyTimeSheets;

use Livewire\Component;
use App\Models\TimeSheetsMonthly;
use App\Models\TimeSheetsDaily;
use App\Models\TimeSheetsFile;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\UserInf;
use App\Models\Contract;
use App\Enums\EWorkingStatus;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use App\Exports\TimekeepingExport;
use Excel;

class ListData extends Component
{

    public $filter_month_year;
    public $startOfMonth;
    public $endOfMonth;
    public $user;
    public $daily;

    public function mount() {
        $timeSheetsFile = TimeSheetsFile::select('month_working', 'year_working')->distinct()->orderBy('month_working', 'desc')->get();
        if (!empty(count($timeSheetsFile))) {
            $this->filter_month_year = $timeSheetsFile[0]->month_working . '/' . $timeSheetsFile[0]->year_working;
        } else {
            $this->filter_month_year = now()->format(Config::get('common.formatDate'));
        }
    }

    public function render() {
        $monthYear = explode('/', $this->filter_month_year);
        $startOfWeek = Carbon::create($monthYear[1], $monthYear[0])->startOfWeek();
        
        $this->startOfMonth = Carbon::create($monthYear[1], $monthYear[0])->startOfMonth();
        $this->endOfMonth = Carbon::create($monthYear[1], $monthYear[0])->endOfMonth();
        
        $day = $startOfWeek->diffInDays($this->endOfMonth);

        $user = UserInf::where('id', auth()->user()->user_info_id)->first();
        $this->user = $user;
        if (!empty($user)) {
            $userId = $user->id;
        } else {
            $userId = null;
        }
        $monthly = TimeSheetsMonthly::where('user_info_id', $userId)
            ->where('month_working', $monthYear[0])
            ->where('year_working', $monthYear[1])
            ->first();

        if (!empty($monthly)) {
            $daily = TimeSheetsDaily::where('timesheets_monthly_id', $monthly->id)
                ->orderBy('id', 'asc')
                ->get();
            $this->daily = $daily;
        } else {
            $daily = [];
        }

        $contract = Contract::where('user_info_id', $userId)->orderBy('id', 'desc')->first();

        $data = [];

        if (!empty(count($daily))) {
            $arr = [
                'day' => [],
                'timekeeping' => []
            ];
            Carbon::setWeekendDays([
                Carbon::SUNDAY,
            ]);
            $arrDay = [];
            $arrTimekeep = [];
            $ind = 0;
            for ($z = 0; $z <= $day; $z++) {
                $item = $startOfWeek->copy()->addDays($z);
                if (isset($daily[$ind]) && $item->day == $daily[$ind]->working_day) {
                    array_push($arrTimekeep, $daily[$ind]);
                    array_push($arrDay, $item);
                    $ind++;
                } else {
                    array_push($arrTimekeep, null);
                    array_push($arrDay, null);
                }
                $arr['day'] = $arrDay;
                $arr['timekeeping'] = $arrTimekeep;
                if ($item->isWeekend()) {
                    array_push($data, $arr);
                    $arr = [];
                    $arrDay = [];
                    $arrTimekeep = [];
                }
            }
            if (!empty(count($arr))) {
                array_push($data, $arr);
            }
        }

        $timeSheetsFile = TimeSheetsFile::select('month_working', 'year_working')->distinct()->orderBy('month_working', 'desc')->get();

        return view('livewire.admin.executive.my-time-sheets.list-data' , [
        	'user' => $user,
            'monthly' => $monthly,
            'daily' => $daily,
            'contract' => $contract,
            'listTimeSheet' => $data,
            'timeSheetsFile' => $timeSheetsFile
        ]);
    }

    public function export() {
        return Excel::download(new TimekeepingExport($this->daily, $this->filter_month_year, $this->user), 'time-keeping-' . str_replace('/', '-', $this->filter_month_year) . '-' . $this->user->fullname . '-' . '.xlsx');
    }
}
