<div class=" box-news" style="    max-width: calc(100% )!important;">
    <div class="row">
        <div class="col-md-6">
            <h4 class="title-page" style="
    margin-top: 20px;
">
                {{ __('news/newsManager.menu_name.list-new') }}
            </h4>
        </div>
        <div class="col-md-6">
            <div class="d-inline-block search-expertise">
                <div class="search-expertise">
                    <input type="text" class="form-control" style="border-radius: 12px;" placeholder="{{ __('data_field_name.department.search') }}" name="searchTerm" autocomplete="off" wire:model.debounce.1000ms="searchTerm">
                    <span>
                        <img src="/images/Search.svg" alt="search">
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" style="background: white;    margin-top: 15px;     padding: 10px 50px 0px;">
        @forelse($news as $key => $new)
            <div class="row box-new " wire:click.prevent="getDetail('{!! $new->slug !!}','{{ $new->category->slug ?? 'unkhown' }}')" style="padding: 10px 0">
                <div class="col-md-4" style="padding: 20px 10px 20px 0">
                    <img style=" width: 100%;border-radius: 8px ;" src="{{ trim($new->image_path) ? asset('storage/' . $new->image_path) : asset('images/img.jpg') }}" alt="">
                </div>
                <div class="col-md-8" style="padding: 20px 0 20px 20px;    text-align: justify;">
                    <h6 class="title-new">
                        {!! boldTextSearch($new->name, $searchTerm) !!}
                    </h6>
                    <div class="date date-detail new-detail-date">
                        <img src="{{ asset('images/date.svg') }}" alt="date">
                        {{ formatDateLocale($new->updated_at, '%a, %e/%m/%Y') }}
                    </div>
                    <div class="description-new">
                        {{ boldTextSearch(strLimit($new->content, 430), $searchTerm) }}
                    </div>
                    <a class="viewmore" style="cursor:pointer;" wire:click="getDetail('{{ $new->slug }}','{{ $new->category->slug ?? '' }}')">{!! __('news/newsManager.menu_name.see-more') !!}</a>
                </div>

            </div>
        @empty
            <div class="col-md-12">
                <div class="card">
                    <h5 class="title-new-empty">{{ __('status.news.empty') }}</h5>
                </div>
            </div>

        @endforelse
        {{-- Because she competes with no one, no one can compete with her. --}}
    </div>
    @include('livewire.common._modalDelete')
    <script>
        $(window).scroll(function() {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                window.livewire.emit('addTip', 10);
            }
        });
    </script>
</div>
