<?php

namespace App\Http\Livewire\Admin\Executive\MyDayOff;

use App\Enums\ERequestLeave;
use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\RequestLeave;
use App\Models\UserInf;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Excel;
use App\Http\Livewire\Base\BaseLive;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Http\UploadedFile;
use Livewire\WithFileUploads;
use App\Component\FileUpload;
class ApplyForLeave extends BaseLive
{
    use WithFileUploads;
    public $fromDate;
    public $toDate;
    public $fromHour;
    public $toHour;
    public $toTime;
    public $fromTime;
    public $leaveDay;
    public $leaveDayRemaining;
    public $type;
    public $content;
    public $approved_by;
    public $approved_by_list;
    public $modelId;
    public $request_leave;
    public $requestId;
    public $model_name;
    public $type_file;
    public $folder;
    public $disabled;
    protected $listeners = [
        'set-from-date' => 'setFromDate',
        'set-to-date' => 'setToDate',
    ];
    public function mount() {
        $this->fromDate = date('Y-m-d');
        $this->toDate = date('Y-m-d');
        $this->setFromDate(['from-date' => $this->fromDate]);
        $this->fromHour = "1";
        $this->toHour = "1";
    }
    public function updatedFromHour(){
        $this->setFromDate(['from-date' => $this->fromDate]);
    }
    public function updatedToHour(){
        $this->setFromDate(['from-date' => $this->fromDate]);
    }

    public function render() {
        $this->configFile();
        return view('livewire.admin.executive.my-day-off.apply-for-leave');
    }
    public function configFile(){
        $this->model_name = RequestLeave::class;
        $this->type_file = Config::get('common.type_upload.RequestLeave');
        $this->folder = app($this->model_name)->getTable();
    }

    public function save() {
        $this->fromTime = null;
        $this->toTime = null;
        if($this->fromDate && $this->fromHour){
            if($this->fromHour==1) {
                $hour='08:30';
            } else {
                $hour = '13:30';
            }
            $time = strtotime($this->fromDate.' '.$hour);
            $this->fromTime = date('Y-m-d H:i',$time);
        }

        if($this->toDate && $this->toHour){
            if($this->toHour==1) {
                $hour='12:00';
            } else {
                $hour = '17:30';
            }
            $time = strtotime($this->toDate.' '.$hour);
            $this->toTime = date('Y-m-d H:i',$time);
        }

        $date = date("Y-m-d");
        $this->validate([
            'fromTime' => 'required|afterOrEqual:'.$date,
            'toTime' => 'required|after:fromTime',
            'type' => 'required',
            'content' => 'required|max:100',
        ],[], [
            'fromDate' => __('executive/my-day-off.from_date'),
            'toDate' => __('executive/my-day-off.to_date'),
            'type' => __('executive/my-day-off.chose_leave_type'),
            'content' => __('executive/my-day-off.reason_for_leave'),
            'approved_by' => __('executive/my-day-off.approved_by'),
            'fromTime' => __('executive/my-day-off.from_date'),
            'toTime' => __('executive/my-day-off.to_date'),
        ]);
        $user= User::find(auth()->id());
        $this->request_leave = RequestLeave::create([
            'content' => $this->content,
            'start_date' => $this->fromTime,
            'end_date' => $this->toTime,
            'day_leave' => $this->leaveDay,
            'type' => $this->type,
            'admin_id' => auth()->id(),
            'status' => ERequestLeave::STATUS_NEW,
            'user_info_id'=>$user->user_info_id,
        ]);
        \App\Models\File::where('model_name', RequestLeave::class)
        ->where('type', Config::get('common.type_upload.RequestLeave'))->where('admin_id', auth()->id())->where('model_id', null)->update([
            'model_id' => $this->request_leave->id,
        ]);
        $this->disabled = true;
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('executive/my-day-off.register_success')]);

        $this->requestId =  time() . random_int(0,99);
    }

    public function saveReq() {
        $this->save();
        return redirect()->route('admin.executive.my-day-off.index');
    }

    public function sendGW() {
        $this->save();
        $this->emit('saveDataSendApproval');
    }

    public function setFromDate($data) {
        $start = Carbon::parse(reFormatDate($data['from-date'],'Y-m-d'));
        $this->fromDate = reFormatDate($data['from-date'],'Y-m-d');
        $end = Carbon::parse(reFormatDate($this->toDate,'Y-m-d'));
        $end->modify('+1 day');

        if(strtotime($start->format('Y-m-d'))>=strtotime($end->format('Y-m-d'))) {
            $countDate = 0;
        } else {
            $countDate = $end->diffInDays($start);
        }

        $days = $countDate*2;

        $fromHour = $this->fromHour;
        $toHour = $this->toHour;

        $holidays = \App\Models\Holiday::pluck('date_time')->toArray();
        foreach($holidays as $key => $value){
            $holidays[$key] = reFormatDate($holidays[$key],'d-m-Y');
        }

        $day_in_week = Carbon::parse(reFormatDate($this->fromDate,'Y-m-d'))->format('N');

        if($day_in_week%7==0||$day_in_week%7==6||in_array(reFormatDate($this->fromDate,'d-m-Y'),$holidays)) {
            $fromHour = 1;
        }
        $day_in_week = Carbon::parse(reFormatDate($this->toDate,'Y-m-d'))->format('N');
        if($day_in_week%7==0||$day_in_week%7==6||in_array(reFormatDate($this->toDate,'d-m-Y'),$holidays)) {
            $toHour = 2;
        }
        $days = $days + 1 - $fromHour - 2 + $toHour;
        $day_in_week = Carbon::parse($this->fromDate)->format('N');
        $sub_day = 0;

        $startDate = Carbon::parse($this->fromDate);
        for($i=0; $i < $countDate ; $i++){
            $date = $startDate->format('d-m-Y');
            $startDate->modify('+1 day');
            if($day_in_week%7==0||$day_in_week%7==6||in_array($date,$holidays)) {
                $sub_day+=2;
            }
            $day_in_week++;
        }
        $this->leaveDay = ($days-$sub_day)/2;
        $this->leaveDay = ($this->leaveDay>0)?$this->leaveDay:0;
        $userInf = Auth()->user()->info;
        $contract = '';
        if($userInf){
            $contract = \App\Models\Contract::where('user_info_id', $userInf->id)->orderBy('contract.start_date','ASC')->get()->first();
        }
        if(isset($contract->start_date)&&$contract->start_date){
            $date = $contract->start_date;
            $curentMonth =  (int)Carbon::parse(now())->format('m');
            $curentYear = (int)Carbon::parse(now())->format('Y');
            $curentDate = (int)Carbon::parse(now())->format('d');
            $signMonth = (int)Carbon::parse($date)->format('m');
            $signYear = (int)Carbon::parse($date)->format('Y');
            $signDate = (int)Carbon::parse($date)->format('d');
            if($curentYear==$signYear) {
                if($curentDate - $signDate < 0){
                    $countDate =  $curentMonth - $signMonth -1;
                }
                else if($curentDate - $signDate >= 0 &&$curentDate - $signDate <= 15) {
                    $countDate =  $curentMonth - $signMonth;
                } else {
                    $countDate =  $curentMonth - $signMonth +1;
                }
            }
            else {
                $countDate = $curentMonth;
            }
        }
        else {
            $countDate = 0;
        }
        $config = \App\Models\SystemConfig::where('type',2)->get()->first();
        $config = $config->content;
        $config = $config['unused_leave_to_new_year'];
        $currentYear = now()->format('Y');
        if($config['check']){
            if($config['expired_in']['check']){
                $date = '01/'.$config['expired_in']['value'].'/'.($currentYear);
                $date =  Carbon::parse(reFormatDate($date))->format('Y/m/t');
                $date1 = Carbon::parse(now()); //ngày hiện tại
                $date2 =  Carbon::parse($date);
                if($date1->greaterThan($date2)){
                    $leave_day_last_year = 0;
                }
                else {
                    if($config['value']=='limit'){
                        $limit = (int) $config['limit'];
                        $leave_day_last_year = (isset(auth()->user()->info->leave_day_last_year)&&auth()->user()->info->leave_day_last_year)?(float) auth()->user()->info->leave_day_last_year:0;
                        $leave_day_last_year = ($leave_day_last_year>$limit)?$limit:$leave_day_last_year;
                    }
                    else {
                        $leave_day_last_year = (isset(auth()->user()->info->leave_day_last_year)&&auth()->user()->info->leave_day_last_year)?(float) auth()->user()->info->leave_day_last_year:0;
                    }
                }
            }
            else {
                $leave_day_last_year = 0;
            }
        }
        else {
            $leave_day_last_year = 0;
        }
        $leave_day_current_year = (isset(auth()->user()->info->leave_day_current_year)&&auth()->user()->info->leave_day_current_year)?(float) auth()->user()->info->leave_day_current_year:0;
        $seniority = getSeniority();
        $this->leaveDayRemaining = (float) ($countDate + $leave_day_last_year + $seniority - $leave_day_current_year - (float) $this->leaveDay);
    }
    public function setToDate($data) {
        $start = Carbon::parse(reFormatDate($this->fromDate,'Y-m-d'));
        $end = Carbon::parse(reFormatDate($data['to-date'],'Y-m-d'));
        $this->toDate = reFormatDate($data['to-date'],'Y-m-d');
        $end->modify('+1 day');

        if(strtotime($start->format('Y-m-d'))>=strtotime($end->format('Y-m-d'))) {
            $countDate = 0;
        } else {
            $countDate = $end->diffInDays($start);
        }
        $days = $countDate*2;

        $fromHour = $this->fromHour;
        $toHour = $this->toHour;

        $holidays = \App\Models\Holiday::pluck('date_time')->toArray();
        foreach($holidays as $key => $value){
            $holidays[$key] = reFormatDate($holidays[$key],'d-m-Y');
        }
        $day_in_week = Carbon::parse(reFormatDate($this->fromDate,'Y-m-d'))->format('N');

        if($day_in_week%7==0||$day_in_week%7==6||in_array(reFormatDate($this->fromDate,'d-m-Y'),$holidays)) {
            $fromHour = 1;
        }
        $day_in_week = Carbon::parse(reFormatDate($this->toDate,'Y-m-d'))->format('N');
        if($day_in_week%7==0||$day_in_week%7==6||in_array(reFormatDate($this->toDate,'d-m-Y'),$holidays)) {
            $toHour = 2;
        }
        $days = $days + 1 - $fromHour - 2 + $toHour;
        $day_in_week = Carbon::parse($this->fromDate)->format('N');
        $sub_day = 0;

        $startDate = Carbon::parse($this->fromDate);
        for($i=0; $i < $countDate ; $i++){
            $date = $startDate->format('d-m-Y');
            $startDate->modify('+1 day');
            if($day_in_week%7==0||$day_in_week%7==6||in_array($date,$holidays)) {
                $sub_day+=2;
            }
            $day_in_week++;
        }
        $this->leaveDay = ($days-$sub_day)/2;
        $this->leaveDay = ($this->leaveDay>0)?$this->leaveDay:0;
        $userInf = Auth()->user()->info;
        $contract = '';
        if($userInf){
            $contract = \App\Models\Contract::where('user_info_id', $userInf->id)->orderBy('contract.start_date','ASC')->get()->first();
        }
        if(isset($contract->start_date)&&$contract->start_date){
            $date = $contract->start_date;
            $curentMonth =  (int)Carbon::parse(now())->format('m');
            $curentYear = (int)Carbon::parse(now())->format('Y');
            $curentDate = (int)Carbon::parse(now())->format('d');
            $signMonth = (int)Carbon::parse($date)->format('m');
            $signYear = (int)Carbon::parse($date)->format('Y');
            $signDate = (int)Carbon::parse($date)->format('d');
            if($curentYear==$signYear) {
                if($curentDate - $signDate < 0){
                    $countDate =  $curentMonth - $signMonth -1;
                }
                else if($curentDate - $signDate >= 0 &&$curentDate - $signDate <= 15) {
                    $countDate =  $curentMonth - $signMonth;
                }
                else {
                    $countDate =  $curentMonth - $signMonth +1;
                }
            }
            else {
                $countDate = $curentMonth;
            }
        }
        else {
            $countDate = 0;
        }
        $config = \App\Models\SystemConfig::where('type',2)->get()->first();
        $config = $config->content;
        $config = $config['unused_leave_to_new_year'];
        $currentYear = now()->format('Y');
        if($config['check']){
            if($config['expired_in']['check']){
                $date = '01/'.$config['expired_in']['value'].'/'.($currentYear);
                $date =  Carbon::parse(reFormatDate($date))->format('Y/m/t');
                $date1 = Carbon::parse(now()); //ngày hiện tại
                $date2 =  Carbon::parse($date);
                if($date1->greaterThan($date2)){
                    $leave_day_last_year = 0;
                }
                else {
                    if($config['value']=='limit'){
                        $limit = (int) $config['limit'];
                        $leave_day_last_year = (isset(auth()->user()->info->leave_day_last_year)&&auth()->user()->info->leave_day_last_year)?(float) auth()->user()->info->leave_day_last_year:0;
                        $leave_day_last_year = ($leave_day_last_year>$limit)?$limit:$leave_day_last_year;
                    }
                    else {
                        $leave_day_last_year = (isset(auth()->user()->info->leave_day_last_year)&&auth()->user()->info->leave_day_last_year)?(float) auth()->user()->info->leave_day_last_year:0;
                    }
                }
            }
            else {
                $leave_day_last_year = 0;
            }
        }
        else {
            $leave_day_last_year = 0;
        }
        $leave_day_current_year = (isset(auth()->user()->info->leave_day_current_year)&&auth()->user()->info->leave_day_current_year)?(float) auth()->user()->info->leave_day_current_year:0;
        $seniority = getSeniority();
        $this->leaveDayRemaining = (float) ($countDate + $leave_day_last_year + $seniority - $leave_day_current_year - (float) $this->leaveDay);
    }
}
