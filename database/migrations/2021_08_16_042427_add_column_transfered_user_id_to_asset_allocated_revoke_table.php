<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTransferedUserIdToAssetAllocatedRevokeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->foreignId('transfered_user_id')->nullable()->constrained('user_info')->comment('Người chuyển');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_allocated_revoke', function (Blueprint $table) {
            $table->dropColumn('transfered_user_id');
        });
    }
}
