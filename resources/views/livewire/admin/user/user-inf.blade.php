
<div class="row inner-tab">

        <div class="col-md-3">
            <div class="row">
                <div class="avater-user">
                    <span><img src="/images/Image.png" alt=""/></span>
                    <a href="" class="btn-pent"> <img src="/images/pent.svg" alt="edit"/>Sửa thông tin</a>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <h3 class="title-personal">Thông tin cá nhân</h3>
                <div class="col-md-12">
                   <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Họ và tên</label>
                                {!! Form::text('fullname', null, array('placeholder' => 'fullname','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Mã nhân viên</label>
                                {!! Form::text('code', null, array('placeholder' => 'code','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Đơn vị</label>
                                {!! Form::text('department_id', null, array('placeholder' => 'department_id','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Chức danh</label>
                                {!! Form::text('position_id', null, array('placeholder' => 'position_id','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Địa chỉ</label>
                            {!! Form::text('address', null, array('placeholder' => 'address','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Số điện thoại</label>
                            {!! Form::text('phone', null, array('placeholder' => 'phone','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Ngày sinh</label>
                            {!! Form::date('birthday', null, array('placeholder' => 'birthday','class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Giới tính</label>
                            <select class="form-control form-control-lg">
                                <option >Greece</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Hôn nhân</label>
                            <select class="form-control form-control-lg">
                                <option >Greece</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Email</label>
                            {!! Form::text('email', null, array('placeholder' => 'email','class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h3 class="title-personal">THÔNG TIN KHÁC</h3>
            <div class="col-md-12">
               <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Dân tộc</label>
                        {!! Form::text('ethnic_id', null, array('placeholder' => 'ethnic_id','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tôn giáo</label>
                        <select class="form-control form-control-lg">
                            <option >Greece</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Quốc tịch</label>
                        <select class="form-control form-control-lg">
                            <option >Greece</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Số CMND/ CCCD</label>
                        {!! Form::text('id_card', null, array('placeholder' => 'id_card','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Ngày cấp</label>
                        {!! Form::date('issued_date', null, array('placeholder' => 'issued_date','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nơi cấp</label>
                        {!! Form::text('issued_place', null, array('placeholder' => 'issued_place','class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Số hộ chiếu</label>
                        {!! Form::text('passport_number', null, array('placeholder' => 'passport_number','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Ngày cấp</label>
                        {!! Form::text('passport_date', null, array('placeholder' => 'passport_date','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nơi cấp</label>
                        {!! Form::text('passport_place', null, array('placeholder' => 'passport_place','class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="row">
            <h3 class="title-personal">GHI CHÚ</h3>
            <div class="col-md-12">
                     {!! Form::textarea('note', null, array('placeholder' => 'note','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="group-btn text-center">
                    <button type="submit" class="btn btn-save">Lưu lại</button>
                </div>
            </div>
        </div>

</div>

</div>
