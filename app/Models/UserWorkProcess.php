<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use Illuminate\Support\Facades\Config;
use PhpOffice\PhpSpreadsheet\Shared\OLE\PPS;

class UserWorkProcess extends Eloquent
{
    use HasFactory;
    protected $table = "user_working_process";
    protected $guarded=['*'];
    protected $fillable = [
        'user_info_id',
        'start_date',
        'end_date',
        'position_id',
        'department_id',
        'major',
        'work_unit',
    ];

    public function position() {
        return $this->belongsTo(MasterData::class, 'position_id');
    }

    public function department() {
        return $this->belongsTo(Department::class, 'department_id');
    }
    public function userInfo(){
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }

}
