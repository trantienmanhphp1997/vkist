<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_leave', function (Blueprint $table) {
            $table->id();
            $table->string('content', 1000)->nullable()->comment('noi dung nghi phep');
            $table->dateTime('start_date')->nullable()->comment('ngay bat dau nghi phep');
            $table->dateTime('end_date')->nullable()->comment('ngay ket thuc nghi phep');
            $table->bigInteger('user_info_id')->nullable()->constrained('user_info')->comment('Nguoi xin nghi phep');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_leave');
    }
}
