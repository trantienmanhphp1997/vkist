<?php

namespace App\Http\Livewire\Admin\Research\Cost;

use App\Enums\EAssetCost;
use App\Enums\ETopicFee;
use App\Models\TopicFee;
use App\Models\TopicFeeDetail;
use App\Models\AssetCost;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Topic;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class AssetCostList extends BaseLive
{
    public $topicFeeDetailId;
    public $name;
    public $unit;
    public $quantity;
    public $price;
    public $state_capital;
    public $other_capital;
    public $type;
    public $assetCostID;
    public $model;
    public $type_upload;
    public $topicFeeId;
    public $total;
    public $topicFeeStatus;
    public $updateMode = false;
    public $displayAttachFile = true;



    public function mount($topicFeeDetailId, $topicFeeId, $topicFeeStatus) {
        $this->updateMode = false;
        $this->type_upload = Config::get('common.type_upload.AssetCost');
        $this->topicFeeDetailId = $topicFeeDetailId;
        $this->topicFeeId = $topicFeeId;
        $this->topicFeeStatus = $topicFeeStatus;
    }

    public function render()
    {
        $this->model = AssetCost::class;
        $this->updateTopicFeeDetail();
        $query = AssetCost::query();
        $query->where('topic_fee_detail_id', '=', $this->topicFeeDetailId);
        $dataTotal = [
            'total' => $query->sum('total'),
            'state_capital' => $query->sum('state_capital'),
            'other_capital' => $query->sum('other_capital'),
        ];

        $data = $query->with('files')->orderBy('id', 'desc')->paginate($this->perPage);

        if ($this->quantity != null && $this->price != null) {
            $total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
            $this->total = numberFormat($total);
        }

        $listType = EAssetCost::getListType();

        return view('livewire.admin.research.cost.asset-cost-list', [
            'dataTopicFee' => $this->topicFeeDetailId, 'listType' => $listType,
            'model' => $this->model, 'type_upload' => $this->type_upload, 'model_id'=>$this->assetCostID, 'updateMode'=>$this->updateMode, 
            'data' => $data, 'dataTotal' => $dataTotal,
        ]);
    }

    public function validateData()
    {
        $this->validate([
            'name' => 'required|max:100',
            'unit' => 'required|max:10',
            'type' => 'required',
            'quantity' => 'required|regex:/^[0-9,]+$/|max:7',
            'price' => 'required|regex:/^[0-9,]+$/|max:14',
            'state_capital' => 'nullable|regex:/^[0-9,]+$/|max:18',
            'other_capital' => 'nullable|regex:/^[0-9,]+$/|max:18',
            ], [
                'quantity.max' => __('client_validation.form.research_cost.max.quantity'),
                'price.max' => __('client_validation.form.research_cost.max.price'),
                'state_capital.max' => __('client_validation.form.research_cost.max.state_capital'),
                'other_capital.max' => __('client_validation.form.research_cost.max.other_capital'),
            ], [
            'name' => __('data_field_name.research_cost.name'),
            'unit' => __('data_field_name.research_cost.unit'),
            'type' => __('data_field_name.research_cost.type_cost'),
            'quantity' => __('data_field_name.research_cost.quantity'),
            'price' => __('data_field_name.research_cost.price'),
            'state_capital' => __('data_field_name.research_cost.state_capital'),
            'other_capital' => __('data_field_name.research_cost.from_state_other'),

        ]);
    }
    public function store()
    {

        $this->validateData();
        $assetCost = new assetCost;
        $assetCost->name = $this->name;
        $assetCost->type = $this->type;
        $assetCost->unit = $this->unit;
        $assetCost->quantity = removeFormatNumber($this->quantity);
        $assetCost->price = removeFormatNumber($this->price);
        $assetCost->state_capital = removeFormatNumber($this->state_capital);
        $assetCost->other_capital = removeFormatNumber($this->other_capital);
        $assetCost->total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
        $assetCost->topic_fee_detail_id = $this->topicFeeDetailId;
        $assetCost->admin_id = Auth::user()->id;
        $assetCost ->save();
        \App\Models\File::where('model_name', AssetCost::class)
                ->where('type', $this->type_upload)->where('admin_id', auth()->id())->where('model_id', null)->update([
                    'model_id' => $assetCost->id,
        ]);
        $this->displayAttachFile = false;
        $this->emit('close-modal-create-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function resetInputFields(){
        $this->displayAttachFile = true;
        $this->name = null;
        $this->unit = null;
        $this->quantity = null;
        $this->price = null;
        $this->state_capital = null;
        $this->other_capital = null;
        $this->type = null;
        $this->total = null;
        $this->resetValidation();
    }

    public function edit($id){
        $this->updateMode = true;
        $this->assetCostID = $id;
        $assetCost = AssetCost::findOrFail($id);
        $this->name = $assetCost->name;
        $this->type = $assetCost->type;
        $this->unit = $assetCost->unit;
        $this->quantity = numberFormat($assetCost->quantity);
        $this->price = numberFormat($assetCost->price);
        $this->state_capital = numberFormat($assetCost->state_capital);
        $this->other_capital = numberFormat($assetCost->other_capital);
        $this->total = numberFormat($assetCost->total);
        $this->resetValidation();
    }

    public function update(){
        $assetCost = AssetCost::findOrFail($this->assetCostID);
        $this->validateData();
        $assetCost->name = $this->name;
        $assetCost->type = $this->type;
        $assetCost->unit = $this->unit;
        $assetCost->quantity = removeFormatNumber($this->quantity);
        $assetCost->price = removeFormatNumber($this->price);
        $assetCost->state_capital = removeFormatNumber($this->state_capital);
        $assetCost->other_capital = removeFormatNumber($this->other_capital);
        $assetCost->total = removeFormatNumber($this->price)*removeFormatNumber($this->quantity);
        $assetCost->save();
        \App\Models\File::where('model_name', AssetCost::class)
        ->where('type', $this->type_upload)->where('admin_id', auth()->id())->where('model_id', null)->update([
            'model_id' => $assetCost->id,
        ]);
        $this->emit('close-modal-edit-material');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }

    public function deleteId($id){
        $this->assetCostID = $id;
    }

    public function delete(){
        $assetCost = AssetCost::findOrFail($this->assetCostID);

        $assetCost->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }

    public function updateTopicFeeDetail(){
        $topicFeeDetail = TopicFeeDetail::findOrFail($this->topicFeeDetailId);
        $topicFeeDetail->total_capital = AssetCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('total');
        $topicFeeDetail->state_capital = AssetCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('state_capital');
        $topicFeeDetail->other_capital = AssetCost::where('topic_fee_detail_id', $this->topicFeeDetailId)->sum('other_capital');
        $topicFeeDetail->save();
    }

    public function resetMode(){
        $this->updateMode = false;
    }

    public function download($id){
        $file = \App\Models\File::findOrFail($id);
        return Storage::download($file->url, $file->file_name);
    }
}
