<div class="col-md-10 col-xl-11 box-detail box-user">
	<div class="row">
		<div class="col-md-12">
				<div  class="breadcrumbs"><a href="{{route('admin.research.ideal.index')}}">{{__('executive/contract.breadcrumbs.activity-management')}}</a> \ <span>{{__('executive/import-time-sheets.title')}}</span></div> 
		</div>
	</div>
	<div class="row bd-border">
		<div class="col-md-12">
			<h3 class="title">{{__('executive/import-time-sheets.title')}}</h3>
			<div class="information">
				<div class="inner-tab pt0">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group search-expertise">
								
							</div>
						</div>
						<div class="col-md-4 ">
							@if($checkCreatePermission)
								<div class="form-group float-right">
									<a href="#" data-toggle="modal" data-target="#import" class="btn btn-viewmore-news mr0" wire:click="resetData">
										<img src="/images/plus2.svg" alt="plus">{{__('executive/import-time-sheets.btn-add')}}
									</a>
								</div>
							@endif
						</div>
					</div>
					<table class="table">
						<thead>
							<th scope="col" class="text-center border-radius-left">
	                            {{__('data_field_name.import-time-sheets.index')}}
	                        </th>
							<th scope="col" class="text-center">
								{{__('executive/import-time-sheets.table_column.name')}}
							</th>
							<th scope="col" class="text-center">
								{{__('data_field_name.import-time-sheets.month_placeholder')}}
							</th>
							<th scope="col" class="text-center">
								{{__('data_field_name.import-time-sheets.year_placeholder')}}
							</th>
							<th scope="col" class="text-center border-radius-right">
								{{__('executive/contract.table_column.action')}}
							</th>
						</thead>
						<div wire:loading class="loader"></div>
						<tbody>
							@foreach($data as $index => $row)
								<tr>
									<td class="text-center">
										{{$index + 1}}
									</td>
									<td class="text-left">
										<a href="{{route('admin.executive.time-sheets.show', ['id' => $row->id])}}">{!! $row->name !!}</a>
									</td>
									<td class="text-center">{{App\Enums\EMonth::valueToName($row->month_working)}}</td>
									<td class="text-center">{{$row->year_working}}</td>
									<td class="text-center">
										@if($checkDestroyPermission)
											<button type="button" class="btn par6" title="{{__('common.button.delete')}}" wire:click="$set('deleteId', {{$row->id}})" data-target="#exampleDelete" data-toggle="modal">
												<img src="/images/trash.svg" alt="trash">
											</button>
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					@if(count($data) > 0)
						{{$data->links()}}
					@else
						<div class="title-approve text-center ml-0">
							<span>{{__('common.message.empty_search')}}</span>
						</div>
					@endif
					@include('livewire.common._modalDelete')
					@include('livewire.admin.executive.time-sheets.modal-import')
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	 $("document").ready(() => {
	 	window.livewire.on('closeModalImport', () => {
	    	$('#import').modal('hide');
	    });
	    $('#import').on('hidden.bs.modal', () => {
		 	$('#result').modal('show');
		});
	 });
</script>
