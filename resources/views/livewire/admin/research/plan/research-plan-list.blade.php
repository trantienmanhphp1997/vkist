<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="">{{__('data_field_name.research_plan.research_plan_control')}}</a>\ <span>{{__('data_field_name.research_plan.plan_control')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <h3 class="title">{{__('data_field_name.research_plan.research_plan_list')}}</h3>
            <div class="col-md-12 approve">
                <div class="row rowUser">
                    <div class="col-md-12">
                        <div id="search" class="w-100">
                            <div class="row  box-filter">
                                <div class="col-xs-12 col-md-10 d-flex filter-left">
                                    <div class="d-inline-block search-expertise mb-24 w-xs-100 pr-8  ">
                                        <div class="search-expertise">
                                            <input type="text" class="form-control size13 w-xs-100 " placeholder="{{__('data_field_name.research_plan.search')}}" name="searchTerm" autocomplete="off" wire:model.debounce.1000ms="searchTerm">
                                            <span>
                                                <img src="/images/Search.svg" alt="search">
                                            </span>
                                        </div>
                                    </div>

                                    <div class="px-8 d-flex w-180">
                                        @include('layouts.partials.input._inputDate', ['input_id' => 'from_date','place_holder' => __('data_field_name.research_plan.start_time'), 'set_null_when_enter_invalid_date' => true ])
                                    </div>
                                    <div class="px-8 d-flex w-180">

                                        @include('layouts.partials.input._inputDate', ['input_id' => 'to_date' ,'place_holder' => __('data_field_name.research_plan.end_time'),'set_null_when_enter_invalid_date' => true ])
                                    </div>
                                    <div class="px-8 d-flex w-180">
                                        {{ Form::select('source', ['' =>__('data_field_name.research_plan.status')] + $researchSoure, null, ['class' => 'form-control size13', 'wire:model.lazy' => 'searchStatus']) }}
                                    </div>
                                </div>
                                <div class="col-md-2 text-right justify-content-end">
                                    @include('livewire.common.buttons._createRP')
                                </div>
                            </div>
                        </div>
                        <table class="table table-general text-2">
                            <thead>
                                <tr class="border-radius">
                                    <th scope="col" class="text-center border-radius-left">{{__('data_field_name.research_plan.plan')}}</th>
                                    <th scope="col" class="text-center">{{__('data_field_name.research_plan.topic')}}</th>
                                    <th scope="col" class="text-center">{{__('data_field_name.research_plan.topic_manager')}}</th>
                                    <th scope="col" class="text-center">{{__('data_field_name.research_plan.status')}}</th>
                                    <th scope="col" class="text-center">{{__('data_field_name.research_plan.start_time')}}</th>
                                    <th scope="col" class="text-center">{{__('data_field_name.research_plan.end_time')}}</th>
                                    <th scope="col" class="text-center border-radius-right text-center">{{__('data_field_name.research_plan.action')}}</th>
                                </tr>
                            </thead>
                            <div wire:loading class="loader"></div>
                            <tbody>
                                @foreach($data as $row)
                                <tr border-radius>
                                    <td class="border-radius-left"><a href="{{route('admin.research.plan.research_plan.edit', ['id' => $row->id])}}">{!! boldTextSearch($row->name,$searchTerm) !!}</a></td>
                                    <td class="border-radius-left">{!! boldTextSearch($row->topic->name ?? '',$searchTerm) !!}</td>
                                    <td class="text-center">{!! boldTextSearch($row->topic->leader->fullname ?? '',$searchTerm) !!}</td>
                                    <td class="text-center">{{\App\Enums\EResearchPlan::valueToName($row->status)}}</td>
                                    <td class="text-center">{{reFormatDate($row->topic->start_date?? null)}}</td>
                                    <td class="text-center">{{reFormatDate($row->topic->end_date ?? null)}}</td>
                                    <td class="text-center">
                                        @if($row->status == \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT)
                                        @if($checkEditPermission)
                                        <button  data-toggle="modal" id="show_modal_edit" data-target="#target" title="Sửa" wire:click="edit({{ $row->id }})" class="btn-sm border-0 bg-transparent mr-1 show_modal_edit">
                                            <img src="/images/edit.png" alt="edit">
                                        </button>
                                        @endif
                                        @endif
                                        @if($row->status == \App\Enums\EResearchPlan::STATUS_NOT_SUBMIT)
                                        @if($checkDestroyPermission)
                                        <button title="{{__('common.button.delete')}}" type="button" style="background-color: white; border:none;" wire:click="deleteId({{$row->id}})" data-target="#deleteModal" data-toggle="modal">
                                            <img src="/images/trash.svg" alt="trash">
                                        </button>
                                        @endif
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if(count($data) > 0)
                        {{$data->links()}}
                        @else
                        <div class="title-approve text-center">
                            <span>{{__('common.message.empty_search')}}</span>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Delete -->
    <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body box-user">
                    <h4 class="modal-title" id="exampleModalLabel">{{__('notification.member.warning.warning')}}</h4>
                    {{__('notification.member.warning.warning_delete?')}}
                </div>
                <div class="group-btn2 text-center pt-24">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.back')}}</button>
                    <button type="button" class="btn btn-save" data-dismiss="modal" wire:click="delete()">{{__('common.button.delete')}}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Delete -->
    <!-- Modal Create -->
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body box-user">
                        <h4 class="modal-title text-center" id="exampleModalLabel">{{__('data_field_name.research_plan.create_research_plan')}}</h4>
                        <form>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{__('data_field_name.research_plan.plan_name')}}<span class="text-danger">(*)</label>
                                <input type="text" wire:model.lazy="name" class="form-control" id="exampleFormControlInput1" placeholder="{{__('data_field_name.research_plan.input_name')}}">
                                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror

                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput4">{{__('data_field_name.research_plan.topic')}}<span class="text-danger">(*)</label>
                                <select name="topic_id" id="exampleFormControlInput4" class="form-control" wire:model.lazy="selectedTopicId">
                                    <option value="">--{{__('data_field_name.research_plan.select_topic')}}--</option>
                                    @foreach($topicList as $value)
                                    <option value="{{$value->id}}">{{$value->name ?? ''}}</option>
                                    @endforeach
                                </select>
                                @error('selectedTopicId') <span class="text-danger error">{{ $message }}</span>@enderror

                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput3">{{__('data_field_name.research_plan.time')}}</label>
                                <div class="form-group form-inline">
                                    <div class="form-control bg-content">
                                        <div class="row ">
                                            @if($selectedTopicId)
                                            <h6 class="ml-3 mt-2"><label style="color:#4E5D78;font-size:14px" for="exampleFormControlInput3">{{__('data_field_name.research_plan.from')}}</label></h6>
                                            @endif
                                            <input style="border:none; color:#4E5D78;font-size:14px" disabled class="col-3  pl-3 ml-3" type="text" value="{{($selectedTopicId)?reFormatDate($topic->start_date ?? ''):''}}">
                                            @if($selectedTopicId)
                                            <h6 class="mt-2"><label style="color:#4E5D78;font-size:14px" for="exampleFormControlInput3">{{__('data_field_name.research_plan.to')}}</label></h6>
                                            @endif
                                            <input style="border:none; color:#4E5D78;font-size:14px" disabled type="text" class="col-3 ml-3 mr-3" value="{{($selectedTopicId)?reFormatDate($topic->end_date ?? ''):''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput3">{{__('data_field_name.research_plan.topic_manager')}}</label>
                                <input type="text" readonly class="d-flex form-control" value="{{($selectedTopicId)?$topic->leader->fullname?? '':''}}">

                            </div>
                        </form>
                    </div>
                    <div class="group-btn2 text-center  pt-12">
                        <button type="button" id="close-modal-create-research-plan" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.close')}}</button>
                        <button type="submit" wire:click.prevent="store()" class="btn btn-save" wire:loading.attr="disabled">{{__('common.button.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Create -->
    <!-- Modal Edit -->
    <form wire:submit.prevent="submit">
        <div wire:ignore.self class="modal fade" id="target" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body box-user">
                        <h4 class="modal-title text-center" id="exampleModalLabel">{{__('data_field_name.research_plan.edit_research_plan')}}</h4>
                        <form>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">{{__('data_field_name.research_plan.plan_name')}}<span class="text-danger">(*)</label>
                                <input type="text" wire:model.lazy="name" class="form-control" id="exampleFormControlInput1" placeholder="{{__('data_field_name.research_plan.input_name')}}">
                                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput4">{{__('data_field_name.research_plan.topic')}}<span class="text-danger">(*)</label>
                                <select name="topic_id" disabled id="exampleFormControlInput4" class="form-control" wire:model.lazy="selectedTopicId">
                                    <option value="">--{{__('data_field_name.research_plan.select_topic')}}--</option>
                                    @foreach($topicList as $value)
                                    <option value="{{$value->id}}">{{$value->name ?? ''}}</option>
                                    @endforeach
                                </select>
                                @error('selectedTopicId') <span class="text-danger error">{{ $message }}</span>@enderror
                            
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput3">{{__('data_field_name.research_plan.time')}}</label>
                                <div class="form-group form-inline">
                                    <div class="form-control " disabled>
                                        <div class="row ">
                                            @if($selectedTopicId)
                                            <h6 class="ml-3 mt-2"><label style="color:#4E5D78;font-size:14px" for="exampleFormControlInput3">{{__('data_field_name.research_plan.from')}}</label></h6>
                                            @endif
                                            <input style="border:none; color:#4E5D78;font-size:14px" disabled class="col-3  pl-3 ml-3" type="text" value="{{($selectedTopicId)?reFormatDate($topic->start_date ?? ''):''}}">
                                            @if($selectedTopicId)
                                            <h6 class="mt-2"><label style="color:#4E5D78;font-size:14px" for="exampleFormControlInput3">{{__('data_field_name.research_plan.to')}}</label></h6>
                                            @endif
                                            <input style="border:none; color:#4E5D78;font-size:14px" disabled type="text" class="col-3 ml-3 mr-3" value="{{($selectedTopicId)?reFormatDate($topic->end_date ?? ''):''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput3">{{__('data_field_name.research_plan.topic_manager')}}</label>
                                <input type="text" disabled class="d-flex form-control" value="{{($selectedTopicId)?$topic->leader->fullname?? '':''}}">

                            </div>
                        </form>
                    </div>
                    <div class="group-btn2 text-center  pt-12">
                        <button type="button" id="close-modal-edit-research-plan" class="btn btn-cancel" data-dismiss="modal">{{__('common.button.close')}}</button>
                        <button type="submit" wire:click.prevent="update()" class="btn btn-save" wire:loading.attr="disabled">{{__('common.button.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Edit-->
    <script>
        $("document").ready(function() {
            window.livewire.on('close-modal-create-research-plan', () => {
                document.getElementById('close-modal-create-research-plan').click()
            });
            window.livewire.on('close-modal-edit-research-plan', () => {
                document.getElementById('close-modal-edit-research-plan').click()
            });
        });
    </script>
</div>