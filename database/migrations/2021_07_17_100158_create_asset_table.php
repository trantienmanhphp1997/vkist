<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',255)->unique()->comment('Mã tài sản');
            $table->string('name', 500)->nullable();
            $table->string('series_number',255)->nullable()->unique()->comment('Mã tài sản');
            $table->integer('quantity')->nullable()->comment('so luong');
            $table->bigInteger('original_price')->nullable()->comment('nguyên giá');
            $table->date('gifting_date')->nullable()->comment('ngay ghi tang');
            $table->integer('follow_year')->nullable()->comment('nam theo doi');
            $table->string('unit_type')->nullable()->comment('quy cách');
            $table->date('buy_date')->nullable()->comment('ngay mua');
            $table->date('wasting_start_date')->nullable()->comment('ngay bat dau tinh su hao mon');
            $table->tinyInteger('status')->nullable()->comment('trạng thái:1: báo mat, 2: bao hong, 3:Cap nhat, 4: thu hoi,5: bao duong');
            $table->tinyInteger('situation')->nullable()->comment('tinh trang hoat dong: 2: đã thu hồi,1:dang su dung, 0:chưa sử dung');
            $table->softDeletes();
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->foreignId('category_id')->nullable()->constrained('asset_category')->comment('Loai tài sản map voi category_asset');
            $table->foreignId('user_info_id')->nullable()->constrained('user_info')->comment('Bộ phận sử dụng map user_info');
            $table->date('guarantee_date')->nullable()->comment('thoi gian bao hanh');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset');
    }
}
