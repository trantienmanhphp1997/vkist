<?php

namespace App\Http\Livewire\Admin\Asset\Maintenance;

use App\Http\Livewire\Base\BaseLive;
use Livewire\Component;

class ListAll extends BaseLive
{
    public function render()
    {
        return view('livewire.admin.asset.maintenance.list-all');
    }
}
