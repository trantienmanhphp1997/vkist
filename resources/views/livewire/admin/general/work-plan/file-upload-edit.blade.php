<div class="col-md-12">
    <div class="col-md-12">
        <form wire:submit.prevent="submit">
            <div class="form-group upload-file">
                <label>{{__('data_field_name.work_plan.create.file_title')}}</label>
                <div class="bd-attached">
                    <div class="attached-center">
                        <input type="file"
                               class="custom-file-input" id="input-file-edit"
                               wire:model.lazy="file">
                        <span class="file">{{__('data_field_name.work_plan.create.file_btn')}} <img
                                src="/images/Clip.svg" alt="" height="24"></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <div class="form-group form-inline">
            @foreach($files as $item)
            <div class="attached-files col-md-4">
                <img src="/images/File.svg" alt="file">
                <div class="content-file">
                    <p>{{$item->file_name}}</p>
                    <p class="kb">{{$item->size_file}}</p>
                </div>
                <span>
                                    <button wire:click.prevent="deleteFile({{$item->id}})"
                                            style="border: none;background: white" type="button"><img
                                            src="/images/close.svg" alt="close"/></button>
                                </span>
            </div>
            @endforeach
        </div>
    </div>
</div>
