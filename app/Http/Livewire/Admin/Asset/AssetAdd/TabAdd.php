<?php

namespace App\Http\Livewire\Admin\Asset\AssetAdd;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use App\Models\AssetAllocatedRevoke;
use App\Models\AssetCategory;
use App\Models\Budget;
use App\Models\File;
use App\Service\Community;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Livewire\Component;
use App\Http\Livewire\Base\BaseTrimString;


class TabAdd extends BaseTrimString

{
    public $department_id;

    public $category_id;
    public $name;
    public $code;
    public $quantity;
    public $unit_id;
    public $origin;
    public $buy_date;
    public $contract_number;
    public $asset_provider_id;
    public $series_number;
    public $asset_value;
    public $unit_type;
    public $note;
    public $fixed_assets;

    public $expiry_warranty_date;
    public $warranty_date;
    public $warranty_type_date;
    public $condition_warranty;
    public $maintenance_date;
    public $maintenance_type_date;
    public $maintenance_start_date;

    public $distribute_value;
    public $distribute_start_date;
    public $depreciation_value_previous;

    public $status;
    public $user_info_id;
    public $implementation_date;
    public $number_report;
    public $allocate_quantity;
    public $amount_used;

    public $data_quantity;
    public $increase_code;
    public $group_asset;

    protected $listeners=['getValue','getValueWarranty','getValueDistribute'];
    public $getValue;

    public function render()
    {
        return view('livewire.admin.asset.asset-add.tab-add');
    }
    public function save(){

        $warranty_json='';
        $maintenance_json='';


        if ($this->warranty_date&&$this->warranty_type_date){
            $data_json = ['warranty_date'=>$this->warranty_date,'warranty_type_date'=>$this->warranty_type_date];
            $warranty_json=json_encode($data_json);
        }
        if ($this->maintenance_date&&$this->maintenance_type_date){
            $data_maintenance_json = ['maintenance_date'=>$this->maintenance_date,'maintenance_type_date'=>$this->maintenance_type_date];
            $maintenance_json=json_encode($data_maintenance_json);
        }

        $increase_year = Asset::query()->pluck('code')->toArray();

//        $sub=[];
//        foreach ($increase_year as $value){
//            $sub[] = strstr($value,'.');
//        }
//        $now_code = strstr($this->code,'.');

        $this->emit('saveTabAdd');
        $val=Validator::make(
            [
                'department_id'=>$this->department_id,
                'category_id'=>$this->category_id,
                'name' =>$this->name,
                'code' =>$this->code,
                'quantity' => $this->quantity,
                'series_number'=>$this->series_number,
                'contract_number'=>$this->contract_number,
                'unit_type'=>$this->unit_type,
                'note'=>$this->note,
                'asset_value'=>$this->asset_value,
                'distribute_value'=>$this->distribute_value,
                'origin'=>$this->origin
            ]
            ,[
            'department_id' => 'required',
            'category_id' => 'required',
            'name' => ['required','regex:/^[a-zA-Z' . config('common.special_character.alphabet') .']+$/', 'max:100'],
            'code'=>['required','regex:/^[A-Z0-9' . config('common.special_character.alphabet') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:25','unique:asset,code'],
            'quantity' => 'max:7',
            'series_number' => ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:20','nullable'],
            'contract_number'=> ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:20','nullable'],
            'unit_type' =>  ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:255','nullable'],
            'note' =>  ['regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/\#\@\%\$\&\^\*\+\ ]+$/', 'max:255','nullable'],
            'asset_value'=>'max:10',
            'distribute_value' => ['regex:/^[0-9]+$/', 'max:2','nullable'],
            'origin'=>'required'
        ],[],[
            'department_id' => __('data_field_name.asset.department'),
            'category_id' => __('data_field_name.asset.asset_cate'),
            'name' => __('data_field_name.asset.asset_name'),
            'code' => __('data_field_name.asset.asset_code'),
            'quantity' => __('data_field_name.asset.quantity'),
            'series_number'=>__('data_field_name.asset.serial'),
            'contract_number'=>__('data_field_name.asset.contract_number'),
            'unit_type'=>__('data_field_name.asset.unit_type'),
            'note'=>__('data_field_name.asset.note'),
            'asset_value'=>__('data_field_name.asset.asset_value'),
            'distribute_value' => __('data_field_name.asset.number_distribute_year'),
            'origin'=>__('data_field_name.asset.origin')
        ]);

        if ($val->fails()) {
            $this->dispatchBrowserEvent('show-toast', ['type' => 'error', 'message' => __('data_field_name.topic.error_tab_general')]);
            $this->emit('click-tab-info');
        }
        $val->validate();
        $situtation ='';
        if($this->status==1){
            $situtation = 1;
        }else{
            $situtation = 0;
        }



        if ($this->status){
            $validator = Validator::make(['number_report'=>$this->number_report],[
                'number_report' =>  'required|max:10',
            ],[
                'number_report' => __('data_field_name.asset.number_report'),
            ]);

            if ($validator->fails()) {
                $this->emit('click-tab-warranty');
                $this->dispatchBrowserEvent('show-toast', ['type' => 'error', 'message' => __('data_field_name.asset.error_tab_warranty')]);
            }
            $validator->validate();
//            dd($this->distribute_start_date);
            $data = Asset::create([
                'department_id'=>$this->department_id,
                'category_id'=>$this->category_id,
                'name'=>$this->name,
                'code'=> $this->code,
                'quantity'=>$this->quantity,
                'unit_id'=> $this->unit_id,
                'origin'=> $this->origin ,
                'buy_date'=>$this->buy_date ,
                'contract_number'=>$this->contract_number ,
                'asset_provider_id'=>$this->asset_provider_id,
                'series_number'=>$this->series_number,
                'asset_value'=>Community::getAmount($this->asset_value),
                'unit_type'=>$this->unit_type,
                'note'=>$this->note,
                'fixed_assets'=>$this->fixed_assets,
                'warranty_date_json'=>$warranty_json,
                'expiry_warranty_date'=>$this->expiry_warranty_date,
                'condition_warranty'=>$this->condition_warranty,
                'maintenance_date_json'=>$maintenance_json,
                'maintenance_start_date'=>$this->maintenance_start_date,
                'distribute_value'=>$this->distribute_value,
                'distribute_start_date'=>$this->distribute_start_date,
                'situation'=>$situtation,
                'user_info_id'=>$this->user_info_id,
                'amount_used'=>$this->allocate_quantity,
                'increase_code'=>$this->increase_code,
                'depreciation_value_previous'=>Community::getAmount($this->depreciation_value_previous),
                'group_asset'=>$this->group_asset
            ]);
            AssetAllocatedRevoke::create([
                'asset_id'=>$data->id,
                'status'=>$this->status,
                'user_info_id'=>$this->user_info_id,
                'implementation_date'=>$this->implementation_date,
                'number_report'=>$this->number_report,
                'implementation_quantity'=>$this->allocate_quantity
            ]);
            \App\Models\File::where('model_name', Asset::class)
                ->where('type', Config::get('common.type_upload.Asset'))
                ->where('admin_id', auth()->id())
                ->where('status',-1)
                ->where('model_id', null)
                ->update([
                    'model_id' => $data->id,
                    'status'=>1
                ]);
        }else{

            $data = Asset::create([
                'department_id'=>$this->department_id,
                'category_id'=>$this->category_id,
                'name'=>$this->name,
                'code'=> $this->code,
                'quantity'=>$this->quantity,
                'unit_id'=> $this->unit_id,
                'origin'=> $this->origin ,
                'buy_date'=>$this->buy_date ,
                'contract_number'=>$this->contract_number ,
                'asset_provider_id'=>$this->asset_provider_id,
                'series_number'=>$this->series_number,
                'asset_value'=>Community::getAmount($this->asset_value),
                'unit_type'=>$this->unit_type,
                'note'=>$this->note,
                'fixed_assets'=>$this->fixed_assets,
                'warranty_date_json'=>$warranty_json,
                'expiry_warranty_date'=>$this->expiry_warranty_date,
                'condition_warranty'=>$this->condition_warranty,
                'maintenance_date_json'=>$maintenance_json,
                'maintenance_start_date'=>$this->maintenance_start_date,
                'distribute_value'=>$this->distribute_value,
                'distribute_start_date'=>$this->distribute_start_date,
                'situation'=>$situtation,
                'user_info_id'=>$this->user_info_id,
                'amount_used'=>$this->allocate_quantity,
                'increase_code'=>$this->increase_code,
                'depreciation_value_previous'=>Community::getAmount($this->depreciation_value_previous),
                'group_asset'=>$this->group_asset
            ]);
            \App\Models\File::where('model_name', Asset::class)
                ->where('type', Config::get('common.type_upload.Asset'))
                ->where('admin_id', auth()->id())
                ->where('status',-1)
                ->where('model_id', null)
                ->update([
                    'model_id' => $data->id,
                    'status'=>1
                ]);
        }

        session()->flash("success", __('notification.common.success.add'));
        return $this->redirectRoute('admin.asset.index');

    }

    public function cancel(){
        \App\Models\File::where('model_name', Asset::class)
            ->where('type', Config::get('common.type_upload.Asset'))
            ->where('admin_id', auth()->id())
            ->where('status',-1)
            ->where('model_id', null)
            ->delete();
        return $this->redirectRoute('admin.asset.index');
    }
    public function getValue($fixed_assets,$department_id,$category_id, $name,$code,$quantity,
                             $unit_id,$origin,$buy_date,$contract_number,$asset_provider_id,
                             $series_number,$asset_value,$unit_type,$note,$data_quantity,$increase_code,$group_asset)
    {
        $this->department_id = $department_id;
        $this->category_id = $category_id;
        $this->name = $name;
        $this->code = $code;
        $this->quantity = $quantity;
        $this->unit_id = $unit_id;
        $this->origin = $origin;
        $this->buy_date = $buy_date;
        $this->contract_number = $contract_number;
        $this->asset_provider_id = $asset_provider_id;
        $this->series_number = $series_number;
        $this->asset_value = $asset_value;
        $this->unit_type = $unit_type;
        $this->note = $note;
        $this->fixed_assets = $fixed_assets;
        $this->data_quantity = $data_quantity;
        $this->increase_code = $increase_code;
        $this->group_asset = $group_asset;

    }
    public function getValueWarranty($warranty_date,$warranty_type_date,
                                     $expiry_warranty_date,$condition_warranty,
                                     $maintenance_date,$maintenance_type_date,$maintenance_start_date,
                                     $status,$user_info_id,$implementation_date,$number_report,$allocate_quantity
    ){
        $this->warranty_date = $warranty_date;
        $this->warranty_type_date = $warranty_type_date;
        $this->expiry_warranty_date = $expiry_warranty_date;
        $this->condition_warranty = $condition_warranty;
        $this->maintenance_date= $maintenance_date;
        $this->maintenance_type_date= $maintenance_type_date;
        $this->maintenance_start_date= $maintenance_start_date;

        $this->status = $status;
        $this->user_info_id=$user_info_id;
        $this->implementation_date=$implementation_date;
        $this->number_report=$number_report;
        $this->allocate_quantity=$allocate_quantity;

    }
    public function getValueDistribute($distribute_value, $distribute_start_date,$depreciation_value_previous)
    {
        $this->distribute_value= $distribute_value;
        $this->distribute_start_date= $distribute_start_date;
        $this->depreciation_value_previous=$depreciation_value_previous;
    }


}
