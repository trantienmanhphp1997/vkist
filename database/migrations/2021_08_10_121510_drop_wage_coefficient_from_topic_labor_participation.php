<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropWageCoefficientFromTopicLaborParticipation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_labor_participation', function (Blueprint $table) {
            $table->dropColumn('wage_coefficient');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_labor_participation', function (Blueprint $table) {
            $table->string('wage_coefficient', 10)->comment('He so tien cong');
        });
    }
}
