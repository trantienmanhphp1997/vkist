<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class StoreRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'unique:roles', 'max:48', 'regex:/^[a-zA-Z0-9'.config('common.special_character.VN').'\.\,\-\(\)\/ ]+$/'],
            'code' => ['required', 'unique:roles', 'max:48', 'regex:/^[a-zA-Z0-9]+$/'],
            'note' => ['nullable', 'regex:/^[a-zA-Z0-9'.config('common.special_character.VN').'\.\,\-\(\)\/ ]+$/', 'max:255'],
        ];
        $data = request()->all();
        if (array_key_exists('id', $data)) {
            $role = Role::findOrFail($data['id']);
            if (!empty($role)) {
                if ($role->name == $data['name']) {
                    unset($rules['name']);
                }
                if ($role->code == $data['code']) {
                    unset($rules['code']);
                }
            }
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => __('data_field_name.system.role.name'),
            'code' => __('data_field_name.system.role.code'),
            'note' => __('data_field_name.common_field.descriptions'),
        ];
    }
}
