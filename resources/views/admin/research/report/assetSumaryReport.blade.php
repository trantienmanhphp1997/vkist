<div class="report-main-text text-center">
    <h3>{{__('research/report.report2-page.title')}}</h3>
    <p>{{__('research/report.report2-page.sub-title')}}</p>
</div>
<div class="col-md-12 download-report-btn">
    @if(checkPermission('admin.asset.report.download'))
        <div class=" float-right">
            {{--                                wire:click='export'--}}
            <button type="button"  class="btn par6" data-toggle="modal" data-target="#exportModal"
                    title="{{__('common.button.export_file')}}"><img src="/images/filterdown.svg" alt="filter-down"></button>
        </div>
    @endif
</div>
<div class="table-responsive">
    <table class="table table-general working-plan">
        <thead>
        <tr class="border-radius text-center">
            <th scope="col">
                {{__('research/report.table.stt')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.code')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.name')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.not-use')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.in-use')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.in-repaired')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.in-maintenance')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.report-lost')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.losted')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.cancel')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.report-liquidation')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.liquidation')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.transfer')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.recall')}}
            </th>
            <th scope="col">
                {{__('research/report.report2-page.table.total')}}
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($assetSumaryStatus as $asset)
            <tr class="border-radius text-center">
                <td class="text-center">
                    {{ ($assetSumaryStatus->currentPage() - 1) * $assetSumaryStatus->perPage() + $loop->iteration }}
                </td>
                <td class="text-center">
                    {{$asset->code}}
                </td>
                <td class="text-center">
                    {{$asset->name}}
                </td>
                <td class="text-center">
                    {{$asset->notUse}}
                </td>
                <td class="text-center">
                    {{$asset->amount_used}}
                </td>
                <td class="text-center">
                    {{$asset->inRepaired}}
                </td>
                <td class="text-center">
                    {{$asset->inMaintenance}}
                </td>
                <td class="text-center">
                    {{$asset->report_lost}}
                </td>
                <td class="text-center">
                    {{$asset->losted}}
                </td>
                <td class="text-center">
                    {{$asset->cancel}}
                </td>
                <td class="text-center">
                    {{$asset->report_liquidation}}
                </td>
                <td class="text-center">
                    {{$asset->liquidation}}
                </td>
                <td class="text-center">
                    {{$asset->transfer}}
                </td>
                <td class="text-center">
                    {{$asset->recall}}
                </td>
                <td class="text-center">
                    {{$asset->amountStatus}}
                </td>
            </tr>
        @endforeach
        <tr class="border-radius">
            <td class="text-center" colspan="3" style="background: #F6F6F7;">
                {{__('research/report.report2-page.total')}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetNotUse}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetUsed}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetInRepaired}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetInMaintenance}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetReportLost}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetLost}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetCancel}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetReportLiquidation}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetLiquidation}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetTransfer}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->countAssetRecall}}
            </td>
            <td class="text-center" style="background: #F6F6F7;">
                {{$assetSumaryStatus->amountStatus}}
            </td>

        </tr>
        </tbody>
    </table>
    @if (count($assetSumaryStatus) > 0)
        {{ $assetSumaryStatus->links() }}
    @else
        <div class="title-approve text-center">
            <span>{{ __('common.message.empty_search') }}</span>
        </div>
    @endif
</div>
