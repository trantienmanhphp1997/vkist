<div>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:ignore.self class="modal fade modal-custom-appraisal" id="changePassword" tabindex="-1" aria-labelledby="changePassword" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content personnel">
                    <div class="modal-body box-user">
                        <h4 class="text-center">{{__('data_field_name.profile.change_password')}}</h4>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.user.current_password')}}<span class="text-danger">*</span></label>
                                <input type="password" class="form-control" wire:model.defer="current_password" />
                                @error('current_password') <span class="error text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.user.new_password')}}<span class="text-danger">*</span></label>
                                <input type="password" class="form-control" wire:model.defer="password" />
                                @error('password') <span class="error text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('data_field_name.profile.password_confirmation')}}<span class="text-danger">*</span></label>
                                <input type="password" class="form-control" wire:model.defer="password_confirmation" />
                                @error('password_confirmation') <span class="error text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>

                        <div class="attached-center">
                            <button type="button" id="close-modal-edit-department" class="btn btn-secondary close-btn" data-dismiss="modal">{{__('common.button.close')}}</button>
                            <button type="button" wire:click.prevent="updatePassword()" class="btn btn-primary">{{__('common.button.send')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $('#changePassword').on('hidden.bs.modal', function () {
        $('.error').remove();
        $('#changePassword input').val('');
    });
</script>
