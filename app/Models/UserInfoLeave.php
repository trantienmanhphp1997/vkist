<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;



class UserInfoLeave extends BaseModel 
{
    use HasFactory;
    protected $table = "user_info_leave";
    protected $guarded=['*'];

    public function department() {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function position() {
        return $this->belongsTo(MasterData::class, 'position_id');
    }
}

