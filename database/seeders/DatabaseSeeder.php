<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Database\Seeders\DeparmentSeeder;
use Database\Seeders\BusinessFeeSeeder;
use Database\Seeders\AccountSeeder;
use Database\Seeders\TopicSeeder;
use Database\Seeders\ResearchPlanSeeder;
use Database\Seeders\UserInfSeeder;
use Database\Seeders\MasterDBSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            // basic
            MasterDBSeeder::class,
            RoleSeeder::class,
            UserDBSeeder::class,
            DeparmentSeeder::class,
            UserInfSeeder::class,
            MenuSeeder::class,
            PermissionsSeeder::class,

            // seed for account
            // AccountSeeder::class,

            // seed for topic
            ResearchCategorySeeder::class,
            IdealSeeder::class,
            TopicSeeder::class,

            HolidaySeeder::class,
            // BusinessFeeSeeder::class,
            // // ResearchPlanSeeder::class,
        ]);
    }
}
