@extends('layouts.master')
@section('title')
    <title>Danh sách đề tài</title>
@endsection
@section('homeLeft')
@include('layouts.partials.sliderbar._researchLeft')
@endsection
@section('content')
@livewire('admin.research.topic.topic-list')
@endsection