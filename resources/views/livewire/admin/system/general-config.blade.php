<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="{{ route('admin.system.general-config.index') }}">{{ __('menu_management.menu_name.general_configuration') }}</a></div>
        </div>
    </div>
    <div class="bd-border p-4">
        <form class="bg-white p-4">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12 row mb-2">
                        <div class="col-md-3">
                            <label class="d-inline-block">{{__('data_field_name.general_config.date_update_basic_salary')}}</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" wire:model.defer="dateImportBasicSalary.value" class="form-control w-50 input-placeholder d-inline-block" placeholder="13,14,15"><span class="text_monthly">({{ __('data_field_name.general_config.text_monthly') }})</span>
                            @error('dateImportBasicSalary.value') <div><span class="error text-danger">{{ $message }}</span></div>@enderror
                        </div>
                    </div>
                    <div class="col-md-12 row mb-2">
                        <div class="col-md-3">
                            <label class="d-inline-block">{{__('data_field_name.general_config.date_update_position_salary')}}</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" wire:model.defer="dateImportPositionSalary.value" class="form-control w-50 input-placeholder d-inline-block" placeholder="28,29,30"><span class="text_monthly">({{ __('data_field_name.general_config.text_monthly') }})</span>
                            @error('dateImportPositionSalary.value') <div><span class="error text-danger">{{ $message }}</span></div>@enderror
                        </div>
                    </div>
                    <div class="col-md-12 row mb-2">
                        <div class="col-md-3">
                            <label class="d-inline-block">{{__('data_field_name.general_config.date_create_budget_plan')}}</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" wire:model.defer="dateCreateBudgetPlan.value" class="form-control w-50 input-placeholder d-inline-block" placeholder="28,29,30"><span class="text_monthly">({{ __('data_field_name.general_config.text_monthly') }})</span>
                            @error('dateCreateBudgetPlan.value') <div><span class="error text-danger">{{ $message }}</span></div>@enderror
                        </div>
                    </div>
                    <div class="col-md-12 row mb-2">
                        <div class="col-md-3">
                            <label class="d-inline-block">{{__('data_field_name.general_config.password_default')}}</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" wire:model.defer="passwordDefault.value" class="form-control w-50 input-placeholder d-inline-block">
                            @error('passwordDefault.value') <div><span class="error text-danger">{{ $message }}</span></div>@enderror
                        </div>
                    </div>
                    <div class="col-md-12 row mb-2">
                        <div class="col-md-3">
                            <label class="d-inline-block">{{__('data_field_name.general_config.min_length_of_password')}}</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" wire:model.defer="minLengthOfPassword.value" class="form-control w-50 input-placeholder d-inline-block">
                            @error('minLengthOfPassword.value') <div><span class="error text-danger">{{ $message }}</span></div>@enderror
                        </div>
                    </div>
                    <div class="col-md-12 row mb-2">
                        <div class="col-md-3">
                            <label class="d-inline-block">{{__('data_field_name.general_config.password_special_requirement')}}</label>
                        </div>
                        <div class="col-md-9">
                            <input type="checkbox" wire:model.defer="passwordSpecialRequirement.value">
                            @error('passwordSpecialRequirement.value') <div><span class="error text-danger">{{ $message }}</span></div>@enderror
                        </div>
                    </div>
                    <div class="col-md-12 row mb-2">
                        <div class="col-md-3">
                            <label class="d-inline-block">{{__('data_field_name.general_config.seniority_does_not_include_vacation_day')}}</label>
                        </div>
                        <div class="col-md-9">
                            <input type="checkbox" wire:model.defer="seniorityDoesNotIncludeVacationDay.value">
                            @error('seniorityDoesNotIncludeVacationDay.value') <div><span class="error text-danger">{{ $message }}</span></div>@enderror
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.system.general-config.index') }}" class="btn btn-main bg-4 mr-20 text-3 size16 px-48 py-14">{{ __('common.button.cancel') }}</a>
                <button type="button" wire:click="save" class="btn btn-main bg-primary text-9 size16 px-48 py-14">{{ __('common.button.save') }}</button>
            </div>
        </form>
    </div>
</div>
