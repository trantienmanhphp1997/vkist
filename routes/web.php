<?php

use App\Http\Livewire\Product\Index;
use App\Http\Livewire\Shop\Cart;
use App\Http\Livewire\Shop\Checkout;
use App\Models\User;
use App\Notifications\AbnormalLoginNotification;
use App\Notifications\Handlers\NotificationHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function() {
    dd(App\Models\AppraisalBoard::query()->with('chairman.userInfo')->find(21));
});

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();
Route::group(['middleware' => ['auth','password_changed', 'HtmlMinifier']], function () {
    Route::get('/errors/403', 'App\Http\Controllers\Admin\Errors\ErrorsController@indexWarning')->name('errors.403');
    Route::get('errors/404','App\Http\Controllers\Admin\Errors\ErrorsController@indexErrors')->name('errors.404');
    Route::get('errors/500','App\Http\Controllers\Admin\Errors\ErrorsController@indexDisconect')->name('errors.500');
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/change-password', 'App\Http\Controllers\Auth\ChangePasswordController@index')->name('change_password.index')->withoutMiddleware('password_changed');
    Route::post('/change-password', 'App\Http\Controllers\Auth\ChangePasswordController@store')->name('change_password.store')->withoutMiddleware('password_changed');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
    Route::get('/admin/product', Index::class)->name('admin.product');
    Route::get('/cart', Cart::class)->name('shop.cart');
    Route::get('/checkout', Checkout::class)->name('shop.checkout');
    Route::get('/news/{categorySlug?}', 'App\Http\Controllers\Admin\News\NewsController@index')->name('news.index');
    Route::get('/news/{categorySlug?}/{slug?}', 'App\Http\Controllers\Admin\News\NewsController@getDetail')->name('news.detail');
    Route::get('lang/{lang}', 'App\Http\Controllers\LangController@changeLang')->name('lang');
    Route::patch('update-menu', [App\Http\Controllers\HomeController::class, 'updateMenu'])->name('update-menu');

    Route::group([
        'prefix' => 'admin',
    ], function () {
        Route::get('/user/import', 'App\Http\Controllers\Admin\User\UserInfController@getImport');
        Route::post('/user/import', 'App\Http\Controllers\Admin\User\UserInfController@import');


        Route::group(['prefix' => 'news'], function () {
            //status news
            Route::get('posted', 'App\Http\Controllers\Admin\News\NewsController@getListPosted')
                ->name('admin.news.posts.index');
            Route::get('draft', 'App\Http\Controllers\Admin\News\NewsController@getListDraft')
                ->name('admin.news.posts.draft');
            Route::get('need-approval', 'App\Http\Controllers\Admin\News\NewsController@getListNeedApproval')
                ->name('admin.news.posts.need_approval');
            Route::get('approved', 'App\Http\Controllers\Admin\News\NewsController@getListApproved')
                ->name('admin.news.posts.approved');


            // news actions
            Route::get('create', 'App\Http\Controllers\Admin\News\NewsController@create')->name('admin.news.posts.create');
            Route::get('edit/{id}', 'App\Http\Controllers\Admin\News\NewsController@edit')->name('admin.news.posts.edit');
            Route::group(['prefix' => 'category'], function () {
                Route::get('index', 'App\Http\Controllers\Admin\News\CategoryController@index')->name('admin.news.category.index');
            });
        });
        Route::get('/user', 'App\Http\Controllers\Admin\User\UserListController@index')->name('admin.user.index');
        Route::get('/user/create', 'App\Http\Controllers\Admin\User\UserInfController@create')->name('admin.user.create');
        Route::post('/user/store', 'App\Http\Controllers\Admin\User\UserInfController@store')->name('admin.user.store');
        Route::get('/user/edit/{id}', 'App\Http\Controllers\Admin\User\UserInfController@edit')->name('admin.user.edit');
        Route::patch('/user/update/{id}', 'App\Http\Controllers\Admin\User\UserInfController@update')->name('admin.user.update');
        Route::get('/user/show/{id}', 'App\Http\Controllers\Admin\User\UserInfController@show')->name('admin.user.show');

        Route::group([
            'prefix' => 'research',
        ], function () {

            //quản lí khảo sát
            Route::group(['prefix' => '/survey'], function() {
                Route::get('/', 'App\Http\Controllers\Admin\Research\Survey\SurveyController@index')->name('admin.research.survey.index');
                Route::get('/create', 'App\Http\Controllers\Admin\Research\Survey\SurveyController@create')->name('admin.research.survey.create');
                Route::get('/edit/{id}', 'App\Http\Controllers\Admin\Research\Survey\SurveyController@edit')->name('admin.research.survey.edit');
            });

            Route::get('/ideal', 'App\Http\Controllers\Admin\Research\Ideal\IdealController@index')->name('admin.research.ideal.index');
            Route::get('/ideal/create', 'App\Http\Controllers\Admin\Research\Ideal\IdealController@create')->name('admin.research.ideal.create');
            Route::get('/ideal/edit/{id}', 'App\Http\Controllers\Admin\Research\Ideal\IdealController@edit')->name('admin.research.ideal.edit');
            Route::get('/ideal/show/{id}', 'App\Http\Controllers\Admin\Research\Ideal\IdealController@show')->name('admin.research.ideal.detail');

            Route::get('/category-field', 'App\Http\Controllers\Admin\Research\Category\CategoryFieldController@index')->name('admin.research.category-field.index');
            Route::get('/category-topic', 'App\Http\Controllers\Admin\Research\Category\CategoryTopicController@index')->name('admin.research.category-topic.index');


            //Ke hoach nghien cuu
            Route::get('/research_plan', 'App\Http\Controllers\Admin\Research\Plan\ResearchPlanController@index')->name('admin.research.plan.research_plan.index');
            Route::get('/research_plan/edit/{id}', 'App\Http\Controllers\Admin\Research\Plan\ResearchPlanController@edit')->name('admin.research.plan.research_plan.edit');

            // Quản lý đề tài
            Route::get('/topic', 'App\Http\Controllers\Admin\Research\Topic\TopicController@index')->name('admin.research.topic.index');
            Route::get('/topic/create', 'App\Http\Controllers\Admin\Research\Topic\TopicController@create')->name('admin.research.topic.create');
            Route::get('/topic/edit/{id}', 'App\Http\Controllers\Admin\Research\Topic\TopicController@edit')->name('admin.research.topic.edit');
            Route::get('/topic/detail/{id}', 'App\Http\Controllers\Admin\Research\Topic\TopicController@detail')->name('admin.research.topic.detail');
            Route::post('/topic/update', 'App\Http\Controllers\Admin\Research\Topic\TopicController@update')->name('admin.research.topic.update');

            // Quan ly chi phi nghien cuu
            Route::get('/topic-fee', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@index')->name('admin.research.topic-fee.index');
            Route::get('/topic-fee/edit/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@edit')->name('admin.research.topic-fee.edit');
            Route::get('/topic-fee/labor/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@laborParticipation')->name('admin.research.topic-fee.labor');
            Route::get('/topic-fee/labor/detail/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@laborParticipationDetail')->name('admin.research.topic-fee.labor.detail');
            Route::get('/topic-fee/material/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@materialCost')->name('admin.research.topic-fee.material');
            Route::get('/topic-fee/equipment/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@equipmentCost')->name('admin.research.topic-fee.equipment');
            Route::get('/topic-fee/normal/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@normalCost')->name('admin.research.topic-fee.normal');
            Route::get('/topic-fee/asset/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@assetCost')->name('admin.research.topic-fee.asset');
            Route::get('/topic-fee/other/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@otherCost')->name('admin.research.topic-fee.other');
            Route::get('/topic-fee/expert/{id}', 'App\Http\Controllers\Admin\Research\Cost\TopicFeeController@expertCost')->name('admin.research.topic-fee.expert');

            // Quản lý chuyên gia
            Route::get('/expert', 'App\Http\Controllers\Admin\Research\Category\ExpertController@index')->name('admin.research.expert.index');

            Route::get('/statistic','App\Http\Controllers\Admin\Research\Statistic\StatisticController@index' )->name('admin.research.statistic.index');


            Route::group(['prefix' => 'achievement'], function() {
                Route::group(['prefix' => 'science-article'], function() {
                    Route::get('/','App\Http\Controllers\Admin\Research\Achievement\ScienceArticleController@index')
                    ->name('admin.research.achievement.science-article.index');
                    Route::get('/create', 'App\Http\Controllers\Admin\Research\Achievement\ScienceArticleController@create')
                    ->name('admin.research.achievement.science-article.create');
                    Route::get('/edit/{id}', 'App\Http\Controllers\Admin\Research\Achievement\ScienceArticleController@edit')
                    ->name('admin.research.achievement.science-article.edit');
                });
                Route::group(['prefix' => 'intellectual-property'], function() {
                    Route::get('/','App\Http\Controllers\Admin\Research\Achievement\IntellectualPropertyController@index')
                    ->name('admin.research.achievement.intellectual-property.index');
                    Route::get('/create', 'App\Http\Controllers\Admin\Research\Achievement\IntellectualPropertyController@create')
                    ->name('admin.research.achievement.intellectual-property.create');
                    Route::get('/edit/{id}', 'App\Http\Controllers\Admin\Research\Achievement\IntellectualPropertyController@edit')
                    ->name('admin.research.achievement.intellectual-property.edit');
                });

                Route::group(['prefix' => 'technology-transfer'], function() {
                    Route::get('/', 'App\Http\Controllers\Admin\Research\Achievement\TechnologyTransferController@index')
                    ->name('admin.research.achievement.technology-transfer.index');
                    Route::get('/create', 'App\Http\Controllers\Admin\Research\Achievement\TechnologyTransferController@create')
                    ->name('admin.research.achievement.technology-transfer.create');
                    Route::get('/edit/{id}', 'App\Http\Controllers\Admin\Research\Achievement\TechnologyTransferController@edit')
                    ->name('admin.research.achievement.technology-transfer.edit');
                });
            });
        });
        //Quán lý dự án nghiên cứu
        Route::group([
            'prefix' => 'research-project',
            'as' => 'admin.research-project.'
        ], function () {
            Route::group([
                'prefix' => 'information',
                'as' => 'information.'
            ], function () {
                Route::get('/draft', ['as' => 'draft.index', 'uses' => 'App\Http\Controllers\Admin\ResearchProject\ProjectInformationController@getDraftList']);
                Route::get('/', ['as' => 'index', 'uses' => 'App\Http\Controllers\Admin\ResearchProject\ProjectInformationController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => 'App\Http\Controllers\Admin\ResearchProject\ProjectInformationController@create']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'App\Http\Controllers\Admin\ResearchProject\ProjectInformationController@edit']);
            });
        });
        Route::group([
            'prefix' => 'research-project'
        ], function () {
            Route::prefix('task-work')->group(function () {
                Route::get('/{contract_code?}', 'App\Http\Controllers\Admin\ResearchProject\TaskController@index')->name('admin.research-project.task-work.index');
                Route::get('/create/{contract_code?}', 'App\Http\Controllers\Admin\ResearchProject\TaskController@create')->name('admin.research-project.task-work.create');
                Route::get('/edit/{id}','App\Http\Controllers\Admin\ResearchProject\TaskController@edit')->name('admin.research-project.task-work.edit');
            });
        });
        Route::get('productivity','App\Http\Controllers\Admin\ResearchProject\ProductivityController@index')->name('admin.research-project.productivity.index');
        Route::get('/department', 'App\Http\Controllers\Admin\User\DepartmentController@index')->name('admin.department.index');
        Route::get('/department/create', 'App\Http\Controllers\Admin\User\DepartmentController@create')->name('admin.department.create');
        Route::group([
            'prefix' => 'general'
        ], function () {
            Route::group([
                'prefix' => 'work-plan',
            ], function () {

                Route::get('/', 'App\Http\Controllers\Admin\General\WorkPlan\WorkPlanController@index')->name('admin.general.work-plan.index');

                Route::get('/create', 'App\Http\Controllers\Admin\General\WorkPlan\WorkPlanController@create')->name('admin.general.work-plan.create');
                Route::post('/store', 'App\Http\Controllers\Admin\General\WorkPlan\WorkPlanController@store')->name('admin.general.work-plan.store');

                Route::get('/edit/{id}', 'App\Http\Controllers\Admin\General\WorkPlan\WorkPlanController@edit')->name('admin.general.work-plan.edit');
                Route::get('/show/{id}', 'App\Http\Controllers\Admin\General\WorkPlan\WorkPlanController@show')->name('admin.general.work-plan.show');
                Route::post('/update/{id}', 'App\Http\Controllers\Admin\General\WorkPlan\WorkPlanController@update')->name('admin.general.work-plan.update');
            });
            Route::get('/businessfee', 'App\Http\Controllers\Admin\General\BusinessFeeController@index')->name('admin.general.businessfee.index');
            Route::get('/businessfee/create', 'App\Http\Controllers\Admin\General\BusinessFeeController@create')->name('admin.general.businessfee.create');
            Route::post('/businessfee/store', 'App\Http\Controllers\Admin\General\BusinessFeeController@store')->name('admin.general.businessfee.store');
            Route::get('/template-form', 'App\Http\Controllers\Admin\General\TemplateFormController@index')->name('admin.general.form.index');
        });

        Route::group([
            'prefix' => 'budget'
        ], function () {
            Route::get('/', 'App\Http\Controllers\Admin\Budget\BudgetController@index')->name('admin.budget.index');
            Route::get('/create/{topic_fee}', 'App\Http\Controllers\Admin\Budget\BudgetController@create')->name('admin.budget.create');
            Route::post('/store', 'App\Http\Controllers\Admin\Budget\BudgetController@store')->name('admin.budget.store');
            Route::get('/edit/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@edit')->name('admin.budget.edit');
            Route::post('/update/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@update')->name('admin.budget.update');
            Route::get('/list-budget-approved', 'App\Http\Controllers\Admin\Budget\BudgetController@listBudgetApproved')->name('admin.budget.list-budget-approved.index');
            Route::get('/show/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@show')->name('admin.budget.show');
            Route::get('/edit-budget/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@editBudget')->name('admin.budget.edit-budget');
            Route::post('/update-budget/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@updateBudget')->name('admin.budget.update-budget');

            Route::get('/report', 'App\Http\Controllers\Admin\Budget\BudgetReportController@index')->name('admin.budget.report.index');
            Route::get('/copy/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@copyBudget')->name('admin.budget.copy');
            Route::get('/topic-budget/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@topicBudget')->name('admin.budget.topic-budget');

            Route::get('/budget-history', 'App\Http\Controllers\Admin\Budget\BudgetController@historyBudget')->name('admin.budget.history-budget.index');
            Route::get('/budget-history-change/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@historyChangeBudget')->name('admin.budget.budget-history-change');

            Route::get('/copy/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@duplicate')->name('admin.budget.copy');
            Route::get('/change-budget/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@changeBudget')->name('admin.budget.change-budget');
            Route::post('/update-change/{id}', 'App\Http\Controllers\Admin\Budget\BudgetController@updateChange')->name('admin.budget.update-change');
            Route::get('/template-form', 'App\Http\Controllers\Admin\Budget\TemplateFormController@index')->name('admin.template.form.index');
        });


        Route::group(['prefix' => 'system'], function () {
            Route::group(['prefix' => 'role'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\System\Role\RoleController@index')->name('admin.system.role.index');
                Route::get('/create', 'App\Http\Controllers\Admin\System\Role\RoleController@create')->name('admin.system.role.create');
                Route::post('/store', 'App\Http\Controllers\Admin\System\Role\RoleController@store')->name('admin.system.role.store');
                Route::get('/edit/{id}/{tab?}', 'App\Http\Controllers\Admin\System\Role\RoleController@edit')->name('admin.system.role.edit');
                Route::post('/update/{id}', 'App\Http\Controllers\Admin\System\Role\RoleController@update')->name('admin.system.role.update');
                Route::get('/listroleuser/{id}', 'App\Http\Controllers\Admin\System\Role\RoleController@listRoleUser')->name('admin.system.role.listroleuser');
                Route::post('/addroleuser/{id}', 'App\Http\Controllers\Admin\System\Role\RoleController@addRoleUser')->name('admin.system.role.addroleuser');
            });
            Route::get('/group-user/show/{id}','App\Http\Controllers\Admin\User\GroupUserController@show')->name('admin.system.group-user.detail.show');
            Route::get('/group-user/edit/{id}','App\Http\Controllers\Admin\User\GroupUserController@edit')->name('admin.system.group-user.edit');
            Route::get('/group-user/create','App\Http\Controllers\Admin\User\GroupUserController@create')->name('admin.system.group-user.create');
            Route::get('/group-user/index','App\Http\Controllers\Admin\User\GroupUserController@index')->name('admin.system.group-user.index');
            Route::get('/account', 'App\Http\Controllers\Admin\System\Account\AccountController@index')->name('admin.system.account.index');
            Route::get('/account/create', 'App\Http\Controllers\Admin\System\Account\AccountController@create')->name('admin.system.account.create');
            Route::get('/account/edit/{id}', 'App\Http\Controllers\Admin\System\Account\AccountController@edit')->name('admin.system.account.edit');

            Route::get('/leave-config', 'App\Http\Controllers\Admin\System\LeaveConfigController@index')->name('admin.system.leave-config.index');
            Route::post('/leave-config', 'App\Http\Controllers\Admin\System\LeaveConfigController@update')->name('admin.system.leave-config.update');

            Route::get('/general-config', 'App\Http\Controllers\Admin\System\GeneralConfigController@index')->name('admin.system.general-config.index');
            Route::post('/general-config', 'App\Http\Controllers\Admin\System\GeneralConfigController@update')->name('admin.system.general-config.update');
            Route::get('/audit', 'App\Http\Controllers\Admin\System\AuditController@index')->name('admin.system.audit.list');
        });


        Route::group(['prefix' => 'approval'], function () {
            Route::get('/', 'App\Http\Controllers\Admin\Approval\ApprovalController@index')->name('admin.approval.index');
            Route::get('/create', 'App\Http\Controllers\Admin\Approval\ApprovalController@create')->name('admin.approval.create');
            Route::get('/edit/{id}', 'App\Http\Controllers\Admin\Approval\ApprovalController@edit')->name('admin.approval.edit');
            Route::get('/detail/{id}', 'App\Http\Controllers\Admin\Approval\ApprovalController@detail')->name('admin.approval.detail');
        });
        Route::group(['prefix' => 'appraisalboard'], function () {
            Route::get('/', 'App\Http\Controllers\Admin\AppraisalBoard\AppraisalBoardController@index')->name('admin.appraisal.board.index');
            Route::get('/create/{topic?}', 'App\Http\Controllers\Admin\AppraisalBoard\AppraisalBoardController@create')->name('admin.appraisal.board.create');
            Route::post('/store', 'App\Http\Controllers\Admin\AppraisalBoard\AppraisalBoardController@store')->name('admin.appraisal.board.store');
            Route::get('/edit/{id}', 'App\Http\Controllers\Admin\AppraisalBoard\AppraisalBoardController@edit')->name('admin.appraisal.board.edit');
            Route::post('/update/{id}', 'App\Http\Controllers\Admin\AppraisalBoard\AppraisalBoardController@update')->name('admin.appraisal.board.update');
        });

        Route::group(['prefix' => 'profile',], function () {
            Route::get('/', 'App\Http\Controllers\Admin\Profile\ProfileController@index')->name('admin.profile.index');
            Route::post('update', 'App\Http\Controllers\Admin\Profile\ProfileController@update')->name('admin.profile.update');
        });

        Route::group([
            'prefix' => 'asset',
        ], function () {
            //report controller
            Route::get('/report','App\Http\Controllers\Admin\Asset\ReportController@index')->name('admin.asset.report.index');

            Route::get('/', 'App\Http\Controllers\Admin\Asset\AssetController@index')->name('admin.asset.index');
            Route::get('/create', 'App\Http\Controllers\Admin\Asset\AssetController@create')->name('admin.asset.create');
            Route::get('/dashboard', 'App\Http\Controllers\Admin\Asset\AssetController@dashboard')->name('admin.asset.dashboard.index');
            Route::get('/show/{id}', 'App\Http\Controllers\Admin\Asset\AssetController@show')->name('admin.asset.show');
            Route::get('/lost-cancel-liquidation', 'App\Http\Controllers\Admin\Asset\AssetController@lostCancel')->name('admin.asset.lost-cancel-liquidation.index');
            Route::get('/maintenance', 'App\Http\Controllers\Admin\Asset\AssetController@maintenance')->name('admin.asset.maintenance.index');

            Route::group(['prefix' => 'category'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Asset\AssetCategoryController@index')->name('admin.asset.category.index');
            });

            Route::group(['prefix' => 'instrument'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Asset\AssetInstrumentController@index')->name('admin.asset.instrument.index');
            });
            Route::group(['prefix' => 'inventory_period'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Asset\InventoryPeriodController@index')->name('admin.asset.inventory_period.index');
            });
            Route::group(['prefix' => 'inventory'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Asset\AssetInventoryController@index')->name('admin.asset.inventory.index');
                Route::get('/show/{id}', 'App\Http\Controllers\Admin\Asset\AssetInventoryController@show')->name('admin.asset.inventory.show');
            });
            Route::group(['prefix' => 'provider'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Asset\AssetProviderController@index')->name('admin.asset.provider.index');
                Route::get('/create', 'App\Http\Controllers\Admin\Asset\AssetProviderController@create')->name('admin.asset.provider.create');
                Route::get('/edit/{id}', 'App\Http\Controllers\Admin\Asset\AssetProviderController@edit')->name('admin.asset.provider.edit');
            });

            Route::group(['prefix' => 'allocated-revoke'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Asset\AssetAllocatedAndRevokeController@index')->name('admin.asset.allocated-revoke.index');
                Route::get('allocated/{data}', 'App\Http\Controllers\Admin\Asset\AssetAllocatedAndRevokeController@allocated')->name('admin.asset.allocated.create');
                Route::get('revoke/{data}', 'App\Http\Controllers\Admin\Asset\AssetAllocatedAndRevokeController@revoke')->name('admin.asset.revoke.create');
            });

            Route::group(['prefix' => 'transfer'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Asset\TransferController@create')->name('admin.asset.transfer.create');
                Route::get('list', 'App\Http\Controllers\Admin\Asset\TransferController@index')->name('admin.asset.transfer.index');
                Route::get('show/{id}', 'App\Http\Controllers\Admin\Asset\TransferController@show')->name('admin.asset.transfer.detail');
            });
        });

        Route::group(['prefix' => 'decision-reward'], function () {
            Route::get('/', 'App\Http\Controllers\Admin\DecisionReward\DecisionRewardController@index')->name('admin.decision-reward.index');
            Route::get('/create', 'App\Http\Controllers\Admin\DecisionReward\DecisionRewardController@create')->name('admin.decision-reward.create');
            Route::get('/edit/{id}', 'App\Http\Controllers\Admin\DecisionReward\DecisionRewardController@edit')->name('admin.decision-reward.edit');
        });


        Route::group([
            'prefix' => 'executive',
        ], function () {
            Route::get('/dashboard','App\Http\Controllers\Admin\Executive\Dashboard\DashboardController@index')->name('admin.executive.dashboard.index');
            //Hợp đồng lao động
            Route::get('labor-contract', 'App\Http\Controllers\Admin\Executive\ContractAndSalary\LaborContractController@index')->name('admin.executive.contract-and-salary.contract.index');
            Route::get('my-contract', 'App\Http\Controllers\Admin\Executive\ContractAndSalary\MyContractController@index')->name('admin.executive.contract-and-salary.my-contract.index');
            // Import chấm công
            Route::get('time-sheets', 'App\Http\Controllers\Admin\Executive\TimeSheets\TimeSheetsController@index')->name('admin.executive.time-sheets.index');
            Route::get('time-sheets/show/{id}', 'App\Http\Controllers\Admin\Executive\TimeSheets\TimeSheetsController@show')->name('admin.executive.time-sheets.show');
            Route::get('my-time-sheets', 'App\Http\Controllers\Admin\Executive\TimeSheets\TimeSheetsController@MyTimeSheet')->name('admin.executive.my-time-sheets.index');
            Route::get('my-salary', 'App\Http\Controllers\Admin\Executive\ContractAndSalary\SalaryController@index')->name('admin.executive.contract-and-salary.my-salary.index');
            Route::get('day-off', 'App\Http\Controllers\Admin\Executive\DayOff\DayOffController@MyDayOff')->name('admin.executive.my-day-off.index');
            Route::get('day-off/apply-for-leave/{id}/edit', 'App\Http\Controllers\Admin\Executive\DayOff\DayOffController@ApplyForLeaveEdit')->name('admin.executive.my-day-off.apply-for-leave-edit');
            Route::get('day-off/apply-for-leave', 'App\Http\Controllers\Admin\Executive\DayOff\DayOffController@ApplyForLeave')->name('admin.executive.my-day-off.apply-for-leave');


            Route::group(['prefix' => 'user-info-leave'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Executive\UserInfoLeave\UserInfoLeaveController@index')->name('admin.executive.user-info-leave.index');
                Route::get('/create', 'App\Http\Controllers\Admin\Executive\UserInfoLeave\UserInfoLeaveController@create')->name('admin.executive.user-info-leave.create');
                Route::get('/edit/{id}', 'App\Http\Controllers\Admin\Executive\UserInfoLeave\UserInfoLeaveController@edit')->name('admin.executive.user-info-leave.edit');
            });



            // Import lương
            Route::get('salary', 'App\Http\Controllers\Admin\Executive\Salary\SalaryController@index')->name('admin.executive.salary.index');
            Route::get('salary/show/{id}', 'App\Http\Controllers\Admin\Executive\Salary\SalaryController@show')->name('admin.executive.salary.show');
            // Mua sắm
            Route::group(['prefix' => 'shopping'], function () {
                Route::get('/', 'App\Http\Controllers\Admin\Executive\ShoppingController@index')->name('admin.executive.shopping.index');
                Route::get('/create', 'App\Http\Controllers\Admin\Executive\ShoppingController@create')->name('admin.executive.shopping.create');
                Route::post('/store', 'App\Http\Controllers\Admin\Executive\ShoppingController@store')->name('admin.executive.shopping.store');
                Route::get('/show/{id}', 'App\Http\Controllers\Admin\Executive\ShoppingController@show')->name('admin.executive.shopping.show');
                Route::patch('/update/{id}', 'App\Http\Controllers\Admin\Executive\ShoppingController@update')->name('admin.executive.shopping.update');
            });
        });
        Route::group([
            'prefix' => 'master',
        ], function () {
            Route::get('/', 'App\Http\Controllers\Admin\Config\MasterController@index')->name('admin.config.master');
            Route::get('delete/{id}', 'App\Http\Controllers\Admin\Config\MasterController@delete')->name('admin.config.master.delete');
            Route::get('create', 'App\Http\Controllers\Admin\Config\MasterController@create')->name('admin.config.master.create');
            Route::get('edit/{id}', 'App\Http\Controllers\Admin\Config\MasterController@edit')->name('admin.config.master.edit');
            Route::post('insert', 'App\Http\Controllers\Admin\Config\MasterController@insert')->name('admin.config.master.insert');
            Route::post('update', 'App\Http\Controllers\Admin\Config\MasterController@update')->name('admin.config.master.update');
        });

        Route::group(['prefix' => 'notification'], function () {
            Route::get('/', 'App\Http\Controllers\Admin\Config\NotificationController@index')->name('admin.config.notification.index');
            Route::get('/public-config', 'App\Http\Controllers\Admin\Config\NotificationController@publicConfig')->name('admin.config.notification.public');
            Route::get('/private-config', 'App\Http\Controllers\Admin\Config\NotificationController@privateConfig')->name('admin.config.notification.private');
        });

        // Chi phí nghiên cứu thực
        Route::get('/reality_expense','App\Http\Controllers\Admin\RealityExpense\RealityExpenseController@index')->name('admin.reality_expense.index');
        Route::get('/reality_expense/create','App\Http\Controllers\Admin\RealityExpense\RealityExpenseController@create')->name('admin.reality_expense.create');
        Route::get('/reality_expense/edit/{id}','App\Http\Controllers\Admin\RealityExpense\RealityExpenseController@edit')->name('admin.reality_expense.edit');
        Route::get('/reality_expense/show/{id}','App\Http\Controllers\Admin\RealityExpense\RealityExpenseController@show')->name('admin.reality_expense.show');

        // Finance & Accounting
        Route::get('/finance','App\Http\Controllers\Admin\Finance\FinanceController@index')->name('admin.finance.index');
    });
    // API login
    Route::group([
        'prefix' => 'login-gw',
    ], function () {
        Route::get('/mail', 'App\Http\Controllers\Api\LoginGWController@loginMail')->name('api.login.mail.index');
        Route::get('/approval', 'App\Http\Controllers\Api\LoginGWController@loginApproval')->name('api.login.approval.index');
        Route::get('/whisper', 'App\Http\Controllers\Api\LoginGWController@loginWhisper')->name('api.login.whisper.index');

        Route::get('/handleRequest', 'App\Http\Controllers\Api\LoginGWController@handleRequest')->name('api.login.approval.handleRequest');
    });

     Route::group([
         'prefix' => 'gw-approval',
     ], function () {
         Route::get('/download/{docId}/{fileNo}', 'App\Http\Controllers\Api\DownloadGwController@download')->name('api.gw-approval.download');
     });
});

