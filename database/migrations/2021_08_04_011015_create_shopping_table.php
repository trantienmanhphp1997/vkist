<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping', function (Blueprint $table) {
            $table->id();
            $table->string('name', 500)->comment('Tên yêu cầu mua sắm');
            $table->string('note',1000)->nullable()->comment('ghi chu khac');
            $table->string('approved_note',1000)->nullable()->comment('Noi dung phe duyet');

            $table->bigInteger('estimate_money')->nullable()->comment('So tien du kien');
            $table->date('needed_date')->nullable()->comment('Ngay su dung');
            $table->tinyInteger('status')->nullable()->comment('Trạng thái de xuat');
            $table->foreignId('budget_id')->nullable()->constrained('budget')->comment("nguồn ngân sách");

            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->foreignId('admin_id')->nullable()->constrained('users')->comment("nguoi tao");
            $table->foreignId('proposer_id')->nullable()->constrained('users')->comment('Nguoi de xuat');
            $table->foreignId('executor_id')->nullable()->constrained('users')->comment('Nguoi thuc hien');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping');
    }
}
