<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Enums\ESystemConfigType;
use Illuminate\Support\Facades\Hash;
use App\Models\SystemConfig;

class ChangePasswordController extends Controller
{
    public function __construct()
	{
        
    }
    public function index(){
        return view('auth.passwords.change');
    }
    public function store(Request $request) {
        $config = SystemConfig::query()->where('name','Import salary config')->where('type', ESystemConfigType::TYPE_IMPORT_SALARY)->first();
        $min_length_password = $config->content['min_length_of_password']['value'] ?? 8;
        $password_special_requirement = $config->content['password_special_requirement']['value'] ?? false;

        $user = auth()->user();

        $rules = [
            'old_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!Hash::check($value, $user->password)) {
                    return $fail(__('client_validation.form.profile.password.old'));
                }
            }],
            'new_password' => $password_special_requirement ? ['required', 'min:'.$min_length_password, 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{3,}$/'] : ['required', 'min:'. $min_length_password],
            'confirm_password' => ['required', 'same:new_password']
        ];

        $this->validate($request,$rules,[
            'new_password.regex' => __('client_validation.form.profile.password.regex'),
            'confirm_password.same' => __('client_validation.form.profile.password.same'),
        ], [
            'old_password' => __('common.old_password'),
            'new_password' => __('common.new_password'),
            'confirm_password' => __('common.confirm_password'),
        ]);

        $user->password = Hash::make($request->new_password);
        $user->change_password_at = now();
        $user->save();
        session()->flash('success', __("notification.profile.success_change_pass"));
        return redirect('/');
    }
}
