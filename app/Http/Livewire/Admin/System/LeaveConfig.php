<?php

namespace App\Http\Livewire\Admin\System;

use App\Enums\ESystemConfigType;
use App\Models\SystemConfig;
use Illuminate\Validation\Rule;
use Livewire\Component;

class LeaveConfig extends Component
{

    public SystemConfig $config;
    public array $content;

    public $leaveOnProbationary = [];
    public $firstMonthLeave = [];
    public $allowAdvanceLeave = [];
    public $increaseLeaveBySeniority = [];
    public $unusedLeaveToNewYear = [];

    protected function rules()
    {
        return [
            'firstMonthLeave.before_date' => Rule::requiredIf(function () {
                return $this->firstMonthLeave['value'] == 'before_date';
            }),
            'allowAdvanceLeave.limited' => Rule::requiredIf(function () {
                return $this->allowAdvanceLeave['check'] && $this->allowAdvanceLeave['value'] == 'limited';
            }),
            'increaseLeaveBySeniority.customize.passed_years' => Rule::requiredIf(function () {
                return $this->increaseLeaveBySeniority['check'] && $this->increaseLeaveBySeniority['value'] == 'customize';
            }),
            'increaseLeaveBySeniority.customize.addition_days' => Rule::requiredIf(function () {
                return $this->increaseLeaveBySeniority['check'] && $this->increaseLeaveBySeniority['value'] == 'customize';
            }),
            'unusedLeaveToNewYear.limit' => Rule::requiredIf(function () {
                return $this->unusedLeaveToNewYear['check'] && $this->unusedLeaveToNewYear['value'] == 'limit';
            }),
            'unusedLeaveToNewYear.expired_in.value' => [
                'numeric',
                'min:1',
                'max:12',
                Rule::requiredIf(function () {
                    return $this->unusedLeaveToNewYear['check'] && $this->unusedLeaveToNewYear['expired_in']['check'];
                })
            ]
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'firstMonthLeave.before_date' => __('common.time.day'),
            'allowAdvanceLeave.limited' => __('common.time.day_count'),
            'increaseLeaveBySeniority.customize.passed_years' => __('common.time.year_count'),
            'increaseLeaveBySeniority.customize.addition_days' => __('common.time.day_count'),
            'unusedLeaveToNewYear.limit' => __('common.time.day_count'),
            'unusedLeaveToNewYear.expired_in.value' => __('common.time.month_count')
        ];
    }

    public function mount()
    {
        $this->config = SystemConfig::query()
            ->firstOrCreate([
                'name' => 'Staff leave config',
                'model_name' => UserInfoLeave::class,
                'type' => ESystemConfigType::USER_INFO_LEAVE,
                'admin_id' => null
            ], [
                'content' => [
                    "leave_on_probationary" => [
                        "check" => true
                    ],
                    "first_month_leave" => [
                        "value" => "any_date_in_month",
                        "any_date" => null,
                        "before_date" => 1
                    ],
                    "allow_advance_leave" => [
                        "check" => false,
                        "value" => "unlimited",
                        "unlimited" => null,
                        "limited" => 10
                    ],
                    "increase_leave_by_seniority" => [
                        "check" => false,
                        "value" => "labor_law",
                        "calculate_from" => "probation",
                        "labor_law" => [
                            "passed_years" => 5,
                            "addition_days" => 1
                        ],
                        "customize" => [
                            "passed_years" => 5,
                            "addition_days" => 1
                        ],
                        "probation" => null,
                        "official" => null
                    ],
                    "unused_leave_to_new_year" => [
                        "check" => false,
                        "value" => "",
                        "all" => null,
                        "limit" => 10,
                        "expired_in" => [
                            "check" => false,
                            "value" => 1
                        ]
                    ]
                ]
            ]);

        $this->content = $this->config->content;

        $this->leaveOnProbationary = $this->content['leave_on_probationary'];
        $this->firstMonthLeave = $this->content['first_month_leave'];
        $this->allowAdvanceLeave = $this->content['allow_advance_leave'];
        $this->increaseLeaveBySeniority = $this->content['increase_leave_by_seniority'];
        $this->unusedLeaveToNewYear = $this->content['unused_leave_to_new_year'];
    }

    public function render()
    {
        return view('livewire.admin.system.leave-config');
    }


    public function save()
    {
        $this->validate();

        $this->content['leave_on_probationary'] = $this->leaveOnProbationary;
        $this->content['first_month_leave'] = $this->firstMonthLeave;
        $this->content['allow_advance_leave'] = $this->allowAdvanceLeave;
        $this->content['increase_leave_by_seniority'] = $this->increaseLeaveBySeniority;
        $this->content['unused_leave_to_new_year'] = $this->unusedLeaveToNewYear;

        $this->config->content = $this->content;
        $this->config->save();
        $this->dispatchBrowserEvent('show-toast', ['message' => __('data_field_name.leave_config.save_successfully'), 'type' => 'success']);
    }
}
