<?php

namespace App\Http\Livewire\Admin\Asset\AssetDetail;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Asset;
use Livewire\Component;

class TabDetail extends BaseLive
{
    public $assetDetail;
    public function render()
    {
        
        return view('livewire.admin.asset.asset-detail.tab-detail');
    }
}
