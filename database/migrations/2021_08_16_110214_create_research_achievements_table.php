<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_achievements', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Danh sach thanh qua nghien cuu');
            $table->string('name', 500)->nullable()->comment('Ten bai bao, Ten dang ký shtt, Noi dung chuyen giao cong nghe ');
            $table->foreignId('topic_id')->nullable()->comment('`De tai`')->constrained('topic');
            $table->tinyInteger('type')->comment('Loai thanh qua. 1 : Bai bao khoa hoc, 2: quyen so huu tri tue, 3: Chuyen giao cong nghe');

            $table->string('magazine_name', 255)->nullable()->comment('Ten tap chi, dung voi type = 1');
            $table->string('article_link', 255)->nullable()->comment('Link bai bao, dung voi type = 1');
            $table->tinyInteger('type_of_owner')->nullable()->comment('Dung voi type = 2. Loai hinh so huu . 1 : sang che, 2: kieu dang cong nghiep');

            $table->tinyInteger('type_of_object_transfer')->nullable()->comment('Dung voi type = 3. Doi tuong chuyen giao . 1 : cac doi tuong so huu cong nghiep, 2: cac bi quyet ki thuat,kien thuc ki thuat ve cong nghe, 3: cac giai phap hop li hoa san xuat, doi moi cong nghe');
            $table->string('receiver_department', 255)->nullable()->comment('Don vi tiep nhan, dung voi type = 3');

            $table->dateTime('publish_date')->nullable()->comment('Thoi gian dang bai bao, Thoi gian cong bo sang che, Thoi gian hoan thanh chuyen giao cong nghe');
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_achievements');
    }
}
