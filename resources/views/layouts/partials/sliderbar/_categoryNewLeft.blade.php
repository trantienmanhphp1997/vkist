<div class="col-md-2 col-xl-3 sidebar-left" style="max-width: 300px !important;">
    <style>
        .active-category{
            border-radius: 20px;
            color: #4E5D78;
        }
    </style>
    <ul class="nav flex-column memu-main-left">
        <li>
            <a class="nav-link @if(!$categorySlug) active-category @endif"  style="    font-size: 14px;
    color: #323B4B;"
               href="{{route('news.index')}}"> <img src="/images/clipboard-list.svg" alt="clipboard-list"
                                                                                       height="24">
                <span class="circle"></span>     {{__("news/newsManager.menu_name.all")}}
            </a>
        </li>
        @foreach($categorys as $category)
            <li>
                <a class="nav-link @if($categorySlug === $category->slug) active-category @endif" style="    font-size: 14px;
    color: #323B4B;"
                   href="{{route('news.index',['categorySlug'=> $category->slug])}}"> <img src="/images/clipboard-list.svg" alt="clipboard-list"
                                                                    height="24">
{{--                    format string duy nhat viet hoa chu cai dau tien--}}
                    <span class="circle"></span>{{ucfirst(strtolower($category->name))}}
                </a>
            </li>
        @endforeach
    </ul>
</div>
