<?php
return [
    'breadcrumbs' => [
        'activity-management' => 'Quản lý hoạt động',
    ],
    'list' => 'Danh sách Hợp đồng lao động',
    'filter' => [
        'select-box' => [
            'option' => [
                'status' => 'Trạng thái',
            ],
        ],
    ],
    'status' => [
        'active' => 'Đang làm việc',
        'off' => 'Dừng làm việc',
        'deleted' => 'Đã xóa',
    ],
    'table_column' => [
        'user_name' => 'Họ và tên',
        'code' => 'Số hợp đồng',
        'position' => 'Chức danh',
        'contract_type' => 'Loại hợp đồng',
        'duration' => 'Thời hạn hợp đồng',
        'start_date' => 'Ngày bắt đầu',
        'end_date' => 'Ngày kết thúc',
        'status' => 'Trạng thái',
        'action' => 'Hành Động',
        'coefficients_salary' => 'Hệ số lương',
        'bank_account' => 'Số TK ngân hàng',
        'bank_name' => 'Ngân hàng',
    ],
    'title' => [
        'create' => 'Tạo ý tưởng mới',
        'edit' => 'Chỉnh sửa',
        'detail' => 'Thông tin chi tiết ý tưởng'
    ],
];