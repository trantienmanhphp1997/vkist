<?php

namespace App\Http\Livewire\Admin\User\GroupUser;

use App\Http\Livewire\Base\BaseLive;
use App\Models\GroupUser;
use App\Models\GroupUserHasTopic;
use App\Models\Topic;
use Exception;
use Illuminate\Support\Str;

class FormData extends BaseLive
{
    public $selectedResearchCategory;
    public $selectedTopics = [];

    public $readyTopics = [];

    public $groupUser;

    public $allTopicsOfField = false;
    public $searchTerm;
    public $currentTab = 'group-user-tab';
    public $selectionKey;

    protected $listeners = [
        'changed-research-category-event' => 'selectResearchCategory',
        'changed-topic-event' => 'selectTopic'
    ];

    protected function rules()
    {
        return [
            'groupUser.code' => 'required|max:10',
            'groupUser.name' => ['required', 'regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/ ]+$/', 'max:100'],
            'groupUser.description' => ['required', 'regex:/^[a-zA-Z0-9' . config('common.special_character.VN') . '\.\,\-\(\)\/ ]+$/', 'max:1000'],
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'groupUser.name' => __('user/group_user.form-data.name'),
            'groupUser.code' => __('user/group_user.form-data.code'),
            'groupUser.description' => __('user/group_user.form-data.description'),
        ];
    }

    public function selectResearchCategory($value)
    {
        $this->selectedResearchCategory = $value;
        $this->allTopicsOfField = false;
    }

    public function selectTopic($value)
    {
        $this->selectedTopics = $value;
    }

    public function setCurrentTab($tab)
    {
        $this->currentTab = $tab;
    }

    public function mount($id = 0)
    {
        $this->selectionKey = Str::random(5);
        $this->groupUser = GroupUser::query()->findOrNew($id);
    }

    public function render()
    {
        $query = $this->groupUser->topics();

        if (strlen($this->searchTerm) > 0) {
            $query->where(function ($query) {
                return $query->where('unsign_text', 'like', '%' . strtolower(removeStringUtf8($this->searchTerm)) . '%')
                    ->orWhereRaw("LOWER(code) LIKE LOWER('%{$this->searchTerm}%')");
            });
        }

        $topics = $query->paginate($this->perPage);

        return view('livewire.admin.user.group-user.form-data', [
            'topics' => $topics
        ]);
    }

    public function save()
    {
        try {
            $message = $this->groupUser->exists ? __('user/group_user.msg.success-update') : __('user/group_user.msg.add-topic-success');
            $this->validate();

            $this->groupUser->save();
            $this->saveReadyTopics();

            session()->flash('success', $message);
            $this->redirectRoute('admin.system.group-user.index');
        } catch(Exception $exception) {
            $this->setCurrentTab('group-user-tab');
            throw $exception;
        }
    }

    public function saveSelectedTopics()
    {
        $this->readyTopics = $this->readyTopics + $this->selectedTopics;
        $this->resetSelection();
    }

    public function saveReadyTopics()
    {
        if (!$this->groupUser->exists) return;
        $this->groupUser->topics()->syncWithoutDetaching(array_keys($this->readyTopics));
    }

    public function deleteTopic($id)
    {
        GroupUserHasTopic::query()->where('topic_id', $id)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('user/group_user.msg.delete-topic-success')]);
    }

    public function deleteReadyTopic($id)
    {
        if (isset($this->readyTopics[$id])) {
            unset($this->readyTopics[$id]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('user/group_user.msg.delete-topic-success')]);
        }
    }

    public function updatedAllTopicsOfField()
    {
        if ($this->allTopicsOfField) {
            $this->selectedTopics = Topic::query()
                ->where('research_category_id', $this->selectedResearchCategory['id'])
                ->get(['id', 'name', 'code'])
                ->keyBy('id')
                ->all();
        } else {
            $this->selectedTopics = [];
        }
    }

    public function resetSelection()
    {
        // reset form lựa chọn
        $this->selectionKey = Str::random(5);
        $this->selectedResearchCategory = null;
        $this->selectedTopics = [];
    }

    public function back()
    {
        return $this->redirectRoute('admin.system.group-user.index');
    }
}
