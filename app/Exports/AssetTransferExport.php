<?php

namespace App\Exports;

use App\Models\UserInf;
use Illuminate\Support\Facades\Config;
use App\Models\AssetAllocatedRevoke;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Illuminate\Support\Carbon;
use App\Enums\EAssetStatus;
use App\Enums\EAssetTransferType;
use App\Models\Department;

class AssetTransferExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($searchTerm) {
        $this->searchTerm = $searchTerm;
    }

    public function collection()
    {
        $query = AssetAllocatedRevoke::query()
            ->where('status', EAssetStatus::TRANSFER);
        if (strlen($this->searchTerm)) {
            $query->where('unsign_text', 'like', '%'. strtolower(removeStringUtf8($this->searchTerm)) . '%');
        }
        $data = $query->orderBy('id','DESC')->get();
        $data = $data->map(function ($item) {
        	$perfomer = UserInf::query()->find($item->admin_id);

            $from = $to = '';
            if ($item->transfer_type == EAssetTransferType::BY_INDIVIDUAL) {
                $from = UserInf::query()->find($item->transfered_user_id)->fullname ?? '';
                $to = UserInf::query()->find($item->user_info_id)->fullname ?? '';
            } elseif ($item->transfer_type == EAssetTransferType::BY_DEPARTMENT) {
                $from = Department::query()->find($item->from_department_id)->name ?? '';
                $to = Department::query()->find($item->to_department_id)->name ?? '';
            }

            return [
                'number_report' => $item->number_report,
                'implementation_date' => Carbon::parse($item->implementation_date)->format(Config::get('common.formatDate')),
                'transfer_type' => EAssetTransferType::valueToName($item->transfer_type),
                'from' => $from,
                'to' => $to,
                'implementation_quantity' => $item->implementation_quantity,
                'reason' => $item->implementation_reason,
                'perfomer' => !empty($perfomer) ? $perfomer->fullname : null,
            ];
        });
        return $data;
    }
    public function headings():array
    {
        return
        [
            'Số biên bản',
            'Ngày điều chuyển',
            'Loại điều chuyển',
            'Điều chuyển từ',
            'Điều chuyển đến',
            'Số lượng',
            'Lý do điều chuyển',
            'Người thực hiện',
        ];
    }

    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event) {
            $default_font_style = [
                'font' => ['name' => 'Times New Roman', 'size' => 12, 'color'=> ['argb' => '#FFFFFF'],
                    'background' => [
                        'color'=> '#5B9BD5'
                    ]]
            ];
            $default_font_style_title = [
                'font' => ['name' => 'Times New Roman', 'size' => 20, 'bold' =>  true,'horizontal' => 'center', 'vertical'=>'center'],
            ];
            $active_sheet = $event->sheet->getDelegate();
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
            $active_sheet->getParent()->getDefaultStyle()->applyFromArray([

            ]);
            $arrayAlphabet = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G',
            ];
            foreach ($arrayAlphabet as $alphabet) {
                $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
            }
            $event->sheet->mergeCells('D1:G1');
            $event->sheet->setCellValue('D1','DANH SÁCH ĐIỀU CHUYỂN');
            $event->sheet->getStyle('D1:G1')->applyFromArray(
                $default_font_style_title
            );
            // title
            $cellRange = 'A3:U3';
            $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style);
            $active_sheet->getStyle($cellRange)->getFont()
                ->getColor()->setRGB('FFFFFF');

            $active_sheet->getStyle($cellRange)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('5B9BD5');
            $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                array('horizontal' => 'center', 'vertical'=>'center')
            );
            $active_sheet->getStyle($cellRange)->getFont()->setBold(true);

        },];

    }
    public function title(): string
    {
        return 'DANH SÁCH ĐIỀU CHUYỂN';
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
