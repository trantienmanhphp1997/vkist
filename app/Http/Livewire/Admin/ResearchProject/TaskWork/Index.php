<?php

namespace App\Http\Livewire\Admin\ResearchProject\TaskWork;

use App\Http\Livewire\Base\BaseLive;
use App\Models\ResearchProject;
use App\Models\TaskWork;
use App\Service\Community;
use Illuminate\Support\Facades\Auth;
use Cookie;
use App\Models\ResearchCategory;
class Index extends BaseLive
{
    public $level_id;
    public $status_id;
    public $isGetPersonalTask = false;
    public $isGetTeamTask = false;
    public $project_code_id;
    public $field;
    public $leader_id;

    public function mount($contractCode = '')
    {
        if ($contractCode){
            $this->project_code_id = $contractCode;
        }
        $this->checkDestroyPermission = checkRoutePermission('destroy');
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->checkEditPermission = checkRoutePermission('edit');
    }

    public function render()
    {
        $statusDataList = [
            TaskWork::STATUS_UNDO_TASK => __('research/taskwork.select-status.undo'),
            TaskWork::STATUS_ON_WORKING_TASK => __('research/taskwork.select-status.on-working'),
            TaskWork::STATUS_DONE_TASK => __('research/taskwork.select-status.done'),
        ];

        $levelDataList = [
            TaskWork::PRIORITY_LEVEL_LOW => __('research/taskwork.select-level.low'),
            TaskWork::PRIORITY_LEVEL_MEDIUM => __('research/taskwork.select-level.medium'),
            TaskWork::PRIORITY_LEVEL_HIGH => __('research/taskwork.select-level.high'),
            TaskWork::PRIORITY_LEVEL_EMERGENCY => __('research/taskwork.select-level.emergency'),
        ];

        $query = TaskWork::query()->with(['topic.projectResearch', 'reviewer', 'follower.memberInfo', 'assigner']);
        if (trim($this->searchTerm)){
            $query->whereRaw("unsign_text LIKE '%" . strtolower(removeStringUtf8(trim($this->searchTerm))) . "%'")
                ->orWhere('id','LIKE',"%$this->searchTerm%");
        }
        if ($this->status_id != null){
            $query->where(['status'=>$this->status_id]);
        }
        if ($this->level_id != null){
            $query->where(['priority_level'=>$this->level_id]);
        }
        if ($this->project_code_id != null){
            $query->whereHas('topic.projectResearch', function ($query)  {
                $query->where(['contract_code'=>$this->project_code_id]);
            });
        }
        if ($this->isGetPersonalTask){
            $this->isGetTeamTask = false;
            $query->where(['admin_id'=>Auth::id()]);
        }
        if ($this->field != null){
            $query->orWhereHas('topic', function ($q)  {
                $q->where(['research_category_id' => $this->field]);
            });
        }
        if ($this->leader_id != null){
            $query->orWhereHas('topic', function ($q)  {
                $q->where(['leader_id' => $this->leader_id]);
            });
        }
        if ($this->isGetTeamTask){
            $taskListByAdminId = TaskWork::query()->where(['admin_id'=>Auth::id()])->get();
            $topicByGroup = [];
            foreach ($taskListByAdminId as $task){
                if (array_search($task->topic_id, $topicByGroup) !== false) {
                } elseif (!array_search($task->topic_id, $topicByGroup)) {
                    array_push($topicByGroup, $task->topic_id);
                }
            }
            $query->whereIn('topic_id',$topicByGroup);
        }

        $data = $query->orderBy('id', 'DESC')->paginate($this->perPage);
        $researchCategory = ResearchCategory::all();
        $dataUniqueTopic = TaskWork::get()->unique('topic_id');
        $leaderList = [];
        foreach($dataUniqueTopic as $item) {
            if (!empty($item->topic->leader ?? null) && !in_array($item->topic->leader, $leaderList)) {
                array_push($leaderList, $item->topic->leader);
            }
        }
        return view('livewire.admin.research-project.task-work.index', compact('data', 'statusDataList', 'levelDataList', 'researchCategory', 'leaderList'));
    }

    public function getPersonalTask(){
        //active render()
    }

    public function getTeamTask(){
        $this->isGetPersonalTask = false;
        //active render()
    }
}
