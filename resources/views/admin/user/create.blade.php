@extends('layouts.master')
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')

    <div class="col-md-8 col-xl-11 box-news box-user">
        <div class="row">
            <div class="col-md-12 px-0">
                    <div  class="breadcrumbs"><a href="{{route('admin.user.index')}}">{{ __('menu_management.menu_name.user_info_list') }}</a> \ <span>{{  __('data_field_name.user.personal_information')  }}</span></div> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="title">{{  __('data_field_name.user.personal_information')  }}</h3>
                <div class="information">
                    <div class="group-tabs">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                    <span>
                                        {{ __('data_field_name.user.general_information') }}
                                    </span>
                                </a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <form method="POST" action="{{ route('admin.user.store') }}" id="create-staff" enctype="multipart/form-data" class="___class_+?10___">
                                    {{ method_field('POST') }}
                                    @csrf
                                    @include('admin.user._tabInfo')
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        let $form = document.getElementById('create-staff');
        $form.onchange = (event) => {
            let data = [];

            for (let element of $form.elements) {
                if (element instanceof HTMLInputElement && element.classList.contains('form-control') && element.value.trim() != '') data.push(element.value.trim());
            }

            if (data.length > 0) {
                window.addEventListener('beforeunload', handleOnLeave);
            } else {
                window.removeEventListener('beforeunload', handleOnLeave);
            }
        }

        $form.onsubmit = (event) => {
            window.removeEventListener('beforeunload', handleOnLeave);
        }

        function handleOnLeave(event) {
            event.returnValue = '';
        }
    </script>
@endsection
