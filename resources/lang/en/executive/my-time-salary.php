<?php
return [
    'index' => 'Index',
    'salary-infomation' => 'Salary infomation',
    'salary-sheets' => 'Salary slip for :month',
    'incomes' => 'INCOMES',
    'detail' => 'DETAILS',
    'basic-salary' => 'Basic salary',
    'position-salary' => 'Position salart',
    'allowance' => 'Allowance',
    'position_allowance' => 'Position allowance',
    'responsibility_allowance' => 'Responsibility allowance',
    'work_allowance' => 'work allowance',
    'incentive_allowance' => 'Incentive allowance',
    'lunch_allowance' => 'Lunch allowance',
    'other' => 'Other (pursuit)',
    'total' => 'Total',
    'total_amount_received' => 'Total amount received',
    'deductions' => 'Deductions',
    'compulsory_insurance' => 'Compulsory insurance (10.5%)',
    'personal_income_tax' => 'Personal income tax',
    'other2' => 'other (arrears)'
];