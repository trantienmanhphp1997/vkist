<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnToTopicDetailWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_detail_work', function (Blueprint $table) {
            $table->foreignId('topic_fee_detail_id')->nullable()->constrained('topic_fee_detail');
            $table->dropColumn('topic_labor_participation_id');
            $table->dropColumn('wage_coefficient');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_detail_work', function (Blueprint $table) {
            $table->dropColumn('topic_fee_detail_id');
            $table->string('wage_coefficient', 10)->comment('He so tien cong');
            $table->unsignedBigInteger('topic_labor_participation_id')->nullable()->comment('Map voi topic_labor_participation');
            $table->foreign('topic_labor_participation_id')->references('id')->on('topic_labor_participation');
        });
    }
}
