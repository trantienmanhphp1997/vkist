<?php
return [
    'template-time-sheet' => 'File mẫu import chấm công',
    'title' => 'Dữ liệu chấm công',
    'btn-add' => 'Thêm dữ liệu chấm công',
    'result' => 'Kết quả',
    'download_error_list_file' => 'Tải file báo lỗi',
    'table_column' => [
        'name' => 'Tên bảng chẩm công tổng hợp',
        'duration' => 'Thời gian',

        'title' => [
            'create' => 'Tạo ý tưởng mới',
            'edit' => 'Chỉnh sửa',
            'detail' => 'Thông tin chi tiết ý tưởng'
        ],
    ],
];