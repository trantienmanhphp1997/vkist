<?php

namespace App\Http\Controllers\Admin\General;

use App\Http\Controllers\Controller;
use App\Service\Community;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use DB;
use Validator;

class TemplateFormController extends Controller
{
    public function index()
    {
        return view('admin.general.form.index');
    }

}

