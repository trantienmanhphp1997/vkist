<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div  class="breadcrumbs"><a href="">{{__('menu_management.menu_name.infomation-management')}} </a> \ <span>{{__('executive/my-time-sheets.title')}}</span></div> 
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">{{__('executive/my-time-sheets.time-sheets', [
                    	'month' => $filter_month_year
                    ])}}</h3>
                </div>
                <div class="col-md-6">
                    <div class=" float-right">
                        <form action="" class="d-flex">
                            <div class="form-group field-fix w-160 h-44 m-0">
                                <select class="w-100" wire:model.lazy="filter_month_year">
                                	@foreach($timeSheetsFile as $item)
                                		<option value="{{$item->month_working . '/' . $item->year_working}}">{{$item->month_working < 10 ? 0 . $item->month_working . '/' . $item->year_working : $item->month_working . '/' . $item->year_working}}</option>
                                	@endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn bg-primary btn-fix mr-0 px-24 py-12 ml-12" wire:click="render">{{__('common.button.view')}}</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="information">
                <div class="inner-tab pt0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group float-right">
                                <button type="button" style="border-radius: 11px; border:none;" data-toggle="modal" data-target="#exampleModal">
                                    <img src="/images/filterdown.svg" alt="filterdown">
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Start staff information -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="w-100 body-4 text-2 text-uppercase mb-12">
                                {{__('executive/my-time-sheets.personal-info.title')}}
                            </div>
                            <div class="bg-3 w-100 py-12 px-16 mb-32 bd-rds-12 info-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered w-100 m-0">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="w-ratio-33 p-0 bd-r bd-b">
                                                            <div class="d-flex py-8 pr-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.personal-info.name')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($user) ? $user->fullname : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-b">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.personal-info.position')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($user->position) ? $user->position->v_value : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-l bd-b">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.personal-info.code')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($user) ? $user->code : null}}</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="w-ratio-33 p-0 bd-r bd-t">
                                                            <div class="d-flex py-8 pr-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.personal-info.base_salary_coefficient')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($contract) ? $contract->coefficients->v_value : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-t">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.personal-info.bank_account')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($contract) ? $contract->bank_account_number : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-l bd-t">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.personal-info.bank_name')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($contract) ? $contract->bankName->v_value : null}}</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End staff information -->

                    <!-- Start timekeeping information -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="w-100 body-4 text-2 text-uppercase mb-12">
                                {{__('executive/my-time-sheets.timekeeping-info.title')}}
                            </div>
                            <div class="bg-3 w-100 py-12 px-16 mb-32 bd-rds-12 info-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered w-100 m-0">
                                                <tbody>
                                                    <tr>
                                                        <td class="w-ratio-33 p-0 bd-r bd-b">
                                                            <div class="d-flex py-8 pr-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.period')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{$startOfMonth->format(Config::get('common.formatDate'))}}<span class="font-weight-normal"> {{__('common.to')}} </span>{{$endOfMonth->format(Config::get('common.formatDate'))}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-b">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.number-of-paid-leave-days')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? $monthly->leave_day + $monthly->holiday : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-l bd-b">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.number-of-days-of-unpaid-leave')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? $monthly->unpaid_day : null}}</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="w-ratio-33 p-0 bd-r bd-t">
                                                            <div class="d-flex py-8 pr-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.standard-work-by-month')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? $monthly->leave_day + $monthly->holiday + $monthly->total_work_day : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-t">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.actual-number-of-working-days')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? (int)$monthly->total_work_day : null}}</p>
                                                            </div>
                                                        </td>
                                                        <td class="w-ratio-33 p-0 bd-l bd-t">
                                                            <div class="d-flex py-8 px-16 ">
                                                                <p class="mb-0">{{__('executive/my-time-sheets.timekeeping-info.total-number-of-paid-working-days')}}</p>
                                                                <p class="mb-0 ml-auto fw-600">{{!empty($monthly) ? $monthly->leave_day + $monthly->holiday + $monthly->total_work_day : null}}</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End timekeeping information -->

                    <!-- Start Timekeeping table -->
                    <!-- Cham cong tuan -->
                    @foreach($listTimeSheet as $index => $week)
	                    <div class="row">
	                        <div class="col-md-12">
	                            <div class="d-flex">
	                                <p class="px-0 pt-0 pb-12 head-9 text-2">{{__('common.week', ['week' => $index + 1])}}</p>
	                            </div>
	                        </div>
	                    </div>
	                    <table class="table table-general text-2">   
	                        <thead>
	                            <tr class="border-radius">
	                                <th  scope="col" class="hl-tb border-radius-left">{{__('common.day')}}</th>
	                                @for($i = 0; $i < 7; $i++)
	                                	@if($i == 6)
	                                		<th  scope="col" class="border-radius-right text-center">{{__('common.sunday')}}</th>
	                                	@endif
	                                	@if ($i == 0)
	                                		<th  scope="col" class="border-radius-right text-center">{{__('common.monday')}}</th>
	                                	@endif
	                                	@if ($i == 1)
	                                		<th  scope="col" class="border-radius-right text-center">{{__('common.tuesday')}}</th>
	                                	@endif
	                                	@if ($i == 2)
	                                		<th  scope="col" class="border-radius-right text-center">{{__('common.wednesday')}}</th>
	                                	@endif
	                                	@if ($i == 3)
	                                		<th  scope="col" class="border-radius-right text-center">{{__('common.thursday')}}</th>
	                                	@endif
	                                	@if ($i == 4)
	                                		<th  scope="col" class="border-radius-right text-center">{{__('common.friday')}}</th>
	                                	@endif
	                                	@if ($i == 5)
	                                		<th  scope="col" class="border-radius-right text-center">{{__('common.saturday')}}</th>
	                                	@endif
	                               	@endfor
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <tr>
	                                <td class="hl-tb">{{__('executive/my-time-sheets.table_column.date')}}</td>
	                                @foreach($week['day'] as $ind => $day)
		                                <td class="text-center">{{!empty($day) ? $day->format(Config::get('common.day/month')) : $day}}</td>
		                            @endforeach
	                            </tr>
	                            <tr>
	                            	<td class="hl-tb">{{__('executive/my-time-sheets.table_column.timekeeping')}}</td>
	                            	@foreach($week['timekeeping'] as $ind => $item)
		                                <td class="text-center font-weight-bold">{{!empty($item) ? App\Enums\EWorkingStatus::valueToName($item->working_status) : null}}</td>
		                            @endforeach
	                            </tr>
	                            <tr>
	                                <td colspan="8" class="px-0 pt-12 pb-24"><div class="divider-tb"></div></td>
	                            </tr>
	                        </tbody>
	                    </table>
	                @endforeach
	                @include('livewire.common._modalExportFile')
                    <!-- End Timekeeping table -->

                    <!-- Start Note list -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="body-4 text-2 text-uppercase mb-12">
                                {{__('executive/my-time-sheets.symbol-of-timesheets.title')}}
                            </div>
                            <div class="bg-3 w-100 py-24 px-24 mb-32 bd-rds-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="d-flex flex-column note-box">
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">1</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.working-day')}}</p>
                                                <p class="my-0 ml-auto note-stt">K</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">2</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.annual-leave')}}</p>
                                                <p class="my-0 ml-auto note-stt">P</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">3</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.paid-leave')}}</p>
                                                <p class="my-0 ml-auto note-stt">X</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">4</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.holiday')}}</p>
                                                <p class="my-0 ml-auto note-stt">L</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">5</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.meetings')}}</p>
                                                <p class="my-0 ml-auto note-stt">H</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">6</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.paid-personal-leave')}}</p>
                                                <p class="my-0 ml-auto note-stt">R</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">7</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.unpaid-personal-leave')}}</p>
                                                <p class="my-0 ml-auto note-stt">Ro</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">8</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.sick-leave')}}</p>
                                                <p class="my-0 ml-auto note-stt">Co</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">9</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.maternity-leave')}}</p>
                                                <p class="my-0 ml-auto note-stt">D</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">10</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.leave-without-reason')}}</p>
                                                <p class="my-0 ml-auto note-stt">O</p>
                                            </div>
                                            <div class="d-flex flex-nowrap py-8">
                                                <p class="my-0 mr-8 note-num">11</p>
                                                <p class="my-0">{{__('executive/my-time-sheets.symbol-of-timesheets.labor-accident')}}</p>
                                                <p class="my-0 ml-auto note-stt">T</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Note list -->
                </div>
            </div>
        </div>
    </div>
</div>