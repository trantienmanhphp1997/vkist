<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;



class DecisionRewardReceiver extends BaseModel 
{
    use HasFactory;
    protected $table = "decision_reward_receiver";
    protected $guarded=['*'];

    public function info() {
        return $this->belongsTo(UserInf::class, 'user_info_id');
    }
}

