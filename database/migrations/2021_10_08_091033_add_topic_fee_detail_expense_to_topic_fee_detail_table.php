<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTopicFeeDetailExpenseToTopicFeeDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_fee_detail', function (Blueprint $table) {
            $table->json('topic_fee_detail_expense')->nullable()->comment('Mảng chứa kinh phí và khoán chi ');
            $table->bigInteger('total_prescribed_expense')->nullable()->comment('Tong khoan chi theo quy dinh');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_fee_detail', function (Blueprint $table) {
            $table->dropColumn('topic_fee_detail_expense');
            $table->dropColumn('total_prescribed_expense');
        });
    }
}
