<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_inventory', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('kiểm kê theo nhóm tài sản');
            $table->string('name', 500)->nullable()->comment('tên biên bản kiểm kê');
            $table->foreignId('department_id')->nullable()->constrained('department')->comment(' phòng thực hiện map vs department');
            $table->date('inventory_date')->nullable()->comment('ngày bắt thực hiện');
            $table->tinyInteger('status')->nullable()->comment('1:đã hoàn thành kiểm kê, 2: chưa thực hiện');
            $table->foreignId('asset_category_id')->nullable()->constrained('asset_category')->comment(' nhóm tài sản map vs asset_category');
            $table->timestamps();
            $table->string('unsign_text', 1000)->nullable()->comment('luu tim kiem khong dau');
            $table->index(['unsign_text']);
            $table->softDeletes();
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_inventory');
    }
}
