<?php

namespace App\Exports;

use App\Enums\EApproval;
use App\Enums\ETopicSource;
use App\Enums\ETopicStatus;
use App\Models\Approval;
use App\Models\Topic;
use App\Service\Community;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class ApprovalResearchPlanExport implements FromCollection, WithHeadings, WithEvents, WithTitle, ShouldAutoSize, WithCustomStartCell, WithMapping
{

    private $mergeRows = [];

    public function __construct($searchTerm, $searchStatus)
    {
        $this->order = 1;
        $this->searchTerm = $searchTerm;
        $this->searchStatus = $searchStatus;
    }

    public function collection()
    {
        $query = Approval::query()
            ->with('admin', 'appraisal.chairman.userInfo', 'researchPlan.topic', 'researchPlan.mainTasks.userInfo')
            ->where('type', EApproval::TYPE_PLAN);
        $status = $this->searchStatus;

        if ($this->searchTerm != null) {
            $term = strtolower(removeStringUtf8($this->searchTerm));
            $query->where('unsign_text', 'like', '%' . $term . '%')
                ->where(function ($q) use ($term) {
                    return $q->where('unsign_text', 'like', '%' . $term . '%')
                        ->with('admin')
                        ->orWhereHas('admin.info', function ($query) use ($term) {
                            return $query->where('unsign_text', 'like', '%' . $term . '%');
                        });
                });
        }

        if ($status != null) {
            $query->where('status', $status);
        }

        //anh ta add
        return $query->get();
    }
    public function headings(): array
    {
        return [
            __('data_field_name.common_field.stt'),
            __('data_field_name.approval_topic.profile_code'),
            __('data_field_name.approval_topic.name_profile'),
            __('data_field_name.approval_topic.text_sender'),
            __('data_field_name.approval_topic.appraisal_name'),
            __('data_field_name.approval_topic.chairman'),
            __('data_field_name.common_field.status'),

            __('data_field_name.research_plan.research_plan_name'),
            __('data_field_name.topic.code'),
            __('data_field_name.topic.name'),
            __('data_field_name.research_plan.start_time'),
            __('data_field_name.research_plan.end_time'),

            __('data_field_name.research_plan.task_name'),
            __('data_field_name.research_plan.perform_human'),
            __('data_field_name.research_plan.perform_time')
        ];
    }
    public function map($approval): array
    {
        if (!isset($approval->researchPlan)) return [];

        $tmp = [
            $this->order++,
            $approval->code,
            $approval->name,
            $approval->admin->name ?? '',
            $approval->appraisal->name ?? '',
            $approval->appraisal->chairman->userInfo->fullname ?? '',
            EApproval::valueToName($approval->status),

            $approval->researchPlan->name ?? '',
            $approval->researchPlan->topic->code ?? '',
            $approval->researchPlan->topic->name ?? '',
            reformatDate($approval->researchPlan->topic->start_date ?? ''),
            reformatDate($approval->researchPlan->topic->end_date ?? ''),
        ];

        $mainTaskCount = $approval->researchPlan->mainTasks->count();
        $this->mergeRows[] = $mainTaskCount;

        if ($mainTaskCount > 0) {
            $row = [];
            foreach ($approval->researchPlan->mainTasks as $key => $task) {
                $row[] = [
                    ...$tmp,
                    $task->name ?? '',
                    $task->userInfo->fullname ?? '',
                    reformatDate($task->start_date) . ' - ' . reformatDate($task->end_date)
                ];
            }

            return $row;
        }

        return $tmp;
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {

            // tạo tiêu đề
            $titleCells = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
            $event->sheet->mergeCells('A1:G1');
            $event->sheet->setCellValue('A1', mb_strtoupper(__('data_field_name.approval_topic.list_review'), 'UTF-8'));
            $event->sheet->getStyle('A1:O3')->applyFromArray([
                'font' => ['bold' => true],
                'alignment' => array(
                    'horizontal' => \PHPOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                )
            ]);
            foreach ($titleCells as $cell) {
                $value = $event->sheet->getCell("{$cell}3")->getValue();
                $event->sheet->mergeCells("{$cell}2:{$cell}3");
                $event->sheet->setCellValue("{$cell}2", $value);
            }

            // tạo phần thông tin kế hoạch
            $event->sheet->mergeCells('H2:O2');
            $event->sheet->setCellValue('H2', mb_strtoupper(__('data_field_name.approval_topic.type_research_plan'), 'UTF-8'));

            // gộp ô
            $mainCells = range('A', 'L');
            $index = 4;
            foreach ($this->mergeRows as $value) {
                if($value == 0) {
                    $index += 1;
                    continue;
                }
                
                foreach ($mainCells as $cell) {
                    $event->sheet->mergeCells("{$cell}{$index}:{$cell}" . ($index + $value - 1));
                }
                $index += $value;
            }

        }];
    }
    public function title(): string
    {
        return __('data_field_name.approval_topic.list_review');
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
