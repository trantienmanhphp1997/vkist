<?php

namespace App\Http\Livewire\Admin\Research\Plan;

use App\Http\Livewire\Admin\General\WorkPlan\UserInfo;
use App\Service\Community;
use Livewire\Component;
use App\Models\ResearchPlan;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use App\Models\Topic;
use App\Models\UserInf;
use App\Enums\EResearchPlan;
use App\Enums\ETopicStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\Listener;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ResearchPlanList extends BaseLive
{
    public $searchTerm;
    public $searchStatus;
    public $selectedTopicId;
    public $name;
    public $start_date;
    public $end_date;
    public $fullname;
    public $researchPlanSource;
    public ?Topic $topic;
    public ?ResearchPlan $researchplan;
    public $researchPlan;
    public $researchSoure;
    public $from_date;
    public $to_date;
    public $researchPlanId;

    protected $rules = [
        'name' => 'max:50|required|regex:/^[A-Za-z0-9]+$/u',
        'selectedTopicId' => 'required',
    ];
    protected $listeners = [
        'set-from_date' => 'setFromDate',
        'set-to_date' => 'setToDate',
    ];
    public function mount()
    {
        $this->checkCreatePermission = checkRoutePermission('create');
        $this->researchSoure = [
            EResearchPlan::STATUS_NOT_SUBMIT => __('data_field_name.research_plan.not_submit'),
            EResearchPlan::STATUS_SUBMIT => __('data_field_name.research_plan.submit'),
            EResearchPlan::STATUS_COMPLETE_PROFILE => __('data_field_name.topic.status_completed_file'),
            EResearchPlan::STATUS_INVALID_PROFILE => __('data_field_name.topic.status_invalid_file'),
            EResearchPlan::STATUS_WAIT_ASSESSMENT => __('data_field_name.research_plan.waiting_expertise'),
            EResearchPlan::STATUS_WAIT_APPROVAL => __('data_field_name.research_plan.wait_approve'),
            EResearchPlan::STATUS_REJECT => __('data_field_name.topic.status_rejected'),
            EResearchPlan::STATUS_APPROVAL => __('data_field_name.topic.status_approved'),
        ];
    }
    public function setFromDate($data)
    {
        $this->from_date = $data['from_date'] ? date('Y-m-d', strtotime($data['from_date'])) : null;
    }
    public function setToDate($data)
    {
        $this->to_date = $data['to_date'] ? date('Y-m-d', strtotime($data['to_date'])) : null;
    }

    public function render()
    {
        $query = ResearchPlan::query()->with('topic');
        $topicList = Topic::all()->where('status', ETopicStatus::STATUS_SUBMIT);
        if (strlen($this->searchTerm) > 0) {
            $query->whereRaw("LOWER(name) LIKE LOWER('%{$this->searchTerm}%')")->orWhereHas('topic', function (Builder $query) {
                $query->whereRaw("LOWER(name) LIKE LOWER('%{$this->searchTerm}%')")->orWhereHas('leader', function (Builder $query) {
                    $query->whereRaw("LOWER(fullname) LIKE LOWER('%{$this->searchTerm}%')");
                });
            });
        }
        if(!is_null($this->from_date)) {
            $query->whereHas('topic',function(Builder $query){
               return $query->whereDate('start_date', '=', $this->from_date);
            });

        }
        if(!is_null($this->to_date)) {
            $query->orWhereHas('topic',function(Builder $query){
            return $query->whereDate('end_date', '=', $this->to_date);
            });
        }
        if(!is_null($this->from_date) && !is_null($this->to_date)){
            $query->orwhereHas('topic',function(Builder $query){
                 $query->whereDate('start_date', '>=', $this->from_date);
                 $query->WhereDate('end_date', '<=', $this->to_date);
             });
        }
        if (strlen($this->searchStatus) > 0) {
            if($this->searchStatus == EResearchPlan::STATUS_NOT_SUBMIT) {
                $query->where('status', '=', $this->searchStatus)->orWhereNull('status');
            } else {
                $query->where('status', '=', $this->searchStatus);

            }
        }
        // $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 25;
        //check permission topic
        if (!empty(Community::listTopicIdAllowed())) {
            $query->whereIn('topic_id', Community::listTopicIdAllowed());
        }
        $data = $query->orderBy('id', 'desc')->paginate($this->perPage);

        return view('livewire.admin.research.plan.research-plan-list', compact('data', 'topicList'));
    }

    public function store()
    {
        $research_plan = new ResearchPlan;
        $this->validate([
            'name' => ['required',Rule::unique('research_plan', 'name')->ignore($research_plan->id)->whereNull('deleted_at'), 'regex:/[^.][^<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:50'],
            'selectedTopicId' => ['required',
            Rule::unique('research_plan', 'topic_id')->ignore($research_plan->id)->whereNull('deleted_at')]
        ],[],[

            'name'=>__('data_field_name.research_plan.research_plan_name'),
            'selectedTopicId'=>__('data_field_name.research_plan.topic'),
        ]);
        $research_plan->name = $this->name;
        $research_plan->topic_id = $this->topic->id;
        $research_plan->status = EResearchPlan::STATUS_NOT_SUBMIT;
        $research_plan->admin_id  =Auth::user()->id;
        $research_plan->save();
        $this->emit('close-modal-create-research-plan');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')]);
    }

    public function delete()
    {
        $researchPlan = ResearchPlan::findorFail($this->deleteId);
        $researchPlan->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
    }
    public function resetInputFields()
    {
        $this->name = '';
        $this->selectedTopicId = '';
        $this->resetValidation();
    }

    public function updatedSelectedTopicId()
    {
        $this->topic = Topic::findOrFail($this->selectedTopicId);
    }
    public function edit($id)
    {
        $this->resetInputFields();
        $researchPlan = ResearchPlan::findorFail($id);
        $this->researchPlanId = $id;
        $this->name = $researchPlan->name;
        $this->selectedTopicId = $researchPlan->topic_id;
        $this->updatedSelectedTopicId();
    }
    public function update(){
       
        if($this->researchPlanId){
            $researchPlan = ResearchPlan::findOrFail($this->researchPlanId);
            $this->validate([
                'name' => ['required',Rule::unique('research_plan', 'name')->ignore($researchPlan->id)->whereNull('deleted_at'), 'regex:/[^.][^<>!@#$%^&*(){}\[\]\'\"\/\|]+$/', 'max:50'],
            ]);
            $researchPlan->name = $this->name;
            $researchPlan->save();
            $this->emit('close-modal-edit-research-plan');
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')]);
        }



    }

}
