<?php

namespace Database\Seeders;

use App\Models\MasterData;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TopicMasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topic_level = 'Topic level';
        $role_in_topic = 'Role in topic';
        // tạo cấp đề tài & vai trò trong dự án
        $data = [
            // cấp đề tài
            [
                'type' => 11,
                'v_key' => $topic_level,
                'v_value' => 'Cấp bộ',
                'order_number' => 1
            ],
            [
                'type' => 11,
                'v_key' => $topic_level,
                'v_value' => 'Cấp cơ sở',
                'order_number' => 0
            ],
            [
                'type' => 11,
                'v_key' => $topic_level,
                'v_value' => 'Cấp trung ương',
                'order_number' => 2
            ],

            // vai trò trong dự án
            [
                'type' => 13,
                'v_key' => $role_in_topic,
                'v_value' => 'Chủ nhiệm đề tài',
                'order_number' => 0
            ],
            [
                'type' => 13,
                'v_key' => $role_in_topic,
                'v_value' => 'Nghiên cứu viên',
                'order_number' => 1
            ],
            [
                'type' => 13,
                'v_key' => $role_in_topic,
                'v_value' => 'Thành viên',
                'order_number' => 2
            ]
        ];
        foreach($data as $item) {
            MasterData::query()->updateOrCreate($item);
        }

        // tạo mã đề tài theo chuyên ngành khoa học
        for($i = 0; $i < 5; $i++) {
            $masterData = MasterData::create([
                'type' => 12,
                'v_key' => 'Scientific name',
                'v_value' => "Chuyên ngành khoa học $i",
            ]);

            for($j = 0; $j < random_int(3, 5); $j++) {
                MasterData::create([
                    'type' => 12,
                    'v_key' => 'Scientific name',
                    'v_value' => "Chuyên ngành khoa học $i.$j",
                    'parent_id' => $masterData->id
                ]);
            }
        }
    }
}
