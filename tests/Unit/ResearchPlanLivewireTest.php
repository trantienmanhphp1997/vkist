<?php

namespace Tests\Unit;

use App\Models\ResearchPlan;
use Tests\TestCase;
use Livewire\Livewire;
use App\Models\User;
use App\Models\Topic;

class ResearchPlanLivewireTest extends TestCase
{

    /**
     * A basic unit test example.
     *
     * @return void
     */
    /** @test */
    public function testIndexResearchPlan()
    {
        $user = $this->user;
        $this->actingAs($user)->get(route('admin.research.plan.research_plan.index'))->assertOk();
    }
    public function testResearchPlanCreateSuccess(){
        $user = User::first();
        if(empty($user)){
            $user = User::factory()->create();

        }
        Livewire::actingAs($user)->test('admin.research.plan.research-plan-list',[
            'name'=>'ABC',
            'topic_id'=>'2'
        ])
        ->call('store')->assertHasNoErrors(['name','topic_id']);
       
    }
    public function testResearchPlanCreateFail(){
        $user = User::first();
        if(empty($user)){
            $user = User::factory()->create();

        }
        Livewire::actingAs($user)->test('admin.research.plan.research-plan-list',[
            'name'=>'',
            'topic_id'=>'2'
        ])
        ->call('store')->assertHasErrors(['name']);
       
    }
}
