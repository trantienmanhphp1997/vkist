<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a href="#">{{ __('news/newsManager.menu_name.news') }} </a> \
                <span>{{ __('news/newsManager.menu_name.form-data.create') }}</span>
            </div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12 form-component">
            <div class="information">
                <div class="pt0">
                    @if ($news->exists)
                        <h3 class="title">{{ __('news/newsManager.menu_name.form-data.update') }}</h3>
                    @else
                        <h3 class="title">{{ __('news/newsManager.menu_name.form-data.create') }}</h3>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group position: relative;">
                                <label>
                                    {{ __('news/newsManager.menu_name.form-data.name') }}
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" class="form-control" wire:model.defer="news.name" placeholder="{{ __('news/newsManager.menu_name.form-data.name') }}" oninput="countCharacter(this)" style="padding-right: 75px;" maxlength="255">
                                <small class="character-count p-2 bg-white" style="position: absolute; top: 40px; right: 30px;">{{ strlen($news->name) }}/255</small>
                                @error('news.name')
                                    <div class="text-danger mt-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>{{ __('news/newsManager.menu_name.form-data.category') }}<span class="text-danger"> *</span></label>
                                {{ Form::select('category', ['' => __('news/newsManager.menu_name.form-data.chose')] + $categories, null, ['class' => 'form-control form-control-lg', 'wire:model.defer' => 'news.category_news_id']) }}
                                @error('news.category_news_id') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{ __('news/newsManager.menu_name.form-data.trending') }}</label>
                                <div class="checkbox-config" style="margin-top: 9px;">
                                    <input type="checkbox" id="checkbox" wire:model.defer="news.is_trending" value="1">
                                    <label for="checkbox"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group position-relative">
                                <label>{{ __('news/newsManager.menu_name.form-data.content') }}</label>
                                <textarea class="form-control" rows="6" wire:model.defer="news.content" placeholder="{{ __('news/newsManager.menu_name.form-data.content') }}" oninput="countCharacter(this)" maxlength="10000"></textarea>
                                <small class="character-count p-2 bg-white" style="position: absolute; bottom: 1px; right: 15px;">{{ strlen($news->content) }}/10000</small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div wire:ignore>
                                <label>{{ __('news/newsManager.menu_name.form-data.image') }} {{ __('news/newsManager.menu_name.imageSize') }} <span class="text-danger"> *</span></label>
                                <input type="file" wire:model.lazy="image" class="image-preview" id="avatar-preview" data-image-error="{{ __('news/newsManager.menu_name.msg.upload-avatar.mimes') }}" data-image="{{ file_exists('storage/' . $news->image_path) ? asset('storage/' . $news->image_path) : '' }}">
                            </div>
                            @error('image')
                                <div class="text-danger mt-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="description" wire:model.defer="news.description" name="content" value="{{ $news->description }}">
                            <div class="form-group" wire:ignore>
                                <trix-editor class="form-control" input="description" x-ref="trix" style="height: 600px !important;overflow: auto;" placeholder="{{ __('news/newsManager.menu_name.form-data.description') }}"></trix-editor>
                            </div>
                            @error('news.description')
                                <div class="text-danger mt-1">{{ $message }}</div>
                            @enderror

                            @error('editorFile')
                                <div class="text-danger mt-1">{{ $message }}</div>
                            @enderror

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @livewire('component.files', ['model_name' => get_class($news), 'folder' => $news->getTable(), 'model_id' => $news->id])
                        </div>
                    </div>
                    <div class="group-btn2 row">
                        <div class="col-sm-6">
                            <a type="button" href="{{ route('admin.news.posts.index') }}" class="btn btn-save bg-danger mr-3">{{ __('news/newsManager.menu_name.back') }}</a>
                            <button onclick="submit('draft')" class="btn btn-cancel mr-3">{{ __('news/newsManager.menu_name.button_save_draft') }}</button>
                            @if (checkPermission('admin.news.posts.approve'))
                                <button onclick="submit('publish')" type="button" class="btn btn-save">{{ __('news/newsManager.menu_name.button_publish') }} </button>
                            @elseif(checkPermission('admin.news.posts.wait_approve'))
                                <button onclick="submit('wait_approval')" type="button" class="btn btn-save">{{ __('news/newsManager.menu_name.button_wait_approval') }} </button>
                            @endif
                        </div>

                        <div class="col-sm-6 text-right">
                            @if (checkPermission('admin.news.posts.approve') && $news->exists && $news->status != \App\Models\News::APPROVED_NEWS)
                                <button class="btn btn-save mr-3" wire:click="approve">{{ __('news/newsManager.menu_name.approved') }}</button>
                            @endif

                            <button type="button" class="btn par6" title="delete" data-toggle="modal" data-target="#deleteModal">
                                <img src="/images/trash.svg" alt="trash" height="30px">
                            </button>
                        </div>
                    </div>
                    <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content ">
                                <div class="modal-body box-user">
                                    <h4 class="modal-title">{{ __('common.confirm_message.confirm_title') }}</h4>
                                    {{ __('common.confirm_message.are_you_sure_delete') }}
                                </div>
                                <div class="group-btn2 text-center pt-24">
                                    <button type="button" class="btn btn-cancel" data-dismiss="modal">{{ __('common.button.cancel') }}</button>
                                    <button type="button" wire:click="delete" class="btn btn-save" data-dismiss="modal">{{ __('common.button.delete') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    function uploadTrixImage(attachment) {
        @this.upload(
            'editorFile',
            attachment.file,
            function(uploadedUrl) {
                const eventName = 'myapp:trix-upload-complete:${btoa(uploadedUrl)}';
                const listener = function(event) {
                    attachment.setAttributes(event.detail);
                    console.log(event.detail);
                    window.removeEventListener(eventName, listener)
                }
                window.addEventListener(eventName, listener)

                @this.call('saveEditorFile', uploadedUrl, eventName);
            },
            function() {

            },
            function(event) {
                attachment.setUploadProgress(event.detail.progress);
            }
        )
    }

    addEventListener("trix-attachment-add", function(event) {
        if(checkFileExtension(
            event.attachment.file,
            ['jpeg', 'jpg', 'png', 'svg', 'gif'],
            "{{ __('news/newsManager.menu_name.msg.upload-avatar.mimes') }}"
        )) {
            uploadTrixImage(event.attachment);
        }

        changeData();
    });

    $("document").ready(function() {
        $('.image-preview').imagePreview();
    })

    function changeData() {
        // var description = document.getElementById("trix-description");
        // @this.description = description.value;
        document.getElementById('description').dispatchEvent(new Event('input'));
    }

    function countCharacter(input) {
        let value = $(input).val();
        let maxlength = $(input).attr('maxlength') || 255;
        $(input).parents('.form-group').find('.character-count').html(value.length + '/' + maxlength);
    }

    function submit(action) {
        changeData();
        setTimeout(() => {
            @this.call('save', action);
        }, 500);
    }
</script>
