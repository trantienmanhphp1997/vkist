@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.work_plan.create.title')}}</title>
@endsection

@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection

@section('content')
    <div class="col-md-10 col-xl-11 box-detail box-user">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs"><a href="">{{__('data_field_name.work_plan.create.general')}} </a> \ <span>{{__('data_field_name.work_plan.create.planWork')}}</span></div>
            </div>
        </div>
        <div class="row bd-border">
            <form action="{{route('admin.general.work-plan.store')}}" class="" method="post">
                {{ method_field('POST') }}
                @csrf
                <div class="col-md-12">
                    <div class="detail-task information box-idea">
                        <h4>{{__('data_field_name.work_plan.create.title')}}</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.content')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="" name="content" value="{{old('content')}}">
                                    @error('content') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.mission_id')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="" name="missions" value="{{old('missions')}}">
                                    @error('missions') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.MISSIONS_CODE')}}</label>
                                    <input type="text" class="form-control mission-code" placeholder="" name="missions_code" value="{{old('missions_code')}}">
                                    @error('missions_code') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.start_time')}}<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control" placeholder="" name="start_date" value="{{old('start_date')}}">
                                    @error('start_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.end_time')}}<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control" placeholder="" name="end_date" value="{{old('end_date')}}">
                                    @error('end_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{__('data_field_name.work_plan.create.fee_type')}}</label>
                                    <select class="form-control form-control-lg" name="budget_id">
                                        @foreach($source_cost as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{__('data_field_name.work_plan.create.fee')}}</label>
                                            <input type="text" class="form-control format_number" placeholder="" name="estimated_cost">
                                            @error('estimated_cost') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{__('data_field_name.work_plan.index.priority1')}}</label>
                                            {{ Form::select('source', ['' =>__('data_field_name.work_plan.index.priority_select')] + $priority_list, null, ['class' => 'form-control','name'=>'priority_level']) }}
                                            @error('priority_level') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @livewire('admin.general.work-plan.list-member')
                            <div class="col-md-12">
                                @livewire('component.files', ['model_name'=>$model_name, 'type'=>$type, 'folder'=>$folder,'status'=>$status,'deleteUnknownFilesOnMount' => false])
                            </div>
                            <div class="col-md-12 border-top">
                                <div class="group-btn2 text-center">
                                    <button type="" class="btn btn-cancel" name="cancel" wire:click="deleteAllFile">{{__('common.button.cancel')}}</button>
                                    <button type="submit" class="btn btn-save" name="save">{{__('common.button.save')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

