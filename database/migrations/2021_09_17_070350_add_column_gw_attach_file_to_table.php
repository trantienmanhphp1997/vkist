<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnGwAttachFileToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_fee', function (Blueprint $table) {
            $table->string('gw_attach_file', 2000)->nullable()->comment('Link file dinh kem groupware topic_fee');
        });
        Schema::table('research_plan', function (Blueprint $table) {
            $table->string('gw_attach_file', 2000)->nullable()->comment('Link file dinh kem groupware research_plan');
        });
        Schema::table('work_plan', function (Blueprint $table) {
            $table->string('gw_attach_file', 2000)->nullable()->comment('Link file dinh kem groupware work_plan');
        });
        Schema::table('budget', function (Blueprint $table) {
            $table->string('gw_attach_file', 2000)->nullable()->comment('Link file dinh kem groupware budget');
        });
        Schema::table('request_leave', function (Blueprint $table) {
            $table->string('gw_attach_file', 2000)->nullable()->comment('Link file dinh kem groupware request_leave');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_fee', function (Blueprint $table) {
            $table->dropColumn('gw_attach_file');
        });
        Schema::table('research_plan', function (Blueprint $table) {
            $table->dropColumn('gw_attach_file');
        });
        Schema::table('work_plan', function (Blueprint $table) {
            $table->dropColumn('gw_attach_file');
        });
        Schema::table('budget', function (Blueprint $table) {
            $table->dropColumn('gw_attach_file');
        });
        Schema::table('request_leave', function (Blueprint $table) {
            $table->dropColumn('gw_attach_file');
        });
    }
}
