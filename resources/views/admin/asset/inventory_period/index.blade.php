@extends('layouts.master')
@section('title')
    <title>{{__('data_field_name.asset_inventory.title')}}</title>
@endsection
@section('homeLeft')
    @include('layouts.partials.sliderbar._activityLeft')
@endsection
@section('content')
    @livewire('admin.asset.inventory-period.list-period')
@endsection
