<?php

namespace App\Http\Livewire\Admin\Research\Topic;

use App\Models\Topic;
use Livewire\Component;

class TabExpectedResult extends Component
{

    public Topic $topic;

    protected $listeners = [
        'validate-data-event' => 'validateData',
        'changed-root-topic-event' => 'setRootTopic',
    ];

    public function setRootTopic($topicId)
    {
        $rootTopic = Topic::query()->find($topicId);
        $this->topic->achievement_note = $rootTopic->achievement_note ?? '';
        $this->topic->achievement_paper_check = $rootTopic->achievement_paper_check ?? '';
        $this->topic->achievement_review_check = $rootTopic->achievement_review_check ?? '';
        $this->topic->achievement_announcement_check = $rootTopic->achievement_announcement_check ?? '';
        $this->topic->achievement_conference_check = $rootTopic->achiachievement_conference_checkevement_note ?? '';
        $this->topic->achievement_work_check = $rootTopic->achievement_work_check ?? '';
        $this->topic->achievement_textbook_check = $rootTopic->achievement_textbook_check ?? '';
    }

    protected function rules() {
        return [
            'topic.achievement_note' => 'required',
            'topic.achievement_paper_check' => 'nullable',
            'topic.achievement_review_check' => 'nullable',
            'topic.achievement_announcement_check' => 'nullable',
            'topic.achievement_conference_check' => 'nullable',
            'topic.achievement_work_check' => 'nullable',
            'topic.achievement_textbook_check' => 'nullable',
        ];
    }

    protected function getValidationAttributes()
    {
        return [
            'topic.achievement_note' => __('data_field_name.topic.achievement_note'),
            'topic.achievement_paper_check' => __('data_field_name.topic.achievement_paper_check'),
            'topic.achievement_review_check' => __('data_field_name.topic.achievement_review_check'),
            'topic.achievement_announcement_check' => __('data_field_name.topic.achievement_announcement_check'),
            'topic.achievement_conference_check' => __('data_field_name.topic.achievement_conference_check'),
            'topic.achievement_work_check' => __('data_field_name.topic.achievement_work_check'),
            'topic.achievement_textbook_check' => __('data_field_name.topic.achievement_textbook_check'),
        ];
    }

    public function mount(Topic $topic) {
        $this->topic = $topic;
    }

    public function render()
    {
        return view('livewire.admin.research.topic.tab-expected-result');
    }

    public function validateData() {
        try {
            $this->validate();
            $this->emitUp('validated-data-event', 'tab-expected-result', $this->topic->getDirty());
        } catch (\Exception $exception) {
            $this->emitUp('invalid-data-event', 'tab-expected-result', __('data_field_name.topic.error_tab_expected_result'));
            throw $exception;
        }
    }
}
