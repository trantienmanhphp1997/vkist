<div class=" tab-pane" id="insurance" wire:ignore.self>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.warranty_info')}}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.warranty_time')}}</label>

                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{$warranty_date['warranty_date'] ?? ''}}"
                                   disabled>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control"
                                   value="{{$warranty_date?$assetTypeDate[$warranty_date['warranty_type_date']]:''}}" disabled>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.warranty_expiry_date')}}</label>
                    <input type="text" class="form-control"
                           value="{{reFormatDate($assetDetail->expiry_warranty_date, 'd/m/Y')??''}}"
                           disabled>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.warranty_condition')}}</label>
                    <textarea class="form-control" rows="4" disabled>
                        {{$assetDetail->condition_warranty ?? ''}}
                    </textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.maintenance_info')}}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.maintenance_date')}}</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" class="form-control"
                                   value="{{$maintenance_date['maintenance_date'] ?? ''}}"
                                   disabled>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control"
                                   value="{{$maintenance_date?$assetTypeDate[$maintenance_date['maintenance_type_date']]: ''}}"
                                   disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.maintenance_start_date')}}</label>
                    <input type="text" class="form-control"
                           value="{{reFormatDate($assetDetail->maintenance_start_date, 'd/m/Y')??''}}"
                           disabled>
                </div>
            </div>
        </div>
    </div>
    <div class="margin_bottom">
        <h3 class="title-asset">{{__('data_field_name.asset.implement')}}</h3>
        <div class="row">
            <div class="col-sm-12 form-group">
                <input type="checkbox" name="achievement_paper_check" id="a-1"
                       @if($assetDetail->allocate&&$assetDetail->allocate->status == 1) checked @endif
                       disabled>
                <label for="a-1">{{__('data_field_name.asset.implement')}}</label>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.implement_date')}}</label>
                    <input type="text" class="form-control"
                           value="{{$assetDetail->allocate?reFormatDate($assetDetail->allocate->implementation_date, 'd/m/Y'): ''}}"
                           disabled>
                </div>

                <div class="form-group">
                    <label>{{__('data_field_name.asset.users')}}</label>
                    <input type="text" class="form-control" value="{{$assetDetail->allocate->user->fullname ?? ''}}"
                           disabled>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.asset_quantity')}}</label>
                    <input type="text" class="form-control" value="{{$assetDetail->amount_used ?? ''}}"
                           disabled>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__('data_field_name.asset.department_id')}}</label>
                    <input type="text" class="form-control"
                           value="{{$assetDetail->allocate->user->department->name ?? ''}}" disabled>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.position_id')}}</label>
                    <input type="text" class="form-control"
                           value="{{$assetDetail->allocate->user->position->v_value ?? ''}}" disabled>
                </div>
                <div class="form-group">
                    <label>{{__('data_field_name.asset.number_report')}}</label>
                    <input type="text" class="form-control" value="{{$assetDetail->allocate->number_report ?? ''}}"
                           disabled>
                </div>
            </div>
        </div>
    </div>
</div>
