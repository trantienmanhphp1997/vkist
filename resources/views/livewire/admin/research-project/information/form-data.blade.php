<div class="col-md-10 col-xl-11 box-detail box-user">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs"><a
                    href="{{ route('admin.research-project.information.index') }}">{{__('research/project.name')}}</a> \
                <span>{{__('research/project.information.breadcrumbs')}}</span></div>
        </div>
    </div>
    <div class="row bd-border">
        <div class="col-md-12">
            <div class="detail-task information box-idea">
                <a href="{{route('admin.research-project.information.index')}}" class="float-right">{{__('research/project.information.table-title')}}</a>
                <h4>
{{--                    @if ($user->exists)--}}
{{--                        {{ __('data_field_name.system.account.edit_user') }}--}}
{{--                    @else--}}
{{--                        {{ __('data_field_name.system.account.create_user') }}--}}
{{--                    @endif--}}
                    @if($isCreate)
                        {{__('research/project.information.tab-project.title-create')}}
                    @else
                        {{__('research/project.information.tab-project.title-update')}}
                    @endif

                </h4>
                <div wire:loading class="loader" style="z-index: 999;"></div>
                <div class="group-tabs">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link {{$currentTab == 'research' ? 'active' : ''}}" wire:click="setCurrentTab('research')" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="false">
                                <span>   {{__('research/project.information.tab-project.tab-name')}}</span>
                            </a>
                            <a class="nav-item nav-link {{$currentTab == 'member' ? 'active' : ''}}" wire:click="setCurrentTab('member')" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                                <span>  {{__('research/project.information.tab-project-member.tab-name')}}</span>
                            </a>

                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade {{$currentTab == 'research' ? 'active show' : ''}}" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <livewire:admin.research-project.information.tabs.project-tab :id="$researchID"/>
                        </div>
                        <div class="tab-pane fade {{$currentTab == 'member' ? 'active show' : ''}}" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <livewire:admin.research-project.information.tabs.member-tab :id="$researchID"/>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="group-btn2 text-center group-btn">
                        <button type="button" onclick="window.history.back()"
                                class="btn btn-cancel">{{ __('common.button.cancel') }}</button>
                        @if($isCreate)
                            <button type="button" wire:click="store" wire:loading.attr="disabled"
                                    class="btn btn-save  @if($disableBtn) disabled @endif">{{ __('common.button.save') }}</button>
                        @else
                            <button type="button" wire:click="update" wire:loading.attr="disabled"
                                    class="btn btn-save">{{ __('common.button.save') }}</button>
                        @endif

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

