<?php

namespace App\Http\Livewire\Admin\Executive\MyDayOff;

use App\Enums\ERequestLeave;
use Livewire\Component;
use App\Models\TimeSheetsMonthly;
use App\Models\TimeSheetsDaily;
use App\Models\TimeSheetsFile;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\UserInf;
use App\Models\Contract;
use App\Enums\EWorkingStatus;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use App\Models\RequestLeave;
use Excel;
use App\Models\UserInfoLeave;
use App\Http\Livewire\Base\BaseLive;
class Index extends BaseLive
{
    public $fromdDate;
    public $fromdDate2;
    public $toDate;
    public $currentYear;
    public $daysUsedCurrentYear=0;
    public $daysUsedLastYear=0;
    public $daysExpiryCurrentYear =0;
    public $daysExpiryLastYear =0;
    public $dateMoveCurrentYear;
    public $dateMoveLastYear;
    const LAST_DAY_OF_YEAR = '31/12/';
    protected $listeners = [
        'set-fromdDate' => 'setFromDate',
        'set-toDate' => 'setToDate',
    ];
    public function mount() {
        $this->fromdDate2 = now()->startOfMonth();
        $this->fromdDate = now()->startOfMonth()->format(Config::get('common.formatDate'));
        $this->toDate = now();
 
        $config = \App\Models\SystemConfig::where('type',2)->get()->first();

        $config = $config->content;
        $config = $config['unused_leave_to_new_year'];
        $this->currentYear = now()->format('Y');

        if($config['check']){
            if($config['expired_in']['check']){
                $this->dateMoveCurrentYear = self::LAST_DAY_OF_YEAR.$this->currentYear;
                $date = '01/'.$config['expired_in']['value'].'/'.($this->currentYear);
                $this->dateMoveLastYear = Carbon::parse(reFormatDate($date))->format('t/m/Y');
                $date =  Carbon::parse(reFormatDate($date))->format('Y/m/t');
                $date1 = Carbon::parse(now()); //ngày hiện tại
                $date2 =  Carbon::parse($date);
                if($date1->greaterThan($date2)){
                    $this->daysExpiryLastYear = 0;
                }
                else {
                    if($config['value']=='limit'){
                        $limit = (int) $config['limit'];
                        $this->daysExpiryLastYear = (isset(auth()->user()->info->leave_day_last_year)&&auth()->user()->info->leave_day_last_year)?(float) auth()->user()->info->leave_day_last_year:0;
                        $this->daysExpiryLastYear = ($this->daysExpiryLastYear>$limit)?$limit:$this->daysExpiryLastYear;
                    }
                    else {
                        $this->daysExpiryLastYear = (isset(auth()->user()->info->leave_day_last_year)&&auth()->user()->info->leave_day_last_year)?(float) auth()->user()->info->leave_day_last_year:0;
                    }
                }
            }
            else {
                $this->dateMoveCurrentYear = self::LAST_DAY_OF_YEAR.$this->currentYear;
                $this->dateMoveLastYear = self::LAST_DAY_OF_YEAR.($this->currentYear-1);
                $this->daysExpiryLastYear = 0;
            }
        }
        else {
            $this->dateMoveCurrentYear = self::LAST_DAY_OF_YEAR.$this->currentYear;
            $this->dateMoveLastYear = self::LAST_DAY_OF_YEAR.($this->currentYear-1);
            $this->daysExpiryLastYear = 0;
        }

        $this->currentYear = now()->format('Y');
        $this->daysUsedCurrentYear =  (isset(auth()->user()->info->leave_day_current_year)&&auth()->user()->info->leave_day_current_year)?(float) auth()->user()->info->leave_day_current_year:0;
        $leave_day_remaining = (isset(auth()->user()->info->leave_day_remaining)&&auth()->user()->info->leave_day_remaining)?(float) auth()->user()->info->leave_day_remaining:0;
        $this->daysUsedLastYear = (float) ($leave_day_remaining - $this->daysExpiryLastYear);
        $userInf = Auth()->user()->info;
        $contract = '';
        if($userInf){
            $contract = \App\Models\Contract::where('user_info_id', $userInf->id)->orderBy('contract.start_date','ASC')->get()->first();
        }
        if(isset($contract->start_date)&&$contract->start_date){
            $date = $contract->start_date;
            $curentMonth =  (int)Carbon::parse(now())->format('m');
            $curentYear = (int)Carbon::parse(now())->format('Y');
            $curentDate = (int)Carbon::parse(now())->format('d');
            $signMonth = (int)Carbon::parse($date)->format('m');
            $signYear = (int)Carbon::parse($date)->format('Y');
            $signDate = (int)Carbon::parse($date)->format('d');
            if($curentYear==$signYear) {
                if($curentDate - $signDate < 0){
                    $countDate =  $curentMonth - $signMonth -1;
                }
                else if($curentDate - $signDate >= 0 &&$curentDate - $signDate <= 15) {
                    $countDate =  $curentMonth - $signMonth;
                }
                else {
                    $countDate =  $curentMonth - $signMonth +1;
                }
            }
            else {
                $countDate = $curentMonth;
            }
        }
        else {
            $countDate = 0;
        }

        $seniority = getSeniority();
        $this->daysExpiryCurrentYear = (float) ($countDate + $seniority - $this->daysUsedCurrentYear);
    }
    public function setFromDate($data) {
        $this->fromdDate2= date('Y-m-d', strtotime($data['fromdDate']));
    }
    public function setToDate($data) {
        $this->toDate= date('Y-m-d', strtotime($data['toDate']));
    }
    public function render() {
        $user = UserInf::where('id', auth()->user()->user_info_id)->first();

        $start_time = Carbon::parse($this->fromdDate2);
        $end_time = Carbon::parse($this->toDate)->endOfDay();
        $data = RequestLeave::where('admin_id',auth()->id())
            ->where('start_date', ">=", $start_time)
            ->where('start_date', "<=", $end_time)
            ->orderBy('start_date','DESC')
            ->paginate($this->perPage);

        $contract = Contract::where('user_info_id', auth()->user()->user_info_id)->first();
        $user = UserInf::where('id', auth()->user()->user_info_id)->first();
        $leaves = $user->resquestLeaves;
        $dayLeave = 0;
        foreach($leaves as $item)  {
            $dayLeave += Carbon::parse($item->end_date)
            ->diffInDays(Carbon::parse($item->start_date));
        }
        $userInfoLeave = UserInfoLeave::where('status', 1)->where('user_info_id', auth()->user()->user_info_id)->first();
        if (!empty($userInfoLeave)) {
            if (!empty($contract && $userInfoLeave->leave_date)) {
               $year = (Carbon::parse($contract->start_date)->diffInDays(Carbon::parse($userInfoLeave->leave_date))-$dayLeave)/365;
            } else {
                $year = 0;
            }
        } else {
            if (!empty($contract)) {
                $year = (Carbon::parse($contract->start_date)->diffInDays(Carbon::parse($contract->end_date))-$dayLeave)/365;
            } else {
                $year = 0;
            }
        }

        $dataFromYearSeniority = (int)$year / 5;
        return view('livewire.admin.executive.my-day-off.index' , [
        	'user' => $user,
            'data' => $data,
            'dataFromYearSeniority' => $dataFromYearSeniority,
            'listStatus' => ERequestLeave::getListStatus(),
        ]);
    }
}
