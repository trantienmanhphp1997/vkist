<?php


namespace App\Enums;


class EAssetTypeDate {
    const DAY = 1;
    const MONTH = 2;
    const YEAR = 3;
}

